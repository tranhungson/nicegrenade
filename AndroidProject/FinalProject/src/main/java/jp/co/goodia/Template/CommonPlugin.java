package jp.co.goodia.Template;

import android.app.Activity;
import android.util.Log;

//import com.facebook.appevents.AppEventsLogger;
import com.unity3d.player.UnityPlayer;

import jp.co.goodia.Advertising.Providers.FlurryHelper;
import jp.co.goodia.Advertising.SceneManager;
import jp.co.goodia.Advertising.SplashAdManager;
import jp.co.goodia.Social.GooglePlayServices.GooglePlayServicesManager;
import jp.co.goodia.Social.RateApp;
import jp.co.goodia.Social.ShareUtils;

/*
 * CommonPlugin.java
 */

public class CommonPlugin {

    private static final String TAG = "CommonPlugin";

    private static Activity activity;

    //初期化
    public CommonPlugin() {
        Log.d(TAG, "CommonPlugin");
        activity = UnityPlayer.currentActivity;
    }

    //=================================================================================
    //コモン
    //=================================================================================

    //ウェブページ表示(日本)
    public void showCommonWebJPN() {
        String path = "";
        Log.d(TAG, "launchUrl:" + path);
        ShareUtils.launchUrl(activity, path);
    }

    //ウェブページ表示(海外)
    public void showCommonWebUSA() {
        String path = "";
        Log.d(TAG, "launchUrl:" + path);
        ShareUtils.launchUrl(activity, path);
    }

    //レビュー催促を表示する
    public void showReviewPopup() {
        Log.d(TAG, "showReviewPopup");
        RateApp.showRateDialog(activity);
    }

    //Flurryにイベント送信
    public void reportToFlurry(String key) {
        Log.d(TAG, "reportToFlurry:" + key);
//        FlurryHelper.reportFlurryWithEvent(key);
    }

    public void reportToFacebookAnalytic(String key) {
        Log.d(TAG, "reportToFacebook:" + key);
//        AppEventsLogger logger = AppEventsLogger.newLogger(activity);
//        logger.logEvent(key);
    }

    public void ShowAppStoreInApp() {
        String path = "";
        Log.d(TAG, "launchUrl:" + path);
//        ShareUtils.launchUrl(activity, path);
    }
    //=================================================================================
    //シェア
    //=================================================================================

    //ツイート
    public void launchTwitterWithMessage(String msg) {
        Log.d(TAG, "launchTwitterWithMessage:" + msg);
        ShareUtils.launchTwitterWithMessage(activity, msg);
    }

    //=================================================================================
    //ランキング
    //=================================================================================

    //リーダボード表示
    public void showLeaderBoard(int mode) {
        Log.d(TAG, "showLeaderBoard:" + mode);
        GooglePlayServicesManager.showWorldLeaderboard(activity);
    }

    //ランキングにスコア送信
    public void reportScore(String id, int score) {
        Log.d(TAG, "reportScore:" + id + ":" + score);
        GooglePlayServicesManager.pushAcomplishements(activity, score, Integer.parseInt(id));
    }

    public void reportScore(int id, int score) {
      GooglePlayServicesManager.pushAcomplishements(activity, score, id);
    }

    //=================================================================================
    //広告
    //=================================================================================

    //広告の表示設定
    public void updateAd(int scene) {
        Log.d(TAG, "updateAd:" + scene);
        SceneManager.setScene(activity, scene);
    }

    //リザルトで表示するスプラッシュ広告
    public void showSplashAd(int adCount) {
        Log.d(TAG, "showOptionalAd:" + adCount);

//        SplashAdManager.showOptionalAd(activity, adCount);
        UnityPlayer.UnitySendMessage("NativeManager", "FinishInterstial", "");
    }

    //終了時の広告
    public void showExitAd() {
    }

    //ウォール広告の表示
    public void showWallAd() {
    }

    public static boolean canShowRewardVideo() {

//        return  MovieAdsHelper.IsCanShowMovieAds(activity);
        return true;
    }
    public void showRewardVideo()
    {
//        MovieAdsHelper.ShowMovieAds(activity);
        UnityPlayer.UnitySendMessage("NativeManager", "FinishedPlayingMovieAd", "true");
    }

}
