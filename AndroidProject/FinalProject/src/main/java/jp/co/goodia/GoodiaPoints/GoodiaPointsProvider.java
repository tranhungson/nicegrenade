package jp.co.goodia.GoodiaPoints;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import androidx.annotation.NonNull;
import android.util.Log;

import jp.co.goodia.GoodiaPoints.GoodiaPointsSQLite.GoodiaPointsColums;

public class GoodiaPointsProvider extends ContentProvider {

    // TODO JavaDoc and instructions

    private final String TAG = "GoodiaPointsProvider";

    private static final String appName = "WigTyurun"; // IMPORTANT

    private static final String authority = "jp.co.goodia." + appName + ".GoodiaPoints.provider";
    private static final String uri = "content://" + authority
            + "/goodiapoints";
    public static final Uri CONTENT_URI = Uri.parse(uri);
    public static final String SELECTION = null;
    public static final String[] SELECTION_ARGS = null;
    public static final String SORT_ORDER = null;
    // Init the UriMatcher
    private static final UriMatcher uriMatcher;
    private static final int GOODIAPOINT_ID = 1;

    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(authority, "goodiapoints", GOODIAPOINT_ID);
    }

    private GoodiaPointsSQLite db = null;

    @Override
    public boolean onCreate() {
        Log.v(TAG, "onCreate");
        db = new GoodiaPointsSQLite(getContext());
        return true;
    }

    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        Log.v(TAG, "query()");
        // Return the goodiapoints row with only the right INT value
        Cursor c = null;
        if (uriMatcher.match(uri) == GOODIAPOINT_ID) {
            String where = GoodiaPointsSQLite.KEY_ID_GOODIAPOINTS + "='"
                    + GoodiaPointsSQLite.KEY_GOODIAPOINTS + "'";
            c = db.getWritableDatabase().query(
                    GoodiaPointsSQLite.TABLE_GOODIAPOINTS,
                    GoodiaPointsSQLite.PROJECTION_GOODIAPOINTS, where, null,
                    null, null, null);
        }
        return c;
    }

    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        Log.v(TAG, "update()");
        int cont = 0;
        // Decode the value
        String w = GoodiaPointsCrypto.decode(appName,
                values.getAsString(GoodiaPointsColums.COL_VALUE));
        // Parse as an int
        int x = Integer.parseInt(w);
        x = limitedGoodiaPoints(x);
        ContentValues finalValues = new ContentValues();
        finalValues.put(GoodiaPointsColums.COL_VALUE, "" + x);

        if (uriMatcher.match(uri) == GOODIAPOINT_ID) {
            String where = GoodiaPointsSQLite.KEY_ID_GOODIAPOINTS + "='"
                    + GoodiaPointsSQLite.KEY_GOODIAPOINTS + "'";
            cont = db.getWritableDatabase().update(
                    GoodiaPointsSQLite.TABLE_GOODIAPOINTS, finalValues, where,
                    null);
        }
        return cont;
    }

    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        Log.v(TAG, "delete()");
        return 0;
    }

    @Override
    public String getType(@NonNull Uri uri) {
        Log.v(TAG, "getType()");
        return null;
    }

    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        Log.v(TAG, "insert()");
        return null;
    }

    // Edit this for your Game
    private int limitedGoodiaPoints(int points) {
        if (points >= 9999) {
            return 9999;
        } else if (points <= 0) {
            return 0;
        } else {
            return points;
        }
    }

}
