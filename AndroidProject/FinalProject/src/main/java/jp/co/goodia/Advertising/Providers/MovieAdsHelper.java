package jp.co.goodia.Advertising.Providers;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import jp.co.goodia.Template.R;

import jp.co.goodia.Advertising.CheckSplSpfJson;
import jp.co.goodia.Advertising.FlagsManager;

public class MovieAdsHelper {

    private static final String TAG = "MovieAdsHelper";

    public static void doOnCreate(Activity activity) {

        ApplovinVideoAdsHelper.doOnCreate(activity);
    }

    public static  void ShowMovieAds(Activity activity) {

        ApplovinVideoAdsHelper.showVideoAd();
    }

    public static boolean IsCanShowMovieAds(Activity activity) {

        boolean result = false;
        result = ApplovinVideoAdsHelper.isCanShow();
        return result;
}

    public static boolean isJapaneseLanguage(Activity activity) {
        String lang = activity.getResources().getConfiguration().locale.getLanguage();
        if (lang.equals("ja")) {
            Log.d(TAG, "日本語");
            return true;
        }
        return false;
    }
}
