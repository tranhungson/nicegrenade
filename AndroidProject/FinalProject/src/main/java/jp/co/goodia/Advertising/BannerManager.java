package jp.co.goodia.Advertising;

import android.app.Activity;
import android.util.Log;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

public class BannerManager {

    // XXX probably can do this without passing a layout
    private static final String TAG = "BannerManager";

    public static void doOncreate(@SuppressWarnings("UnusedParameters") Activity activity, @SuppressWarnings("UnusedParameters") FrameLayout mainLayout) {
        Log.v(TAG, "doOnCreate()");
    }

    public static void doOnDestroy(Activity activity) {

    }

    public static void doOnPause(Activity activity) {

    }

    public static RelativeLayout getView(final Activity activity) {

        RelativeLayout bannerLayout = new RelativeLayout(activity);
        bannerLayout.setContentDescription("bannerLayout");
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT);
        lp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        lp.addRule(RelativeLayout.CENTER_HORIZONTAL);

        return bannerLayout;
    }
    public static boolean isJapaneseLanguage(Activity activity) {
        String lang = activity.getResources().getConfiguration().locale.getLanguage();
        if (lang.equals("ja")) {
            Log.d(TAG, "日本語");
            return true;
        }
        return false;
    }
}
