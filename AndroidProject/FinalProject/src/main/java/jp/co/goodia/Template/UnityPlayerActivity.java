package jp.co.goodia.Template;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;

import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdViewAdListener;
import com.applovin.mediation.ads.MaxAdView;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinSdkConfiguration;
import com.applovin.sdk.AppLovinSdkUtils;
import com.flurry.android.FlurryAgent;
import com.gameanalytics.sdk.GameAnalytics;
import com.unity3d.player.UnityPlayer;

import java.util.List;
import java.util.Locale;

import jp.co.goodia.Advertising.AdvertisingManager;
import jp.co.goodia.Advertising.SceneManager;
import jp.co.goodia.Social.GooglePlayServices.GooglePlayServicesManager;

public class UnityPlayerActivity extends Activity {
    protected UnityPlayer mUnityPlayer; // don't change the name of this
    // variable; referenced from native code

    private static final String TAG = "UnityPlayerActivity";
    private static MaxAdView adView = null;
    private static UnityPlayerActivity activity = null;

    // Setup activity layout
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activity = this;

        requestWindowFeature(Window.FEATURE_NO_TITLE);

        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        mUnityPlayer = new UnityPlayer(this);
        if (mUnityPlayer.getSettings().getBoolean("hide_status_bar", true))
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);

        int glesMode = mUnityPlayer.getSettings().getInt("gles_mode", 1);
        boolean trueColor8888 = true;
        mUnityPlayer.init(glesMode, trueColor8888);

        setContentView(mUnityPlayer.getView());
        mUnityPlayer.requestFocus();
/*
        GameAnalytics.configureBuild("android 1.0.1");
        GameAnalytics.initializeWithGameKey(activity, activity.getResources().getString(R.string.GameAnalytics_App_Key), activity.getResources().getString(R.string.GameAnalytics_Hash_Key));

        AppLovinSdk.getInstance(getApplicationContext()).setMediationProvider("max");
        AppLovinSdk.initializeSdk( this, new AppLovinSdk.SdkInitializationListener() {
            @Override
            public void onSdkInitialized(final AppLovinSdkConfiguration configuration)
            {

//                AppLovinSdk.getInstance(activity).showMediationDebugger();
                AdvertisingManager.doOnCreateApplovin();
                final Handler handler = new Handler();
                handler.postDelayed( new Runnable()
                {
                    @Override
                    public void run()
                    {
                        showBanner();
                    }
                }, 4000 );

            }
        } );

        AdvertisingManager.doOnCreate(this, (FrameLayout) getWindow().getDecorView());
        SceneManager.setWindowSize(this, 320, 480);
        GooglePlayServicesManager.doOnCreate(this);
*/
        if (isOnline()) {
            Log.d(TAG, "online");

            final SharedPreferences sharedPreferences = this.getSharedPreferences(
                    "NewsPopUp", Context.MODE_PRIVATE);
            int count = sharedPreferences.getInt("LaunchCount", 0);
            // 初回起動時はポップアップを表示しない
            if (count < 1) {
                SharedPreferences.Editor editor = sharedPreferences.edit();
                if (editor != null) {
                    editor.putInt("LaunchCount", count + 1);
                    editor.apply();
                }
            } else {

            }
        }
    }

    // Quit Unity
    @Override
    protected void onDestroy() {
//		AdvertisingManager.doOnDestroy(this);
        mUnityPlayer.quit();
//        GooglePlayServicesManager.doOnDestroy(this);
        super.onDestroy();
    }

    // Pause Unity
    @Override
    protected void onPause() {
        super.onPause();
        mUnityPlayer.pause();
//        AdvertisingManager.doOnPause(this);
    }

    // Resume Unity
    @Override
    protected void onResume() {
        super.onResume();
        mUnityPlayer.resume();
//        AdvertisingManager.doOnResume(this);
    }

    protected void onStart() {
        super.onStart();
        AdvertisingManager.doOnStart(this);
//        GooglePlayServicesManager.doOnStart(this);
    }

    protected void onStop() {
        AdvertisingManager.doOnStop(this);
//        GooglePlayServicesManager.doOnStop(this);
        super.onStop();
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    @Override
    public void onBackPressed() {
        Log.d(TAG, "onBackPressed");
        //AdvertisingManager.doOnBackPressed(this);
        AdvertisingManager.showExitDialog(this);
    }

    @Override
    protected void onActivityResult(int request, int response, Intent data) {
        super.onActivityResult(request, response, data);
//        GooglePlayServicesManager.doOnActivityResult(request, response, data);
    }

    // This ensures the layout will be correct.
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mUnityPlayer.configurationChanged(newConfig);
    }

    // Notify Unity of the focus change.
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        mUnityPlayer.windowFocusChanged(hasFocus);
    }

    // For some reason the multiple keyevent type is not supported by the ndk.
    // Force event injection by overriding dispatchKeyEvent().
    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_MULTIPLE)
            return mUnityPlayer.injectEvent(event);
        return super.dispatchKeyEvent(event);
    }

    // Pass any events not handled by (unfocused) views straight to UnityPlayer
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        return mUnityPlayer.injectEvent(event);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        Log.d(TAG, "onKeyDown");

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            //AdvertisingManager.doOnBackPressed(this);
            AdvertisingManager.showExitDialog(this);
            return true;
        }

        return mUnityPlayer.injectEvent(event);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return mUnityPlayer.injectEvent(event);
    }

    /* API12 */
    public boolean onGenericMotionEvent(MotionEvent event) {
        return mUnityPlayer.injectEvent(event);
    }

    // アプリケーション名
    private String getAppName(String title) {
        String[] strsA;
        String[] strsB;

        Log.d(TAG, "title:" + title);
        strsA = title.split("「");
        strsB = strsA[1].split("」");
        Log.d(TAG, "strsB[0]:" + strsB[0]);

        return strsB[0];
    }

    // インストール済みかどうか
    private boolean checkInstalled(String scheme) {
        PackageManager pm = this.getPackageManager();
        List<ApplicationInfo> infos = pm.getInstalledApplications(0);
        for (ApplicationInfo ai : infos) {
            if (scheme != null && scheme.equals(ai.packageName)) {
                return true;
            }
        }
        return false;
    }

    // オンラインかどうか
    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) this
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();

        if (info != null) {
            int type = info.getType();
            if (type == ConnectivityManager.TYPE_MOBILE
                    || type == ConnectivityManager.TYPE_WIFI) {
                Log.d("myTag", "NetworkInfo:" + info.getTypeName());
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    // レコメンドダイアログ
    private void createRecomDialog(final String title, final String url) {

        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        if (Locale.getDefault().equals(Locale.JAPAN)) {
            adb.setTitle("お知らせ");
            adb.setMessage(title);
            adb.setPositiveButton("はい", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // Flurry
                    FlurryAgent.logEvent("POPUP_TAP_YES:" + getAppName(title));

                    Uri uri = Uri.parse(url);
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);
                }
            });
            adb.setNegativeButton("いいえ", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
        } else {
            adb.setTitle("Hot News!");
            adb.setMessage(title);
            adb.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // Flurry
                    FlurryAgent.logEvent("POPUP_TAP_YES:" + getAppName(title));

                    Uri uri = Uri.parse(url);
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);
                }
            });
            adb.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
        }
        AlertDialog ad = adb.create();
        ad.show();
    }

    private void showBanner()
    {
        Log.v("ApplovinHelper", "show banner...");

        adView = new MaxAdView( activity.getResources().getString(R.string.APPLOVIN_BANNER), this );

        final boolean isTablet = AppLovinSdkUtils.isTablet(getApplicationContext()); // Available on Android SDK 9.6.2+
        final int heightPx = AppLovinSdkUtils.dpToPx( this, isTablet ? 90 : 50 );
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams( ViewGroup.LayoutParams.MATCH_PARENT, heightPx );
        params.gravity = Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM;
        adView.setLayoutParams(params);
        adView.setBackgroundColor(Color.WHITE);
        adView.setListener(new MaxAdViewAdListener() {
            @Override
            public void onAdExpanded(MaxAd ad) {

            }

            @Override
            public void onAdCollapsed(MaxAd ad) {

            }

            @Override
            public void onAdLoaded(MaxAd ad) {
                Log.v("ApplovinHelper : banner", "onAdLoaded ...");
            }

            @Override
            public void onAdLoadFailed(String adUnitId, int errorCode) {
                Log.v("ApplovinHelper : banner", "onAdLoadFailed ... " + errorCode);
            }

            @Override
            public void onAdDisplayed(MaxAd ad) {
                Log.v("ApplovinHelper : banner", "onAdDisplayed ...");
            }

            @Override
            public void onAdHidden(MaxAd ad) {
                Log.v("ApplovinHelper : banner", "onAdHidden ...");
            }

            @Override
            public void onAdClicked(MaxAd ad) {
                Log.v("ApplovinHelper : banner", "onAdClicked ...");
            }

            @Override
            public void onAdDisplayFailed(MaxAd ad, int errorCode) {
                Log.v("ApplovinHelper : banner", "onAdDisplayFailed ..." + errorCode);
            }
        });

        FrameLayout view = (FrameLayout) getWindow().getDecorView();
        view.addView(adView);

        adView.loadAd();
    }

    public static void hideBanner(boolean isHide)
    {
        if (adView != null) {
            if (isHide)
                adView.setVisibility(View.GONE);
            else
                adView.setVisibility(View.VISIBLE);
        }
    }
}
