package jp.co.goodia.GoodiaPoints;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;

public class GoodiaPointsSQLite extends SQLiteOpenHelper {

    public static final class GoodiaPointsColums implements BaseColumns {
        private GoodiaPointsColums() {
        }

        public static final String COL_KEY = "key";
        public static final String COL_VALUE = "value";
    }

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "goodiapoints.db";
    public static final String TABLE_GOODIAPOINTS = "goodiapoints";
    public static final String KEY_ID_GOODIAPOINTS = GoodiaPointsColums.COL_KEY;
    public static final String KEY_GOODIAPOINTS = "goodiapoints";
    public static final String[] PROJECTION_GOODIAPOINTS = {GoodiaPointsColums.COL_VALUE};
    private static final String CREATE_TABLE_GOODIAPOINTS = "CREATE TABLE "
            + TABLE_GOODIAPOINTS + " (_id INT PRIMARY KEY, "
            + GoodiaPointsColums.COL_KEY + " TEXT, "
            + GoodiaPointsColums.COL_VALUE + " INT)";
    private static final String INTIALIZE_GOODIAPOINTS = "INSERT INTO "
            + TABLE_GOODIAPOINTS + " ( " + GoodiaPointsColums.COL_KEY + ","
            + GoodiaPointsColums.COL_VALUE + ") VALUES ('" + KEY_GOODIAPOINTS
            + "','0') ";

    public GoodiaPointsSQLite(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        Log.d("CONTENT_PROVIDER", "GoodiaPointsSQLite()");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d("CONTENT_PROVIDER", "onCreate()");
        db.execSQL(CREATE_TABLE_GOODIAPOINTS);
        db.execSQL(INTIALIZE_GOODIAPOINTS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d("CONTENT_PROVIDER", "onUpgrade():" + oldVersion + ":" + newVersion);
        //db.execSQL("DROP TABLE IF EXISTS " + TABLE_GOODIAPOINTS);
        //onCreate(db);

    }

}
