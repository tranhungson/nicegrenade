package jp.co.goodia.GoodiaPoints;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

// Need the <uses-permission android:name="jp.co.goodia.GoodiaPoints" />

public class GoodiaPointsUtils {

    // TODO JavaDoc

    private static final String TAG = "GoodiaPointsUtils";

    public static void addGoodiaPoints(Context context, String appName,
                                       int points) {
        setGoodiaPoints(context, appName, getGoodiaPoints(context, appName) + points);
    }

    public static void addGoodiaPoints(Context context, String appName,
                                       String points) {
        addGoodiaPoints(context, appName, Integer.parseInt(points));
    }

    public static void setGoodiaPoints(Context context, String appName,
                                       int points) {
        setGoodiaPoints(context, appName, Integer.toString(points));
    }

    public static void setGoodiaPoints(Context context, String appName,
                                       String points) {
        // Create key and cipher
        int i = 0;
        if (existsContentProvider(context, appName)) {
            try {
                ContentValues values = new ContentValues();
                values.put("value", GoodiaPointsCrypto.encode(appName, points));
                i = context.getContentResolver().update(
                        Uri.parse("content://jp.co.goodia." + appName
                                + ".GoodiaPoints.provider/goodiapoints"),
                        values, null, null);
            } catch (Exception e) {
                e.printStackTrace();
                Log.d(TAG, "Provider Not Found");
            }
        }
        if (i == 1) {
            Log.d(TAG, "setGoodiaPoints(" + points + ") OK");
        } else {
            Log.d(TAG, "setGoodiaPoints(" + points + ") FAILED");
        }

    }

    public static String getGoodiaPointsAsString(Context context, String appName) {
        return Integer.toString(getGoodiaPoints(context, appName));
    }

    public static int getGoodiaPoints(Context context, String appName) {
        int value = 0;
        Cursor c = null;
        if (existsContentProvider(context, appName)) {
            try {
                c = context.getContentResolver().query(
                        Uri.parse("content://jp.co.goodia." + appName
                                + ".GoodiaPoints.provider/goodiapoints"), null,
                        null, null, null);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (c != null && c.moveToFirst()) {
                int colValue = c.getColumnIndex("value");
                do {
                    value = c.getInt(colValue);
                } while (c.moveToNext());
                c.close();
            }
        }
        Log.d(TAG, "GoodiaPoints=" + value);
        return value;
    }

    public static boolean existsContentProvider(Context context, String appName) {
        try {
            if (context.getContentResolver().acquireContentProviderClient(
                    "jp.co.goodia." + appName + ".GoodiaPoints.provider") == null) {
                Log.d(TAG, "Content provider 'jp.co.goodia." + appName
                        + ".GoodiaPoints.provider' doesn't exist");
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

}
