package jp.co.goodia.Advertising.Providers;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.util.Log;

import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxReward;
import com.applovin.mediation.MaxRewardedAdListener;
import com.applovin.mediation.ads.MaxRewardedAd;
import com.unity3d.player.UnityPlayer;

import jp.co.goodia.Template.R;

public class ApplovinVideoAdsHelper {

    private static final String TAG = "ApplovinVideoAdsHelper";

    private static MaxRewardedAd rewardedAd;
    private  static  boolean isSuccess = false;
    private static Activity _activity;
    public static void doOnCreate(Activity activity) {

        _activity = activity;
        createRewardVideoAd();
        Log.v(TAG, "create");
    }

    public static void createRewardVideoAd()
    {
        rewardedAd = MaxRewardedAd.getInstance(_activity.getResources().getString(R.string.APPLOVIN_REWARVIDEO),_activity);
        rewardedAd.setListener(new MaxRewardedAdListener() {
            @Override
            public void onRewardedVideoStarted(MaxAd ad) {

                Log.v(TAG, "onRewardedVideoStarted");

                UnityPlayer.UnitySendMessage("NativeManager", "OnLockUI", "");
            }

            @Override
            public void onRewardedVideoCompleted(MaxAd ad) {
                Log.v(TAG, "onRewardedVideoCompleted");
                isSuccess = true;
            }

            @Override
            public void onUserRewarded(MaxAd ad, MaxReward reward) {
                Log.v(TAG, "onUserRewarded");
                isSuccess = true;
            }

            @Override
            public void onAdLoaded(MaxAd ad) {
                Log.v(TAG, "onAdLoaded");
                UnityPlayer.UnitySendMessage("NativeManager", "FinishLoadRewardVideo", "");
            }

            @Override
            public void onAdLoadFailed(String adUnitId, int errorCode) {
                Log.v(TAG, "onAdLoadFailed" + errorCode);
                final Handler handler = new Handler();
                handler.postDelayed( new Runnable()
                {
                    @Override
                    public void run()
                    {
                        rewardedAd.loadAd();
                    }
                }, 3000 );
            }

            @Override
            public void onAdDisplayed(MaxAd ad) {
                Log.v(TAG, "onAdDisplayed");
            }

            @Override
            public void onAdHidden(MaxAd ad) {
                Log.v(TAG, "onAdHidden");
                if (isSuccess){

                    UnityPlayer.UnitySendMessage("NativeManager", "FinishedPlayingMovieAd", "true");
                    finishRewardVideo();
                }else {

                    UnityPlayer.UnitySendMessage("NativeManager", "FinishedPlayingMovieAd", "false");
                }
                rewardedAd.loadAd();
            }

            @Override
            public void onAdClicked(MaxAd ad) {
                Log.v(TAG, "onAdClicked");
            }

            @Override
            public void onAdDisplayFailed(MaxAd ad, int errorCode) {
                Log.v(TAG, "onAdDisplayFailed " + errorCode);
                rewardedAd.loadAd();
            }
        });
        rewardedAd.loadAd();
    }

    //動画広告を表示
    public static void showVideoAd(){

        Log.v(TAG, "showVideoAd(canshow : " + isSuccess+ ")");

        isSuccess = false;

        if (rewardedAd.isReady()) {
            rewardedAd.showAd();

        }
        else{
            Log.v(TAG, "can't show video...");
            UnityPlayer.UnitySendMessage("NativeManager", "FinishedPlayingMovieAd", "false");
        }
    }

    public static void finishRewardVideo()
    {
        // Save last mil
        SharedPreferences sharedPreferences = _activity.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong("Interstitial_delay", System.currentTimeMillis());
        editor.putBoolean("RewardVideo_Finished", true);

        editor.commit();
    }

    public static boolean isCanShow()
    {
        boolean result = rewardedAd.isReady();
        return result;
    }
}
