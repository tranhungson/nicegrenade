package jp.co.goodia.GoodiaPoints;

import android.util.Base64;
import android.util.Log;

public class GoodiaPointsCrypto {
    private static final String TAG = "GoodiaPointsCrypto";
    private static final String TOKEN = "###";

    // Set up secret key spec for 128-bit AES encryption and decryption
    public static String encode(String key, String points) {
        String s = Base64.encodeToString((key + TOKEN + points).getBytes(), Base64.DEFAULT);
        Log.d(TAG, "Encoded key='" + key + "' points='" + points + "' result='" + s + "'");
        return s;
    }

    public static String decode(String key, String encode) {
        String s = new String(Base64.decode(encode.getBytes(), Base64.DEFAULT));
        s = s.replaceAll(key + TOKEN, "");
        Log.d(TAG, "Decoded key='" + key + "' encode='" + encode + "' result='" + s + "'");
        return s;
    }
}
