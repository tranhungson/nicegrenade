package jp.co.goodia.Advertising;

public class FlagsManager {

    public static final String IMOBILE = "I";
    public static final String NEND = "N";
    public static final String NEND_FULLSCREEN = "NF";
    public static final String ADSTIR = "S";
    public static final String IDX = "IX";
    public static final String FIVE = "F";
    public static final String APPLOVIN = "L";
}
