package jp.co.goodia.Advertising.Providers;

import jp.co.goodia.Template.R;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;

import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdListener;
import com.applovin.mediation.ads.MaxInterstitialAd;
import com.applovin.sdk.AppLovinSdk;
import com.unity3d.player.UnityPlayer;

import jp.co.goodia.Advertising.SplashAdManager;

/**
 * Library dependencies: i-mobileSDK.jar imobileSdkAds.jar google-play-services
 * <p/>
 * AndroidManifest.xml:
 * <uses-permission android:name="android.permission.INTERNET"/>
 * <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
 * <p/>
 * <application>
 * <meta-data android:name="com.google.android.gms.version"
 * android:value="@integer/google_play_services_version" />
 * </application>
 *
 * @author Aaron Santin - 2014
 * @version 1.0
 */
public class AppLovinHelper {
    // XXX iMobile also has wall, banner, bigbanner, rectanglebanner, implement
    // it in the future

    private static final String TAG = "AppLovinHelper";
    static MaxInterstitialAd interstitialAd;
    private static boolean canShowAds = false;

    // Default values for icons
    private static final String titleColor = "#FFFFFF";
    private static boolean canShowRateAndReview = false;
    private static final int icons = 1;
    private static final int iconSize = 57;

    // 緊急対応(LinearLayout.VERTICAL未対応)
    // Notice: 1 LayoutView needs 1 spotID!! OMG!!
    private static int spotIdIndex = 0;

    /**
     * Put this method on your activity onCreate method to initialize the
     * iMobile provider
     *
     * @param activity The main activity
     */
    public static void doOnCreate(Activity activity) {
        Log.v(TAG, "doOnCreate()");

        // Save first mil
        SharedPreferences sharedPreferences = activity.getPreferences(Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPreferences.edit();

        long delay = sharedPreferences.getLong("Interstitial_delay", 0);
        if (delay == 0) {

            editor.putLong("Interstitial_delay", System.currentTimeMillis());
            editor.commit();
        }

        AppLovinSdk.getInstance(activity);
        AppLovinSdk.initializeSdk(activity);
        initFullScreenApplovin(activity);
    }

    /**
     * Put this method on your activity onPause method
     */
    public static void doOnPause() {
    }

    /**
     * Put this method on your activity onResume method
     */
    public static void doOnResume() {
    }

    /**
     * Put this method on your activity onDestroy method
     */
    public static void doOnDestroy() {
        Log.v(TAG, "doOnDestroy()");
        // for leaked windows
    }

    private static void initFullScreenApplovin(final Activity activity) {

        interstitialAd = new MaxInterstitialAd(activity.getResources().getString(R.string.APPLOVIN_INTERSITITAL), activity);
        interstitialAd.setListener(new MaxAdListener() {
            @Override
            public void onAdLoaded(MaxAd ad) {
                Log.v(TAG, "onAdLoaded()");
            }

            @Override
            public void onAdLoadFailed(String adUnitId, int errorCode) {
                Log.v(TAG, "onAdLoadFailed()");
                final android.os.Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        interstitialAd.loadAd();
                    }
                }, 3000);
            }

            @Override
            public void onAdDisplayed(MaxAd ad) {
                Log.v(TAG, "onAdDisplayed()");
            }

            @Override
            public void onAdHidden(MaxAd ad) {
                Log.v(TAG, "onAdHidden() " + canShowRateAndReview);
                canShowAds = false;

                // Save last mil
                SharedPreferences sharedPreferences = activity.getPreferences(Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putLong("Interstitial_delay", System.currentTimeMillis());
                editor.commit();
                interstitialAd.loadAd();
                UnityPlayer.UnitySendMessage("NativeManager", "FinishInterstial", "");

                if (canShowRateAndReview) {
                    ShowRateAndReview(activity);
                }
            }

            @Override
            public void onAdClicked(MaxAd ad) {

            }

            @Override
            public void onAdDisplayFailed(MaxAd ad, int errorCode) {
                Log.v(TAG, "onAdDisplayFailed()");
                interstitialAd.loadAd();
                UnityPlayer.UnitySendMessage("NativeManager", "FinishInterstial", "");
            }
        });
        interstitialAd.loadAd();
    }

    public static void showAd(final Activity activity, final int adCount) {

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {


                SharedPreferences sharedPreferences = activity.getPreferences(Context.MODE_PRIVATE);

                SharedPreferences.Editor editor = sharedPreferences.edit();

                long currentMil = System.currentTimeMillis();
                long lastMil = sharedPreferences.getLong("Interstitial_delay", 0);

                boolean rewardFinished = sharedPreferences.getBoolean("RewardVideo_Finished", false);
                boolean _canRateAndReview = sharedPreferences.getBoolean("RateAndReview", true);
                float distance = 40;
                if (rewardFinished) {
                    distance = 30;
                }
                Log.d("Applovin", "Start" + (currentMil - lastMil) / 1000);
                if ((currentMil - lastMil) / 1000 >= distance) {

                    canShowAds = true;

                    editor.putBoolean("RewardVideo_Finished", false);
                    editor.commit();
                }

                if (interstitialAd.isReady() && canShowAds) {

                    interstitialAd.showAd();
                    if (adCount == 11 && _canRateAndReview) {
                        canShowRateAndReview = true;
                    }
                } else {
                    if (adCount == 11 && _canRateAndReview) {
                        ShowRateAndReview(activity);
                    }
                    UnityPlayer.UnitySendMessage("NativeManager", "FinishInterstial", "");
                }
            }
        });
    }

    public static boolean CanShowInter(final Activity activity, int playCount) {
        boolean result = false;
        SharedPreferences sharedPreferences = activity.getPreferences(Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        long currentMil = System.currentTimeMillis();
        long lastMil = sharedPreferences.getLong("Interstitial_delay", 0);

        boolean rewardFinished = sharedPreferences.getBoolean("RewardVideo_Finished", false);
        boolean _canRateAndReview = sharedPreferences.getBoolean("RateAndReview", true);
        float distance = 40;
        if (rewardFinished) {
            distance = 30;
        }
        if ((currentMil - lastMil) / 1000 >= distance && playCount >= 1 && interstitialAd.isReady()) {
            result = true;
        }

        return result;
    }

    private static void ShowRateAndReview(final Activity activity) {

        SharedPreferences sharedPreferences = activity.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("RateAndReview", false);
        editor.commit();

        canShowRateAndReview = false;
        final Uri uri = Uri.parse("market://details?id=" + activity.getApplicationContext().getPackageName());
        final Intent rateAppIntent = new Intent(Intent.ACTION_VIEW, uri);

        if (activity.getPackageManager().queryIntentActivities(rateAppIntent, 0).size() > 0) {
            Log.d("RateAndReview", "Show");
            activity.startActivity(rateAppIntent);
        } else {
            Log.d("RateAndReview", "Can't show");
        }
    }
}
