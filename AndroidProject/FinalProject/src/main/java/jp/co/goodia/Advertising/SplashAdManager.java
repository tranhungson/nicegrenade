package jp.co.goodia.Advertising;

import java.util.EventListener;

import android.app.Activity;
import android.util.Log;

import jp.co.goodia.Advertising.Providers.AppLovinHelper;


public class SplashAdManager {
	// TODO javadoc

	private static final String TAG = "SplashAdManager";

	//インタースティシャル広告 結果通知用リスナー
	public abstract interface SplashAdListener extends EventListener {
		//閉じた時
		public abstract void onClose();
	}

	;


	public static void doOnCreate(Activity activity) {
		CheckSplSpfJson.doOnCreate(activity);
	}

	public static void doOnBackPressed(Activity activity) {
	}

	public static void showOptionalAd(Activity activity, int adCount) {
		Log.d(TAG, "showOptionalAd:" + adCount);

		AppLovinHelper.showAd(activity,adCount);
	}

	public static boolean isJapaneseLanguage(Activity activity) {
		String lang = activity.getResources().getConfiguration().locale.getLanguage();
		if (lang.equals("ja")) {
			Log.d(TAG, "日本語");
			return true;
		}
		return false;
	}
}
