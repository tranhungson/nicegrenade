package jp.co.goodia.Social.GooglePlayServices;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.Games;
import com.google.example.games.basegameutils.GameHelper;

import jp.co.goodia.Template.R;

public class GooglePlayServicesManager {
    private final static String TAG = "GooglePlayServicesMng";
    private final static String SHARED_PREFERENCES_FILE_NAME = "GooglePlayServicesManager";
    private final static String SHARED_PREFERENCES_KEY_LOGIN_IGNORED = "SharedPreferencesKeyLoginIgnored";

    private final static int REQUEST_ACHIEVEMENTS = 111;
    private final static int REQUEST_LEADERBOARDS = 222;
    private final static int REQUEST_LEADERBOARD = 333;

    private static GameHelper mHelper;
    private static final int mRequestedClients = GameHelper.CLIENT_GAMES;
    private static DefaultOutbox mOutbox;

    private static final boolean mDebugLog = true;

    private static Activity activity = null;

    public static void doOnCreate(Activity activity) {
        Log.d(TAG, "doOnCreate()");

        GooglePlayServicesManager.activity = activity;

        if (mHelper == null) {
            mHelper = new GameHelper(activity, mRequestedClients);
            mHelper.enableDebugLog(mDebugLog);
        }

        try {
            mHelper.setup(getGameHelperListener());
            if (mHelper.isSignedIn()) {
                Log.d(TAG, "Already signed in YO!!");
            } else {
                if (!GooglePlayServicesManager.activity.getSharedPreferences(SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE).getBoolean(SHARED_PREFERENCES_KEY_LOGIN_IGNORED, false)) {
                    Log.d(TAG, "Trying to show the login window...");
                    mHelper.beginUserInitiatedSignIn();
                } else {
                    Log.d(TAG, "The login was skipped the first time...");
                    mHelper.setConnectOnStart(false);
                }
            }
        } catch (IllegalStateException e) {
            // don't do it twice...
            Log.e(TAG, "IllegalStateException:" + e.toString());
            return;
        }

        mOutbox = new DefaultOutbox();
        mOutbox.loadLocal(activity);

    }

    public static void doOnStart(Activity activity) {
        Log.d(TAG, "doOnStart()");

        mHelper.onStart(activity);
    }

    public static void doOnStop(Activity activity) {
        Log.d(TAG, "doOnStop()");

        mHelper.onStop();
        mOutbox.saveLocal(activity);
    }

    public static void doOnDestroy(Activity activity) {
        Log.d(TAG, "doOnDestroy()");

        mHelper = null;
        mOutbox = null;
    }

    public static void doOnActivityResult(int request, int response, Intent data) {
        Log.d(TAG, "doOnActivityResult()");

        mHelper.onActivityResult(request, response, data);
    }

    private static GoogleApiClient getApiClient() {
        return mHelper.getApiClient();
    }

    public static boolean isSignedIn() {
        return mHelper.isSignedIn();
    }

    public static void beginUserInitiatedSignIn() {
        Log.d(TAG, "beginUserInitiatedSignIn()");

        mHelper.beginUserInitiatedSignIn();
    }

    public static void signOut() {
        mHelper.signOut();
    }

    public static void showAlert(String message) {
        mHelper.makeSimpleDialog(message).show();
    }

    public static void showAlert(String title, String message) {
        mHelper.makeSimpleDialog(title, message).show();
    }

    public static String getInvitationId() {
        return mHelper.getInvitationId();
    }

    public static void reconnectClient() {
        mHelper.reconnectClient();
    }

    public static boolean hasSignInError() {
        return mHelper.hasSignInError();
    }

    public static GameHelper.SignInFailureReason getSignInError() {
        return mHelper.getSignInError();
    }

    private static GameHelper.GameHelperListener getGameHelperListener() {

        return new GameHelper.GameHelperListener() {

            @Override
            public void onSignInSucceeded() {
                Log.v(TAG, "The sign in fuction worked!");
                GooglePlayServicesManager.activity.getSharedPreferences(SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE).edit().putBoolean(SHARED_PREFERENCES_KEY_LOGIN_IGNORED, false).apply();
            }

            @Override
            public void onSignInFailed() {
                Log.i(TAG, "Login ignored or failed");
                GooglePlayServicesManager.activity.getSharedPreferences(SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE).edit().putBoolean(SHARED_PREFERENCES_KEY_LOGIN_IGNORED, true).apply();
            }
        };

    }

    public static void showAchievements(Activity activity) {
        if (isSignedIn()) {
            activity.startActivityForResult(
                    Games.Achievements.getAchievementsIntent(getApiClient()),
                    REQUEST_ACHIEVEMENTS);
        } else {
            showAlert(activity.getString(R.string.achievements_not_available));
        }
    }

    public static void showLeaderboards(final Activity activity) {
        if (isSignedIn()) {
            activity.startActivityForResult(
                    Games.Leaderboards.getAllLeaderboardsIntent(getApiClient()),
                    REQUEST_LEADERBOARDS);
        } else {
            beginUserInitiatedSignIn();
        }
    }

    public static void showWorldLeaderboard(final Activity activity) {
        if (isSignedIn()) {
            try {
                activity.startActivityForResult(
                        Games.Leaderboards.getAllLeaderboardsIntent(getApiClient()),
                        REQUEST_LEADERBOARD);
            } catch (Exception e) {
                Log.w(TAG, "Really not signed in");
                e.printStackTrace();
                beginUserInitiatedSignIn();
            }
        } else {
            beginUserInitiatedSignIn();
        }
    }

    public static void pushAcomplishements(Activity activity, long score, int mode) {
        Log.d(TAG, "pushAcomplishements (activity, " + score + ", " + mode + ")");
        if (!isSignedIn()) {
			// can't push to the cloud, so save locally
			Log.v(TAG,"Not signed in ");
			mOutbox.saveLocal(activity, score);
			return;
		}

		try {
			mOutbox.saveLocal(activity, score);
			if (!mOutbox.isEmpty()) {
				int id;
				switch (mode){
				case 1:
					id = R.string.leaderboard_world_ranking_1;
					Games.Leaderboards.submitScore(getApiClient(),
							activity.getString(id), score);
					break;
                    case 2:
                        id = R.string.leaderboard_world_ranking_2;
                        Games.Leaderboards.submitScore(getApiClient(),
                                activity.getString(id), score);
                        break;
                    case 3:
                        id = R.string.leaderboard_world_ranking_3;
                        Games.Leaderboards.submitScore(getApiClient(),
                                activity.getString(id), score);
                        break;
				default:
					break;
				}
			}
		} catch (Exception e) {
			Log.w(TAG, "Probably not really signed in");
			e.printStackTrace();
		}
    }

    public static void reportScore(Activity activity, String id, long score) {

        if (!isSignedIn()) {
            // can't push to the cloud, so save locally
            Log.v(TAG, "Not signed in ");
            mOutbox.saveLocal(activity, score);
            return;
        }

        try {
            mOutbox.saveLocal(activity, score);
            if (!mOutbox.isEmpty()) {
                Games.Leaderboards.submitScore(getApiClient(), id, score);
            }
        } catch (Exception e) {
            Log.w(TAG, "Probably not really signed in");
            e.printStackTrace();
        }
    }
}
