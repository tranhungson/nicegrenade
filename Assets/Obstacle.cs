﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{    
    Rigidbody _body;

    private void Awake()
    {
        _body = GetComponent<Rigidbody>();        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Explostion"))
        {
            _body.isKinematic = false;

            _body.AddExplosionForce(1500, other.transform.position, 3);
        }
    }
}
