﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using DG.Tweening.Core;

public class DelayProgressBar : SingletonMonoBehaviour<DelayProgressBar>
{
    public UIProgressBar Bar;
    public GameObject Warning;
    public UILabel CountdownLabel;

    public Gradient ColorBar;

    float _value;

    public Grenade CurrentGrenade { set; get; } = null;

    public bool CanThrow { set; get; } = true;

    public bool ShouldExplode { set; get; } = true;

    private void OnEnable()
    {
        _value = Bar.value;
        
        CountdownLabel.text = string.Format("{0}s", Mathf.RoundToInt(UserData.Delay));
    }

    public void StartCountDown()
    {
        UIController.Instance.HideHelp();

        if(UserData.LevelCount < 2)
            Warning.SetActive(true);

        ShouldExplode = true;

        DOTween.To(() => _value, x => { _value = x; Bar.value = _value; CountdownLabel.text = string.Format("{0}s", Mathf.RoundToInt(_value * UserData.Delay)); }, 0, UserData.Delay).SetEase(Ease.Linear).OnComplete(() =>
        {
            Warning.SetActive(false);

            if (CurrentGrenade == null)
            {
                if(ShouldExplode)
                    GameController.Instance.ForceGameOver();
            }
            else
            {
                CurrentGrenade.Explode();
            }

            CurrentGrenade = null;

            StopCountDown();

            CanThrow = true;

        }).SetTarget(this); ;
    }

    public void StopCountDown()
    {
        DOTween.Kill(this);

        _value = 1;

        Bar.value = _value;

        CountdownLabel.text = string.Format("{0}s", Mathf.RoundToInt(UserData.Delay));
    }
}
