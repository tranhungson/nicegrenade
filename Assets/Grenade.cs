﻿using UnityEngine;

public class Grenade : MonoBehaviour
{
    public bool ActiveBlastZone { set; get; } = true;

    bool _vibrate = true;

    public void Explode()
    {
        GameController.Instance.Explose(transform, ActiveBlastZone);

        gameObject.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Water"))
        {
            ActiveBlastZone = false;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(_vibrate)
        {
            _vibrate = false;

            CustomVibration.Do(EVibrationType.VibrateLight_Pop);
        }

        if(collision.gameObject.CompareTag("Ground"))
        {
            SEManager.Instance.PlaySE("bound_02");
        }
        else if (collision.gameObject.CompareTag("Wall"))
        {
            SEManager.Instance.PlaySE("bound_01");
        }

    }

}
