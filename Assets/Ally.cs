﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Ally : Character
{
    private void OnEnable()
    {
        OnDead += GameOver;
    }

    private void OnDisable()
    {
        OnDead -= GameOver;
    }

    void GameOver()
    {
        if (GameController.Instance.GameEnd)
            return;

        GameController.Instance.GameEnd = true;

        TrajectoryController.Instance.HideAll();

        DelayProgressBar.Instance.gameObject.SetActive(false);

        DOTween.Sequence().AppendInterval(1).AppendCallback(() => {

            UIController.Instance.GameOver();
        });
    }
}