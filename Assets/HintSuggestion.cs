﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class HintSuggestion : MonoBehaviour
{
    public Transform Target;

    private Vector3 _pos;
    private Transform _transform;
    private UISprite _sprite;
    private Sequence _seq;

    private void Awake()
    {
        _sprite = GetComponent<UISprite>();
        _transform = transform;

        _pos = _transform.localPosition;
    }

    private void OnEnable()
    {
        Move();
    }

    void Move()
    {
        _transform.localPosition = _pos;

        _seq = DOTween.Sequence()

            .AppendCallback(() => {

                TweenAlpha.Begin(gameObject, 0.1f, 1);
            })

            .AppendInterval(0.1f + 0.1f)
            .AppendCallback(() => {

                _transform.DOLocalMove(Target.transform.localPosition - (Target.localPosition - _transform.localPosition)*0.2f, 0.75f);
            })

            .AppendInterval(0.75f)
            .AppendCallback(() => {

                TweenAlpha.Begin(gameObject, 0.1f, 0);
            })

            .AppendInterval(0.1f + 0.1f)
            .AppendCallback(() => {

                _transform.localPosition = _pos;
            })

            .AppendInterval(0.5f)
            .AppendCallback(() => {

                Move();
            })
        ;
    }

    private void OnDisable()
    {
        _seq.Kill();
    }
}
