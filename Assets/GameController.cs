﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : SingletonMonoBehaviour<GameController>
{
    public GameObject ExplotionFx;

    public GameObject[] Stage;

    public UILabel StageLbl;
    
    public bool IsDebug;

    public bool GameStart { set; get; } = false;
    public bool GameEnd { set; get; } = false;

    public int TargetCount { set; get; } = 0;

    public GameObject HintIndicator { set; get; }

    public int ThrowCount { set; get; } = 0;

    private void OnEnable()
    {
        if (!IsDebug)
        {
            Stage[UserData.CurrentStage].SetActive(true);

            StageLbl.text = string.Format("LEVEL {0}", UserData.LevelCount + 1);
        }
    }

    public void ShowHint()
    {
        HintIndicator.SetActive(true);
    }


    public void Explose(Transform target, bool ActiveBlastZone)
    {
        GameObject go = Instantiate(ExplotionFx);

        if(!ActiveBlastZone)
        {
            var fx = go.GetComponent<ExplostionFx>();

            fx.BlastZone.SetActive(false);
        }

        go.transform.position = target.position;

        go.SetActive(true);

        CustomVibration.Do(EVibrationType.Success);
    }

    public void ForceGameOver()
    {
        ExplotionFx.SetActive(true);
    }
}
