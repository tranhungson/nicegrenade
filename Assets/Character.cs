﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    public LayerMask Layer;

    public System.Action OnDead = delegate { };

    protected Animator _animator;

    protected bool _isDead = false;

    Rigidbody[] _bodyList;
    Collider[] _colliderList;

    private void Awake()
    {
        _animator = GetComponent<Animator>();

        _colliderList = GetComponentsInChildren<Collider>();

        foreach (Collider collider in _colliderList)
        {
            collider.isTrigger = true;
        }

        _bodyList = GetComponentsInChildren<Rigidbody>();

        foreach (Rigidbody body in _bodyList)
        {            
            body.isKinematic = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (_isDead)
            return;

        if(other.CompareTag("Explostion") || other.CompareTag("HurtObject"))
        {
            
            if(!Physics.Linecast(other.transform.position, transform.position + Vector3.up, Layer))
            {

                _isDead = true;

                _animator.enabled = false;

                foreach (Collider collider in _colliderList)
                {
                    collider.isTrigger = false;
                }

                foreach (Rigidbody body in _bodyList)
                {
                    body.isKinematic = false;

                    body.AddExplosionForce(1500, other.transform.position, 3);
                }

                OnDead.Invoke();

                SEManager.Instance.PlaySE("enemy");
            }
        }
    }
}
