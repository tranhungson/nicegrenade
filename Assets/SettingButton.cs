﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingButton : ButtonAssistant
{
    public bool IsClose = false;

    protected override void OnClick()
    {
        base.OnClick();

        if(IsClose)
        {
            UIController.Instance.Setting(false);
        }
        else
        {
            UIController.Instance.Setting(true);
        }
    }

}
