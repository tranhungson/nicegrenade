﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController : SingletonMonoBehaviour<UIController>
{

    public GameObject ClearUI, OverUI, HelpUI, SettingUI, TitleUI, GameUI, MainUI;

    public UILabel ClearLabel;

    public GameObject HintSuggest;
    public TweenScale HintScale;

    private void OnEnable()
    {
        if(UserData.ShowUI)
        {
            TitleUI.SetActive(true);
        }
        else
        {
            GameUI.SetActive(true);

            GameController.Instance.GameStart = true;
        }

        UserData.ShowUI = false;

        UIController.Instance.SuggestHint(false);
    }

    public void GameClear()
    {
        UIController.Instance.SuggestHint(false);

        UserData.ShowUI = true;

        UserData.ShowHint = 0;

        ClearLabel.text = string.Format("[333333]LEVEL[-] [0872F2]{0}[-]", UserData.LevelCount.ToString());

        MainUI.SetActive(false);

        ClearUI.SetActive(true);
    }

    public void GameOver()
    {
        UserData.ShowUI = true;

        UserData.ShowHint = 0;

        MainUI.SetActive(false);

        OverUI.SetActive(true);
    }

    public void HideHelp()
    {
        HelpUI.SetActive(false);
    }

    public void Setting(bool enable)
    {
        SettingUI.SetActive(enable);
    }

    public void StartGame()
    {
        TitleUI.SetActive(false);
        GameUI.SetActive(true);

        GameController.Instance.GameStart = true;
    }

    public void SuggestHint(bool enable)
    {
        HintScale.enabled = enable;
        HintScale.transform.localScale = Vector3.one;

        HintSuggest.SetActive(enable);
    }
}
