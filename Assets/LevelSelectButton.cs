﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSelectButton : ButtonAssistant
{
    public int Index { set; get; }
    public int Display { set; get; }

    private void OnEnable()
    {
        GetComponentInChildren<UILabel>().text = Display.ToString();
    }

    protected override void OnClick()
    {
        base.OnClick();

        UserData.CurrentStage = Index;
        UserData.LevelCount = Index;

        SceneNavigator.Instance.LoadScene("Game");
    }
}
