﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HintButton : ButtonAssistant
{
    protected override void OnClick()
    {
        if (GameController.Instance.GameEnd)
            return;

        base.OnClick();

        if (UserData.ShowHint == 0)
        {
            UIController.Instance.SuggestHint(false);

            GameController.Instance.ShowHint();

            UserData.ShowHint = 1;
        }
    }
}
