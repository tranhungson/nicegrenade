﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetButton : ButtonAssistant
{
    protected override void OnClick()
    {
        base.OnClick();

        SceneNavigator.Instance.LoadScene(SceneNavigator.Instance.CurrentSceneName);
    }
}
