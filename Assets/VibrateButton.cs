﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VibrateButton : ButtonAssistant
{
    bool _isOn = false;

    private void OnEnable()
    {
        _isOn = UserData.IsVibrate;

        UpdateInfo();
    }

    protected override void OnClick()
    {
        base.OnClick();

        _isOn = !_isOn;

        UserData.IsVibrate = _isOn;

        UpdateInfo();
    }

    void UpdateInfo()
    {
        if(_isOn)
        {
            _sprite.spriteName = "btn_on";
        }
        else
        {
            _sprite.spriteName = "btn_off";
        }

        UpdateButtonSprite(_sprite.spriteName);
    }
}
