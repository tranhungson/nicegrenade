﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocalizeLabel : MonoBehaviour
{

    [SerializeField]
    private string JapanText;

    [SerializeField]
    private string EnglishText;

    // Use this for initialization
    void Awake()
    {
        if (Application.systemLanguage == SystemLanguage.Japanese)
        {

            JapanText = JapanText.Replace("\\n", "\n");
            GetComponent<UILabel>().text = JapanText;

        }
        else
        {

            EnglishText = EnglishText.Replace("\\n", "\n");
            GetComponent<UILabel>().text = EnglishText;
        }
    }
}
