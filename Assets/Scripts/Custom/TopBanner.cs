﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TopBanner : MonoBehaviour
{
    private UISprite _sprite;

    public GameObject Root;

    private void OnEnable()
    {
        _sprite = GetComponent<UISprite>();

        float screenRate = (float)Screen.height / (float)Screen.width;

        if (screenRate >= 2.0f)
        {
            var screenHeight = Screen.height;

            _sprite.SetAnchor(Root, 0, 1574, 0, 50);
        }
    }
}
