﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocalizeObject : MonoBehaviour
{
    public GameObject[] Popup;

    // Use this for initialization
    void Start()
    {
        if (Application.systemLanguage == SystemLanguage.Japanese)
        {
            Popup[0].SetActive(true);
            Popup[1].SetActive(false);
        }
        else
        {
            Popup[0].SetActive(false);
            Popup[1].SetActive(true);
        }

    }
}
