﻿//  ResultTweetButtonAssistant.cs
//  ProductName BreakoutTower
//
//  Created by kan kikuchi on 2015.10.22.

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// リザルトシーンでのツイートボタンのアシスタントクラス
/// </summary>
public class TweetButtonAssistant : ButtonAssistant
{
    public string Message;

    //=================================================================================
    //内部
    //=================================================================================

    //クリックした時に実行されるメソッド
    protected override void OnClick()
    {
        base.OnClick();
        ShareManager.Instance.Tweet(Message);
    }

}