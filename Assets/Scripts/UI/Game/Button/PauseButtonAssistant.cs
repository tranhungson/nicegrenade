﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseButtonAssistant : ButtonAssistant
{
    public GameObject _canvas;

    private void OnApplicationPause(bool pauseStatus)
    {
        if (pauseStatus && !_canvas.activeSelf)
        {
            ChangeState(isPause: true);
        }
    }

    //クリックした時に実行されるメソッド
    protected override void OnClick()
    {
        base.OnClick();
        ChangeState(isPause: true);
    }

    private void ChangeState(bool isPause)
    {
        Time.timeScale = isPause ? 0 : 1;

        _canvas.SetActive(isPause);

        if (isPause)
        {
            BGMManager.Instance.Pause();
        }
        else
        {
            BGMManager.Instance.Resume();
        }
    }
}
