﻿//  RankingButtonAssistant.cs
//  ProductName BreakoutTower
//
//  Created by kan kikuchi on 2015.10.22.

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// ランキングボタンのアシスタントクラス
/// </summary>
public class RankingButtonAssistant : ButtonAssistant {

	//=================================================================================
	//内部
	//=================================================================================

	//クリックした時に実行されるメソッド
	protected override void OnClick(){
		base.OnClick ();

		RankingManager.Instance.ShowLeaderBoard ();
	}

}