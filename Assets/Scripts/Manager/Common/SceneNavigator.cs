﻿//  EnumUtility.cs
//  ProductName Template
//
//  Created by kan.kikuchi

using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using System.Collections;

public class SceneNavigator : SingletonMonoBehaviour<SceneNavigator>
{
    //一つ前と次のシーン名
    private string _afterSceneName = "", _beforeSceneName = "";

    public string BeforeSceneName
    {
        get { return _beforeSceneName; }
    }

    //現在のシーン名とシーン番号
    public string CurrentSceneName
    {
        get { return SceneManager.GetActiveScene().name; }
    }

    public int CurrentSceneNo
    {
        get { return SceneManager.GetActiveScene().buildIndex; }
    }

    //シーン読み込み完了時のイベント
    public event Action<int> OnLoadedScene = delegate { };

    //=================================================================================
    //初期化
    //=================================================================================

    protected override void Init()
    {
        base.Init();

        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    //=================================================================================
    //シーン加算
    //=================================================================================

    /// <summary>
    /// シーンを加算する
    /// </summary>
    public void AddScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName, LoadSceneMode.Additive);
    }

    public void LoadScene(string sceneName)
    {
        _afterSceneName = sceneName;
        _beforeSceneName = CurrentSceneName;

        SceneManager.LoadScene(sceneName);
    }

    //=================================================================================
    //シーン移動
    //=================================================================================

    //シーン移動後
    private void OnSceneLoaded(Scene scene, LoadSceneMode sceneMode)
    {
        OnLoadedScene(scene.buildIndex);
    }
}