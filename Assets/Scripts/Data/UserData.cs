﻿//  UserData.cs
//  ProductName Template
//
//  Created by kan.kikuchi on 2016.04.28.

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// 永続的に保存するデータを管理するクラス
/// </summary>
public static class UserData
{    
    public static int[] STAGE_LIST = {0, 1, 21, 3, 8, 25, 10, 7, 14, 6, 17, 36, 22, 2, 16, 5, 28, 4, 37, 13, 20, 23 };

    public static bool ShowUI = true;

    //=================================================================================
    //外部からの保存用
    //=================================================================================

    /// <summary>
    /// 諸々のデータを保存
    /// </summary>
    public static void Save()
    {
        PlayerPrefsUtility.Save(PLAY_COUNT_KEY, PlayCount);
                
        //全てのデータを同期
        PlayerPrefs.Save();
        Debug.Log("Save PlayerPrefs");
    }

    //=================================================================================
    //プレイカウント
    //=================================================================================
    private const string PLAY_COUNT_KEY = "PLAY_COUNT_KEY";

    private static bool _isInitializedPlayCount = false;
    private static int _playCount = 0;

    /// <summary>
    /// ゲームをプレイした回数
    /// </summary>
    public static int PlayCount
    {
        get
        {
            if (!_isInitializedPlayCount)
            {
                _playCount = PlayerPrefsUtility.Load(PLAY_COUNT_KEY, _playCount);
                _isInitializedPlayCount = true;
            }
            return _playCount;
        }
        set { _playCount = value; }
    }

    //=================================================================================
    //ミュートかどうか
    //=================================================================================
    private const string IS_MUTE_KEY = "IS_MUTE_KEY";
    
    //ミュートの状態が変わった時のイベント
    public static event Action<bool> OnChangeMuteSetting = delegate { };

    /// <summary>
    /// ミュートに設定されてるか否か
    /// </summary>
    public static bool IsMute
    {
        get
        {
            return PlayerPrefs.GetInt(IS_MUTE_KEY, 0) == 1;
        }
        set
        {
            if (value)
                PlayerPrefs.SetInt(IS_MUTE_KEY, 1);
            else
                PlayerPrefs.SetInt(IS_MUTE_KEY, 0);

            PlayerPrefs.Save();

            OnChangeMuteSetting(value);
        }
    }
    
    private const string VIBRATE = "VIBRATE_NICE_GRENADE";

    public static bool IsVibrate
    {
        get
        {
            if(!SystemInfo.deviceModel.Contains("iPad"))
                return PlayerPrefs.GetInt(VIBRATE, 1) == 1;
            else
                return PlayerPrefs.GetInt(VIBRATE, 0) == 1;
        }
        set
        {
            if (value)
                PlayerPrefs.SetInt(VIBRATE, 1);
            else
                PlayerPrefs.SetInt(VIBRATE, 0);

            PlayerPrefs.Save();
        }
    }

    private const string DELAY = "DELAY";

    public static float Delay
    {
        get
        {
            return PlayerPrefs.GetFloat(DELAY, 8);
        }
        set
        {
            PlayerPrefs.SetFloat(DELAY, value);
            PlayerPrefs.Save();
        }
    }

    private const string CURRENT_STAGE = "CURRENT_STAGE";

    public static int CurrentStage
    {
        get
        {
            return PlayerPrefs.GetInt(CURRENT_STAGE, 0);
        }
        set
        {
            PlayerPrefs.SetInt(CURRENT_STAGE, value);

            if (CurrentStage < 0)
                CurrentStage = UserData.MaxStage - 1;
            else if (CurrentStage >= UserData.MaxStage)
                CurrentStage = 0;

            PlayerPrefs.Save();
        }
    }

    public static int MaxStage
    {
        get
        {
            return STAGE_LIST.Length;
        }
    }

    private const string HINT = "HINT";

    public static int RemainHint
    {
        get
        {
            return PlayerPrefs.GetInt(HINT, 3);
        }
        set
        {
            PlayerPrefs.SetInt(HINT, value);

            PlayerPrefs.Save();
        }
    }

    private const string SHOW_HINT = "SHOW_HINT";

    public static int ShowHint
    {
        get
        {
            return PlayerPrefs.GetInt(SHOW_HINT, 0);
        }
        set
        {
            PlayerPrefs.SetInt(SHOW_HINT, value);

            PlayerPrefs.Save();
        }
    }

    private const string LEVEL_COUNT = "LEVEL_COUNT";

    public static int LevelCount
    {
        get
        {
            return PlayerPrefs.GetInt(LEVEL_COUNT, 0);
        }
        set
        {
            PlayerPrefs.SetInt(LEVEL_COUNT, value);

            PlayerPrefs.Save();
        }
    }
}
