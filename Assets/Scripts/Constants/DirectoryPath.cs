﻿//  DirectoryPath.cs
//  ProductName Template
//
//  Created by kan.kikuchi

using UnityEngine;
using System.Collections;

/// <summary>
/// ディレクトリへのパスを定義する定数クラス
/// </summary>
public static class DirectoryPath{

	//リソースフォルダのパス
	public const string TOP_RESOURCES             = "Assets/Resources/";
	public const string TOP_RESOURCES_MAYBE_BUILD = "Assets/Resources_MaybeBuild/";

	//シーンディレクトリへのパス
	public const string SCENES = "Assets/Scenes";

	//テンプレートがあるディレクトリへのパス
	public const string TEMPLATE_SCRIPT_DIRECTORY_PATH = "Assets/Editor/Template/Templates";

	//定数データのパス
	public const string CONSTANTS = "Assets/Scripts/Constants/";
	public const string AUTO_CREATING_CONSTANTS = CONSTANTS + "AutoCreating/";

}