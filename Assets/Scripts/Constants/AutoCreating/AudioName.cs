﻿/// <summary>
/// オーディオ名を定数で管理するクラス
/// </summary>
public static class AudioName{

	  public const string BGM_IN_GAME = "BGM_InGame";
	  public const string BGM_RESULT  = "BGM_Result";
	  public const string BGM_TITLE   = "BGM_Title";
	  public const string SE_BUTTON   = "SE_Button";
	  public const string SE_CLEAR    = "SE_Clear";
	  public const string SE_MISS     = "SE_Miss";

}
