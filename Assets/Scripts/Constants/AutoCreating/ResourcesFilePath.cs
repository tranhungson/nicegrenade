﻿/// <summary>
/// Resources以下のファイルパスを定数で管理するクラス
/// </summary>
public static class ResourcesFilePath{

	  public const string ASSET_CACHE_DATA                                     = "Data/CacheData";
	  public const string ASSET_DEFINE_SYMBOL_SETTING                          = "DefineSymbolSetting";
	  public const string ASSET_UNITY_PROJECT_SETTING                          = "Data/UnityProjectSetting";
	  public const string ASSET_XCODE_PROJECT_SETTING                          = "Data/XcodeProjectSetting";
	  public const string AUDIO_BGM_IN_GAME                                    = "Audio/BGM/BGM_InGame";
	  public const string AUDIO_BGM_RESULT                                     = "Audio/BGM/BGM_Result";
	  public const string AUDIO_BGM_TITLE                                      = "Audio/BGM/BGM_Title";
	  public const string AUDIO_SE_BUTTON                                      = "Audio/SE/SE_Button";
	  public const string AUDIO_SE_CLEAR                                       = "Audio/SE/SE_Clear";
	  public const string AUDIO_SE_MISS                                        = "Audio/SE/SE_Miss";
	  public const string PREFAB_LEVEL_SELECT_BUTTON                           = "Prefab/UI/LevelSelectButton";
	  public const string PREFAB_SAMPLE_CUBE                                   = "Prefab/RecyclingObject/SampleCube";
	  public const string SHADER_UNLIT_PREMULTIPLIED_COLORED                   = "Shaders/Unlit - Premultiplied Colored";
	  public const string SHADER_UNLIT_PREMULTIPLIED_COLORED_TEXTURE_CLIP      = "Shaders/Unlit - Premultiplied Colored (TextureClip)";
	  public const string SHADER_UNLIT_PREMULTIPLIED_COLORED1                  = "Shaders/Unlit - Premultiplied Colored 1";
	  public const string SHADER_UNLIT_PREMULTIPLIED_COLORED2                  = "Shaders/Unlit - Premultiplied Colored 2";
	  public const string SHADER_UNLIT_PREMULTIPLIED_COLORED3                  = "Shaders/Unlit - Premultiplied Colored 3";
	  public const string SHADER_UNLIT_TEXT                                    = "Shaders/Unlit - Text";
	  public const string SHADER_UNLIT_TEXT_TEXTURE_CLIP                       = "Shaders/Unlit - Text (TextureClip)";
	  public const string SHADER_UNLIT_TEXT1                                   = "Shaders/Unlit - Text 1";
	  public const string SHADER_UNLIT_TEXT2                                   = "Shaders/Unlit - Text 2";
	  public const string SHADER_UNLIT_TEXT3                                   = "Shaders/Unlit - Text 3";
	  public const string SHADER_UNLIT_TRANSPARENT_COLORED                     = "Shaders/Unlit - Transparent Colored";
	  public const string SHADER_UNLIT_TRANSPARENT_COLORED_PACKED_TEXTURE_CLIP = "Shaders/Unlit - Transparent Colored (Packed) (TextureClip)";
	  public const string SHADER_UNLIT_TRANSPARENT_COLORED_TEXTURE_CLIP        = "Shaders/Unlit - Transparent Colored (TextureClip)";
	  public const string SHADER_UNLIT_TRANSPARENT_COLORED1                    = "Shaders/Unlit - Transparent Colored 1";
	  public const string SHADER_UNLIT_TRANSPARENT_COLORED2                    = "Shaders/Unlit - Transparent Colored 2";
	  public const string SHADER_UNLIT_TRANSPARENT_COLORED3                    = "Shaders/Unlit - Transparent Colored 3";
	  public const string SHADER_UNLIT_TRANSPARENT_PACKED                      = "Shaders/Unlit - Transparent Packed";
	  public const string SHADER_UNLIT_TRANSPARENT_PACKED1                     = "Shaders/Unlit - Transparent Packed 1";
	  public const string SHADER_UNLIT_TRANSPARENT_PACKED2                     = "Shaders/Unlit - Transparent Packed 2";
	  public const string SHADER_UNLIT_TRANSPARENT_PACKED3                     = "Shaders/Unlit - Transparent Packed 3";

}
