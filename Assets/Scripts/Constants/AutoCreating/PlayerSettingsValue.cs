﻿/// <summary>
/// PlayerSettingsの設定を定数で管理するクラス
/// </summary>
public static class PlayerSettingsValue{

	  public const string BUNDLE_IDENTIFIER = "com.Goodia.NiceGrenade";
	  public const string BUNDLE_VERSION    = "1.0.0";
	  public const string PRODUCT_NAME      = "Grenade Master";

}
