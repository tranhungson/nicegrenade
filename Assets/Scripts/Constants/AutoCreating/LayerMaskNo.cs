﻿/// <summary>
/// レイヤーマスク番号を定数で管理するクラス
/// </summary>
public static class LayerMaskNo{

	  public const int BACK              = 256;
	  public const int CHARACTER         = 1024;
	  public const int DEFAULT           = 1;
	  public const int EFFECT            = 512;
	  public const int FRAGMENT          = 16384;
	  public const int IGNORE_RAYCAST    = 4;
	  public const int IGNORE_CHARACTER  = 8192;
	  public const int IGNORE_EXPLOSTION = 4096;
	  public const int OBSTACLE          = 2048;
	  public const int TRANSPARENT_FX    = 2;
	  public const int UI                = 32;
	  public const int WATER             = 16;

}
