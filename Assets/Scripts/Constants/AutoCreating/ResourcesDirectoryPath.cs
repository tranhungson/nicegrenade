﻿/// <summary>
/// Resources以下のディレクトリパスを定数で管理するクラス
/// </summary>
public static class ResourcesDirectoryPath{

	  public const string AUDIO                   = "Audio";
	  public const string AUDIO_BGM               = "Audio/BGM";
	  public const string AUDIO_SE                = "Audio/SE";
	  public const string DATA                    = "Data";
	  public const string PREFAB                  = "Prefab";
	  public const string PREFAB_RECYCLING_OBJECT = "Prefab/RecyclingObject";
	  public const string PREFAB_UI               = "Prefab/UI";
	  public const string SHADERS                 = "Shaders";

}
