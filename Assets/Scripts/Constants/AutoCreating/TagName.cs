﻿/// <summary>
/// タグ名を定数で管理するクラス
/// </summary>
public static class TagName{

	  public const string EDITOR_ONLY     = "EditorOnly";
	  public const string EXPLODER        = "Exploder";
	  public const string EXPLOSTION      = "Explostion";
	  public const string FINISH          = "Finish";
	  public const string GAME_CONTROLLER = "GameController";
	  public const string GRENADE         = "Grenade";
	  public const string GROUND          = "Ground";
	  public const string HURT_OBJECT     = "HurtObject";
	  public const string MAIN_CAMERA     = "MainCamera";
	  public const string PLAYER          = "Player";
	  public const string RESPAWN         = "Respawn";
	  public const string UICAMERA        = "UICamera";
	  public const string UNTAGGED        = "Untagged";
	  public const string WALL            = "Wall";
	  public const string WATER           = "Water";

}
