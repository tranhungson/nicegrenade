﻿//  AdManager.cs
//  ProductName 激ムズ!タワー崩し
//
//  Created by kan kikuchi on 2015.10.27.

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

/// <summary>
/// 広告関連を管理するクラス
/// </summary>
public class AdManager : SingletonMonoBehaviour<AdManager>
{
#if !NO_AD || !NO_NAITIVE
    //ネイティブ連携用プラグイン
    private AdPlugin _adPlugin = null;
#endif

    //=================================================================================
    //初期化
    //=================================================================================
    //広告を表示しないデバッグ
#if !NO_AD || !NO_NAITIVE

    public static event Action<bool> RewardVideoComplete = delegate { };
    public static event Action InterAdClose = delegate { };
    public static event Action RewardVideoLoaded = delegate { };
    protected override void Init()
    {
        base.Init();

        //ネイティブ連携用プラグイン初期化
        _adPlugin = gameObject.AddComponent<AdPlugin>();
        _adPlugin.Init();

    }

    private void Start()
    {
        if (!_adPlugin)
        {
            return;
        }

        //Android or Unity上では即ネイティブの初期化が終わった事にする(審査かどうか待つ必要がないため)
#if UNITY_ANDROID || UNITY_EDITOR
        OnInitNative("false");
#endif

    }

#endif
    //=================================================================================
    //内部
    //=================================================================================
#if !NO_AD || !NO_NAITIVE

    //ネイティブ側の初期化が終わった時のメソッド
    private void OnInitNative(string isRevewing)
    {
        Debug.Log("OnInitNative ");
    }

    public void ShowSplashAd()
    {
        _adPlugin.ShowSplashAd(UserData.PlayCount);
    }

    public void ShowRewardVideo()
    {
#if UNITY_EDITOR || CHEAT
        FinishedPlayingMovieAd("true");
#elif !NO_AD || !NO_NAITIVE
        _adPlugin.ShowRewardVideo();
#endif
    }

    public bool CheckCanShowVideo()
    {
#if UNITY_EDITOR || CHEAT
        return true;
#elif !NO_AD || !NO_NAITIVE
        return _adPlugin.CanShowRewardVideo();
#endif
    }

    public void HideAllAds()
    {
#if !NO_AD || !NO_NAITIVE
        _adPlugin.HideAllAds();
#endif
    }

    public void PauseSound()
    {
        BGMManager.Instance.Pause();
    }

    public void ResumeSound()
    {
        BGMManager.Instance.Resume();
    }

    public void FinishedPlayingMovieAd(string isSuccess)
    {
        RewardVideoComplete.Invoke(isSuccess.Equals("true"));
    }

    public void FinishInterstial()
    {
        InterAdClose.Invoke();
    }

    public void FinishLoadRewardVideo()
    {
        RewardVideoLoaded.Invoke();
    }
    //cheat callback for editor
    public void CheatReward()
    {
        RewardVideoComplete.Invoke(true);
    }
    public void CheatInter()
    {
        InterAdClose.Invoke();
    }
    // ////////////////////////
#endif
}