﻿//  AdPlugin.cs
//  ProductName 激ムズ!タワー崩し
//
//  Created by kan kikuchi on 2015.10.27.

//モバイルの時だけMOBILE_DEVICEを宣言する
#if UNITY_EDITOR || UNITY_STANDALONE || UNITY_WEBGL || UNITY_WEBPLAYER
#undef MOBILE_DEVICE
#else
#define MOBILE_DEVICE
#endif

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

/// <summary>
/// 広告関連のネイティブプラグイン
/// </summary>
public class AdPlugin : NativePlugin
{

    //=================================================================================
    //初期化
    //=================================================================================

    /// <summary>
    /// 初期化
    /// </summary>
    public void Init()
    {
#if UNITY_IPHONE && MOBILE_DEVICE
		base.Init (_AdPlugin_Init, "");
#elif UNITY_ANDROID && MOBILE_DEVICE
		base.Init (null, "");
#endif
    }

#if UNITY_IPHONE
    [DllImport("__Internal")]
    private static extern IntPtr _AdPlugin_Init(string gameObjectName);
#endif

    public void HideAllAds()
    {
#if UNITY_IPHONE && MOBILE_DEVICE
		if(_nativeInstance == IntPtr.Zero){
			return;
		}
		else{
			_HideAllAds(_nativeInstance);
		}
#elif UNITY_ANDROID && MOBILE_DEVICE
		CommonNativeManager.Instance.CommonAndroidPlugin.Call("hideAllAds");
#endif

    }

#if UNITY_IPHONE
    [DllImport("__Internal")]
    private static extern void _HideAllAds(IntPtr instance);
#endif

    /// <summary>
    /// スプラッシュ広告を表示
    /// </summary>
    public void ShowSplashAd(int playCount)
    {
#if UNITY_IPHONE && MOBILE_DEVICE
		if(_nativeInstance == IntPtr.Zero){
			return;
		}
		else{
			_ShowSplashAd(_nativeInstance, playCount);
		}
#elif UNITY_ANDROID && MOBILE_DEVICE
		CommonNativeManager.Instance.CommonAndroidPlugin.Call("showSplashAd", playCount);
#endif

    }

#if UNITY_IPHONE
    [DllImport("__Internal")]
    private static extern void _ShowSplashAd(IntPtr instance, int playCount);
#endif

    public void ShowRewardVideo()
    {
#if UNITY_IPHONE && MOBILE_DEVICE
		if(_nativeInstance == IntPtr.Zero){
			return;
		}
		else{
			_ShowRewardVideo(_nativeInstance);
		}
#elif UNITY_ANDROID && MOBILE_DEVICE
		CommonNativeManager.Instance.CommonAndroidPlugin.Call("showRewardVideo");
#endif

    }

#if UNITY_IPHONE
    [DllImport("__Internal")]
    private static extern void _ShowRewardVideo(IntPtr instance);
#endif

    public bool CanShowRewardVideo()
    {
#if UNITY_IPHONE && MOBILE_DEVICE
		if(_nativeInstance == IntPtr.Zero){
			return false;
		}
		else{
			return _CanShowRewardVideo(_nativeInstance);
		}
#elif UNITY_ANDROID && MOBILE_DEVICE
		CommonNativeManager.Instance.CommonAndroidPlugin.Call("canShowRewardVideo");
#endif

        return false;
    }

#if UNITY_IPHONE
    [DllImport("__Internal")]
    private static extern bool _CanShowRewardVideo(IntPtr instance);
#endif    
}