﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoulletRewardVideoButton : RewardButtonAssistant
{
    [SerializeField] UILabel _labelTestAd;
    protected override void OnVideoSuccess(bool isSuccess)
    {
        if (isSuccess)
        {
            _labelTestAd.text = "Test Ad Success";
        }
        else
        {
            _labelTestAd.text = "Test Ad Failed";
        }
    }

    protected override void OnClick()
    {
        ShowAdVideo();
    }
}
