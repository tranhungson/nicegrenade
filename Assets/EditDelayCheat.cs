﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EditDelayCheat : MonoBehaviour
{
    UILabel _label;

    private void OnEnable()
    {
        _label = GetComponent<UILabel>();

        _label.text = UserData.Delay.ToString();
    }

    public void ChangeDelayTime()
    {
        UserData.Delay = float.Parse(_label.text);
    }
}
