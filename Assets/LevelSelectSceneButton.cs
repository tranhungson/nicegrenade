﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSelectSceneButton : ButtonAssistant
{
    protected override void OnClick()
    {
        base.OnClick();

        SceneNavigator.Instance.LoadScene("Title");
    }
}
