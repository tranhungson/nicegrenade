﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hint : MonoBehaviour
{
    private void Awake()
    {
        GameController.Instance.HintIndicator = gameObject;
                
        if (UserData.ShowHint == 0)
        {
            gameObject.SetActive(false);
        }        
    }
}
