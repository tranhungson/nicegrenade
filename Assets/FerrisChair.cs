﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FerrisChair : MonoBehaviour
{
    public Transform Target;

    public float Speed = 20;

    public bool LockRotation = true;

    void Update()
    {
        // Spin the object around the target at 20 degrees/second.
        transform.RotateAround(Target.position, Vector3.forward, Speed * Time.deltaTime);

        if(LockRotation)
            transform.eulerAngles = Vector3.zero;
    }
}
