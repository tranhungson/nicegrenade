﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class HelpFinger : MonoBehaviour
{
    private Transform _transform;
    private UISprite _sprite;
    private TweenAlpha _alpha;

    private Vector3 _pos;

    // Start is called before the first frame update
    void Start()
    {
        _sprite = GetComponent<UISprite>();

        _transform = transform;

        _pos = _transform.position;

        _alpha = GetComponent<TweenAlpha>();

        Animation();
    }

    // Update is called once per frame
    void Animation()
    {
        DOTween.Sequence()

            .AppendCallback(() => {

                _alpha.PlayForward();
            })

            .AppendInterval(_alpha.duration + 0.1f)

            .AppendCallback(() => {

                _sprite.spriteName = "icon_finger_2";

                _transform.DOLocalMoveY(-500, 1);
            })

            .AppendInterval(1f)
            .AppendCallback(() => {
                _sprite.spriteName = "icon_finger_1";                
            })

            .AppendInterval(0.1f)
            .AppendCallback(() => {
                _alpha.PlayReverse();
            })

            .AppendInterval(_alpha.duration + 0.1f)
            .AppendCallback(() => {

                _transform.position = _pos;                
            })

            .AppendInterval(1)
            .AppendCallback(() => {

                Animation();
            })
            ;
    }

    private void OnDisable()
    {
        DOTween.Kill(this);
    }
}
