﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSelectController : MonoBehaviour
{
    public GameObject Button;

    UIGrid _grid;

    // Start is called before the first frame update
    void Start()
    {
        _grid = GetComponent<UIGrid>();

        for (int i = 0; i < UserData.STAGE_LIST.Length; i++)
        {
            var go = Instantiate(Button, _grid.transform);

            var btn = go.GetComponent<LevelSelectButton>();

            btn.Index = i;
            btn.Display = UserData.STAGE_LIST[i];

            go.SetActive(true);
        }

        _grid.enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
