﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchController : MonoBehaviour
{
    public GameObject BallPrefab;
    
    public LayerMask Layer;

    public GameObject AimSign;

    public int Segment = 10;

    //public float MaxRadius = 10f;

    private RaycastHit _hit;
    private Ray _ray;

    private Rigidbody _body;
            
    private LineRenderer _renderer;

    private Vector3 _startPos, _endPos, _newPos;
    private float _distance = 0;

    //private void OnDrawGizmos()
    //{
    //    Gizmos.color = Color.blue;
    //    Gizmos.DrawSphere(_newPos, 0.25f);
    //}

    private void OnEnable()
    {
        _renderer = GetComponent<LineRenderer>();

        _renderer.positionCount = Segment;
    }

    void Update()
    {
        if (UICamera.isOverUI)
        {
            return;
        }

        if (Input.GetMouseButtonDown(0))
        {
            if(Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out _hit))
            {
                _startPos = _hit.point;

                _ray = new Ray(Camera.main.transform.position, ((BallPrefab.transform.position + Vector3.forward) - Camera.main.transform.position).normalized);

                if (Physics.SphereCast(_ray, 0.25f, out _hit, 100, Layer))
                {
                    AimSign.transform.position = _hit.point;
                    AimSign.transform.rotation = Quaternion.LookRotation(_hit.normal);

                    AimSign.gameObject.SetActive(true);
                }

                Visualize(VelocityByTime(BallPrefab.transform.position, AimSign.transform.position, 1));

                _renderer.enabled = true;
            }            
        }
        else if(Input.GetMouseButton(0))
        {
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out _hit))
            {
                _endPos = _hit.point;

                _distance = Vector3.Distance(_endPos, _startPos);

                if(_distance >= 0.1f)
                {
                    _newPos = AimSign.transform.position + (_endPos - _startPos).normalized * _distance;

                    _ray = new Ray(Camera.main.transform.position, _newPos - Camera.main.transform.position);

                    if(Physics.Raycast(_ray, out _hit, 1000, Layer))
                    {
                        AimSign.transform.position = _hit.point;
                        AimSign.transform.rotation = Quaternion.LookRotation(_hit.normal);
                    }

                    _startPos = _endPos;

                    Visualize(VelocityByTime(BallPrefab.transform.position, AimSign.transform.position, 1));
                }
            }            
        }
        else if (Input.GetMouseButtonUp(0))
        {
            AimSign.gameObject.SetActive(false);

            Fire();

            //_renderer.enabled = false;
        }
    }

    Vector3 VelocityByTime(Vector3 start, Vector3 end, float time)
    {
        Vector3 distance = end - start;
        Vector3 distanceXZ = distance;
        distanceXZ.y = 0;

        float sy = distance.y;
        float sxz = distanceXZ.magnitude;

        float vxz = sxz / time;
        float vy = sy / time + 0.5f * Mathf.Abs(Physics.gravity.y) * time;

        Vector3 result = distanceXZ.normalized;
        result *= vxz;
        result.y = vy*2;

        return result;
    }

    public void Fire()
    {
        Vector3 velocity = VelocityByTime(BallPrefab.transform.position, AimSign.transform.position, 1);
        
        GameObject go = Instantiate(BallPrefab, BallPrefab.transform.parent);

        go.SetActive(true);

        _body = go.GetComponent<Rigidbody>();

        _body.isKinematic = false;

        _body.velocity = velocity;
    }

    void Visualize(Vector3 vo)
    {
        for (int i = 0; i < Segment; i++)
        {
            Vector3 p = PositionByTime(vo, i / (float) (Segment - 1));

            _renderer.SetPosition(i, p);
        }
    }

    Vector3 PositionByTime(Vector3 vo, float time)
    {
        //Vector3 vxz = vo;
        //vxz.y = 0;

        Vector3 result = BallPrefab.transform.position + vo * time;

        float sy = (-0.5f * Mathf.Abs(Physics.gravity.y) * (time * time)) + (vo.y*time) + BallPrefab.transform.position.y;

        result.y = sy;

        return result;
    }
}
