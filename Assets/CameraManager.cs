﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    private void Awake()
    {
        float screenRate = (float)Screen.height / (float)Screen.width;

        if (screenRate >= 2.0f)
        {
            Camera.main.fieldOfView = 66;
        }
    }
}
