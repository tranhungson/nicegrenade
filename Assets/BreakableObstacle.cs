﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Exploder.Utils;

public class BreakableObstacle : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Explostion"))
        {
            ExploderSingleton.Instance.ExplodeObject(gameObject);
        }
    }
}
