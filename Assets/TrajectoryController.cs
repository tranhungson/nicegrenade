﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TrajectoryController : SingletonMonoBehaviour<TrajectoryController>
{
    public GameObject BallPrefab;
    public LayerMask Layer;
    public GameObject AimSign;

    public int Segment = 50;

    public float Radius = 20;

    LineRenderer _line;

    Rigidbody _body;

    private Vector3 _startPos, _endPos;

    private RaycastHit _hit;
    
    private Vector3 _velocity;

    private List<Vector3> _posList;

    private Vector3 _normal;

    bool _touchDown = false;
    
    public Player MainPlayer { set; get; }

    void Start()
    {
        _body = GetComponent<Rigidbody>();

        _line = GetComponent<LineRenderer>();
    }

    void Update()
    {
        if (!GameController.Instance.GameStart || UICamera.isOverUI || !DelayProgressBar.Instance.CanThrow || GameController.Instance.GameEnd)
        {
            return;
        }

        if (Input.GetMouseButtonDown(0) && !_touchDown)
        {
            _touchDown = true;

            _startPos = Input.mousePosition;

            DelayProgressBar.Instance.StartCountDown();

            MainPlayer.Aim();

            CustomVibration.Do(EVibrationType.VibrateLight_Pop);
        }
        else if (Input.GetMouseButton(0) && _touchDown)
        {
            _endPos = Input.mousePosition;

            _velocity = (_startPos - _endPos);

            _velocity *= 0.05f;
            
            _velocity.y = Mathf.Clamp(_velocity.y, 2, _velocity.y);

            _velocity.z = _velocity.y;

            _velocity.y += 2;

            _velocity = Vector3.ClampMagnitude(_velocity, Radius);

            _posList = new List<Vector3>();

            for (int i = 0; i < Segment; i++)
            {
                Vector3 p = CalculatePosition(i * 0.025f, _velocity);

                if (!Physics.Linecast(Camera.main.transform.position, p, out _hit, Layer))
                {
                    _posList.Add(p);
                }
                else
                {
                    _normal = _hit.normal;

                    break;
                }
            }

            if(!_line.enabled)
                _line.enabled = true;

            _line.positionCount = _posList.Count;

            for (int i = 0; i < _posList.Count; i++)
            {
                _line.SetPosition(i, _posList[i]);
            }

            if(!AimSign.activeSelf)
                AimSign.SetActive(true);

            AimSign.transform.position = _posList[_posList.Count - 1];
            AimSign.transform.rotation = Quaternion.LookRotation(_normal);

            Quaternion _rot = Quaternion.LookRotation(AimSign.transform.position - MainPlayer.transform.position);
            _rot.x = _rot.z = 0;

            MainPlayer.transform.rotation = _rot;

        }
        else if (Input.GetMouseButtonUp(0) && _touchDown)
        {
            DelayProgressBar.Instance.CanThrow = false;
            
            _line.enabled = false;
            _line.positionCount = 0;

            AimSign.gameObject.SetActive(false);
            
            MainPlayer.Throw();

            DelayProgressBar.Instance.ShouldExplode = false;

            DOTween.Sequence().AppendInterval(0.25f).AppendCallback(() => {

                GameObject go = Instantiate(BallPrefab, BallPrefab.transform.parent);

                DelayProgressBar.Instance.CurrentGrenade = go.GetComponent<Grenade>();

                go.SetActive(true);

                _body = go.GetComponent<Rigidbody>();

                _body.isKinematic = false;

                _body.AddForce(_velocity, ForceMode.VelocityChange);
            });

            _touchDown = false;

            GameController.Instance.ThrowCount++;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawSphere(_startPos, 0.25f);

        Gizmos.color = Color.yellow;
        Gizmos.DrawSphere(_endPos, 0.25f);
    }

    private Vector3 CalculatePosition(float elapsedTime, Vector3 velocity)
    {
        return Physics.gravity * elapsedTime * elapsedTime * 0.5f +
                   velocity * elapsedTime + BallPrefab.transform.position;
    }

    public void HideAll()
    {
        AimSign.SetActive(false);

        gameObject.SetActive(false);
    }

    private void OnDestroy()
    {
        DOTween.KillAll();
    }
}
