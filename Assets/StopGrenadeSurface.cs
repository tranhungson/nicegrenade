﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class StopGrenadeSurface : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag("Grenade"))
        {
            var body = collision.gameObject.GetComponent<Rigidbody>();
            body.velocity *= 0.1f;
            body.angularVelocity *= 0.1f;            
        }
    }
}
