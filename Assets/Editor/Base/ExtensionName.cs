﻿using UnityEngine;
using System.Collections;

/// <summary>
/// 拡張子を定数で管理する定数クラス
/// </summary>
public static class ExtensionName {

	public const string META      = ".meta";
	public const string ARCHIVE   = ".a";
	public const string FRAMEWORK =".framework";
	public const string BUNDLE    =".bundle";

	//アセットの拡張子
	public const string ASSET = ".asset";

	//テンプレートの拡張子
	public const string TEMPLATE_SCRIPT = ".txt";

	//スクリプトの拡張子
	public const string SCRIPT = ".cs";

	//エクセルの拡張子
	public const string EXCEL = ".xls";

	//プレハブの拡張子
	public const string PREFAB = ".prefab";

	//シーンの拡張子
	public const string SCENE = ".unity";

}
