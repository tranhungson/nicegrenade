﻿//  TemplateTag.cs
//  ProductName BreakBall
//
//  Created by kan kikuchi on 2015.08.31.


/// <summary>
/// テンプレートで置換するタグ
/// </summary>
public static class TemplateTag {

	public const string PRODUCT_NAME_TAG = "#PRODUCTNAME#";
	public const string AUTHOR_TAG       = "#AUTHOR#";
	public const string DATA_TAG         = "#DATA#";
	public const string SUMMARY_TAG      = "#SUMMARY#";
	public const string SCRIPT_NAME_TAG  = "#SCRIPTNAME#";

}
