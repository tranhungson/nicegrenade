﻿using UnityEditor;

/// <summary>
/// ButtonAssistantのテンプレートから新しくスクリプトを作るクラス
/// </summary>
public class ButtonAssistantScriptCreator : CustomScriptCreator {

	private const string TEMPLATE_SCRIPT_NAME  = "ButtonAssistant";

	[MenuItem(MENU_PATH + TEMPLATE_SCRIPT_NAME)]
	private static void CreateScript(){
		ShowWindow (TEMPLATE_SCRIPT_NAME);
	}

}
