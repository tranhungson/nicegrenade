﻿using UnityEditor;

/// <summary>
/// MonoBehaviourのテンプレートから新しくスクリプトを作るクラス
/// </summary>
public class MonoBehaviourScriptCreator : CustomScriptCreator {

	private const string TEMPLATE_SCRIPT_NAME  = "MonoBehaviour";

	[MenuItem(MENU_PATH + TEMPLATE_SCRIPT_NAME)]
	private static void CreateScript(){
		ShowWindow (TEMPLATE_SCRIPT_NAME);
	}

}
