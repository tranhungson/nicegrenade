﻿//  ScriptableObjectEditor.cs
//  ProductName Template
//
//  Created by kan.kikuchi on 2016.04.27.

using UnityEditor;
using UnityEngine;

/// <summary>
/// ScriptableObjectのインスペクター表示を変更するクラス
/// </summary>
[CustomEditor( typeof( ScriptableObject ), true )]
public sealed class ScriptableObjectEditor : Editor{
	
	public override void OnInspectorGUI(){
		DrawDefaultInspector();
	}
}