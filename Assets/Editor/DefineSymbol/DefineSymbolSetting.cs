﻿//  DefineSymbolSetting.cs
//  ProductName Template
//
//  Created by kan.kikuchi on 2016.05.11.

using UnityEditor;
using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;

//[CreateAssetMenu()]
/// <summary>
/// DefineSymbolの設定値
/// </summary>
public class DefineSymbolSetting : ScriptableObject {

	//設定ファイルの実体
	private static DefineSymbolSetting _entity, _original;
	public  static DefineSymbolSetting  Entity{
		get{
			if(_entity == null){
				_original = Instantiate(Resources.Load<DefineSymbolSetting> (ResourcesFilePath.ASSET_DEFINE_SYMBOL_SETTING));
				_entity   = Instantiate(_original);
			}
			return _entity;
		}
	}

	//各シンボルの情報
	public List<DefineSymbol> DefineSymbolList = new List<DefineSymbol>();

	[System.SerializableAttribute]
	public struct DefineSymbol{
		public string Key, Value, Info;
		public bool IsEnabled;

		//コンストラクタ
		public DefineSymbol(string key, string value, string info, bool isEnabled){
			Key       = key;
			Value     = value;
			Info      = info;
			IsEnabled = isEnabled;

			//Keyは半角英数字+アンダースコア以外を除き、大文字で設定
			Regex regex = new Regex(@"[^_a-zA-Z0-9]");
			Key = regex.Replace(Key, "").ToUpper();
		}
	}

	//シンボルを編集したか
	private bool _isEdited = false;
	public  bool  IsEdited{
		get{return _isEdited;}
	}

	//=================================================================================
	//ウィンドウ表示
	//=================================================================================

	//メニューからウィンドウを表示
	[MenuItem("Tools/Open/SymbolEditorWindow")]
	public static void Open (){
		SymbolEditorWindow.GetWindow (typeof(SymbolEditorWindow));
	}

	//=================================================================================
	//シンボル修正
	//=================================================================================

	/// <summary>
	/// シンボルを修正
	/// </summary>
	public void EditSymbol(int no, string key, string value, string info, bool isEnabled){
		_isEdited = true;

		//新たなシンボル作成
		DefineSymbol newSymbol = new DefineSymbol (key, value, info, isEnabled);
		if(no >= DefineSymbolList.Count){
			DefineSymbolList.Add (newSymbol);
		}
		else{
			DefineSymbolList [no] = newSymbol;
		}

	}

	/// <summary>
	/// 全てのシンボルを有効or無効にする
	/// </summary>
	public void SetAllEnabled(bool isEnabled){
		_isEdited = true;

		for (int i = 0; i < DefineSymbolList.Count; i++) {
			DefineSymbol symbol = DefineSymbolList [i];
			symbol.IsEnabled = isEnabled;

			DefineSymbolList [i] = symbol;
		}
	}

	/// <summary>
	/// シンボルを削除する
	/// </summary>
	public void Delete(int symbolNo){
		if(symbolNo >= DefineSymbolList.Count){
			return;
		}
		_isEdited = true;
		DefineSymbolList.RemoveAt (symbolNo);
	}

	//=================================================================================
	//判定
	//=================================================================================

	//重複したシンボルのkeyがあるか
	public bool IsDuplicateSymbolKey{
		get{return DefineSymbolList.Select (symbol => symbol.Key).Distinct().Count() != DefineSymbolList.Count;}
	}

	//=================================================================================
	//シンボルのリセット
	//=================================================================================

	/// <summary>
	/// シンボルをリセットする
	/// </summary>
	public void Reset(){
		_entity = Instantiate(_original);
	}

	//=================================================================================
	//シンボルの保存
	//=================================================================================

	/// <summary>
	/// 全プラットフォームに同じシンボルを保存
	/// </summary>
	public void SaveAll(){
		//各プラットフォーム毎に設定
		foreach (BuildTargetGroup buildTarget in Enum.GetValues(typeof(BuildTargetGroup))) {
			Save (buildTarget);
		}

		//Symbolに対応した値を定数クラスに書き出す
		Dictionary<string, string> _defineValueDic = new Dictionary<string, string>();

		foreach (DefineSymbol symbol in DefineSymbolList) {
			if(!string.IsNullOrEmpty(symbol.Value)){
				_defineValueDic[symbol.Key] = symbol.Value;
			}
		}
		ConstantsClassCreator.Create<string>("DefineValue", "Symbolに対応した値を定数で管理するクラス", _defineValueDic);

		//空白及び重複しているシンボル名を除く
		DefineSymbolList = DefineSymbolList.Where(defineSymbol => !string.IsNullOrEmpty(defineSymbol.Key)).Distinct().ToList();

		//有効になっているシンボルを上に表示するように
		DefineSymbolList = DefineSymbolList.OrderBy (symbol => symbol.IsEnabled ? 0 : 1).ToList();

		//設定ファイルを更新する
		string path = "Assets/Editor/DefineSymbol/Resources/" + ResourcesFilePath.ASSET_DEFINE_SYMBOL_SETTING + ExtensionName.ASSET;

		AssetDatabase.DeleteAsset(path);
		AssetDatabase.CreateAsset(_entity, path);
		AssetDatabase.SaveAssets();

		//データを読み直す
		_original = Instantiate(_entity);
		_entity   = Instantiate(_original);
	}

	//指定したプラットフォームにシンボルを保存
	private void Save(BuildTargetGroup targetGroup){
		_isEdited = false;

		//各シンボルの対応した設定を保存
		string setSymbols = "";

		foreach (DefineSymbol symbol in DefineSymbolList) {
			//有効になっているシンボルは設定するようにsetSymbolsにkeyをまとめる
			if(symbol.IsEnabled){

				if(!string.IsNullOrEmpty(setSymbols)){
					setSymbols += ";";
				}
				setSymbols += symbol.Key;
			}
		}

		//シンボルを設定
		if(targetGroup != BuildTargetGroup.Unknown && targetGroup != BuildTargetGroup.tvOS){
			PlayerSettings.SetScriptingDefineSymbolsForGroup (targetGroup, setSymbols);
		}

	}

}