﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using PathCreation;
using MyBox;

public enum EnemyState
{
    Idle,
};

public class Enemy : Character
{
    public EnemyState State;

    public bool DetachOnDead = false;

    #region Path follow
    [Separator("Patrol")]
    public bool FollowPath = false;

    [ConditionalField(nameof(FollowPath), false, true)]
    public PathCreator Path;
    
    [ConditionalField(nameof(FollowPath), false, true)]
    public float Speed = 5;

    [ConditionalField(nameof(FollowPath), false, true)]
    public float RestDuration = 5;

    float _distance;

    bool _moving = true;

    int _direction = 1;

    #endregion

    private void OnEnable()
    {
        GameController.Instance.TargetCount++;

        if (FollowPath)
        {
            Walk();
        }
        OnDead += GameOver;
    }

    private void OnDisable()
    {
        OnDead -= GameOver;
    }

    void Update()
    {
        if (FollowPath && Path != null && !_isDead && _moving)
        {
            _distance += Speed * Time.deltaTime * _direction;

            transform.position = Path.path.GetPointAtDistance(_distance, EndOfPathInstruction.Stop);
            transform.rotation = Quaternion.LookRotation(Path.path.GetDirectionAtDistance(_distance, EndOfPathInstruction.Stop) * _direction);

            if(_distance >= Path.path.length || _distance <= 0)
            {
                if (_distance >= Path.path.length)
                    _distance = Path.path.length;
                else
                    _distance = 0;

                _moving = false;

                Idle();

                DOTween.Sequence().AppendInterval(RestDuration).AppendCallback(() => {

                    _moving = true;

                    _direction *= -1;

                    Walk();                    
                });
            }
        }

    }
    void GameOver()
    {
        if(DetachOnDead)
        {
            transform.parent = transform.parent.parent;
        }

        GameController.Instance.TargetCount--;

        if(GameController.Instance.TargetCount <= 0)
        {

            TrajectoryController.Instance.HideAll();

            DOTween.Sequence().AppendInterval(1).AppendCallback(() => {

                if (GameController.Instance.GameEnd)
                    return;

                UserData.CurrentStage++;
                UserData.LevelCount++;

                GameController.Instance.GameEnd = true;

                UIController.Instance.GameClear();

                TrajectoryController.Instance.MainPlayer.Victory();
            });
        }        
    }

    public void Idle()
    {
        _animator.SetTrigger("Idle");
    }

    public void Walk()
    {
        _animator.SetTrigger("Walk");
    }

    public void ThrowLeft()
    {
        _animator.SetTrigger("ThrowLeft");
    }
}
