﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Stage_6 : MonoBehaviour
{
    public ParticleSystem SmokeFx;

    public Enemy Enemy;

    public Rigidbody Bomb;

    Renderer[] _renderer;

    private void OnEnable()
    {
        Enemy.OnDead += OnDead;

                     
    }

    private void Start()
    {
        Enemy.ThrowLeft();

        _renderer = Enemy.GetComponentsInChildren<Renderer>();

        DOTween.Sequence().AppendInterval(0.5f).AppendCallback(() => {

            Bomb.isKinematic = false;

            Bomb.AddForce(new Vector3(0, -10, 0), ForceMode.VelocityChange);

        }).AppendInterval(0.05f).AppendCallback(() => {

            Bomb.gameObject.SetActive(false);

            SmokeFx.gameObject.SetActive(true);

        }).AppendInterval(0.5f).AppendCallback(() => {
            
            foreach (var item in _renderer)
            {
                item.enabled = false;
            }
        });
    }

    private void OnDisable()
    {
        Enemy.OnDead -= OnDead;
    }

    void OnDead()
    {
        ParticleSystem.EmissionModule module = SmokeFx.emission;
        
        module.enabled = false;

        foreach (var item in _renderer)
        {
            item.enabled = true;
        }
    }
}
