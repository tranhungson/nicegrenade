﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheatButton : ButtonAssistant
{
    public GameObject CheatUI;

    bool _show = false;

    private void OnEnable()
    {
        _show = PlayerPrefs.GetInt("SHOW_CHEAT", 1) == 1;

        CheatUI.SetActive(_show);
    }

    protected override void OnClick()
    {
        base.OnClick();

        _show = !_show;

        CheatUI.SetActive(_show);

        if(_show)
            PlayerPrefs.SetInt("SHOW_CHEAT", 1);
        else
            PlayerPrefs.SetInt("SHOW_CHEAT", 0);

        PlayerPrefs.Save();
    }
}
