﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocalRigibodyContrains : MonoBehaviour
{
    public bool LockX, LockY, LockZ;

    Rigidbody _body;

    Vector3 _localVelocity;

    private void OnEnable()
    {
        _body = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        _localVelocity = transform.InverseTransformDirection(_body.velocity);

        if(LockY)
            _localVelocity.y = 0;

        if (LockX)
            _localVelocity.x = 0;

        if (LockZ)
            _localVelocity.z = 0;

        _body.velocity = transform.TransformDirection(_localVelocity);
    }
}
