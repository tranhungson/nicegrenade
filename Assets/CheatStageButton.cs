﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheatStageButton : ButtonAssistant
{
    public int Value;

    protected override void OnClick()
    {
        base.OnClick();

        UserData.CurrentStage += Value;
        UserData.LevelCount = UserData.CurrentStage;

        UserData.ShowHint = 0;

        SceneNavigator.Instance.LoadScene(SceneNavigator.Instance.CurrentSceneName);
    }
}
