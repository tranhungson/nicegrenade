﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayButton : ButtonAssistant
{
    protected override void OnClick()
    {
        base.OnClick();

        UIController.Instance.StartGame();
    }
}
