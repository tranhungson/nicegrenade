﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Player : Character
{
    public GameObject FakeGrenade;

    private void OnEnable()
    {
        OnDead += GameOver;
    }

    private void OnDisable()
    {
        OnDead -= GameOver;
    }

    private void Start()
    {
        TrajectoryController.Instance.MainPlayer = this;
    }

    void GameOver()
    {
        if (GameController.Instance.GameEnd)
            return;

        GameController.Instance.GameEnd = true;

        TrajectoryController.Instance.HideAll();

        DelayProgressBar.Instance.gameObject.SetActive(false);

        DOTween.Sequence().AppendInterval(1).AppendCallback(() => {

            UIController.Instance.GameOver();
        });
    }

    public void Aim()
    {
        FakeGrenade.SetActive(true);

        _animator.SetTrigger("Aim");
    }

    public void Throw()
    {
        _animator.SetTrigger("Throw");

        DOTween.Sequence().AppendInterval(0.2f).AppendCallback(() => {

            FakeGrenade.SetActive(false);
        });
    }

    public void Victory()
    {
        transform.eulerAngles = new Vector3(0, 180, 0);

        _animator.SetTrigger("Victory");
    }
}
