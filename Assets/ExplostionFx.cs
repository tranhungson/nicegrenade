﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplostionFx : MonoBehaviour
{
    public GameObject BlastZone;

    private void OnEnable()
    {
        SEManager.Instance.PlaySE("bomb");

        Invoke("SelfDestroy", 0.25f);
    }

    void SelfDestroy()
    {
        BlastZone.SetActive(false);

        if(GameController.Instance.ThrowCount >= 3 && !GameController.Instance.GameEnd && UserData.ShowHint == 0)
        {
            UIController.Instance.SuggestHint(true);
        }
    }
}
