//
//  AdManager.m
//  Unity-iPhone
//
//  Created  on 2015/10/27.
//
//  広告関係の処理を行うクラス

#import "FlurryHelper.h"
#import "AdManager.h"
#import "AdControllerInitializer.h"

#import "CommonConfig.h"
#import "UtilDeviceInfo.h"
#include "TenjinSDK.h"
#import "UnityAppController.h"
//#import "MoPub.h"
@implementation AdManager

//審査中フラグ
static BOOL _isRevewing = YES;

//=================================================================================
//初期化
//=================================================================================

//初期化
- (id)initWithGameObjectName:(const char *)gameobjectName{
    if (self = [super init]) {
        //do stuff
    }
 
    //各フラグ、広告のコントローラーを初期化
    _isConfirmedGAE      = NO;
    _isPreparedHouseAd   = NO;

    _bannerAdController = nil;

    _baseInterstitialAdController = nil;

//    _pauseRectangleAdController   = nil;
    _endRectangleAdController     = nil;
    
    _movieAdController = nil;

    //Flurry開始
    [FlurryHelper startFlurry];
	

	//デバイスの言語を取得
	[UtilDeviceInfo confirmDeviceLanguage];
	
    [self CanShowAds];
    return self;
}

-(void)CanShowAds
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        bool loaded = [[NSUserDefaults standardUserDefaults] boolForKey:@"finish_load_applovin"];
        if (loaded) {
//            [self confirmedGAE];
        }
        else{
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(CanShowAds) name:@"CanShowAds" object:nil];
        }
    });
}
  

//=================================================================================
//コールバック
//=================================================================================

//GAEの確認が終了した時のコールバック
- (void)confirmedGAE{
    _isConfirmedGAE = YES;
    [self startAdIfNeeded];
}

//自社広告の準備が終了した時のコールバック
- (void)preparedHouseAd:(BOOL)isRevewing{

    _isPreparedHouseAd = YES;

    //審査中かどうか
    _isRevewing = isRevewing;

#ifdef DEBUG_IN_REVIEW
    _isRevewing = true;
#elif DEBUG_NOT_IN_REVIEW
    _isRevewing = false;
#endif

    NSLog(@"preparedHouseAd:(isRevewing %@)", isRevewing ? @"true" : @"false");

    //必要に応じて広告開始
    [self startAdIfNeeded];
}

//=================================================================================
//内部
//=================================================================================

//必要に応じて広告開始
- (void)startAdIfNeeded{
    if(!_isConfirmedGAE){
        return;
    }
    
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    _isRemoveAds = false;//[defaults valueForKey:@"remove_ads"];
    if(!_isRemoveAds)
    {
        _bannerAdController = [AdControllerInitializer initBannerAdController];
        _baseInterstitialAdController = [AdControllerInitializer initRankInterstitialAdController];
        
        [_bannerAdController updateBannerAd:true];
    }
    
    _movieAdController = [AdControllerInitializer initMovieAdController];

    //Unity側に初期化が終了した事を通知、審査中かも伝える
    NSLog(@"SendInitializedMessageToUnityIfNeeded(isRevewing %@)", _isRevewing ? @"true" : @"false");
    UnitySendMessage(UNITY_OBJECT_NAME, UNITY_ON_INIT_METHOD_NAME, _isRevewing ?  "true" :  "false");
}

//=================================================================================
//外部
//=================================================================================

//審査中かどうかのフラグを取得
+(BOOL)getIsRevewing{
    return false;
}


//シーンに合わせて広告の更新
-(void)updateAdByScene:(int)scene
{
    if(_isRemoveAds)
        return;
    //バナー広告
    //[_bannerAdController updateBannerAd:scene != SCENE_RESULT];

    //レクタングル
    //[_endRectangleAdController updateRectangleAd:scene == SCENE_RESULT];

}

- (bool)canShowVideo{
    bool result = false;
    result = [_movieAdController isCanShow];
//    if ([_movieAdController isCanShow])
//    {
//        result = true;
//    }
    
    return result;
}


-(void)showVideoAd
{
   [_movieAdController show];
}

-(void)showIndicator:(BOOL) enable
{
//    if(enable)
//    {
//        [GetAppController() showIndicator];
//        
//    } else {
//        
//        [GetAppController() hideIndicator];
//        
//    }
}

-(void) hideAllAds
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:true forKey:@"remove_ads"];
    [defaults synchronize];
    
    _isRemoveAds = true;
    [_bannerAdController updateBannerAd:false];
}

//スプラッシュ広告を表示
-(void)showInterstitialAd:(int)scene playCount:(int)playCount{
//
//    if(_isRemoveAds)
//        return;
//
    [_baseInterstitialAdController showInterstitialAd:playCount];
}

-(void)reportSessionPlayCount:(int) playCount
{
    switch (playCount) {
        case 1:
        {
            [TenjinSDK sendEventWithName:@"Ending_1"];
            break;
        }
        case 2:
        {
            [TenjinSDK sendEventWithName:@"Ending_2"];
            break;
        }
        case 3:
        {
            [TenjinSDK sendEventWithName:@"Ending_3"];
            break;
        }
        case 4:
        {
            [TenjinSDK sendEventWithName:@"Ending_4"];
            break;
        }
        case 5:
        {
            [TenjinSDK sendEventWithName:@"Ending_5"];
            break;
        }
        case 10:
        {
            [TenjinSDK sendEventWithName:@"Ending_10"];
            break;
        }
        case 20:
        {
            [TenjinSDK sendEventWithName:@"Ending_20"];
            break;
        }
        default:
            break;
    }
}

-(void)reportRetentionCount:(int) retentionCount
{
    switch (retentionCount) {
        case 1:
        {
            [TenjinSDK sendEventWithName:@"Retention_1d"];
            break;
        }
        case 2:
        {
            [TenjinSDK sendEventWithName:@"Retention_2d"];
            break;
        }
        case 3:
        {
            [TenjinSDK sendEventWithName:@"Retention_3d"];
            break;
        }
        case 4:
        {
            [TenjinSDK sendEventWithName:@"Retention_4d"];
            break;
        }
        case 5:
        {
            [TenjinSDK sendEventWithName:@"Retention_5d"];
            break;
        }
        case 6:
        {
            [TenjinSDK sendEventWithName:@"Retention_6d"];
            break;
        }
        case 7:
        {
            [TenjinSDK sendEventWithName:@"Retention_7d"];
            break;
        }
        case 14:
        {
            [TenjinSDK sendEventWithName:@"Retention_14d"];
            break;
        }
        case 30:
        {
            [TenjinSDK sendEventWithName:@"Retention_30d"];
            break;
        }
        default:
            break;
    }
}

//リーダーボードを表示した時に広告を重ねる
-(void)addAdToLeaderBoard
{
    //[_rankSplashAdController showSplashAd:0];
}

//デバイスの言語が日本語か
- (BOOL)isLangJapanese{
	return [UtilDeviceInfo isJapaneseLanguage];
}

@end
