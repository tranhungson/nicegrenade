//
//  AdControllerInitializer.m
//  Unity-iPhone
//
//  Created  on 2015/10/29.
//
//  各広告のコントローラーを初期化するクラス

#import "AdControllerInitializer.h"
#import "AdManager.h"
#import "ApplovinBannerHelper.h"
#import "ApplovinInterstitialAdController.h"
#import "ApplovinMovieAdController.h"
#import "UtilDeviceInfo.h"

@implementation AdControllerInitializer


//=================================================================================
//バナー広告コントローラー初期化
//=================================================================================

+(BannerAdController*)initBannerAdController{
    BannerAdController *bannerAdController = nil;
    bannerAdController = [[ApplovinBannerHelper alloc] init];
    return bannerAdController;
}

//=================================================================================
//インステ広告コントローラー初期化
//=================================================================================

+(InterstitialAdController*)initInterstitialAdController:(NSString*)splashType{
    InterstitialAdController* interstitialAdController = nil;
    
   interstitialAdController = [[ApplovinInterstitialAdController alloc] init];
    
    return interstitialAdController;
}

//エンディングで表示するインステ用コントローラー初期化
+(InterstitialAdController*)initBaseInterstitialAdController{
    InterstitialAdController* baseInterstitialAdController = [self initInterstitialAdController:nil];
    [baseInterstitialAdController setRankType:NO];
    return baseInterstitialAdController;
}

//タイトルとランキングボタンを押した時に表示するインステ用コントローラー初期化
+(InterstitialAdController*)initRankInterstitialAdController{
    InterstitialAdController* rankInterstitialAdController = [self initInterstitialAdController:nil];
    [rankInterstitialAdController setRankType:YES];
    return rankInterstitialAdController;
}

//=================================================================================
//レクタングル広告コントローラー初期化
//=================================================================================

//エンディングで表示するレクタングル用コントローラー初期化
+(RectangleAdController*)initEndRectangleAdController:(int)screenIndex
{
    RectangleAdController* rectangleAdController = nil;
   
    
    return rectangleAdController;
}

//=================================================================================
//動画広告コントローラー初期化
//=================================================================================

+(MovieAdController*)initMovieAdController{
    MovieAdController *movieAdController = nil;
    
    movieAdController = [[ApplovinMovieAdController alloc] init];
    
    return movieAdController;
}


@end
