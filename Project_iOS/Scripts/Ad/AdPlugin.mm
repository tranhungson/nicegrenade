//
//  AdPlugin.mm
//  Unity-iPhone
//
//  Created  on 2015/10/27.
//
//  Unity側とのラ広告関連で連携を行うプラグイン

#import "AdManager.h"

//=================================================================================
//Unity側との連携
//=================================================================================


extern "C" {
    
    //初期化、ネイティブ側のプラグインのインスタンスを作成し、Unity側へ渡す
    void *_AdPlugin_Init(const char *gameObjectName){
        id instance = [[AdManager alloc]initWithGameObjectName:gameObjectName];
        
        CFRetain((CFTypeRef)instance);
        return (__bridge void *)instance;
    }
    
    //シーンに合わせて広告の更新
    void _UpdateAdByScene(void *instance, int scene){
        AdManager *adManager = (__bridge AdManager *)instance;
        [adManager updateAdByScene:scene];
    }


    void _FIRRemoteConfigThrottledEndTimeInSecondsKey(void *instance){
 
    }

    void _ReportAnalytic(void *instance)
    {

    }

    void _ReportTenjinAnalytic(void *instance)
    {

    }

    void _ShowRewardVideo(void *instance){

    }

    bool _CanShowRewardVideo (void *instance){
     
    }

    void _HidePopupRect(void *instance)
    {

    }

    void _ShowPopupRect(void *instance)
    {

    }

    void _CheckCanShowPopupRect(void *instance, int playCount)
    {

    }

    void _ShowGameRect(void *instance)
    {

    }

    void _HideGameRect(void *instance)
    {

    }
    
    //スプラッシュ広告を表示
    void _ShowSplashAd(void *instance, int scene, int playCount){
        AdManager *adManager = (__bridge AdManager *)instance;
        [adManager showInterstitialAd:scene playCount:playCount];
    }
    
    //リーダーボードを表示した時に広告を重ねる
    void _AddAdToLeaderBoard(void *instance){
        AdManager *adManager = (__bridge AdManager *)instance;
        [adManager addAdToLeaderBoard];
    }
    
    bool _IsCanShowVideo(void *instance)
    {
        AdManager *adManager = (__bridge AdManager *)instance;
        return [adManager canShowVideo];
    }
    
    
    //動画広告を表示
    void _ShowVideoAd(void *instance){
        AdManager *adManager = (__bridge AdManager *)instance;
        [adManager showVideoAd];
    }
    
    //IAP
    void _HideIndicator(void *instance)
    {
        AdManager *adManager = (__bridge AdManager *)instance;
        [adManager showIndicator:false];
    }
    
    void _ShowIndicator(void *instance)
    {
        AdManager *adManager = (__bridge AdManager *)instance;
        [adManager showIndicator:true];
    }
    
    void _HideAllAds(void *instance)
    {
        AdManager *adManager = (__bridge AdManager *)instance;
        [adManager hideAllAds];
    }
    
    void _ReportSessionPlayCount(void *instance, int playCount) {
        
        AdManager *adManager = (__bridge AdManager *)instance;
        [adManager reportSessionPlayCount:playCount];
    }
    
    void _ReportRetentionCount(void *instance, int retentionCount) {
        
        AdManager *adManager = (__bridge AdManager *)instance;
        [adManager reportRetentionCount:retentionCount];
    }

}
