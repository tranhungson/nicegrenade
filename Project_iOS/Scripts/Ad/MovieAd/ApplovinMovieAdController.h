//
//  AdstirMovieAdController.h
//  Unity-iPhone
//
//  Created  on 2015/06/30.
//
//  Adstirの動画広告のコントローラー


#import "MovieAdController.h"
#import <AppLovinSDK/AppLovinSDK.h>

@interface ApplovinMovieAdController : MovieAdController{
    
    //リワードを受け取ったか、ビューが閉じられたか
    BOOL _isReceivedReward;
}
-(void)show;

-(id)init;
-(void)show;
-(BOOL)canShow;
@end
