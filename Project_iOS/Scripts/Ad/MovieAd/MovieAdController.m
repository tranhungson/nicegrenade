//
//  MovieAdController.m
//  Unity-iPhone
//
//  Created  on 2015/06/30.
//
//  動画広告の親クラスこれを継承して各プロバイダ毎のコントローラーを作成

#import "MovieAdController.h"

@implementation MovieAdController

//=================================================================================
//共通処理
//=================================================================================

//初期化
- (id)init{
    
    self = [super init];
    
    return self;
}

//表示
-(void)show
{
    
}

-(BOOL)isCanShow
{
    return  false;
}


//=================================================================================
//Unity側にメッセージを送るメソッド
//=================================================================================

//動画の視聴が完了したことをUnity側に送信
+(void)sendFinishedPlayingMessage :(BOOL)isSuccess{
    UnitySendMessage(UNITY_OBJECT_NAME, "FinishedPlayingMovieAd", isSuccess ? "true" : "false");
}
+(void)startInterstial{
    UnitySendMessage(UNITY_OBJECT_NAME, "startInterstital",  "");
}
    
+(void)finishInterstial{
    UnitySendMessage(UNITY_OBJECT_NAME, "InterstitalCompleted",  "true");
}

@end
