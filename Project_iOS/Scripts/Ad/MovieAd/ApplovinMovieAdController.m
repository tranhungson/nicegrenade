//
//  AdstirMovieAdController.m
//  Unity-iPhone
//
//  Created  on 2015/06/30.
//
//  Adstirの動画広告のコントローラー

#import "ApplovinMovieAdController.h"

@interface ApplovinMovieAdController()<MARewardedAdDelegate>
    @property (nonatomic, strong) MARewardedAd *rewardedAd;
    @end

@implementation ApplovinMovieAdController


//=================================================================================
//共通処理
//=================================================================================

//初期化
- (id)init{
    self = [super init];
    [self loadVideoRandomly];
    
    return self;
}

- (void)loadVideoRandomly{
    _isReceivedReward = false;
    self.rewardedAd = [MARewardedAd sharedWithAdUnitIdentifier:APPLOVIN_VIDEO_ID];
    self.rewardedAd.delegate = self;
    
    // Load the first ad
    [self.rewardedAd loadAd];
}

//表示
-(void)show {
    
    if ( [self.rewardedAd isReady] )
    {
        [self.rewardedAd showAd];
    }
}

-(BOOL)canShow
{
   return [self.rewardedAd isReady];
}

#pragma mark - MAAdDelegate Protocol
    
- (void)didLoadAd:(MAAd *)ad{
    
    NSLog(@"Applovin Reward Video: didLoadAd");
}
    
- (void)didFailToLoadAdForAdUnitIdentifier:(NSString *)adUnitIdentifier withErrorCode:(NSInteger)errorCode{
    
    NSLog(@"Applovin Reward Video: didFailWithError %ld", (long)errorCode);
    _isReceivedReward = false;
    // Rewarded ad failed to load. We recommend re-trying in 3 seconds.
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self.rewardedAd loadAd];
    });
}
    
- (void)didDisplayAd:(MAAd *)ad {
    NSLog(@"Applovin Reward Video: didDisplayAd");
}
    
- (void)didClickAd:(MAAd *)ad {
    NSLog(@"Applovin Reward Video: didClickAd");
}
    
- (void)didHideAd:(MAAd *)ad{
    NSLog(@"Applovin Reward Video: didHideAd");
    [MovieAdController sendFinishedPlayingMessage:_isReceivedReward];
    [self.rewardedAd loadAd];
    
    if (_isReceivedReward) {
        
        // set time for show interstial
        NSDate *currentTime = [NSDate date];
        NSTimeInterval interstitialTimeRaw = [currentTime timeIntervalSince1970];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setDouble:interstitialTimeRaw forKey:@"InterstitialTimeKey"];
        [defaults synchronize];
        [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"finish_rewardvideo"];
    }
    
     _isReceivedReward = false;
}
    
- (void)didFailToDisplayAd:(MAAd *)ad withErrorCode:(NSInteger)errorCode{
    NSLog(@"Applovin Reward Video: didFailToDisplayAd %ld", (long)errorCode);
    // Rewarded ad failed to display. We recommend loading the next ad
    _isReceivedReward = false;
    [self.rewardedAd loadAd];
}
    
#pragma mark - MARewardedAdDelegate Protocol
    
- (void)didStartRewardedVideoForAd:(MAAd *)ad {}
    
- (void)didCompleteRewardedVideoForAd:(MAAd *)ad {
     _isReceivedReward = true;
}
    
- (void)didRewardUserForAd:(MAAd *)ad withReward:(MAReward *)reward{
    
    NSLog(@"Applovin Reward Video: didRewardUserForAd with amount %ld", (long)reward.amount);
    // Rewarded ad was displayed and user should receive the reward
}
@end
