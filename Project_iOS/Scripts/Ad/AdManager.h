//
//  AdManager.h
//  Unity-iPhone
//
//  Created  on 2015/10/27.
//
//  広告関係の処理を行うクラス

#import "CommonConfig.h"

#import "BannerAdController.h"
#import "InterstitialAdController.h"
#import "RectangleAdController.h"
#import "MovieAdController.h"

//Unity側のオブジェクト名
#define UNITY_OBJECT_NAME "NativeManager"

//Unity側のメソッド名
#define UNITY_ON_INIT_METHOD_NAME "OnInitNative"

@interface AdManager : UIViewController {
	
    //バナー広告管理クラス
    BannerAdController* _bannerAdController;

    //インステ広告管理クラス
    InterstitialAdController* _baseInterstitialAdController;


    //レクタングル広告管理クラス
//    RectangleAdController *_pauseRectangleAdController;
    RectangleAdController *_endRectangleAdController;

    //動画広告管理クラス
    MovieAdController *_movieAdController;
    
    //GAEの確認が済んだか
    BOOL _isConfirmedGAE;

    //自社広告の準備が済んだか
    BOOL _isPreparedHouseAd;
    
    BOOL _isRemoveAds;
    
}

//=================================================================================
//メソッド宣言
//=================================================================================

//初期化
- (id)initWithGameObjectName:(const char *)gameobjectName;

//審査中かどうかのフラグを取得
+(BOOL)getIsRevewing;


//シーンに合わせて広告の更新
-(void)updateAdByScene:(int)scene;

//スプラッシュ広告を表示
-(void)showInterstitialAd:(int)scene playCount:(int)playCount;

- (bool)canShowVideo;
//動画広告を表示
-(void)showVideoAd;

-(void)reportSessionPlayCount:(int) playCount;
-(void)reportRetentionCount:(int) playCount;

-(void) showIndicator:(BOOL) enable;

-(void) hideAllAds;

//リーダーボードを表示した時に広告を重ねる
-(void)addAdToLeaderBoard;

//デバイスの言語が日本語か
- (BOOL)isLangJapanese;


@end
