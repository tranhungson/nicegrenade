//
//  AdControllerInitializer.h
//  Unity-iPhone
//
//  Created  on 2015/10/29.
//
//  各広告のコントローラーを初期化するクラス

#import "BannerAdController.h"
#import "InterstitialAdController.h"
#import "RectangleAdController.h"
#import "MovieAdController.h"

@interface AdControllerInitializer : NSObject;

//バナー広告コントローラー初期化
+(BannerAdController*)initBannerAdController;

//インステ広告コントローラー初期化
+(InterstitialAdController*)initBaseInterstitialAdController;
+(InterstitialAdController*)initRankInterstitialAdController;

//レクタングル広告コントローラー初期化
+(RectangleAdController*)initEndRectangleAdController:(int)screenIndex;

//動画広告コントローラー初期化
+(MovieAdController*)initMovieAdController;
+(MovieAdController*)initMovieAdController2;

@end
