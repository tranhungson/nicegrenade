//
//  Unity-iPhone
//
//  Created  on 2015/10/29.
//
//  Nendのスプラッシュ(インターステイシャル)広告コントローラー

#import "ApplovinInterstitialAdController.h"
#import "MovieAdController.h"
#import <StoreKit/StoreKit.h>

@interface ApplovinInterstitialAdController()<MAAdDelegate>
    @property (nonatomic, strong) MAInterstitialAd *interstitialAd;
    @end

@implementation ApplovinInterstitialAdController
{
    BOOL canShow;
    BOOL canShowReviewPopup;
}
//=================================================================================
//外部
//=================================================================================

//初期化
- (id)init{
    self = [super init];
    canShowReviewPopup = false;
    [self initInterstitial];
    return self;
}

//表示
-(void)showInterstitialAd:(int)playCount{
//    if (![super canShow:playCount]) {
//        return;
//    }
    bool firstLoad = [[NSUserDefaults standardUserDefaults] boolForKey:@"first_load"];
    if (!firstLoad) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setBool:true forKey:@"first_load"];
        [defaults synchronize];
        canShow = true;
    }else {
        
        canShow = [self checkCanShow];
    }
    
    if ([self.interstitialAd isReady] && canShow && playCount >= 2)
    {
        [self.interstitialAd showAd];
        if (playCount == 10) {
            canShowReviewPopup = true;
        }else {
            canShowReviewPopup = false;
        }
    }else if(playCount == 10 && !canShowReviewPopup){
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        bool canShowReview = [defaults boolForKey:@"canShowReview"];
        if (!canShowReview) {
            
            [SKStoreReviewController requestReview];
            [defaults setBool:true forKey:@"canShowReview"];
            [defaults synchronize];
        }
        [MovieAdController finishInterstial];
    }
    else
    {        
        [MovieAdController finishInterstial];
    }
}

-(bool)checkCanShow
{
    bool result = false;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSTimeInterval interstitialTimeRaw = [defaults doubleForKey:@"InterstitialTimeKey"];
    NSDate *interstitialTime = [NSDate dateWithTimeIntervalSince1970:interstitialTimeRaw];
    NSDate *currentTime = [NSDate date];
    NSTimeInterval timeDiff = [currentTime timeIntervalSinceDate:interstitialTime];
    
    bool isRewardVideo = [[NSUserDefaults standardUserDefaults] boolForKey:@"finish_rewardvideo"];
    
    if(interstitialTimeRaw == 0)
    {
        result = true;
    }
    else
    {
        float distance = 40;
        if (isRewardVideo) {
            distance = 30;
            [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"finish_rewardvideo"];
        }
        if(timeDiff >= distance)
        {
            result = true;
        }
        else
        {
            result = false;
        }
    }
    
    return result;
}
//=================================================================================
//内部
//=================================================================================


#pragma mark - Nend Interstitial
- (void)initInterstitial{
    
    self.interstitialAd = [[MAInterstitialAd alloc] initWithAdUnitIdentifier:APPLOVIN_INTERSTITIAL_ID];
    self.interstitialAd.delegate = self;
    
    // Load the first ad
    [self.interstitialAd loadAd];
}

#pragma mark - MAAdDelegate Protocol
    
- (void)didLoadAd:(MAAd *)ad{
    NSLog(@"Applovin Interstitial: did load");
}
    
- (void)didFailToLoadAdForAdUnitIdentifier:(NSString *)adUnitIdentifier withErrorCode:(NSInteger)errorCode{
    NSLog(@"Applovin Interstitial: did failed with id : %@ - error: %ld ", adUnitIdentifier, (long)errorCode);
    // Interstitial ad failed to load. We recommend re-trying in 3 seconds.
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self.interstitialAd loadAd];
        });
}
    
- (void)didDisplayAd:(MAAd *)ad {
    NSLog(@"Applovin Interstitial: didDisplayAd");
    [MovieAdController startInterstial];
}
    
- (void)didClickAd:(MAAd *)ad {
    NSLog(@"Applovin Interstitial: didDisplayAd");
}
    
- (void)didHideAd:(MAAd *)ad{
    
     [self finishInterstitial];
    // Interstitial ad is hidden. Pre-load the next ad
    [self.interstitialAd loadAd];
}
    
- (void)didFailToDisplayAd:(MAAd *)ad withErrorCode:(NSInteger)errorCode{
    NSLog(@"Applovin Interstitial: didFailToDisplayAd with id : %@ - error: %ld ", ad.adUnitIdentifier, (long)errorCode);
    // Interstitial ad failed to display. We recommend loading the next ad
    [self.interstitialAd loadAd];
    [MovieAdController finishInterstial];
}

-(void)finishInterstitial
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    bool canShowReview = [defaults boolForKey:@"canShowReview"];
    if (canShowReviewPopup && !canShowReview) {
        
        [SKStoreReviewController requestReview];
        [defaults setBool:true forKey:@"canShowReview"];
        [defaults synchronize];
    }
    
    [MovieAdController finishInterstial];
    NSDate *currentTime = [NSDate date];
    NSTimeInterval interstitialTimeRaw = [currentTime timeIntervalSince1970];
    [defaults setDouble:interstitialTimeRaw forKey:@"InterstitialTimeKey"];
    [defaults synchronize];
}

@end
