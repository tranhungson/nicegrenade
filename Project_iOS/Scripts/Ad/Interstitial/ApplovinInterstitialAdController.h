//
//  Unity-iPhone
//
//  Created  on 2015/10/29.
//
//  Nendのスプラッシュ(インターステイシャル)広告コントローラー

#import "InterstitialAdController.h"
#import <AppLovinSDK/AppLovinSDK.h>

@interface ApplovinInterstitialAdController : InterstitialAdController{

}
-(id)init;
-(void)showInterstitialAd:(int)playCount;
-(void)finishInterstitial;
@end
