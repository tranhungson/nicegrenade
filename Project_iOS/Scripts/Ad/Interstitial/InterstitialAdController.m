//
//  Unity-iPhone
//
//  Created  on 2015/10/29.
//
//  スプラッシュ(インターステイシャル)広告の親クラスこれを継承して各プロバイダ毎のコントローラーを作成

#import "InterstitialAdController.h"

@implementation InterstitialAdController

//初期化
- (id)init{
    
    self = [super init];
    
    _firstTimes = [[NSUserDefaults standardUserDefaults] boolForKey:@"logged_in"];
    
    return self;
}

//表示
-(void)showInterstitialAd:(int)playCount{}

//表示が可能か
-(BOOL)canShow:(int)playCount{
    
    //ランキングのかぶせるやつはこの時点で表示
    if(_isRankInterstitial){
        return NO;
    } else {
        
        if(!_firstTimes) {
                        
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"logged_in"];
            
            if (playCount > 0) {
                
                return YES;
                
            } else {
                
                return NO;
            }
            
        } else {
            
            return YES;
        }
        
    }
}

-(void)setRankType:(BOOL)isRankInterstitial{
    _isRankInterstitial = isRankInterstitial;
}

@end
