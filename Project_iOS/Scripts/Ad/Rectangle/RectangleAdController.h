//
//  RectangleAdController.m
//  Unity-iPhone
//
//  Created  on 2015/10/30.
//
//  レクタングル広告の親クラスこれを継承して各プロバイダ毎のコントローラーを作成

#import "CommonConfig.h"

@interface RectangleAdController : NSObject{
    UIView* _rectangleAdView;
    BOOL isEndRect;
}

-(id)init:(int)screenIndex;
-(void)updateRectangleAd:(BOOL)enable;

-(UIView*)prepareRectangleAdEnd:(CGRect)frame;

-(void) removeRectangleAd;
-(void) reloadRectangleAd;

-(void)dealloc;
@end
