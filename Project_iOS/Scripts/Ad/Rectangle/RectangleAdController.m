//
//  RectangleAdController.m
//  Unity-iPhone
//
//  Created  on 2015/10/30.
//
//  レクタングル広告の親クラスこれを継承して各プロバイダ毎のコントローラーを作成

#import "RectangleAdController.h"


@implementation RectangleAdController


//=================================================================================
//共通処理
//=================================================================================

//初期化
- (id)init:(int)screenIndex{
    self = [super init];
    
    _rectangleAdView = [[UIView alloc] init];
    _rectangleAdView.backgroundColor = [UIColor clearColor];
    
    [_rectangleAdView setFrame:[self GetRectangleAdFrame:screenIndex]];
    switch (screenIndex) {
        case 2:
        {
            [_rectangleAdView addSubview: [self prepareRectangleAdEnd:[self GetFrame]]];
            break;
        }
        default:
            break;
    }


    [_rectangleAdView setHidden:true];

    [UnityGetGLViewController().view addSubview:_rectangleAdView];

    return self;
}


//レクタングルのフレームを取得
-(CGRect)GetRectangleAdFrame:(int)screenIndex{

    //下部に表示
    switch (screenIndex) {
            
        case 2:
        {
            return CGRectMake(0,
                              [[UIScreen mainScreen] bounds].size.height - RECT_AD_HEIGHT -10,
                              [[UIScreen mainScreen] bounds].size.width,
                              RECT_AD_HEIGHT);
            break;
        }
        default:
            break;
    }

    
    return CGRectMake(0,
                      0,
                      [[UIScreen mainScreen] bounds].size.width,
                      RECT_AD_HEIGHT);

}

//フレームを取得
-(CGRect)GetFrame{
    return  CGRectMake(([_rectangleAdView frame].size.width - RECT_AD_WIDTH)*0.5,
                       0,
                       RECT_AD_WIDTH,
                       RECT_AD_HEIGHT);
}


//非表示切り替え
-(void)updateRectangleAd:(BOOL)enable{
    [_rectangleAdView setHidden:!enable];
    
    if(!enable)
        [self removeRectangleAd];
    else
        [self reloadRectangleAd];
}

-(void) removeRectangleAd
{
    
}

-(void) reloadRectangleAd
{
    
}

//=================================================================================
//継承先で実装
//=================================================================================

//レクタングルアドを初期化
-(UIView *)prepareRectangleAdEnd:(CGRect)frame{
    return nil;
}


-(UIView *)prepareRectangleAdNative:(CGRect)frame{
	return nil;
}

-(void)dealloc{
}


@end
