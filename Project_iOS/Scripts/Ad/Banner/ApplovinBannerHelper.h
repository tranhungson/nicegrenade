//
//  BannerAdController.h
//  Unity-iPhone
//
//  Created  on 2015/10/28.
//
//  Adstirのバナー広告のコントローラー

#import "BannerAdController.h"
#import <AppLovinSDK/AppLovinSDK.h>

@interface ApplovinBannerHelper : BannerAdController

-(UIView*)prepareBannerAd;

@end
