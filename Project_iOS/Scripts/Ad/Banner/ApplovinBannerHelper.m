//
//  BannerAdController.m
//  Unity-iPhone
//
//  Created  on 2015/10/28.
//
//  Adstirのバナー広告のコントローラー

#import "ApplovinBannerHelper.h"
#import "CommonConfig.h"

@interface ApplovinBannerHelper()<MAAdViewAdDelegate>
    @property (nonatomic, strong) MAAdView *adView;
    @end


@implementation ApplovinBannerHelper
//バナーアドを初期化
-(UIView*)prepareBannerAd{
    
    int height = 0;
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone) {
        
        int sizeHight = (int)[[UIScreen mainScreen] nativeBounds].size.height;
        int sizeWidth = (int)[[UIScreen mainScreen] nativeBounds].size.width;
        float alpha = sizeHight/ sizeWidth;
        
        if(alpha >= 1.99f) {
            
            height = 30;
        }
    }
    
    self.adView = [[MAAdView alloc] initWithAdUnitIdentifier:APPLOVIN_BANNER_ID];
    self.adView.delegate = self;
    
    CGFloat adsheight = (UIDevice.currentDevice.userInterfaceIdiom == UIUserInterfaceIdiomPad) ? 90 : 50;
    
    CGFloat width = CGRectGetWidth(UIScreen.mainScreen.bounds);
    
    self.adView.frame = CGRectMake((UnityGetGLViewController().view.bounds.size.width - width) / 2, UnityGetGLViewController().view.bounds.size.height - adsheight - height, width, adsheight + height + 2);
    self.adView.backgroundColor = [UIColor whiteColor];
    [self.adView loadAd];
    
    return self.adView;
}

#pragma mark - MAAdDelegate Protocol
    
- (void)didLoadAd:(MAAd *)ad {
    
     NSLog(@"Applovin banner: did load");
}
    
- (void)didFailToLoadAdForAdUnitIdentifier:(NSString *)adUnitIdentifier withErrorCode:(NSInteger)errorCode {
    NSLog(@"Applovin banner: did fail with Id %@ - code %ld", adUnitIdentifier, (long)errorCode);
}
    
- (void)didDisplayAd:(MAAd *)ad {
    NSLog(@"Applovin banner: didDisplayAd");
}
    
- (void)didHideAd:(MAAd *)ad {
     NSLog(@"Applovin banner: didHideAd");
}
    
- (void)didClickAd:(MAAd *)ad {
     NSLog(@"Applovin banner: didClickAd");
}
    
- (void)didFailToDisplayAd:(MAAd *)ad withErrorCode:(NSInteger)errorCode {
    NSLog(@"Applovin banner: didFailToDisplayAd with error %ld", (long)errorCode);
}
    
#pragma mark - MAAdViewAdDelegate Protocol
    
- (void)didExpandAd:(MAAd *)ad {
    NSLog(@"Applovin banner: didExpandAd");
}
    
- (void)didCollapseAd:(MAAd *)ad {
    NSLog(@"Applovin banner: didCollapseAd");
}
@end
