//
//  BannerAdController.h
//  Unity-iPhone
//
//  Created  on 2015/10/28.
//
//  バナー広告の親クラスこれを継承して各プロバイダ毎のコントローラーを作成

#import "CommonConfig.h"

@interface BannerAdController : NSObject{
    UIView *_bannerAdView;

    
}

-(id)init;
-(void)updateBannerAd:(BOOL)enable;

-(UIView*)prepareBannerAd;

@end
