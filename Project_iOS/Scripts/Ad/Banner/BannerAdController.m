//
//  BannerAdController.m
//  Unity-iPhone
//
//  Created  on 2015/10/28.
//
//  バナー広告の親クラスこれを継承して各プロバイダ毎のコントローラーを作成

#import "BannerAdController.h"

@implementation BannerAdController

//=================================================================================
//共通処理
//=================================================================================

//初期化
- (id)init{
    self = [super init];
    
    _bannerAdView = [self prepareBannerAd];
    [_bannerAdView setHidden:true];
    
    [UnityGetGLViewController().view addSubview: _bannerAdView];
    
    return self;
}

//非表示切り替え
-(void)updateBannerAd:(BOOL)enable{
    [_bannerAdView setHidden:!enable];
}

//=================================================================================
//継承先で実装
//=================================================================================

//バナーアドを初期化
-(UIView*)prepareBannerAd{
    return nil;
}


@end
