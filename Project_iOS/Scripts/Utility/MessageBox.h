//
//  MessageBox.h
//  Unity-iPhone
//
//  Created  on 2015/11/02.
//
//  メッセージを表示する便利クラス

#import "CommonConfig.h"

@interface MessageBox : NSObject;
//メッセージを表示
+ (void)Show:(NSString *)title message:(NSString*)message buttonTitle:(NSString*)buttonTitle;

@end