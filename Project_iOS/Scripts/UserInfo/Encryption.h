//
//  Encryption.h
//  TemplateProject
//
//
//

#ifndef __Common__EncryptionXorCalc__
#define __Common__EncryptionXorCalc__

#include <iostream>


class Encryption
{
public:
    //XOR演算による暗号化・復号化処理
    static std::string xorCalc(const int key, const char * data);

};

#endif
