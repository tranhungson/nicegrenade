#ifndef UnityCustomVibration_h
#define UnityCustomVibration_h

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioToolbox.h>

@interface UnityCustomVibration : NSObject
{
}

+ (void) Do: (int)type;

@end

#endif
