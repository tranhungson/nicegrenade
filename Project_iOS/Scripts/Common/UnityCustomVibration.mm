#import "UnityCustomVibration.h"
#pragma mark - UnityCustomVibration

@interface UnityCustomVibration ()
@end

@implementation UnityCustomVibration
+ (void) Do: (int)type
{
    // 'Peek' feedback (weak boom) - 1519 - VibrateSuperLight_Peek
    // 'Pop' feedback (strong boom) - 1520 - VibrateLight_Pop
    // 'Cancelled' feedback (three sequential weak booms) - 1521 - VibrateLightLong_Cancelled
    // 'Try Again' feedback (week boom then strong boom) - 1102
    // 'Failed' feedback (three sequential strong booms) - 1107
    
    switch (type)
    {
        case 0:
        {
            if (@available(iOS 10, *)){
                NSString *code = [[UIDevice currentDevice] valueForKey:@"_feedbackSupportLevel"];
                if (code.intValue == 2){
                    UISelectionFeedbackGenerator *generator = [[UISelectionFeedbackGenerator alloc] init];
                    [generator prepare];
                    [generator selectionChanged];
                    generator = nil;
                }else{
                    AudioServicesPlaySystemSound(1519);
                }
            }else{
                AudioServicesPlaySystemSound(1519);
            }
        }
            break;
                
        case 1:
            if (@available(iOS 10, *)){
                NSString *code = [[UIDevice currentDevice] valueForKey:@"_feedbackSupportLevel"];
                if (code.intValue == 2){
                    UIImpactFeedbackGenerator *generator = [[UIImpactFeedbackGenerator alloc] initWithStyle:UIImpactFeedbackStyleMedium];
                    [generator prepare];
                    [generator impactOccurred];
                    generator = nil;
                }else{
                    AudioServicesPlaySystemSound(1520);
                }
            }else{
                AudioServicesPlaySystemSound(1520);
            }
            break;
            
        case 2:
            if (@available(iOS 10, *)){
                NSString *code = [[UIDevice currentDevice] valueForKey:@"_feedbackSupportLevel"];
                if (code.intValue == 2){
                    UINotificationFeedbackGenerator *generator = [[UINotificationFeedbackGenerator alloc] init];
                    [generator prepare];
                    [generator notificationOccurred:UINotificationFeedbackTypeError];
                    generator = nil;
                }else{
                    AudioServicesPlaySystemSound(1521);
                }
            }else{
                AudioServicesPlaySystemSound(1521);
            }
            break;
                
        case 3:
            AudioServicesPlaySystemSound(1102);
            break;
                
        case 4:
            AudioServicesPlaySystemSound(1107);
            break;
        
        case 7:
        {
            AudioServicesPlaySystemSound(1003);
            AudioServicesDisposeSystemSoundID(1003);
            break;
        }
                
        default:
            break;
    }
}

@end

#pragma mark - Unity Bridge

extern "C"
{
    void _UnityCustomVibrationDo(int type)
    {
        [UnityCustomVibration Do: type];
    }
}
