//
//  ShareController.m
//  Unity-iPhone
//
//  Created  on 2015/10/26.
//
//  シェア関係の処理を行うクラス

#import "ShareController.h"

@implementation ShareController

//=================================================================================
//初期化
//=================================================================================

//初期化
- (id)initWithGameObjectName:(const char *)gameobjectName{
    self = [super init];
    return self;
}

//=================================================================================
//内部
//=================================================================================


//=================================================================================
//外部
//=================================================================================

//シェア
//シェア
- (void)tweet:(const char *)text path:(const char *)path{
    NSString* shareMessage = [NSString stringWithUTF8String:text];
    NSString* imageFileSTR = [NSString stringWithUTF8String:path];
    NSLog(@"tweet %@", shareMessage);
    
    //    UIDevice *device = [UIDevice currentDevice];
    //    float version_ = [device.systemVersion floatValue];
    //    if (version_ < 6.0)
    //    {
    //        return;
    //    }
    
//    __block UIActivityIndicatorView *spiner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    UIViewController* view = UnityGetGLViewController();
    
//    [view.view addSubview:spiner];
//    CGRect mainFrame = view.view.frame;
//    CGSize spinerSize = spiner.frame.size;
//    float padingX = mainFrame.origin.x + mainFrame.size.width*0.5 - spinerSize.width*0.5;
//    float padingY = mainFrame.origin.y + mainFrame.size.height*0.5 - spinerSize.height*0.5;
//    [spiner setFrame:CGRectMake(padingX , padingY, spinerSize.width, spinerSize.height)];
//    [spiner startAnimating];
    
    // 画像リサイズ
    UIImage* image = [UIImage imageWithContentsOfFile:imageFileSTR];
    UIGraphicsBeginImageContext(CGSizeMake(image.size.width*0.5, image.size.height*0.5));
    [image drawInRect:CGRectMake(0, 0, image.size.width*0.5, image.size.height*0.5)];
    UIImage *resizedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    //    // 画像切り抜き
    //    CGRect trimArea = CGRectMake(0, 0, resizedImage.size.width, resizedImage.size.height*0.6);
    //    CGImageRef srcImageRef = [resizedImage CGImage];
    //    CGImageRef trimmedImageRef = CGImageCreateWithImageInRect(srcImageRef, trimArea);
    //    UIImage *trimmedImage = [UIImage imageWithCGImage:trimmedImageRef];
    
    
    NSArray* items = [[NSArray alloc] initWithObjects:shareMessage, resizedImage, nil];
    UIActivityViewController* viewController = [[UIActivityViewController alloc] initWithActivityItems:items applicationActivities:nil];
    
    if ( [viewController respondsToSelector:@selector(popoverPresentationController)] )
    {
        //iOS8以降
        viewController.popoverPresentationController.sourceView = view.view;
    }
    
    [view presentViewController:viewController animated:YES completion:^{
//        [spiner removeFromSuperview];
//        spiner = nil;
    }];
    
    
    //    SLComposeViewController *tweetViewController = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    //    UIImage *image = [UIImage imageWithContentsOfFile:[NSString stringWithUTF8String:path]];
    //    [tweetViewController addImage:image];
    //
    //    [tweetViewController setInitialText:@"aadfghu adiur aidfg"];
    //
    //    [tweetViewController setCompletionHandler:^(SLComposeViewControllerResult result) {
    //
    //        switch (result) {
    //            case SLComposeViewControllerResultCancelled:
    //                NSLog(@"Post Canceled");
    //                break;
    //            case SLComposeViewControllerResultDone:
    //                NSLog(@"Post Sucessful");
    //                break;
    //
    //            default:
    //                break;
    //        }
    //        [UnityGetGLViewController() dismissViewControllerAnimated:YES completion:nil];
    //    }];
    //
    //    [UnityGetGLViewController() presentViewController:tweetViewController animated:YES completion:nil];
    
    //    TWTweetComposeViewController *tweetViewController = [[TWTweetComposeViewController alloc] init];
    //    [tweetViewController setInitialText:shareMessage];
    //    UIImage *image = [UIImage imageWithContentsOfFile:[NSString stringWithUTF8String:path]];
    //    [tweetViewController addImage:image];
    //
    //    [UnityGetGLViewController() presentViewController:tweetViewController animated:YES completion:nil];
    //
    //    tweetViewController.completionHandler = ^(TWTweetComposeViewControllerResult twResult) {
    //        switch (twResult) {
    //            case TWTweetComposeViewControllerResultCancelled:
    //                //キャンセル
    //                break;
    //            case TWTweetComposeViewControllerResultDone:
    //                //送信完了
    //                break;
    //            default:
    //                break;
    //        }
    //        [UnityGetGLViewController() dismissViewControllerAnimated:YES completion:nil];
    //    };
}


@end
