//
//  CommonConfig.h
//  Unity-iPhone
//
//  Created  on 2015/10/28.
//
//  諸々の設定を定義するファイル

#ifndef CommonConfig
#define CommonConfig


//GAE URL
#define FMS_HOST @"shinflags.appspot.com";

//ポップアップ用アプリID
#define APP_ID @"MAZ"

//GAE ID
#define POP_ID @"POPNEW01"
#define POP_ID_E @"POPNEW01E"
#define END_ID @"END01"
#define SPL_ID @"SPL01"

//iOS_ID
#define IOS_APP_ID (1490064332)

//ランキングID(基本的に1を使う。2,3は複数のランキングがある場合)
#define RANKING_KEY_1 @"bestscore_template1"
#define RANKING_KEY_2 @"bestscore_template2"
#define RANKING_KEY_3 @"bestscore_template3"

//=================================================================================
//シーンの設定
//=================================================================================

//各シーンの番号
enum SceneNo{
    SCENE_TITLE        = 0,
    SCENE_GAME         = 1,
    SCENE_RESULT       = 2,
    SCENE_SELECT       = 3,
    SCENE_SHOP         = 4,

    SCENE_TITLE_HELP   = 1000,
    SCENE_PAUSE        = 10000,
};

//=================================================================================
//レビュー
//=================================================================================
#pragma mark REVIEW

#define REVIEW_SHOW_COUNT (5)   // 動作は REVIEW_SHOW_COUNT +1 です。
#define POPUP_TITLE @"新記録おめでとうございます！"
#define POPUP_MESSAGE @"\nもしよかったら\nレビューを書いてもらえますか？"

#define POPUP_OK_BUTTON_TITLE @"レビューを書く"
#define POPUP_OK_OTHER_BUTTON_TITLE @"二度と表示しない"
#define POPUP_OK_CANCEL_BUTTON_TITLE @"キャンセル"

//Ad Layout Type
//自社広告を出す場合は有効に、ネイティブ広告を出す場合はコメントアウトしてください
//#define AD_LAYOUT_TYPE_A

//=================================================================================
//広告
//=================================================================================
#pragma mark AD

//FLurry
#define FLURRY_API_KEY					@"9PSTX6T8B2P3ZK3SJQ8R"
#define FLURRY_EVENT_HEADER_TAPPED		(@"POPUP_TAP_YES")
#define FLURRY_EVENT_HEADER_SHOW		(@"POPUP_SHOW")

//Game analytic
#define GAME_ANALYTIC_GAME_KEY          @"bd329724d21bc2f1065bccb6937499e6"
#define GAME_ANALYTIC_HASH_KEY          @"bf0e9be29bc0e701237b0d8a50f6ebca5d2b3232"

#define TENJIN_KEY                      @"QENGYAKZDN1V4C9UQUBFLMGPYKIJ8VXU"

#define APPLOVIN_BANNER_ID             @""
#define APPLOVIN_INTERSTITIAL_ID       @""
#define APPLOVIN_VIDEO_ID              @""

#define UNITY_OBJECT_NAME "NativeManager"

//=================================================================================
//他
//=================================================================================

//Rectangle Ad Size
#define RECT_AD_WIDTH  300
#define RECT_AD_HEIGHT 250

//ログ非表示用マクロ
#ifdef DEBUG_LOG
# define NSLog(...) NSLog(__VA_ARGS__);
#else
# define NSLog(...)
#endif

#endif
