﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 UnityEngine.SphereCollider UnityEngine.ClothSphereColliderPair::get_first()
extern void ClothSphereColliderPair_get_first_m6EE02F2490C866EE3DE10C883F50DE6BDD2F2E13_AdjustorThunk ();
// 0x00000002 UnityEngine.SphereCollider UnityEngine.ClothSphereColliderPair::get_second()
extern void ClothSphereColliderPair_get_second_m2E16CC7EA6FA1CA4338A2F9C82497B6FD6C496BB_AdjustorThunk ();
// 0x00000003 UnityEngine.CapsuleCollider[] UnityEngine.Cloth::get_capsuleColliders()
extern void Cloth_get_capsuleColliders_m63C7E3486D024D778CE9800B916E50509DA5118C ();
// 0x00000004 UnityEngine.ClothSphereColliderPair[] UnityEngine.Cloth::get_sphereColliders()
extern void Cloth_get_sphereColliders_mFF63C8FC5B1F96C7A913AC4546CE54BCB38186BF ();
static Il2CppMethodPointer s_methodPointers[4] = 
{
	ClothSphereColliderPair_get_first_m6EE02F2490C866EE3DE10C883F50DE6BDD2F2E13_AdjustorThunk,
	ClothSphereColliderPair_get_second_m2E16CC7EA6FA1CA4338A2F9C82497B6FD6C496BB_AdjustorThunk,
	Cloth_get_capsuleColliders_m63C7E3486D024D778CE9800B916E50509DA5118C,
	Cloth_get_sphereColliders_mFF63C8FC5B1F96C7A913AC4546CE54BCB38186BF,
};
static const int32_t s_InvokerIndices[4] = 
{
	14,
	14,
	14,
	14,
};
extern const Il2CppCodeGenModule g_UnityEngine_ClothModuleCodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_ClothModuleCodeGenModule = 
{
	"UnityEngine.ClothModule.dll",
	4,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
