﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.String UnityEngine.PhysicsScene2D::ToString()
extern void PhysicsScene2D_ToString_m6F48AC6CE0D8540FCE4914ABB78ED0BAF0D83CBE_AdjustorThunk ();
// 0x00000002 System.Int32 UnityEngine.PhysicsScene2D::GetHashCode()
extern void PhysicsScene2D_GetHashCode_mB1C0E9E977ACCBF0AA0D266E5851B4D778354467_AdjustorThunk ();
// 0x00000003 System.Boolean UnityEngine.PhysicsScene2D::Equals(System.Object)
extern void PhysicsScene2D_Equals_mA91E96FDE086CF876D4D469CBFF0D43400C834E8_AdjustorThunk ();
// 0x00000004 System.Boolean UnityEngine.PhysicsScene2D::Equals(UnityEngine.PhysicsScene2D)
extern void PhysicsScene2D_Equals_mAA6F413AD3CDDD052496FAF69C34A45CA25D4293_AdjustorThunk ();
// 0x00000005 UnityEngine.RaycastHit2D UnityEngine.PhysicsScene2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32)
extern void PhysicsScene2D_Raycast_m8A048506EDDC5C968DB55584FBF650DAB3BCB987_AdjustorThunk ();
// 0x00000006 UnityEngine.RaycastHit2D UnityEngine.PhysicsScene2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.ContactFilter2D)
extern void PhysicsScene2D_Raycast_mFA61658E024A98E2A7BC1B6965E9E5537DCA2DB8_AdjustorThunk ();
// 0x00000007 UnityEngine.RaycastHit2D UnityEngine.PhysicsScene2D::Raycast_Internal(UnityEngine.PhysicsScene2D,UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.ContactFilter2D)
extern void PhysicsScene2D_Raycast_Internal_mB24F5D2B6967C70371484EA703E16346DBFD0718 ();
// 0x00000008 System.Int32 UnityEngine.PhysicsScene2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.RaycastHit2D[],System.Int32)
extern void PhysicsScene2D_Raycast_mE2E2B8BE4BA6D7E8FACCCAE115C49FADA74A8CE6_AdjustorThunk ();
// 0x00000009 System.Int32 UnityEngine.PhysicsScene2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.ContactFilter2D,UnityEngine.RaycastHit2D[])
extern void PhysicsScene2D_Raycast_mE0460FE0CEE7076962DC2983A7B0DBB757DB133A_AdjustorThunk ();
// 0x0000000A System.Int32 UnityEngine.PhysicsScene2D::RaycastArray_Internal(UnityEngine.PhysicsScene2D,UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.ContactFilter2D,UnityEngine.RaycastHit2D[])
extern void PhysicsScene2D_RaycastArray_Internal_m40B8BDD4BE4D95E3826334DA2A8E31EBAD7B6E8D ();
// 0x0000000B System.Int32 UnityEngine.PhysicsScene2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.ContactFilter2D,System.Collections.Generic.List`1<UnityEngine.RaycastHit2D>)
extern void PhysicsScene2D_Raycast_mC5642256C2119435B87AD87ED30086973D9F3A24_AdjustorThunk ();
// 0x0000000C System.Int32 UnityEngine.PhysicsScene2D::RaycastList_Internal(UnityEngine.PhysicsScene2D,UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.ContactFilter2D,System.Collections.Generic.List`1<UnityEngine.RaycastHit2D>)
extern void PhysicsScene2D_RaycastList_Internal_m4D2446707FAC9EC36975B8119616F30BB724EA09 ();
// 0x0000000D System.Int32 UnityEngine.PhysicsScene2D::GetRayIntersection(UnityEngine.Ray,System.Single,UnityEngine.RaycastHit2D[],System.Int32)
extern void PhysicsScene2D_GetRayIntersection_m2DB850378F1910BFC62243A1A33D8B17738882EC_AdjustorThunk ();
// 0x0000000E System.Int32 UnityEngine.PhysicsScene2D::GetRayIntersectionArray_Internal(UnityEngine.PhysicsScene2D,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.RaycastHit2D[])
extern void PhysicsScene2D_GetRayIntersectionArray_Internal_m1A9DC1520B80AF8444C38FEEDEB40EDD405805A5 ();
// 0x0000000F UnityEngine.Collider2D UnityEngine.PhysicsScene2D::OverlapPoint(UnityEngine.Vector2,UnityEngine.ContactFilter2D)
extern void PhysicsScene2D_OverlapPoint_m4575E9ADCF25D78BD59B4BC15C516AAC82B58744_AdjustorThunk ();
// 0x00000010 UnityEngine.Collider2D UnityEngine.PhysicsScene2D::OverlapPoint_Internal(UnityEngine.PhysicsScene2D,UnityEngine.Vector2,UnityEngine.ContactFilter2D)
extern void PhysicsScene2D_OverlapPoint_Internal_mFE43D6F27EC3A15F2A27E0A1731F45B4C851664D ();
// 0x00000011 System.Int32 UnityEngine.PhysicsScene2D::OverlapPoint(UnityEngine.Vector2,UnityEngine.ContactFilter2D,UnityEngine.Collider2D[])
extern void PhysicsScene2D_OverlapPoint_m5AE8607A7F3230465CA31DC532E4DF0005EDE003_AdjustorThunk ();
// 0x00000012 System.Int32 UnityEngine.PhysicsScene2D::OverlapPointArray_Internal(UnityEngine.PhysicsScene2D,UnityEngine.Vector2,UnityEngine.ContactFilter2D,UnityEngine.Collider2D[])
extern void PhysicsScene2D_OverlapPointArray_Internal_m48F806E4D712E54817810D1394747A69D1BD623A ();
// 0x00000013 System.Int32 UnityEngine.PhysicsScene2D::OverlapCircle(UnityEngine.Vector2,System.Single,UnityEngine.ContactFilter2D,UnityEngine.Collider2D[])
extern void PhysicsScene2D_OverlapCircle_m204738F4E5D2E9923E1671E5E15750A7641F7950_AdjustorThunk ();
// 0x00000014 System.Int32 UnityEngine.PhysicsScene2D::OverlapCircleArray_Internal(UnityEngine.PhysicsScene2D,UnityEngine.Vector2,System.Single,UnityEngine.ContactFilter2D,UnityEngine.Collider2D[])
extern void PhysicsScene2D_OverlapCircleArray_Internal_mB9277F0D2860FB4D2E1734CB01460BA48937397F ();
// 0x00000015 System.Void UnityEngine.PhysicsScene2D::Raycast_Internal_Injected(UnityEngine.PhysicsScene2D&,UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&,UnityEngine.RaycastHit2D&)
extern void PhysicsScene2D_Raycast_Internal_Injected_m197B563F302D9E7C336EE7BB0A356F6785F1584A ();
// 0x00000016 System.Int32 UnityEngine.PhysicsScene2D::RaycastArray_Internal_Injected(UnityEngine.PhysicsScene2D&,UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&,UnityEngine.RaycastHit2D[])
extern void PhysicsScene2D_RaycastArray_Internal_Injected_mC5FDF82692390ECAB17CF821D25349A66B3C8143 ();
// 0x00000017 System.Int32 UnityEngine.PhysicsScene2D::RaycastList_Internal_Injected(UnityEngine.PhysicsScene2D&,UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&,System.Collections.Generic.List`1<UnityEngine.RaycastHit2D>)
extern void PhysicsScene2D_RaycastList_Internal_Injected_mD8495122B2F8BD1194E83CA20DFC005D414C707B ();
// 0x00000018 System.Int32 UnityEngine.PhysicsScene2D::GetRayIntersectionArray_Internal_Injected(UnityEngine.PhysicsScene2D&,UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.RaycastHit2D[])
extern void PhysicsScene2D_GetRayIntersectionArray_Internal_Injected_m74194745127DA849411A2191EE7C52EB07BB21A9 ();
// 0x00000019 UnityEngine.Collider2D UnityEngine.PhysicsScene2D::OverlapPoint_Internal_Injected(UnityEngine.PhysicsScene2D&,UnityEngine.Vector2&,UnityEngine.ContactFilter2D&)
extern void PhysicsScene2D_OverlapPoint_Internal_Injected_mF130C9EAF9101517F47E9D48EAA794F4825CF029 ();
// 0x0000001A System.Int32 UnityEngine.PhysicsScene2D::OverlapPointArray_Internal_Injected(UnityEngine.PhysicsScene2D&,UnityEngine.Vector2&,UnityEngine.ContactFilter2D&,UnityEngine.Collider2D[])
extern void PhysicsScene2D_OverlapPointArray_Internal_Injected_m8BB576977982188BF9B57A7023B015D59C67F642 ();
// 0x0000001B System.Int32 UnityEngine.PhysicsScene2D::OverlapCircleArray_Internal_Injected(UnityEngine.PhysicsScene2D&,UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&,UnityEngine.Collider2D[])
extern void PhysicsScene2D_OverlapCircleArray_Internal_Injected_mC8CA0F2C5DE4CD6B39A56011400C63F2958E5674 ();
// 0x0000001C UnityEngine.PhysicsScene2D UnityEngine.Physics2D::get_defaultPhysicsScene()
extern void Physics2D_get_defaultPhysicsScene_m2C9DA4DFAFB71332EC48E50CCB16275441CADE84 ();
// 0x0000001D System.Boolean UnityEngine.Physics2D::get_queriesHitTriggers()
extern void Physics2D_get_queriesHitTriggers_m8BB98B1754A86777B4D58A4F28F63E8EC77B031B ();
// 0x0000001E UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2)
extern void Physics2D_Raycast_mD22D6BC52ACAB22598A720525B3840C019842FFC ();
// 0x0000001F UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern void Physics2D_Raycast_m468BF2D74BED92728533EA2108830C44ED93A0EF ();
// 0x00000020 UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32)
extern void Physics2D_Raycast_mBEB66E9AA034BD0AE1B1C99DF872247B0131CBDD ();
// 0x00000021 UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32,System.Single)
extern void Physics2D_Raycast_mB43742B1077F487D1458388C5B11EE46D73533C0 ();
// 0x00000022 UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Single)
extern void Physics2D_Raycast_m4803AD692674FEE7EE269A6170AD5CEFEA6D3D78 ();
// 0x00000023 System.Int32 UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.ContactFilter2D,UnityEngine.RaycastHit2D[])
extern void Physics2D_Raycast_m0C22B1CACFA7E2A16D731B6E2D9D2ABC0666CCCE ();
// 0x00000024 System.Int32 UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.ContactFilter2D,UnityEngine.RaycastHit2D[],System.Single)
extern void Physics2D_Raycast_m8678AB161A71C09D7606299D194A90BA814BA543 ();
// 0x00000025 System.Int32 UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.ContactFilter2D,System.Collections.Generic.List`1<UnityEngine.RaycastHit2D>,System.Single)
extern void Physics2D_Raycast_m940284F559A12F0594CF6E1A20583F7EA67E8645 ();
// 0x00000026 System.Int32 UnityEngine.Physics2D::RaycastNonAlloc(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.RaycastHit2D[])
extern void Physics2D_RaycastNonAlloc_m1F45E84C0D9E8E71537A464C15C0BCB557114874 ();
// 0x00000027 System.Int32 UnityEngine.Physics2D::RaycastNonAlloc(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.RaycastHit2D[],System.Single)
extern void Physics2D_RaycastNonAlloc_m94BB67047043AF11DA7134882A71611C75B0CBE5 ();
// 0x00000028 System.Int32 UnityEngine.Physics2D::RaycastNonAlloc(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.RaycastHit2D[],System.Single,System.Int32)
extern void Physics2D_RaycastNonAlloc_m2571094B334BA2DF1AB1F83B3DD1CA82687A93FB ();
// 0x00000029 System.Int32 UnityEngine.Physics2D::RaycastNonAlloc(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.RaycastHit2D[],System.Single,System.Int32,System.Single,System.Single)
extern void Physics2D_RaycastNonAlloc_mD9A24B7936572AB37514F15CC6442129B439BC21 ();
// 0x0000002A UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::GetRayIntersectionAll(UnityEngine.Ray)
extern void Physics2D_GetRayIntersectionAll_mBD650C3EA6E692CE3E1255B6EAADF659307012D8 ();
// 0x0000002B UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::GetRayIntersectionAll(UnityEngine.Ray,System.Single)
extern void Physics2D_GetRayIntersectionAll_mACC24DD73E1388C1DF86847390B89A5B0223F03A ();
// 0x0000002C UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::GetRayIntersectionAll(UnityEngine.Ray,System.Single,System.Int32)
extern void Physics2D_GetRayIntersectionAll_m04BCAB03333B049C48BE61036E512E12A5FBD053 ();
// 0x0000002D UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::GetRayIntersectionAll_Internal(UnityEngine.PhysicsScene2D,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32)
extern void Physics2D_GetRayIntersectionAll_Internal_m4E68866CF4A79A58DBF4B8A355D3EEE62BEF6612 ();
// 0x0000002E System.Int32 UnityEngine.Physics2D::GetRayIntersectionNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit2D[])
extern void Physics2D_GetRayIntersectionNonAlloc_m5F1AF31EEB67FE97AD2C40C102914371C2E825F0 ();
// 0x0000002F System.Int32 UnityEngine.Physics2D::GetRayIntersectionNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit2D[],System.Single)
extern void Physics2D_GetRayIntersectionNonAlloc_m1A638894F08E9F401C7161D02171805B4897B51E ();
// 0x00000030 System.Int32 UnityEngine.Physics2D::GetRayIntersectionNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit2D[],System.Single,System.Int32)
extern void Physics2D_GetRayIntersectionNonAlloc_m3817EA2CC7B95C89683ACE0E433D6D4C6735CA0A ();
// 0x00000031 UnityEngine.Collider2D UnityEngine.Physics2D::OverlapPoint(UnityEngine.Vector2,System.Int32)
extern void Physics2D_OverlapPoint_m8085D4E5C284AB1550C5AF62DF33F6DD64E64F4F ();
// 0x00000032 System.Int32 UnityEngine.Physics2D::OverlapPointNonAlloc(UnityEngine.Vector2,UnityEngine.Collider2D[],System.Int32)
extern void Physics2D_OverlapPointNonAlloc_mEC65BD8ADAA15BC39520011ED1AC32ACA0EB0CFE ();
// 0x00000033 UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapCircleAll(UnityEngine.Vector2,System.Single)
extern void Physics2D_OverlapCircleAll_mBFBC7E35BEE5A16F6AB7184BF7302A6DBB9278BB ();
// 0x00000034 UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapCircleAll_Internal(UnityEngine.PhysicsScene2D,UnityEngine.Vector2,System.Single,UnityEngine.ContactFilter2D)
extern void Physics2D_OverlapCircleAll_Internal_m0542CFE7F662C028FFCD91A68AF5907F963AC889 ();
// 0x00000035 System.Int32 UnityEngine.Physics2D::OverlapCircleNonAlloc(UnityEngine.Vector2,System.Single,UnityEngine.Collider2D[],System.Int32,System.Single,System.Single)
extern void Physics2D_OverlapCircleNonAlloc_m2346A2F01D5596A6242C0D3F0DA10C38A6594886 ();
// 0x00000036 System.Void UnityEngine.Physics2D::.cctor()
extern void Physics2D__cctor_mC0D622F2EAF13BF0513DB2969E50EEC5631CDBFC ();
// 0x00000037 UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::GetRayIntersectionAll_Internal_Injected(UnityEngine.PhysicsScene2D&,UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32)
extern void Physics2D_GetRayIntersectionAll_Internal_Injected_m8B627D4448B34665FC8BCF560EE851152FE2D15A ();
// 0x00000038 UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapCircleAll_Internal_Injected(UnityEngine.PhysicsScene2D&,UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&)
extern void Physics2D_OverlapCircleAll_Internal_Injected_mEFD991E51C0EC3EB5BD1BF01E06E98E115E62398 ();
// 0x00000039 System.Void UnityEngine.ContactFilter2D::CheckConsistency()
extern void ContactFilter2D_CheckConsistency_m0E1FC7D646C418F545F778197348F97ADA5409A2_AdjustorThunk ();
// 0x0000003A System.Void UnityEngine.ContactFilter2D::SetLayerMask(UnityEngine.LayerMask)
extern void ContactFilter2D_SetLayerMask_mECEE981A09393F1097555D46449ED7CA4D8659E6_AdjustorThunk ();
// 0x0000003B System.Void UnityEngine.ContactFilter2D::SetDepth(System.Single,System.Single)
extern void ContactFilter2D_SetDepth_mF4AB9C380EDC3726D58734010BD90AD7D36ABDB0_AdjustorThunk ();
// 0x0000003C UnityEngine.ContactFilter2D UnityEngine.ContactFilter2D::CreateLegacyFilter(System.Int32,System.Single,System.Single)
extern void ContactFilter2D_CreateLegacyFilter_mA52A1C54BA7C4A49094B172BE7FA6044EF346A51 ();
// 0x0000003D System.Void UnityEngine.ContactFilter2D::CheckConsistency_Injected(UnityEngine.ContactFilter2D&)
extern void ContactFilter2D_CheckConsistency_Injected_m4640AA8896FEFE90396C5B47C4FE07930DA918BE ();
// 0x0000003E UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_point()
extern void RaycastHit2D_get_point_mC567E234B1B673C3A9819023C3DC97C781443098_AdjustorThunk ();
// 0x0000003F UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_normal()
extern void RaycastHit2D_get_normal_m9F0974E4514AD56C00FCF6FF4CDF10AED62FE6E4_AdjustorThunk ();
// 0x00000040 System.Single UnityEngine.RaycastHit2D::get_distance()
extern void RaycastHit2D_get_distance_m2D9F391717ECACFDA8E01A4126E0F8F59F7E774F_AdjustorThunk ();
// 0x00000041 UnityEngine.Collider2D UnityEngine.RaycastHit2D::get_collider()
extern void RaycastHit2D_get_collider_m6A7EC53B2E179C2EFF4F29018A132B2979CBE976_AdjustorThunk ();
// 0x00000042 UnityEngine.Rigidbody2D UnityEngine.RaycastHit2D::get_rigidbody()
extern void RaycastHit2D_get_rigidbody_mE6D43BB62B41529F1FF3A66615C571B418662F47_AdjustorThunk ();
// 0x00000043 UnityEngine.Transform UnityEngine.RaycastHit2D::get_transform()
extern void RaycastHit2D_get_transform_m9AD59D59FC4F85128F139C0DE9B0A2AE27D75DAC_AdjustorThunk ();
// 0x00000044 UnityEngine.Vector2 UnityEngine.Rigidbody2D::get_position()
extern void Rigidbody2D_get_position_m68CB3236D19D7472ABDE1F5A5A9BD924595361B8 ();
// 0x00000045 System.Void UnityEngine.Rigidbody2D::set_position(UnityEngine.Vector2)
extern void Rigidbody2D_set_position_m33DABE99FD458EEA8807FCC48F99AAFFCFECD60F ();
// 0x00000046 System.Single UnityEngine.Rigidbody2D::get_rotation()
extern void Rigidbody2D_get_rotation_mAF8F2E151EF82D8CF48DEDC17FE6882C2A67AF5C ();
// 0x00000047 System.Void UnityEngine.Rigidbody2D::set_rotation(System.Single)
extern void Rigidbody2D_set_rotation_m0945A4D66B39C6698EFBF73F2A95969C976A858B ();
// 0x00000048 System.Void UnityEngine.Rigidbody2D::MoveRotation(System.Single)
extern void Rigidbody2D_MoveRotation_m657F4C682EF73C115151B5442B0AD415C09F130A ();
// 0x00000049 System.Void UnityEngine.Rigidbody2D::MoveRotation_Angle(System.Single)
extern void Rigidbody2D_MoveRotation_Angle_mC3124E587CD7E3524A1FFE8C82808D73BD013EF4 ();
// 0x0000004A UnityEngine.Vector2 UnityEngine.Rigidbody2D::get_velocity()
extern void Rigidbody2D_get_velocity_m5ABF36BDF90FD7308BE608667B9E8F3DA5A207F1 ();
// 0x0000004B System.Void UnityEngine.Rigidbody2D::set_velocity(UnityEngine.Vector2)
extern void Rigidbody2D_set_velocity_mE0DBCE5B683024B106C2AB6943BBA550B5BD0B83 ();
// 0x0000004C System.Single UnityEngine.Rigidbody2D::get_angularVelocity()
extern void Rigidbody2D_get_angularVelocity_m98681305F4188D29DA152F508BE7B73260CCEE46 ();
// 0x0000004D System.Void UnityEngine.Rigidbody2D::set_angularVelocity(System.Single)
extern void Rigidbody2D_set_angularVelocity_mEA0807B27FFA3A839397575C2696F6B15693C599 ();
// 0x0000004E System.Single UnityEngine.Rigidbody2D::get_mass()
extern void Rigidbody2D_get_mass_mD217EC45743AB52C6555287A0FF765BC7F6002E9 ();
// 0x0000004F System.Void UnityEngine.Rigidbody2D::set_mass(System.Single)
extern void Rigidbody2D_set_mass_m36294E719B842A9CEF10925A77A822BE47B669B3 ();
// 0x00000050 System.Void UnityEngine.Rigidbody2D::set_bodyType(UnityEngine.RigidbodyType2D)
extern void Rigidbody2D_set_bodyType_m239CDB6FFA033FD3B5BAC061A3F96DC9264D9900 ();
// 0x00000051 System.Void UnityEngine.Rigidbody2D::set_isKinematic(System.Boolean)
extern void Rigidbody2D_set_isKinematic_mA7711684E1E1E25FA7C1A1FF297B6E45DFD03BEE ();
// 0x00000052 System.Boolean UnityEngine.Rigidbody2D::IsSleeping()
extern void Rigidbody2D_IsSleeping_mF480444AF1AF7F57463437CAB1607F100AB05D70 ();
// 0x00000053 System.Void UnityEngine.Rigidbody2D::Sleep()
extern void Rigidbody2D_Sleep_m79A8750BA2160738B63771EBDA27F63272B349F3 ();
// 0x00000054 System.Void UnityEngine.Rigidbody2D::WakeUp()
extern void Rigidbody2D_WakeUp_m50C62CDD1DC46A9D27C11E9013DFF5FE37D6EC23 ();
// 0x00000055 System.Void UnityEngine.Rigidbody2D::.ctor()
extern void Rigidbody2D__ctor_mEADC6DF3297B27BF8DAE647A5D3384CA80E005FB ();
// 0x00000056 System.Void UnityEngine.Rigidbody2D::get_position_Injected(UnityEngine.Vector2&)
extern void Rigidbody2D_get_position_Injected_m7684DCAD6874A8D0A8560A052FEF85EF48280F73 ();
// 0x00000057 System.Void UnityEngine.Rigidbody2D::set_position_Injected(UnityEngine.Vector2&)
extern void Rigidbody2D_set_position_Injected_m1C9E1C0F87779A134997F60E761CB23149B3C231 ();
// 0x00000058 System.Void UnityEngine.Rigidbody2D::get_velocity_Injected(UnityEngine.Vector2&)
extern void Rigidbody2D_get_velocity_Injected_mFB508815764F2895389C2B75002D3D7A33630B3C ();
// 0x00000059 System.Void UnityEngine.Rigidbody2D::set_velocity_Injected(UnityEngine.Vector2&)
extern void Rigidbody2D_set_velocity_Injected_m8DCAEDCB8C0165DCEC87B70089C7B4EA41FEB73E ();
// 0x0000005A System.Void UnityEngine.Collider2D::set_isTrigger(System.Boolean)
extern void Collider2D_set_isTrigger_m15514794FC3D677B712080672EAEEA0CE2C3A20B ();
// 0x0000005B UnityEngine.Vector2 UnityEngine.Collider2D::get_offset()
extern void Collider2D_get_offset_mCB3DEFB9ACB05211320B8406B01F089EF7F8788D ();
// 0x0000005C System.Void UnityEngine.Collider2D::set_offset(UnityEngine.Vector2)
extern void Collider2D_set_offset_mC752F6CA4C47C5543538BA3BC9B04A3D267AAC6C ();
// 0x0000005D UnityEngine.Rigidbody2D UnityEngine.Collider2D::get_attachedRigidbody()
extern void Collider2D_get_attachedRigidbody_m7BBA6D4F834B78D334349066540EA4DDDE4F0308 ();
// 0x0000005E System.Void UnityEngine.Collider2D::.ctor()
extern void Collider2D__ctor_m9E497E4A527D5ADD864AB3D7901A4087C45712C0 ();
// 0x0000005F System.Void UnityEngine.Collider2D::get_offset_Injected(UnityEngine.Vector2&)
extern void Collider2D_get_offset_Injected_mFBEEA538206330D2D4C62A04DF339332596C0F97 ();
// 0x00000060 System.Void UnityEngine.Collider2D::set_offset_Injected(UnityEngine.Vector2&)
extern void Collider2D_set_offset_Injected_m98CB990275094E1261101B082C51DDD634CA0A79 ();
// 0x00000061 UnityEngine.Vector2 UnityEngine.BoxCollider2D::get_size()
extern void BoxCollider2D_get_size_m6230015317115D9BED5C61A4EDAC013C8A7664E1 ();
// 0x00000062 System.Void UnityEngine.BoxCollider2D::set_size(UnityEngine.Vector2)
extern void BoxCollider2D_set_size_mFA630C4BD41F786D208C1C0382A7FCF7868BA489 ();
// 0x00000063 System.Void UnityEngine.BoxCollider2D::get_size_Injected(UnityEngine.Vector2&)
extern void BoxCollider2D_get_size_Injected_m6E18F627969D38CF513DB4CF680ACD51810F74CD ();
// 0x00000064 System.Void UnityEngine.BoxCollider2D::set_size_Injected(UnityEngine.Vector2&)
extern void BoxCollider2D_set_size_Injected_mB787996D8E2E7919FE975BC435677115539A53A0 ();
// 0x00000065 System.Int32 UnityEngine.PolygonCollider2D::GetTotalPointCount()
extern void PolygonCollider2D_GetTotalPointCount_m3DB7B6E36C685F81F4A7DC503DD93E8F32347877 ();
// 0x00000066 UnityEngine.Vector2[] UnityEngine.PolygonCollider2D::get_points()
extern void PolygonCollider2D_get_points_m65A26A2D193E9F2C0D517635B46190FF8DBD9AF2 ();
// 0x00000067 System.Void UnityEngine.PolygonCollider2D::SetPath(System.Int32,UnityEngine.Vector2[])
extern void PolygonCollider2D_SetPath_mE0B11473C90B4F848A146DD08DC5EC23004E99CA ();
// 0x00000068 System.Void UnityEngine.PolygonCollider2D::SetPath_Internal(System.Int32,UnityEngine.Vector2[])
extern void PolygonCollider2D_SetPath_Internal_m74F981E2C411B7DA32620FD26AB792843FDA2843 ();
static Il2CppMethodPointer s_methodPointers[104] = 
{
	PhysicsScene2D_ToString_m6F48AC6CE0D8540FCE4914ABB78ED0BAF0D83CBE_AdjustorThunk,
	PhysicsScene2D_GetHashCode_mB1C0E9E977ACCBF0AA0D266E5851B4D778354467_AdjustorThunk,
	PhysicsScene2D_Equals_mA91E96FDE086CF876D4D469CBFF0D43400C834E8_AdjustorThunk,
	PhysicsScene2D_Equals_mAA6F413AD3CDDD052496FAF69C34A45CA25D4293_AdjustorThunk,
	PhysicsScene2D_Raycast_m8A048506EDDC5C968DB55584FBF650DAB3BCB987_AdjustorThunk,
	PhysicsScene2D_Raycast_mFA61658E024A98E2A7BC1B6965E9E5537DCA2DB8_AdjustorThunk,
	PhysicsScene2D_Raycast_Internal_mB24F5D2B6967C70371484EA703E16346DBFD0718,
	PhysicsScene2D_Raycast_mE2E2B8BE4BA6D7E8FACCCAE115C49FADA74A8CE6_AdjustorThunk,
	PhysicsScene2D_Raycast_mE0460FE0CEE7076962DC2983A7B0DBB757DB133A_AdjustorThunk,
	PhysicsScene2D_RaycastArray_Internal_m40B8BDD4BE4D95E3826334DA2A8E31EBAD7B6E8D,
	PhysicsScene2D_Raycast_mC5642256C2119435B87AD87ED30086973D9F3A24_AdjustorThunk,
	PhysicsScene2D_RaycastList_Internal_m4D2446707FAC9EC36975B8119616F30BB724EA09,
	PhysicsScene2D_GetRayIntersection_m2DB850378F1910BFC62243A1A33D8B17738882EC_AdjustorThunk,
	PhysicsScene2D_GetRayIntersectionArray_Internal_m1A9DC1520B80AF8444C38FEEDEB40EDD405805A5,
	PhysicsScene2D_OverlapPoint_m4575E9ADCF25D78BD59B4BC15C516AAC82B58744_AdjustorThunk,
	PhysicsScene2D_OverlapPoint_Internal_mFE43D6F27EC3A15F2A27E0A1731F45B4C851664D,
	PhysicsScene2D_OverlapPoint_m5AE8607A7F3230465CA31DC532E4DF0005EDE003_AdjustorThunk,
	PhysicsScene2D_OverlapPointArray_Internal_m48F806E4D712E54817810D1394747A69D1BD623A,
	PhysicsScene2D_OverlapCircle_m204738F4E5D2E9923E1671E5E15750A7641F7950_AdjustorThunk,
	PhysicsScene2D_OverlapCircleArray_Internal_mB9277F0D2860FB4D2E1734CB01460BA48937397F,
	PhysicsScene2D_Raycast_Internal_Injected_m197B563F302D9E7C336EE7BB0A356F6785F1584A,
	PhysicsScene2D_RaycastArray_Internal_Injected_mC5FDF82692390ECAB17CF821D25349A66B3C8143,
	PhysicsScene2D_RaycastList_Internal_Injected_mD8495122B2F8BD1194E83CA20DFC005D414C707B,
	PhysicsScene2D_GetRayIntersectionArray_Internal_Injected_m74194745127DA849411A2191EE7C52EB07BB21A9,
	PhysicsScene2D_OverlapPoint_Internal_Injected_mF130C9EAF9101517F47E9D48EAA794F4825CF029,
	PhysicsScene2D_OverlapPointArray_Internal_Injected_m8BB576977982188BF9B57A7023B015D59C67F642,
	PhysicsScene2D_OverlapCircleArray_Internal_Injected_mC8CA0F2C5DE4CD6B39A56011400C63F2958E5674,
	Physics2D_get_defaultPhysicsScene_m2C9DA4DFAFB71332EC48E50CCB16275441CADE84,
	Physics2D_get_queriesHitTriggers_m8BB98B1754A86777B4D58A4F28F63E8EC77B031B,
	Physics2D_Raycast_mD22D6BC52ACAB22598A720525B3840C019842FFC,
	Physics2D_Raycast_m468BF2D74BED92728533EA2108830C44ED93A0EF,
	Physics2D_Raycast_mBEB66E9AA034BD0AE1B1C99DF872247B0131CBDD,
	Physics2D_Raycast_mB43742B1077F487D1458388C5B11EE46D73533C0,
	Physics2D_Raycast_m4803AD692674FEE7EE269A6170AD5CEFEA6D3D78,
	Physics2D_Raycast_m0C22B1CACFA7E2A16D731B6E2D9D2ABC0666CCCE,
	Physics2D_Raycast_m8678AB161A71C09D7606299D194A90BA814BA543,
	Physics2D_Raycast_m940284F559A12F0594CF6E1A20583F7EA67E8645,
	Physics2D_RaycastNonAlloc_m1F45E84C0D9E8E71537A464C15C0BCB557114874,
	Physics2D_RaycastNonAlloc_m94BB67047043AF11DA7134882A71611C75B0CBE5,
	Physics2D_RaycastNonAlloc_m2571094B334BA2DF1AB1F83B3DD1CA82687A93FB,
	Physics2D_RaycastNonAlloc_mD9A24B7936572AB37514F15CC6442129B439BC21,
	Physics2D_GetRayIntersectionAll_mBD650C3EA6E692CE3E1255B6EAADF659307012D8,
	Physics2D_GetRayIntersectionAll_mACC24DD73E1388C1DF86847390B89A5B0223F03A,
	Physics2D_GetRayIntersectionAll_m04BCAB03333B049C48BE61036E512E12A5FBD053,
	Physics2D_GetRayIntersectionAll_Internal_m4E68866CF4A79A58DBF4B8A355D3EEE62BEF6612,
	Physics2D_GetRayIntersectionNonAlloc_m5F1AF31EEB67FE97AD2C40C102914371C2E825F0,
	Physics2D_GetRayIntersectionNonAlloc_m1A638894F08E9F401C7161D02171805B4897B51E,
	Physics2D_GetRayIntersectionNonAlloc_m3817EA2CC7B95C89683ACE0E433D6D4C6735CA0A,
	Physics2D_OverlapPoint_m8085D4E5C284AB1550C5AF62DF33F6DD64E64F4F,
	Physics2D_OverlapPointNonAlloc_mEC65BD8ADAA15BC39520011ED1AC32ACA0EB0CFE,
	Physics2D_OverlapCircleAll_mBFBC7E35BEE5A16F6AB7184BF7302A6DBB9278BB,
	Physics2D_OverlapCircleAll_Internal_m0542CFE7F662C028FFCD91A68AF5907F963AC889,
	Physics2D_OverlapCircleNonAlloc_m2346A2F01D5596A6242C0D3F0DA10C38A6594886,
	Physics2D__cctor_mC0D622F2EAF13BF0513DB2969E50EEC5631CDBFC,
	Physics2D_GetRayIntersectionAll_Internal_Injected_m8B627D4448B34665FC8BCF560EE851152FE2D15A,
	Physics2D_OverlapCircleAll_Internal_Injected_mEFD991E51C0EC3EB5BD1BF01E06E98E115E62398,
	ContactFilter2D_CheckConsistency_m0E1FC7D646C418F545F778197348F97ADA5409A2_AdjustorThunk,
	ContactFilter2D_SetLayerMask_mECEE981A09393F1097555D46449ED7CA4D8659E6_AdjustorThunk,
	ContactFilter2D_SetDepth_mF4AB9C380EDC3726D58734010BD90AD7D36ABDB0_AdjustorThunk,
	ContactFilter2D_CreateLegacyFilter_mA52A1C54BA7C4A49094B172BE7FA6044EF346A51,
	ContactFilter2D_CheckConsistency_Injected_m4640AA8896FEFE90396C5B47C4FE07930DA918BE,
	RaycastHit2D_get_point_mC567E234B1B673C3A9819023C3DC97C781443098_AdjustorThunk,
	RaycastHit2D_get_normal_m9F0974E4514AD56C00FCF6FF4CDF10AED62FE6E4_AdjustorThunk,
	RaycastHit2D_get_distance_m2D9F391717ECACFDA8E01A4126E0F8F59F7E774F_AdjustorThunk,
	RaycastHit2D_get_collider_m6A7EC53B2E179C2EFF4F29018A132B2979CBE976_AdjustorThunk,
	RaycastHit2D_get_rigidbody_mE6D43BB62B41529F1FF3A66615C571B418662F47_AdjustorThunk,
	RaycastHit2D_get_transform_m9AD59D59FC4F85128F139C0DE9B0A2AE27D75DAC_AdjustorThunk,
	Rigidbody2D_get_position_m68CB3236D19D7472ABDE1F5A5A9BD924595361B8,
	Rigidbody2D_set_position_m33DABE99FD458EEA8807FCC48F99AAFFCFECD60F,
	Rigidbody2D_get_rotation_mAF8F2E151EF82D8CF48DEDC17FE6882C2A67AF5C,
	Rigidbody2D_set_rotation_m0945A4D66B39C6698EFBF73F2A95969C976A858B,
	Rigidbody2D_MoveRotation_m657F4C682EF73C115151B5442B0AD415C09F130A,
	Rigidbody2D_MoveRotation_Angle_mC3124E587CD7E3524A1FFE8C82808D73BD013EF4,
	Rigidbody2D_get_velocity_m5ABF36BDF90FD7308BE608667B9E8F3DA5A207F1,
	Rigidbody2D_set_velocity_mE0DBCE5B683024B106C2AB6943BBA550B5BD0B83,
	Rigidbody2D_get_angularVelocity_m98681305F4188D29DA152F508BE7B73260CCEE46,
	Rigidbody2D_set_angularVelocity_mEA0807B27FFA3A839397575C2696F6B15693C599,
	Rigidbody2D_get_mass_mD217EC45743AB52C6555287A0FF765BC7F6002E9,
	Rigidbody2D_set_mass_m36294E719B842A9CEF10925A77A822BE47B669B3,
	Rigidbody2D_set_bodyType_m239CDB6FFA033FD3B5BAC061A3F96DC9264D9900,
	Rigidbody2D_set_isKinematic_mA7711684E1E1E25FA7C1A1FF297B6E45DFD03BEE,
	Rigidbody2D_IsSleeping_mF480444AF1AF7F57463437CAB1607F100AB05D70,
	Rigidbody2D_Sleep_m79A8750BA2160738B63771EBDA27F63272B349F3,
	Rigidbody2D_WakeUp_m50C62CDD1DC46A9D27C11E9013DFF5FE37D6EC23,
	Rigidbody2D__ctor_mEADC6DF3297B27BF8DAE647A5D3384CA80E005FB,
	Rigidbody2D_get_position_Injected_m7684DCAD6874A8D0A8560A052FEF85EF48280F73,
	Rigidbody2D_set_position_Injected_m1C9E1C0F87779A134997F60E761CB23149B3C231,
	Rigidbody2D_get_velocity_Injected_mFB508815764F2895389C2B75002D3D7A33630B3C,
	Rigidbody2D_set_velocity_Injected_m8DCAEDCB8C0165DCEC87B70089C7B4EA41FEB73E,
	Collider2D_set_isTrigger_m15514794FC3D677B712080672EAEEA0CE2C3A20B,
	Collider2D_get_offset_mCB3DEFB9ACB05211320B8406B01F089EF7F8788D,
	Collider2D_set_offset_mC752F6CA4C47C5543538BA3BC9B04A3D267AAC6C,
	Collider2D_get_attachedRigidbody_m7BBA6D4F834B78D334349066540EA4DDDE4F0308,
	Collider2D__ctor_m9E497E4A527D5ADD864AB3D7901A4087C45712C0,
	Collider2D_get_offset_Injected_mFBEEA538206330D2D4C62A04DF339332596C0F97,
	Collider2D_set_offset_Injected_m98CB990275094E1261101B082C51DDD634CA0A79,
	BoxCollider2D_get_size_m6230015317115D9BED5C61A4EDAC013C8A7664E1,
	BoxCollider2D_set_size_mFA630C4BD41F786D208C1C0382A7FCF7868BA489,
	BoxCollider2D_get_size_Injected_m6E18F627969D38CF513DB4CF680ACD51810F74CD,
	BoxCollider2D_set_size_Injected_mB787996D8E2E7919FE975BC435677115539A53A0,
	PolygonCollider2D_GetTotalPointCount_m3DB7B6E36C685F81F4A7DC503DD93E8F32347877,
	PolygonCollider2D_get_points_m65A26A2D193E9F2C0D517635B46190FF8DBD9AF2,
	PolygonCollider2D_SetPath_mE0B11473C90B4F848A146DD08DC5EC23004E99CA,
	PolygonCollider2D_SetPath_Internal_m74F981E2C411B7DA32620FD26AB792843FDA2843,
};
static const int32_t s_InvokerIndices[104] = 
{
	14,
	10,
	9,
	1516,
	1517,
	1518,
	1519,
	1520,
	1521,
	1522,
	1521,
	1522,
	1523,
	1524,
	1525,
	1526,
	1527,
	1528,
	1529,
	1530,
	1531,
	1532,
	1532,
	1533,
	1534,
	1535,
	1536,
	1537,
	49,
	1538,
	1539,
	1540,
	1541,
	1542,
	1543,
	1544,
	1544,
	1545,
	1546,
	1547,
	1548,
	1377,
	1376,
	1375,
	1549,
	1381,
	1380,
	1379,
	1550,
	1551,
	1552,
	1553,
	1554,
	3,
	1555,
	1556,
	23,
	1557,
	1024,
	1558,
	17,
	1060,
	1060,
	664,
	14,
	14,
	14,
	1060,
	1083,
	664,
	275,
	275,
	275,
	1060,
	1083,
	664,
	275,
	664,
	275,
	32,
	31,
	102,
	23,
	23,
	23,
	6,
	6,
	6,
	6,
	31,
	1060,
	1083,
	14,
	23,
	6,
	6,
	1060,
	1083,
	6,
	6,
	10,
	14,
	62,
	62,
};
extern const Il2CppCodeGenModule g_UnityEngine_Physics2DModuleCodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_Physics2DModuleCodeGenModule = 
{
	"UnityEngine.Physics2DModule.dll",
	104,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
