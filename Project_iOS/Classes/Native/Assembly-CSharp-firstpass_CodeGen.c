﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void Skeleton::Start()
extern void Skeleton_Start_mF4E67DE657C4EA481F8E38A5BA18BF52B226F6EC ();
// 0x00000002 System.Void Skeleton::OnRebuild()
extern void Skeleton_OnRebuild_mD8D147125009BEE13E1E6FFBE68692CC5C20DE5C ();
// 0x00000003 System.Void Skeleton::OnMuscleRemoved(RootMotion.Dynamics.Muscle)
extern void Skeleton_OnMuscleRemoved_m90238A8A69AFE3ADE235B0956ABF5138C3224A6F ();
// 0x00000004 System.Boolean Skeleton::IsLegMuscle(RootMotion.Dynamics.Muscle,System.Boolean&)
extern void Skeleton_IsLegMuscle_mCD1014B54FB52FFB98CD62E55553B85FBDBC5E38 ();
// 0x00000005 System.Void Skeleton::.ctor()
extern void Skeleton__ctor_mDBB38CDFBB1D92AD96FE2BFF83B53A9D4F468558 ();
// 0x00000006 System.Single RootMotion.CameraController::get_x()
extern void CameraController_get_x_mB38627ACD13A0433C003B2CA9CB00CEF9C73ACAD ();
// 0x00000007 System.Void RootMotion.CameraController::set_x(System.Single)
extern void CameraController_set_x_mDDAC65936DA7CEA5156B0EC3677466E79FA1DA95 ();
// 0x00000008 System.Single RootMotion.CameraController::get_y()
extern void CameraController_get_y_m35304C2395EDD49D66639E528C2DC9386F3B0F8D ();
// 0x00000009 System.Void RootMotion.CameraController::set_y(System.Single)
extern void CameraController_set_y_mD0222EE6DAF94275CC7F21D72635AAE7F0EB12D2 ();
// 0x0000000A System.Single RootMotion.CameraController::get_distanceTarget()
extern void CameraController_get_distanceTarget_mD806C0E22859D79334CA9A59C2C24C4C4373CE86 ();
// 0x0000000B System.Void RootMotion.CameraController::set_distanceTarget(System.Single)
extern void CameraController_set_distanceTarget_mF07AC56B1302E4F1E28422CC82E70EF857FDD320 ();
// 0x0000000C System.Void RootMotion.CameraController::Awake()
extern void CameraController_Awake_m5FBC56193C89322F8E2391B8A5DA36B3718A46B0 ();
// 0x0000000D System.Void RootMotion.CameraController::Update()
extern void CameraController_Update_mDD7D1D87264444B69E033F7E0E54AA997002DF4E ();
// 0x0000000E System.Void RootMotion.CameraController::FixedUpdate()
extern void CameraController_FixedUpdate_mE6B3E2617546629D9EF05F9D810EA03CF14A4061 ();
// 0x0000000F System.Void RootMotion.CameraController::LateUpdate()
extern void CameraController_LateUpdate_m08AE76EBCA0760E4C39CBB75CE1A4061E472EED5 ();
// 0x00000010 System.Void RootMotion.CameraController::UpdateInput()
extern void CameraController_UpdateInput_mF33A7394E88D096EF0AF7AB9CEB77CE0B06515EB ();
// 0x00000011 System.Void RootMotion.CameraController::UpdateTransform()
extern void CameraController_UpdateTransform_m9D84649F399911F1B0378D7C4A27101A8B824F46 ();
// 0x00000012 System.Void RootMotion.CameraController::UpdateTransform(System.Single)
extern void CameraController_UpdateTransform_mE951741E10350226C58AB57C7291A9FDEAD8CAB6 ();
// 0x00000013 System.Single RootMotion.CameraController::get_zoomAdd()
extern void CameraController_get_zoomAdd_m776E555CB2660685855ACCA83A289609B2DD8965 ();
// 0x00000014 System.Single RootMotion.CameraController::ClampAngle(System.Single,System.Single,System.Single)
extern void CameraController_ClampAngle_m6F19803B13D07E13BE330D65D6194193AAEB2264 ();
// 0x00000015 System.Void RootMotion.CameraController::.ctor()
extern void CameraController__ctor_mBEE86142FB97255F9249E3508D7273541996728A ();
// 0x00000016 System.Void RootMotion.CameraControllerFPS::Awake()
extern void CameraControllerFPS_Awake_mC31E44E1175C7C6610825E1114DB77985B012363 ();
// 0x00000017 System.Void RootMotion.CameraControllerFPS::LateUpdate()
extern void CameraControllerFPS_LateUpdate_mAF198EAEE545135331BF4FE2CCB2C6175363D883 ();
// 0x00000018 System.Single RootMotion.CameraControllerFPS::ClampAngle(System.Single,System.Single,System.Single)
extern void CameraControllerFPS_ClampAngle_m922CBC59F48965F6AB58839ED060E59181871D8B ();
// 0x00000019 System.Void RootMotion.CameraControllerFPS::.ctor()
extern void CameraControllerFPS__ctor_m11BB35374105CF5D83667BCC71759C870B54BD71 ();
// 0x0000001A UnityEngine.Vector3 RootMotion.AxisTools::ToVector3(RootMotion.Axis)
extern void AxisTools_ToVector3_mCD503009B0780195B310046A0997E22909E6B383 ();
// 0x0000001B RootMotion.Axis RootMotion.AxisTools::ToAxis(UnityEngine.Vector3)
extern void AxisTools_ToAxis_mA2CAD6CF2A7860002286BC038CA81B1165D47F22 ();
// 0x0000001C RootMotion.Axis RootMotion.AxisTools::GetAxisToPoint(UnityEngine.Transform,UnityEngine.Vector3)
extern void AxisTools_GetAxisToPoint_mC9798C7443E866FD0ED4CB5A26DE289FD87B73DD ();
// 0x0000001D RootMotion.Axis RootMotion.AxisTools::GetAxisToDirection(UnityEngine.Transform,UnityEngine.Vector3)
extern void AxisTools_GetAxisToDirection_m2E6F2E35366E46C9171B91C594500F54BD52F0DD ();
// 0x0000001E UnityEngine.Vector3 RootMotion.AxisTools::GetAxisVectorToPoint(UnityEngine.Transform,UnityEngine.Vector3)
extern void AxisTools_GetAxisVectorToPoint_mFBFFFE2DF1D8FD4FCB9A5F133BDD21E222E0A1F7 ();
// 0x0000001F UnityEngine.Vector3 RootMotion.AxisTools::GetAxisVectorToDirection(UnityEngine.Transform,UnityEngine.Vector3)
extern void AxisTools_GetAxisVectorToDirection_m0E72B70F2316F9732F5793FB5DF2D3E93A49DD3C ();
// 0x00000020 System.Void RootMotion.AxisTools::.ctor()
extern void AxisTools__ctor_m856184BD6721EE6094162BF0105E4A4C8288666B ();
// 0x00000021 System.Void RootMotion.BipedLimbOrientations::.ctor(RootMotion.BipedLimbOrientations_LimbOrientation,RootMotion.BipedLimbOrientations_LimbOrientation,RootMotion.BipedLimbOrientations_LimbOrientation,RootMotion.BipedLimbOrientations_LimbOrientation)
extern void BipedLimbOrientations__ctor_m628014D35803DAB72046E4FAC5476B8DB5248D31 ();
// 0x00000022 RootMotion.BipedLimbOrientations RootMotion.BipedLimbOrientations::get_UMA()
extern void BipedLimbOrientations_get_UMA_m1B4A8EF3D56D04D43582CA0056F77208CF3F70E9 ();
// 0x00000023 RootMotion.BipedLimbOrientations RootMotion.BipedLimbOrientations::get_MaxBiped()
extern void BipedLimbOrientations_get_MaxBiped_m878607BA4C487DFF0773656452493C0636B10555 ();
// 0x00000024 UnityEngine.Transform[] RootMotion.BipedNaming::GetBonesOfType(RootMotion.BipedNaming_BoneType,UnityEngine.Transform[])
extern void BipedNaming_GetBonesOfType_mBDC17B558CA273758E18C4422AC1988E6FC98C18 ();
// 0x00000025 UnityEngine.Transform[] RootMotion.BipedNaming::GetBonesOfSide(RootMotion.BipedNaming_BoneSide,UnityEngine.Transform[])
extern void BipedNaming_GetBonesOfSide_m18E08316FB58D81997B8075717DCE82AF424551B ();
// 0x00000026 UnityEngine.Transform[] RootMotion.BipedNaming::GetBonesOfTypeAndSide(RootMotion.BipedNaming_BoneType,RootMotion.BipedNaming_BoneSide,UnityEngine.Transform[])
extern void BipedNaming_GetBonesOfTypeAndSide_mC4AA07963100FE3DCBACB470D8A2D5326C624DE1 ();
// 0x00000027 UnityEngine.Transform RootMotion.BipedNaming::GetFirstBoneOfTypeAndSide(RootMotion.BipedNaming_BoneType,RootMotion.BipedNaming_BoneSide,UnityEngine.Transform[])
extern void BipedNaming_GetFirstBoneOfTypeAndSide_m37ED9E3AB05C0027CB2CBF89A924496F4E858E5A ();
// 0x00000028 UnityEngine.Transform RootMotion.BipedNaming::GetNamingMatch(UnityEngine.Transform[],System.String[][])
extern void BipedNaming_GetNamingMatch_m07DCBB41BA4F914D3BFFF4EAE5CC54C4CA8BC45A ();
// 0x00000029 RootMotion.BipedNaming_BoneType RootMotion.BipedNaming::GetBoneType(System.String)
extern void BipedNaming_GetBoneType_m6ECDE309F7EC61C6B96AD29FDDB4B23CF6ADD7AA ();
// 0x0000002A RootMotion.BipedNaming_BoneSide RootMotion.BipedNaming::GetBoneSide(System.String)
extern void BipedNaming_GetBoneSide_m416D27D838BF78EAF90E6468A3346F886F7922EE ();
// 0x0000002B UnityEngine.Transform RootMotion.BipedNaming::GetBone(UnityEngine.Transform[],RootMotion.BipedNaming_BoneType,RootMotion.BipedNaming_BoneSide,System.String[][])
extern void BipedNaming_GetBone_mF8EE1CF09589C2486CE0670DFF8C6F5A003CFB23 ();
// 0x0000002C System.Boolean RootMotion.BipedNaming::isLeft(System.String)
extern void BipedNaming_isLeft_mFEE12954060CA56C42287319F51828E652095D80 ();
// 0x0000002D System.Boolean RootMotion.BipedNaming::isRight(System.String)
extern void BipedNaming_isRight_m279D139974DCFD68EB6F0E666F88850E2CE22C60 ();
// 0x0000002E System.Boolean RootMotion.BipedNaming::isSpine(System.String)
extern void BipedNaming_isSpine_m108D9FF35A112C6418A1189888877D3350E6DB7B ();
// 0x0000002F System.Boolean RootMotion.BipedNaming::isHead(System.String)
extern void BipedNaming_isHead_m977CDD1DE2D5E42F6770047421677F4995F790C9 ();
// 0x00000030 System.Boolean RootMotion.BipedNaming::isArm(System.String)
extern void BipedNaming_isArm_m4401AD291EEDA15834D0D0D64DDD990824D69E4F ();
// 0x00000031 System.Boolean RootMotion.BipedNaming::isLeg(System.String)
extern void BipedNaming_isLeg_m3CAD732B60831C842EE9E1D9D07D5CF678D7937F ();
// 0x00000032 System.Boolean RootMotion.BipedNaming::isTail(System.String)
extern void BipedNaming_isTail_m59197C34830C9D0142A150FFFC2B235FE24C5033 ();
// 0x00000033 System.Boolean RootMotion.BipedNaming::isEye(System.String)
extern void BipedNaming_isEye_m1F65D1E12C1C82BA3341C152C535A6CD857D1F84 ();
// 0x00000034 System.Boolean RootMotion.BipedNaming::isTypeExclude(System.String)
extern void BipedNaming_isTypeExclude_m2E7BD947E61BEB518E29BE7748A47224A2BB3644 ();
// 0x00000035 System.Boolean RootMotion.BipedNaming::matchesNaming(System.String,System.String[])
extern void BipedNaming_matchesNaming_m457322B452EB97308839E8202735115D4F202406 ();
// 0x00000036 System.Boolean RootMotion.BipedNaming::excludesNaming(System.String,System.String[])
extern void BipedNaming_excludesNaming_mF890A0D56DFEF3A3FB083DE2CEEB7547F0B28CD9 ();
// 0x00000037 System.Boolean RootMotion.BipedNaming::matchesLastLetter(System.String,System.String[])
extern void BipedNaming_matchesLastLetter_mECAD3C3FE434A94420E73882B1203185A0FA70C6 ();
// 0x00000038 System.Boolean RootMotion.BipedNaming::LastLetterIs(System.String,System.String)
extern void BipedNaming_LastLetterIs_m0A633F180E882DE90E7FDCD55542131A703141EF ();
// 0x00000039 System.String RootMotion.BipedNaming::firstLetter(System.String)
extern void BipedNaming_firstLetter_mA04DC15B9CC358F32EB08711CD7B7C543E878418 ();
// 0x0000003A System.String RootMotion.BipedNaming::lastLetter(System.String)
extern void BipedNaming_lastLetter_m6309E0A4ADFF5A6256A48B63FFA0C96C216FF212 ();
// 0x0000003B System.Void RootMotion.BipedNaming::.cctor()
extern void BipedNaming__cctor_mB787E279939F32302988A01537088F13830A192B ();
// 0x0000003C System.Boolean RootMotion.BipedReferences::get_isFilled()
extern void BipedReferences_get_isFilled_m532616CFF9B9F487D198B337C4E2CFC97668B5DA ();
// 0x0000003D System.Boolean RootMotion.BipedReferences::get_isEmpty()
extern void BipedReferences_get_isEmpty_m37BC8894320B138293100AAB2110819B5EEED103 ();
// 0x0000003E System.Boolean RootMotion.BipedReferences::IsEmpty(System.Boolean)
extern void BipedReferences_IsEmpty_mD2484F0388AF9B9D799BE45BFAF9BCE81A2498FF ();
// 0x0000003F System.Boolean RootMotion.BipedReferences::Contains(UnityEngine.Transform,System.Boolean)
extern void BipedReferences_Contains_m00FC8E73E8D1E6855AA5DA3AD098CD2421CDEDF8 ();
// 0x00000040 System.Boolean RootMotion.BipedReferences::AutoDetectReferences(RootMotion.BipedReferences&,UnityEngine.Transform,RootMotion.BipedReferences_AutoDetectParams)
extern void BipedReferences_AutoDetectReferences_m480B4137C8758F57831AF089036EE0A3A67A99D5 ();
// 0x00000041 System.Void RootMotion.BipedReferences::DetectReferencesByNaming(RootMotion.BipedReferences&,UnityEngine.Transform,RootMotion.BipedReferences_AutoDetectParams)
extern void BipedReferences_DetectReferencesByNaming_mC4AA23322AA015044E090C711B99F0538DDF8879 ();
// 0x00000042 System.Void RootMotion.BipedReferences::AssignHumanoidReferences(RootMotion.BipedReferences&,UnityEngine.Animator,RootMotion.BipedReferences_AutoDetectParams)
extern void BipedReferences_AssignHumanoidReferences_mD2020E45A8C9FE92D05E75E8456799489F1CBA1F ();
// 0x00000043 System.Boolean RootMotion.BipedReferences::SetupError(RootMotion.BipedReferences,System.String&)
extern void BipedReferences_SetupError_mBBA8543B0CE84E5D5C72E9EE3550ADE1A8957B1C ();
// 0x00000044 System.Boolean RootMotion.BipedReferences::SetupWarning(RootMotion.BipedReferences,System.String&)
extern void BipedReferences_SetupWarning_mB5A044E4AAC480E11CBAC069ED985226204AD67C ();
// 0x00000045 System.Boolean RootMotion.BipedReferences::IsNeckBone(UnityEngine.Transform,UnityEngine.Transform)
extern void BipedReferences_IsNeckBone_mAD641572662F55A3BFA9200CEF2C49C1C04611C3 ();
// 0x00000046 System.Boolean RootMotion.BipedReferences::AddBoneToEyes(UnityEngine.Transform,RootMotion.BipedReferences&,RootMotion.BipedReferences_AutoDetectParams)
extern void BipedReferences_AddBoneToEyes_mDB9C8D26AA6D2E371B12117A99881E27DC84B21A ();
// 0x00000047 System.Boolean RootMotion.BipedReferences::AddBoneToSpine(UnityEngine.Transform,RootMotion.BipedReferences&,RootMotion.BipedReferences_AutoDetectParams)
extern void BipedReferences_AddBoneToSpine_mC2C9AAB83D61FF52580C53993577533DB6FA3375 ();
// 0x00000048 System.Void RootMotion.BipedReferences::DetectLimb(RootMotion.BipedNaming_BoneType,RootMotion.BipedNaming_BoneSide,UnityEngine.Transform&,UnityEngine.Transform&,UnityEngine.Transform&,UnityEngine.Transform[])
extern void BipedReferences_DetectLimb_m7998E2F989BF07D7ECD91DE4AC98C7C88F6FD04B ();
// 0x00000049 System.Void RootMotion.BipedReferences::AddBoneToHierarchy(UnityEngine.Transform[]&,UnityEngine.Transform)
extern void BipedReferences_AddBoneToHierarchy_mF4BB305D8BC48B162251E573DCB29E720687355A ();
// 0x0000004A System.Boolean RootMotion.BipedReferences::LimbError(UnityEngine.Transform,UnityEngine.Transform,UnityEngine.Transform,System.String&)
extern void BipedReferences_LimbError_m961DEF3BDA371552C207A5C7061B46334910F96A ();
// 0x0000004B System.Boolean RootMotion.BipedReferences::LimbWarning(UnityEngine.Transform,UnityEngine.Transform,UnityEngine.Transform,System.String&)
extern void BipedReferences_LimbWarning_m8883041D57B3E96ADB064B5256EC41EF80C3364D ();
// 0x0000004C System.Boolean RootMotion.BipedReferences::SpineError(RootMotion.BipedReferences,System.String&)
extern void BipedReferences_SpineError_m8124177406CACA5A9C2C35C2E218FF5C5DAC768C ();
// 0x0000004D System.Boolean RootMotion.BipedReferences::SpineWarning(RootMotion.BipedReferences,System.String&)
extern void BipedReferences_SpineWarning_mAB17505D98D0625A851178E35EE2181982B03DE4 ();
// 0x0000004E System.Boolean RootMotion.BipedReferences::EyesError(RootMotion.BipedReferences,System.String&)
extern void BipedReferences_EyesError_m6B621EAAA5C86D1C20C575FFEFDBB1FE9680C602 ();
// 0x0000004F System.Boolean RootMotion.BipedReferences::EyesWarning(RootMotion.BipedReferences,System.String&)
extern void BipedReferences_EyesWarning_m0597A26ABEEF3864B1D45767FD53AF6C315F6348 ();
// 0x00000050 System.Boolean RootMotion.BipedReferences::RootHeightWarning(RootMotion.BipedReferences,System.String&)
extern void BipedReferences_RootHeightWarning_m4F63F8A0697B363761CBF6C2C01371DE7F089A9E ();
// 0x00000051 System.Boolean RootMotion.BipedReferences::FacingAxisWarning(RootMotion.BipedReferences,System.String&)
extern void BipedReferences_FacingAxisWarning_m24D82893DEA747A31AB498537AE7B68E8DC48E36 ();
// 0x00000052 System.Single RootMotion.BipedReferences::GetVerticalOffset(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Quaternion)
extern void BipedReferences_GetVerticalOffset_m6EC5AA10A16C757A4601261BCE1858C1A965BD0A ();
// 0x00000053 System.Void RootMotion.BipedReferences::.ctor()
extern void BipedReferences__ctor_m22C81763C6BCBEB21D611DCFBF6417E74E374258 ();
// 0x00000054 System.Void RootMotion.Comments::.ctor()
extern void Comments__ctor_mC318D7C889BD34F9720479808DE974A219C708BA ();
// 0x00000055 System.Void RootMotion.DemoGUIMessage::OnGUI()
extern void DemoGUIMessage_OnGUI_m3CA388F7FAA8DABF4C4F3B39D8FAF7C28B51F424 ();
// 0x00000056 System.Void RootMotion.DemoGUIMessage::.ctor()
extern void DemoGUIMessage__ctor_m9C4AE190B3977074C3B65350952C95E51FD8CEC8 ();
// 0x00000057 System.Boolean RootMotion.Hierarchy::HierarchyIsValid(UnityEngine.Transform[])
extern void Hierarchy_HierarchyIsValid_m98AF5CA108529E03914102F181875DF6789CA4D6 ();
// 0x00000058 UnityEngine.Object RootMotion.Hierarchy::ContainsDuplicate(UnityEngine.Object[])
extern void Hierarchy_ContainsDuplicate_mF0EA7DDF773AEAD13C2DC2F18982C225878DAF93 ();
// 0x00000059 System.Boolean RootMotion.Hierarchy::IsAncestor(UnityEngine.Transform,UnityEngine.Transform)
extern void Hierarchy_IsAncestor_m749A3F4684360F7A093751B48CEA48A112ADCC8E ();
// 0x0000005A System.Boolean RootMotion.Hierarchy::ContainsChild(UnityEngine.Transform,UnityEngine.Transform)
extern void Hierarchy_ContainsChild_m4DD0E4A39956016A9C23EB196688D9B06C2EAE0A ();
// 0x0000005B System.Void RootMotion.Hierarchy::AddAncestors(UnityEngine.Transform,UnityEngine.Transform,UnityEngine.Transform[]&)
extern void Hierarchy_AddAncestors_m9E0BE7DA6055DACFCD61527F9E7348100E0CFF9D ();
// 0x0000005C UnityEngine.Transform RootMotion.Hierarchy::GetAncestor(UnityEngine.Transform,System.Int32)
extern void Hierarchy_GetAncestor_m8595FF4227653B0C249BFC57C1E180C4872835F9 ();
// 0x0000005D UnityEngine.Transform RootMotion.Hierarchy::GetFirstCommonAncestor(UnityEngine.Transform,UnityEngine.Transform)
extern void Hierarchy_GetFirstCommonAncestor_m4B1387FB35547ECA3856D79665D088913121C383 ();
// 0x0000005E UnityEngine.Transform RootMotion.Hierarchy::GetFirstCommonAncestor(UnityEngine.Transform[])
extern void Hierarchy_GetFirstCommonAncestor_m0A8BCACB0C4EB1A8DD1E8FAAC40D236FC16203CF ();
// 0x0000005F UnityEngine.Transform RootMotion.Hierarchy::GetFirstCommonAncestorRecursive(UnityEngine.Transform,UnityEngine.Transform[])
extern void Hierarchy_GetFirstCommonAncestorRecursive_m962DE35E5208E0EA02D4B334777856F83839CBC6 ();
// 0x00000060 System.Boolean RootMotion.Hierarchy::IsCommonAncestor(UnityEngine.Transform,UnityEngine.Transform[])
extern void Hierarchy_IsCommonAncestor_m4B5549450455C71A57EA8A4191E07C3B5AAA1CF7 ();
// 0x00000061 System.Void RootMotion.Hierarchy::.ctor()
extern void Hierarchy__ctor_m08940D4DE4D2BA15C094FCE918D6CC043CA9C85A ();
// 0x00000062 System.Void RootMotion.InspectorComment::.ctor(System.String)
extern void InspectorComment__ctor_m36EF2C7214BF1CA6306E15CD56C4BF8BCBEC6F30 ();
// 0x00000063 System.Void RootMotion.InspectorComment::.ctor(System.String,System.String)
extern void InspectorComment__ctor_mAE8A93B7E88F9F6AB5B56A36DC710A530D977E34 ();
// 0x00000064 System.Single RootMotion.Interp::Float(System.Single,RootMotion.InterpolationMode)
extern void Interp_Float_m8315388009920ADB90310DEA6EC6A48F8A596035 ();
// 0x00000065 UnityEngine.Vector3 RootMotion.Interp::V3(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,RootMotion.InterpolationMode)
extern void Interp_V3_mD52ECED9A462AA2139A1E514B0B8993849C93A62 ();
// 0x00000066 System.Single RootMotion.Interp::LerpValue(System.Single,System.Single,System.Single,System.Single)
extern void Interp_LerpValue_m55BF212FA996148F68F5884AA0D0DC2EFE3429B2 ();
// 0x00000067 System.Single RootMotion.Interp::None(System.Single,System.Single,System.Single)
extern void Interp_None_m62275D882F59BC2EF6FE267544FD6428AD53D778 ();
// 0x00000068 System.Single RootMotion.Interp::InOutCubic(System.Single,System.Single,System.Single)
extern void Interp_InOutCubic_m9234832B9839C5EC19F5F83D1BA47D550E0298B6 ();
// 0x00000069 System.Single RootMotion.Interp::InOutQuintic(System.Single,System.Single,System.Single)
extern void Interp_InOutQuintic_m430A945746907AF2C321F1792E51C7ACF7E66B40 ();
// 0x0000006A System.Single RootMotion.Interp::InQuintic(System.Single,System.Single,System.Single)
extern void Interp_InQuintic_m4808AB9E20C94C2D9C9D6557CE4A2555B9AC8970 ();
// 0x0000006B System.Single RootMotion.Interp::InQuartic(System.Single,System.Single,System.Single)
extern void Interp_InQuartic_m458ED4544667AD67B868A1F2EFE9C50ED066CD10 ();
// 0x0000006C System.Single RootMotion.Interp::InCubic(System.Single,System.Single,System.Single)
extern void Interp_InCubic_mE1594EF4BB7685BE8A0FBCA6CD6596C5AFDE0153 ();
// 0x0000006D System.Single RootMotion.Interp::InQuadratic(System.Single,System.Single,System.Single)
extern void Interp_InQuadratic_mFD41A4AD631DB7952962A23774713CD4C97B72C8 ();
// 0x0000006E System.Single RootMotion.Interp::OutQuintic(System.Single,System.Single,System.Single)
extern void Interp_OutQuintic_mBA4F699F0566FE2F8BF5830B60887633A5025880 ();
// 0x0000006F System.Single RootMotion.Interp::OutQuartic(System.Single,System.Single,System.Single)
extern void Interp_OutQuartic_m186A3354FA77C8EE50BAD305FDDDF2E19C20F846 ();
// 0x00000070 System.Single RootMotion.Interp::OutCubic(System.Single,System.Single,System.Single)
extern void Interp_OutCubic_mEF6C796EA66CAF998965778AE6F381B366FCDF90 ();
// 0x00000071 System.Single RootMotion.Interp::OutInCubic(System.Single,System.Single,System.Single)
extern void Interp_OutInCubic_m19389B61E0D7F6DBBC26893B1770690EEFF62EA0 ();
// 0x00000072 System.Single RootMotion.Interp::OutInQuartic(System.Single,System.Single,System.Single)
extern void Interp_OutInQuartic_m2801A8B9F2214F0DBCFA7560CE5C0755C889D2F8 ();
// 0x00000073 System.Single RootMotion.Interp::BackInCubic(System.Single,System.Single,System.Single)
extern void Interp_BackInCubic_mE00BE40371B334F077A484EBCCF9452BA8127136 ();
// 0x00000074 System.Single RootMotion.Interp::BackInQuartic(System.Single,System.Single,System.Single)
extern void Interp_BackInQuartic_m87D76B3B89DB7538099BF034870F6A11337FBC1A ();
// 0x00000075 System.Single RootMotion.Interp::OutBackCubic(System.Single,System.Single,System.Single)
extern void Interp_OutBackCubic_mD5A333EDB3BA5E08B8C3491FB65CD435162DB89F ();
// 0x00000076 System.Single RootMotion.Interp::OutBackQuartic(System.Single,System.Single,System.Single)
extern void Interp_OutBackQuartic_mE6FA382DD1E9B5E1EEC4B8C58928B2CA0E4C676D ();
// 0x00000077 System.Single RootMotion.Interp::OutElasticSmall(System.Single,System.Single,System.Single)
extern void Interp_OutElasticSmall_mE28AE19A061B2C87727FF77F91481E09181FE697 ();
// 0x00000078 System.Single RootMotion.Interp::OutElasticBig(System.Single,System.Single,System.Single)
extern void Interp_OutElasticBig_m3B5DC741EE5FDBFF0DFAA9389374A5F2884692ED ();
// 0x00000079 System.Single RootMotion.Interp::InElasticSmall(System.Single,System.Single,System.Single)
extern void Interp_InElasticSmall_m452AB8C4F78F4D239F97E4E4D34BE14F026C7D82 ();
// 0x0000007A System.Single RootMotion.Interp::InElasticBig(System.Single,System.Single,System.Single)
extern void Interp_InElasticBig_m040CE5F05E73BA906F5AC0C03FD80A45C0DB061E ();
// 0x0000007B System.Single RootMotion.Interp::InSine(System.Single,System.Single,System.Single)
extern void Interp_InSine_mFA4D2EAB9F229F95344740DC1351E357DCC48007 ();
// 0x0000007C System.Single RootMotion.Interp::OutSine(System.Single,System.Single,System.Single)
extern void Interp_OutSine_mD40D8388BAB5E4AE1F286740388A9685B112DD9C ();
// 0x0000007D System.Single RootMotion.Interp::InOutSine(System.Single,System.Single,System.Single)
extern void Interp_InOutSine_mB010D9D50EF06D636E56253D6D36B29494EC2A21 ();
// 0x0000007E System.Single RootMotion.Interp::InElastic(System.Single,System.Single,System.Single)
extern void Interp_InElastic_mF213B6EF60418456D6D57C38A67B6B2D817A826B ();
// 0x0000007F System.Single RootMotion.Interp::OutElastic(System.Single,System.Single,System.Single)
extern void Interp_OutElastic_m38EC0CD8C33D07B8A5CBA52A6FF3F52F3FEE8AA2 ();
// 0x00000080 System.Single RootMotion.Interp::InBack(System.Single,System.Single,System.Single)
extern void Interp_InBack_mB9F3C5044904882DFE32BD58939B66CD91F681DB ();
// 0x00000081 System.Single RootMotion.Interp::OutBack(System.Single,System.Single,System.Single)
extern void Interp_OutBack_m859EE78507AC785355CE2E62D2913861F98E81C7 ();
// 0x00000082 System.Void RootMotion.Interp::.ctor()
extern void Interp__ctor_mE2D6F710C7DE6279DC65035463CCFF4611F7F497 ();
// 0x00000083 System.Void RootMotion.LargeHeader::.ctor(System.String)
extern void LargeHeader__ctor_m242EAA703F6345754944786FEBAC1536FF97A8E7 ();
// 0x00000084 System.Void RootMotion.LargeHeader::.ctor(System.String,System.String)
extern void LargeHeader__ctor_m1AFECED29C7D43C3A7E2A66A0D02DCD5DD162954 ();
// 0x00000085 System.Boolean RootMotion.LayerMaskExtensions::Contains(UnityEngine.LayerMask,System.Int32)
extern void LayerMaskExtensions_Contains_m9AA25A3336A1D6CCB3D78CF207310E988A1F968F ();
// 0x00000086 UnityEngine.LayerMask RootMotion.LayerMaskExtensions::Create(System.String[])
extern void LayerMaskExtensions_Create_m86A9DD5238E84499AD59F9CD4F87461773878993 ();
// 0x00000087 UnityEngine.LayerMask RootMotion.LayerMaskExtensions::Create(System.Int32[])
extern void LayerMaskExtensions_Create_mD56ECB38C3CB567F3852747CBF40680E44547471 ();
// 0x00000088 UnityEngine.LayerMask RootMotion.LayerMaskExtensions::NamesToMask(System.String[])
extern void LayerMaskExtensions_NamesToMask_mB6A159D6148E5DE247F35CAFA792B637A48E5AE9 ();
// 0x00000089 UnityEngine.LayerMask RootMotion.LayerMaskExtensions::LayerNumbersToMask(System.Int32[])
extern void LayerMaskExtensions_LayerNumbersToMask_m076C790344B579202E95813AB4B567683131E4C7 ();
// 0x0000008A UnityEngine.LayerMask RootMotion.LayerMaskExtensions::Inverse(UnityEngine.LayerMask)
extern void LayerMaskExtensions_Inverse_m9014247C9AEBF3E71D2298F0E6F4990987D91322 ();
// 0x0000008B UnityEngine.LayerMask RootMotion.LayerMaskExtensions::AddToMask(UnityEngine.LayerMask,System.String[])
extern void LayerMaskExtensions_AddToMask_mB8C61CA62DC235C390D7F0BC59A168DBD8E6D87E ();
// 0x0000008C UnityEngine.LayerMask RootMotion.LayerMaskExtensions::RemoveFromMask(UnityEngine.LayerMask,System.String[])
extern void LayerMaskExtensions_RemoveFromMask_m2647A6FDC8CCF34C96DA2E7E587AD4B315794816 ();
// 0x0000008D System.String[] RootMotion.LayerMaskExtensions::MaskToNames(UnityEngine.LayerMask)
extern void LayerMaskExtensions_MaskToNames_m2D9C04F2C3BA77FB12821060BA9AA9F77B9777DB ();
// 0x0000008E System.Int32[] RootMotion.LayerMaskExtensions::MaskToNumbers(UnityEngine.LayerMask)
extern void LayerMaskExtensions_MaskToNumbers_m8EC7DBEBD94817574AEB00ACA440FCF4B5F93CB4 ();
// 0x0000008F System.String RootMotion.LayerMaskExtensions::MaskToString(UnityEngine.LayerMask)
extern void LayerMaskExtensions_MaskToString_m022D4B6AF4F29DB15BC299D0AD93A2667BD00933 ();
// 0x00000090 System.String RootMotion.LayerMaskExtensions::MaskToString(UnityEngine.LayerMask,System.String)
extern void LayerMaskExtensions_MaskToString_m2C8EF33ADD2B9395541C452C22AB910A6B3890BC ();
// 0x00000091 UnityEngine.Quaternion RootMotion.QuaTools::Lerp(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
extern void QuaTools_Lerp_m0570DD53D86627AE23F9E1DDAA9B4033FB461CB3 ();
// 0x00000092 UnityEngine.Quaternion RootMotion.QuaTools::Slerp(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
extern void QuaTools_Slerp_mAB2B4EF8CCB8D2F61921608F945E8DAE8782345C ();
// 0x00000093 UnityEngine.Quaternion RootMotion.QuaTools::LinearBlend(UnityEngine.Quaternion,System.Single)
extern void QuaTools_LinearBlend_m0EF0482993F1EDB7CB148D29D5F3C1BF1EC91192 ();
// 0x00000094 UnityEngine.Quaternion RootMotion.QuaTools::SphericalBlend(UnityEngine.Quaternion,System.Single)
extern void QuaTools_SphericalBlend_m3DEFA8B64AE7DF900C78F51FD9695814D306114A ();
// 0x00000095 UnityEngine.Quaternion RootMotion.QuaTools::FromToAroundAxis(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void QuaTools_FromToAroundAxis_m59C218E5622BC695F655346A098BAA288D20B437 ();
// 0x00000096 UnityEngine.Quaternion RootMotion.QuaTools::RotationToLocalSpace(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern void QuaTools_RotationToLocalSpace_m0D8E4AC34823B7EBC5F399DF7B604C578F21B06D ();
// 0x00000097 UnityEngine.Quaternion RootMotion.QuaTools::FromToRotation(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern void QuaTools_FromToRotation_m3D768D23A4BCEDDF46F0F2EA0FCC824801DD95DF ();
// 0x00000098 UnityEngine.Vector3 RootMotion.QuaTools::GetAxis(UnityEngine.Vector3)
extern void QuaTools_GetAxis_mD45851D36E9DC32CD18D49AFA83B496F91D442BE ();
// 0x00000099 UnityEngine.Quaternion RootMotion.QuaTools::ClampRotation(UnityEngine.Quaternion,System.Single,System.Int32)
extern void QuaTools_ClampRotation_m2CC4F0F1F609A43A1856ED4E5F415B76DAFBEE5C ();
// 0x0000009A System.Single RootMotion.QuaTools::ClampAngle(System.Single,System.Single,System.Int32)
extern void QuaTools_ClampAngle_m063A5914F8978FF25DDE339CB60368E80A5F4B4B ();
// 0x0000009B UnityEngine.Quaternion RootMotion.QuaTools::MatchRotation(UnityEngine.Quaternion,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void QuaTools_MatchRotation_m3D0E437711B13691D3BC3CF8E37A751B47F9688E ();
// 0x0000009C T RootMotion.Singleton`1::get_instance()
// 0x0000009D System.Void RootMotion.Singleton`1::Awake()
// 0x0000009E System.Void RootMotion.Singleton`1::.ctor()
// 0x0000009F System.Void RootMotion.Singleton`1::.cctor()
// 0x000000A0 System.Void RootMotion.SolverManager::Disable()
extern void SolverManager_Disable_mAF7F1B03409CD5F15B92EB2CA3E4CDCC78822A6E ();
// 0x000000A1 System.Void RootMotion.SolverManager::InitiateSolver()
extern void SolverManager_InitiateSolver_m5BB7CCAF5151B14EFB11A6177B95097C87DF757F ();
// 0x000000A2 System.Void RootMotion.SolverManager::UpdateSolver()
extern void SolverManager_UpdateSolver_mCFA8146EF728678AA6530CB351AF15AB2769A473 ();
// 0x000000A3 System.Void RootMotion.SolverManager::FixTransforms()
extern void SolverManager_FixTransforms_m5915C5F3E7AE3003DF5BC8FAE2E29484AA133E91 ();
// 0x000000A4 System.Void RootMotion.SolverManager::OnDisable()
extern void SolverManager_OnDisable_mAB35E35F021A75E36EB0761D6ABE2909CBE78081 ();
// 0x000000A5 System.Void RootMotion.SolverManager::Start()
extern void SolverManager_Start_m1898B450B9DF67B21D2E1D0E464C18E58185449E ();
// 0x000000A6 System.Boolean RootMotion.SolverManager::get_animatePhysics()
extern void SolverManager_get_animatePhysics_m6B149D06101817217013A43B85E03BB77B635F64 ();
// 0x000000A7 System.Void RootMotion.SolverManager::Initiate()
extern void SolverManager_Initiate_mDF420FB1D3F8C66C4F30F36D6FCDF83D2C2B1C13 ();
// 0x000000A8 System.Void RootMotion.SolverManager::Update()
extern void SolverManager_Update_m81D50B25FD8D018C41ACDECBE8D7F99437B5E20B ();
// 0x000000A9 System.Void RootMotion.SolverManager::FindAnimatorRecursive(UnityEngine.Transform,System.Boolean)
extern void SolverManager_FindAnimatorRecursive_m405BC00B17F6227AC117D6D1E688355144F3FFA1 ();
// 0x000000AA System.Boolean RootMotion.SolverManager::get_isAnimated()
extern void SolverManager_get_isAnimated_m59432E7A0EF712F3A1A72CA2B21F3DAA9BC42182 ();
// 0x000000AB System.Void RootMotion.SolverManager::FixedUpdate()
extern void SolverManager_FixedUpdate_m7925F1E72E87A32BC92909A734309CE40C092E83 ();
// 0x000000AC System.Void RootMotion.SolverManager::LateUpdate()
extern void SolverManager_LateUpdate_m7E4147E101C1F2692CE6FDE75CE779FAD0AE5488 ();
// 0x000000AD System.Void RootMotion.SolverManager::UpdateSolverExternal()
extern void SolverManager_UpdateSolverExternal_m622BBE432080EB672523141DDB023CD89EFE40A4 ();
// 0x000000AE System.Void RootMotion.SolverManager::.ctor()
extern void SolverManager__ctor_m5EC263C17E103B73190883B068920291998D403B ();
// 0x000000AF System.Void RootMotion.TriggerEventBroadcaster::OnTriggerEnter(UnityEngine.Collider)
extern void TriggerEventBroadcaster_OnTriggerEnter_mF7981EFFE30D975B741F473339B9B0EE89BA7A90 ();
// 0x000000B0 System.Void RootMotion.TriggerEventBroadcaster::OnTriggerStay(UnityEngine.Collider)
extern void TriggerEventBroadcaster_OnTriggerStay_m034359136D2CC6615C0E30892519E7E1424649F4 ();
// 0x000000B1 System.Void RootMotion.TriggerEventBroadcaster::OnTriggerExit(UnityEngine.Collider)
extern void TriggerEventBroadcaster_OnTriggerExit_m9C1FFE01E7F045A5F521AE38DA9345945401D783 ();
// 0x000000B2 System.Void RootMotion.TriggerEventBroadcaster::.ctor()
extern void TriggerEventBroadcaster__ctor_m15BA15CA6936262AE959073DAC087952849E98F8 ();
// 0x000000B3 UnityEngine.Vector3 RootMotion.V3Tools::Lerp(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void V3Tools_Lerp_mE627D3C6353F8119AC6F187F049BB39CFC8BF293 ();
// 0x000000B4 UnityEngine.Vector3 RootMotion.V3Tools::Slerp(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void V3Tools_Slerp_m559EC1621BE6655096DB52FE6502209A495CE4B6 ();
// 0x000000B5 UnityEngine.Vector3 RootMotion.V3Tools::ExtractVertical(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void V3Tools_ExtractVertical_m21E5C5CC0291B497B2AE92FBCD5ACAC30CDF0A4A ();
// 0x000000B6 UnityEngine.Vector3 RootMotion.V3Tools::ExtractHorizontal(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void V3Tools_ExtractHorizontal_mDB3A7E16FFDF5F296800F5F920D7DF0336564F73 ();
// 0x000000B7 UnityEngine.Vector3 RootMotion.V3Tools::ClampDirection(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,System.Boolean&)
extern void V3Tools_ClampDirection_mE723D67930F3385280314BE79B2B2178279CF71F ();
// 0x000000B8 UnityEngine.Vector3 RootMotion.V3Tools::ClampDirection(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,System.Single&)
extern void V3Tools_ClampDirection_m12D4397DF2774F461537E9806AB36367807CA6DA ();
// 0x000000B9 UnityEngine.Vector3 RootMotion.V3Tools::LineToPlane(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void V3Tools_LineToPlane_m75F1F5804DC9A3E3BA11B42957CF879EA8242A3A ();
// 0x000000BA UnityEngine.Vector3 RootMotion.V3Tools::PointToPlane(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void V3Tools_PointToPlane_m85DCF160324062BEA6A92414B7F9FEBA64CD8829 ();
// 0x000000BB System.Void RootMotion.Warning::Log(System.String,RootMotion.Warning_Logger,System.Boolean)
extern void Warning_Log_m6B0EC4690D2F7990B353426976FCA58FE2448ECE ();
// 0x000000BC System.Void RootMotion.Warning::Log(System.String,UnityEngine.Transform,System.Boolean)
extern void Warning_Log_mD2162F6CAE666F6BA1981317A3AAC1CE778530B8 ();
// 0x000000BD System.Void RootMotion.Demos.AnimatorIKDemo::Start()
extern void AnimatorIKDemo_Start_mA97FCA1BED2930ACF9C57524EB0233A8ECA5878E ();
// 0x000000BE System.Void RootMotion.Demos.AnimatorIKDemo::OnAnimatorIK(System.Int32)
extern void AnimatorIKDemo_OnAnimatorIK_m79EEAA1888CD25C98B8F7B201DD42225A9C7BA9B ();
// 0x000000BF System.Void RootMotion.Demos.AnimatorIKDemo::.ctor()
extern void AnimatorIKDemo__ctor_m508EC83475C03FF9F1C7FFBB191A5EE3EA4D0777 ();
// 0x000000C0 System.Void RootMotion.Demos.BallShooter::Update()
extern void BallShooter_Update_m7532DC8514B5AB4E2088BD420A7CCD1075C96755 ();
// 0x000000C1 System.Void RootMotion.Demos.BallShooter::.ctor()
extern void BallShooter__ctor_m4ADD8786FAF7276FB5C8D8F644AB8F48FD4F8932 ();
// 0x000000C2 System.Void RootMotion.Demos.BoardController::Awake()
extern void BoardController_Awake_m285ED6882E5546F42518748021C79846E3364CF4 ();
// 0x000000C3 System.Void RootMotion.Demos.BoardController::Update()
extern void BoardController_Update_mA3FD75C4AD09DF209973B2F5A13181C3BEBE2060 ();
// 0x000000C4 System.Void RootMotion.Demos.BoardController::FixedUpdate()
extern void BoardController_FixedUpdate_m6884DD9296FF0D069FC8A6F7FD45AB28D7D1C053 ();
// 0x000000C5 System.Void RootMotion.Demos.BoardController::OnCollisionEnter(UnityEngine.Collision)
extern void BoardController_OnCollisionEnter_mCA697E7CD8CCF5A34027B462C9817CC449BD5387 ();
// 0x000000C6 System.Void RootMotion.Demos.BoardController::OnCollisionStay(UnityEngine.Collision)
extern void BoardController_OnCollisionStay_m54FA0AA47BBDEDE71F5BFB24721597670990263A ();
// 0x000000C7 System.Void RootMotion.Demos.BoardController::OnCollisionExit(UnityEngine.Collision)
extern void BoardController_OnCollisionExit_mD849AB5111282B2DA88ED2F392422FC459EEE3CB ();
// 0x000000C8 System.Void RootMotion.Demos.BoardController::.ctor()
extern void BoardController__ctor_m3E01DC4D30B81C7D861C63E61C69B7CAA1061CE0 ();
// 0x000000C9 RootMotion.Demos.CharacterMeleeDemo RootMotion.Demos.CharacterAnimationMeleeDemo::get_melee()
extern void CharacterAnimationMeleeDemo_get_melee_m0D347C6E86DFAC7184BB380B35E21E5135A2C523 ();
// 0x000000CA System.Void RootMotion.Demos.CharacterAnimationMeleeDemo::Update()
extern void CharacterAnimationMeleeDemo_Update_m092A4BB714901C85CFBBD9BFE32E0D8BC73F574A ();
// 0x000000CB System.Void RootMotion.Demos.CharacterAnimationMeleeDemo::.ctor()
extern void CharacterAnimationMeleeDemo__ctor_m1748A1BEE8CFC253B260F90BAFB58DAFE2F98890 ();
// 0x000000CC System.Void RootMotion.Demos.CharacterMeleeDemo::Start()
extern void CharacterMeleeDemo_Start_m39E0F9ABE2B86AFD08487A1B1C8B376637EC4594 ();
// 0x000000CD RootMotion.Demos.CharacterMeleeDemo_Action RootMotion.Demos.CharacterMeleeDemo::get_currentAction()
extern void CharacterMeleeDemo_get_currentAction_mD379E363F1E9281ED86CE7709F1DEFA29C139696 ();
// 0x000000CE System.Void RootMotion.Demos.CharacterMeleeDemo::Update()
extern void CharacterMeleeDemo_Update_m75CB5D150931A730D5BBB0790B62EF37F1279C1A ();
// 0x000000CF System.Boolean RootMotion.Demos.CharacterMeleeDemo::StartAction(RootMotion.Demos.CharacterMeleeDemo_Action)
extern void CharacterMeleeDemo_StartAction_mA9DDD4A69D168DE35E170CE17EAA4829E7535553 ();
// 0x000000D0 System.Void RootMotion.Demos.CharacterMeleeDemo::.ctor()
extern void CharacterMeleeDemo__ctor_m0157CE34094107AD563DE1CC91DFB4217499CCAF ();
// 0x000000D1 RootMotion.Dynamics.BehaviourPuppet RootMotion.Demos.CharacterPuppet::get_puppet()
extern void CharacterPuppet_get_puppet_m8678A5D70042C6C7EFA3D56AE46756D6E1DCB721 ();
// 0x000000D2 System.Void RootMotion.Demos.CharacterPuppet::set_puppet(RootMotion.Dynamics.BehaviourPuppet)
extern void CharacterPuppet_set_puppet_mBB33AC33AA6BBF41D3297C33CB952217870920A6 ();
// 0x000000D3 System.Void RootMotion.Demos.CharacterPuppet::Start()
extern void CharacterPuppet_Start_mDBF0CBB08F072520EFB185B92042F78FAE826ED9 ();
// 0x000000D4 System.Void RootMotion.Demos.CharacterPuppet::Move(UnityEngine.Vector3,UnityEngine.Quaternion)
extern void CharacterPuppet_Move_m79E6280BC30C8133EE81939C026A8DCEF47FA6F9 ();
// 0x000000D5 System.Void RootMotion.Demos.CharacterPuppet::Rotate()
extern void CharacterPuppet_Rotate_mD924F477B414028EDA602CB9958DEF29293F384A ();
// 0x000000D6 System.Boolean RootMotion.Demos.CharacterPuppet::Jump()
extern void CharacterPuppet_Jump_m999435B14362886C4DA46B7BDD0C6F1BBA4AE91D ();
// 0x000000D7 System.Void RootMotion.Demos.CharacterPuppet::.ctor()
extern void CharacterPuppet__ctor_mE623C03BBC0A2AFB91016C6CF5BBC971909D545D ();
// 0x000000D8 System.Void RootMotion.Demos.CreatePuppetInRuntime::Start()
extern void CreatePuppetInRuntime_Start_m721142FF986BEE095E4B62A210E51536B283A5BC ();
// 0x000000D9 System.Void RootMotion.Demos.CreatePuppetInRuntime::.ctor()
extern void CreatePuppetInRuntime__ctor_m07E8BFBEAEC32FB0C5302F070B4CC1A0085F6A4F ();
// 0x000000DA System.Void RootMotion.Demos.CreateRagdollInRuntime::Start()
extern void CreateRagdollInRuntime_Start_mE9639DE3EF8793E97DA8E7B4A6852DC7FDB830AF ();
// 0x000000DB System.Void RootMotion.Demos.CreateRagdollInRuntime::Update()
extern void CreateRagdollInRuntime_Update_mDCB816ED4D5F9803D06F66D2BA52A8D16CB41023 ();
// 0x000000DC System.Void RootMotion.Demos.CreateRagdollInRuntime::.ctor()
extern void CreateRagdollInRuntime__ctor_m2BCDB173772ABC7F38B76F7BB03DA3F8D9703086 ();
// 0x000000DD System.Void RootMotion.Demos.Destructor::Start()
extern void Destructor_Start_mAADF48E45731FFF0B0A9E8C9A638E36447D72FDE ();
// 0x000000DE System.Collections.IEnumerator RootMotion.Demos.Destructor::Destruct()
extern void Destructor_Destruct_mF60E3DCAA6B3487CDBBAD6370F6FA3B18F74A238 ();
// 0x000000DF System.Void RootMotion.Demos.Destructor::.ctor()
extern void Destructor__ctor_mDD213AA0BEF75EF03B7134D6F54CB617BCC3BFD1 ();
// 0x000000E0 System.Void RootMotion.Demos.Dying::Start()
extern void Dying_Start_m7BA6FA03A78E86E2CA49D172BDCAB1F9A74EC789 ();
// 0x000000E1 System.Void RootMotion.Demos.Dying::Update()
extern void Dying_Update_m7F5DEEC5E9C54BBE992943A87AD1331430EACBF0 ();
// 0x000000E2 System.Collections.IEnumerator RootMotion.Demos.Dying::FadeOutPinWeight()
extern void Dying_FadeOutPinWeight_mA1206D05CAD834919911F06BEBCCB6182CB1F60C ();
// 0x000000E3 System.Collections.IEnumerator RootMotion.Demos.Dying::FadeOutMuscleWeight()
extern void Dying_FadeOutMuscleWeight_m5E1A1010D6D79CEA5DA5C4C65B7D4BC0F22A253E ();
// 0x000000E4 System.Void RootMotion.Demos.Dying::.ctor()
extern void Dying__ctor_mED8691843BDAE2C26F57995F57F34A82B5CA0718 ();
// 0x000000E5 System.Void RootMotion.Demos.FXCollisionBlood::Start()
extern void FXCollisionBlood_Start_m956562DCCF786A82B2B2C3FFC363215CCCEBBAD2 ();
// 0x000000E6 System.Void RootMotion.Demos.FXCollisionBlood::OnCollisionImpulse(RootMotion.Dynamics.MuscleCollision,System.Single)
extern void FXCollisionBlood_OnCollisionImpulse_m93A4671E896A2BD8A2D470487DE59B33D00A04D8 ();
// 0x000000E7 System.Void RootMotion.Demos.FXCollisionBlood::OnDestroy()
extern void FXCollisionBlood_OnDestroy_mE198DCB5C810D74B032A111F53C376ACF47CD701 ();
// 0x000000E8 System.Void RootMotion.Demos.FXCollisionBlood::.ctor()
extern void FXCollisionBlood__ctor_mF88C219CAD577DFB23A939A40F6B2FA2A27DFA38 ();
// 0x000000E9 System.Void RootMotion.Demos.Grab::Start()
extern void Grab_Start_m1F759583236AA67EE6C00CAF82F87C723EEDAA1A ();
// 0x000000EA System.Void RootMotion.Demos.Grab::OnCollisionEnter(UnityEngine.Collision)
extern void Grab_OnCollisionEnter_mDDD2DF7E47C1DD3C55245B1D2FE9DAB2B5F3D3F6 ();
// 0x000000EB System.Void RootMotion.Demos.Grab::Update()
extern void Grab_Update_m7419324CA39BD4B0A613D1A250D1653D06517FF8 ();
// 0x000000EC System.Void RootMotion.Demos.Grab::.ctor()
extern void Grab__ctor_mF222A4D491694F0E92F1A74BDAE39BE67E29253D ();
// 0x000000ED System.Void RootMotion.Demos.Killing::Update()
extern void Killing_Update_mF24EDDB544BFDAAE3DB6007DC66D64717C380F85 ();
// 0x000000EE System.Void RootMotion.Demos.Killing::.ctor()
extern void Killing__ctor_mF47D6EBB14CF827F1B95C390BE265723F2874E02 ();
// 0x000000EF System.Void RootMotion.Demos.LayerSetup::Awake()
extern void LayerSetup_Awake_m7DEA59CFFB9AB7101A673DDD78EF4DE155D08D08 ();
// 0x000000F0 System.Void RootMotion.Demos.LayerSetup::.ctor()
extern void LayerSetup__ctor_m183877FCEF105AA555BD0045D47DBB4E881F35C9 ();
// 0x000000F1 System.Void RootMotion.Demos.NavMeshPuppet::Update()
extern void NavMeshPuppet_Update_mB5C5469A60D850CA9554859F56090F015B14C852 ();
// 0x000000F2 System.Void RootMotion.Demos.NavMeshPuppet::.ctor()
extern void NavMeshPuppet__ctor_m82E14CDCA0F5A5F4712C93F3635743E05A20A855 ();
// 0x000000F3 System.Void RootMotion.Demos.Planet::Start()
extern void Planet_Start_mDBDB3B54F73994CCF7A64CAF362FA5ED98E30487 ();
// 0x000000F4 System.Void RootMotion.Demos.Planet::FixedUpdate()
extern void Planet_FixedUpdate_m401221BAF1CDED4123AF69E9F6093F322655AC0B ();
// 0x000000F5 System.Void RootMotion.Demos.Planet::ApplyGravity(UnityEngine.Rigidbody)
extern void Planet_ApplyGravity_mBDC4C2E37E73CE3D2CEDD5EA2555A9104AFF47F2 ();
// 0x000000F6 System.Void RootMotion.Demos.Planet::.ctor()
extern void Planet__ctor_m43512F914349F64019272DB89239FBE208D7FCFC ();
// 0x000000F7 System.Void RootMotion.Demos.PropDemo::Start()
extern void PropDemo_Start_m5DC538E6010F108DC192AA348F4C66E7C7F3EAF8 ();
// 0x000000F8 System.Void RootMotion.Demos.PropDemo::Update()
extern void PropDemo_Update_m9DD648186BB19E3CF27F0F8CC9D7D13F97A06D9E ();
// 0x000000F9 RootMotion.Dynamics.PropRoot RootMotion.Demos.PropDemo::get_connectTo()
extern void PropDemo_get_connectTo_m13EC341FBB271BAC5B2D5FD28B99BB083200593B ();
// 0x000000FA System.Void RootMotion.Demos.PropDemo::.ctor()
extern void PropDemo__ctor_m5740291D7A66B17FA49C295832C708F89C1560CE ();
// 0x000000FB System.Void RootMotion.Demos.PropMelee::StartAction(System.Single)
extern void PropMelee_StartAction_m2A72D775590077C627FFE6DF24211AFA818ADBAC ();
// 0x000000FC System.Collections.IEnumerator RootMotion.Demos.PropMelee::Action(System.Single)
extern void PropMelee_Action_mD4068BB6BB5DEF26303E7AE86763EAF3C211E222 ();
// 0x000000FD System.Void RootMotion.Demos.PropMelee::OnStart()
extern void PropMelee_OnStart_m6AEEFCE5506EA20FE1D34F0C7049237ED17E77B6 ();
// 0x000000FE System.Void RootMotion.Demos.PropMelee::OnPickUp(RootMotion.Dynamics.PropRoot)
extern void PropMelee_OnPickUp_m3E934DA37529CE775F114524241161B5912576FD ();
// 0x000000FF System.Void RootMotion.Demos.PropMelee::OnDrop()
extern void PropMelee_OnDrop_mD0D7342C3259D31377120A808A264AC5E1E96C81 ();
// 0x00000100 System.Void RootMotion.Demos.PropMelee::.ctor()
extern void PropMelee__ctor_m4E6510C22CAE46DF32260D9DF9A58FCB8E89DB99 ();
// 0x00000101 System.Void RootMotion.Demos.PropPickUpTrigger::OnTriggerEnter(UnityEngine.Collider)
extern void PropPickUpTrigger_OnTriggerEnter_mE061C5D1591FF54DF4AA6118745F86333CDAF22C ();
// 0x00000102 System.Void RootMotion.Demos.PropPickUpTrigger::.ctor()
extern void PropPickUpTrigger__ctor_m41F1890365B6312EE8D70C1F0D564414CA453410 ();
// 0x00000103 System.Void RootMotion.Demos.PuppetBoard::Start()
extern void PuppetBoard_Start_mF108C27EAA27E97F68DE3601167EEB6CDEAD14F8 ();
// 0x00000104 System.Void RootMotion.Demos.PuppetBoard::FixedUpdate()
extern void PuppetBoard_FixedUpdate_m9741339591C31768CBE55F73C40553872BCE30D1 ();
// 0x00000105 System.Void RootMotion.Demos.PuppetBoard::.ctor()
extern void PuppetBoard__ctor_mABFD0B358B0B646513F3F676DCAE154313285D51 ();
// 0x00000106 System.Void RootMotion.Demos.PuppetScaling::Start()
extern void PuppetScaling_Start_m0AE9F0F94DAFC70ED292338F84DB5589037FFB9D ();
// 0x00000107 System.Void RootMotion.Demos.PuppetScaling::Update()
extern void PuppetScaling_Update_m6426071E460E9AE2FB33EAAB240F46FCF08462B4 ();
// 0x00000108 System.Void RootMotion.Demos.PuppetScaling::.ctor()
extern void PuppetScaling__ctor_m7EE7A16B33263640B714C99D39321B3C1E767D14 ();
// 0x00000109 System.Void RootMotion.Demos.RaycastShooter::Update()
extern void RaycastShooter_Update_m22C91F5E76AD1CCB3A6F97CF170A76ABD0637B0A ();
// 0x0000010A System.Void RootMotion.Demos.RaycastShooter::.ctor()
extern void RaycastShooter__ctor_m01ED18AD26D9D718E1F0E68017DFF5E88726825E ();
// 0x0000010B System.Boolean RootMotion.Demos.Respawning::get_isPooled()
extern void Respawning_get_isPooled_mEEBDA48A9F70320688F958CC8C892218716BB316 ();
// 0x0000010C System.Void RootMotion.Demos.Respawning::Start()
extern void Respawning_Start_mDE8312A8D9AFE128B54B867655EE5FE81AB26C38 ();
// 0x0000010D System.Void RootMotion.Demos.Respawning::Update()
extern void Respawning_Update_m55EE2AF65B21D2117BADA5D5CCAEF5AE0AC850A1 ();
// 0x0000010E System.Void RootMotion.Demos.Respawning::Pool()
extern void Respawning_Pool_m7FA999B8D6E3443881080A9DED97764B92C1DD3C ();
// 0x0000010F System.Void RootMotion.Demos.Respawning::Respawn(UnityEngine.Vector3,UnityEngine.Quaternion)
extern void Respawning_Respawn_mE5E0F1A996DD4BE003376E989EACCCAACCFCF762 ();
// 0x00000110 System.Void RootMotion.Demos.Respawning::.ctor()
extern void Respawning__ctor_mCF4BC2871CBC5AD975B5B91136E9302DDE1F7112 ();
// 0x00000111 System.Void RootMotion.Demos.RotateShoulderToTarget::OnPuppetMasterRead()
extern void RotateShoulderToTarget_OnPuppetMasterRead_m1B97FAE39EEB786BCAFB7BBEB1A489D101793639 ();
// 0x00000112 System.Void RootMotion.Demos.RotateShoulderToTarget::.ctor()
extern void RotateShoulderToTarget__ctor_mF4ACD4582DF7FABF0781DF4CC5FEB6344CB214A0 ();
// 0x00000113 System.Void RootMotion.Demos.SkeletonShooter::Update()
extern void SkeletonShooter_Update_mB2583DFD8CCD5712F3C4383B7257B5175F2F998F ();
// 0x00000114 System.Void RootMotion.Demos.SkeletonShooter::.ctor()
extern void SkeletonShooter__ctor_m19030E1AE83AAD14D46675742C063DBC69843733 ();
// 0x00000115 UnityEngine.Transform RootMotion.Demos.UserControlAIMelee::get_moveTarget()
extern void UserControlAIMelee_get_moveTarget_m496EAE4EB9A3AB089E00B603EE478C5B439E7037 ();
// 0x00000116 System.Void RootMotion.Demos.UserControlAIMelee::Update()
extern void UserControlAIMelee_Update_m7E3791BC25407CC1BED0A90BB210AA7509A0DAEF ();
// 0x00000117 System.Boolean RootMotion.Demos.UserControlAIMelee::CanAttack()
extern void UserControlAIMelee_CanAttack_m85208227CFFA9803E619A6CFD05A46B50AF81184 ();
// 0x00000118 System.Void RootMotion.Demos.UserControlAIMelee::.ctor()
extern void UserControlAIMelee__ctor_m0A925F7788B39EA66A1A21E999678D2F16F0909A ();
// 0x00000119 System.Void RootMotion.Demos.UserControlMelee::Update()
extern void UserControlMelee_Update_m6835F9DBE4B2F748A9AFCCB87FC7C9A0B749734C ();
// 0x0000011A System.Void RootMotion.Demos.UserControlMelee::.ctor()
extern void UserControlMelee__ctor_m39B8505FE47A9E83F54E841EA5A218E9A43AB851 ();
// 0x0000011B UnityEngine.Vector3 RootMotion.Demos.CharacterAnimationBase::GetPivotPoint()
extern void CharacterAnimationBase_GetPivotPoint_m1018CB765301AE015F036104328BF4508E14A473 ();
// 0x0000011C System.Boolean RootMotion.Demos.CharacterAnimationBase::get_animationGrounded()
extern void CharacterAnimationBase_get_animationGrounded_mF576A1A357D965533A534EB9A6258FE0FBD94D79 ();
// 0x0000011D System.Single RootMotion.Demos.CharacterAnimationBase::GetAngleFromForward(UnityEngine.Vector3)
extern void CharacterAnimationBase_GetAngleFromForward_m308199E64F4970AEBBECEB1E4EC3A35CCFE0B095 ();
// 0x0000011E System.Void RootMotion.Demos.CharacterAnimationBase::Start()
extern void CharacterAnimationBase_Start_m8A3E2715CC16BA1ABDE37F57C108202DCE707204 ();
// 0x0000011F System.Void RootMotion.Demos.CharacterAnimationBase::LateUpdate()
extern void CharacterAnimationBase_LateUpdate_m69F9ED282C2B5ED54BF008790992FD41581284C2 ();
// 0x00000120 System.Void RootMotion.Demos.CharacterAnimationBase::FixedUpdate()
extern void CharacterAnimationBase_FixedUpdate_m9BD9AA4ADE8C6D01C0FE1C4E3F37D8EDE9C6EC26 ();
// 0x00000121 System.Void RootMotion.Demos.CharacterAnimationBase::SmoothFollow()
extern void CharacterAnimationBase_SmoothFollow_m7B5158868C4342133F57210A84411B4F600F81E8 ();
// 0x00000122 System.Void RootMotion.Demos.CharacterAnimationBase::.ctor()
extern void CharacterAnimationBase__ctor_mC5598C331E3F61B698957C31DBFAB6A434A44285 ();
// 0x00000123 System.Void RootMotion.Demos.CharacterAnimationSimple::Start()
extern void CharacterAnimationSimple_Start_m92D1575C4FD4B3E66B584C2C34B597295122B5AB ();
// 0x00000124 UnityEngine.Vector3 RootMotion.Demos.CharacterAnimationSimple::GetPivotPoint()
extern void CharacterAnimationSimple_GetPivotPoint_m4DD7B1306C196C1E9843A2ACC1502807F58F6836 ();
// 0x00000125 System.Void RootMotion.Demos.CharacterAnimationSimple::Update()
extern void CharacterAnimationSimple_Update_m1C9704FEBC5B20924117AD5B3D0FE73167683874 ();
// 0x00000126 System.Void RootMotion.Demos.CharacterAnimationSimple::.ctor()
extern void CharacterAnimationSimple__ctor_m9E57665AC7464BD698C02E1B41D80A8094DB7B7A ();
// 0x00000127 System.Void RootMotion.Demos.CharacterAnimationThirdPerson::Start()
extern void CharacterAnimationThirdPerson_Start_mCE97CA2A3B883DB660296E19984870F30993601D ();
// 0x00000128 UnityEngine.Vector3 RootMotion.Demos.CharacterAnimationThirdPerson::GetPivotPoint()
extern void CharacterAnimationThirdPerson_GetPivotPoint_m3AA31633895332745BCD4856339A237C14BAF048 ();
// 0x00000129 System.Boolean RootMotion.Demos.CharacterAnimationThirdPerson::get_animationGrounded()
extern void CharacterAnimationThirdPerson_get_animationGrounded_mB167095B5D2E233D74FEBB1407DD18858CB1A7C9 ();
// 0x0000012A System.Void RootMotion.Demos.CharacterAnimationThirdPerson::Update()
extern void CharacterAnimationThirdPerson_Update_m6BB492201C5EE44476FCD9F10E1CFE94DE7FB06E ();
// 0x0000012B System.Void RootMotion.Demos.CharacterAnimationThirdPerson::OnAnimatorMove()
extern void CharacterAnimationThirdPerson_OnAnimatorMove_mE2B33D242C221CEE05992F4D4297E61F94BE3A8B ();
// 0x0000012C System.Void RootMotion.Demos.CharacterAnimationThirdPerson::.ctor()
extern void CharacterAnimationThirdPerson__ctor_m7FA89D7FD0CE7BBAC61C92A773A73392D2313F6A ();
// 0x0000012D System.Void RootMotion.Demos.CharacterBase::Move(UnityEngine.Vector3,UnityEngine.Quaternion)
// 0x0000012E UnityEngine.Vector3 RootMotion.Demos.CharacterBase::GetGravity()
extern void CharacterBase_GetGravity_m52928070D1FD9818AEEA17004E67BCD3EF349DBB ();
// 0x0000012F System.Void RootMotion.Demos.CharacterBase::Start()
extern void CharacterBase_Start_m97EBCF95A437AF5C809AB15FC3560B6DCB1DDC04 ();
// 0x00000130 UnityEngine.RaycastHit RootMotion.Demos.CharacterBase::GetSpherecastHit()
extern void CharacterBase_GetSpherecastHit_m665E7A08DF4FD1E12CB09A62D1EA594CD898B7F0 ();
// 0x00000131 System.Single RootMotion.Demos.CharacterBase::GetAngleFromForward(UnityEngine.Vector3)
extern void CharacterBase_GetAngleFromForward_m1D8A6EE938756474E581F74D5E750089A8752902 ();
// 0x00000132 System.Void RootMotion.Demos.CharacterBase::RigidbodyRotateAround(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void CharacterBase_RigidbodyRotateAround_m627B8E966C6CFD3442FE1802C266E6C8562C628D ();
// 0x00000133 System.Void RootMotion.Demos.CharacterBase::ScaleCapsule(System.Single)
extern void CharacterBase_ScaleCapsule_m7E46447FAE188DDDED07DC75BE2B2587224B1FD4 ();
// 0x00000134 System.Void RootMotion.Demos.CharacterBase::HighFriction()
extern void CharacterBase_HighFriction_m5B9CB0E513FE38E2F32790D8EB08EE7311366706 ();
// 0x00000135 System.Void RootMotion.Demos.CharacterBase::ZeroFriction()
extern void CharacterBase_ZeroFriction_m60A9ECC47F369571E2220712B33AD6649688BE97 ();
// 0x00000136 System.Single RootMotion.Demos.CharacterBase::GetSlopeDamper(UnityEngine.Vector3,UnityEngine.Vector3)
extern void CharacterBase_GetSlopeDamper_m36021B2F5C9300CBE4514BFAFA57A35038012493 ();
// 0x00000137 System.Void RootMotion.Demos.CharacterBase::.ctor()
extern void CharacterBase__ctor_m643636E039B8359397126853AD95BFC407156127 ();
// 0x00000138 System.Boolean RootMotion.Demos.CharacterThirdPerson::get_onGround()
extern void CharacterThirdPerson_get_onGround_m8FAB65BBFE10A7D958BE8562C781004454B3B185 ();
// 0x00000139 System.Void RootMotion.Demos.CharacterThirdPerson::set_onGround(System.Boolean)
extern void CharacterThirdPerson_set_onGround_m7138F56B42D5F8F41D8F0239C804A79DB39E20CD ();
// 0x0000013A System.Void RootMotion.Demos.CharacterThirdPerson::Start()
extern void CharacterThirdPerson_Start_mC010FA7FEC916FA3490C696F5D5908F540AC273D ();
// 0x0000013B System.Void RootMotion.Demos.CharacterThirdPerson::OnAnimatorMove()
extern void CharacterThirdPerson_OnAnimatorMove_m6E67668759563100F5E41DC29AF3C5B415C03D4E ();
// 0x0000013C System.Void RootMotion.Demos.CharacterThirdPerson::Move(UnityEngine.Vector3,UnityEngine.Quaternion)
extern void CharacterThirdPerson_Move_m856C3FBDA610CE24FF7F1EC6E2DE6AD093B55A7C ();
// 0x0000013D System.Void RootMotion.Demos.CharacterThirdPerson::FixedUpdate()
extern void CharacterThirdPerson_FixedUpdate_m9E62D0B642FE1E8D883D019472958E8E2B3D9310 ();
// 0x0000013E System.Void RootMotion.Demos.CharacterThirdPerson::Update()
extern void CharacterThirdPerson_Update_m33EEAC902435B41B78B03339D87FF82B16D04EA4 ();
// 0x0000013F System.Void RootMotion.Demos.CharacterThirdPerson::LateUpdate()
extern void CharacterThirdPerson_LateUpdate_m204DD570122E903D4BE39F7B30654569CC1F931B ();
// 0x00000140 System.Void RootMotion.Demos.CharacterThirdPerson::MoveFixed(UnityEngine.Vector3)
extern void CharacterThirdPerson_MoveFixed_m8025BEFB98AA606ABDE2828A72087290F89596F9 ();
// 0x00000141 System.Void RootMotion.Demos.CharacterThirdPerson::WallRun()
extern void CharacterThirdPerson_WallRun_mFDE6238062E36512E1C259C2111AE4C4C688321E ();
// 0x00000142 System.Boolean RootMotion.Demos.CharacterThirdPerson::CanWallRun()
extern void CharacterThirdPerson_CanWallRun_m49F8A48969093D765DF8887E6C5798E242BE5B6A ();
// 0x00000143 UnityEngine.Vector3 RootMotion.Demos.CharacterThirdPerson::GetMoveDirection()
extern void CharacterThirdPerson_GetMoveDirection_mACB1F2204C72B8DFD0AD7D1ACB7B42BC8DA3AA7C ();
// 0x00000144 System.Void RootMotion.Demos.CharacterThirdPerson::Rotate()
extern void CharacterThirdPerson_Rotate_mEEBAF749E60D2B4C0ED2C7FA6B7302B54FD0946A ();
// 0x00000145 UnityEngine.Vector3 RootMotion.Demos.CharacterThirdPerson::GetForwardDirection()
extern void CharacterThirdPerson_GetForwardDirection_mA7F39ECCF4558CD5A702BEE32F5CFDF30864E16D ();
// 0x00000146 System.Boolean RootMotion.Demos.CharacterThirdPerson::Jump()
extern void CharacterThirdPerson_Jump_mDF0BC561C27E93A0A60F2B84CE443D2104B9A083 ();
// 0x00000147 System.Void RootMotion.Demos.CharacterThirdPerson::GroundCheck()
extern void CharacterThirdPerson_GroundCheck_m5EF602C558AA2A106A20EBCDF2468164E481A549 ();
// 0x00000148 System.Void RootMotion.Demos.CharacterThirdPerson::.ctor()
extern void CharacterThirdPerson__ctor_mD74DE56D370C9B25B833BC60798FA858F9FE9B7F ();
// 0x00000149 System.Boolean RootMotion.Demos.SimpleLocomotion::get_isGrounded()
extern void SimpleLocomotion_get_isGrounded_m896F64939C860C875BAF5DA09407B9B4E9C3388D ();
// 0x0000014A System.Void RootMotion.Demos.SimpleLocomotion::set_isGrounded(System.Boolean)
extern void SimpleLocomotion_set_isGrounded_mD20363625AE25634D3971FDBEDC9F26E65CC91AA ();
// 0x0000014B System.Void RootMotion.Demos.SimpleLocomotion::Start()
extern void SimpleLocomotion_Start_mE9882312EB255A8DB96BFE19C852A10718BB7949 ();
// 0x0000014C System.Void RootMotion.Demos.SimpleLocomotion::Update()
extern void SimpleLocomotion_Update_m5040C75AC416DF82E9D695922585024B360E76F7 ();
// 0x0000014D System.Void RootMotion.Demos.SimpleLocomotion::LateUpdate()
extern void SimpleLocomotion_LateUpdate_m438B1B1FFEB4F9D6393DCA233C294D07A5567653 ();
// 0x0000014E System.Void RootMotion.Demos.SimpleLocomotion::Rotate()
extern void SimpleLocomotion_Rotate_mC59E45FFA964EDFDED02227CEA9264F03E1818CE ();
// 0x0000014F System.Void RootMotion.Demos.SimpleLocomotion::Move()
extern void SimpleLocomotion_Move_mB371FAA143D1EA4A8025DF464321FDE9509D3D29 ();
// 0x00000150 UnityEngine.Vector3 RootMotion.Demos.SimpleLocomotion::GetInputVector()
extern void SimpleLocomotion_GetInputVector_mD6CF1D37650E583967AF16A1576DA1842B41641F ();
// 0x00000151 UnityEngine.Vector3 RootMotion.Demos.SimpleLocomotion::GetInputVectorRaw()
extern void SimpleLocomotion_GetInputVectorRaw_m4F84C83E09A942D60D6171F50467AC754FDEEA9C ();
// 0x00000152 System.Void RootMotion.Demos.SimpleLocomotion::.ctor()
extern void SimpleLocomotion__ctor_mDBBB3F00DAA51BA20BFD7F254F969229F2B71375 ();
// 0x00000153 System.Void RootMotion.Demos.UserControlAI::Start()
extern void UserControlAI_Start_mFCDBF39A9C9833CB9D4E1DBCAE8254778AD28A2D ();
// 0x00000154 System.Void RootMotion.Demos.UserControlAI::Update()
extern void UserControlAI_Update_mE0EA9C30597B13F01957B12F799B46E5DA7EDA4E ();
// 0x00000155 System.Void RootMotion.Demos.UserControlAI::OnDrawGizmos()
extern void UserControlAI_OnDrawGizmos_m5A2C63DA64038B1391079954DE158DF5117CD004 ();
// 0x00000156 System.Void RootMotion.Demos.UserControlAI::.ctor()
extern void UserControlAI__ctor_m1FC3B0C86442AB41C9DBCBC663882F8A445E94E1 ();
// 0x00000157 System.Void RootMotion.Demos.UserControlThirdPerson::Start()
extern void UserControlThirdPerson_Start_mEF4C562B944405861D024173B475CFE8B6B146CD ();
// 0x00000158 System.Void RootMotion.Demos.UserControlThirdPerson::Update()
extern void UserControlThirdPerson_Update_m454D94BA692425382861A95F897D2BAB8F6F39B2 ();
// 0x00000159 System.Void RootMotion.Demos.UserControlThirdPerson::.ctor()
extern void UserControlThirdPerson__ctor_m7F6A7DB1FAFDFC8B469A56D06801BEDD32F90187 ();
// 0x0000015A System.Void RootMotion.Demos.ApplicationQuit::Update()
extern void ApplicationQuit_Update_mD64D857990584D3A62FBA5891CF28FFC917577E2 ();
// 0x0000015B System.Void RootMotion.Demos.ApplicationQuit::.ctor()
extern void ApplicationQuit__ctor_m7E9D64C6FA0CBB664E9E070013092952246A60D0 ();
// 0x0000015C System.Void RootMotion.Demos.SlowMo::Update()
extern void SlowMo_Update_m3FFFBB69B9E074C605BF466FCF3D84E57DA27FC5 ();
// 0x0000015D System.Boolean RootMotion.Demos.SlowMo::IsSlowMotion()
extern void SlowMo_IsSlowMotion_m778D493F324C59E86F5B98E3046F14762CF17AFC ();
// 0x0000015E System.Void RootMotion.Demos.SlowMo::.ctor()
extern void SlowMo__ctor_mC54701C0A85703FC33D4E32E0BC7CC9C5E408D1E ();
// 0x0000015F UnityEngine.Vector3 RootMotion.Demos.Navigator::get_normalizedDeltaPosition()
extern void Navigator_get_normalizedDeltaPosition_m82B6AA3A327BECE1FE3A206A69E2CD15AE89F9DB ();
// 0x00000160 System.Void RootMotion.Demos.Navigator::set_normalizedDeltaPosition(UnityEngine.Vector3)
extern void Navigator_set_normalizedDeltaPosition_m08A4772D516114921B9FE39FDAD733B89357D4E3 ();
// 0x00000161 RootMotion.Demos.Navigator_State RootMotion.Demos.Navigator::get_state()
extern void Navigator_get_state_m025F9DD0DEC0FB822D318D93208351CD1638ACAB ();
// 0x00000162 System.Void RootMotion.Demos.Navigator::set_state(RootMotion.Demos.Navigator_State)
extern void Navigator_set_state_mA99231CF329132577E9A7CE08A9980C2D28663FA ();
// 0x00000163 System.Void RootMotion.Demos.Navigator::Initiate(UnityEngine.Transform)
extern void Navigator_Initiate_m1CFA7FCE28EDA78E25782F7518F8B6028CFC6802 ();
// 0x00000164 System.Void RootMotion.Demos.Navigator::Update(UnityEngine.Vector3)
extern void Navigator_Update_m20B945E6A358C881A682395CADE17306F81551E6 ();
// 0x00000165 System.Void RootMotion.Demos.Navigator::CalculatePath(UnityEngine.Vector3)
extern void Navigator_CalculatePath_mCACAA6F8F5CA04CC2F6C8BA302328B56AADFA99D ();
// 0x00000166 System.Boolean RootMotion.Demos.Navigator::Find(UnityEngine.Vector3)
extern void Navigator_Find_m27DAF60AF770BB5EB714C38D9C2B78291EEAB9E5 ();
// 0x00000167 System.Void RootMotion.Demos.Navigator::Stop()
extern void Navigator_Stop_mF2094527EA4A5BAD12B76846D170B7058BE1823F ();
// 0x00000168 System.Single RootMotion.Demos.Navigator::HorDistance(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Navigator_HorDistance_mE3E27E418775F9CC297C65B510F0CD1E5240D9E9 ();
// 0x00000169 System.Void RootMotion.Demos.Navigator::Visualize()
extern void Navigator_Visualize_m6CD93094EEFA8699A54B79DD627E727CA563123D ();
// 0x0000016A System.Void RootMotion.Demos.Navigator::.ctor()
extern void Navigator__ctor_mC62BE7392A21DED1957FE19F335C53E5106B8DAF ();
// 0x0000016B System.Void RootMotion.Dynamics.AnimationBlocker::LateUpdate()
extern void AnimationBlocker_LateUpdate_mFB743DA09B1DAD4BFB588080C45F952381559956 ();
// 0x0000016C System.Void RootMotion.Dynamics.AnimationBlocker::.ctor()
extern void AnimationBlocker__ctor_m99ED3538BBAC8FE7FE6102D74B9827A63593BBCE ();
// 0x0000016D System.Void RootMotion.Dynamics.BehaviourAnimatedStagger::OnInitiate()
extern void BehaviourAnimatedStagger_OnInitiate_m2679961E3291D0540BFFC4637F193A51711D0B00 ();
// 0x0000016E System.Void RootMotion.Dynamics.BehaviourAnimatedStagger::OnActivate()
extern void BehaviourAnimatedStagger_OnActivate_mFF969716AE295DAE059CF26F29EBEEDA239FDB3F ();
// 0x0000016F System.Void RootMotion.Dynamics.BehaviourAnimatedStagger::OnReactivate()
extern void BehaviourAnimatedStagger_OnReactivate_mC00880B74F79094AC4CDC0022A2754A679523AEC ();
// 0x00000170 System.Collections.IEnumerator RootMotion.Dynamics.BehaviourAnimatedStagger::LoseBalance()
extern void BehaviourAnimatedStagger_LoseBalance_m89E6846FC9758C5CE36A2ECE638FC004439FEC68 ();
// 0x00000171 RootMotion.Dynamics.BehaviourAnimatedStagger_FallParams RootMotion.Dynamics.BehaviourAnimatedStagger::GetFallParams(RootMotion.Dynamics.Muscle_Group)
extern void BehaviourAnimatedStagger_GetFallParams_m7064733D3BC08C3A3A43CA6FBED44ED56590E9FE ();
// 0x00000172 System.Void RootMotion.Dynamics.BehaviourAnimatedStagger::.ctor()
extern void BehaviourAnimatedStagger__ctor_m4B4E9E561EAF3B9B5105280848602B7A41BFD359 ();
// 0x00000173 System.Void RootMotion.Dynamics.BehaviourBase::OnReactivate()
// 0x00000174 System.Void RootMotion.Dynamics.BehaviourBase::Resurrect()
extern void BehaviourBase_Resurrect_m8B17A8F553ABAFF82EF419191D793FC61D341CB3 ();
// 0x00000175 System.Void RootMotion.Dynamics.BehaviourBase::Freeze()
extern void BehaviourBase_Freeze_mA3413A7689517D70CD1F0517EE3CD9A49FC0BE97 ();
// 0x00000176 System.Void RootMotion.Dynamics.BehaviourBase::Unfreeze()
extern void BehaviourBase_Unfreeze_m69845ECA69ED8AD76637D4EBA7406C2CC8CCABCB ();
// 0x00000177 System.Void RootMotion.Dynamics.BehaviourBase::KillStart()
extern void BehaviourBase_KillStart_m901A5B5C62E22CAF7A0554F6284A7E4C98B47CAB ();
// 0x00000178 System.Void RootMotion.Dynamics.BehaviourBase::KillEnd()
extern void BehaviourBase_KillEnd_m9EDE39F1B2E65C0CBF48BA948648F29DABF3DA42 ();
// 0x00000179 System.Void RootMotion.Dynamics.BehaviourBase::OnTeleport(UnityEngine.Quaternion,UnityEngine.Vector3,UnityEngine.Vector3,System.Boolean)
extern void BehaviourBase_OnTeleport_mA014710B907079074A7AEF31B16FCF6B6191713B ();
// 0x0000017A System.Void RootMotion.Dynamics.BehaviourBase::OnMuscleAdded(RootMotion.Dynamics.Muscle)
extern void BehaviourBase_OnMuscleAdded_m110CBD603773B041F79F5246C4223776D942D12D ();
// 0x0000017B System.Void RootMotion.Dynamics.BehaviourBase::OnMuscleRemoved(RootMotion.Dynamics.Muscle)
extern void BehaviourBase_OnMuscleRemoved_mF5D676184D9A9AC58C1563F1656686559133D169 ();
// 0x0000017C System.Void RootMotion.Dynamics.BehaviourBase::OnActivate()
extern void BehaviourBase_OnActivate_mEFDD5115CA6B288CF7B4F6B73F5B7CF83A8EF000 ();
// 0x0000017D System.Void RootMotion.Dynamics.BehaviourBase::OnDeactivate()
extern void BehaviourBase_OnDeactivate_mE3426851AC88951605D4977813DCC4CBCFC9E604 ();
// 0x0000017E System.Void RootMotion.Dynamics.BehaviourBase::OnInitiate()
extern void BehaviourBase_OnInitiate_mF0C4FB109099D2BB7D68DFB367C1A719B6091404 ();
// 0x0000017F System.Void RootMotion.Dynamics.BehaviourBase::OnFixedUpdate()
extern void BehaviourBase_OnFixedUpdate_mFD95ECB4218F708D5880DDA9C0577F7B5805AF90 ();
// 0x00000180 System.Void RootMotion.Dynamics.BehaviourBase::OnUpdate()
extern void BehaviourBase_OnUpdate_mC9618416B88BD41DA4348BF0A215C1EB17B6FBEF ();
// 0x00000181 System.Void RootMotion.Dynamics.BehaviourBase::OnLateUpdate()
extern void BehaviourBase_OnLateUpdate_mB5EAAD16B0A7E65CEB31A74F7C9DECFC4A1A0673 ();
// 0x00000182 System.Void RootMotion.Dynamics.BehaviourBase::OnDrawGizmosBehaviour()
extern void BehaviourBase_OnDrawGizmosBehaviour_mB5BC3555B9150954AF7B4641109526D77BF717E4 ();
// 0x00000183 System.Void RootMotion.Dynamics.BehaviourBase::OnFixTransformsBehaviour()
extern void BehaviourBase_OnFixTransformsBehaviour_mD3BCC64E322E076734B899FB5011C82745DD3683 ();
// 0x00000184 System.Void RootMotion.Dynamics.BehaviourBase::OnReadBehaviour()
extern void BehaviourBase_OnReadBehaviour_m4785EE40457243018DF47D2B45547ED4DF097DE3 ();
// 0x00000185 System.Void RootMotion.Dynamics.BehaviourBase::OnWriteBehaviour()
extern void BehaviourBase_OnWriteBehaviour_mA4459125D08A552C292EF8F077E80457B19D028C ();
// 0x00000186 System.Void RootMotion.Dynamics.BehaviourBase::OnMuscleHitBehaviour(RootMotion.Dynamics.MuscleHit)
extern void BehaviourBase_OnMuscleHitBehaviour_mE432A4FB40BDEFB92B32BEAAF79AB75921F085EE ();
// 0x00000187 System.Void RootMotion.Dynamics.BehaviourBase::OnMuscleCollisionBehaviour(RootMotion.Dynamics.MuscleCollision)
extern void BehaviourBase_OnMuscleCollisionBehaviour_m29A32E68A3A30015ABD2CF7F55FE18DE3839BEB4 ();
// 0x00000188 System.Void RootMotion.Dynamics.BehaviourBase::OnMuscleCollisionExitBehaviour(RootMotion.Dynamics.MuscleCollision)
extern void BehaviourBase_OnMuscleCollisionExitBehaviour_m5CE9A7B19C32AB9111BBCD387609D83E55E24085 ();
// 0x00000189 System.Boolean RootMotion.Dynamics.BehaviourBase::get_forceActive()
extern void BehaviourBase_get_forceActive_mE8B65E1782D2DC848990CC4D6C53733906C0A324 ();
// 0x0000018A System.Void RootMotion.Dynamics.BehaviourBase::set_forceActive(System.Boolean)
extern void BehaviourBase_set_forceActive_m45C9374BDBFD0F2076DC143572DB869B1F7E5CE4 ();
// 0x0000018B System.Void RootMotion.Dynamics.BehaviourBase::Initiate()
extern void BehaviourBase_Initiate_mBACE9C3DA23D50324FC1ED50B9FB37A296050E38 ();
// 0x0000018C System.Void RootMotion.Dynamics.BehaviourBase::OnFixTransforms()
extern void BehaviourBase_OnFixTransforms_m31F1BADCCDB6BB7E4D64B319A250A03F9BD0D112 ();
// 0x0000018D System.Void RootMotion.Dynamics.BehaviourBase::OnRead()
extern void BehaviourBase_OnRead_m6CA367D866ED61D3D870ECE6FE6C264B44075638 ();
// 0x0000018E System.Void RootMotion.Dynamics.BehaviourBase::OnWrite()
extern void BehaviourBase_OnWrite_m0CA0432DE3598F1F166DCB9FB7D17D2056E963BE ();
// 0x0000018F System.Void RootMotion.Dynamics.BehaviourBase::OnMuscleHit(RootMotion.Dynamics.MuscleHit)
extern void BehaviourBase_OnMuscleHit_m9D3601EC9DBC3DC74E37DED168851D80C96BDED0 ();
// 0x00000190 System.Void RootMotion.Dynamics.BehaviourBase::OnMuscleCollision(RootMotion.Dynamics.MuscleCollision)
extern void BehaviourBase_OnMuscleCollision_mBC9EE4372C0FEC96B254902319EEF1439909C202 ();
// 0x00000191 System.Void RootMotion.Dynamics.BehaviourBase::OnMuscleCollisionExit(RootMotion.Dynamics.MuscleCollision)
extern void BehaviourBase_OnMuscleCollisionExit_m0BAF4A23F43DE7BA94B96AB8FAC687DC3368C2B6 ();
// 0x00000192 System.Void RootMotion.Dynamics.BehaviourBase::OnEnable()
extern void BehaviourBase_OnEnable_m0D88683CF850729D959B0409ED3D15DD0EEE66AD ();
// 0x00000193 System.Void RootMotion.Dynamics.BehaviourBase::Activate()
extern void BehaviourBase_Activate_mC24D2115F5E1E96550A24A5A81B28C06F6135A12 ();
// 0x00000194 System.Void RootMotion.Dynamics.BehaviourBase::OnDisable()
extern void BehaviourBase_OnDisable_m80149EBB2115E7759053841B542366173AD5C56C ();
// 0x00000195 System.Void RootMotion.Dynamics.BehaviourBase::FixedUpdateB()
extern void BehaviourBase_FixedUpdateB_mAE0E4F3D8A3478A17E105A112D4578FD6E39D9AA ();
// 0x00000196 System.Void RootMotion.Dynamics.BehaviourBase::UpdateB()
extern void BehaviourBase_UpdateB_mBBE2F697EE3DE4058FB114386C2C7606820DFC14 ();
// 0x00000197 System.Void RootMotion.Dynamics.BehaviourBase::LateUpdateB()
extern void BehaviourBase_LateUpdateB_mD561E7E32DF38742831CB67F372B04ECF9DAA6C0 ();
// 0x00000198 System.Void RootMotion.Dynamics.BehaviourBase::OnDrawGizmos()
extern void BehaviourBase_OnDrawGizmos_m85712977BA5F6C8EA927A663751786835F09C8D1 ();
// 0x00000199 System.Void RootMotion.Dynamics.BehaviourBase::RotateTargetToRootMuscle()
extern void BehaviourBase_RotateTargetToRootMuscle_mE5DF646A2A86A5AC0C7FDEE5B4905DF364A3B4A9 ();
// 0x0000019A System.Void RootMotion.Dynamics.BehaviourBase::TranslateTargetToRootMuscle(System.Single)
extern void BehaviourBase_TranslateTargetToRootMuscle_mCFDF6D1CC294E2ECE4FBF6C47D314FFA837140B3 ();
// 0x0000019B System.Void RootMotion.Dynamics.BehaviourBase::RemoveMusclesOfGroup(RootMotion.Dynamics.Muscle_Group)
extern void BehaviourBase_RemoveMusclesOfGroup_mE56FFF2CA411F61947CB05DBCAF3BA76F62EE820 ();
// 0x0000019C System.Void RootMotion.Dynamics.BehaviourBase::GroundTarget(UnityEngine.LayerMask)
extern void BehaviourBase_GroundTarget_mFE565F7994CB0432F60DF1D353FE991CF0D1FE7B ();
// 0x0000019D System.Boolean RootMotion.Dynamics.BehaviourBase::MusclesContainsGroup(RootMotion.Dynamics.Muscle_Group)
extern void BehaviourBase_MusclesContainsGroup_mD9FA41600C6FB75CCFFCBC87531C1BDC104AC142 ();
// 0x0000019E System.Void RootMotion.Dynamics.BehaviourBase::.ctor()
extern void BehaviourBase__ctor_m92696285C1C5F7E42CC3531BF22BBDA15F09CF25 ();
// 0x0000019F System.Void RootMotion.Dynamics.BehaviourFall::OpenUserManual()
extern void BehaviourFall_OpenUserManual_m9BE4AAA6A063603EA189AA0DDC752B36202B0772 ();
// 0x000001A0 System.Void RootMotion.Dynamics.BehaviourFall::OpenScriptReference()
extern void BehaviourFall_OpenScriptReference_m402E8684F8E0A2C395B249FFED4E490C6E6C5D2E ();
// 0x000001A1 System.Void RootMotion.Dynamics.BehaviourFall::OnActivate()
extern void BehaviourFall_OnActivate_m1BF955CE407CAC0C390736CBE52BB139A157AD4D ();
// 0x000001A2 System.Void RootMotion.Dynamics.BehaviourFall::OnDeactivate()
extern void BehaviourFall_OnDeactivate_m1DB414E23F566324398B98A0D3D31511876C928E ();
// 0x000001A3 System.Void RootMotion.Dynamics.BehaviourFall::OnReactivate()
extern void BehaviourFall_OnReactivate_m7081F3E8C36A068EEF2F049E4CC48B83C6FFDD5C ();
// 0x000001A4 System.Collections.IEnumerator RootMotion.Dynamics.BehaviourFall::SmoothActivate()
extern void BehaviourFall_SmoothActivate_m6310CAFF9CEF0D1C0EAFDBCCD2A71B39AD79E830 ();
// 0x000001A5 System.Void RootMotion.Dynamics.BehaviourFall::OnFixedUpdate()
extern void BehaviourFall_OnFixedUpdate_mE43B0C8AB8E0298FFC78EF27F5D63171EAA523D4 ();
// 0x000001A6 System.Void RootMotion.Dynamics.BehaviourFall::OnLateUpdate()
extern void BehaviourFall_OnLateUpdate_mDE326CAAE6BFDFC42ACC06EBB119D23A1EE78E79 ();
// 0x000001A7 System.Void RootMotion.Dynamics.BehaviourFall::Resurrect()
extern void BehaviourFall_Resurrect_m1600FBB3088AEE36182C5B606191F854A8CB1193 ();
// 0x000001A8 System.Single RootMotion.Dynamics.BehaviourFall::GetBlendTarget(System.Single)
extern void BehaviourFall_GetBlendTarget_m13D09D84CDFBB96060B6C7D83E9FC84933C45F64 ();
// 0x000001A9 System.Single RootMotion.Dynamics.BehaviourFall::GetGroundHeight()
extern void BehaviourFall_GetGroundHeight_mB69D18516F755BCAA7FD70C68A8A2C504CA9D35B ();
// 0x000001AA System.Void RootMotion.Dynamics.BehaviourFall::.ctor()
extern void BehaviourFall__ctor_m1C795A658F844859E01F8821C8B6886E0E2D8F7C ();
// 0x000001AB System.Void RootMotion.Dynamics.BehaviourPuppet::OpenUserManual()
extern void BehaviourPuppet_OpenUserManual_mBDAA7E396698FDC358ED879630F4F9F7FF0A2C92 ();
// 0x000001AC System.Void RootMotion.Dynamics.BehaviourPuppet::OpenScriptReference()
extern void BehaviourPuppet_OpenScriptReference_mFE84E95F20B926E920386242510EF0170D49D1EF ();
// 0x000001AD RootMotion.Dynamics.BehaviourPuppet_State RootMotion.Dynamics.BehaviourPuppet::get_state()
extern void BehaviourPuppet_get_state_m4F5BA0A4750977A8D117DAD6A2EF6602FDC0309D ();
// 0x000001AE System.Void RootMotion.Dynamics.BehaviourPuppet::set_state(RootMotion.Dynamics.BehaviourPuppet_State)
extern void BehaviourPuppet_set_state_m33938814ED00B2C319F7269E1A0F6634EAFAFA93 ();
// 0x000001AF System.Void RootMotion.Dynamics.BehaviourPuppet::OnReactivate()
extern void BehaviourPuppet_OnReactivate_m757C43EAB4ABF5E5D42763F5DCED860A1DB1B281 ();
// 0x000001B0 System.Void RootMotion.Dynamics.BehaviourPuppet::Reset(UnityEngine.Vector3,UnityEngine.Quaternion)
extern void BehaviourPuppet_Reset_mF6E06A780DFDA3FF2A4665445B1F7C8023C2AB14 ();
// 0x000001B1 System.Void RootMotion.Dynamics.BehaviourPuppet::OnTeleport(UnityEngine.Quaternion,UnityEngine.Vector3,UnityEngine.Vector3,System.Boolean)
extern void BehaviourPuppet_OnTeleport_m0D6735B8136699299B540C557B1E41D6AC05E30F ();
// 0x000001B2 System.Void RootMotion.Dynamics.BehaviourPuppet::OnInitiate()
extern void BehaviourPuppet_OnInitiate_m11D5CF666B3C3729B3D76F8C155A93DE58174750 ();
// 0x000001B3 System.Void RootMotion.Dynamics.BehaviourPuppet::OnActivate()
extern void BehaviourPuppet_OnActivate_mC01198123B104C416A915D410C886D6B54A63555 ();
// 0x000001B4 System.Void RootMotion.Dynamics.BehaviourPuppet::KillStart()
extern void BehaviourPuppet_KillStart_mF56DC95CBF3CB236D913B2709A09E570DCF091BA ();
// 0x000001B5 System.Void RootMotion.Dynamics.BehaviourPuppet::KillEnd()
extern void BehaviourPuppet_KillEnd_m3FC3620413B51D54A83771AED4CAF3B24D92D764 ();
// 0x000001B6 System.Void RootMotion.Dynamics.BehaviourPuppet::Resurrect()
extern void BehaviourPuppet_Resurrect_mF69FA83BFE31D7A5E221BFEA242224989BF55EA4 ();
// 0x000001B7 System.Void RootMotion.Dynamics.BehaviourPuppet::OnDeactivate()
extern void BehaviourPuppet_OnDeactivate_m967411B5B0BD5FFF97109209EFDEC5C797DEFB56 ();
// 0x000001B8 System.Void RootMotion.Dynamics.BehaviourPuppet::OnFixedUpdate()
extern void BehaviourPuppet_OnFixedUpdate_m166A0200AB022B72EEC48FD9D50E50F22C1F98EB ();
// 0x000001B9 System.Void RootMotion.Dynamics.BehaviourPuppet::OnLateUpdate()
extern void BehaviourPuppet_OnLateUpdate_m48376624F186BD0289A2944C599B1CB9ECE0E7A4 ();
// 0x000001BA System.Boolean RootMotion.Dynamics.BehaviourPuppet::SetKinematic()
extern void BehaviourPuppet_SetKinematic_m0A996523E81345FFC91CF0482BA0409CFB9C8FCA ();
// 0x000001BB System.Void RootMotion.Dynamics.BehaviourPuppet::OnReadBehaviour()
extern void BehaviourPuppet_OnReadBehaviour_m34FBC5C1B12599C0E8CFE735F1135197506542B2 ();
// 0x000001BC System.Void RootMotion.Dynamics.BehaviourPuppet::BlendMuscleMapping(System.Int32,System.Boolean&)
extern void BehaviourPuppet_BlendMuscleMapping_m066C1F8161F97B318DA85F50C24A2C98B36F587B ();
// 0x000001BD System.Void RootMotion.Dynamics.BehaviourPuppet::OnMuscleAdded(RootMotion.Dynamics.Muscle)
extern void BehaviourPuppet_OnMuscleAdded_m8962E4B9473A6E66BFE16EBEE89121FB4A1CA0AE ();
// 0x000001BE System.Void RootMotion.Dynamics.BehaviourPuppet::OnMuscleRemoved(RootMotion.Dynamics.Muscle)
extern void BehaviourPuppet_OnMuscleRemoved_mD85B25E7AA74B33F074A030E1EA21BBBBE71EA63 ();
// 0x000001BF System.Void RootMotion.Dynamics.BehaviourPuppet::MoveTarget(UnityEngine.Vector3)
extern void BehaviourPuppet_MoveTarget_m2EBE398937F38194CF4DB435B34BF5A3DC7837F7 ();
// 0x000001C0 System.Void RootMotion.Dynamics.BehaviourPuppet::RotateTarget(UnityEngine.Quaternion)
extern void BehaviourPuppet_RotateTarget_mE74CA4F2C28EC3109BA7807AE40D80ECA1EDFE48 ();
// 0x000001C1 System.Void RootMotion.Dynamics.BehaviourPuppet::GroundTarget(UnityEngine.LayerMask)
extern void BehaviourPuppet_GroundTarget_mABE0A5F175A8EBF1D3A2053879ADFF1B69F97AC3 ();
// 0x000001C2 System.Void RootMotion.Dynamics.BehaviourPuppet::OnDrawGizmosSelected()
extern void BehaviourPuppet_OnDrawGizmosSelected_m6C0FB5191D1FFF60C826F28BB78AA52A29C08A8E ();
// 0x000001C3 System.Void RootMotion.Dynamics.BehaviourPuppet::Boost(System.Single,System.Single)
extern void BehaviourPuppet_Boost_mCAA091BC176893CC2430E243173FAC0F0B8D225E ();
// 0x000001C4 System.Void RootMotion.Dynamics.BehaviourPuppet::Boost(System.Int32,System.Single,System.Single)
extern void BehaviourPuppet_Boost_m6CE30219629469ED171B19FB8A23FFC3A5D42759 ();
// 0x000001C5 System.Void RootMotion.Dynamics.BehaviourPuppet::Boost(System.Int32,System.Single,System.Single,System.Single,System.Single)
extern void BehaviourPuppet_Boost_mF863B70D1D99B5F9423DA969B22C8EACCCB5C06B ();
// 0x000001C6 System.Void RootMotion.Dynamics.BehaviourPuppet::BoostImmunity(System.Single)
extern void BehaviourPuppet_BoostImmunity_m1BEC8DC47233C0A7A6EAAE383F2ACE5A7DBA79BA ();
// 0x000001C7 System.Void RootMotion.Dynamics.BehaviourPuppet::BoostImmunity(System.Int32,System.Single)
extern void BehaviourPuppet_BoostImmunity_mB6AFF5612A77E4F3DB4D04232003DE7B313732F8 ();
// 0x000001C8 System.Void RootMotion.Dynamics.BehaviourPuppet::BoostImmunity(System.Int32,System.Single,System.Single,System.Single)
extern void BehaviourPuppet_BoostImmunity_m4E177D8BDBD3A37CFC9BD805A5FF88E01BE4D229 ();
// 0x000001C9 System.Void RootMotion.Dynamics.BehaviourPuppet::BoostImpulseMlp(System.Single)
extern void BehaviourPuppet_BoostImpulseMlp_m0A833386915C6DDB94BAECBB02AEE8FF57695822 ();
// 0x000001CA System.Void RootMotion.Dynamics.BehaviourPuppet::BoostImpulseMlp(System.Int32,System.Single)
extern void BehaviourPuppet_BoostImpulseMlp_mCECFFFB217250460D958385E8F520B80A7C35168 ();
// 0x000001CB System.Void RootMotion.Dynamics.BehaviourPuppet::BoostImpulseMlp(System.Int32,System.Single,System.Single,System.Single)
extern void BehaviourPuppet_BoostImpulseMlp_mDA0B48BE7AB1DA599ED33252DACA2BF00F375441 ();
// 0x000001CC System.Void RootMotion.Dynamics.BehaviourPuppet::Unpin()
extern void BehaviourPuppet_Unpin_m45C52718A593B6A02FE9B51CB82BE458E0EC0AB1 ();
// 0x000001CD System.Void RootMotion.Dynamics.BehaviourPuppet::OnMuscleHitBehaviour(RootMotion.Dynamics.MuscleHit)
extern void BehaviourPuppet_OnMuscleHitBehaviour_mD4542D1ED4243C8404E2838B74F1BE5BA2B22CF1 ();
// 0x000001CE System.Void RootMotion.Dynamics.BehaviourPuppet::OnMuscleCollisionBehaviour(RootMotion.Dynamics.MuscleCollision)
extern void BehaviourPuppet_OnMuscleCollisionBehaviour_m85ACAAE448F36F95A97E919D33819157DD2E23B9 ();
// 0x000001CF System.Single RootMotion.Dynamics.BehaviourPuppet::GetImpulse(RootMotion.Dynamics.MuscleCollision,System.Single&)
extern void BehaviourPuppet_GetImpulse_mE5091FA98AEED45A7CEA82A602C9EA6AAF8C7EA8 ();
// 0x000001D0 System.Void RootMotion.Dynamics.BehaviourPuppet::UnPin(System.Int32,System.Single)
extern void BehaviourPuppet_UnPin_m2E787B26EE3BBAB5644AA299123AB37E078222F4 ();
// 0x000001D1 System.Void RootMotion.Dynamics.BehaviourPuppet::UnPinMuscle(System.Int32,System.Single)
extern void BehaviourPuppet_UnPinMuscle_m288B373A79CD1711EE2BFF5DB260E640C8226B39 ();
// 0x000001D2 System.Boolean RootMotion.Dynamics.BehaviourPuppet::Activate(UnityEngine.Collision,System.Single)
extern void BehaviourPuppet_Activate_mBB59B5F2A4A0139EFB25137279484022612B97CA ();
// 0x000001D3 System.Boolean RootMotion.Dynamics.BehaviourPuppet::IsProne()
extern void BehaviourPuppet_IsProne_mCAA034B67CB5CC9AADB79BF530513686B1B2FFBB ();
// 0x000001D4 System.Single RootMotion.Dynamics.BehaviourPuppet::GetFalloff(System.Int32,System.Int32,System.Single,System.Single)
extern void BehaviourPuppet_GetFalloff_m9ADCBCDFB7BF4323B3F7E4B97CFCB36929C4D157 ();
// 0x000001D5 System.Single RootMotion.Dynamics.BehaviourPuppet::GetFalloff(System.Int32,System.Int32,System.Single,System.Single,System.Single)
extern void BehaviourPuppet_GetFalloff_m35E2585C17025A6046A553043BA0A59D57B831F3 ();
// 0x000001D6 System.Boolean RootMotion.Dynamics.BehaviourPuppet::InGroup(RootMotion.Dynamics.Muscle_Group,RootMotion.Dynamics.Muscle_Group)
extern void BehaviourPuppet_InGroup_m743DA5D515FFC7D119AF7AF4D45F1BFF6C7F1A0D ();
// 0x000001D7 RootMotion.Dynamics.BehaviourPuppet_MuscleProps RootMotion.Dynamics.BehaviourPuppet::GetProps(RootMotion.Dynamics.Muscle_Group)
extern void BehaviourPuppet_GetProps_m320F43FF0BDB135EBF6891A5969438329843C335 ();
// 0x000001D8 System.Void RootMotion.Dynamics.BehaviourPuppet::SetState(RootMotion.Dynamics.BehaviourPuppet_State)
extern void BehaviourPuppet_SetState_mEBF4AE0E220F8268FA58E48913E919B2E7663381 ();
// 0x000001D9 System.Void RootMotion.Dynamics.BehaviourPuppet::SetColliders(System.Boolean)
extern void BehaviourPuppet_SetColliders_m9E10BDF9D52EDE1ECC0F821C18A93256C2355147 ();
// 0x000001DA System.Void RootMotion.Dynamics.BehaviourPuppet::SetColliders(RootMotion.Dynamics.Muscle,System.Boolean)
extern void BehaviourPuppet_SetColliders_mF897D58B7C979E04D2213CD505DD4D7A94DABC23 ();
// 0x000001DB System.Void RootMotion.Dynamics.BehaviourPuppet::.ctor()
extern void BehaviourPuppet__ctor_m0DF10D416AA5E6D5B43EC7D7BBBEBD11854251EB ();
// 0x000001DC System.Void RootMotion.Dynamics.BehaviourTemplate::OnInitiate()
extern void BehaviourTemplate_OnInitiate_m1D69D7821DB88CE7F75340BC761BA2858CA4E3C5 ();
// 0x000001DD System.Void RootMotion.Dynamics.BehaviourTemplate::OnActivate()
extern void BehaviourTemplate_OnActivate_mA7354DDC0DD69B497E45B549F808E0D9A73B2DA8 ();
// 0x000001DE System.Void RootMotion.Dynamics.BehaviourTemplate::OnReactivate()
extern void BehaviourTemplate_OnReactivate_m129EDEF1FC09EBA6C6AF9369EA32527F021D8B72 ();
// 0x000001DF System.Void RootMotion.Dynamics.BehaviourTemplate::OnDeactivate()
extern void BehaviourTemplate_OnDeactivate_m630F349217CD6AED874760CDB88E7F7C0151D7A5 ();
// 0x000001E0 System.Void RootMotion.Dynamics.BehaviourTemplate::OnFixedUpdate()
extern void BehaviourTemplate_OnFixedUpdate_m2EDCAB1BE736047EC7E0324D8F6A3508A4A7B8DD ();
// 0x000001E1 System.Void RootMotion.Dynamics.BehaviourTemplate::OnLateUpdate()
extern void BehaviourTemplate_OnLateUpdate_mD123FD8A1B83723A5D0C9DEF9305F1D1B3DB1912 ();
// 0x000001E2 System.Void RootMotion.Dynamics.BehaviourTemplate::OnMuscleHitBehaviour(RootMotion.Dynamics.MuscleHit)
extern void BehaviourTemplate_OnMuscleHitBehaviour_m9CB3A96DB0931CE4447BEC5870C5F533371C7DBC ();
// 0x000001E3 System.Void RootMotion.Dynamics.BehaviourTemplate::OnMuscleCollisionBehaviour(RootMotion.Dynamics.MuscleCollision)
extern void BehaviourTemplate_OnMuscleCollisionBehaviour_mF2FC6655AD03F182CB211FEEC0880B5E8EC626C3 ();
// 0x000001E4 System.Void RootMotion.Dynamics.BehaviourTemplate::.ctor()
extern void BehaviourTemplate__ctor_m7AC26544F128C4203A51035076E25BA1B1C4BD35 ();
// 0x000001E5 UnityEngine.ConfigurableJoint RootMotion.Dynamics.SubBehaviourBalancer::get_joint()
extern void SubBehaviourBalancer_get_joint_m802538E215DAB5CF8A46B99436F2E38D260E88E9 ();
// 0x000001E6 System.Void RootMotion.Dynamics.SubBehaviourBalancer::set_joint(UnityEngine.ConfigurableJoint)
extern void SubBehaviourBalancer_set_joint_mD28CEBF4ED702C2ED6A7C64E627FC93EB90D65FF ();
// 0x000001E7 UnityEngine.Vector3 RootMotion.Dynamics.SubBehaviourBalancer::get_dir()
extern void SubBehaviourBalancer_get_dir_m58BD74AC51AC73FEA11CB8F396383A2DA0816305 ();
// 0x000001E8 System.Void RootMotion.Dynamics.SubBehaviourBalancer::set_dir(UnityEngine.Vector3)
extern void SubBehaviourBalancer_set_dir_mEBB2EBC252372EDA6B62B63D465B4ED040AF12A4 ();
// 0x000001E9 UnityEngine.Vector3 RootMotion.Dynamics.SubBehaviourBalancer::get_dirVel()
extern void SubBehaviourBalancer_get_dirVel_m1025E52F7302CA60B47256B77C12B57516E78D14 ();
// 0x000001EA System.Void RootMotion.Dynamics.SubBehaviourBalancer::set_dirVel(UnityEngine.Vector3)
extern void SubBehaviourBalancer_set_dirVel_mF671B644901CA7CEF4EFD9BDB1A96FFA064F57DA ();
// 0x000001EB UnityEngine.Vector3 RootMotion.Dynamics.SubBehaviourBalancer::get_cop()
extern void SubBehaviourBalancer_get_cop_m6CA43E1CA05587F95AADCDCE8527519134A7D5AC ();
// 0x000001EC System.Void RootMotion.Dynamics.SubBehaviourBalancer::set_cop(UnityEngine.Vector3)
extern void SubBehaviourBalancer_set_cop_m501443224AF24107977A8D53ED8B96ED7C500763 ();
// 0x000001ED UnityEngine.Vector3 RootMotion.Dynamics.SubBehaviourBalancer::get_com()
extern void SubBehaviourBalancer_get_com_mC4E9F129B76FEED980C2CEEBF301103CF5121E35 ();
// 0x000001EE System.Void RootMotion.Dynamics.SubBehaviourBalancer::set_com(UnityEngine.Vector3)
extern void SubBehaviourBalancer_set_com_m55E0FB15463AB964007F1B478CC6BE26E7F11B9E ();
// 0x000001EF UnityEngine.Vector3 RootMotion.Dynamics.SubBehaviourBalancer::get_comV()
extern void SubBehaviourBalancer_get_comV_mA8C82A03457AB62D8F4A1B81EC82A449E14AB219 ();
// 0x000001F0 System.Void RootMotion.Dynamics.SubBehaviourBalancer::set_comV(UnityEngine.Vector3)
extern void SubBehaviourBalancer_set_comV_m98D9D067B3EFE08944BF78777292740E7A809DBD ();
// 0x000001F1 System.Void RootMotion.Dynamics.SubBehaviourBalancer::Initiate(RootMotion.Dynamics.BehaviourBase,RootMotion.Dynamics.SubBehaviourBalancer_Settings,UnityEngine.Rigidbody,UnityEngine.Rigidbody[],UnityEngine.ConfigurableJoint,UnityEngine.Transform[],RootMotion.Dynamics.PressureSensor)
extern void SubBehaviourBalancer_Initiate_m40DBD98B8B9EFAAC14F02F04CEABEB3D0EA6CC5A ();
// 0x000001F2 System.Void RootMotion.Dynamics.SubBehaviourBalancer::Solve()
extern void SubBehaviourBalancer_Solve_mAAE1CD242292A6A08F923AD0BC830472E4A64562 ();
// 0x000001F3 System.Void RootMotion.Dynamics.SubBehaviourBalancer::.ctor()
extern void SubBehaviourBalancer__ctor_mC87BF6C32ED1E599824AEA5CEC8FEED693C9339D ();
// 0x000001F4 UnityEngine.Vector2 RootMotion.Dynamics.SubBehaviourBase::XZ(UnityEngine.Vector3)
extern void SubBehaviourBase_XZ_m85DE1D630F4730144F1EEF6EC85792BB1B4BE796 ();
// 0x000001F5 UnityEngine.Vector3 RootMotion.Dynamics.SubBehaviourBase::XYZ(UnityEngine.Vector2)
extern void SubBehaviourBase_XYZ_mF275B352B17BC1AB7DD4BEF0A9F4A41B6DDBB9FB ();
// 0x000001F6 UnityEngine.Vector3 RootMotion.Dynamics.SubBehaviourBase::Flatten(UnityEngine.Vector3)
extern void SubBehaviourBase_Flatten_m474BC1B7544C9BC3A0EC62FCA1B6BD4FCB7B57C7 ();
// 0x000001F7 UnityEngine.Vector3 RootMotion.Dynamics.SubBehaviourBase::SetY(UnityEngine.Vector3,System.Single)
extern void SubBehaviourBase_SetY_m878D72951ECAEF1D923D22214CCDD9D791883325 ();
// 0x000001F8 System.Void RootMotion.Dynamics.SubBehaviourBase::.ctor()
extern void SubBehaviourBase__ctor_mFD851118C243500C15632EBA3996FD47F7010FE3 ();
// 0x000001F9 UnityEngine.Vector3 RootMotion.Dynamics.SubBehaviourCOM::get_position()
extern void SubBehaviourCOM_get_position_mA49CAD402C44CFF468CF2A3A1E826B4EDE3BFE66 ();
// 0x000001FA System.Void RootMotion.Dynamics.SubBehaviourCOM::set_position(UnityEngine.Vector3)
extern void SubBehaviourCOM_set_position_m303206079646F9EA93A7A6B0CB6730689CE2BE01 ();
// 0x000001FB UnityEngine.Vector3 RootMotion.Dynamics.SubBehaviourCOM::get_direction()
extern void SubBehaviourCOM_get_direction_mBD73EB9520993518E43FC1568E6F4EE34889D464 ();
// 0x000001FC System.Void RootMotion.Dynamics.SubBehaviourCOM::set_direction(UnityEngine.Vector3)
extern void SubBehaviourCOM_set_direction_mF68730B5E4E6FEFE919B1D592E63497DAD87C324 ();
// 0x000001FD System.Single RootMotion.Dynamics.SubBehaviourCOM::get_angle()
extern void SubBehaviourCOM_get_angle_m39E54DA0C94294808B78D729F8581D2802D75204 ();
// 0x000001FE System.Void RootMotion.Dynamics.SubBehaviourCOM::set_angle(System.Single)
extern void SubBehaviourCOM_set_angle_m66A02CD933CA90FAAEC6E02158C665E8135FB342 ();
// 0x000001FF UnityEngine.Vector3 RootMotion.Dynamics.SubBehaviourCOM::get_velocity()
extern void SubBehaviourCOM_get_velocity_m11F32045576B48324328D5403E86770AC2D626E1 ();
// 0x00000200 System.Void RootMotion.Dynamics.SubBehaviourCOM::set_velocity(UnityEngine.Vector3)
extern void SubBehaviourCOM_set_velocity_mFB56FEACFA55ACF6101F8D127C5ADA35BABEB3C0 ();
// 0x00000201 UnityEngine.Vector3 RootMotion.Dynamics.SubBehaviourCOM::get_centerOfPressure()
extern void SubBehaviourCOM_get_centerOfPressure_m9CE233ECE9E7CA1D1A8E5C50ED821EE066295FE5 ();
// 0x00000202 System.Void RootMotion.Dynamics.SubBehaviourCOM::set_centerOfPressure(UnityEngine.Vector3)
extern void SubBehaviourCOM_set_centerOfPressure_m9C3665EDFABA1DEDFAB4DD1F4EE545D1B50D5891 ();
// 0x00000203 UnityEngine.Quaternion RootMotion.Dynamics.SubBehaviourCOM::get_rotation()
extern void SubBehaviourCOM_get_rotation_mD3E6E61B86DFC9873F9010C606FFC518D6CF9D34 ();
// 0x00000204 System.Void RootMotion.Dynamics.SubBehaviourCOM::set_rotation(UnityEngine.Quaternion)
extern void SubBehaviourCOM_set_rotation_mBCD8EBF866FFD0EE481DC28A54A3FB246D1E37C4 ();
// 0x00000205 UnityEngine.Quaternion RootMotion.Dynamics.SubBehaviourCOM::get_inverseRotation()
extern void SubBehaviourCOM_get_inverseRotation_mAEF2E6DF0569050C9641017114473D416F3CBA8E ();
// 0x00000206 System.Void RootMotion.Dynamics.SubBehaviourCOM::set_inverseRotation(UnityEngine.Quaternion)
extern void SubBehaviourCOM_set_inverseRotation_m53CE2D3E8284A098F08CF71FAC64B542427428DE ();
// 0x00000207 System.Boolean RootMotion.Dynamics.SubBehaviourCOM::get_isGrounded()
extern void SubBehaviourCOM_get_isGrounded_m2437729FE6FCC9668F4E3D38D29AD3C45F33B58C ();
// 0x00000208 System.Void RootMotion.Dynamics.SubBehaviourCOM::set_isGrounded(System.Boolean)
extern void SubBehaviourCOM_set_isGrounded_m62E50301498886C8E49488657EE83EDB62A944C2 ();
// 0x00000209 System.Single RootMotion.Dynamics.SubBehaviourCOM::get_lastGroundedTime()
extern void SubBehaviourCOM_get_lastGroundedTime_m751FFE463E53CB6D5D6769E543DCC305BF7FDA70 ();
// 0x0000020A System.Void RootMotion.Dynamics.SubBehaviourCOM::set_lastGroundedTime(System.Single)
extern void SubBehaviourCOM_set_lastGroundedTime_m35B0AB876B37D817D97C1E6ED14CC6678AB8FC3F ();
// 0x0000020B System.Void RootMotion.Dynamics.SubBehaviourCOM::Initiate(RootMotion.Dynamics.BehaviourBase,UnityEngine.LayerMask)
extern void SubBehaviourCOM_Initiate_m1B74EACD1CB7AEB08E6EA3D865F2BCE7A355E792 ();
// 0x0000020C System.Void RootMotion.Dynamics.SubBehaviourCOM::OnHierarchyChanged()
extern void SubBehaviourCOM_OnHierarchyChanged_mDE1E59D1D594FFA258778F347EF82FA2E6E436B1 ();
// 0x0000020D System.Void RootMotion.Dynamics.SubBehaviourCOM::OnPreMuscleCollision(RootMotion.Dynamics.MuscleCollision)
extern void SubBehaviourCOM_OnPreMuscleCollision_m5D88478AF9C34016219AD5FCDF4AA6A736AC4544 ();
// 0x0000020E System.Void RootMotion.Dynamics.SubBehaviourCOM::OnPreMuscleCollisionExit(RootMotion.Dynamics.MuscleCollision)
extern void SubBehaviourCOM_OnPreMuscleCollisionExit_m413A58810513B27C1AE9C47731CEC2A0C3EB5504 ();
// 0x0000020F System.Void RootMotion.Dynamics.SubBehaviourCOM::OnPreActivate()
extern void SubBehaviourCOM_OnPreActivate_mCB622726936614D6EFD9968B3ECC40134E192E48 ();
// 0x00000210 System.Void RootMotion.Dynamics.SubBehaviourCOM::OnPreLateUpdate()
extern void SubBehaviourCOM_OnPreLateUpdate_mE1BB34F16274A3046411BE5AC3DF3BA1416CF8CF ();
// 0x00000211 System.Void RootMotion.Dynamics.SubBehaviourCOM::OnPreDeactivate()
extern void SubBehaviourCOM_OnPreDeactivate_mB24950495359768733323202E0FF8FD704127CF3 ();
// 0x00000212 UnityEngine.Vector3 RootMotion.Dynamics.SubBehaviourCOM::GetCollisionCOP(UnityEngine.Collision)
extern void SubBehaviourCOM_GetCollisionCOP_mBC279D49965450470003FFD837843BEBD1741D56 ();
// 0x00000213 System.Boolean RootMotion.Dynamics.SubBehaviourCOM::IsGrounded()
extern void SubBehaviourCOM_IsGrounded_mC24230F6FDD790F97F1EEC20E2D59219929760D6 ();
// 0x00000214 UnityEngine.Vector3 RootMotion.Dynamics.SubBehaviourCOM::GetCenterOfMass()
extern void SubBehaviourCOM_GetCenterOfMass_m842AB085A1CF0E4527DA9D1C47189B1521B0825E ();
// 0x00000215 UnityEngine.Vector3 RootMotion.Dynamics.SubBehaviourCOM::GetCenterOfMassVelocity()
extern void SubBehaviourCOM_GetCenterOfMassVelocity_mC96E0CA88CBDBE90B581FE8EE7F9808DFB5CC6F7 ();
// 0x00000216 UnityEngine.Vector3 RootMotion.Dynamics.SubBehaviourCOM::GetMomentum()
extern void SubBehaviourCOM_GetMomentum_m5453FD7337814D8974F08EE79185FB0DC1A6889C ();
// 0x00000217 UnityEngine.Vector3 RootMotion.Dynamics.SubBehaviourCOM::GetCenterOfPressure()
extern void SubBehaviourCOM_GetCenterOfPressure_m86E1D8EA5CC5D398EE91CCB3869CBD70F9C5AE37 ();
// 0x00000218 UnityEngine.Vector3 RootMotion.Dynamics.SubBehaviourCOM::GetFeetCentroid()
extern void SubBehaviourCOM_GetFeetCentroid_m1605B398B99CFEE09E36F82060DEE94B6628527B ();
// 0x00000219 System.Void RootMotion.Dynamics.SubBehaviourCOM::.ctor()
extern void SubBehaviourCOM__ctor_m02048379561EF8EEAA0D56AF2CC1D4C992BFB214 ();
// 0x0000021A System.Void RootMotion.Dynamics.Booster::Boost(RootMotion.Dynamics.BehaviourPuppet)
extern void Booster_Boost_m7398D6C1616A3817EC1072BA4D2C40CB8A70D044 ();
// 0x0000021B System.Void RootMotion.Dynamics.Booster::.ctor()
extern void Booster__ctor_mF09152A4D153897027A2B5FDDF1AFAB05872931C ();
// 0x0000021C System.Void RootMotion.Dynamics.JointBreakBroadcaster::OnJointBreak()
extern void JointBreakBroadcaster_OnJointBreak_mEE0F29F1608732D2BA61F21B0771860BD8099A11 ();
// 0x0000021D System.Void RootMotion.Dynamics.JointBreakBroadcaster::.ctor()
extern void JointBreakBroadcaster__ctor_m21DE2DA542311F591F35CDFF0B86C3FB88BA4454 ();
// 0x0000021E UnityEngine.Transform RootMotion.Dynamics.Muscle::get_transform()
extern void Muscle_get_transform_m0B8E9578F734C48EA796719CAA9D7FD29FA256DF ();
// 0x0000021F System.Void RootMotion.Dynamics.Muscle::set_transform(UnityEngine.Transform)
extern void Muscle_set_transform_m98649520C3E3FF3DE348842C34EB83773EFD909D ();
// 0x00000220 UnityEngine.Rigidbody RootMotion.Dynamics.Muscle::get_rigidbody()
extern void Muscle_get_rigidbody_m51E9F00CF3CEBF26C226458E09846B6D01A46D9C ();
// 0x00000221 System.Void RootMotion.Dynamics.Muscle::set_rigidbody(UnityEngine.Rigidbody)
extern void Muscle_set_rigidbody_m37AC05BFD444E7C68A0D0891D8522817EA4B0669 ();
// 0x00000222 UnityEngine.Transform RootMotion.Dynamics.Muscle::get_connectedBodyTarget()
extern void Muscle_get_connectedBodyTarget_m99C2266EF1D0A966367528816D2224D136D483CF ();
// 0x00000223 System.Void RootMotion.Dynamics.Muscle::set_connectedBodyTarget(UnityEngine.Transform)
extern void Muscle_set_connectedBodyTarget_m301820C4A66D3C1E1EFB8F0D7B421FAFB370CDD9 ();
// 0x00000224 UnityEngine.Vector3 RootMotion.Dynamics.Muscle::get_targetAnimatedPosition()
extern void Muscle_get_targetAnimatedPosition_m040A92DB83F1C8B8CF4983AB960AAF0771EE4461 ();
// 0x00000225 System.Void RootMotion.Dynamics.Muscle::set_targetAnimatedPosition(UnityEngine.Vector3)
extern void Muscle_set_targetAnimatedPosition_mCE4A4CA1B1FAB59CE22D68B95F462F736831D316 ();
// 0x00000226 UnityEngine.Collider[] RootMotion.Dynamics.Muscle::get_colliders()
extern void Muscle_get_colliders_mB88A027B42979C40E2CEEB01C5090FEB1D55E275 ();
// 0x00000227 UnityEngine.Vector3 RootMotion.Dynamics.Muscle::get_targetVelocity()
extern void Muscle_get_targetVelocity_mA5AAF13D5DECEB29CF86FD1BF98562C9CFF79FB3 ();
// 0x00000228 System.Void RootMotion.Dynamics.Muscle::set_targetVelocity(UnityEngine.Vector3)
extern void Muscle_set_targetVelocity_m2741F9ECA14FD53F168A0C6820254AF0DECE6689 ();
// 0x00000229 UnityEngine.Vector3 RootMotion.Dynamics.Muscle::get_targetAngularVelocity()
extern void Muscle_get_targetAngularVelocity_m4D25311475C4DFEE1FF0775CBAF7FE59FE271726 ();
// 0x0000022A System.Void RootMotion.Dynamics.Muscle::set_targetAngularVelocity(UnityEngine.Vector3)
extern void Muscle_set_targetAngularVelocity_mF14D93671870E70E4893964A2F28D8082BBBC78A ();
// 0x0000022B UnityEngine.Quaternion RootMotion.Dynamics.Muscle::get_targetRotationRelative()
extern void Muscle_get_targetRotationRelative_m92506E5EE779F825497B389FDA2B8580167783BE ();
// 0x0000022C System.Void RootMotion.Dynamics.Muscle::set_targetRotationRelative(UnityEngine.Quaternion)
extern void Muscle_set_targetRotationRelative_m345D5921645E4B6C2C75E910A9F7161E29385E22 ();
// 0x0000022D System.Boolean RootMotion.Dynamics.Muscle::IsValid(System.Boolean)
extern void Muscle_IsValid_m0AD77740C982B5551DFE5D486DC1BC57C3D57E2D ();
// 0x0000022E UnityEngine.Rigidbody RootMotion.Dynamics.Muscle::get_rebuildConnectedBody()
extern void Muscle_get_rebuildConnectedBody_m928A993CF6C2A0898AA0C50DD4BC9B86857EC0BC ();
// 0x0000022F System.Void RootMotion.Dynamics.Muscle::set_rebuildConnectedBody(UnityEngine.Rigidbody)
extern void Muscle_set_rebuildConnectedBody_mFD8FE17D466BAB516826249372FAD769F3F6C43D ();
// 0x00000230 UnityEngine.Transform RootMotion.Dynamics.Muscle::get_rebuildTargetParent()
extern void Muscle_get_rebuildTargetParent_m61D8BDA49EF7B40C8578F417BD4F9075679FF563 ();
// 0x00000231 System.Void RootMotion.Dynamics.Muscle::set_rebuildTargetParent(UnityEngine.Transform)
extern void Muscle_set_rebuildTargetParent_mDB5D748006B5E1EF3551E898FFD7C3CC69E79078 ();
// 0x00000232 System.Void RootMotion.Dynamics.Muscle::Rebuild()
extern void Muscle_Rebuild_m4E2526AAA8F77A77BC669484BB7A1E71F765A0BD ();
// 0x00000233 System.Void RootMotion.Dynamics.Muscle::Initiate(RootMotion.Dynamics.Muscle[])
extern void Muscle_Initiate_mC3F1F4E91FC2B81CD3A02782017BF216A6990467 ();
// 0x00000234 System.Void RootMotion.Dynamics.Muscle::UpdateColliders()
extern void Muscle_UpdateColliders_m576656664B9E2B8E2902E6B3BA1BC6CB56E8410D ();
// 0x00000235 System.Void RootMotion.Dynamics.Muscle::DisableColliders()
extern void Muscle_DisableColliders_m614DA544A2D63430A166CCDF32057DE46BE83DB0 ();
// 0x00000236 System.Void RootMotion.Dynamics.Muscle::EnableColliders()
extern void Muscle_EnableColliders_m777E63C7599493C6FFE6DD7AC08F5D95D9085FC1 ();
// 0x00000237 System.Void RootMotion.Dynamics.Muscle::AddColliders(UnityEngine.Transform,UnityEngine.Collider[]&,System.Boolean)
extern void Muscle_AddColliders_mD23E2A6F485268904946A8A12D9D4759ED4E3C85 ();
// 0x00000238 System.Void RootMotion.Dynamics.Muscle::AddCompoundColliders(UnityEngine.Transform,UnityEngine.Collider[]&)
extern void Muscle_AddCompoundColliders_mC55C4777B43B14E340A06B728CE008049AED6C29 ();
// 0x00000239 System.Void RootMotion.Dynamics.Muscle::IgnoreCollisions(RootMotion.Dynamics.Muscle,System.Boolean)
extern void Muscle_IgnoreCollisions_m32C36405F51BE18F683887ADCD010405FEF316E0 ();
// 0x0000023A System.Void RootMotion.Dynamics.Muscle::IgnoreAngularLimits(System.Boolean)
extern void Muscle_IgnoreAngularLimits_m5BD77F7543414D743871B6E8B53A0BCA15172D9A ();
// 0x0000023B System.Void RootMotion.Dynamics.Muscle::FixTargetTransforms()
extern void Muscle_FixTargetTransforms_mE21331F360F13C2CFDE1C4427188150F87176626 ();
// 0x0000023C System.Void RootMotion.Dynamics.Muscle::Reset()
extern void Muscle_Reset_m5C0F2A1820C88E4A4AF0E733D1A8951D2094C8EE ();
// 0x0000023D System.Void RootMotion.Dynamics.Muscle::MoveToTarget()
extern void Muscle_MoveToTarget_m8EB6AE016AB3B90425247A3C0216C4DEBAAD5FE9 ();
// 0x0000023E System.Void RootMotion.Dynamics.Muscle::Read()
extern void Muscle_Read_m574324194596A24DF8DBBCF002243897E7344980 ();
// 0x0000023F System.Void RootMotion.Dynamics.Muscle::ClearVelocities()
extern void Muscle_ClearVelocities_m94EEE3D62C1D26CCA49B3CBC8899A25205BB3997 ();
// 0x00000240 System.Void RootMotion.Dynamics.Muscle::UpdateAnchor(System.Boolean)
extern void Muscle_UpdateAnchor_m6D0E5AD9DD72B972306D222969BA2657592BB4C1 ();
// 0x00000241 System.Void RootMotion.Dynamics.Muscle::Update(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Boolean,System.Single)
extern void Muscle_Update_m1420719D753F91EC61036B4B3DB8E7EF7751F1F2 ();
// 0x00000242 System.Void RootMotion.Dynamics.Muscle::Map(System.Single)
extern void Muscle_Map_mCF47D85786318CE3F3A6909845A9EBED6F2ADBA8 ();
// 0x00000243 System.Void RootMotion.Dynamics.Muscle::CalculateMappedVelocity()
extern void Muscle_CalculateMappedVelocity_m53AFB03EAE23FE1EA3E12B7B7F2E6789A0BE833F ();
// 0x00000244 System.Void RootMotion.Dynamics.Muscle::Pin(System.Single,System.Single,System.Single,System.Single)
extern void Muscle_Pin_m7183D534A394102D88D2F665B82D81410601D4DD ();
// 0x00000245 System.Void RootMotion.Dynamics.Muscle::MuscleRotation(System.Single,System.Single,System.Single)
extern void Muscle_MuscleRotation_m279399D0EA90BF8354271A78E67A44B29101C3A9 ();
// 0x00000246 UnityEngine.Quaternion RootMotion.Dynamics.Muscle::get_localRotation()
extern void Muscle_get_localRotation_mD7DFEE6491C2F6B942C7281FDC4398B014483A08 ();
// 0x00000247 UnityEngine.Quaternion RootMotion.Dynamics.Muscle::get_parentRotation()
extern void Muscle_get_parentRotation_m2052C7E148F78346166494A463D965FA7D1BB8C8 ();
// 0x00000248 UnityEngine.Quaternion RootMotion.Dynamics.Muscle::get_targetParentRotation()
extern void Muscle_get_targetParentRotation_mE4EC0DC3FC87A1DA6A89D2EE4C50F79891B578F6 ();
// 0x00000249 UnityEngine.Quaternion RootMotion.Dynamics.Muscle::get_targetLocalRotation()
extern void Muscle_get_targetLocalRotation_mA13C51DAA502023B9BECF67ACD76C25BC4B44D13 ();
// 0x0000024A UnityEngine.Quaternion RootMotion.Dynamics.Muscle::LocalToJointSpace(UnityEngine.Quaternion)
extern void Muscle_LocalToJointSpace_mEC0AC8303FA842A31909475F9A0A595A591C9510 ();
// 0x0000024B UnityEngine.Vector3 RootMotion.Dynamics.Muscle::InverseTransformPointUnscaled(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3)
extern void Muscle_InverseTransformPointUnscaled_mCCB03A739F317D74AFBD6CB81CCFF7972BADFE10 ();
// 0x0000024C UnityEngine.Vector3 RootMotion.Dynamics.Muscle::CalculateInertiaTensorCuboid(UnityEngine.Vector3,System.Single)
extern void Muscle_CalculateInertiaTensorCuboid_mE7FA9FD8D31DD5A4C5525DD54021C0C65105C6AB ();
// 0x0000024D System.Void RootMotion.Dynamics.Muscle::.ctor()
extern void Muscle__ctor_m01E12C68FEDE53B9AA2F043E164D98B94C4C4A6B ();
// 0x0000024E System.Void RootMotion.Dynamics.MuscleCollision::.ctor(System.Int32,UnityEngine.Collision,System.Boolean)
extern void MuscleCollision__ctor_m1AEC27F36070583B949FCB0C874FEC9801FDB51F_AdjustorThunk ();
// 0x0000024F System.Void RootMotion.Dynamics.MuscleHit::.ctor(System.Int32,System.Single,UnityEngine.Vector3,UnityEngine.Vector3)
extern void MuscleHit__ctor_m8B50A0D9B4A1C2BBB6DEC0C4D5BEA62CBBDCD3D4_AdjustorThunk ();
// 0x00000250 System.Void RootMotion.Dynamics.MuscleCollisionBroadcaster::Hit(System.Single,UnityEngine.Vector3,UnityEngine.Vector3)
extern void MuscleCollisionBroadcaster_Hit_m6FBE9230BEB4737066FE1C78A6A2ED6A44684192 ();
// 0x00000251 System.Void RootMotion.Dynamics.MuscleCollisionBroadcaster::OnCollisionEnter(UnityEngine.Collision)
extern void MuscleCollisionBroadcaster_OnCollisionEnter_mFC7F6E0C12F9C2F860AEDEB9223BFB816F6D256F ();
// 0x00000252 System.Void RootMotion.Dynamics.MuscleCollisionBroadcaster::OnCollisionStay(UnityEngine.Collision)
extern void MuscleCollisionBroadcaster_OnCollisionStay_m3623ACCA1947889494312B89B7C74AA2F94EE60A ();
// 0x00000253 System.Void RootMotion.Dynamics.MuscleCollisionBroadcaster::OnCollisionExit(UnityEngine.Collision)
extern void MuscleCollisionBroadcaster_OnCollisionExit_m2007950C053A14D7F1E06A861DD1C0691FD419AC ();
// 0x00000254 System.Void RootMotion.Dynamics.MuscleCollisionBroadcaster::.ctor()
extern void MuscleCollisionBroadcaster__ctor_m865A8270B63D132D684A9F837ED0EA3DC010A27D ();
// 0x00000255 System.Void RootMotion.Dynamics.PhysXTools::Predict(UnityEngine.Rigidbody,System.Int32,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern void PhysXTools_Predict_mA46BB1ABBBBDA540F985D56E63A80706BB4807EB ();
// 0x00000256 System.Void RootMotion.Dynamics.PhysXTools::Predict(UnityEngine.Rigidbody,System.Int32,UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Vector3,System.Single,System.Single)
extern void PhysXTools_Predict_m1D256121A1F9D48E5C9B3B8C69570572D71E7118 ();
// 0x00000257 System.Void RootMotion.Dynamics.PhysXTools::Predict(UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3,System.Single,System.Single)
extern void PhysXTools_Predict_m3B415490C96EAEB6E6B821797ADD2E9CA8D35024 ();
// 0x00000258 UnityEngine.Vector3 RootMotion.Dynamics.PhysXTools::GetCenterOfMass(RootMotion.Dynamics.PuppetMaster)
extern void PhysXTools_GetCenterOfMass_m028CE20624AFFD1A58F39BC7BD836781977C85D4 ();
// 0x00000259 UnityEngine.Vector3 RootMotion.Dynamics.PhysXTools::GetCenterOfMass(UnityEngine.Rigidbody[])
extern void PhysXTools_GetCenterOfMass_m43E9540851C37C8059B0BA72B0F30AEB66EEEDD5 ();
// 0x0000025A UnityEngine.Vector3 RootMotion.Dynamics.PhysXTools::GetCenterOfMassVelocity(UnityEngine.Rigidbody[])
extern void PhysXTools_GetCenterOfMassVelocity_mD02A65C58851EDCE99C3826307184E61C1D61F3B ();
// 0x0000025B System.Void RootMotion.Dynamics.PhysXTools::DivByInertia(UnityEngine.Vector3&,UnityEngine.Quaternion,UnityEngine.Vector3)
extern void PhysXTools_DivByInertia_mFDB14AF0E9BBD006D7B8ABC20FEC8EA053E333E5 ();
// 0x0000025C System.Void RootMotion.Dynamics.PhysXTools::ScaleByInertia(UnityEngine.Vector3&,UnityEngine.Quaternion,UnityEngine.Vector3)
extern void PhysXTools_ScaleByInertia_m5424D460BFA3BC2EF9C5E51B31123F5CEE572F31 ();
// 0x0000025D UnityEngine.Vector3 RootMotion.Dynamics.PhysXTools::GetFromToAcceleration(UnityEngine.Vector3,UnityEngine.Vector3)
extern void PhysXTools_GetFromToAcceleration_m3773C1D065F6BD04D287500B94F3DC8D59E694B1 ();
// 0x0000025E UnityEngine.Vector3 RootMotion.Dynamics.PhysXTools::GetAngularAcceleration(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern void PhysXTools_GetAngularAcceleration_m3510C50B9778C06000A1CC059EC3B6708B6E0960 ();
// 0x0000025F System.Void RootMotion.Dynamics.PhysXTools::AddFromToTorque(UnityEngine.Rigidbody,UnityEngine.Quaternion,UnityEngine.ForceMode)
extern void PhysXTools_AddFromToTorque_m8F7D2EA5531EBD81482F41578DC685E15FF1F0B5 ();
// 0x00000260 System.Void RootMotion.Dynamics.PhysXTools::AddFromToTorque(UnityEngine.Rigidbody,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.ForceMode)
extern void PhysXTools_AddFromToTorque_m1029F51AF235C06737B7F9422D9FECEE9D7EA5DB ();
// 0x00000261 System.Void RootMotion.Dynamics.PhysXTools::AddFromToForce(UnityEngine.Rigidbody,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.ForceMode)
extern void PhysXTools_AddFromToForce_m610EE2C9EFB650C6DD81C06B59E417001E4E659B ();
// 0x00000262 UnityEngine.Vector3 RootMotion.Dynamics.PhysXTools::GetLinearAcceleration(UnityEngine.Vector3,UnityEngine.Vector3)
extern void PhysXTools_GetLinearAcceleration_m863CA49E0C00D4A8EBDBE15C785F8CCE97A7C534 ();
// 0x00000263 UnityEngine.Quaternion RootMotion.Dynamics.PhysXTools::ToJointSpace(UnityEngine.ConfigurableJoint)
extern void PhysXTools_ToJointSpace_mB28D3A889A49F2079ABF23922108D31E450670F3 ();
// 0x00000264 UnityEngine.Vector3 RootMotion.Dynamics.PhysXTools::CalculateInertiaTensorCuboid(UnityEngine.Vector3,System.Single)
extern void PhysXTools_CalculateInertiaTensorCuboid_m55F1C9F036A7908397CA51C3E3A0550D55190ACE ();
// 0x00000265 UnityEngine.Vector3 RootMotion.Dynamics.PhysXTools::Div(UnityEngine.Vector3,UnityEngine.Vector3)
extern void PhysXTools_Div_m281C283FC52E36984A6769CF6F3A36EAAF781EA4 ();
// 0x00000266 UnityEngine.Vector3 RootMotion.Dynamics.PressureSensor::get_center()
extern void PressureSensor_get_center_m080FB7E4B70D8493FC5BF277CBE9724F6A589DE7 ();
// 0x00000267 System.Void RootMotion.Dynamics.PressureSensor::set_center(UnityEngine.Vector3)
extern void PressureSensor_set_center_m8B2269B72CB68817060B89535F8B859F823361EC ();
// 0x00000268 System.Boolean RootMotion.Dynamics.PressureSensor::get_inContact()
extern void PressureSensor_get_inContact_mB82077BD03DBED2F456E7A4D8DC2CD373136DF22 ();
// 0x00000269 System.Void RootMotion.Dynamics.PressureSensor::set_inContact(System.Boolean)
extern void PressureSensor_set_inContact_mB9BCABDA40101E7F542DE52EC0E75B2439C07398 ();
// 0x0000026A UnityEngine.Vector3 RootMotion.Dynamics.PressureSensor::get_bottom()
extern void PressureSensor_get_bottom_m14FA0762E9C65A3D0F5CAB49422E95ECF9F5876F ();
// 0x0000026B System.Void RootMotion.Dynamics.PressureSensor::set_bottom(UnityEngine.Vector3)
extern void PressureSensor_set_bottom_m70EECD64D6ED13C93EDEED75A18308BFB8B5149C ();
// 0x0000026C UnityEngine.Rigidbody RootMotion.Dynamics.PressureSensor::get_r()
extern void PressureSensor_get_r_m2AAF6E1F7B755D87E9FAEDA940E3D384098299AF ();
// 0x0000026D System.Void RootMotion.Dynamics.PressureSensor::set_r(UnityEngine.Rigidbody)
extern void PressureSensor_set_r_m904BAC2981E06FA01DA0EA878A7B173A05858FE7 ();
// 0x0000026E System.Void RootMotion.Dynamics.PressureSensor::Awake()
extern void PressureSensor_Awake_m55EEB328A358C6233163B328329B11E497217A32 ();
// 0x0000026F System.Void RootMotion.Dynamics.PressureSensor::OnCollisionEnter(UnityEngine.Collision)
extern void PressureSensor_OnCollisionEnter_m2B120DD85BF52E30C379D44AAC6FE29828DCB5B6 ();
// 0x00000270 System.Void RootMotion.Dynamics.PressureSensor::OnCollisionStay(UnityEngine.Collision)
extern void PressureSensor_OnCollisionStay_m6B9DCCF4C7F5429DBF09EB5664C80C48C0A408BA ();
// 0x00000271 System.Void RootMotion.Dynamics.PressureSensor::OnCollisionExit(UnityEngine.Collision)
extern void PressureSensor_OnCollisionExit_m0E8CCE90A8DB5B430255C914A78B40815C64CBF0 ();
// 0x00000272 System.Void RootMotion.Dynamics.PressureSensor::FixedUpdate()
extern void PressureSensor_FixedUpdate_m2B0AD171C462D9E398C57041BF5BDB865CAFAD6B ();
// 0x00000273 System.Void RootMotion.Dynamics.PressureSensor::LateUpdate()
extern void PressureSensor_LateUpdate_m141AF1E6434DFCC3C174E065F4FE81683F9094B7 ();
// 0x00000274 System.Void RootMotion.Dynamics.PressureSensor::ProcessCollision(UnityEngine.Collision)
extern void PressureSensor_ProcessCollision_mC238E22D0CFF37F713ED52F4A2DC38148A098D7C ();
// 0x00000275 System.Void RootMotion.Dynamics.PressureSensor::OnDrawGizmos()
extern void PressureSensor_OnDrawGizmos_m4E310773FBB782623BCEA85426934D4235C8DCE5 ();
// 0x00000276 System.Void RootMotion.Dynamics.PressureSensor::.ctor()
extern void PressureSensor__ctor_mDA5D0D57F29E9E3759888D84A849BF381D001396 ();
// 0x00000277 System.Boolean RootMotion.Dynamics.Prop::get_isPickedUp()
extern void Prop_get_isPickedUp_m4819577624EEE994B0FBB6DBD62F25A7E2B9BF1E ();
// 0x00000278 RootMotion.Dynamics.PropRoot RootMotion.Dynamics.Prop::get_propRoot()
extern void Prop_get_propRoot_m20B85350A49AFE0BB3B1D847EE7E2EEF4BC838D4 ();
// 0x00000279 System.Void RootMotion.Dynamics.Prop::set_propRoot(RootMotion.Dynamics.PropRoot)
extern void Prop_set_propRoot_m225C639CAACE7BEAF9278F96B58429DDC6385375 ();
// 0x0000027A System.Void RootMotion.Dynamics.Prop::PickUp(RootMotion.Dynamics.PropRoot)
extern void Prop_PickUp_m00D7A6175CD046432433B83233C4A8924A5A3C8A ();
// 0x0000027B System.Void RootMotion.Dynamics.Prop::Drop()
extern void Prop_Drop_mA69DD8680B29F5846316C86BAC4FCF153AC7D7B3 ();
// 0x0000027C System.Void RootMotion.Dynamics.Prop::StartPickedUp(RootMotion.Dynamics.PropRoot)
extern void Prop_StartPickedUp_m95963E75DD8CDF55B1E67CAEF1F281B00790AFEE ();
// 0x0000027D System.Void RootMotion.Dynamics.Prop::OnPickUp(RootMotion.Dynamics.PropRoot)
extern void Prop_OnPickUp_m928179EAC8B00DA4905DAAE621E61B17688C3E22 ();
// 0x0000027E System.Void RootMotion.Dynamics.Prop::OnDrop()
extern void Prop_OnDrop_mAA8BE2564A25660178C336BC66570E0984454674 ();
// 0x0000027F System.Void RootMotion.Dynamics.Prop::OnStart()
extern void Prop_OnStart_m9F364D3619D60C53BC6C809ED7EAA8CC2E09A5B6 ();
// 0x00000280 System.Void RootMotion.Dynamics.Prop::Start()
extern void Prop_Start_mE4D76AA644A9C8A11797B0E4351402785B1B519D ();
// 0x00000281 System.Void RootMotion.Dynamics.Prop::ReleaseJoint()
extern void Prop_ReleaseJoint_m8BD78B6D365F8F07D98054563F7E8A145963B713 ();
// 0x00000282 System.Void RootMotion.Dynamics.Prop::OnDrawGizmos()
extern void Prop_OnDrawGizmos_mDB5EEDBBABF46A85C360019A87487267504DFA57 ();
// 0x00000283 System.Void RootMotion.Dynamics.Prop::.ctor()
extern void Prop__ctor_m48127381A18340489B7E798F62EC809EC6B7ECAF ();
// 0x00000284 System.Void RootMotion.Dynamics.PropRoot::OpenUserManual()
extern void PropRoot_OpenUserManual_m6A9467480DD5FCE20905A19467E3EF28B34DD6C4 ();
// 0x00000285 System.Void RootMotion.Dynamics.PropRoot::OpenScriptReference()
extern void PropRoot_OpenScriptReference_mAE170E27379ADB0A7F240B81EC5ED31BB9F9DCD9 ();
// 0x00000286 System.Void RootMotion.Dynamics.PropRoot::DropImmediate()
extern void PropRoot_DropImmediate_m36E7D7A141968CB637ECDAA5F1EEF03A2F115201 ();
// 0x00000287 System.Void RootMotion.Dynamics.PropRoot::Awake()
extern void PropRoot_Awake_mBEF25FD3033A73E9FB4FE5BF328EE0E1BC633EAE ();
// 0x00000288 System.Void RootMotion.Dynamics.PropRoot::Update()
extern void PropRoot_Update_m19411501D06C07F31234194D3AD07AFDD53A4F17 ();
// 0x00000289 System.Void RootMotion.Dynamics.PropRoot::FixedUpdate()
extern void PropRoot_FixedUpdate_mA45D73D4F5F65B5ECAFA46C00F9480EF88F0889A ();
// 0x0000028A System.Void RootMotion.Dynamics.PropRoot::AttachProp(RootMotion.Dynamics.Prop)
extern void PropRoot_AttachProp_mEFA22183C4136D6160A2FA172B943BB530A6D627 ();
// 0x0000028B System.Void RootMotion.Dynamics.PropRoot::.ctor()
extern void PropRoot__ctor_m3150D88A9CFC2892C363000E70869DA12B817F72 ();
// 0x0000028C System.Void RootMotion.Dynamics.PropTemplate::OnStart()
extern void PropTemplate_OnStart_m5796CE422656A394261EE7A8D6D923459B4E0434 ();
// 0x0000028D System.Void RootMotion.Dynamics.PropTemplate::OnPickUp(RootMotion.Dynamics.PropRoot)
extern void PropTemplate_OnPickUp_m7100AB7C3B7FE16C2F632D71EF4ED75A6B2B6625 ();
// 0x0000028E System.Void RootMotion.Dynamics.PropTemplate::OnDrop()
extern void PropTemplate_OnDrop_m8A9987990F286ADBB0226596C4443928DA347572 ();
// 0x0000028F System.Void RootMotion.Dynamics.PropTemplate::.ctor()
extern void PropTemplate__ctor_mF39BB624DD9AA34E63F54173D22C925F3A0173C3 ();
// 0x00000290 System.Void RootMotion.Dynamics.PuppetMaster::OpenUserManualSetup()
extern void PuppetMaster_OpenUserManualSetup_mAFC2ED82D46BCC033A9D6A0F06DB68DA530E7877 ();
// 0x00000291 System.Void RootMotion.Dynamics.PuppetMaster::OpenUserManualComponent()
extern void PuppetMaster_OpenUserManualComponent_mEBA24733FEC617C53C4FF45D879CE455ACAD5413 ();
// 0x00000292 System.Void RootMotion.Dynamics.PuppetMaster::OpenUserManualPerformance()
extern void PuppetMaster_OpenUserManualPerformance_mB573DEFDBED75034A92BD2D577BB41804F0F09DF ();
// 0x00000293 System.Void RootMotion.Dynamics.PuppetMaster::OpenScriptReference()
extern void PuppetMaster_OpenScriptReference_m71CA7EE6E8A8479A2D672834027459C582ABF0B0 ();
// 0x00000294 System.Void RootMotion.Dynamics.PuppetMaster::OpenSetupTutorial()
extern void PuppetMaster_OpenSetupTutorial_m1965E01622012451D8D12A26E91BD6DB8B7B25C5 ();
// 0x00000295 System.Void RootMotion.Dynamics.PuppetMaster::OpenComponentTutorial()
extern void PuppetMaster_OpenComponentTutorial_mB12B99F1D4370D7AF52C44A00B531FDDE38A344D ();
// 0x00000296 System.Void RootMotion.Dynamics.PuppetMaster::ResetStateSettings()
extern void PuppetMaster_ResetStateSettings_m3814E898E9D9B0501BA4AFDD006FF8A3A09603A2 ();
// 0x00000297 UnityEngine.Animator RootMotion.Dynamics.PuppetMaster::get_targetAnimator()
extern void PuppetMaster_get_targetAnimator_m47D08CF8F8093BCEDBFFA95CA6731F9B4D95DEB7 ();
// 0x00000298 System.Void RootMotion.Dynamics.PuppetMaster::set_targetAnimator(UnityEngine.Animator)
extern void PuppetMaster_set_targetAnimator_m166E631CA90B20D74E3EC5FDDBFD84C0B51EDF8C ();
// 0x00000299 UnityEngine.Animation RootMotion.Dynamics.PuppetMaster::get_targetAnimation()
extern void PuppetMaster_get_targetAnimation_mA20302A0BB433A191C29C7926ECA071E106ED375 ();
// 0x0000029A System.Void RootMotion.Dynamics.PuppetMaster::set_targetAnimation(UnityEngine.Animation)
extern void PuppetMaster_set_targetAnimation_mD65DBED2C191E67D45BD7D891006815D963C56DB ();
// 0x0000029B RootMotion.Dynamics.BehaviourBase[] RootMotion.Dynamics.PuppetMaster::get_behaviours()
extern void PuppetMaster_get_behaviours_mC62978107F22A6D3F24957DEA6ACF5BE4D423D60 ();
// 0x0000029C System.Void RootMotion.Dynamics.PuppetMaster::set_behaviours(RootMotion.Dynamics.BehaviourBase[])
extern void PuppetMaster_set_behaviours_m5D0A809F4A185767A4EBD6453D11A75D17FAC582 ();
// 0x0000029D System.Boolean RootMotion.Dynamics.PuppetMaster::get_isActive()
extern void PuppetMaster_get_isActive_m99EBDCA8A7DE96F15DAF3E015A03DAD554E74AEE ();
// 0x0000029E System.Boolean RootMotion.Dynamics.PuppetMaster::get_initiated()
extern void PuppetMaster_get_initiated_m07985C10CCABC30C2AF7A2A8D6C3DCE08414477B ();
// 0x0000029F System.Void RootMotion.Dynamics.PuppetMaster::set_initiated(System.Boolean)
extern void PuppetMaster_set_initiated_m03C1B6A2ABD7359C47E1FA78577ADACBE3AEDBF2 ();
// 0x000002A0 RootMotion.Dynamics.PuppetMaster_UpdateMode RootMotion.Dynamics.PuppetMaster::get_updateMode()
extern void PuppetMaster_get_updateMode_m351C12D72D0017E39E58553ACA7DE7C85EF013BA ();
// 0x000002A1 System.Boolean RootMotion.Dynamics.PuppetMaster::get_controlsAnimator()
extern void PuppetMaster_get_controlsAnimator_mE54E885EA22AAA8F4A68047B8EA51B3446B7D54A ();
// 0x000002A2 System.Boolean RootMotion.Dynamics.PuppetMaster::get_isBlending()
extern void PuppetMaster_get_isBlending_m149920882FE9273183F1503BF3D4E8133A8C748B ();
// 0x000002A3 System.Void RootMotion.Dynamics.PuppetMaster::Teleport(UnityEngine.Vector3,UnityEngine.Quaternion,System.Boolean)
extern void PuppetMaster_Teleport_mE3EB83CF258447A585079363A48B00021EF2EFF1 ();
// 0x000002A4 System.Boolean RootMotion.Dynamics.PuppetMaster::get_autoSimulate()
extern void PuppetMaster_get_autoSimulate_m1E4F089E6A49E2846BA3058AF2FC5F004DE60BC0 ();
// 0x000002A5 System.Void RootMotion.Dynamics.PuppetMaster::OnDisable()
extern void PuppetMaster_OnDisable_m349AD4DD5C76797848D9510A005D9CAC255A9AE5 ();
// 0x000002A6 System.Void RootMotion.Dynamics.PuppetMaster::OnEnable()
extern void PuppetMaster_OnEnable_m64F4C09C0C21E69F58573881CD6C11445B88EAAA ();
// 0x000002A7 System.Void RootMotion.Dynamics.PuppetMaster::Awake()
extern void PuppetMaster_Awake_m9A8AF07A1346C86DF8CE2BAEFAC950824842C320 ();
// 0x000002A8 System.Void RootMotion.Dynamics.PuppetMaster::Start()
extern void PuppetMaster_Start_mF6DC23D68E3674A2C8A62C753B6161EAC66AB23B ();
// 0x000002A9 UnityEngine.Transform RootMotion.Dynamics.PuppetMaster::FindTargetRootRecursive(UnityEngine.Transform)
extern void PuppetMaster_FindTargetRootRecursive_mC3274F4A69762EF531E1A6C4B200C016D307F891 ();
// 0x000002AA System.Void RootMotion.Dynamics.PuppetMaster::Initiate()
extern void PuppetMaster_Initiate_m6475BDFA637BE4A8AC0D57B2C39A2964AB5A5C13 ();
// 0x000002AB System.Void RootMotion.Dynamics.PuppetMaster::ActivateBehaviour(RootMotion.Dynamics.BehaviourBase)
extern void PuppetMaster_ActivateBehaviour_m67EEC414193CC57144DE5AB7B64EA81EC68FBE4F ();
// 0x000002AC System.Void RootMotion.Dynamics.PuppetMaster::OnDestroy()
extern void PuppetMaster_OnDestroy_m05D2F5B5E0B0A6A60B1A15B360ED4ECAD32BB387 ();
// 0x000002AD System.Boolean RootMotion.Dynamics.PuppetMaster::IsInterpolated()
extern void PuppetMaster_IsInterpolated_mDF44D52E0FD70B2613EBF57EAE3507568F37F111 ();
// 0x000002AE System.Void RootMotion.Dynamics.PuppetMaster::OnRebuild()
extern void PuppetMaster_OnRebuild_m736733633EEA4E16268A0EDD543CAA2B5C5694EE ();
// 0x000002AF System.Void RootMotion.Dynamics.PuppetMaster::OnPreSimulate(System.Single)
extern void PuppetMaster_OnPreSimulate_m95A31C31039CE4CF30E162F0008BD8A4F94C0FDF ();
// 0x000002B0 System.Void RootMotion.Dynamics.PuppetMaster::OnPostSimulate()
extern void PuppetMaster_OnPostSimulate_m968432DBA76A780DEC450B70F35AA60BEDB3CA8E ();
// 0x000002B1 System.Void RootMotion.Dynamics.PuppetMaster::FixedUpdate()
extern void PuppetMaster_FixedUpdate_mE311114F12BE7273394DF8313D7E9B273F3C1EC5 ();
// 0x000002B2 System.Void RootMotion.Dynamics.PuppetMaster::Update()
extern void PuppetMaster_Update_m09D8071139E870BB3218AD70403BC1B95B5D1DDC ();
// 0x000002B3 System.Void RootMotion.Dynamics.PuppetMaster::LateUpdate()
extern void PuppetMaster_LateUpdate_m42A74FB8F0CD073A39575E2581F06EDB458FF3A0 ();
// 0x000002B4 System.Void RootMotion.Dynamics.PuppetMaster::OnLateUpdate()
extern void PuppetMaster_OnLateUpdate_mE08ECBCF9927FADFD2E1EE658AC6359746EC046C ();
// 0x000002B5 System.Void RootMotion.Dynamics.PuppetMaster::MoveToTarget()
extern void PuppetMaster_MoveToTarget_mA90222253CD1422D337E0099B2F443921C159460 ();
// 0x000002B6 System.Void RootMotion.Dynamics.PuppetMaster::Read()
extern void PuppetMaster_Read_m7FCBB03BF74AAD3BB9B17D30D601AAADD4F25AE1 ();
// 0x000002B7 System.Void RootMotion.Dynamics.PuppetMaster::FixTargetTransforms()
extern void PuppetMaster_FixTargetTransforms_mD0F746D17BD670B6452B07AC5ADBF8394A31DDDE ();
// 0x000002B8 UnityEngine.AnimatorUpdateMode RootMotion.Dynamics.PuppetMaster::get_targetUpdateMode()
extern void PuppetMaster_get_targetUpdateMode_mE8C266331A70930141E02EFC49A16415B09CED9E ();
// 0x000002B9 System.Void RootMotion.Dynamics.PuppetMaster::VisualizeTargetPose()
extern void PuppetMaster_VisualizeTargetPose_m54D593597ED18FB02AD7648E3AA40C473A313E04 ();
// 0x000002BA System.Void RootMotion.Dynamics.PuppetMaster::VisualizeHierarchy(UnityEngine.Transform,UnityEngine.Color)
extern void PuppetMaster_VisualizeHierarchy_m1DBE413B3ABA04F60D9E0EC6FDE6C417DE0B307A ();
// 0x000002BB System.Void RootMotion.Dynamics.PuppetMaster::SetInternalCollisions(System.Boolean)
extern void PuppetMaster_SetInternalCollisions_m773356588C67E7A37D93D9619DE84C269DA9E3CD ();
// 0x000002BC System.Void RootMotion.Dynamics.PuppetMaster::SetAngularLimits(System.Boolean)
extern void PuppetMaster_SetAngularLimits_m56CC6EBAF183C9BC6BBFE5536A3B08321874C641 ();
// 0x000002BD System.Void RootMotion.Dynamics.PuppetMaster::AddMuscle(UnityEngine.ConfigurableJoint,UnityEngine.Transform,UnityEngine.Rigidbody,UnityEngine.Transform,RootMotion.Dynamics.Muscle_Props,System.Boolean,System.Boolean)
extern void PuppetMaster_AddMuscle_m0C9882D2D6C160E498E0A550CA459F446A4D54C6 ();
// 0x000002BE System.Void RootMotion.Dynamics.PuppetMaster::Rebuild()
extern void PuppetMaster_Rebuild_mE8298A18B6C15301908C938243703BBE4964EDD2 ();
// 0x000002BF System.Void RootMotion.Dynamics.PuppetMaster::RemoveMuscleRecursive(UnityEngine.ConfigurableJoint,System.Boolean,System.Boolean,RootMotion.Dynamics.MuscleRemoveMode)
extern void PuppetMaster_RemoveMuscleRecursive_m7CCC5F4C3BFE9BCA643AB885FBEDC80B47FC87C2 ();
// 0x000002C0 System.Void RootMotion.Dynamics.PuppetMaster::ReplaceMuscle(UnityEngine.ConfigurableJoint,UnityEngine.ConfigurableJoint)
extern void PuppetMaster_ReplaceMuscle_m41181F510DCD04C26DA68A0FE07214E9AA5FD89E ();
// 0x000002C1 System.Void RootMotion.Dynamics.PuppetMaster::SetMuscles(RootMotion.Dynamics.Muscle[])
extern void PuppetMaster_SetMuscles_m905767BA7DAB3E463ADBC75F9B0BC5861F76AFCF ();
// 0x000002C2 System.Void RootMotion.Dynamics.PuppetMaster::DisableMuscleRecursive(UnityEngine.ConfigurableJoint)
extern void PuppetMaster_DisableMuscleRecursive_mB1BB1C6135A02CAFC10A26852ABD9E532552FF7F ();
// 0x000002C3 System.Void RootMotion.Dynamics.PuppetMaster::EnableMuscleRecursive(UnityEngine.ConfigurableJoint)
extern void PuppetMaster_EnableMuscleRecursive_m7C95236108B65DC8EC3E3D9D5127A5098D4EF647 ();
// 0x000002C4 System.Void RootMotion.Dynamics.PuppetMaster::FlattenHierarchy()
extern void PuppetMaster_FlattenHierarchy_m693547E577D7D5F1CF784080E517CC17F2CF0FF8 ();
// 0x000002C5 System.Void RootMotion.Dynamics.PuppetMaster::TreeHierarchy()
extern void PuppetMaster_TreeHierarchy_m41CC7B3E1339CB569B816508AF5D60506730166D ();
// 0x000002C6 System.Void RootMotion.Dynamics.PuppetMaster::FixMusclePositions()
extern void PuppetMaster_FixMusclePositions_m8EADB7598F3DAD7637E36E14BE34321EE5E443A3 ();
// 0x000002C7 System.Void RootMotion.Dynamics.PuppetMaster::AddIndexesRecursive(System.Int32,System.Int32[]&)
extern void PuppetMaster_AddIndexesRecursive_m4A60221A40A1EC28489D34E4A3A805377D009870 ();
// 0x000002C8 System.Boolean RootMotion.Dynamics.PuppetMaster::HierarchyIsFlat()
extern void PuppetMaster_HierarchyIsFlat_mC14BC5DDCA5E0E35EC71E20BCF02DD62D0850CF7 ();
// 0x000002C9 System.Void RootMotion.Dynamics.PuppetMaster::DisconnectJoint(UnityEngine.ConfigurableJoint)
extern void PuppetMaster_DisconnectJoint_m56C5141E12203492F61FE557B82BA4427149C09A ();
// 0x000002CA System.Void RootMotion.Dynamics.PuppetMaster::KillJoint(UnityEngine.ConfigurableJoint)
extern void PuppetMaster_KillJoint_m1C351C543D15D35D6E9E0E5B330E1D52ECAE44D8 ();
// 0x000002CB System.Boolean RootMotion.Dynamics.PuppetMaster::get_isSwitchingMode()
extern void PuppetMaster_get_isSwitchingMode_m3F68E43846F90378612B9C696F080F2FCB309BA1 ();
// 0x000002CC System.Void RootMotion.Dynamics.PuppetMaster::set_isSwitchingMode(System.Boolean)
extern void PuppetMaster_set_isSwitchingMode_m2637BAFA0D3038B6572D30DF4D4BA50EFFE7CA64 ();
// 0x000002CD System.Void RootMotion.Dynamics.PuppetMaster::DisableImmediately()
extern void PuppetMaster_DisableImmediately_m20D47A9513823AA5B40815F74A0C0DA9C0B6DDD3 ();
// 0x000002CE System.Void RootMotion.Dynamics.PuppetMaster::SwitchModes()
extern void PuppetMaster_SwitchModes_m2A963A7AA8FE1F6E75915FBF67275C43695E4A13 ();
// 0x000002CF System.Void RootMotion.Dynamics.PuppetMaster::DisabledToKinematic()
extern void PuppetMaster_DisabledToKinematic_mBD0591016576717D4B50715EC5FEE6F2500B6C15 ();
// 0x000002D0 System.Collections.IEnumerator RootMotion.Dynamics.PuppetMaster::DisabledToActive()
extern void PuppetMaster_DisabledToActive_m5A4A166042CECF16F23BCBF061E9F1D135F91C4F ();
// 0x000002D1 System.Void RootMotion.Dynamics.PuppetMaster::KinematicToDisabled()
extern void PuppetMaster_KinematicToDisabled_mD50BD5CF9007E44FB4C16653019E81804DB07BFC ();
// 0x000002D2 System.Collections.IEnumerator RootMotion.Dynamics.PuppetMaster::KinematicToActive()
extern void PuppetMaster_KinematicToActive_mB82B6B2D9E03DA18DDAB567B350B9AE41783F2D7 ();
// 0x000002D3 System.Collections.IEnumerator RootMotion.Dynamics.PuppetMaster::ActiveToDisabled()
extern void PuppetMaster_ActiveToDisabled_m6B671406D8EE241766CF091F069C3FDB8612EDDE ();
// 0x000002D4 System.Collections.IEnumerator RootMotion.Dynamics.PuppetMaster::ActiveToKinematic()
extern void PuppetMaster_ActiveToKinematic_m43DABEC5AD5BE4D8BCD1E590BF3E3B4B156F96E7 ();
// 0x000002D5 System.Void RootMotion.Dynamics.PuppetMaster::UpdateInternalCollisions()
extern void PuppetMaster_UpdateInternalCollisions_m0E6A20850933656CBB421C5AA3F9192014B56527 ();
// 0x000002D6 System.Void RootMotion.Dynamics.PuppetMaster::SetMuscleWeights(RootMotion.Dynamics.Muscle_Group,System.Single,System.Single,System.Single,System.Single)
extern void PuppetMaster_SetMuscleWeights_m158D686008EA7AF65DF49B309ED1237FF25A0BC8 ();
// 0x000002D7 System.Void RootMotion.Dynamics.PuppetMaster::SetMuscleWeights(UnityEngine.Transform,System.Single,System.Single,System.Single,System.Single)
extern void PuppetMaster_SetMuscleWeights_m75432E3EAFC03A2FCA8113360AFE07535DE43703 ();
// 0x000002D8 System.Void RootMotion.Dynamics.PuppetMaster::SetMuscleWeights(UnityEngine.HumanBodyBones,System.Single,System.Single,System.Single,System.Single)
extern void PuppetMaster_SetMuscleWeights_m58341B71F5FD117693364FFD97E55183A7C26A65 ();
// 0x000002D9 System.Void RootMotion.Dynamics.PuppetMaster::SetMuscleWeightsRecursive(UnityEngine.Transform,System.Single,System.Single,System.Single,System.Single)
extern void PuppetMaster_SetMuscleWeightsRecursive_mD1EE13FA607E077949E181C7AE12ED50557CC6DF ();
// 0x000002DA System.Void RootMotion.Dynamics.PuppetMaster::SetMuscleWeightsRecursive(System.Int32,System.Single,System.Single,System.Single,System.Single)
extern void PuppetMaster_SetMuscleWeightsRecursive_mF1C4CD0DC39D236951CA9A46C03AB664AB2B06B7 ();
// 0x000002DB System.Void RootMotion.Dynamics.PuppetMaster::SetMuscleWeightsRecursive(UnityEngine.HumanBodyBones,System.Single,System.Single,System.Single,System.Single)
extern void PuppetMaster_SetMuscleWeightsRecursive_mED9BFEDA57BF6D54964A9C597186CDF275BCD3E7 ();
// 0x000002DC System.Void RootMotion.Dynamics.PuppetMaster::SetMuscleWeights(System.Int32,System.Single,System.Single,System.Single,System.Single)
extern void PuppetMaster_SetMuscleWeights_m324E7CBAE17F6F873194148FFB6B8A177C6DE095 ();
// 0x000002DD RootMotion.Dynamics.Muscle RootMotion.Dynamics.PuppetMaster::GetMuscle(UnityEngine.Transform)
extern void PuppetMaster_GetMuscle_m63F0FCDC3530F3F88B085082BFFAE2609A9D1D3E ();
// 0x000002DE RootMotion.Dynamics.Muscle RootMotion.Dynamics.PuppetMaster::GetMuscle(UnityEngine.Rigidbody)
extern void PuppetMaster_GetMuscle_m4EC83F8E157576E349FDF4ACF549A94AD7B519CE ();
// 0x000002DF RootMotion.Dynamics.Muscle RootMotion.Dynamics.PuppetMaster::GetMuscle(UnityEngine.ConfigurableJoint)
extern void PuppetMaster_GetMuscle_mA78D4064A80F7161059B861DF14B4EE635F91E98 ();
// 0x000002E0 System.Boolean RootMotion.Dynamics.PuppetMaster::ContainsJoint(UnityEngine.ConfigurableJoint)
extern void PuppetMaster_ContainsJoint_m52D20B4BBC2E3D15D8AE59194EBA166A4FCCC346 ();
// 0x000002E1 System.Int32 RootMotion.Dynamics.PuppetMaster::GetMuscleIndex(UnityEngine.HumanBodyBones)
extern void PuppetMaster_GetMuscleIndex_mC54A0EDFC0DA58F7DA6D25D5B982781510820AA2 ();
// 0x000002E2 System.Int32 RootMotion.Dynamics.PuppetMaster::GetMuscleIndex(UnityEngine.Transform)
extern void PuppetMaster_GetMuscleIndex_mD0E63ACED0A542CF864F783DE3CCF0C3341444E3 ();
// 0x000002E3 System.Int32 RootMotion.Dynamics.PuppetMaster::GetMuscleIndex(UnityEngine.Rigidbody)
extern void PuppetMaster_GetMuscleIndex_m36AF5FAA0A965CEB97F118D0287DB916F011AE71 ();
// 0x000002E4 System.Int32 RootMotion.Dynamics.PuppetMaster::GetMuscleIndex(UnityEngine.ConfigurableJoint)
extern void PuppetMaster_GetMuscleIndex_m16BDCE9F90DB2B6FD5A14927846CAEA09529D9A9 ();
// 0x000002E5 RootMotion.Dynamics.PuppetMaster RootMotion.Dynamics.PuppetMaster::SetUp(UnityEngine.Transform,UnityEngine.Transform,System.Int32,System.Int32)
extern void PuppetMaster_SetUp_mB590C35C218DCBB8657ABAD4AD3E4D7E3E1B2FBC ();
// 0x000002E6 RootMotion.Dynamics.PuppetMaster RootMotion.Dynamics.PuppetMaster::SetUp(UnityEngine.Transform,System.Int32,System.Int32)
extern void PuppetMaster_SetUp_m7486C849609ACBC820E9C8CCCBB19E37EEE7C16B ();
// 0x000002E7 System.Void RootMotion.Dynamics.PuppetMaster::SetUpTo(UnityEngine.Transform,System.Int32,System.Int32)
extern void PuppetMaster_SetUpTo_mC7F0A7FA68474FDE6D53337F04C2D566FD98202C ();
// 0x000002E8 System.Void RootMotion.Dynamics.PuppetMaster::RemoveRagdollComponents(UnityEngine.Transform,System.Int32)
extern void PuppetMaster_RemoveRagdollComponents_m115EAC1FB4726A8BFC5DD50F794284E46DA71A3D ();
// 0x000002E9 System.Void RootMotion.Dynamics.PuppetMaster::SetUpMuscles(UnityEngine.Transform)
extern void PuppetMaster_SetUpMuscles_m3F38B1E9F93E2CEF1E8F7CF3A11E8BA69049DB1C ();
// 0x000002EA RootMotion.Dynamics.Muscle_Group RootMotion.Dynamics.PuppetMaster::FindGroup(UnityEngine.Animator,UnityEngine.Transform)
extern void PuppetMaster_FindGroup_m46DA2B6EC75E1DE32A1878C953DEF21036231AB2 ();
// 0x000002EB System.Void RootMotion.Dynamics.PuppetMaster::RemoveUnnecessaryBones()
extern void PuppetMaster_RemoveUnnecessaryBones_mFF21FA3E5CA7E563864C135483C95444A0746D7A ();
// 0x000002EC System.Boolean RootMotion.Dynamics.PuppetMaster::IsClothCollider(UnityEngine.Collider,UnityEngine.Cloth[])
extern void PuppetMaster_IsClothCollider_m26BA387C486B6A99479D2E06D095C7FBC5482BAA ();
// 0x000002ED System.Boolean RootMotion.Dynamics.PuppetMaster::get_isSwitchingState()
extern void PuppetMaster_get_isSwitchingState_m4359CB336B8B6A4F357D0862306AFB1FC2401DE1 ();
// 0x000002EE System.Boolean RootMotion.Dynamics.PuppetMaster::get_isKilling()
extern void PuppetMaster_get_isKilling_m78F18D0FDC74A10182CE3659D2E77105BA7FD1BC ();
// 0x000002EF System.Void RootMotion.Dynamics.PuppetMaster::set_isKilling(System.Boolean)
extern void PuppetMaster_set_isKilling_mF34F21233C3A095C6149BEB0CAFE9836CA94BB9D ();
// 0x000002F0 System.Boolean RootMotion.Dynamics.PuppetMaster::get_isAlive()
extern void PuppetMaster_get_isAlive_m877FDB53D2C8B1970D8BFBD52AC4F4A2578469B2 ();
// 0x000002F1 System.Boolean RootMotion.Dynamics.PuppetMaster::get_isFrozen()
extern void PuppetMaster_get_isFrozen_mEFAA674FDFE0807E0EA65662CF9B6DAD963CA57C ();
// 0x000002F2 System.Void RootMotion.Dynamics.PuppetMaster::Kill()
extern void PuppetMaster_Kill_mE0C035CF7FFAB0CEBE99E9F0D34869E91A7B11F1 ();
// 0x000002F3 System.Void RootMotion.Dynamics.PuppetMaster::Kill(RootMotion.Dynamics.PuppetMaster_StateSettings)
extern void PuppetMaster_Kill_mA907CACBBE3FF6566C35E8EE7DCAE7045671CC1D ();
// 0x000002F4 System.Void RootMotion.Dynamics.PuppetMaster::Freeze()
extern void PuppetMaster_Freeze_mAE2EFC168AB166A70A5D64C6B4B3086CB2F5B409 ();
// 0x000002F5 System.Void RootMotion.Dynamics.PuppetMaster::Freeze(RootMotion.Dynamics.PuppetMaster_StateSettings)
extern void PuppetMaster_Freeze_m18FDD5ADE8F18B5949E6C612AD087C05B636F458 ();
// 0x000002F6 System.Void RootMotion.Dynamics.PuppetMaster::Resurrect()
extern void PuppetMaster_Resurrect_m8BD0CC8902DE7E512C5A87BA326A5341790C416C ();
// 0x000002F7 System.Void RootMotion.Dynamics.PuppetMaster::SwitchStates()
extern void PuppetMaster_SwitchStates_m9D5CF5DDED8C08053A5772AB68B4E789F1A49106 ();
// 0x000002F8 System.Collections.IEnumerator RootMotion.Dynamics.PuppetMaster::AliveToDead(System.Boolean)
extern void PuppetMaster_AliveToDead_m4E9BCC7ECE53183FFEF80879390EA5C6F602A9FD ();
// 0x000002F9 System.Void RootMotion.Dynamics.PuppetMaster::OnFreezeFlag()
extern void PuppetMaster_OnFreezeFlag_m37D2FD7F963B657F437EC90E6C88B7274B68601C ();
// 0x000002FA System.Void RootMotion.Dynamics.PuppetMaster::DeadToAlive()
extern void PuppetMaster_DeadToAlive_m58E2983D0D9B80F305DE4F05F703724ABE4F6A96 ();
// 0x000002FB System.Void RootMotion.Dynamics.PuppetMaster::SetAnimationEnabled(System.Boolean)
extern void PuppetMaster_SetAnimationEnabled_mDAC15D6F7F23B07BA3D0FB9B8613D96C86AA531C ();
// 0x000002FC System.Void RootMotion.Dynamics.PuppetMaster::DeadToFrozen()
extern void PuppetMaster_DeadToFrozen_m74A6F81A7333E226564DE7821D7C3771CA3AFC48 ();
// 0x000002FD System.Void RootMotion.Dynamics.PuppetMaster::FrozenToAlive()
extern void PuppetMaster_FrozenToAlive_m887320A29D27082554C298452426C219D21AB184 ();
// 0x000002FE System.Void RootMotion.Dynamics.PuppetMaster::FrozenToDead()
extern void PuppetMaster_FrozenToDead_mF356614A2AF1153388EE49E58734E1DE085DCAD7 ();
// 0x000002FF System.Void RootMotion.Dynamics.PuppetMaster::ActivateRagdoll(System.Boolean)
extern void PuppetMaster_ActivateRagdoll_mAB3CCBD89AD18F0D502B937AE35865D01BBD8E8E ();
// 0x00000300 System.Boolean RootMotion.Dynamics.PuppetMaster::CanFreeze()
extern void PuppetMaster_CanFreeze_mFBA33031E1D55680448A2ABCB47D71D4E4271D3F ();
// 0x00000301 System.Void RootMotion.Dynamics.PuppetMaster::SampleTargetMappedState()
extern void PuppetMaster_SampleTargetMappedState_m1F69C877953D6412166D964F7AF1D5977CFC7999 ();
// 0x00000302 System.Void RootMotion.Dynamics.PuppetMaster::FixTargetToSampledState(System.Single)
extern void PuppetMaster_FixTargetToSampledState_m1A647A236AC33B41C2FBC96F08F6F70032D04642 ();
// 0x00000303 System.Void RootMotion.Dynamics.PuppetMaster::StoreTargetMappedState()
extern void PuppetMaster_StoreTargetMappedState_mCCBAF89358AC2012EC1FE32054A9CCA600F9D44C ();
// 0x00000304 System.Void RootMotion.Dynamics.PuppetMaster::UpdateHierarchies()
extern void PuppetMaster_UpdateHierarchies_mE18887F82926576035BD9657C365F5EDD789ED8B ();
// 0x00000305 System.Boolean RootMotion.Dynamics.PuppetMaster::HasProp()
extern void PuppetMaster_HasProp_m935396A7BAD5C6525BCB408F3CE63564534B101C ();
// 0x00000306 System.Void RootMotion.Dynamics.PuppetMaster::UpdateBroadcasterMuscleIndexes()
extern void PuppetMaster_UpdateBroadcasterMuscleIndexes_mB92ED893FA5E25F7B19F33555FC662A6E6C1414A ();
// 0x00000307 System.Void RootMotion.Dynamics.PuppetMaster::AssignParentAndChildIndexes()
extern void PuppetMaster_AssignParentAndChildIndexes_m4AE39A6FEACB515A8EE2961CB1A064B08186ABB7 ();
// 0x00000308 System.Void RootMotion.Dynamics.PuppetMaster::AddToParentsRecursive(UnityEngine.ConfigurableJoint,System.Int32[]&)
extern void PuppetMaster_AddToParentsRecursive_mB45B5D1F65D277F8F4E336CC0A7172350D94593D ();
// 0x00000309 System.Void RootMotion.Dynamics.PuppetMaster::AddToChildrenRecursive(UnityEngine.ConfigurableJoint,System.Int32[]&,System.Boolean[]&)
extern void PuppetMaster_AddToChildrenRecursive_m1D56FD0C685E0AD65F0507CA9EE9B5AD81D2CA55 ();
// 0x0000030A System.Void RootMotion.Dynamics.PuppetMaster::AssignKinshipDegrees()
extern void PuppetMaster_AssignKinshipDegrees_mC03A8C73089FCB10A9C4C20E8419E90B1CCEFBE8 ();
// 0x0000030B System.Void RootMotion.Dynamics.PuppetMaster::AssignKinshipsDownRecursive(System.Int32[]&,System.Int32,System.Int32)
extern void PuppetMaster_AssignKinshipsDownRecursive_m12E3AF3B4F7198CFE4BCFE8B51E2B13BD2129B5C ();
// 0x0000030C System.Void RootMotion.Dynamics.PuppetMaster::AssignKinshipsUpRecursive(System.Int32[]&,System.Int32,System.Int32)
extern void PuppetMaster_AssignKinshipsUpRecursive_mEC99187176BA964926657ECF0DFD1BACDC5F00EC ();
// 0x0000030D System.Int32 RootMotion.Dynamics.PuppetMaster::GetMuscleIndexLowLevel(UnityEngine.ConfigurableJoint)
extern void PuppetMaster_GetMuscleIndexLowLevel_mDAED00EF0679144C8956304C347AD32636C96C1C ();
// 0x0000030E System.Boolean RootMotion.Dynamics.PuppetMaster::IsValid(System.Boolean)
extern void PuppetMaster_IsValid_m75409641970CFB2AD50F8F6DFF74687040F6E087 ();
// 0x0000030F System.Boolean RootMotion.Dynamics.PuppetMaster::CheckMassVariation(System.Single,System.Boolean)
extern void PuppetMaster_CheckMassVariation_mF75F48D8B2E2E2FDCE35B1B65F7CC1F96C45CEAC ();
// 0x00000310 System.Boolean RootMotion.Dynamics.PuppetMaster::CheckIfInitiated()
extern void PuppetMaster_CheckIfInitiated_mDB84F1B32280168813F4C909B0AADA694C43AFAE ();
// 0x00000311 System.Void RootMotion.Dynamics.PuppetMaster::.ctor()
extern void PuppetMaster__ctor_mFA777BE864DAE06F6DB39D7936189B8F5AF5A999 ();
// 0x00000312 System.Void RootMotion.Dynamics.PuppetMasterHumanoidConfig::ApplyTo(RootMotion.Dynamics.PuppetMaster)
extern void PuppetMasterHumanoidConfig_ApplyTo_mC403B1125B2C7D270656C722BB02AC34A501A04E ();
// 0x00000313 RootMotion.Dynamics.Muscle RootMotion.Dynamics.PuppetMasterHumanoidConfig::GetMuscle(UnityEngine.HumanBodyBones,UnityEngine.Animator,RootMotion.Dynamics.PuppetMaster)
extern void PuppetMasterHumanoidConfig_GetMuscle_mA6039ECF0DBC3C055D07D35997C17D1304C6DDF5 ();
// 0x00000314 System.Void RootMotion.Dynamics.PuppetMasterHumanoidConfig::.ctor()
extern void PuppetMasterHumanoidConfig__ctor_m39CAB91537C5C4C56226ED23FDB5A8221E93CD15 ();
// 0x00000315 System.Int32 RootMotion.Dynamics.PuppetMasterSettings::get_currentlyActivePuppets()
extern void PuppetMasterSettings_get_currentlyActivePuppets_m1A1C150CDF003F01BBCD3D86A6598FEAC81E47B0 ();
// 0x00000316 System.Void RootMotion.Dynamics.PuppetMasterSettings::set_currentlyActivePuppets(System.Int32)
extern void PuppetMasterSettings_set_currentlyActivePuppets_mAF2C6BB345E113C66FC4CEC8FF3D952640EC8655 ();
// 0x00000317 System.Int32 RootMotion.Dynamics.PuppetMasterSettings::get_currentlyKinematicPuppets()
extern void PuppetMasterSettings_get_currentlyKinematicPuppets_mA45B62B27EB3E38D24BB2C1F6CF6E7C25DF07627 ();
// 0x00000318 System.Void RootMotion.Dynamics.PuppetMasterSettings::set_currentlyKinematicPuppets(System.Int32)
extern void PuppetMasterSettings_set_currentlyKinematicPuppets_mA80A3939D29642AB6A6F307769714E3819D55EDD ();
// 0x00000319 System.Int32 RootMotion.Dynamics.PuppetMasterSettings::get_currentlyDisabledPuppets()
extern void PuppetMasterSettings_get_currentlyDisabledPuppets_mF560E9401F157C8989C9ADFC9F4043BE9CCF4CC2 ();
// 0x0000031A System.Void RootMotion.Dynamics.PuppetMasterSettings::set_currentlyDisabledPuppets(System.Int32)
extern void PuppetMasterSettings_set_currentlyDisabledPuppets_mD9456C898DFC0C1B2B3767DDBA63399123B2DB0F ();
// 0x0000031B System.Collections.Generic.List`1<RootMotion.Dynamics.PuppetMaster> RootMotion.Dynamics.PuppetMasterSettings::get_puppets()
extern void PuppetMasterSettings_get_puppets_mB74BDDA129CB74D6849B4BB49C76AF7206AF2DB6 ();
// 0x0000031C System.Void RootMotion.Dynamics.PuppetMasterSettings::Register(RootMotion.Dynamics.PuppetMaster)
extern void PuppetMasterSettings_Register_mA679B9A78F060D522EFEFC15551EB631A657E7CC ();
// 0x0000031D System.Void RootMotion.Dynamics.PuppetMasterSettings::Unregister(RootMotion.Dynamics.PuppetMaster)
extern void PuppetMasterSettings_Unregister_m777596EE74011409A6D3804486BE4E742D16052C ();
// 0x0000031E System.Boolean RootMotion.Dynamics.PuppetMasterSettings::UpdateMoveToTarget(RootMotion.Dynamics.PuppetMaster)
extern void PuppetMasterSettings_UpdateMoveToTarget_m4FA52E51AD69C9FCB6F07A488E636576D057F97F ();
// 0x0000031F System.Boolean RootMotion.Dynamics.PuppetMasterSettings::UpdateFree(RootMotion.Dynamics.PuppetMaster)
extern void PuppetMasterSettings_UpdateFree_m6FE8B2C4588251FA5893EA1135D2649D8BCDCAA3 ();
// 0x00000320 System.Boolean RootMotion.Dynamics.PuppetMasterSettings::UpdateFixed(RootMotion.Dynamics.PuppetMaster)
extern void PuppetMasterSettings_UpdateFixed_m8C82C6B84C23764098547C2D50EF10A9B31BBEA5 ();
// 0x00000321 System.Void RootMotion.Dynamics.PuppetMasterSettings::Update()
extern void PuppetMasterSettings_Update_m6A1458818DC132BB4E6EA3E824368F0C872B1CBA ();
// 0x00000322 System.Void RootMotion.Dynamics.PuppetMasterSettings::FixedUpdate()
extern void PuppetMasterSettings_FixedUpdate_m7A18D97866BC0875B53577EB8D4DA7EAFC02EC52 ();
// 0x00000323 System.Void RootMotion.Dynamics.PuppetMasterSettings::.ctor()
extern void PuppetMasterSettings__ctor_m222849AD88D5DC42AB03345E8E4BF88DF492F89B ();
// 0x00000324 System.Void RootMotion.Dynamics.PuppetMasterTools::PositionRagdoll(RootMotion.Dynamics.PuppetMaster)
extern void PuppetMasterTools_PositionRagdoll_m58DF512744AE8D84B740A243A6C1179AED127876 ();
// 0x00000325 System.Void RootMotion.Dynamics.PuppetMasterTools::RealignRagdoll(RootMotion.Dynamics.PuppetMaster)
extern void PuppetMasterTools_RealignRagdoll_m5CB33153F6B63C7E576D34BC62B3FBBCA1E7D464 ();
// 0x00000326 UnityEngine.Vector3 RootMotion.Dynamics.PuppetMasterTools::DirectionIntToVector3(System.Int32)
extern void PuppetMasterTools_DirectionIntToVector3_m64C1320D36F1185869C059463AB644FF9C75126E ();
// 0x00000327 System.Int32 RootMotion.Dynamics.PuppetMasterTools::DirectionVector3ToInt(UnityEngine.Vector3)
extern void PuppetMasterTools_DirectionVector3ToInt_m65A17DF9C7FF1B9328A8369C1518583800E8E40B ();
// 0x00000328 System.Void RootMotion.Dynamics.Weight::.ctor(System.Single)
extern void Weight__ctor_m0A63821127A7C4C5B442D426BA989CF43081B8F9 ();
// 0x00000329 System.Void RootMotion.Dynamics.Weight::.ctor(System.Single,System.String)
extern void Weight__ctor_m2A884C21880AF50ED4F1C14FE4D3A45BDA20240B ();
// 0x0000032A System.Single RootMotion.Dynamics.Weight::GetValue(System.Single)
extern void Weight_GetValue_m1A9BE7282C6E50E56808E29DCEAA29527CE9EE32 ();
// 0x0000032B System.Void RootMotion.Dynamics.InitialVelocity::Start()
extern void InitialVelocity_Start_m0E2388398FC823C5FC1D6954DF11E1E5FBD4C554 ();
// 0x0000032C System.Void RootMotion.Dynamics.InitialVelocity::.ctor()
extern void InitialVelocity__ctor_m9BFBB2E435C8E6B7668A010FD0EAD945DD343AA8 ();
// 0x0000032D System.Void RootMotion.Dynamics.BipedRagdollCreator::OpenUserManual()
extern void BipedRagdollCreator_OpenUserManual_m101A31B6FA10416B9A3496357D9D686961EC4C61 ();
// 0x0000032E System.Void RootMotion.Dynamics.BipedRagdollCreator::OpenScriptReference()
extern void BipedRagdollCreator_OpenScriptReference_mF8E9BFB60699FB63452D67EA1F472BFB16F07060 ();
// 0x0000032F System.Void RootMotion.Dynamics.BipedRagdollCreator::OpenTutorial()
extern void BipedRagdollCreator_OpenTutorial_mB8D72804137043CE371E4023CBE6B1C689608880 ();
// 0x00000330 RootMotion.Dynamics.BipedRagdollCreator_Options RootMotion.Dynamics.BipedRagdollCreator::AutodetectOptions(RootMotion.Dynamics.BipedRagdollReferences)
extern void BipedRagdollCreator_AutodetectOptions_m6ACF38C0EC9449957B30284200F849E38D37A695 ();
// 0x00000331 System.Void RootMotion.Dynamics.BipedRagdollCreator::Create(RootMotion.Dynamics.BipedRagdollReferences,RootMotion.Dynamics.BipedRagdollCreator_Options)
extern void BipedRagdollCreator_Create_m3C2CBEC7FCEFB1486CFA5F245D3BBA010E708E3B ();
// 0x00000332 System.Void RootMotion.Dynamics.BipedRagdollCreator::CreateColliders(RootMotion.Dynamics.BipedRagdollReferences,RootMotion.Dynamics.BipedRagdollCreator_Options)
extern void BipedRagdollCreator_CreateColliders_mDC7346D7EC17EED69D8C2EFB9349EC8F726B7DE2 ();
// 0x00000333 System.Void RootMotion.Dynamics.BipedRagdollCreator::CreateHandCollider(UnityEngine.Transform,UnityEngine.Transform,UnityEngine.Transform,RootMotion.Dynamics.BipedRagdollCreator_Options)
extern void BipedRagdollCreator_CreateHandCollider_m7E692275D8FDAE15267D7313AD7F2CC9104C4F89 ();
// 0x00000334 System.Void RootMotion.Dynamics.BipedRagdollCreator::CreateFootCollider(UnityEngine.Transform,UnityEngine.Transform,UnityEngine.Transform,UnityEngine.Transform,RootMotion.Dynamics.BipedRagdollCreator_Options)
extern void BipedRagdollCreator_CreateFootCollider_m560E478862DFC309F12CD154AACCEBEEAF6E05EF ();
// 0x00000335 UnityEngine.Vector3 RootMotion.Dynamics.BipedRagdollCreator::GetChildCentroid(UnityEngine.Transform,UnityEngine.Vector3)
extern void BipedRagdollCreator_GetChildCentroid_m43FAA92D367334318CD02725D494114137AA9BF4 ();
// 0x00000336 System.Void RootMotion.Dynamics.BipedRagdollCreator::MassDistribution(RootMotion.Dynamics.BipedRagdollReferences,RootMotion.Dynamics.BipedRagdollCreator_Options)
extern void BipedRagdollCreator_MassDistribution_mA39D51D32956B501ED574D35249C322625123779 ();
// 0x00000337 System.Void RootMotion.Dynamics.BipedRagdollCreator::CreateJoints(RootMotion.Dynamics.BipedRagdollReferences,RootMotion.Dynamics.BipedRagdollCreator_Options)
extern void BipedRagdollCreator_CreateJoints_m5B513C6952EBF786A03A5F435A4A95CED9DBCD2D ();
// 0x00000338 System.Void RootMotion.Dynamics.BipedRagdollCreator::CreateLimbJoints(UnityEngine.Transform,UnityEngine.Transform,UnityEngine.Transform,UnityEngine.Transform,UnityEngine.Transform,UnityEngine.Vector3,RootMotion.Dynamics.RagdollCreator_JointType,RootMotion.Dynamics.RagdollCreator_CreateJointParams_Limits,RootMotion.Dynamics.RagdollCreator_CreateJointParams_Limits,RootMotion.Dynamics.RagdollCreator_CreateJointParams_Limits)
extern void BipedRagdollCreator_CreateLimbJoints_m3EB7F93788EF538E69239F51ED56C1067C080708 ();
// 0x00000339 System.Void RootMotion.Dynamics.BipedRagdollCreator::ClearBipedRagdoll(RootMotion.Dynamics.BipedRagdollReferences)
extern void BipedRagdollCreator_ClearBipedRagdoll_m420FAAE2FA727E1AD2414BD3F4F5A62125E3EB89 ();
// 0x0000033A System.Boolean RootMotion.Dynamics.BipedRagdollCreator::IsClear(RootMotion.Dynamics.BipedRagdollReferences)
extern void BipedRagdollCreator_IsClear_mDDF58CDD3A64DD3A0C06E07EDB31931A22730202 ();
// 0x0000033B UnityEngine.Vector3 RootMotion.Dynamics.BipedRagdollCreator::GetUpperArmToHeadCentroid(RootMotion.Dynamics.BipedRagdollReferences)
extern void BipedRagdollCreator_GetUpperArmToHeadCentroid_m7648ADE2375E56B8D70F6D2EC7A0A3FF6C694EA1 ();
// 0x0000033C UnityEngine.Vector3 RootMotion.Dynamics.BipedRagdollCreator::GetUpperArmCentroid(RootMotion.Dynamics.BipedRagdollReferences)
extern void BipedRagdollCreator_GetUpperArmCentroid_m908D86D1DFAFF7DBC1B548D2120581D801578661 ();
// 0x0000033D System.Void RootMotion.Dynamics.BipedRagdollCreator::.ctor()
extern void BipedRagdollCreator__ctor_mE8452241AB7E28035F89AB733FECC86FA7CC1853 ();
// 0x0000033E System.Boolean RootMotion.Dynamics.BipedRagdollReferences::IsValid(System.String&)
extern void BipedRagdollReferences_IsValid_mDA4D8365899E9DC83616E0B017355B1E49A66107_AdjustorThunk ();
// 0x0000033F System.Boolean RootMotion.Dynamics.BipedRagdollReferences::IsChildRecursive(UnityEngine.Transform,UnityEngine.Transform)
extern void BipedRagdollReferences_IsChildRecursive_m3407509D571865F1DAEAFA868F85CECD2C089F8D_AdjustorThunk ();
// 0x00000340 System.Boolean RootMotion.Dynamics.BipedRagdollReferences::IsEmpty(System.Boolean)
extern void BipedRagdollReferences_IsEmpty_m965F7658D4B1B597A35CBFA94090B81302A642BD_AdjustorThunk ();
// 0x00000341 System.Boolean RootMotion.Dynamics.BipedRagdollReferences::Contains(UnityEngine.Transform,System.Boolean)
extern void BipedRagdollReferences_Contains_m45447FFFD48A23717118CE75D21220D86D13C6E1_AdjustorThunk ();
// 0x00000342 UnityEngine.Transform[] RootMotion.Dynamics.BipedRagdollReferences::GetRagdollTransforms()
extern void BipedRagdollReferences_GetRagdollTransforms_m0D55607F6C00614EA33CCB993A2464938744FE87_AdjustorThunk ();
// 0x00000343 RootMotion.Dynamics.BipedRagdollReferences RootMotion.Dynamics.BipedRagdollReferences::FromAvatar(UnityEngine.Animator)
extern void BipedRagdollReferences_FromAvatar_mC8E55D297B46113B34CF50036CDA2D1C9320E7DB ();
// 0x00000344 RootMotion.Dynamics.BipedRagdollReferences RootMotion.Dynamics.BipedRagdollReferences::FromBipedReferences(RootMotion.BipedReferences)
extern void BipedRagdollReferences_FromBipedReferences_m1ED51DE3694E1384314A60FE77C77BF2940A49D4 ();
// 0x00000345 System.Void RootMotion.Dynamics.JointConverter::ToConfigurable(UnityEngine.GameObject)
extern void JointConverter_ToConfigurable_m7486BC65316A40EB2BF42114838103CCBD13E259 ();
// 0x00000346 System.Void RootMotion.Dynamics.JointConverter::HingeToConfigurable(UnityEngine.HingeJoint)
extern void JointConverter_HingeToConfigurable_m77A241554ECF48D3E28958108FB591EF5860A16E ();
// 0x00000347 System.Void RootMotion.Dynamics.JointConverter::FixedToConfigurable(UnityEngine.FixedJoint)
extern void JointConverter_FixedToConfigurable_m398C68A83D2E198996258480DFD3099E5058F8DC ();
// 0x00000348 System.Void RootMotion.Dynamics.JointConverter::SpringToConfigurable(UnityEngine.SpringJoint)
extern void JointConverter_SpringToConfigurable_m980A27C816680FE3C712A2CCE852A235128F4D28 ();
// 0x00000349 System.Void RootMotion.Dynamics.JointConverter::CharacterToConfigurable(UnityEngine.CharacterJoint)
extern void JointConverter_CharacterToConfigurable_m0203B5B8D02570FACAD1E4C9A34188AFB2EF4625 ();
// 0x0000034A System.Void RootMotion.Dynamics.JointConverter::ConvertJoint(UnityEngine.ConfigurableJoint&,UnityEngine.Joint)
extern void JointConverter_ConvertJoint_mAFC0BED539F03AE3ECB6CC351C3B2D1A56D97720 ();
// 0x0000034B UnityEngine.SoftJointLimit RootMotion.Dynamics.JointConverter::ConvertToHighSoftJointLimit(UnityEngine.JointLimits,UnityEngine.JointSpring,System.Boolean)
extern void JointConverter_ConvertToHighSoftJointLimit_m003C1A9DCF0D07D09AB23A8B1AB843042F34668C ();
// 0x0000034C UnityEngine.SoftJointLimit RootMotion.Dynamics.JointConverter::ConvertToLowSoftJointLimit(UnityEngine.JointLimits,UnityEngine.JointSpring,System.Boolean)
extern void JointConverter_ConvertToLowSoftJointLimit_mAE3FFC0F9B67B116D566B6D9FB41ABA67A56B4B2 ();
// 0x0000034D UnityEngine.SoftJointLimitSpring RootMotion.Dynamics.JointConverter::ConvertToSoftJointLimitSpring(UnityEngine.JointLimits,UnityEngine.JointSpring,System.Boolean)
extern void JointConverter_ConvertToSoftJointLimitSpring_m04949A8CFE6257138BE321B39C98E55712639D91 ();
// 0x0000034E UnityEngine.SoftJointLimit RootMotion.Dynamics.JointConverter::CopyLimit(UnityEngine.SoftJointLimit)
extern void JointConverter_CopyLimit_m0270D2DD51F9A0300FAEDCB89394AA02A25A8AE1 ();
// 0x0000034F UnityEngine.SoftJointLimitSpring RootMotion.Dynamics.JointConverter::CopyLimitSpring(UnityEngine.SoftJointLimitSpring)
extern void JointConverter_CopyLimitSpring_m290B06E81D8615769F5AB0834C23707EA06A0104 ();
// 0x00000350 System.Void RootMotion.Dynamics.RagdollCreator::ClearAll(UnityEngine.Transform)
extern void RagdollCreator_ClearAll_m025B764017222F843D573856F79E08E70EE3AF59 ();
// 0x00000351 System.Void RootMotion.Dynamics.RagdollCreator::ClearTransform(UnityEngine.Transform)
extern void RagdollCreator_ClearTransform_m44076A67B8AC49910F5DC6E565E948863940B631 ();
// 0x00000352 System.Void RootMotion.Dynamics.RagdollCreator::CreateCollider(UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.Vector3,RootMotion.Dynamics.RagdollCreator_ColliderType,System.Single,System.Single)
extern void RagdollCreator_CreateCollider_mD0B50640725C13B6C3610AE00351556918A4C5AC ();
// 0x00000353 System.Void RootMotion.Dynamics.RagdollCreator::CreateCollider(UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.Vector3,RootMotion.Dynamics.RagdollCreator_ColliderType,System.Single,System.Single,System.Single,UnityEngine.Vector3)
extern void RagdollCreator_CreateCollider_mBF2A8AC2AB0914283E3B7CF081BC5642B344F259 ();
// 0x00000354 System.Single RootMotion.Dynamics.RagdollCreator::GetScaleF(UnityEngine.Transform)
extern void RagdollCreator_GetScaleF_m2BF9F7F2053AFCF4D56BDDD4B6C5D0C4755BA6A9 ();
// 0x00000355 UnityEngine.Vector3 RootMotion.Dynamics.RagdollCreator::Abs(UnityEngine.Vector3)
extern void RagdollCreator_Abs_mF093AE880AB1C58728DF9694786E55E0C5F39EF3 ();
// 0x00000356 System.Void RootMotion.Dynamics.RagdollCreator::Vector3Abs(UnityEngine.Vector3&)
extern void RagdollCreator_Vector3Abs_m670CE644388B80D42B8669A8B9C0D415BF90AD4C ();
// 0x00000357 UnityEngine.Vector3 RootMotion.Dynamics.RagdollCreator::DirectionIntToVector3(System.Int32)
extern void RagdollCreator_DirectionIntToVector3_m5E21BA8ADEB22315A3240D71E224E64BD4F01516 ();
// 0x00000358 UnityEngine.Vector3 RootMotion.Dynamics.RagdollCreator::DirectionToVector3(RootMotion.Dynamics.RagdollCreator_Direction)
extern void RagdollCreator_DirectionToVector3_m6E252DE8EF52F5587EC977594E328172A496142C ();
// 0x00000359 System.Int32 RootMotion.Dynamics.RagdollCreator::DirectionVector3ToInt(UnityEngine.Vector3)
extern void RagdollCreator_DirectionVector3ToInt_m38E848F97AA8073271DE9B6F74D084B6CE712391 ();
// 0x0000035A UnityEngine.Vector3 RootMotion.Dynamics.RagdollCreator::GetLocalOrthoDirection(UnityEngine.Transform,UnityEngine.Vector3)
extern void RagdollCreator_GetLocalOrthoDirection_m0C36A6B96D67F9610EB6254A51CC3C4328101DB5 ();
// 0x0000035B UnityEngine.Rigidbody RootMotion.Dynamics.RagdollCreator::GetConnectedBody(UnityEngine.Transform,UnityEngine.Transform[]&)
extern void RagdollCreator_GetConnectedBody_mEA345AD45FC52ADECB35D1FADE56206F01622DB5 ();
// 0x0000035C System.Void RootMotion.Dynamics.RagdollCreator::CreateJoint(RootMotion.Dynamics.RagdollCreator_CreateJointParams)
extern void RagdollCreator_CreateJoint_mF9CD250B2D718D2383696058615C997345A8406F ();
// 0x0000035D UnityEngine.SoftJointLimit RootMotion.Dynamics.RagdollCreator::ToSoftJointLimit(System.Single)
extern void RagdollCreator_ToSoftJointLimit_m385750A35122960E624DB7E610A7B58BEBA03751 ();
// 0x0000035E System.Void RootMotion.Dynamics.RagdollCreator::.ctor()
extern void RagdollCreator__ctor_m8D076280BBA9F56C8160AF0A04DB67C3E92BA6D9 ();
// 0x0000035F System.Void RootMotion.Dynamics.RagdollEditor::OpenUserManual()
extern void RagdollEditor_OpenUserManual_mC606D6CA3E4E0ED0CD39ACA7640D553EBE7D4CC5 ();
// 0x00000360 System.Void RootMotion.Dynamics.RagdollEditor::OpenScriptReference()
extern void RagdollEditor_OpenScriptReference_m5A598EE5C81BE7956A751E35464D14A5249F52E6 ();
// 0x00000361 System.Void RootMotion.Dynamics.RagdollEditor::OpenTutorial()
extern void RagdollEditor_OpenTutorial_m48DBCD4CC69F3769E6C7AB2C8F078E0744500E55 ();
// 0x00000362 System.Void RootMotion.Dynamics.RagdollEditor::.ctor()
extern void RagdollEditor__ctor_m25958F96AD6C115CF5C18DF9D2167DBC279512E4 ();
// 0x00000363 System.Void RootMotion.BipedLimbOrientations_LimbOrientation::.ctor(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void LimbOrientation__ctor_m319A1CD829F39F8B992734E1BC7EE40FA02F1E72 ();
// 0x00000364 System.Void RootMotion.BipedReferences_AutoDetectParams::.ctor(System.Boolean,System.Boolean)
extern void AutoDetectParams__ctor_m2435ACA52D63236221123817EAF30185D49F414D_AdjustorThunk ();
// 0x00000365 RootMotion.BipedReferences_AutoDetectParams RootMotion.BipedReferences_AutoDetectParams::get_Default()
extern void AutoDetectParams_get_Default_m3D6674EE654AE9CECE139BE08AC9AC5EB8E7125D ();
// 0x00000366 System.Void RootMotion.Warning_Logger::.ctor(System.Object,System.IntPtr)
extern void Logger__ctor_mD837D3055E1B46E799F39F4D47CD32182421BEE1 ();
// 0x00000367 System.Void RootMotion.Warning_Logger::Invoke(System.String)
extern void Logger_Invoke_mB3A4E7D37799962FBA128DDEF6505472715C8CD0 ();
// 0x00000368 System.IAsyncResult RootMotion.Warning_Logger::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void Logger_BeginInvoke_m8F3EBFF0C8C04463D7D417589ADD3F9023C2DA2B ();
// 0x00000369 System.Void RootMotion.Warning_Logger::EndInvoke(System.IAsyncResult)
extern void Logger_EndInvoke_mCF47445F73C5AB02DF45A5DE02B3E805951F5A40 ();
// 0x0000036A System.Void RootMotion.Demos.CharacterMeleeDemo_Action::.ctor()
extern void Action__ctor_m36112F278BFEBAD5B68584424AF11D38FD26E575 ();
// 0x0000036B System.Void RootMotion.Demos.Destructor_<Destruct>d__2::.ctor(System.Int32)
extern void U3CDestructU3Ed__2__ctor_mE25109E867AB45381390300EA99BAFAF31D04B7E ();
// 0x0000036C System.Void RootMotion.Demos.Destructor_<Destruct>d__2::System.IDisposable.Dispose()
extern void U3CDestructU3Ed__2_System_IDisposable_Dispose_m415F539D035D8FE468C51E2DAAFA70C399D6CD28 ();
// 0x0000036D System.Boolean RootMotion.Demos.Destructor_<Destruct>d__2::MoveNext()
extern void U3CDestructU3Ed__2_MoveNext_mAA9C444FE8911C0BB46043E0D2C127CC0E457D63 ();
// 0x0000036E System.Object RootMotion.Demos.Destructor_<Destruct>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDestructU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4D0E9A81C619510B9C02CC15697700771433AC7A ();
// 0x0000036F System.Void RootMotion.Demos.Destructor_<Destruct>d__2::System.Collections.IEnumerator.Reset()
extern void U3CDestructU3Ed__2_System_Collections_IEnumerator_Reset_mFAB5902921AB540AEE327D996F60A03F8D194FB6 ();
// 0x00000370 System.Object RootMotion.Demos.Destructor_<Destruct>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CDestructU3Ed__2_System_Collections_IEnumerator_get_Current_m6114D6399412B96ACD40D8A3055A4C94A178F61D ();
// 0x00000371 System.Void RootMotion.Demos.Dying_<FadeOutPinWeight>d__10::.ctor(System.Int32)
extern void U3CFadeOutPinWeightU3Ed__10__ctor_m17EE4B1D3BB3F8372015BAB95CE3DA2063F5C893 ();
// 0x00000372 System.Void RootMotion.Demos.Dying_<FadeOutPinWeight>d__10::System.IDisposable.Dispose()
extern void U3CFadeOutPinWeightU3Ed__10_System_IDisposable_Dispose_m47A17E1D9D97189F0DE1F213BE980FEE7CC2F7B7 ();
// 0x00000373 System.Boolean RootMotion.Demos.Dying_<FadeOutPinWeight>d__10::MoveNext()
extern void U3CFadeOutPinWeightU3Ed__10_MoveNext_m18080DEA0B9EB98B9633277B49D2502EBD325CDC ();
// 0x00000374 System.Object RootMotion.Demos.Dying_<FadeOutPinWeight>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFadeOutPinWeightU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0ED4FF6DF45BB3FEED0865E13791CCCAE8C81BD0 ();
// 0x00000375 System.Void RootMotion.Demos.Dying_<FadeOutPinWeight>d__10::System.Collections.IEnumerator.Reset()
extern void U3CFadeOutPinWeightU3Ed__10_System_Collections_IEnumerator_Reset_mF1964A707B085132B62920D94BD03C9D03739B5A ();
// 0x00000376 System.Object RootMotion.Demos.Dying_<FadeOutPinWeight>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CFadeOutPinWeightU3Ed__10_System_Collections_IEnumerator_get_Current_mF89F36668BFDF7540B38960E45769FCE2138CF63 ();
// 0x00000377 System.Void RootMotion.Demos.Dying_<FadeOutMuscleWeight>d__11::.ctor(System.Int32)
extern void U3CFadeOutMuscleWeightU3Ed__11__ctor_m3ACDEEF054A2C8D132060A1C84C120732127493E ();
// 0x00000378 System.Void RootMotion.Demos.Dying_<FadeOutMuscleWeight>d__11::System.IDisposable.Dispose()
extern void U3CFadeOutMuscleWeightU3Ed__11_System_IDisposable_Dispose_m8F4175A8C96A4294FF2304332A955E2DE9D1F923 ();
// 0x00000379 System.Boolean RootMotion.Demos.Dying_<FadeOutMuscleWeight>d__11::MoveNext()
extern void U3CFadeOutMuscleWeightU3Ed__11_MoveNext_m97D98B8575B76881F56519688F58816B09032EAE ();
// 0x0000037A System.Object RootMotion.Demos.Dying_<FadeOutMuscleWeight>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFadeOutMuscleWeightU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBEF7D3406D57A3E06DCAD5ED2A17017A60543429 ();
// 0x0000037B System.Void RootMotion.Demos.Dying_<FadeOutMuscleWeight>d__11::System.Collections.IEnumerator.Reset()
extern void U3CFadeOutMuscleWeightU3Ed__11_System_Collections_IEnumerator_Reset_mBF5BCF7ED9B7E22F31E4C152CDDD28E69A0D8D81 ();
// 0x0000037C System.Object RootMotion.Demos.Dying_<FadeOutMuscleWeight>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CFadeOutMuscleWeightU3Ed__11_System_Collections_IEnumerator_get_Current_m86A5EEA3D980469EB77982D38F2FE0450A83CCE9 ();
// 0x0000037D System.Void RootMotion.Demos.PropMelee_<Action>d__11::.ctor(System.Int32)
extern void U3CActionU3Ed__11__ctor_mCD98042B06943AE03CB899663E45D2766B6C282B ();
// 0x0000037E System.Void RootMotion.Demos.PropMelee_<Action>d__11::System.IDisposable.Dispose()
extern void U3CActionU3Ed__11_System_IDisposable_Dispose_m1184C21A0F4EEBA21622410B621AA7B468EB32BF ();
// 0x0000037F System.Boolean RootMotion.Demos.PropMelee_<Action>d__11::MoveNext()
extern void U3CActionU3Ed__11_MoveNext_m9DA1B08260D07B225E9A271F1AF6E9CAE2A4881D ();
// 0x00000380 System.Object RootMotion.Demos.PropMelee_<Action>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CActionU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1BF234FF80172CDD15173BE0253E7B62F3CB8024 ();
// 0x00000381 System.Void RootMotion.Demos.PropMelee_<Action>d__11::System.Collections.IEnumerator.Reset()
extern void U3CActionU3Ed__11_System_Collections_IEnumerator_Reset_m43FCFE21C0454EDA6A74644E2E46CAECB43C889C ();
// 0x00000382 System.Object RootMotion.Demos.PropMelee_<Action>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CActionU3Ed__11_System_Collections_IEnumerator_get_Current_mF2AC613007A12B52C2D44E5C44A4EECED2B346FD ();
// 0x00000383 System.Void RootMotion.Dynamics.BehaviourAnimatedStagger_<LoseBalance>d__23::.ctor(System.Int32)
extern void U3CLoseBalanceU3Ed__23__ctor_m6C0B4E5DAB1537B105D2D16EDC08B5748ABF6BAA ();
// 0x00000384 System.Void RootMotion.Dynamics.BehaviourAnimatedStagger_<LoseBalance>d__23::System.IDisposable.Dispose()
extern void U3CLoseBalanceU3Ed__23_System_IDisposable_Dispose_mEDFEEF5D31705C6457EF8BA9AA836D1E117F2D9D ();
// 0x00000385 System.Boolean RootMotion.Dynamics.BehaviourAnimatedStagger_<LoseBalance>d__23::MoveNext()
extern void U3CLoseBalanceU3Ed__23_MoveNext_m25497A9507BF70163E25C330E0C84138362A40DF ();
// 0x00000386 System.Object RootMotion.Dynamics.BehaviourAnimatedStagger_<LoseBalance>d__23::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoseBalanceU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m243910F219229B1A8DE1794D93940195E6F0BA29 ();
// 0x00000387 System.Void RootMotion.Dynamics.BehaviourAnimatedStagger_<LoseBalance>d__23::System.Collections.IEnumerator.Reset()
extern void U3CLoseBalanceU3Ed__23_System_Collections_IEnumerator_Reset_m55DD46FEEABE2677726B3860729493CBBF341957 ();
// 0x00000388 System.Object RootMotion.Dynamics.BehaviourAnimatedStagger_<LoseBalance>d__23::System.Collections.IEnumerator.get_Current()
extern void U3CLoseBalanceU3Ed__23_System_Collections_IEnumerator_get_Current_mB3A4F1B9777E2CFA20D025D66D3C609F2D1EE0A7 ();
// 0x00000389 System.Void RootMotion.Dynamics.BehaviourBase_BehaviourDelegate::.ctor(System.Object,System.IntPtr)
extern void BehaviourDelegate__ctor_mDC6E06D6EA50133126E55B4D01458C7C38B4F831 ();
// 0x0000038A System.Void RootMotion.Dynamics.BehaviourBase_BehaviourDelegate::Invoke()
extern void BehaviourDelegate_Invoke_m2E21B79018FF6D07D694AE14E31B55AEE7D799FE ();
// 0x0000038B System.IAsyncResult RootMotion.Dynamics.BehaviourBase_BehaviourDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern void BehaviourDelegate_BeginInvoke_m5D2389AAFCD6A4353C0A52899325FBDD70E16658 ();
// 0x0000038C System.Void RootMotion.Dynamics.BehaviourBase_BehaviourDelegate::EndInvoke(System.IAsyncResult)
extern void BehaviourDelegate_EndInvoke_m54DAA044F652BAE49E51CDE70E3C529DC297D181 ();
// 0x0000038D System.Void RootMotion.Dynamics.BehaviourBase_HitDelegate::.ctor(System.Object,System.IntPtr)
extern void HitDelegate__ctor_m5D39E94D34D75B1E66130DE4B7B6FF5C119E0086 ();
// 0x0000038E System.Void RootMotion.Dynamics.BehaviourBase_HitDelegate::Invoke(RootMotion.Dynamics.MuscleHit)
extern void HitDelegate_Invoke_mBE83643AA541AF6EC6F71C41384EA642964FECB9 ();
// 0x0000038F System.IAsyncResult RootMotion.Dynamics.BehaviourBase_HitDelegate::BeginInvoke(RootMotion.Dynamics.MuscleHit,System.AsyncCallback,System.Object)
extern void HitDelegate_BeginInvoke_mCA82EEEB242FF7C5FC98B7251FF48DF45F4A813C ();
// 0x00000390 System.Void RootMotion.Dynamics.BehaviourBase_HitDelegate::EndInvoke(System.IAsyncResult)
extern void HitDelegate_EndInvoke_m32CD136984ED5B1A1596FA08B845FE73065FAA8B ();
// 0x00000391 System.Void RootMotion.Dynamics.BehaviourBase_CollisionDelegate::.ctor(System.Object,System.IntPtr)
extern void CollisionDelegate__ctor_mD3EF92BD4ACBBD591F073698FBD2C2D506537B01 ();
// 0x00000392 System.Void RootMotion.Dynamics.BehaviourBase_CollisionDelegate::Invoke(RootMotion.Dynamics.MuscleCollision)
extern void CollisionDelegate_Invoke_m0CF45D6D17978394284E6ADCB82C6A1A05ABC21C ();
// 0x00000393 System.IAsyncResult RootMotion.Dynamics.BehaviourBase_CollisionDelegate::BeginInvoke(RootMotion.Dynamics.MuscleCollision,System.AsyncCallback,System.Object)
extern void CollisionDelegate_BeginInvoke_m62B9A77B9BE5A6F6235FEA6A702D2E3358FB42A3 ();
// 0x00000394 System.Void RootMotion.Dynamics.BehaviourBase_CollisionDelegate::EndInvoke(System.IAsyncResult)
extern void CollisionDelegate_EndInvoke_mC894E90A84C65D68115AD861738F75A02808889C ();
// 0x00000395 System.Boolean RootMotion.Dynamics.BehaviourBase_PuppetEvent::get_switchBehaviour()
extern void PuppetEvent_get_switchBehaviour_m239070C6A51734D6A66B91F822D876234405BE36_AdjustorThunk ();
// 0x00000396 System.Void RootMotion.Dynamics.BehaviourBase_PuppetEvent::Trigger(RootMotion.Dynamics.PuppetMaster,System.Boolean)
extern void PuppetEvent_Trigger_mAAF24429CA3A9AADBCB59F01130265D2E2BE81F9_AdjustorThunk ();
// 0x00000397 System.Void RootMotion.Dynamics.BehaviourBase_AnimatorEvent::Activate(UnityEngine.Animator,UnityEngine.Animation)
extern void AnimatorEvent_Activate_m963A857D5FA9635D98029C6FBF1C38586A3F5CC7 ();
// 0x00000398 System.Void RootMotion.Dynamics.BehaviourBase_AnimatorEvent::Activate(UnityEngine.Animator)
extern void AnimatorEvent_Activate_m6F3086B3C67091F6363568FD9F6DAAC992E64F4A ();
// 0x00000399 System.Void RootMotion.Dynamics.BehaviourBase_AnimatorEvent::Activate(UnityEngine.Animation)
extern void AnimatorEvent_Activate_mD49B580BE52A6511EEAE801CBDF8148B3253D0CD ();
// 0x0000039A System.Void RootMotion.Dynamics.BehaviourBase_AnimatorEvent::.ctor()
extern void AnimatorEvent__ctor_m1D4043E6290AE4619DCDDE0DD2467B674B5D2615 ();
// 0x0000039B System.Void RootMotion.Dynamics.BehaviourFall_<SmoothActivate>d__21::.ctor(System.Int32)
extern void U3CSmoothActivateU3Ed__21__ctor_mC13E4922758110ED81D0470B5FDBB43C31DBF765 ();
// 0x0000039C System.Void RootMotion.Dynamics.BehaviourFall_<SmoothActivate>d__21::System.IDisposable.Dispose()
extern void U3CSmoothActivateU3Ed__21_System_IDisposable_Dispose_m19C5D66935D252E60D2BA61AFB4976A009EAA83F ();
// 0x0000039D System.Boolean RootMotion.Dynamics.BehaviourFall_<SmoothActivate>d__21::MoveNext()
extern void U3CSmoothActivateU3Ed__21_MoveNext_m62D59362D58C7D9D5C3904E79CB6DB85EF5CC182 ();
// 0x0000039E System.Object RootMotion.Dynamics.BehaviourFall_<SmoothActivate>d__21::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSmoothActivateU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m90430D8A1D5E5A68903395109F999DB4587C95C4 ();
// 0x0000039F System.Void RootMotion.Dynamics.BehaviourFall_<SmoothActivate>d__21::System.Collections.IEnumerator.Reset()
extern void U3CSmoothActivateU3Ed__21_System_Collections_IEnumerator_Reset_m9AF719A255135FD14022709E44319B63F4D4DB30 ();
// 0x000003A0 System.Object RootMotion.Dynamics.BehaviourFall_<SmoothActivate>d__21::System.Collections.IEnumerator.get_Current()
extern void U3CSmoothActivateU3Ed__21_System_Collections_IEnumerator_get_Current_mFE63DE004EB7A434C9AFCD91EFE181730BBA6BD0 ();
// 0x000003A1 System.Void RootMotion.Dynamics.BehaviourPuppet_MasterProps::.ctor()
extern void MasterProps__ctor_m75E7889DD20AEDFF4B362AD3EFD3AFA7A5DF7223 ();
// 0x000003A2 System.Void RootMotion.Dynamics.BehaviourPuppet_CollisionDelegate::.ctor(System.Object,System.IntPtr)
extern void CollisionDelegate__ctor_m902C43D4DF833C58D5893142F0B327B995A5E6A9 ();
// 0x000003A3 System.Void RootMotion.Dynamics.BehaviourPuppet_CollisionDelegate::Invoke(RootMotion.Dynamics.MuscleCollision)
extern void CollisionDelegate_Invoke_m88AB3391DAA653BA50FF1249C15B75645722ACE0 ();
// 0x000003A4 System.IAsyncResult RootMotion.Dynamics.BehaviourPuppet_CollisionDelegate::BeginInvoke(RootMotion.Dynamics.MuscleCollision,System.AsyncCallback,System.Object)
extern void CollisionDelegate_BeginInvoke_mEF8F89478770040257AB37F31AB2820B1C4ACCE3 ();
// 0x000003A5 System.Void RootMotion.Dynamics.BehaviourPuppet_CollisionDelegate::EndInvoke(System.IAsyncResult)
extern void CollisionDelegate_EndInvoke_m6FD1080748E51F155EA164FF9B72AA9575A38FA6 ();
// 0x000003A6 System.Void RootMotion.Dynamics.BehaviourPuppet_CollisionImpulseDelegate::.ctor(System.Object,System.IntPtr)
extern void CollisionImpulseDelegate__ctor_m165FC9F1850C5A4BD70ADCD231BDAE3D40223989 ();
// 0x000003A7 System.Void RootMotion.Dynamics.BehaviourPuppet_CollisionImpulseDelegate::Invoke(RootMotion.Dynamics.MuscleCollision,System.Single)
extern void CollisionImpulseDelegate_Invoke_mDABC542F10FF44FEEB72894C7436184F0EFE7762 ();
// 0x000003A8 System.IAsyncResult RootMotion.Dynamics.BehaviourPuppet_CollisionImpulseDelegate::BeginInvoke(RootMotion.Dynamics.MuscleCollision,System.Single,System.AsyncCallback,System.Object)
extern void CollisionImpulseDelegate_BeginInvoke_m78A6DD56AF2A038E82DEFCD1782A955E99566089 ();
// 0x000003A9 System.Void RootMotion.Dynamics.BehaviourPuppet_CollisionImpulseDelegate::EndInvoke(System.IAsyncResult)
extern void CollisionImpulseDelegate_EndInvoke_mD3F3886BD7892A555E82C1B5A3850B4F9E7D476C ();
// 0x000003AA System.Void RootMotion.Dynamics.SubBehaviourBalancer_Settings::.ctor()
extern void Settings__ctor_m8625A5A4A7A526DA4A0732B67FDBE6D13A75628B ();
// 0x000003AB System.Void RootMotion.Dynamics.Muscle_Props::.ctor()
extern void Props__ctor_m12A229CCA990A456E3931384B0FDF6557DF83296 ();
// 0x000003AC System.Void RootMotion.Dynamics.Muscle_Props::.ctor(System.Single,System.Single,System.Single,System.Single,System.Boolean,RootMotion.Dynamics.Muscle_Group)
extern void Props__ctor_mCBF341FD742B9E35EE2FD8C4FB9D00DE87B21691 ();
// 0x000003AD System.Void RootMotion.Dynamics.Muscle_Props::Clamp()
extern void Props_Clamp_mEBDEAFCB7FAF273D1846295BF667D4917CBF23D2 ();
// 0x000003AE RootMotion.Dynamics.Muscle_State RootMotion.Dynamics.Muscle_State::get_Default()
extern void State_get_Default_m9F147BF7DB595D1F604A981BA180C743E0C5BE8A ();
// 0x000003AF System.Void RootMotion.Dynamics.Muscle_State::Clamp()
extern void State_Clamp_m1A181B74A1F047C52971C9105DE635559902F22E_AdjustorThunk ();
// 0x000003B0 System.Void RootMotion.Dynamics.PuppetMaster_UpdateDelegate::.ctor(System.Object,System.IntPtr)
extern void UpdateDelegate__ctor_mE9035718C1411B3FCE927E81EAABC46B9A9FC819 ();
// 0x000003B1 System.Void RootMotion.Dynamics.PuppetMaster_UpdateDelegate::Invoke()
extern void UpdateDelegate_Invoke_mA8EB8DA3089CEBD5C73E0C8D43C58FBE5CD551C9 ();
// 0x000003B2 System.IAsyncResult RootMotion.Dynamics.PuppetMaster_UpdateDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern void UpdateDelegate_BeginInvoke_mD1A66CCB14C927AB3635C7DA80F6CFD892A41B01 ();
// 0x000003B3 System.Void RootMotion.Dynamics.PuppetMaster_UpdateDelegate::EndInvoke(System.IAsyncResult)
extern void UpdateDelegate_EndInvoke_mDCCE415B1CB196AD26DE2B0610FA3149F2CC8CA8 ();
// 0x000003B4 System.Void RootMotion.Dynamics.PuppetMaster_MuscleDelegate::.ctor(System.Object,System.IntPtr)
extern void MuscleDelegate__ctor_mD76393A809DE05B32959AFAAB983AB818EC4D9E7 ();
// 0x000003B5 System.Void RootMotion.Dynamics.PuppetMaster_MuscleDelegate::Invoke(RootMotion.Dynamics.Muscle)
extern void MuscleDelegate_Invoke_mF9C7DDD2938689E279556BFA02B717A4EF4512BD ();
// 0x000003B6 System.IAsyncResult RootMotion.Dynamics.PuppetMaster_MuscleDelegate::BeginInvoke(RootMotion.Dynamics.Muscle,System.AsyncCallback,System.Object)
extern void MuscleDelegate_BeginInvoke_m80217B44316E0F026E4C209BE2A5AC97E44F3DFF ();
// 0x000003B7 System.Void RootMotion.Dynamics.PuppetMaster_MuscleDelegate::EndInvoke(System.IAsyncResult)
extern void MuscleDelegate_EndInvoke_m8F48C9088D6A9457C2B12EDB947E9A9B99EA8CB6 ();
// 0x000003B8 System.Void RootMotion.Dynamics.PuppetMaster_StateSettings::.ctor(System.Single,System.Single,System.Single,System.Single,System.Boolean,System.Boolean,System.Boolean)
extern void StateSettings__ctor_m99A580B475E4DD34014A94B53C7F2C1DABD39673_AdjustorThunk ();
// 0x000003B9 RootMotion.Dynamics.PuppetMaster_StateSettings RootMotion.Dynamics.PuppetMaster_StateSettings::get_Default()
extern void StateSettings_get_Default_mDAE02DEE9542D59805031FCACDD4F25C649320DA ();
// 0x000003BA System.Void RootMotion.Dynamics.PuppetMaster_<DisabledToActive>d__136::.ctor(System.Int32)
extern void U3CDisabledToActiveU3Ed__136__ctor_mE6BD06DA4998D2829846E8A69C1E334760B7C7B0 ();
// 0x000003BB System.Void RootMotion.Dynamics.PuppetMaster_<DisabledToActive>d__136::System.IDisposable.Dispose()
extern void U3CDisabledToActiveU3Ed__136_System_IDisposable_Dispose_m9FA62B5E60626A63819B03BCB7785E03AF2DC5C5 ();
// 0x000003BC System.Boolean RootMotion.Dynamics.PuppetMaster_<DisabledToActive>d__136::MoveNext()
extern void U3CDisabledToActiveU3Ed__136_MoveNext_m8E43FFE48DBDF95204CF372A64FA83CAD2052C4D ();
// 0x000003BD System.Object RootMotion.Dynamics.PuppetMaster_<DisabledToActive>d__136::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisabledToActiveU3Ed__136_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m50939F455F4EFBFE76DFA3D0E361ED51A8C9B481 ();
// 0x000003BE System.Void RootMotion.Dynamics.PuppetMaster_<DisabledToActive>d__136::System.Collections.IEnumerator.Reset()
extern void U3CDisabledToActiveU3Ed__136_System_Collections_IEnumerator_Reset_m16F0EB6B51D8D5F28B8E6F6472CB018B00273966 ();
// 0x000003BF System.Object RootMotion.Dynamics.PuppetMaster_<DisabledToActive>d__136::System.Collections.IEnumerator.get_Current()
extern void U3CDisabledToActiveU3Ed__136_System_Collections_IEnumerator_get_Current_m9225135FA8D81E5D3F62E5153E450843B52D511A ();
// 0x000003C0 System.Void RootMotion.Dynamics.PuppetMaster_<KinematicToActive>d__138::.ctor(System.Int32)
extern void U3CKinematicToActiveU3Ed__138__ctor_m45F1A67C78067BE68FC25E4DDCF2425FCD2619CA ();
// 0x000003C1 System.Void RootMotion.Dynamics.PuppetMaster_<KinematicToActive>d__138::System.IDisposable.Dispose()
extern void U3CKinematicToActiveU3Ed__138_System_IDisposable_Dispose_mC2C475433CAE533CF2F07C07C93D20372B3C9770 ();
// 0x000003C2 System.Boolean RootMotion.Dynamics.PuppetMaster_<KinematicToActive>d__138::MoveNext()
extern void U3CKinematicToActiveU3Ed__138_MoveNext_mB77C0F87D0C474597B36A4B132CE6827759B48E3 ();
// 0x000003C3 System.Object RootMotion.Dynamics.PuppetMaster_<KinematicToActive>d__138::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CKinematicToActiveU3Ed__138_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBE320C6CAF45847EA30E23A671A5E97C6B2B8294 ();
// 0x000003C4 System.Void RootMotion.Dynamics.PuppetMaster_<KinematicToActive>d__138::System.Collections.IEnumerator.Reset()
extern void U3CKinematicToActiveU3Ed__138_System_Collections_IEnumerator_Reset_m9ECF77B38E27F125E7553A055B473A75592A6F31 ();
// 0x000003C5 System.Object RootMotion.Dynamics.PuppetMaster_<KinematicToActive>d__138::System.Collections.IEnumerator.get_Current()
extern void U3CKinematicToActiveU3Ed__138_System_Collections_IEnumerator_get_Current_mB8EAF2AD7F59BD09B169214BF1258264EB63DD7E ();
// 0x000003C6 System.Void RootMotion.Dynamics.PuppetMaster_<ActiveToDisabled>d__139::.ctor(System.Int32)
extern void U3CActiveToDisabledU3Ed__139__ctor_m1E03104CE360F023A15B8EFCC6CD698A46692803 ();
// 0x000003C7 System.Void RootMotion.Dynamics.PuppetMaster_<ActiveToDisabled>d__139::System.IDisposable.Dispose()
extern void U3CActiveToDisabledU3Ed__139_System_IDisposable_Dispose_m934F86BBF99E3D2241414F5DC2427D4A56AB7661 ();
// 0x000003C8 System.Boolean RootMotion.Dynamics.PuppetMaster_<ActiveToDisabled>d__139::MoveNext()
extern void U3CActiveToDisabledU3Ed__139_MoveNext_m7C5FC9AD22A75CF9C2239127330FB3E299C90824 ();
// 0x000003C9 System.Object RootMotion.Dynamics.PuppetMaster_<ActiveToDisabled>d__139::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CActiveToDisabledU3Ed__139_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFE7000D2F89EBFBB16B21C2F7A59ADE66E242ECF ();
// 0x000003CA System.Void RootMotion.Dynamics.PuppetMaster_<ActiveToDisabled>d__139::System.Collections.IEnumerator.Reset()
extern void U3CActiveToDisabledU3Ed__139_System_Collections_IEnumerator_Reset_mC0776974F6F881C5140B2974774873B2ABE324E3 ();
// 0x000003CB System.Object RootMotion.Dynamics.PuppetMaster_<ActiveToDisabled>d__139::System.Collections.IEnumerator.get_Current()
extern void U3CActiveToDisabledU3Ed__139_System_Collections_IEnumerator_get_Current_m00237DF83C194E70DB686F82A21A6FAE604A6872 ();
// 0x000003CC System.Void RootMotion.Dynamics.PuppetMaster_<ActiveToKinematic>d__140::.ctor(System.Int32)
extern void U3CActiveToKinematicU3Ed__140__ctor_m76E9307DC4784748B261C450DC40D22BFB1AED46 ();
// 0x000003CD System.Void RootMotion.Dynamics.PuppetMaster_<ActiveToKinematic>d__140::System.IDisposable.Dispose()
extern void U3CActiveToKinematicU3Ed__140_System_IDisposable_Dispose_m03C00836DA8A5774BA3D6058AB776FD56C4E1D08 ();
// 0x000003CE System.Boolean RootMotion.Dynamics.PuppetMaster_<ActiveToKinematic>d__140::MoveNext()
extern void U3CActiveToKinematicU3Ed__140_MoveNext_m0805F7D458FC8C1DAFA2746CD9E6A1ADBFCA278F ();
// 0x000003CF System.Object RootMotion.Dynamics.PuppetMaster_<ActiveToKinematic>d__140::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CActiveToKinematicU3Ed__140_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m16FDB5D544107C3B85585005D0D12A3A29A644C9 ();
// 0x000003D0 System.Void RootMotion.Dynamics.PuppetMaster_<ActiveToKinematic>d__140::System.Collections.IEnumerator.Reset()
extern void U3CActiveToKinematicU3Ed__140_System_Collections_IEnumerator_Reset_mBEC42670BE484BCD731733ED5221A36414A5E79B ();
// 0x000003D1 System.Object RootMotion.Dynamics.PuppetMaster_<ActiveToKinematic>d__140::System.Collections.IEnumerator.get_Current()
extern void U3CActiveToKinematicU3Ed__140_System_Collections_IEnumerator_get_Current_m6DE2BE7367A93745AAC98D1EF6D4DA694291FC54 ();
// 0x000003D2 System.Void RootMotion.Dynamics.PuppetMaster_<AliveToDead>d__192::.ctor(System.Int32)
extern void U3CAliveToDeadU3Ed__192__ctor_m17CD7CD77BD1DF061AB3DFE68C43F112964D1E3F ();
// 0x000003D3 System.Void RootMotion.Dynamics.PuppetMaster_<AliveToDead>d__192::System.IDisposable.Dispose()
extern void U3CAliveToDeadU3Ed__192_System_IDisposable_Dispose_m23A7843354EC8E19B9E426D28D235A8294C58157 ();
// 0x000003D4 System.Boolean RootMotion.Dynamics.PuppetMaster_<AliveToDead>d__192::MoveNext()
extern void U3CAliveToDeadU3Ed__192_MoveNext_m09A893C9973DA8C1881E854B330FBCDE0836A637 ();
// 0x000003D5 System.Object RootMotion.Dynamics.PuppetMaster_<AliveToDead>d__192::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAliveToDeadU3Ed__192_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m96047B7BF2F812F1A210A849A6CF5EB22FCA4B36 ();
// 0x000003D6 System.Void RootMotion.Dynamics.PuppetMaster_<AliveToDead>d__192::System.Collections.IEnumerator.Reset()
extern void U3CAliveToDeadU3Ed__192_System_Collections_IEnumerator_Reset_mA611CC9C671D3C89ED1E8CA033A32F4A2132EB2F ();
// 0x000003D7 System.Object RootMotion.Dynamics.PuppetMaster_<AliveToDead>d__192::System.Collections.IEnumerator.get_Current()
extern void U3CAliveToDeadU3Ed__192_System_Collections_IEnumerator_get_Current_m96B1869FDF4515898A9C7A2E4976E6B60FC705AA ();
// 0x000003D8 System.Void RootMotion.Dynamics.PuppetMasterHumanoidConfig_HumanoidMuscle::.ctor()
extern void HumanoidMuscle__ctor_m48EC18EEC270057667002ADDBB3A79E518D898FC ();
// 0x000003D9 System.Void RootMotion.Dynamics.PuppetMasterSettings_PuppetUpdateLimit::.ctor()
extern void PuppetUpdateLimit__ctor_m941D0329E6CC085B315BF5BEE54733FE081A529A ();
// 0x000003DA System.Void RootMotion.Dynamics.PuppetMasterSettings_PuppetUpdateLimit::Step(System.Int32)
extern void PuppetUpdateLimit_Step_m863B2D4EA03B10C9475C998C127BF1887A7A2CDA ();
// 0x000003DB System.Boolean RootMotion.Dynamics.PuppetMasterSettings_PuppetUpdateLimit::Update(System.Collections.Generic.List`1<RootMotion.Dynamics.PuppetMaster>,RootMotion.Dynamics.PuppetMaster)
extern void PuppetUpdateLimit_Update_m30F3C7F37A60EAEA70273C819C222DBAC268E611 ();
// 0x000003DC RootMotion.Dynamics.BipedRagdollCreator_Options RootMotion.Dynamics.BipedRagdollCreator_Options::get_Default()
extern void Options_get_Default_m9EC809DDC0293363364A0BB39DA71ED726B45E5D ();
// 0x000003DD System.Void RootMotion.Dynamics.RagdollCreator_CreateJointParams::.ctor(UnityEngine.Rigidbody,UnityEngine.Rigidbody,UnityEngine.Transform,UnityEngine.Vector3,RootMotion.Dynamics.RagdollCreator_CreateJointParams_Limits,RootMotion.Dynamics.RagdollCreator_JointType)
extern void CreateJointParams__ctor_m9DDCEB8D59DE3F5F954B09B52F1F5D3F7C5FD2B9_AdjustorThunk ();
// 0x000003DE System.Void RootMotion.Demos.CharacterMeleeDemo_Action_Anim::.ctor()
extern void Anim__ctor_m2577993D4DA14B01F7EE9369A2993F726E088E6F ();
// 0x000003DF System.Void RootMotion.Dynamics.RagdollCreator_CreateJointParams_Limits::.ctor(System.Single,System.Single,System.Single,System.Single)
extern void Limits__ctor_m1F48E7A4951F39DE86FFA9053A21490357A0A843_AdjustorThunk ();
static Il2CppMethodPointer s_methodPointers[991] = 
{
	Skeleton_Start_mF4E67DE657C4EA481F8E38A5BA18BF52B226F6EC,
	Skeleton_OnRebuild_mD8D147125009BEE13E1E6FFBE68692CC5C20DE5C,
	Skeleton_OnMuscleRemoved_m90238A8A69AFE3ADE235B0956ABF5138C3224A6F,
	Skeleton_IsLegMuscle_mCD1014B54FB52FFB98CD62E55553B85FBDBC5E38,
	Skeleton__ctor_mDBB38CDFBB1D92AD96FE2BFF83B53A9D4F468558,
	CameraController_get_x_mB38627ACD13A0433C003B2CA9CB00CEF9C73ACAD,
	CameraController_set_x_mDDAC65936DA7CEA5156B0EC3677466E79FA1DA95,
	CameraController_get_y_m35304C2395EDD49D66639E528C2DC9386F3B0F8D,
	CameraController_set_y_mD0222EE6DAF94275CC7F21D72635AAE7F0EB12D2,
	CameraController_get_distanceTarget_mD806C0E22859D79334CA9A59C2C24C4C4373CE86,
	CameraController_set_distanceTarget_mF07AC56B1302E4F1E28422CC82E70EF857FDD320,
	CameraController_Awake_m5FBC56193C89322F8E2391B8A5DA36B3718A46B0,
	CameraController_Update_mDD7D1D87264444B69E033F7E0E54AA997002DF4E,
	CameraController_FixedUpdate_mE6B3E2617546629D9EF05F9D810EA03CF14A4061,
	CameraController_LateUpdate_m08AE76EBCA0760E4C39CBB75CE1A4061E472EED5,
	CameraController_UpdateInput_mF33A7394E88D096EF0AF7AB9CEB77CE0B06515EB,
	CameraController_UpdateTransform_m9D84649F399911F1B0378D7C4A27101A8B824F46,
	CameraController_UpdateTransform_mE951741E10350226C58AB57C7291A9FDEAD8CAB6,
	CameraController_get_zoomAdd_m776E555CB2660685855ACCA83A289609B2DD8965,
	CameraController_ClampAngle_m6F19803B13D07E13BE330D65D6194193AAEB2264,
	CameraController__ctor_mBEE86142FB97255F9249E3508D7273541996728A,
	CameraControllerFPS_Awake_mC31E44E1175C7C6610825E1114DB77985B012363,
	CameraControllerFPS_LateUpdate_mAF198EAEE545135331BF4FE2CCB2C6175363D883,
	CameraControllerFPS_ClampAngle_m922CBC59F48965F6AB58839ED060E59181871D8B,
	CameraControllerFPS__ctor_m11BB35374105CF5D83667BCC71759C870B54BD71,
	AxisTools_ToVector3_mCD503009B0780195B310046A0997E22909E6B383,
	AxisTools_ToAxis_mA2CAD6CF2A7860002286BC038CA81B1165D47F22,
	AxisTools_GetAxisToPoint_mC9798C7443E866FD0ED4CB5A26DE289FD87B73DD,
	AxisTools_GetAxisToDirection_m2E6F2E35366E46C9171B91C594500F54BD52F0DD,
	AxisTools_GetAxisVectorToPoint_mFBFFFE2DF1D8FD4FCB9A5F133BDD21E222E0A1F7,
	AxisTools_GetAxisVectorToDirection_m0E72B70F2316F9732F5793FB5DF2D3E93A49DD3C,
	AxisTools__ctor_m856184BD6721EE6094162BF0105E4A4C8288666B,
	BipedLimbOrientations__ctor_m628014D35803DAB72046E4FAC5476B8DB5248D31,
	BipedLimbOrientations_get_UMA_m1B4A8EF3D56D04D43582CA0056F77208CF3F70E9,
	BipedLimbOrientations_get_MaxBiped_m878607BA4C487DFF0773656452493C0636B10555,
	BipedNaming_GetBonesOfType_mBDC17B558CA273758E18C4422AC1988E6FC98C18,
	BipedNaming_GetBonesOfSide_m18E08316FB58D81997B8075717DCE82AF424551B,
	BipedNaming_GetBonesOfTypeAndSide_mC4AA07963100FE3DCBACB470D8A2D5326C624DE1,
	BipedNaming_GetFirstBoneOfTypeAndSide_m37ED9E3AB05C0027CB2CBF89A924496F4E858E5A,
	BipedNaming_GetNamingMatch_m07DCBB41BA4F914D3BFFF4EAE5CC54C4CA8BC45A,
	BipedNaming_GetBoneType_m6ECDE309F7EC61C6B96AD29FDDB4B23CF6ADD7AA,
	BipedNaming_GetBoneSide_m416D27D838BF78EAF90E6468A3346F886F7922EE,
	BipedNaming_GetBone_mF8EE1CF09589C2486CE0670DFF8C6F5A003CFB23,
	BipedNaming_isLeft_mFEE12954060CA56C42287319F51828E652095D80,
	BipedNaming_isRight_m279D139974DCFD68EB6F0E666F88850E2CE22C60,
	BipedNaming_isSpine_m108D9FF35A112C6418A1189888877D3350E6DB7B,
	BipedNaming_isHead_m977CDD1DE2D5E42F6770047421677F4995F790C9,
	BipedNaming_isArm_m4401AD291EEDA15834D0D0D64DDD990824D69E4F,
	BipedNaming_isLeg_m3CAD732B60831C842EE9E1D9D07D5CF678D7937F,
	BipedNaming_isTail_m59197C34830C9D0142A150FFFC2B235FE24C5033,
	BipedNaming_isEye_m1F65D1E12C1C82BA3341C152C535A6CD857D1F84,
	BipedNaming_isTypeExclude_m2E7BD947E61BEB518E29BE7748A47224A2BB3644,
	BipedNaming_matchesNaming_m457322B452EB97308839E8202735115D4F202406,
	BipedNaming_excludesNaming_mF890A0D56DFEF3A3FB083DE2CEEB7547F0B28CD9,
	BipedNaming_matchesLastLetter_mECAD3C3FE434A94420E73882B1203185A0FA70C6,
	BipedNaming_LastLetterIs_m0A633F180E882DE90E7FDCD55542131A703141EF,
	BipedNaming_firstLetter_mA04DC15B9CC358F32EB08711CD7B7C543E878418,
	BipedNaming_lastLetter_m6309E0A4ADFF5A6256A48B63FFA0C96C216FF212,
	BipedNaming__cctor_mB787E279939F32302988A01537088F13830A192B,
	BipedReferences_get_isFilled_m532616CFF9B9F487D198B337C4E2CFC97668B5DA,
	BipedReferences_get_isEmpty_m37BC8894320B138293100AAB2110819B5EEED103,
	BipedReferences_IsEmpty_mD2484F0388AF9B9D799BE45BFAF9BCE81A2498FF,
	BipedReferences_Contains_m00FC8E73E8D1E6855AA5DA3AD098CD2421CDEDF8,
	BipedReferences_AutoDetectReferences_m480B4137C8758F57831AF089036EE0A3A67A99D5,
	BipedReferences_DetectReferencesByNaming_mC4AA23322AA015044E090C711B99F0538DDF8879,
	BipedReferences_AssignHumanoidReferences_mD2020E45A8C9FE92D05E75E8456799489F1CBA1F,
	BipedReferences_SetupError_mBBA8543B0CE84E5D5C72E9EE3550ADE1A8957B1C,
	BipedReferences_SetupWarning_mB5A044E4AAC480E11CBAC069ED985226204AD67C,
	BipedReferences_IsNeckBone_mAD641572662F55A3BFA9200CEF2C49C1C04611C3,
	BipedReferences_AddBoneToEyes_mDB9C8D26AA6D2E371B12117A99881E27DC84B21A,
	BipedReferences_AddBoneToSpine_mC2C9AAB83D61FF52580C53993577533DB6FA3375,
	BipedReferences_DetectLimb_m7998E2F989BF07D7ECD91DE4AC98C7C88F6FD04B,
	BipedReferences_AddBoneToHierarchy_mF4BB305D8BC48B162251E573DCB29E720687355A,
	BipedReferences_LimbError_m961DEF3BDA371552C207A5C7061B46334910F96A,
	BipedReferences_LimbWarning_m8883041D57B3E96ADB064B5256EC41EF80C3364D,
	BipedReferences_SpineError_m8124177406CACA5A9C2C35C2E218FF5C5DAC768C,
	BipedReferences_SpineWarning_mAB17505D98D0625A851178E35EE2181982B03DE4,
	BipedReferences_EyesError_m6B621EAAA5C86D1C20C575FFEFDBB1FE9680C602,
	BipedReferences_EyesWarning_m0597A26ABEEF3864B1D45767FD53AF6C315F6348,
	BipedReferences_RootHeightWarning_m4F63F8A0697B363761CBF6C2C01371DE7F089A9E,
	BipedReferences_FacingAxisWarning_m24D82893DEA747A31AB498537AE7B68E8DC48E36,
	BipedReferences_GetVerticalOffset_m6EC5AA10A16C757A4601261BCE1858C1A965BD0A,
	BipedReferences__ctor_m22C81763C6BCBEB21D611DCFBF6417E74E374258,
	Comments__ctor_mC318D7C889BD34F9720479808DE974A219C708BA,
	DemoGUIMessage_OnGUI_m3CA388F7FAA8DABF4C4F3B39D8FAF7C28B51F424,
	DemoGUIMessage__ctor_m9C4AE190B3977074C3B65350952C95E51FD8CEC8,
	Hierarchy_HierarchyIsValid_m98AF5CA108529E03914102F181875DF6789CA4D6,
	Hierarchy_ContainsDuplicate_mF0EA7DDF773AEAD13C2DC2F18982C225878DAF93,
	Hierarchy_IsAncestor_m749A3F4684360F7A093751B48CEA48A112ADCC8E,
	Hierarchy_ContainsChild_m4DD0E4A39956016A9C23EB196688D9B06C2EAE0A,
	Hierarchy_AddAncestors_m9E0BE7DA6055DACFCD61527F9E7348100E0CFF9D,
	Hierarchy_GetAncestor_m8595FF4227653B0C249BFC57C1E180C4872835F9,
	Hierarchy_GetFirstCommonAncestor_m4B1387FB35547ECA3856D79665D088913121C383,
	Hierarchy_GetFirstCommonAncestor_m0A8BCACB0C4EB1A8DD1E8FAAC40D236FC16203CF,
	Hierarchy_GetFirstCommonAncestorRecursive_m962DE35E5208E0EA02D4B334777856F83839CBC6,
	Hierarchy_IsCommonAncestor_m4B5549450455C71A57EA8A4191E07C3B5AAA1CF7,
	Hierarchy__ctor_m08940D4DE4D2BA15C094FCE918D6CC043CA9C85A,
	InspectorComment__ctor_m36EF2C7214BF1CA6306E15CD56C4BF8BCBEC6F30,
	InspectorComment__ctor_mAE8A93B7E88F9F6AB5B56A36DC710A530D977E34,
	Interp_Float_m8315388009920ADB90310DEA6EC6A48F8A596035,
	Interp_V3_mD52ECED9A462AA2139A1E514B0B8993849C93A62,
	Interp_LerpValue_m55BF212FA996148F68F5884AA0D0DC2EFE3429B2,
	Interp_None_m62275D882F59BC2EF6FE267544FD6428AD53D778,
	Interp_InOutCubic_m9234832B9839C5EC19F5F83D1BA47D550E0298B6,
	Interp_InOutQuintic_m430A945746907AF2C321F1792E51C7ACF7E66B40,
	Interp_InQuintic_m4808AB9E20C94C2D9C9D6557CE4A2555B9AC8970,
	Interp_InQuartic_m458ED4544667AD67B868A1F2EFE9C50ED066CD10,
	Interp_InCubic_mE1594EF4BB7685BE8A0FBCA6CD6596C5AFDE0153,
	Interp_InQuadratic_mFD41A4AD631DB7952962A23774713CD4C97B72C8,
	Interp_OutQuintic_mBA4F699F0566FE2F8BF5830B60887633A5025880,
	Interp_OutQuartic_m186A3354FA77C8EE50BAD305FDDDF2E19C20F846,
	Interp_OutCubic_mEF6C796EA66CAF998965778AE6F381B366FCDF90,
	Interp_OutInCubic_m19389B61E0D7F6DBBC26893B1770690EEFF62EA0,
	Interp_OutInQuartic_m2801A8B9F2214F0DBCFA7560CE5C0755C889D2F8,
	Interp_BackInCubic_mE00BE40371B334F077A484EBCCF9452BA8127136,
	Interp_BackInQuartic_m87D76B3B89DB7538099BF034870F6A11337FBC1A,
	Interp_OutBackCubic_mD5A333EDB3BA5E08B8C3491FB65CD435162DB89F,
	Interp_OutBackQuartic_mE6FA382DD1E9B5E1EEC4B8C58928B2CA0E4C676D,
	Interp_OutElasticSmall_mE28AE19A061B2C87727FF77F91481E09181FE697,
	Interp_OutElasticBig_m3B5DC741EE5FDBFF0DFAA9389374A5F2884692ED,
	Interp_InElasticSmall_m452AB8C4F78F4D239F97E4E4D34BE14F026C7D82,
	Interp_InElasticBig_m040CE5F05E73BA906F5AC0C03FD80A45C0DB061E,
	Interp_InSine_mFA4D2EAB9F229F95344740DC1351E357DCC48007,
	Interp_OutSine_mD40D8388BAB5E4AE1F286740388A9685B112DD9C,
	Interp_InOutSine_mB010D9D50EF06D636E56253D6D36B29494EC2A21,
	Interp_InElastic_mF213B6EF60418456D6D57C38A67B6B2D817A826B,
	Interp_OutElastic_m38EC0CD8C33D07B8A5CBA52A6FF3F52F3FEE8AA2,
	Interp_InBack_mB9F3C5044904882DFE32BD58939B66CD91F681DB,
	Interp_OutBack_m859EE78507AC785355CE2E62D2913861F98E81C7,
	Interp__ctor_mE2D6F710C7DE6279DC65035463CCFF4611F7F497,
	LargeHeader__ctor_m242EAA703F6345754944786FEBAC1536FF97A8E7,
	LargeHeader__ctor_m1AFECED29C7D43C3A7E2A66A0D02DCD5DD162954,
	LayerMaskExtensions_Contains_m9AA25A3336A1D6CCB3D78CF207310E988A1F968F,
	LayerMaskExtensions_Create_m86A9DD5238E84499AD59F9CD4F87461773878993,
	LayerMaskExtensions_Create_mD56ECB38C3CB567F3852747CBF40680E44547471,
	LayerMaskExtensions_NamesToMask_mB6A159D6148E5DE247F35CAFA792B637A48E5AE9,
	LayerMaskExtensions_LayerNumbersToMask_m076C790344B579202E95813AB4B567683131E4C7,
	LayerMaskExtensions_Inverse_m9014247C9AEBF3E71D2298F0E6F4990987D91322,
	LayerMaskExtensions_AddToMask_mB8C61CA62DC235C390D7F0BC59A168DBD8E6D87E,
	LayerMaskExtensions_RemoveFromMask_m2647A6FDC8CCF34C96DA2E7E587AD4B315794816,
	LayerMaskExtensions_MaskToNames_m2D9C04F2C3BA77FB12821060BA9AA9F77B9777DB,
	LayerMaskExtensions_MaskToNumbers_m8EC7DBEBD94817574AEB00ACA440FCF4B5F93CB4,
	LayerMaskExtensions_MaskToString_m022D4B6AF4F29DB15BC299D0AD93A2667BD00933,
	LayerMaskExtensions_MaskToString_m2C8EF33ADD2B9395541C452C22AB910A6B3890BC,
	QuaTools_Lerp_m0570DD53D86627AE23F9E1DDAA9B4033FB461CB3,
	QuaTools_Slerp_mAB2B4EF8CCB8D2F61921608F945E8DAE8782345C,
	QuaTools_LinearBlend_m0EF0482993F1EDB7CB148D29D5F3C1BF1EC91192,
	QuaTools_SphericalBlend_m3DEFA8B64AE7DF900C78F51FD9695814D306114A,
	QuaTools_FromToAroundAxis_m59C218E5622BC695F655346A098BAA288D20B437,
	QuaTools_RotationToLocalSpace_m0D8E4AC34823B7EBC5F399DF7B604C578F21B06D,
	QuaTools_FromToRotation_m3D768D23A4BCEDDF46F0F2EA0FCC824801DD95DF,
	QuaTools_GetAxis_mD45851D36E9DC32CD18D49AFA83B496F91D442BE,
	QuaTools_ClampRotation_m2CC4F0F1F609A43A1856ED4E5F415B76DAFBEE5C,
	QuaTools_ClampAngle_m063A5914F8978FF25DDE339CB60368E80A5F4B4B,
	QuaTools_MatchRotation_m3D0E437711B13691D3BC3CF8E37A751B47F9688E,
	NULL,
	NULL,
	NULL,
	NULL,
	SolverManager_Disable_mAF7F1B03409CD5F15B92EB2CA3E4CDCC78822A6E,
	SolverManager_InitiateSolver_m5BB7CCAF5151B14EFB11A6177B95097C87DF757F,
	SolverManager_UpdateSolver_mCFA8146EF728678AA6530CB351AF15AB2769A473,
	SolverManager_FixTransforms_m5915C5F3E7AE3003DF5BC8FAE2E29484AA133E91,
	SolverManager_OnDisable_mAB35E35F021A75E36EB0761D6ABE2909CBE78081,
	SolverManager_Start_m1898B450B9DF67B21D2E1D0E464C18E58185449E,
	SolverManager_get_animatePhysics_m6B149D06101817217013A43B85E03BB77B635F64,
	SolverManager_Initiate_mDF420FB1D3F8C66C4F30F36D6FCDF83D2C2B1C13,
	SolverManager_Update_m81D50B25FD8D018C41ACDECBE8D7F99437B5E20B,
	SolverManager_FindAnimatorRecursive_m405BC00B17F6227AC117D6D1E688355144F3FFA1,
	SolverManager_get_isAnimated_m59432E7A0EF712F3A1A72CA2B21F3DAA9BC42182,
	SolverManager_FixedUpdate_m7925F1E72E87A32BC92909A734309CE40C092E83,
	SolverManager_LateUpdate_m7E4147E101C1F2692CE6FDE75CE779FAD0AE5488,
	SolverManager_UpdateSolverExternal_m622BBE432080EB672523141DDB023CD89EFE40A4,
	SolverManager__ctor_m5EC263C17E103B73190883B068920291998D403B,
	TriggerEventBroadcaster_OnTriggerEnter_mF7981EFFE30D975B741F473339B9B0EE89BA7A90,
	TriggerEventBroadcaster_OnTriggerStay_m034359136D2CC6615C0E30892519E7E1424649F4,
	TriggerEventBroadcaster_OnTriggerExit_m9C1FFE01E7F045A5F521AE38DA9345945401D783,
	TriggerEventBroadcaster__ctor_m15BA15CA6936262AE959073DAC087952849E98F8,
	V3Tools_Lerp_mE627D3C6353F8119AC6F187F049BB39CFC8BF293,
	V3Tools_Slerp_m559EC1621BE6655096DB52FE6502209A495CE4B6,
	V3Tools_ExtractVertical_m21E5C5CC0291B497B2AE92FBCD5ACAC30CDF0A4A,
	V3Tools_ExtractHorizontal_mDB3A7E16FFDF5F296800F5F920D7DF0336564F73,
	V3Tools_ClampDirection_mE723D67930F3385280314BE79B2B2178279CF71F,
	V3Tools_ClampDirection_m12D4397DF2774F461537E9806AB36367807CA6DA,
	V3Tools_LineToPlane_m75F1F5804DC9A3E3BA11B42957CF879EA8242A3A,
	V3Tools_PointToPlane_m85DCF160324062BEA6A92414B7F9FEBA64CD8829,
	Warning_Log_m6B0EC4690D2F7990B353426976FCA58FE2448ECE,
	Warning_Log_mD2162F6CAE666F6BA1981317A3AAC1CE778530B8,
	AnimatorIKDemo_Start_mA97FCA1BED2930ACF9C57524EB0233A8ECA5878E,
	AnimatorIKDemo_OnAnimatorIK_m79EEAA1888CD25C98B8F7B201DD42225A9C7BA9B,
	AnimatorIKDemo__ctor_m508EC83475C03FF9F1C7FFBB191A5EE3EA4D0777,
	BallShooter_Update_m7532DC8514B5AB4E2088BD420A7CCD1075C96755,
	BallShooter__ctor_m4ADD8786FAF7276FB5C8D8F644AB8F48FD4F8932,
	BoardController_Awake_m285ED6882E5546F42518748021C79846E3364CF4,
	BoardController_Update_mA3FD75C4AD09DF209973B2F5A13181C3BEBE2060,
	BoardController_FixedUpdate_m6884DD9296FF0D069FC8A6F7FD45AB28D7D1C053,
	BoardController_OnCollisionEnter_mCA697E7CD8CCF5A34027B462C9817CC449BD5387,
	BoardController_OnCollisionStay_m54FA0AA47BBDEDE71F5BFB24721597670990263A,
	BoardController_OnCollisionExit_mD849AB5111282B2DA88ED2F392422FC459EEE3CB,
	BoardController__ctor_m3E01DC4D30B81C7D861C63E61C69B7CAA1061CE0,
	CharacterAnimationMeleeDemo_get_melee_m0D347C6E86DFAC7184BB380B35E21E5135A2C523,
	CharacterAnimationMeleeDemo_Update_m092A4BB714901C85CFBBD9BFE32E0D8BC73F574A,
	CharacterAnimationMeleeDemo__ctor_m1748A1BEE8CFC253B260F90BAFB58DAFE2F98890,
	CharacterMeleeDemo_Start_m39E0F9ABE2B86AFD08487A1B1C8B376637EC4594,
	CharacterMeleeDemo_get_currentAction_mD379E363F1E9281ED86CE7709F1DEFA29C139696,
	CharacterMeleeDemo_Update_m75CB5D150931A730D5BBB0790B62EF37F1279C1A,
	CharacterMeleeDemo_StartAction_mA9DDD4A69D168DE35E170CE17EAA4829E7535553,
	CharacterMeleeDemo__ctor_m0157CE34094107AD563DE1CC91DFB4217499CCAF,
	CharacterPuppet_get_puppet_m8678A5D70042C6C7EFA3D56AE46756D6E1DCB721,
	CharacterPuppet_set_puppet_mBB33AC33AA6BBF41D3297C33CB952217870920A6,
	CharacterPuppet_Start_mDBF0CBB08F072520EFB185B92042F78FAE826ED9,
	CharacterPuppet_Move_m79E6280BC30C8133EE81939C026A8DCEF47FA6F9,
	CharacterPuppet_Rotate_mD924F477B414028EDA602CB9958DEF29293F384A,
	CharacterPuppet_Jump_m999435B14362886C4DA46B7BDD0C6F1BBA4AE91D,
	CharacterPuppet__ctor_mE623C03BBC0A2AFB91016C6CF5BBC971909D545D,
	CreatePuppetInRuntime_Start_m721142FF986BEE095E4B62A210E51536B283A5BC,
	CreatePuppetInRuntime__ctor_m07E8BFBEAEC32FB0C5302F070B4CC1A0085F6A4F,
	CreateRagdollInRuntime_Start_mE9639DE3EF8793E97DA8E7B4A6852DC7FDB830AF,
	CreateRagdollInRuntime_Update_mDCB816ED4D5F9803D06F66D2BA52A8D16CB41023,
	CreateRagdollInRuntime__ctor_m2BCDB173772ABC7F38B76F7BB03DA3F8D9703086,
	Destructor_Start_mAADF48E45731FFF0B0A9E8C9A638E36447D72FDE,
	Destructor_Destruct_mF60E3DCAA6B3487CDBBAD6370F6FA3B18F74A238,
	Destructor__ctor_mDD213AA0BEF75EF03B7134D6F54CB617BCC3BFD1,
	Dying_Start_m7BA6FA03A78E86E2CA49D172BDCAB1F9A74EC789,
	Dying_Update_m7F5DEEC5E9C54BBE992943A87AD1331430EACBF0,
	Dying_FadeOutPinWeight_mA1206D05CAD834919911F06BEBCCB6182CB1F60C,
	Dying_FadeOutMuscleWeight_m5E1A1010D6D79CEA5DA5C4C65B7D4BC0F22A253E,
	Dying__ctor_mED8691843BDAE2C26F57995F57F34A82B5CA0718,
	FXCollisionBlood_Start_m956562DCCF786A82B2B2C3FFC363215CCCEBBAD2,
	FXCollisionBlood_OnCollisionImpulse_m93A4671E896A2BD8A2D470487DE59B33D00A04D8,
	FXCollisionBlood_OnDestroy_mE198DCB5C810D74B032A111F53C376ACF47CD701,
	FXCollisionBlood__ctor_mF88C219CAD577DFB23A939A40F6B2FA2A27DFA38,
	Grab_Start_m1F759583236AA67EE6C00CAF82F87C723EEDAA1A,
	Grab_OnCollisionEnter_mDDD2DF7E47C1DD3C55245B1D2FE9DAB2B5F3D3F6,
	Grab_Update_m7419324CA39BD4B0A613D1A250D1653D06517FF8,
	Grab__ctor_mF222A4D491694F0E92F1A74BDAE39BE67E29253D,
	Killing_Update_mF24EDDB544BFDAAE3DB6007DC66D64717C380F85,
	Killing__ctor_mF47D6EBB14CF827F1B95C390BE265723F2874E02,
	LayerSetup_Awake_m7DEA59CFFB9AB7101A673DDD78EF4DE155D08D08,
	LayerSetup__ctor_m183877FCEF105AA555BD0045D47DBB4E881F35C9,
	NavMeshPuppet_Update_mB5C5469A60D850CA9554859F56090F015B14C852,
	NavMeshPuppet__ctor_m82E14CDCA0F5A5F4712C93F3635743E05A20A855,
	Planet_Start_mDBDB3B54F73994CCF7A64CAF362FA5ED98E30487,
	Planet_FixedUpdate_m401221BAF1CDED4123AF69E9F6093F322655AC0B,
	Planet_ApplyGravity_mBDC4C2E37E73CE3D2CEDD5EA2555A9104AFF47F2,
	Planet__ctor_m43512F914349F64019272DB89239FBE208D7FCFC,
	PropDemo_Start_m5DC538E6010F108DC192AA348F4C66E7C7F3EAF8,
	PropDemo_Update_m9DD648186BB19E3CF27F0F8CC9D7D13F97A06D9E,
	PropDemo_get_connectTo_m13EC341FBB271BAC5B2D5FD28B99BB083200593B,
	PropDemo__ctor_m5740291D7A66B17FA49C295832C708F89C1560CE,
	PropMelee_StartAction_m2A72D775590077C627FFE6DF24211AFA818ADBAC,
	PropMelee_Action_mD4068BB6BB5DEF26303E7AE86763EAF3C211E222,
	PropMelee_OnStart_m6AEEFCE5506EA20FE1D34F0C7049237ED17E77B6,
	PropMelee_OnPickUp_m3E934DA37529CE775F114524241161B5912576FD,
	PropMelee_OnDrop_mD0D7342C3259D31377120A808A264AC5E1E96C81,
	PropMelee__ctor_m4E6510C22CAE46DF32260D9DF9A58FCB8E89DB99,
	PropPickUpTrigger_OnTriggerEnter_mE061C5D1591FF54DF4AA6118745F86333CDAF22C,
	PropPickUpTrigger__ctor_m41F1890365B6312EE8D70C1F0D564414CA453410,
	PuppetBoard_Start_mF108C27EAA27E97F68DE3601167EEB6CDEAD14F8,
	PuppetBoard_FixedUpdate_m9741339591C31768CBE55F73C40553872BCE30D1,
	PuppetBoard__ctor_mABFD0B358B0B646513F3F676DCAE154313285D51,
	PuppetScaling_Start_m0AE9F0F94DAFC70ED292338F84DB5589037FFB9D,
	PuppetScaling_Update_m6426071E460E9AE2FB33EAAB240F46FCF08462B4,
	PuppetScaling__ctor_m7EE7A16B33263640B714C99D39321B3C1E767D14,
	RaycastShooter_Update_m22C91F5E76AD1CCB3A6F97CF170A76ABD0637B0A,
	RaycastShooter__ctor_m01ED18AD26D9D718E1F0E68017DFF5E88726825E,
	Respawning_get_isPooled_mEEBDA48A9F70320688F958CC8C892218716BB316,
	Respawning_Start_mDE8312A8D9AFE128B54B867655EE5FE81AB26C38,
	Respawning_Update_m55EE2AF65B21D2117BADA5D5CCAEF5AE0AC850A1,
	Respawning_Pool_m7FA999B8D6E3443881080A9DED97764B92C1DD3C,
	Respawning_Respawn_mE5E0F1A996DD4BE003376E989EACCCAACCFCF762,
	Respawning__ctor_mCF4BC2871CBC5AD975B5B91136E9302DDE1F7112,
	RotateShoulderToTarget_OnPuppetMasterRead_m1B97FAE39EEB786BCAFB7BBEB1A489D101793639,
	RotateShoulderToTarget__ctor_mF4ACD4582DF7FABF0781DF4CC5FEB6344CB214A0,
	SkeletonShooter_Update_mB2583DFD8CCD5712F3C4383B7257B5175F2F998F,
	SkeletonShooter__ctor_m19030E1AE83AAD14D46675742C063DBC69843733,
	UserControlAIMelee_get_moveTarget_m496EAE4EB9A3AB089E00B603EE478C5B439E7037,
	UserControlAIMelee_Update_m7E3791BC25407CC1BED0A90BB210AA7509A0DAEF,
	UserControlAIMelee_CanAttack_m85208227CFFA9803E619A6CFD05A46B50AF81184,
	UserControlAIMelee__ctor_m0A925F7788B39EA66A1A21E999678D2F16F0909A,
	UserControlMelee_Update_m6835F9DBE4B2F748A9AFCCB87FC7C9A0B749734C,
	UserControlMelee__ctor_m39B8505FE47A9E83F54E841EA5A218E9A43AB851,
	CharacterAnimationBase_GetPivotPoint_m1018CB765301AE015F036104328BF4508E14A473,
	CharacterAnimationBase_get_animationGrounded_mF576A1A357D965533A534EB9A6258FE0FBD94D79,
	CharacterAnimationBase_GetAngleFromForward_m308199E64F4970AEBBECEB1E4EC3A35CCFE0B095,
	CharacterAnimationBase_Start_m8A3E2715CC16BA1ABDE37F57C108202DCE707204,
	CharacterAnimationBase_LateUpdate_m69F9ED282C2B5ED54BF008790992FD41581284C2,
	CharacterAnimationBase_FixedUpdate_m9BD9AA4ADE8C6D01C0FE1C4E3F37D8EDE9C6EC26,
	CharacterAnimationBase_SmoothFollow_m7B5158868C4342133F57210A84411B4F600F81E8,
	CharacterAnimationBase__ctor_mC5598C331E3F61B698957C31DBFAB6A434A44285,
	CharacterAnimationSimple_Start_m92D1575C4FD4B3E66B584C2C34B597295122B5AB,
	CharacterAnimationSimple_GetPivotPoint_m4DD7B1306C196C1E9843A2ACC1502807F58F6836,
	CharacterAnimationSimple_Update_m1C9704FEBC5B20924117AD5B3D0FE73167683874,
	CharacterAnimationSimple__ctor_m9E57665AC7464BD698C02E1B41D80A8094DB7B7A,
	CharacterAnimationThirdPerson_Start_mCE97CA2A3B883DB660296E19984870F30993601D,
	CharacterAnimationThirdPerson_GetPivotPoint_m3AA31633895332745BCD4856339A237C14BAF048,
	CharacterAnimationThirdPerson_get_animationGrounded_mB167095B5D2E233D74FEBB1407DD18858CB1A7C9,
	CharacterAnimationThirdPerson_Update_m6BB492201C5EE44476FCD9F10E1CFE94DE7FB06E,
	CharacterAnimationThirdPerson_OnAnimatorMove_mE2B33D242C221CEE05992F4D4297E61F94BE3A8B,
	CharacterAnimationThirdPerson__ctor_m7FA89D7FD0CE7BBAC61C92A773A73392D2313F6A,
	NULL,
	CharacterBase_GetGravity_m52928070D1FD9818AEEA17004E67BCD3EF349DBB,
	CharacterBase_Start_m97EBCF95A437AF5C809AB15FC3560B6DCB1DDC04,
	CharacterBase_GetSpherecastHit_m665E7A08DF4FD1E12CB09A62D1EA594CD898B7F0,
	CharacterBase_GetAngleFromForward_m1D8A6EE938756474E581F74D5E750089A8752902,
	CharacterBase_RigidbodyRotateAround_m627B8E966C6CFD3442FE1802C266E6C8562C628D,
	CharacterBase_ScaleCapsule_m7E46447FAE188DDDED07DC75BE2B2587224B1FD4,
	CharacterBase_HighFriction_m5B9CB0E513FE38E2F32790D8EB08EE7311366706,
	CharacterBase_ZeroFriction_m60A9ECC47F369571E2220712B33AD6649688BE97,
	CharacterBase_GetSlopeDamper_m36021B2F5C9300CBE4514BFAFA57A35038012493,
	CharacterBase__ctor_m643636E039B8359397126853AD95BFC407156127,
	CharacterThirdPerson_get_onGround_m8FAB65BBFE10A7D958BE8562C781004454B3B185,
	CharacterThirdPerson_set_onGround_m7138F56B42D5F8F41D8F0239C804A79DB39E20CD,
	CharacterThirdPerson_Start_mC010FA7FEC916FA3490C696F5D5908F540AC273D,
	CharacterThirdPerson_OnAnimatorMove_m6E67668759563100F5E41DC29AF3C5B415C03D4E,
	CharacterThirdPerson_Move_m856C3FBDA610CE24FF7F1EC6E2DE6AD093B55A7C,
	CharacterThirdPerson_FixedUpdate_m9E62D0B642FE1E8D883D019472958E8E2B3D9310,
	CharacterThirdPerson_Update_m33EEAC902435B41B78B03339D87FF82B16D04EA4,
	CharacterThirdPerson_LateUpdate_m204DD570122E903D4BE39F7B30654569CC1F931B,
	CharacterThirdPerson_MoveFixed_m8025BEFB98AA606ABDE2828A72087290F89596F9,
	CharacterThirdPerson_WallRun_mFDE6238062E36512E1C259C2111AE4C4C688321E,
	CharacterThirdPerson_CanWallRun_m49F8A48969093D765DF8887E6C5798E242BE5B6A,
	CharacterThirdPerson_GetMoveDirection_mACB1F2204C72B8DFD0AD7D1ACB7B42BC8DA3AA7C,
	CharacterThirdPerson_Rotate_mEEBAF749E60D2B4C0ED2C7FA6B7302B54FD0946A,
	CharacterThirdPerson_GetForwardDirection_mA7F39ECCF4558CD5A702BEE32F5CFDF30864E16D,
	CharacterThirdPerson_Jump_mDF0BC561C27E93A0A60F2B84CE443D2104B9A083,
	CharacterThirdPerson_GroundCheck_m5EF602C558AA2A106A20EBCDF2468164E481A549,
	CharacterThirdPerson__ctor_mD74DE56D370C9B25B833BC60798FA858F9FE9B7F,
	SimpleLocomotion_get_isGrounded_m896F64939C860C875BAF5DA09407B9B4E9C3388D,
	SimpleLocomotion_set_isGrounded_mD20363625AE25634D3971FDBEDC9F26E65CC91AA,
	SimpleLocomotion_Start_mE9882312EB255A8DB96BFE19C852A10718BB7949,
	SimpleLocomotion_Update_m5040C75AC416DF82E9D695922585024B360E76F7,
	SimpleLocomotion_LateUpdate_m438B1B1FFEB4F9D6393DCA233C294D07A5567653,
	SimpleLocomotion_Rotate_mC59E45FFA964EDFDED02227CEA9264F03E1818CE,
	SimpleLocomotion_Move_mB371FAA143D1EA4A8025DF464321FDE9509D3D29,
	SimpleLocomotion_GetInputVector_mD6CF1D37650E583967AF16A1576DA1842B41641F,
	SimpleLocomotion_GetInputVectorRaw_m4F84C83E09A942D60D6171F50467AC754FDEEA9C,
	SimpleLocomotion__ctor_mDBBB3F00DAA51BA20BFD7F254F969229F2B71375,
	UserControlAI_Start_mFCDBF39A9C9833CB9D4E1DBCAE8254778AD28A2D,
	UserControlAI_Update_mE0EA9C30597B13F01957B12F799B46E5DA7EDA4E,
	UserControlAI_OnDrawGizmos_m5A2C63DA64038B1391079954DE158DF5117CD004,
	UserControlAI__ctor_m1FC3B0C86442AB41C9DBCBC663882F8A445E94E1,
	UserControlThirdPerson_Start_mEF4C562B944405861D024173B475CFE8B6B146CD,
	UserControlThirdPerson_Update_m454D94BA692425382861A95F897D2BAB8F6F39B2,
	UserControlThirdPerson__ctor_m7F6A7DB1FAFDFC8B469A56D06801BEDD32F90187,
	ApplicationQuit_Update_mD64D857990584D3A62FBA5891CF28FFC917577E2,
	ApplicationQuit__ctor_m7E9D64C6FA0CBB664E9E070013092952246A60D0,
	SlowMo_Update_m3FFFBB69B9E074C605BF466FCF3D84E57DA27FC5,
	SlowMo_IsSlowMotion_m778D493F324C59E86F5B98E3046F14762CF17AFC,
	SlowMo__ctor_mC54701C0A85703FC33D4E32E0BC7CC9C5E408D1E,
	Navigator_get_normalizedDeltaPosition_m82B6AA3A327BECE1FE3A206A69E2CD15AE89F9DB,
	Navigator_set_normalizedDeltaPosition_m08A4772D516114921B9FE39FDAD733B89357D4E3,
	Navigator_get_state_m025F9DD0DEC0FB822D318D93208351CD1638ACAB,
	Navigator_set_state_mA99231CF329132577E9A7CE08A9980C2D28663FA,
	Navigator_Initiate_m1CFA7FCE28EDA78E25782F7518F8B6028CFC6802,
	Navigator_Update_m20B945E6A358C881A682395CADE17306F81551E6,
	Navigator_CalculatePath_mCACAA6F8F5CA04CC2F6C8BA302328B56AADFA99D,
	Navigator_Find_m27DAF60AF770BB5EB714C38D9C2B78291EEAB9E5,
	Navigator_Stop_mF2094527EA4A5BAD12B76846D170B7058BE1823F,
	Navigator_HorDistance_mE3E27E418775F9CC297C65B510F0CD1E5240D9E9,
	Navigator_Visualize_m6CD93094EEFA8699A54B79DD627E727CA563123D,
	Navigator__ctor_mC62BE7392A21DED1957FE19F335C53E5106B8DAF,
	AnimationBlocker_LateUpdate_mFB743DA09B1DAD4BFB588080C45F952381559956,
	AnimationBlocker__ctor_m99ED3538BBAC8FE7FE6102D74B9827A63593BBCE,
	BehaviourAnimatedStagger_OnInitiate_m2679961E3291D0540BFFC4637F193A51711D0B00,
	BehaviourAnimatedStagger_OnActivate_mFF969716AE295DAE059CF26F29EBEEDA239FDB3F,
	BehaviourAnimatedStagger_OnReactivate_mC00880B74F79094AC4CDC0022A2754A679523AEC,
	BehaviourAnimatedStagger_LoseBalance_m89E6846FC9758C5CE36A2ECE638FC004439FEC68,
	BehaviourAnimatedStagger_GetFallParams_m7064733D3BC08C3A3A43CA6FBED44ED56590E9FE,
	BehaviourAnimatedStagger__ctor_m4B4E9E561EAF3B9B5105280848602B7A41BFD359,
	NULL,
	BehaviourBase_Resurrect_m8B17A8F553ABAFF82EF419191D793FC61D341CB3,
	BehaviourBase_Freeze_mA3413A7689517D70CD1F0517EE3CD9A49FC0BE97,
	BehaviourBase_Unfreeze_m69845ECA69ED8AD76637D4EBA7406C2CC8CCABCB,
	BehaviourBase_KillStart_m901A5B5C62E22CAF7A0554F6284A7E4C98B47CAB,
	BehaviourBase_KillEnd_m9EDE39F1B2E65C0CBF48BA948648F29DABF3DA42,
	BehaviourBase_OnTeleport_mA014710B907079074A7AEF31B16FCF6B6191713B,
	BehaviourBase_OnMuscleAdded_m110CBD603773B041F79F5246C4223776D942D12D,
	BehaviourBase_OnMuscleRemoved_mF5D676184D9A9AC58C1563F1656686559133D169,
	BehaviourBase_OnActivate_mEFDD5115CA6B288CF7B4F6B73F5B7CF83A8EF000,
	BehaviourBase_OnDeactivate_mE3426851AC88951605D4977813DCC4CBCFC9E604,
	BehaviourBase_OnInitiate_mF0C4FB109099D2BB7D68DFB367C1A719B6091404,
	BehaviourBase_OnFixedUpdate_mFD95ECB4218F708D5880DDA9C0577F7B5805AF90,
	BehaviourBase_OnUpdate_mC9618416B88BD41DA4348BF0A215C1EB17B6FBEF,
	BehaviourBase_OnLateUpdate_mB5EAAD16B0A7E65CEB31A74F7C9DECFC4A1A0673,
	BehaviourBase_OnDrawGizmosBehaviour_mB5BC3555B9150954AF7B4641109526D77BF717E4,
	BehaviourBase_OnFixTransformsBehaviour_mD3BCC64E322E076734B899FB5011C82745DD3683,
	BehaviourBase_OnReadBehaviour_m4785EE40457243018DF47D2B45547ED4DF097DE3,
	BehaviourBase_OnWriteBehaviour_mA4459125D08A552C292EF8F077E80457B19D028C,
	BehaviourBase_OnMuscleHitBehaviour_mE432A4FB40BDEFB92B32BEAAF79AB75921F085EE,
	BehaviourBase_OnMuscleCollisionBehaviour_m29A32E68A3A30015ABD2CF7F55FE18DE3839BEB4,
	BehaviourBase_OnMuscleCollisionExitBehaviour_m5CE9A7B19C32AB9111BBCD387609D83E55E24085,
	BehaviourBase_get_forceActive_mE8B65E1782D2DC848990CC4D6C53733906C0A324,
	BehaviourBase_set_forceActive_m45C9374BDBFD0F2076DC143572DB869B1F7E5CE4,
	BehaviourBase_Initiate_mBACE9C3DA23D50324FC1ED50B9FB37A296050E38,
	BehaviourBase_OnFixTransforms_m31F1BADCCDB6BB7E4D64B319A250A03F9BD0D112,
	BehaviourBase_OnRead_m6CA367D866ED61D3D870ECE6FE6C264B44075638,
	BehaviourBase_OnWrite_m0CA0432DE3598F1F166DCB9FB7D17D2056E963BE,
	BehaviourBase_OnMuscleHit_m9D3601EC9DBC3DC74E37DED168851D80C96BDED0,
	BehaviourBase_OnMuscleCollision_mBC9EE4372C0FEC96B254902319EEF1439909C202,
	BehaviourBase_OnMuscleCollisionExit_m0BAF4A23F43DE7BA94B96AB8FAC687DC3368C2B6,
	BehaviourBase_OnEnable_m0D88683CF850729D959B0409ED3D15DD0EEE66AD,
	BehaviourBase_Activate_mC24D2115F5E1E96550A24A5A81B28C06F6135A12,
	BehaviourBase_OnDisable_m80149EBB2115E7759053841B542366173AD5C56C,
	BehaviourBase_FixedUpdateB_mAE0E4F3D8A3478A17E105A112D4578FD6E39D9AA,
	BehaviourBase_UpdateB_mBBE2F697EE3DE4058FB114386C2C7606820DFC14,
	BehaviourBase_LateUpdateB_mD561E7E32DF38742831CB67F372B04ECF9DAA6C0,
	BehaviourBase_OnDrawGizmos_m85712977BA5F6C8EA927A663751786835F09C8D1,
	BehaviourBase_RotateTargetToRootMuscle_mE5DF646A2A86A5AC0C7FDEE5B4905DF364A3B4A9,
	BehaviourBase_TranslateTargetToRootMuscle_mCFDF6D1CC294E2ECE4FBF6C47D314FFA837140B3,
	BehaviourBase_RemoveMusclesOfGroup_mE56FFF2CA411F61947CB05DBCAF3BA76F62EE820,
	BehaviourBase_GroundTarget_mFE565F7994CB0432F60DF1D353FE991CF0D1FE7B,
	BehaviourBase_MusclesContainsGroup_mD9FA41600C6FB75CCFFCBC87531C1BDC104AC142,
	BehaviourBase__ctor_m92696285C1C5F7E42CC3531BF22BBDA15F09CF25,
	BehaviourFall_OpenUserManual_m9BE4AAA6A063603EA189AA0DDC752B36202B0772,
	BehaviourFall_OpenScriptReference_m402E8684F8E0A2C395B249FFED4E490C6E6C5D2E,
	BehaviourFall_OnActivate_m1BF955CE407CAC0C390736CBE52BB139A157AD4D,
	BehaviourFall_OnDeactivate_m1DB414E23F566324398B98A0D3D31511876C928E,
	BehaviourFall_OnReactivate_m7081F3E8C36A068EEF2F049E4CC48B83C6FFDD5C,
	BehaviourFall_SmoothActivate_m6310CAFF9CEF0D1C0EAFDBCCD2A71B39AD79E830,
	BehaviourFall_OnFixedUpdate_mE43B0C8AB8E0298FFC78EF27F5D63171EAA523D4,
	BehaviourFall_OnLateUpdate_mDE326CAAE6BFDFC42ACC06EBB119D23A1EE78E79,
	BehaviourFall_Resurrect_m1600FBB3088AEE36182C5B606191F854A8CB1193,
	BehaviourFall_GetBlendTarget_m13D09D84CDFBB96060B6C7D83E9FC84933C45F64,
	BehaviourFall_GetGroundHeight_mB69D18516F755BCAA7FD70C68A8A2C504CA9D35B,
	BehaviourFall__ctor_m1C795A658F844859E01F8821C8B6886E0E2D8F7C,
	BehaviourPuppet_OpenUserManual_mBDAA7E396698FDC358ED879630F4F9F7FF0A2C92,
	BehaviourPuppet_OpenScriptReference_mFE84E95F20B926E920386242510EF0170D49D1EF,
	BehaviourPuppet_get_state_m4F5BA0A4750977A8D117DAD6A2EF6602FDC0309D,
	BehaviourPuppet_set_state_m33938814ED00B2C319F7269E1A0F6634EAFAFA93,
	BehaviourPuppet_OnReactivate_m757C43EAB4ABF5E5D42763F5DCED860A1DB1B281,
	BehaviourPuppet_Reset_mF6E06A780DFDA3FF2A4665445B1F7C8023C2AB14,
	BehaviourPuppet_OnTeleport_m0D6735B8136699299B540C557B1E41D6AC05E30F,
	BehaviourPuppet_OnInitiate_m11D5CF666B3C3729B3D76F8C155A93DE58174750,
	BehaviourPuppet_OnActivate_mC01198123B104C416A915D410C886D6B54A63555,
	BehaviourPuppet_KillStart_mF56DC95CBF3CB236D913B2709A09E570DCF091BA,
	BehaviourPuppet_KillEnd_m3FC3620413B51D54A83771AED4CAF3B24D92D764,
	BehaviourPuppet_Resurrect_mF69FA83BFE31D7A5E221BFEA242224989BF55EA4,
	BehaviourPuppet_OnDeactivate_m967411B5B0BD5FFF97109209EFDEC5C797DEFB56,
	BehaviourPuppet_OnFixedUpdate_m166A0200AB022B72EEC48FD9D50E50F22C1F98EB,
	BehaviourPuppet_OnLateUpdate_m48376624F186BD0289A2944C599B1CB9ECE0E7A4,
	BehaviourPuppet_SetKinematic_m0A996523E81345FFC91CF0482BA0409CFB9C8FCA,
	BehaviourPuppet_OnReadBehaviour_m34FBC5C1B12599C0E8CFE735F1135197506542B2,
	BehaviourPuppet_BlendMuscleMapping_m066C1F8161F97B318DA85F50C24A2C98B36F587B,
	BehaviourPuppet_OnMuscleAdded_m8962E4B9473A6E66BFE16EBEE89121FB4A1CA0AE,
	BehaviourPuppet_OnMuscleRemoved_mD85B25E7AA74B33F074A030E1EA21BBBBE71EA63,
	BehaviourPuppet_MoveTarget_m2EBE398937F38194CF4DB435B34BF5A3DC7837F7,
	BehaviourPuppet_RotateTarget_mE74CA4F2C28EC3109BA7807AE40D80ECA1EDFE48,
	BehaviourPuppet_GroundTarget_mABE0A5F175A8EBF1D3A2053879ADFF1B69F97AC3,
	BehaviourPuppet_OnDrawGizmosSelected_m6C0FB5191D1FFF60C826F28BB78AA52A29C08A8E,
	BehaviourPuppet_Boost_mCAA091BC176893CC2430E243173FAC0F0B8D225E,
	BehaviourPuppet_Boost_m6CE30219629469ED171B19FB8A23FFC3A5D42759,
	BehaviourPuppet_Boost_mF863B70D1D99B5F9423DA969B22C8EACCCB5C06B,
	BehaviourPuppet_BoostImmunity_m1BEC8DC47233C0A7A6EAAE383F2ACE5A7DBA79BA,
	BehaviourPuppet_BoostImmunity_mB6AFF5612A77E4F3DB4D04232003DE7B313732F8,
	BehaviourPuppet_BoostImmunity_m4E177D8BDBD3A37CFC9BD805A5FF88E01BE4D229,
	BehaviourPuppet_BoostImpulseMlp_m0A833386915C6DDB94BAECBB02AEE8FF57695822,
	BehaviourPuppet_BoostImpulseMlp_mCECFFFB217250460D958385E8F520B80A7C35168,
	BehaviourPuppet_BoostImpulseMlp_mDA0B48BE7AB1DA599ED33252DACA2BF00F375441,
	BehaviourPuppet_Unpin_m45C52718A593B6A02FE9B51CB82BE458E0EC0AB1,
	BehaviourPuppet_OnMuscleHitBehaviour_mD4542D1ED4243C8404E2838B74F1BE5BA2B22CF1,
	BehaviourPuppet_OnMuscleCollisionBehaviour_m85ACAAE448F36F95A97E919D33819157DD2E23B9,
	BehaviourPuppet_GetImpulse_mE5091FA98AEED45A7CEA82A602C9EA6AAF8C7EA8,
	BehaviourPuppet_UnPin_m2E787B26EE3BBAB5644AA299123AB37E078222F4,
	BehaviourPuppet_UnPinMuscle_m288B373A79CD1711EE2BFF5DB260E640C8226B39,
	BehaviourPuppet_Activate_mBB59B5F2A4A0139EFB25137279484022612B97CA,
	BehaviourPuppet_IsProne_mCAA034B67CB5CC9AADB79BF530513686B1B2FFBB,
	BehaviourPuppet_GetFalloff_m9ADCBCDFB7BF4323B3F7E4B97CFCB36929C4D157,
	BehaviourPuppet_GetFalloff_m35E2585C17025A6046A553043BA0A59D57B831F3,
	BehaviourPuppet_InGroup_m743DA5D515FFC7D119AF7AF4D45F1BFF6C7F1A0D,
	BehaviourPuppet_GetProps_m320F43FF0BDB135EBF6891A5969438329843C335,
	BehaviourPuppet_SetState_mEBF4AE0E220F8268FA58E48913E919B2E7663381,
	BehaviourPuppet_SetColliders_m9E10BDF9D52EDE1ECC0F821C18A93256C2355147,
	BehaviourPuppet_SetColliders_mF897D58B7C979E04D2213CD505DD4D7A94DABC23,
	BehaviourPuppet__ctor_m0DF10D416AA5E6D5B43EC7D7BBBEBD11854251EB,
	BehaviourTemplate_OnInitiate_m1D69D7821DB88CE7F75340BC761BA2858CA4E3C5,
	BehaviourTemplate_OnActivate_mA7354DDC0DD69B497E45B549F808E0D9A73B2DA8,
	BehaviourTemplate_OnReactivate_m129EDEF1FC09EBA6C6AF9369EA32527F021D8B72,
	BehaviourTemplate_OnDeactivate_m630F349217CD6AED874760CDB88E7F7C0151D7A5,
	BehaviourTemplate_OnFixedUpdate_m2EDCAB1BE736047EC7E0324D8F6A3508A4A7B8DD,
	BehaviourTemplate_OnLateUpdate_mD123FD8A1B83723A5D0C9DEF9305F1D1B3DB1912,
	BehaviourTemplate_OnMuscleHitBehaviour_m9CB3A96DB0931CE4447BEC5870C5F533371C7DBC,
	BehaviourTemplate_OnMuscleCollisionBehaviour_mF2FC6655AD03F182CB211FEEC0880B5E8EC626C3,
	BehaviourTemplate__ctor_m7AC26544F128C4203A51035076E25BA1B1C4BD35,
	SubBehaviourBalancer_get_joint_m802538E215DAB5CF8A46B99436F2E38D260E88E9,
	SubBehaviourBalancer_set_joint_mD28CEBF4ED702C2ED6A7C64E627FC93EB90D65FF,
	SubBehaviourBalancer_get_dir_m58BD74AC51AC73FEA11CB8F396383A2DA0816305,
	SubBehaviourBalancer_set_dir_mEBB2EBC252372EDA6B62B63D465B4ED040AF12A4,
	SubBehaviourBalancer_get_dirVel_m1025E52F7302CA60B47256B77C12B57516E78D14,
	SubBehaviourBalancer_set_dirVel_mF671B644901CA7CEF4EFD9BDB1A96FFA064F57DA,
	SubBehaviourBalancer_get_cop_m6CA43E1CA05587F95AADCDCE8527519134A7D5AC,
	SubBehaviourBalancer_set_cop_m501443224AF24107977A8D53ED8B96ED7C500763,
	SubBehaviourBalancer_get_com_mC4E9F129B76FEED980C2CEEBF301103CF5121E35,
	SubBehaviourBalancer_set_com_m55E0FB15463AB964007F1B478CC6BE26E7F11B9E,
	SubBehaviourBalancer_get_comV_mA8C82A03457AB62D8F4A1B81EC82A449E14AB219,
	SubBehaviourBalancer_set_comV_m98D9D067B3EFE08944BF78777292740E7A809DBD,
	SubBehaviourBalancer_Initiate_m40DBD98B8B9EFAAC14F02F04CEABEB3D0EA6CC5A,
	SubBehaviourBalancer_Solve_mAAE1CD242292A6A08F923AD0BC830472E4A64562,
	SubBehaviourBalancer__ctor_mC87BF6C32ED1E599824AEA5CEC8FEED693C9339D,
	SubBehaviourBase_XZ_m85DE1D630F4730144F1EEF6EC85792BB1B4BE796,
	SubBehaviourBase_XYZ_mF275B352B17BC1AB7DD4BEF0A9F4A41B6DDBB9FB,
	SubBehaviourBase_Flatten_m474BC1B7544C9BC3A0EC62FCA1B6BD4FCB7B57C7,
	SubBehaviourBase_SetY_m878D72951ECAEF1D923D22214CCDD9D791883325,
	SubBehaviourBase__ctor_mFD851118C243500C15632EBA3996FD47F7010FE3,
	SubBehaviourCOM_get_position_mA49CAD402C44CFF468CF2A3A1E826B4EDE3BFE66,
	SubBehaviourCOM_set_position_m303206079646F9EA93A7A6B0CB6730689CE2BE01,
	SubBehaviourCOM_get_direction_mBD73EB9520993518E43FC1568E6F4EE34889D464,
	SubBehaviourCOM_set_direction_mF68730B5E4E6FEFE919B1D592E63497DAD87C324,
	SubBehaviourCOM_get_angle_m39E54DA0C94294808B78D729F8581D2802D75204,
	SubBehaviourCOM_set_angle_m66A02CD933CA90FAAEC6E02158C665E8135FB342,
	SubBehaviourCOM_get_velocity_m11F32045576B48324328D5403E86770AC2D626E1,
	SubBehaviourCOM_set_velocity_mFB56FEACFA55ACF6101F8D127C5ADA35BABEB3C0,
	SubBehaviourCOM_get_centerOfPressure_m9CE233ECE9E7CA1D1A8E5C50ED821EE066295FE5,
	SubBehaviourCOM_set_centerOfPressure_m9C3665EDFABA1DEDFAB4DD1F4EE545D1B50D5891,
	SubBehaviourCOM_get_rotation_mD3E6E61B86DFC9873F9010C606FFC518D6CF9D34,
	SubBehaviourCOM_set_rotation_mBCD8EBF866FFD0EE481DC28A54A3FB246D1E37C4,
	SubBehaviourCOM_get_inverseRotation_mAEF2E6DF0569050C9641017114473D416F3CBA8E,
	SubBehaviourCOM_set_inverseRotation_m53CE2D3E8284A098F08CF71FAC64B542427428DE,
	SubBehaviourCOM_get_isGrounded_m2437729FE6FCC9668F4E3D38D29AD3C45F33B58C,
	SubBehaviourCOM_set_isGrounded_m62E50301498886C8E49488657EE83EDB62A944C2,
	SubBehaviourCOM_get_lastGroundedTime_m751FFE463E53CB6D5D6769E543DCC305BF7FDA70,
	SubBehaviourCOM_set_lastGroundedTime_m35B0AB876B37D817D97C1E6ED14CC6678AB8FC3F,
	SubBehaviourCOM_Initiate_m1B74EACD1CB7AEB08E6EA3D865F2BCE7A355E792,
	SubBehaviourCOM_OnHierarchyChanged_mDE1E59D1D594FFA258778F347EF82FA2E6E436B1,
	SubBehaviourCOM_OnPreMuscleCollision_m5D88478AF9C34016219AD5FCDF4AA6A736AC4544,
	SubBehaviourCOM_OnPreMuscleCollisionExit_m413A58810513B27C1AE9C47731CEC2A0C3EB5504,
	SubBehaviourCOM_OnPreActivate_mCB622726936614D6EFD9968B3ECC40134E192E48,
	SubBehaviourCOM_OnPreLateUpdate_mE1BB34F16274A3046411BE5AC3DF3BA1416CF8CF,
	SubBehaviourCOM_OnPreDeactivate_mB24950495359768733323202E0FF8FD704127CF3,
	SubBehaviourCOM_GetCollisionCOP_mBC279D49965450470003FFD837843BEBD1741D56,
	SubBehaviourCOM_IsGrounded_mC24230F6FDD790F97F1EEC20E2D59219929760D6,
	SubBehaviourCOM_GetCenterOfMass_m842AB085A1CF0E4527DA9D1C47189B1521B0825E,
	SubBehaviourCOM_GetCenterOfMassVelocity_mC96E0CA88CBDBE90B581FE8EE7F9808DFB5CC6F7,
	SubBehaviourCOM_GetMomentum_m5453FD7337814D8974F08EE79185FB0DC1A6889C,
	SubBehaviourCOM_GetCenterOfPressure_m86E1D8EA5CC5D398EE91CCB3869CBD70F9C5AE37,
	SubBehaviourCOM_GetFeetCentroid_m1605B398B99CFEE09E36F82060DEE94B6628527B,
	SubBehaviourCOM__ctor_m02048379561EF8EEAA0D56AF2CC1D4C992BFB214,
	Booster_Boost_m7398D6C1616A3817EC1072BA4D2C40CB8A70D044,
	Booster__ctor_mF09152A4D153897027A2B5FDDF1AFAB05872931C,
	JointBreakBroadcaster_OnJointBreak_mEE0F29F1608732D2BA61F21B0771860BD8099A11,
	JointBreakBroadcaster__ctor_m21DE2DA542311F591F35CDFF0B86C3FB88BA4454,
	Muscle_get_transform_m0B8E9578F734C48EA796719CAA9D7FD29FA256DF,
	Muscle_set_transform_m98649520C3E3FF3DE348842C34EB83773EFD909D,
	Muscle_get_rigidbody_m51E9F00CF3CEBF26C226458E09846B6D01A46D9C,
	Muscle_set_rigidbody_m37AC05BFD444E7C68A0D0891D8522817EA4B0669,
	Muscle_get_connectedBodyTarget_m99C2266EF1D0A966367528816D2224D136D483CF,
	Muscle_set_connectedBodyTarget_m301820C4A66D3C1E1EFB8F0D7B421FAFB370CDD9,
	Muscle_get_targetAnimatedPosition_m040A92DB83F1C8B8CF4983AB960AAF0771EE4461,
	Muscle_set_targetAnimatedPosition_mCE4A4CA1B1FAB59CE22D68B95F462F736831D316,
	Muscle_get_colliders_mB88A027B42979C40E2CEEB01C5090FEB1D55E275,
	Muscle_get_targetVelocity_mA5AAF13D5DECEB29CF86FD1BF98562C9CFF79FB3,
	Muscle_set_targetVelocity_m2741F9ECA14FD53F168A0C6820254AF0DECE6689,
	Muscle_get_targetAngularVelocity_m4D25311475C4DFEE1FF0775CBAF7FE59FE271726,
	Muscle_set_targetAngularVelocity_mF14D93671870E70E4893964A2F28D8082BBBC78A,
	Muscle_get_targetRotationRelative_m92506E5EE779F825497B389FDA2B8580167783BE,
	Muscle_set_targetRotationRelative_m345D5921645E4B6C2C75E910A9F7161E29385E22,
	Muscle_IsValid_m0AD77740C982B5551DFE5D486DC1BC57C3D57E2D,
	Muscle_get_rebuildConnectedBody_m928A993CF6C2A0898AA0C50DD4BC9B86857EC0BC,
	Muscle_set_rebuildConnectedBody_mFD8FE17D466BAB516826249372FAD769F3F6C43D,
	Muscle_get_rebuildTargetParent_m61D8BDA49EF7B40C8578F417BD4F9075679FF563,
	Muscle_set_rebuildTargetParent_mDB5D748006B5E1EF3551E898FFD7C3CC69E79078,
	Muscle_Rebuild_m4E2526AAA8F77A77BC669484BB7A1E71F765A0BD,
	Muscle_Initiate_mC3F1F4E91FC2B81CD3A02782017BF216A6990467,
	Muscle_UpdateColliders_m576656664B9E2B8E2902E6B3BA1BC6CB56E8410D,
	Muscle_DisableColliders_m614DA544A2D63430A166CCDF32057DE46BE83DB0,
	Muscle_EnableColliders_m777E63C7599493C6FFE6DD7AC08F5D95D9085FC1,
	Muscle_AddColliders_mD23E2A6F485268904946A8A12D9D4759ED4E3C85,
	Muscle_AddCompoundColliders_mC55C4777B43B14E340A06B728CE008049AED6C29,
	Muscle_IgnoreCollisions_m32C36405F51BE18F683887ADCD010405FEF316E0,
	Muscle_IgnoreAngularLimits_m5BD77F7543414D743871B6E8B53A0BCA15172D9A,
	Muscle_FixTargetTransforms_mE21331F360F13C2CFDE1C4427188150F87176626,
	Muscle_Reset_m5C0F2A1820C88E4A4AF0E733D1A8951D2094C8EE,
	Muscle_MoveToTarget_m8EB6AE016AB3B90425247A3C0216C4DEBAAD5FE9,
	Muscle_Read_m574324194596A24DF8DBBCF002243897E7344980,
	Muscle_ClearVelocities_m94EEE3D62C1D26CCA49B3CBC8899A25205BB3997,
	Muscle_UpdateAnchor_m6D0E5AD9DD72B972306D222969BA2657592BB4C1,
	Muscle_Update_m1420719D753F91EC61036B4B3DB8E7EF7751F1F2,
	Muscle_Map_mCF47D85786318CE3F3A6909845A9EBED6F2ADBA8,
	Muscle_CalculateMappedVelocity_m53AFB03EAE23FE1EA3E12B7B7F2E6789A0BE833F,
	Muscle_Pin_m7183D534A394102D88D2F665B82D81410601D4DD,
	Muscle_MuscleRotation_m279399D0EA90BF8354271A78E67A44B29101C3A9,
	Muscle_get_localRotation_mD7DFEE6491C2F6B942C7281FDC4398B014483A08,
	Muscle_get_parentRotation_m2052C7E148F78346166494A463D965FA7D1BB8C8,
	Muscle_get_targetParentRotation_mE4EC0DC3FC87A1DA6A89D2EE4C50F79891B578F6,
	Muscle_get_targetLocalRotation_mA13C51DAA502023B9BECF67ACD76C25BC4B44D13,
	Muscle_LocalToJointSpace_mEC0AC8303FA842A31909475F9A0A595A591C9510,
	Muscle_InverseTransformPointUnscaled_mCCB03A739F317D74AFBD6CB81CCFF7972BADFE10,
	Muscle_CalculateInertiaTensorCuboid_mE7FA9FD8D31DD5A4C5525DD54021C0C65105C6AB,
	Muscle__ctor_m01E12C68FEDE53B9AA2F043E164D98B94C4C4A6B,
	MuscleCollision__ctor_m1AEC27F36070583B949FCB0C874FEC9801FDB51F_AdjustorThunk,
	MuscleHit__ctor_m8B50A0D9B4A1C2BBB6DEC0C4D5BEA62CBBDCD3D4_AdjustorThunk,
	MuscleCollisionBroadcaster_Hit_m6FBE9230BEB4737066FE1C78A6A2ED6A44684192,
	MuscleCollisionBroadcaster_OnCollisionEnter_mFC7F6E0C12F9C2F860AEDEB9223BFB816F6D256F,
	MuscleCollisionBroadcaster_OnCollisionStay_m3623ACCA1947889494312B89B7C74AA2F94EE60A,
	MuscleCollisionBroadcaster_OnCollisionExit_m2007950C053A14D7F1E06A861DD1C0691FD419AC,
	MuscleCollisionBroadcaster__ctor_m865A8270B63D132D684A9F837ED0EA3DC010A27D,
	PhysXTools_Predict_mA46BB1ABBBBDA540F985D56E63A80706BB4807EB,
	PhysXTools_Predict_m1D256121A1F9D48E5C9B3B8C69570572D71E7118,
	PhysXTools_Predict_m3B415490C96EAEB6E6B821797ADD2E9CA8D35024,
	PhysXTools_GetCenterOfMass_m028CE20624AFFD1A58F39BC7BD836781977C85D4,
	PhysXTools_GetCenterOfMass_m43E9540851C37C8059B0BA72B0F30AEB66EEEDD5,
	PhysXTools_GetCenterOfMassVelocity_mD02A65C58851EDCE99C3826307184E61C1D61F3B,
	PhysXTools_DivByInertia_mFDB14AF0E9BBD006D7B8ABC20FEC8EA053E333E5,
	PhysXTools_ScaleByInertia_m5424D460BFA3BC2EF9C5E51B31123F5CEE572F31,
	PhysXTools_GetFromToAcceleration_m3773C1D065F6BD04D287500B94F3DC8D59E694B1,
	PhysXTools_GetAngularAcceleration_m3510C50B9778C06000A1CC059EC3B6708B6E0960,
	PhysXTools_AddFromToTorque_m8F7D2EA5531EBD81482F41578DC685E15FF1F0B5,
	PhysXTools_AddFromToTorque_m1029F51AF235C06737B7F9422D9FECEE9D7EA5DB,
	PhysXTools_AddFromToForce_m610EE2C9EFB650C6DD81C06B59E417001E4E659B,
	PhysXTools_GetLinearAcceleration_m863CA49E0C00D4A8EBDBE15C785F8CCE97A7C534,
	PhysXTools_ToJointSpace_mB28D3A889A49F2079ABF23922108D31E450670F3,
	PhysXTools_CalculateInertiaTensorCuboid_m55F1C9F036A7908397CA51C3E3A0550D55190ACE,
	PhysXTools_Div_m281C283FC52E36984A6769CF6F3A36EAAF781EA4,
	PressureSensor_get_center_m080FB7E4B70D8493FC5BF277CBE9724F6A589DE7,
	PressureSensor_set_center_m8B2269B72CB68817060B89535F8B859F823361EC,
	PressureSensor_get_inContact_mB82077BD03DBED2F456E7A4D8DC2CD373136DF22,
	PressureSensor_set_inContact_mB9BCABDA40101E7F542DE52EC0E75B2439C07398,
	PressureSensor_get_bottom_m14FA0762E9C65A3D0F5CAB49422E95ECF9F5876F,
	PressureSensor_set_bottom_m70EECD64D6ED13C93EDEED75A18308BFB8B5149C,
	PressureSensor_get_r_m2AAF6E1F7B755D87E9FAEDA940E3D384098299AF,
	PressureSensor_set_r_m904BAC2981E06FA01DA0EA878A7B173A05858FE7,
	PressureSensor_Awake_m55EEB328A358C6233163B328329B11E497217A32,
	PressureSensor_OnCollisionEnter_m2B120DD85BF52E30C379D44AAC6FE29828DCB5B6,
	PressureSensor_OnCollisionStay_m6B9DCCF4C7F5429DBF09EB5664C80C48C0A408BA,
	PressureSensor_OnCollisionExit_m0E8CCE90A8DB5B430255C914A78B40815C64CBF0,
	PressureSensor_FixedUpdate_m2B0AD171C462D9E398C57041BF5BDB865CAFAD6B,
	PressureSensor_LateUpdate_m141AF1E6434DFCC3C174E065F4FE81683F9094B7,
	PressureSensor_ProcessCollision_mC238E22D0CFF37F713ED52F4A2DC38148A098D7C,
	PressureSensor_OnDrawGizmos_m4E310773FBB782623BCEA85426934D4235C8DCE5,
	PressureSensor__ctor_mDA5D0D57F29E9E3759888D84A849BF381D001396,
	Prop_get_isPickedUp_m4819577624EEE994B0FBB6DBD62F25A7E2B9BF1E,
	Prop_get_propRoot_m20B85350A49AFE0BB3B1D847EE7E2EEF4BC838D4,
	Prop_set_propRoot_m225C639CAACE7BEAF9278F96B58429DDC6385375,
	Prop_PickUp_m00D7A6175CD046432433B83233C4A8924A5A3C8A,
	Prop_Drop_mA69DD8680B29F5846316C86BAC4FCF153AC7D7B3,
	Prop_StartPickedUp_m95963E75DD8CDF55B1E67CAEF1F281B00790AFEE,
	Prop_OnPickUp_m928179EAC8B00DA4905DAAE621E61B17688C3E22,
	Prop_OnDrop_mAA8BE2564A25660178C336BC66570E0984454674,
	Prop_OnStart_m9F364D3619D60C53BC6C809ED7EAA8CC2E09A5B6,
	Prop_Start_mE4D76AA644A9C8A11797B0E4351402785B1B519D,
	Prop_ReleaseJoint_m8BD78B6D365F8F07D98054563F7E8A145963B713,
	Prop_OnDrawGizmos_mDB5EEDBBABF46A85C360019A87487267504DFA57,
	Prop__ctor_m48127381A18340489B7E798F62EC809EC6B7ECAF,
	PropRoot_OpenUserManual_m6A9467480DD5FCE20905A19467E3EF28B34DD6C4,
	PropRoot_OpenScriptReference_mAE170E27379ADB0A7F240B81EC5ED31BB9F9DCD9,
	PropRoot_DropImmediate_m36E7D7A141968CB637ECDAA5F1EEF03A2F115201,
	PropRoot_Awake_mBEF25FD3033A73E9FB4FE5BF328EE0E1BC633EAE,
	PropRoot_Update_m19411501D06C07F31234194D3AD07AFDD53A4F17,
	PropRoot_FixedUpdate_mA45D73D4F5F65B5ECAFA46C00F9480EF88F0889A,
	PropRoot_AttachProp_mEFA22183C4136D6160A2FA172B943BB530A6D627,
	PropRoot__ctor_m3150D88A9CFC2892C363000E70869DA12B817F72,
	PropTemplate_OnStart_m5796CE422656A394261EE7A8D6D923459B4E0434,
	PropTemplate_OnPickUp_m7100AB7C3B7FE16C2F632D71EF4ED75A6B2B6625,
	PropTemplate_OnDrop_m8A9987990F286ADBB0226596C4443928DA347572,
	PropTemplate__ctor_mF39BB624DD9AA34E63F54173D22C925F3A0173C3,
	PuppetMaster_OpenUserManualSetup_mAFC2ED82D46BCC033A9D6A0F06DB68DA530E7877,
	PuppetMaster_OpenUserManualComponent_mEBA24733FEC617C53C4FF45D879CE455ACAD5413,
	PuppetMaster_OpenUserManualPerformance_mB573DEFDBED75034A92BD2D577BB41804F0F09DF,
	PuppetMaster_OpenScriptReference_m71CA7EE6E8A8479A2D672834027459C582ABF0B0,
	PuppetMaster_OpenSetupTutorial_m1965E01622012451D8D12A26E91BD6DB8B7B25C5,
	PuppetMaster_OpenComponentTutorial_mB12B99F1D4370D7AF52C44A00B531FDDE38A344D,
	PuppetMaster_ResetStateSettings_m3814E898E9D9B0501BA4AFDD006FF8A3A09603A2,
	PuppetMaster_get_targetAnimator_m47D08CF8F8093BCEDBFFA95CA6731F9B4D95DEB7,
	PuppetMaster_set_targetAnimator_m166E631CA90B20D74E3EC5FDDBFD84C0B51EDF8C,
	PuppetMaster_get_targetAnimation_mA20302A0BB433A191C29C7926ECA071E106ED375,
	PuppetMaster_set_targetAnimation_mD65DBED2C191E67D45BD7D891006815D963C56DB,
	PuppetMaster_get_behaviours_mC62978107F22A6D3F24957DEA6ACF5BE4D423D60,
	PuppetMaster_set_behaviours_m5D0A809F4A185767A4EBD6453D11A75D17FAC582,
	PuppetMaster_get_isActive_m99EBDCA8A7DE96F15DAF3E015A03DAD554E74AEE,
	PuppetMaster_get_initiated_m07985C10CCABC30C2AF7A2A8D6C3DCE08414477B,
	PuppetMaster_set_initiated_m03C1B6A2ABD7359C47E1FA78577ADACBE3AEDBF2,
	PuppetMaster_get_updateMode_m351C12D72D0017E39E58553ACA7DE7C85EF013BA,
	PuppetMaster_get_controlsAnimator_mE54E885EA22AAA8F4A68047B8EA51B3446B7D54A,
	PuppetMaster_get_isBlending_m149920882FE9273183F1503BF3D4E8133A8C748B,
	PuppetMaster_Teleport_mE3EB83CF258447A585079363A48B00021EF2EFF1,
	PuppetMaster_get_autoSimulate_m1E4F089E6A49E2846BA3058AF2FC5F004DE60BC0,
	PuppetMaster_OnDisable_m349AD4DD5C76797848D9510A005D9CAC255A9AE5,
	PuppetMaster_OnEnable_m64F4C09C0C21E69F58573881CD6C11445B88EAAA,
	PuppetMaster_Awake_m9A8AF07A1346C86DF8CE2BAEFAC950824842C320,
	PuppetMaster_Start_mF6DC23D68E3674A2C8A62C753B6161EAC66AB23B,
	PuppetMaster_FindTargetRootRecursive_mC3274F4A69762EF531E1A6C4B200C016D307F891,
	PuppetMaster_Initiate_m6475BDFA637BE4A8AC0D57B2C39A2964AB5A5C13,
	PuppetMaster_ActivateBehaviour_m67EEC414193CC57144DE5AB7B64EA81EC68FBE4F,
	PuppetMaster_OnDestroy_m05D2F5B5E0B0A6A60B1A15B360ED4ECAD32BB387,
	PuppetMaster_IsInterpolated_mDF44D52E0FD70B2613EBF57EAE3507568F37F111,
	PuppetMaster_OnRebuild_m736733633EEA4E16268A0EDD543CAA2B5C5694EE,
	PuppetMaster_OnPreSimulate_m95A31C31039CE4CF30E162F0008BD8A4F94C0FDF,
	PuppetMaster_OnPostSimulate_m968432DBA76A780DEC450B70F35AA60BEDB3CA8E,
	PuppetMaster_FixedUpdate_mE311114F12BE7273394DF8313D7E9B273F3C1EC5,
	PuppetMaster_Update_m09D8071139E870BB3218AD70403BC1B95B5D1DDC,
	PuppetMaster_LateUpdate_m42A74FB8F0CD073A39575E2581F06EDB458FF3A0,
	PuppetMaster_OnLateUpdate_mE08ECBCF9927FADFD2E1EE658AC6359746EC046C,
	PuppetMaster_MoveToTarget_mA90222253CD1422D337E0099B2F443921C159460,
	PuppetMaster_Read_m7FCBB03BF74AAD3BB9B17D30D601AAADD4F25AE1,
	PuppetMaster_FixTargetTransforms_mD0F746D17BD670B6452B07AC5ADBF8394A31DDDE,
	PuppetMaster_get_targetUpdateMode_mE8C266331A70930141E02EFC49A16415B09CED9E,
	PuppetMaster_VisualizeTargetPose_m54D593597ED18FB02AD7648E3AA40C473A313E04,
	PuppetMaster_VisualizeHierarchy_m1DBE413B3ABA04F60D9E0EC6FDE6C417DE0B307A,
	PuppetMaster_SetInternalCollisions_m773356588C67E7A37D93D9619DE84C269DA9E3CD,
	PuppetMaster_SetAngularLimits_m56CC6EBAF183C9BC6BBFE5536A3B08321874C641,
	PuppetMaster_AddMuscle_m0C9882D2D6C160E498E0A550CA459F446A4D54C6,
	PuppetMaster_Rebuild_mE8298A18B6C15301908C938243703BBE4964EDD2,
	PuppetMaster_RemoveMuscleRecursive_m7CCC5F4C3BFE9BCA643AB885FBEDC80B47FC87C2,
	PuppetMaster_ReplaceMuscle_m41181F510DCD04C26DA68A0FE07214E9AA5FD89E,
	PuppetMaster_SetMuscles_m905767BA7DAB3E463ADBC75F9B0BC5861F76AFCF,
	PuppetMaster_DisableMuscleRecursive_mB1BB1C6135A02CAFC10A26852ABD9E532552FF7F,
	PuppetMaster_EnableMuscleRecursive_m7C95236108B65DC8EC3E3D9D5127A5098D4EF647,
	PuppetMaster_FlattenHierarchy_m693547E577D7D5F1CF784080E517CC17F2CF0FF8,
	PuppetMaster_TreeHierarchy_m41CC7B3E1339CB569B816508AF5D60506730166D,
	PuppetMaster_FixMusclePositions_m8EADB7598F3DAD7637E36E14BE34321EE5E443A3,
	PuppetMaster_AddIndexesRecursive_m4A60221A40A1EC28489D34E4A3A805377D009870,
	PuppetMaster_HierarchyIsFlat_mC14BC5DDCA5E0E35EC71E20BCF02DD62D0850CF7,
	PuppetMaster_DisconnectJoint_m56C5141E12203492F61FE557B82BA4427149C09A,
	PuppetMaster_KillJoint_m1C351C543D15D35D6E9E0E5B330E1D52ECAE44D8,
	PuppetMaster_get_isSwitchingMode_m3F68E43846F90378612B9C696F080F2FCB309BA1,
	PuppetMaster_set_isSwitchingMode_m2637BAFA0D3038B6572D30DF4D4BA50EFFE7CA64,
	PuppetMaster_DisableImmediately_m20D47A9513823AA5B40815F74A0C0DA9C0B6DDD3,
	PuppetMaster_SwitchModes_m2A963A7AA8FE1F6E75915FBF67275C43695E4A13,
	PuppetMaster_DisabledToKinematic_mBD0591016576717D4B50715EC5FEE6F2500B6C15,
	PuppetMaster_DisabledToActive_m5A4A166042CECF16F23BCBF061E9F1D135F91C4F,
	PuppetMaster_KinematicToDisabled_mD50BD5CF9007E44FB4C16653019E81804DB07BFC,
	PuppetMaster_KinematicToActive_mB82B6B2D9E03DA18DDAB567B350B9AE41783F2D7,
	PuppetMaster_ActiveToDisabled_m6B671406D8EE241766CF091F069C3FDB8612EDDE,
	PuppetMaster_ActiveToKinematic_m43DABEC5AD5BE4D8BCD1E590BF3E3B4B156F96E7,
	PuppetMaster_UpdateInternalCollisions_m0E6A20850933656CBB421C5AA3F9192014B56527,
	PuppetMaster_SetMuscleWeights_m158D686008EA7AF65DF49B309ED1237FF25A0BC8,
	PuppetMaster_SetMuscleWeights_m75432E3EAFC03A2FCA8113360AFE07535DE43703,
	PuppetMaster_SetMuscleWeights_m58341B71F5FD117693364FFD97E55183A7C26A65,
	PuppetMaster_SetMuscleWeightsRecursive_mD1EE13FA607E077949E181C7AE12ED50557CC6DF,
	PuppetMaster_SetMuscleWeightsRecursive_mF1C4CD0DC39D236951CA9A46C03AB664AB2B06B7,
	PuppetMaster_SetMuscleWeightsRecursive_mED9BFEDA57BF6D54964A9C597186CDF275BCD3E7,
	PuppetMaster_SetMuscleWeights_m324E7CBAE17F6F873194148FFB6B8A177C6DE095,
	PuppetMaster_GetMuscle_m63F0FCDC3530F3F88B085082BFFAE2609A9D1D3E,
	PuppetMaster_GetMuscle_m4EC83F8E157576E349FDF4ACF549A94AD7B519CE,
	PuppetMaster_GetMuscle_mA78D4064A80F7161059B861DF14B4EE635F91E98,
	PuppetMaster_ContainsJoint_m52D20B4BBC2E3D15D8AE59194EBA166A4FCCC346,
	PuppetMaster_GetMuscleIndex_mC54A0EDFC0DA58F7DA6D25D5B982781510820AA2,
	PuppetMaster_GetMuscleIndex_mD0E63ACED0A542CF864F783DE3CCF0C3341444E3,
	PuppetMaster_GetMuscleIndex_m36AF5FAA0A965CEB97F118D0287DB916F011AE71,
	PuppetMaster_GetMuscleIndex_m16BDCE9F90DB2B6FD5A14927846CAEA09529D9A9,
	PuppetMaster_SetUp_mB590C35C218DCBB8657ABAD4AD3E4D7E3E1B2FBC,
	PuppetMaster_SetUp_m7486C849609ACBC820E9C8CCCBB19E37EEE7C16B,
	PuppetMaster_SetUpTo_mC7F0A7FA68474FDE6D53337F04C2D566FD98202C,
	PuppetMaster_RemoveRagdollComponents_m115EAC1FB4726A8BFC5DD50F794284E46DA71A3D,
	PuppetMaster_SetUpMuscles_m3F38B1E9F93E2CEF1E8F7CF3A11E8BA69049DB1C,
	PuppetMaster_FindGroup_m46DA2B6EC75E1DE32A1878C953DEF21036231AB2,
	PuppetMaster_RemoveUnnecessaryBones_mFF21FA3E5CA7E563864C135483C95444A0746D7A,
	PuppetMaster_IsClothCollider_m26BA387C486B6A99479D2E06D095C7FBC5482BAA,
	PuppetMaster_get_isSwitchingState_m4359CB336B8B6A4F357D0862306AFB1FC2401DE1,
	PuppetMaster_get_isKilling_m78F18D0FDC74A10182CE3659D2E77105BA7FD1BC,
	PuppetMaster_set_isKilling_mF34F21233C3A095C6149BEB0CAFE9836CA94BB9D,
	PuppetMaster_get_isAlive_m877FDB53D2C8B1970D8BFBD52AC4F4A2578469B2,
	PuppetMaster_get_isFrozen_mEFAA674FDFE0807E0EA65662CF9B6DAD963CA57C,
	PuppetMaster_Kill_mE0C035CF7FFAB0CEBE99E9F0D34869E91A7B11F1,
	PuppetMaster_Kill_mA907CACBBE3FF6566C35E8EE7DCAE7045671CC1D,
	PuppetMaster_Freeze_mAE2EFC168AB166A70A5D64C6B4B3086CB2F5B409,
	PuppetMaster_Freeze_m18FDD5ADE8F18B5949E6C612AD087C05B636F458,
	PuppetMaster_Resurrect_m8BD0CC8902DE7E512C5A87BA326A5341790C416C,
	PuppetMaster_SwitchStates_m9D5CF5DDED8C08053A5772AB68B4E789F1A49106,
	PuppetMaster_AliveToDead_m4E9BCC7ECE53183FFEF80879390EA5C6F602A9FD,
	PuppetMaster_OnFreezeFlag_m37D2FD7F963B657F437EC90E6C88B7274B68601C,
	PuppetMaster_DeadToAlive_m58E2983D0D9B80F305DE4F05F703724ABE4F6A96,
	PuppetMaster_SetAnimationEnabled_mDAC15D6F7F23B07BA3D0FB9B8613D96C86AA531C,
	PuppetMaster_DeadToFrozen_m74A6F81A7333E226564DE7821D7C3771CA3AFC48,
	PuppetMaster_FrozenToAlive_m887320A29D27082554C298452426C219D21AB184,
	PuppetMaster_FrozenToDead_mF356614A2AF1153388EE49E58734E1DE085DCAD7,
	PuppetMaster_ActivateRagdoll_mAB3CCBD89AD18F0D502B937AE35865D01BBD8E8E,
	PuppetMaster_CanFreeze_mFBA33031E1D55680448A2ABCB47D71D4E4271D3F,
	PuppetMaster_SampleTargetMappedState_m1F69C877953D6412166D964F7AF1D5977CFC7999,
	PuppetMaster_FixTargetToSampledState_m1A647A236AC33B41C2FBC96F08F6F70032D04642,
	PuppetMaster_StoreTargetMappedState_mCCBAF89358AC2012EC1FE32054A9CCA600F9D44C,
	PuppetMaster_UpdateHierarchies_mE18887F82926576035BD9657C365F5EDD789ED8B,
	PuppetMaster_HasProp_m935396A7BAD5C6525BCB408F3CE63564534B101C,
	PuppetMaster_UpdateBroadcasterMuscleIndexes_mB92ED893FA5E25F7B19F33555FC662A6E6C1414A,
	PuppetMaster_AssignParentAndChildIndexes_m4AE39A6FEACB515A8EE2961CB1A064B08186ABB7,
	PuppetMaster_AddToParentsRecursive_mB45B5D1F65D277F8F4E336CC0A7172350D94593D,
	PuppetMaster_AddToChildrenRecursive_m1D56FD0C685E0AD65F0507CA9EE9B5AD81D2CA55,
	PuppetMaster_AssignKinshipDegrees_mC03A8C73089FCB10A9C4C20E8419E90B1CCEFBE8,
	PuppetMaster_AssignKinshipsDownRecursive_m12E3AF3B4F7198CFE4BCFE8B51E2B13BD2129B5C,
	PuppetMaster_AssignKinshipsUpRecursive_mEC99187176BA964926657ECF0DFD1BACDC5F00EC,
	PuppetMaster_GetMuscleIndexLowLevel_mDAED00EF0679144C8956304C347AD32636C96C1C,
	PuppetMaster_IsValid_m75409641970CFB2AD50F8F6DFF74687040F6E087,
	PuppetMaster_CheckMassVariation_mF75F48D8B2E2E2FDCE35B1B65F7CC1F96C45CEAC,
	PuppetMaster_CheckIfInitiated_mDB84F1B32280168813F4C909B0AADA694C43AFAE,
	PuppetMaster__ctor_mFA777BE864DAE06F6DB39D7936189B8F5AF5A999,
	PuppetMasterHumanoidConfig_ApplyTo_mC403B1125B2C7D270656C722BB02AC34A501A04E,
	PuppetMasterHumanoidConfig_GetMuscle_mA6039ECF0DBC3C055D07D35997C17D1304C6DDF5,
	PuppetMasterHumanoidConfig__ctor_m39CAB91537C5C4C56226ED23FDB5A8221E93CD15,
	PuppetMasterSettings_get_currentlyActivePuppets_m1A1C150CDF003F01BBCD3D86A6598FEAC81E47B0,
	PuppetMasterSettings_set_currentlyActivePuppets_mAF2C6BB345E113C66FC4CEC8FF3D952640EC8655,
	PuppetMasterSettings_get_currentlyKinematicPuppets_mA45B62B27EB3E38D24BB2C1F6CF6E7C25DF07627,
	PuppetMasterSettings_set_currentlyKinematicPuppets_mA80A3939D29642AB6A6F307769714E3819D55EDD,
	PuppetMasterSettings_get_currentlyDisabledPuppets_mF560E9401F157C8989C9ADFC9F4043BE9CCF4CC2,
	PuppetMasterSettings_set_currentlyDisabledPuppets_mD9456C898DFC0C1B2B3767DDBA63399123B2DB0F,
	PuppetMasterSettings_get_puppets_mB74BDDA129CB74D6849B4BB49C76AF7206AF2DB6,
	PuppetMasterSettings_Register_mA679B9A78F060D522EFEFC15551EB631A657E7CC,
	PuppetMasterSettings_Unregister_m777596EE74011409A6D3804486BE4E742D16052C,
	PuppetMasterSettings_UpdateMoveToTarget_m4FA52E51AD69C9FCB6F07A488E636576D057F97F,
	PuppetMasterSettings_UpdateFree_m6FE8B2C4588251FA5893EA1135D2649D8BCDCAA3,
	PuppetMasterSettings_UpdateFixed_m8C82C6B84C23764098547C2D50EF10A9B31BBEA5,
	PuppetMasterSettings_Update_m6A1458818DC132BB4E6EA3E824368F0C872B1CBA,
	PuppetMasterSettings_FixedUpdate_m7A18D97866BC0875B53577EB8D4DA7EAFC02EC52,
	PuppetMasterSettings__ctor_m222849AD88D5DC42AB03345E8E4BF88DF492F89B,
	PuppetMasterTools_PositionRagdoll_m58DF512744AE8D84B740A243A6C1179AED127876,
	PuppetMasterTools_RealignRagdoll_m5CB33153F6B63C7E576D34BC62B3FBBCA1E7D464,
	PuppetMasterTools_DirectionIntToVector3_m64C1320D36F1185869C059463AB644FF9C75126E,
	PuppetMasterTools_DirectionVector3ToInt_m65A17DF9C7FF1B9328A8369C1518583800E8E40B,
	Weight__ctor_m0A63821127A7C4C5B442D426BA989CF43081B8F9,
	Weight__ctor_m2A884C21880AF50ED4F1C14FE4D3A45BDA20240B,
	Weight_GetValue_m1A9BE7282C6E50E56808E29DCEAA29527CE9EE32,
	InitialVelocity_Start_m0E2388398FC823C5FC1D6954DF11E1E5FBD4C554,
	InitialVelocity__ctor_m9BFBB2E435C8E6B7668A010FD0EAD945DD343AA8,
	BipedRagdollCreator_OpenUserManual_m101A31B6FA10416B9A3496357D9D686961EC4C61,
	BipedRagdollCreator_OpenScriptReference_mF8E9BFB60699FB63452D67EA1F472BFB16F07060,
	BipedRagdollCreator_OpenTutorial_mB8D72804137043CE371E4023CBE6B1C689608880,
	BipedRagdollCreator_AutodetectOptions_m6ACF38C0EC9449957B30284200F849E38D37A695,
	BipedRagdollCreator_Create_m3C2CBEC7FCEFB1486CFA5F245D3BBA010E708E3B,
	BipedRagdollCreator_CreateColliders_mDC7346D7EC17EED69D8C2EFB9349EC8F726B7DE2,
	BipedRagdollCreator_CreateHandCollider_m7E692275D8FDAE15267D7313AD7F2CC9104C4F89,
	BipedRagdollCreator_CreateFootCollider_m560E478862DFC309F12CD154AACCEBEEAF6E05EF,
	BipedRagdollCreator_GetChildCentroid_m43FAA92D367334318CD02725D494114137AA9BF4,
	BipedRagdollCreator_MassDistribution_mA39D51D32956B501ED574D35249C322625123779,
	BipedRagdollCreator_CreateJoints_m5B513C6952EBF786A03A5F435A4A95CED9DBCD2D,
	BipedRagdollCreator_CreateLimbJoints_m3EB7F93788EF538E69239F51ED56C1067C080708,
	BipedRagdollCreator_ClearBipedRagdoll_m420FAAE2FA727E1AD2414BD3F4F5A62125E3EB89,
	BipedRagdollCreator_IsClear_mDDF58CDD3A64DD3A0C06E07EDB31931A22730202,
	BipedRagdollCreator_GetUpperArmToHeadCentroid_m7648ADE2375E56B8D70F6D2EC7A0A3FF6C694EA1,
	BipedRagdollCreator_GetUpperArmCentroid_m908D86D1DFAFF7DBC1B548D2120581D801578661,
	BipedRagdollCreator__ctor_mE8452241AB7E28035F89AB733FECC86FA7CC1853,
	BipedRagdollReferences_IsValid_mDA4D8365899E9DC83616E0B017355B1E49A66107_AdjustorThunk,
	BipedRagdollReferences_IsChildRecursive_m3407509D571865F1DAEAFA868F85CECD2C089F8D_AdjustorThunk,
	BipedRagdollReferences_IsEmpty_m965F7658D4B1B597A35CBFA94090B81302A642BD_AdjustorThunk,
	BipedRagdollReferences_Contains_m45447FFFD48A23717118CE75D21220D86D13C6E1_AdjustorThunk,
	BipedRagdollReferences_GetRagdollTransforms_m0D55607F6C00614EA33CCB993A2464938744FE87_AdjustorThunk,
	BipedRagdollReferences_FromAvatar_mC8E55D297B46113B34CF50036CDA2D1C9320E7DB,
	BipedRagdollReferences_FromBipedReferences_m1ED51DE3694E1384314A60FE77C77BF2940A49D4,
	JointConverter_ToConfigurable_m7486BC65316A40EB2BF42114838103CCBD13E259,
	JointConverter_HingeToConfigurable_m77A241554ECF48D3E28958108FB591EF5860A16E,
	JointConverter_FixedToConfigurable_m398C68A83D2E198996258480DFD3099E5058F8DC,
	JointConverter_SpringToConfigurable_m980A27C816680FE3C712A2CCE852A235128F4D28,
	JointConverter_CharacterToConfigurable_m0203B5B8D02570FACAD1E4C9A34188AFB2EF4625,
	JointConverter_ConvertJoint_mAFC0BED539F03AE3ECB6CC351C3B2D1A56D97720,
	JointConverter_ConvertToHighSoftJointLimit_m003C1A9DCF0D07D09AB23A8B1AB843042F34668C,
	JointConverter_ConvertToLowSoftJointLimit_mAE3FFC0F9B67B116D566B6D9FB41ABA67A56B4B2,
	JointConverter_ConvertToSoftJointLimitSpring_m04949A8CFE6257138BE321B39C98E55712639D91,
	JointConverter_CopyLimit_m0270D2DD51F9A0300FAEDCB89394AA02A25A8AE1,
	JointConverter_CopyLimitSpring_m290B06E81D8615769F5AB0834C23707EA06A0104,
	RagdollCreator_ClearAll_m025B764017222F843D573856F79E08E70EE3AF59,
	RagdollCreator_ClearTransform_m44076A67B8AC49910F5DC6E565E948863940B631,
	RagdollCreator_CreateCollider_mD0B50640725C13B6C3610AE00351556918A4C5AC,
	RagdollCreator_CreateCollider_mBF2A8AC2AB0914283E3B7CF081BC5642B344F259,
	RagdollCreator_GetScaleF_m2BF9F7F2053AFCF4D56BDDD4B6C5D0C4755BA6A9,
	RagdollCreator_Abs_mF093AE880AB1C58728DF9694786E55E0C5F39EF3,
	RagdollCreator_Vector3Abs_m670CE644388B80D42B8669A8B9C0D415BF90AD4C,
	RagdollCreator_DirectionIntToVector3_m5E21BA8ADEB22315A3240D71E224E64BD4F01516,
	RagdollCreator_DirectionToVector3_m6E252DE8EF52F5587EC977594E328172A496142C,
	RagdollCreator_DirectionVector3ToInt_m38E848F97AA8073271DE9B6F74D084B6CE712391,
	RagdollCreator_GetLocalOrthoDirection_m0C36A6B96D67F9610EB6254A51CC3C4328101DB5,
	RagdollCreator_GetConnectedBody_mEA345AD45FC52ADECB35D1FADE56206F01622DB5,
	RagdollCreator_CreateJoint_mF9CD250B2D718D2383696058615C997345A8406F,
	RagdollCreator_ToSoftJointLimit_m385750A35122960E624DB7E610A7B58BEBA03751,
	RagdollCreator__ctor_m8D076280BBA9F56C8160AF0A04DB67C3E92BA6D9,
	RagdollEditor_OpenUserManual_mC606D6CA3E4E0ED0CD39ACA7640D553EBE7D4CC5,
	RagdollEditor_OpenScriptReference_m5A598EE5C81BE7956A751E35464D14A5249F52E6,
	RagdollEditor_OpenTutorial_m48DBCD4CC69F3769E6C7AB2C8F078E0744500E55,
	RagdollEditor__ctor_m25958F96AD6C115CF5C18DF9D2167DBC279512E4,
	LimbOrientation__ctor_m319A1CD829F39F8B992734E1BC7EE40FA02F1E72,
	AutoDetectParams__ctor_m2435ACA52D63236221123817EAF30185D49F414D_AdjustorThunk,
	AutoDetectParams_get_Default_m3D6674EE654AE9CECE139BE08AC9AC5EB8E7125D,
	Logger__ctor_mD837D3055E1B46E799F39F4D47CD32182421BEE1,
	Logger_Invoke_mB3A4E7D37799962FBA128DDEF6505472715C8CD0,
	Logger_BeginInvoke_m8F3EBFF0C8C04463D7D417589ADD3F9023C2DA2B,
	Logger_EndInvoke_mCF47445F73C5AB02DF45A5DE02B3E805951F5A40,
	Action__ctor_m36112F278BFEBAD5B68584424AF11D38FD26E575,
	U3CDestructU3Ed__2__ctor_mE25109E867AB45381390300EA99BAFAF31D04B7E,
	U3CDestructU3Ed__2_System_IDisposable_Dispose_m415F539D035D8FE468C51E2DAAFA70C399D6CD28,
	U3CDestructU3Ed__2_MoveNext_mAA9C444FE8911C0BB46043E0D2C127CC0E457D63,
	U3CDestructU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4D0E9A81C619510B9C02CC15697700771433AC7A,
	U3CDestructU3Ed__2_System_Collections_IEnumerator_Reset_mFAB5902921AB540AEE327D996F60A03F8D194FB6,
	U3CDestructU3Ed__2_System_Collections_IEnumerator_get_Current_m6114D6399412B96ACD40D8A3055A4C94A178F61D,
	U3CFadeOutPinWeightU3Ed__10__ctor_m17EE4B1D3BB3F8372015BAB95CE3DA2063F5C893,
	U3CFadeOutPinWeightU3Ed__10_System_IDisposable_Dispose_m47A17E1D9D97189F0DE1F213BE980FEE7CC2F7B7,
	U3CFadeOutPinWeightU3Ed__10_MoveNext_m18080DEA0B9EB98B9633277B49D2502EBD325CDC,
	U3CFadeOutPinWeightU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0ED4FF6DF45BB3FEED0865E13791CCCAE8C81BD0,
	U3CFadeOutPinWeightU3Ed__10_System_Collections_IEnumerator_Reset_mF1964A707B085132B62920D94BD03C9D03739B5A,
	U3CFadeOutPinWeightU3Ed__10_System_Collections_IEnumerator_get_Current_mF89F36668BFDF7540B38960E45769FCE2138CF63,
	U3CFadeOutMuscleWeightU3Ed__11__ctor_m3ACDEEF054A2C8D132060A1C84C120732127493E,
	U3CFadeOutMuscleWeightU3Ed__11_System_IDisposable_Dispose_m8F4175A8C96A4294FF2304332A955E2DE9D1F923,
	U3CFadeOutMuscleWeightU3Ed__11_MoveNext_m97D98B8575B76881F56519688F58816B09032EAE,
	U3CFadeOutMuscleWeightU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBEF7D3406D57A3E06DCAD5ED2A17017A60543429,
	U3CFadeOutMuscleWeightU3Ed__11_System_Collections_IEnumerator_Reset_mBF5BCF7ED9B7E22F31E4C152CDDD28E69A0D8D81,
	U3CFadeOutMuscleWeightU3Ed__11_System_Collections_IEnumerator_get_Current_m86A5EEA3D980469EB77982D38F2FE0450A83CCE9,
	U3CActionU3Ed__11__ctor_mCD98042B06943AE03CB899663E45D2766B6C282B,
	U3CActionU3Ed__11_System_IDisposable_Dispose_m1184C21A0F4EEBA21622410B621AA7B468EB32BF,
	U3CActionU3Ed__11_MoveNext_m9DA1B08260D07B225E9A271F1AF6E9CAE2A4881D,
	U3CActionU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1BF234FF80172CDD15173BE0253E7B62F3CB8024,
	U3CActionU3Ed__11_System_Collections_IEnumerator_Reset_m43FCFE21C0454EDA6A74644E2E46CAECB43C889C,
	U3CActionU3Ed__11_System_Collections_IEnumerator_get_Current_mF2AC613007A12B52C2D44E5C44A4EECED2B346FD,
	U3CLoseBalanceU3Ed__23__ctor_m6C0B4E5DAB1537B105D2D16EDC08B5748ABF6BAA,
	U3CLoseBalanceU3Ed__23_System_IDisposable_Dispose_mEDFEEF5D31705C6457EF8BA9AA836D1E117F2D9D,
	U3CLoseBalanceU3Ed__23_MoveNext_m25497A9507BF70163E25C330E0C84138362A40DF,
	U3CLoseBalanceU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m243910F219229B1A8DE1794D93940195E6F0BA29,
	U3CLoseBalanceU3Ed__23_System_Collections_IEnumerator_Reset_m55DD46FEEABE2677726B3860729493CBBF341957,
	U3CLoseBalanceU3Ed__23_System_Collections_IEnumerator_get_Current_mB3A4F1B9777E2CFA20D025D66D3C609F2D1EE0A7,
	BehaviourDelegate__ctor_mDC6E06D6EA50133126E55B4D01458C7C38B4F831,
	BehaviourDelegate_Invoke_m2E21B79018FF6D07D694AE14E31B55AEE7D799FE,
	BehaviourDelegate_BeginInvoke_m5D2389AAFCD6A4353C0A52899325FBDD70E16658,
	BehaviourDelegate_EndInvoke_m54DAA044F652BAE49E51CDE70E3C529DC297D181,
	HitDelegate__ctor_m5D39E94D34D75B1E66130DE4B7B6FF5C119E0086,
	HitDelegate_Invoke_mBE83643AA541AF6EC6F71C41384EA642964FECB9,
	HitDelegate_BeginInvoke_mCA82EEEB242FF7C5FC98B7251FF48DF45F4A813C,
	HitDelegate_EndInvoke_m32CD136984ED5B1A1596FA08B845FE73065FAA8B,
	CollisionDelegate__ctor_mD3EF92BD4ACBBD591F073698FBD2C2D506537B01,
	CollisionDelegate_Invoke_m0CF45D6D17978394284E6ADCB82C6A1A05ABC21C,
	CollisionDelegate_BeginInvoke_m62B9A77B9BE5A6F6235FEA6A702D2E3358FB42A3,
	CollisionDelegate_EndInvoke_mC894E90A84C65D68115AD861738F75A02808889C,
	PuppetEvent_get_switchBehaviour_m239070C6A51734D6A66B91F822D876234405BE36_AdjustorThunk,
	PuppetEvent_Trigger_mAAF24429CA3A9AADBCB59F01130265D2E2BE81F9_AdjustorThunk,
	AnimatorEvent_Activate_m963A857D5FA9635D98029C6FBF1C38586A3F5CC7,
	AnimatorEvent_Activate_m6F3086B3C67091F6363568FD9F6DAAC992E64F4A,
	AnimatorEvent_Activate_mD49B580BE52A6511EEAE801CBDF8148B3253D0CD,
	AnimatorEvent__ctor_m1D4043E6290AE4619DCDDE0DD2467B674B5D2615,
	U3CSmoothActivateU3Ed__21__ctor_mC13E4922758110ED81D0470B5FDBB43C31DBF765,
	U3CSmoothActivateU3Ed__21_System_IDisposable_Dispose_m19C5D66935D252E60D2BA61AFB4976A009EAA83F,
	U3CSmoothActivateU3Ed__21_MoveNext_m62D59362D58C7D9D5C3904E79CB6DB85EF5CC182,
	U3CSmoothActivateU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m90430D8A1D5E5A68903395109F999DB4587C95C4,
	U3CSmoothActivateU3Ed__21_System_Collections_IEnumerator_Reset_m9AF719A255135FD14022709E44319B63F4D4DB30,
	U3CSmoothActivateU3Ed__21_System_Collections_IEnumerator_get_Current_mFE63DE004EB7A434C9AFCD91EFE181730BBA6BD0,
	MasterProps__ctor_m75E7889DD20AEDFF4B362AD3EFD3AFA7A5DF7223,
	CollisionDelegate__ctor_m902C43D4DF833C58D5893142F0B327B995A5E6A9,
	CollisionDelegate_Invoke_m88AB3391DAA653BA50FF1249C15B75645722ACE0,
	CollisionDelegate_BeginInvoke_mEF8F89478770040257AB37F31AB2820B1C4ACCE3,
	CollisionDelegate_EndInvoke_m6FD1080748E51F155EA164FF9B72AA9575A38FA6,
	CollisionImpulseDelegate__ctor_m165FC9F1850C5A4BD70ADCD231BDAE3D40223989,
	CollisionImpulseDelegate_Invoke_mDABC542F10FF44FEEB72894C7436184F0EFE7762,
	CollisionImpulseDelegate_BeginInvoke_m78A6DD56AF2A038E82DEFCD1782A955E99566089,
	CollisionImpulseDelegate_EndInvoke_mD3F3886BD7892A555E82C1B5A3850B4F9E7D476C,
	Settings__ctor_m8625A5A4A7A526DA4A0732B67FDBE6D13A75628B,
	Props__ctor_m12A229CCA990A456E3931384B0FDF6557DF83296,
	Props__ctor_mCBF341FD742B9E35EE2FD8C4FB9D00DE87B21691,
	Props_Clamp_mEBDEAFCB7FAF273D1846295BF667D4917CBF23D2,
	State_get_Default_m9F147BF7DB595D1F604A981BA180C743E0C5BE8A,
	State_Clamp_m1A181B74A1F047C52971C9105DE635559902F22E_AdjustorThunk,
	UpdateDelegate__ctor_mE9035718C1411B3FCE927E81EAABC46B9A9FC819,
	UpdateDelegate_Invoke_mA8EB8DA3089CEBD5C73E0C8D43C58FBE5CD551C9,
	UpdateDelegate_BeginInvoke_mD1A66CCB14C927AB3635C7DA80F6CFD892A41B01,
	UpdateDelegate_EndInvoke_mDCCE415B1CB196AD26DE2B0610FA3149F2CC8CA8,
	MuscleDelegate__ctor_mD76393A809DE05B32959AFAAB983AB818EC4D9E7,
	MuscleDelegate_Invoke_mF9C7DDD2938689E279556BFA02B717A4EF4512BD,
	MuscleDelegate_BeginInvoke_m80217B44316E0F026E4C209BE2A5AC97E44F3DFF,
	MuscleDelegate_EndInvoke_m8F48C9088D6A9457C2B12EDB947E9A9B99EA8CB6,
	StateSettings__ctor_m99A580B475E4DD34014A94B53C7F2C1DABD39673_AdjustorThunk,
	StateSettings_get_Default_mDAE02DEE9542D59805031FCACDD4F25C649320DA,
	U3CDisabledToActiveU3Ed__136__ctor_mE6BD06DA4998D2829846E8A69C1E334760B7C7B0,
	U3CDisabledToActiveU3Ed__136_System_IDisposable_Dispose_m9FA62B5E60626A63819B03BCB7785E03AF2DC5C5,
	U3CDisabledToActiveU3Ed__136_MoveNext_m8E43FFE48DBDF95204CF372A64FA83CAD2052C4D,
	U3CDisabledToActiveU3Ed__136_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m50939F455F4EFBFE76DFA3D0E361ED51A8C9B481,
	U3CDisabledToActiveU3Ed__136_System_Collections_IEnumerator_Reset_m16F0EB6B51D8D5F28B8E6F6472CB018B00273966,
	U3CDisabledToActiveU3Ed__136_System_Collections_IEnumerator_get_Current_m9225135FA8D81E5D3F62E5153E450843B52D511A,
	U3CKinematicToActiveU3Ed__138__ctor_m45F1A67C78067BE68FC25E4DDCF2425FCD2619CA,
	U3CKinematicToActiveU3Ed__138_System_IDisposable_Dispose_mC2C475433CAE533CF2F07C07C93D20372B3C9770,
	U3CKinematicToActiveU3Ed__138_MoveNext_mB77C0F87D0C474597B36A4B132CE6827759B48E3,
	U3CKinematicToActiveU3Ed__138_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBE320C6CAF45847EA30E23A671A5E97C6B2B8294,
	U3CKinematicToActiveU3Ed__138_System_Collections_IEnumerator_Reset_m9ECF77B38E27F125E7553A055B473A75592A6F31,
	U3CKinematicToActiveU3Ed__138_System_Collections_IEnumerator_get_Current_mB8EAF2AD7F59BD09B169214BF1258264EB63DD7E,
	U3CActiveToDisabledU3Ed__139__ctor_m1E03104CE360F023A15B8EFCC6CD698A46692803,
	U3CActiveToDisabledU3Ed__139_System_IDisposable_Dispose_m934F86BBF99E3D2241414F5DC2427D4A56AB7661,
	U3CActiveToDisabledU3Ed__139_MoveNext_m7C5FC9AD22A75CF9C2239127330FB3E299C90824,
	U3CActiveToDisabledU3Ed__139_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFE7000D2F89EBFBB16B21C2F7A59ADE66E242ECF,
	U3CActiveToDisabledU3Ed__139_System_Collections_IEnumerator_Reset_mC0776974F6F881C5140B2974774873B2ABE324E3,
	U3CActiveToDisabledU3Ed__139_System_Collections_IEnumerator_get_Current_m00237DF83C194E70DB686F82A21A6FAE604A6872,
	U3CActiveToKinematicU3Ed__140__ctor_m76E9307DC4784748B261C450DC40D22BFB1AED46,
	U3CActiveToKinematicU3Ed__140_System_IDisposable_Dispose_m03C00836DA8A5774BA3D6058AB776FD56C4E1D08,
	U3CActiveToKinematicU3Ed__140_MoveNext_m0805F7D458FC8C1DAFA2746CD9E6A1ADBFCA278F,
	U3CActiveToKinematicU3Ed__140_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m16FDB5D544107C3B85585005D0D12A3A29A644C9,
	U3CActiveToKinematicU3Ed__140_System_Collections_IEnumerator_Reset_mBEC42670BE484BCD731733ED5221A36414A5E79B,
	U3CActiveToKinematicU3Ed__140_System_Collections_IEnumerator_get_Current_m6DE2BE7367A93745AAC98D1EF6D4DA694291FC54,
	U3CAliveToDeadU3Ed__192__ctor_m17CD7CD77BD1DF061AB3DFE68C43F112964D1E3F,
	U3CAliveToDeadU3Ed__192_System_IDisposable_Dispose_m23A7843354EC8E19B9E426D28D235A8294C58157,
	U3CAliveToDeadU3Ed__192_MoveNext_m09A893C9973DA8C1881E854B330FBCDE0836A637,
	U3CAliveToDeadU3Ed__192_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m96047B7BF2F812F1A210A849A6CF5EB22FCA4B36,
	U3CAliveToDeadU3Ed__192_System_Collections_IEnumerator_Reset_mA611CC9C671D3C89ED1E8CA033A32F4A2132EB2F,
	U3CAliveToDeadU3Ed__192_System_Collections_IEnumerator_get_Current_m96B1869FDF4515898A9C7A2E4976E6B60FC705AA,
	HumanoidMuscle__ctor_m48EC18EEC270057667002ADDBB3A79E518D898FC,
	PuppetUpdateLimit__ctor_m941D0329E6CC085B315BF5BEE54733FE081A529A,
	PuppetUpdateLimit_Step_m863B2D4EA03B10C9475C998C127BF1887A7A2CDA,
	PuppetUpdateLimit_Update_m30F3C7F37A60EAEA70273C819C222DBAC268E611,
	Options_get_Default_m9EC809DDC0293363364A0BB39DA71ED726B45E5D,
	CreateJointParams__ctor_m9DDCEB8D59DE3F5F954B09B52F1F5D3F7C5FD2B9_AdjustorThunk,
	Anim__ctor_m2577993D4DA14B01F7EE9369A2993F726E088E6F,
	Limits__ctor_m1F48E7A4951F39DE86FFA9053A21490357A0A843_AdjustorThunk,
};
static const int32_t s_InvokerIndices[991] = 
{
	23,
	23,
	26,
	1897,
	23,
	664,
	275,
	664,
	275,
	664,
	275,
	23,
	23,
	23,
	23,
	23,
	23,
	275,
	664,
	2146,
	23,
	23,
	23,
	2146,
	23,
	2192,
	2193,
	2194,
	2194,
	1863,
	1863,
	23,
	377,
	4,
	4,
	250,
	250,
	315,
	315,
	1,
	186,
	186,
	494,
	109,
	109,
	109,
	109,
	109,
	109,
	109,
	109,
	109,
	99,
	99,
	99,
	99,
	0,
	0,
	3,
	102,
	102,
	176,
	412,
	2195,
	2196,
	2196,
	178,
	178,
	99,
	2197,
	2197,
	2198,
	2199,
	2200,
	2200,
	178,
	178,
	178,
	178,
	178,
	178,
	2201,
	23,
	23,
	23,
	23,
	109,
	0,
	99,
	99,
	874,
	152,
	1,
	0,
	1,
	99,
	23,
	26,
	27,
	2203,
	2204,
	1834,
	1187,
	1187,
	1187,
	1187,
	1187,
	1187,
	1187,
	1187,
	1187,
	1187,
	1187,
	1187,
	1187,
	1187,
	1187,
	1187,
	1187,
	1187,
	1187,
	1187,
	1187,
	1187,
	1187,
	1187,
	1187,
	1187,
	1187,
	23,
	26,
	27,
	1848,
	2205,
	2205,
	2205,
	2205,
	2206,
	2207,
	2207,
	2208,
	2208,
	2208,
	2209,
	1173,
	1173,
	2210,
	2210,
	2211,
	1179,
	1179,
	1073,
	2212,
	2213,
	2214,
	-1,
	-1,
	-1,
	-1,
	23,
	23,
	23,
	23,
	23,
	23,
	102,
	23,
	23,
	390,
	102,
	23,
	23,
	23,
	23,
	26,
	26,
	26,
	23,
	1157,
	1157,
	1157,
	1157,
	2215,
	2215,
	2179,
	1931,
	1343,
	1343,
	23,
	32,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	26,
	23,
	14,
	23,
	23,
	23,
	14,
	23,
	9,
	23,
	14,
	26,
	23,
	2216,
	23,
	102,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	23,
	23,
	23,
	14,
	14,
	23,
	23,
	2217,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	14,
	23,
	275,
	606,
	23,
	26,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	102,
	23,
	23,
	23,
	2216,
	23,
	23,
	23,
	23,
	23,
	14,
	23,
	102,
	23,
	23,
	23,
	1050,
	102,
	1919,
	23,
	23,
	23,
	23,
	23,
	23,
	1050,
	23,
	23,
	23,
	1050,
	102,
	23,
	23,
	23,
	2216,
	1050,
	23,
	2218,
	1919,
	1242,
	275,
	23,
	23,
	2219,
	23,
	102,
	31,
	23,
	23,
	2216,
	23,
	23,
	23,
	1051,
	23,
	102,
	1050,
	23,
	1050,
	102,
	23,
	23,
	102,
	31,
	23,
	23,
	23,
	23,
	23,
	1050,
	1050,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	102,
	23,
	1050,
	1051,
	10,
	32,
	26,
	1051,
	1051,
	1062,
	23,
	2219,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	2220,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	2221,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	2222,
	2223,
	2223,
	102,
	31,
	23,
	23,
	23,
	23,
	2222,
	2223,
	2223,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	275,
	32,
	1557,
	30,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	23,
	23,
	23,
	1027,
	664,
	23,
	23,
	23,
	10,
	32,
	23,
	2216,
	2221,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	102,
	23,
	751,
	26,
	26,
	1051,
	1240,
	1557,
	23,
	1024,
	2226,
	2227,
	275,
	911,
	2228,
	275,
	911,
	2228,
	23,
	2222,
	2223,
	2229,
	911,
	911,
	2230,
	102,
	2231,
	2232,
	52,
	2233,
	32,
	31,
	390,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	2222,
	2223,
	23,
	14,
	26,
	1050,
	1051,
	1050,
	1051,
	1050,
	1051,
	1050,
	1051,
	1050,
	1051,
	876,
	23,
	23,
	1198,
	1199,
	1073,
	1164,
	23,
	1050,
	1051,
	1050,
	1051,
	664,
	275,
	1050,
	1051,
	1050,
	1051,
	1239,
	1240,
	1239,
	1240,
	102,
	31,
	664,
	275,
	2235,
	23,
	2223,
	2223,
	23,
	23,
	23,
	1913,
	102,
	1050,
	1050,
	1050,
	1050,
	1050,
	23,
	26,
	23,
	23,
	23,
	14,
	26,
	14,
	26,
	14,
	26,
	1050,
	1051,
	14,
	1050,
	1051,
	1050,
	1051,
	1239,
	1240,
	176,
	14,
	26,
	14,
	26,
	23,
	26,
	23,
	23,
	23,
	2236,
	573,
	390,
	31,
	23,
	23,
	23,
	23,
	23,
	31,
	2237,
	275,
	23,
	1025,
	1134,
	1239,
	1239,
	1239,
	1239,
	2238,
	2239,
	2164,
	23,
	2242,
	2243,
	2244,
	26,
	26,
	26,
	23,
	2245,
	2246,
	2247,
	1860,
	1860,
	1860,
	2248,
	2248,
	1161,
	2249,
	2250,
	2251,
	2251,
	1161,
	2252,
	1164,
	1161,
	1050,
	1051,
	102,
	31,
	1050,
	1051,
	14,
	26,
	23,
	26,
	26,
	26,
	23,
	23,
	26,
	23,
	23,
	102,
	14,
	26,
	26,
	23,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	26,
	14,
	26,
	14,
	26,
	102,
	102,
	31,
	10,
	102,
	102,
	2253,
	102,
	23,
	23,
	23,
	23,
	28,
	23,
	26,
	23,
	102,
	23,
	275,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	10,
	23,
	1089,
	31,
	31,
	2254,
	23,
	2255,
	27,
	26,
	26,
	26,
	23,
	23,
	23,
	751,
	102,
	26,
	26,
	102,
	31,
	23,
	23,
	23,
	14,
	23,
	14,
	14,
	14,
	23,
	2227,
	2103,
	2227,
	2103,
	2227,
	2227,
	2227,
	28,
	28,
	28,
	9,
	37,
	104,
	104,
	104,
	424,
	151,
	35,
	313,
	26,
	126,
	23,
	99,
	102,
	102,
	31,
	102,
	102,
	23,
	2256,
	23,
	2256,
	23,
	23,
	310,
	23,
	23,
	31,
	23,
	23,
	23,
	31,
	102,
	23,
	275,
	23,
	23,
	102,
	23,
	23,
	573,
	766,
	23,
	430,
	430,
	104,
	176,
	2257,
	102,
	23,
	26,
	532,
	23,
	10,
	32,
	10,
	32,
	10,
	32,
	14,
	26,
	26,
	9,
	9,
	9,
	23,
	23,
	23,
	111,
	111,
	2192,
	2193,
	275,
	1827,
	1027,
	23,
	23,
	23,
	23,
	23,
	2260,
	2261,
	2261,
	2262,
	2263,
	1863,
	2261,
	2261,
	2264,
	2265,
	2266,
	2267,
	2267,
	23,
	767,
	103,
	176,
	412,
	14,
	2269,
	2269,
	111,
	111,
	111,
	111,
	111,
	2199,
	2270,
	2270,
	2271,
	2272,
	2273,
	111,
	111,
	2274,
	2275,
	423,
	1073,
	17,
	2192,
	2192,
	2193,
	1863,
	627,
	2276,
	2277,
	23,
	23,
	23,
	23,
	23,
	1055,
	42,
	2202,
	163,
	26,
	166,
	26,
	23,
	32,
	23,
	102,
	14,
	23,
	14,
	32,
	23,
	102,
	14,
	23,
	14,
	32,
	23,
	102,
	14,
	23,
	14,
	32,
	23,
	102,
	14,
	23,
	14,
	32,
	23,
	102,
	14,
	23,
	14,
	163,
	23,
	101,
	26,
	163,
	2222,
	2224,
	26,
	163,
	2223,
	2225,
	26,
	102,
	390,
	27,
	26,
	26,
	23,
	32,
	23,
	102,
	14,
	23,
	14,
	23,
	163,
	2223,
	2225,
	26,
	163,
	2217,
	2234,
	26,
	23,
	23,
	2240,
	23,
	2241,
	23,
	163,
	23,
	101,
	26,
	163,
	26,
	166,
	26,
	2258,
	2259,
	32,
	23,
	102,
	14,
	23,
	14,
	32,
	23,
	102,
	14,
	23,
	14,
	32,
	23,
	102,
	14,
	23,
	14,
	32,
	23,
	102,
	14,
	23,
	14,
	32,
	23,
	102,
	14,
	23,
	14,
	23,
	23,
	32,
	103,
	2268,
	2278,
	23,
	1025,
};
static const Il2CppTokenRangePair s_rgctxIndices[1] = 
{
	{ 0x02000013, { 0, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[2] = 
{
	{ (Il2CppRGCTXDataType)2, 19845 },
	{ (Il2CppRGCTXDataType)2, 19843 },
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpU2DfirstpassCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpU2DfirstpassCodeGenModule = 
{
	"Assembly-CSharp-firstpass.dll",
	991,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	1,
	s_rgctxIndices,
	2,
	s_rgctxValues,
	NULL,
};
