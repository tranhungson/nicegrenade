﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void PrimitivesPro.ThirdParty.P2T.P2T::Triangulate(PrimitivesPro.ThirdParty.P2T.Polygon)
extern void P2T_Triangulate_m2265E2FB4CFB0D5546346B1FDA7677B9D09F5E7B ();
// 0x00000002 PrimitivesPro.ThirdParty.P2T.TriangulationContext PrimitivesPro.ThirdParty.P2T.P2T::CreateContext(PrimitivesPro.ThirdParty.P2T.TriangulationAlgorithm)
extern void P2T_CreateContext_m98D7C59587E9955306AA0EAFFAC18E95D60A6111 ();
// 0x00000003 System.Void PrimitivesPro.ThirdParty.P2T.P2T::Triangulate(PrimitivesPro.ThirdParty.P2T.TriangulationAlgorithm,PrimitivesPro.ThirdParty.P2T.ITriangulatable)
extern void P2T_Triangulate_m56D486DD7FBCEDACEE70F362F091EE012FDA1D19 ();
// 0x00000004 System.Void PrimitivesPro.ThirdParty.P2T.P2T::Triangulate(PrimitivesPro.ThirdParty.P2T.TriangulationContext)
extern void P2T_Triangulate_m9770BA5BA667B7D58C1253E110E76F4F4FDD6DD6 ();
// 0x00000005 PrimitivesPro.ThirdParty.P2T.Point2DList_WindingOrderType PrimitivesPro.ThirdParty.P2T.Point2DList::get_WindingOrder()
extern void Point2DList_get_WindingOrder_m847E214B845C646EF7D229A919C8DEFE209DEB28 ();
// 0x00000006 PrimitivesPro.ThirdParty.P2T.Point2D PrimitivesPro.ThirdParty.P2T.Point2DList::get_Item(System.Int32)
extern void Point2DList_get_Item_m587E1CFAA4DDC871A7DC5D4F6F4F546228183AC1 ();
// 0x00000007 System.Void PrimitivesPro.ThirdParty.P2T.Point2DList::set_Item(System.Int32,PrimitivesPro.ThirdParty.P2T.Point2D)
extern void Point2DList_set_Item_mD3702E57C7E3196880A12B5574069D100BDBFB79 ();
// 0x00000008 System.Int32 PrimitivesPro.ThirdParty.P2T.Point2DList::get_Count()
extern void Point2DList_get_Count_mA55165B5410DA07EA0B7CEFDAB022C653D61C8AC ();
// 0x00000009 System.Boolean PrimitivesPro.ThirdParty.P2T.Point2DList::get_IsReadOnly()
extern void Point2DList_get_IsReadOnly_mA7AC7EE2AA779418A0822BFEC2468C30BA839EE0 ();
// 0x0000000A System.Void PrimitivesPro.ThirdParty.P2T.Point2DList::.ctor()
extern void Point2DList__ctor_mF36BD99069E350E7E979A371F87A38E8C0D7287A ();
// 0x0000000B System.String PrimitivesPro.ThirdParty.P2T.Point2DList::ToString()
extern void Point2DList_ToString_m1FC8770A4C9EE929F6BB8D0F0A5CAE801BAB5C57 ();
// 0x0000000C System.Collections.IEnumerator PrimitivesPro.ThirdParty.P2T.Point2DList::System.Collections.IEnumerable.GetEnumerator()
extern void Point2DList_System_Collections_IEnumerable_GetEnumerator_m9FD67933A634939438FD057641E72EC281659BC0 ();
// 0x0000000D System.Collections.Generic.IEnumerator`1<PrimitivesPro.ThirdParty.P2T.Point2D> PrimitivesPro.ThirdParty.P2T.Point2DList::System.Collections.Generic.IEnumerable<PrimitivesPro.ThirdParty.P2T.Point2D>.GetEnumerator()
extern void Point2DList_System_Collections_Generic_IEnumerableU3CPrimitivesPro_ThirdParty_P2T_Point2DU3E_GetEnumerator_m8D32FD791B82DAFE4EA591EF71EB6D9C78BB5D56 ();
// 0x0000000E System.Void PrimitivesPro.ThirdParty.P2T.Point2DList::Clear()
extern void Point2DList_Clear_m160050443EC8A5ABFA0407DDEEA6FEA73603366F ();
// 0x0000000F System.Int32 PrimitivesPro.ThirdParty.P2T.Point2DList::IndexOf(PrimitivesPro.ThirdParty.P2T.Point2D)
extern void Point2DList_IndexOf_m98A802D7C7FE3A8B31298CCFF8FD85DE3C3FC5A3 ();
// 0x00000010 System.Void PrimitivesPro.ThirdParty.P2T.Point2DList::Add(PrimitivesPro.ThirdParty.P2T.Point2D)
extern void Point2DList_Add_m54B3C19571876AFE974EB3D69F8F82503F2DD98F ();
// 0x00000011 System.Void PrimitivesPro.ThirdParty.P2T.Point2DList::Add(PrimitivesPro.ThirdParty.P2T.Point2D,System.Int32,System.Boolean)
extern void Point2DList_Add_m28B76A0449872AD25EFB39BC4CE6C46062A1020D ();
// 0x00000012 System.Void PrimitivesPro.ThirdParty.P2T.Point2DList::Insert(System.Int32,PrimitivesPro.ThirdParty.P2T.Point2D)
extern void Point2DList_Insert_m69D1941D1846E3B0A5E9B8A634CBB7A37D084461 ();
// 0x00000013 System.Boolean PrimitivesPro.ThirdParty.P2T.Point2DList::Remove(PrimitivesPro.ThirdParty.P2T.Point2D)
extern void Point2DList_Remove_mE29EF7058DC9F5C576D8CD21ECA1536B1276E74C ();
// 0x00000014 System.Void PrimitivesPro.ThirdParty.P2T.Point2DList::RemoveAt(System.Int32)
extern void Point2DList_RemoveAt_m8E8FF6C69E6769E369A9296B4BD74AA192DA49C4 ();
// 0x00000015 System.Boolean PrimitivesPro.ThirdParty.P2T.Point2DList::Contains(PrimitivesPro.ThirdParty.P2T.Point2D)
extern void Point2DList_Contains_mDB1A6AAF6F21133BE7D46B53B44F679CB4808B97 ();
// 0x00000016 System.Void PrimitivesPro.ThirdParty.P2T.Point2DList::CopyTo(PrimitivesPro.ThirdParty.P2T.Point2D[],System.Int32)
extern void Point2DList_CopyTo_m70EC8A9DB00BBA04F9DF82455171C9B021B84B3E ();
// 0x00000017 System.Void PrimitivesPro.ThirdParty.P2T.Point2DList::CalculateBounds()
extern void Point2DList_CalculateBounds_m1D92DD47F0B60555E945ABC89D705E6E3CAF5AA6 ();
// 0x00000018 System.Double PrimitivesPro.ThirdParty.P2T.Point2DList::CalculateEpsilon()
extern void Point2DList_CalculateEpsilon_m2AF4DE318D977DFE6A71BE6F37DC7A6010CC6EA6 ();
// 0x00000019 PrimitivesPro.ThirdParty.P2T.Point2DList_WindingOrderType PrimitivesPro.ThirdParty.P2T.Point2DList::CalculateWindingOrder()
extern void Point2DList_CalculateWindingOrder_mB09CBDF4F024A46AECDAD89DAD2D80EF9A6BF568 ();
// 0x0000001A System.Double PrimitivesPro.ThirdParty.P2T.Point2DList::GetSignedArea()
extern void Point2DList_GetSignedArea_m5DE1276773C4CE759D9090CD2EDDB368DF1C0657 ();
// 0x0000001B System.Void PrimitivesPro.ThirdParty.P2T.Point2DList::.cctor()
extern void Point2DList__cctor_mD49010C5D792101676D05212E90F683E84BAE9EC ();
// 0x0000001C PrimitivesPro.ThirdParty.P2T.TriangulationMode PrimitivesPro.ThirdParty.P2T.ITriangulatable::get_TriangulationMode()
// 0x0000001D System.Void PrimitivesPro.ThirdParty.P2T.ITriangulatable::Prepare(PrimitivesPro.ThirdParty.P2T.TriangulationContext)
// 0x0000001E System.Void PrimitivesPro.ThirdParty.P2T.ITriangulatable::AddTriangle(PrimitivesPro.ThirdParty.P2T.DelaunayTriangle)
// 0x0000001F System.Void PrimitivesPro.ThirdParty.P2T.ITriangulatable::AddTriangles(System.Collections.Generic.IEnumerable`1<PrimitivesPro.ThirdParty.P2T.DelaunayTriangle>)
// 0x00000020 System.Collections.Generic.IList`1<PrimitivesPro.ThirdParty.P2T.DelaunayTriangle> PrimitivesPro.ThirdParty.P2T.Polygon::get_Triangles()
extern void Polygon_get_Triangles_mADFE36338A7A73E84F569982AEF15D69B80D592A ();
// 0x00000021 PrimitivesPro.ThirdParty.P2T.TriangulationMode PrimitivesPro.ThirdParty.P2T.Polygon::get_TriangulationMode()
extern void Polygon_get_TriangulationMode_m6E8E18B5060EBA99707D4C93041C9CE449838968 ();
// 0x00000022 PrimitivesPro.ThirdParty.P2T.TriangulationPoint PrimitivesPro.ThirdParty.P2T.Polygon::get_Item(System.Int32)
extern void Polygon_get_Item_m6AB8811BAFA88F6F6372D1A7C1E52B59AED6B401 ();
// 0x00000023 System.Void PrimitivesPro.ThirdParty.P2T.Polygon::set_Item(System.Int32,PrimitivesPro.ThirdParty.P2T.TriangulationPoint)
extern void Polygon_set_Item_m8BA39DE0F9815993F056D2769A63B9CD09927622 ();
// 0x00000024 System.Void PrimitivesPro.ThirdParty.P2T.Polygon::.ctor(System.Collections.Generic.IList`1<PrimitivesPro.ThirdParty.P2T.PolygonPoint>)
extern void Polygon__ctor_m41CC45797BE5018C5FE4D8EFCD78FFF9BB04BDC7 ();
// 0x00000025 System.Collections.Generic.IEnumerator`1<PrimitivesPro.ThirdParty.P2T.TriangulationPoint> PrimitivesPro.ThirdParty.P2T.Polygon::System.Collections.Generic.IEnumerable<PrimitivesPro.ThirdParty.P2T.TriangulationPoint>.GetEnumerator()
extern void Polygon_System_Collections_Generic_IEnumerableU3CPrimitivesPro_ThirdParty_P2T_TriangulationPointU3E_GetEnumerator_m975A4E825027A62584079E20BD8C319D9D0BEADE ();
// 0x00000026 System.Int32 PrimitivesPro.ThirdParty.P2T.Polygon::IndexOf(PrimitivesPro.ThirdParty.P2T.TriangulationPoint)
extern void Polygon_IndexOf_m3A098F2506CEAD6DB7CF1F77CA6AB0A214C61E49 ();
// 0x00000027 System.Void PrimitivesPro.ThirdParty.P2T.Polygon::Add(PrimitivesPro.ThirdParty.P2T.Point2D)
extern void Polygon_Add_mD82493AA2218FCF1D22227D3CEE8345E6CEFC374 ();
// 0x00000028 System.Void PrimitivesPro.ThirdParty.P2T.Polygon::Add(PrimitivesPro.ThirdParty.P2T.TriangulationPoint)
extern void Polygon_Add_m67E8CF7034FC15DA0C755D3730A1CBAB71D49D10 ();
// 0x00000029 System.Void PrimitivesPro.ThirdParty.P2T.Polygon::Add(PrimitivesPro.ThirdParty.P2T.Point2D,System.Int32,System.Boolean)
extern void Polygon_Add_m2CF2A9F4A37C16EFBC3729523A5DEC1402A67EF5 ();
// 0x0000002A System.Void PrimitivesPro.ThirdParty.P2T.Polygon::AddRange(System.Collections.Generic.IList`1<PrimitivesPro.ThirdParty.P2T.PolygonPoint>,PrimitivesPro.ThirdParty.P2T.Point2DList_WindingOrderType)
extern void Polygon_AddRange_mD58133E337FA6C8D71B3BFB0D344E562EF3D3402 ();
// 0x0000002B System.Void PrimitivesPro.ThirdParty.P2T.Polygon::Insert(System.Int32,PrimitivesPro.ThirdParty.P2T.TriangulationPoint)
extern void Polygon_Insert_m608054C6FD476F4A7F643B2685F3435436C5CD7A ();
// 0x0000002C System.Boolean PrimitivesPro.ThirdParty.P2T.Polygon::Remove(PrimitivesPro.ThirdParty.P2T.TriangulationPoint)
extern void Polygon_Remove_m140634EC0628DCE5D212085D23BC065229467DFD ();
// 0x0000002D System.Boolean PrimitivesPro.ThirdParty.P2T.Polygon::Contains(PrimitivesPro.ThirdParty.P2T.TriangulationPoint)
extern void Polygon_Contains_m49E2AFC71DCA6CEDBE44FD7DD20404E63A06B6C5 ();
// 0x0000002E System.Void PrimitivesPro.ThirdParty.P2T.Polygon::CopyTo(PrimitivesPro.ThirdParty.P2T.TriangulationPoint[],System.Int32)
extern void Polygon_CopyTo_mCC114FA7E6C0C697E5A67349FBEBBE584A369171 ();
// 0x0000002F System.Void PrimitivesPro.ThirdParty.P2T.Polygon::AddHole(PrimitivesPro.ThirdParty.P2T.Polygon)
extern void Polygon_AddHole_m509B7FC04488F6C09161EB94813E726091860B1C ();
// 0x00000030 System.Void PrimitivesPro.ThirdParty.P2T.Polygon::AddTriangle(PrimitivesPro.ThirdParty.P2T.DelaunayTriangle)
extern void Polygon_AddTriangle_m88213EC573B49503F1E6E120C3E1BCD8A1E5A9E1 ();
// 0x00000031 System.Void PrimitivesPro.ThirdParty.P2T.Polygon::AddTriangles(System.Collections.Generic.IEnumerable`1<PrimitivesPro.ThirdParty.P2T.DelaunayTriangle>)
extern void Polygon_AddTriangles_m0749DDBC31BB532C20A546735521CFC458771C48 ();
// 0x00000032 System.Void PrimitivesPro.ThirdParty.P2T.Polygon::Prepare(PrimitivesPro.ThirdParty.P2T.TriangulationContext)
extern void Polygon_Prepare_m7DB72852FA3F380E06402FE5F2E337073CF99B10 ();
// 0x00000033 System.Double PrimitivesPro.ThirdParty.P2T.Point2D::get_X()
extern void Point2D_get_X_mDA911B972ADD96890B6FEB380023B8F09C873E10 ();
// 0x00000034 System.Double PrimitivesPro.ThirdParty.P2T.Point2D::get_Y()
extern void Point2D_get_Y_mFCAB6009E3D429D049837FFAC5599B781F14C87D ();
// 0x00000035 System.Void PrimitivesPro.ThirdParty.P2T.Point2D::.ctor(System.Double,System.Double)
extern void Point2D__ctor_m741E4A62812BBA5CBEC7FC71267341B9D1463A98 ();
// 0x00000036 System.String PrimitivesPro.ThirdParty.P2T.Point2D::ToString()
extern void Point2D_ToString_mE2C8E8D18BF164965F65C22D1F89075132D383A9 ();
// 0x00000037 System.Int32 PrimitivesPro.ThirdParty.P2T.Point2D::GetHashCode()
extern void Point2D_GetHashCode_mABA36DB7C5F9D1BE4A87781A11AC12FA10711A6D ();
// 0x00000038 System.Boolean PrimitivesPro.ThirdParty.P2T.Point2D::Equals(System.Object)
extern void Point2D_Equals_m953CB3C2A1285F07A28D30FA491FD16C32B38415 ();
// 0x00000039 System.Boolean PrimitivesPro.ThirdParty.P2T.Point2D::Equals(PrimitivesPro.ThirdParty.P2T.Point2D)
extern void Point2D_Equals_m3D2CC40F1C83127B1C19759D8B387B2EDF0EB7F6 ();
// 0x0000003A System.Boolean PrimitivesPro.ThirdParty.P2T.Point2D::Equals(PrimitivesPro.ThirdParty.P2T.Point2D,System.Double)
extern void Point2D_Equals_mC7D83B9CC5ED24DC537758C51EAA2B625D9B205F ();
// 0x0000003B System.Int32 PrimitivesPro.ThirdParty.P2T.Point2D::CompareTo(PrimitivesPro.ThirdParty.P2T.Point2D)
extern void Point2D_CompareTo_mE7F4D52472AAC615E9E4046CF010B0949A7CC6CD ();
// 0x0000003C System.Double PrimitivesPro.ThirdParty.P2T.TriangulationPoint::get_X()
extern void TriangulationPoint_get_X_mD30E9BDBDA9AFEA926A45DF666824687CADCB0FB ();
// 0x0000003D System.Double PrimitivesPro.ThirdParty.P2T.TriangulationPoint::get_Y()
extern void TriangulationPoint_get_Y_mD25900A541DB899FD060C2F1D836928C251AB4EC ();
// 0x0000003E System.UInt32 PrimitivesPro.ThirdParty.P2T.TriangulationPoint::get_VertexCode()
extern void TriangulationPoint_get_VertexCode_mCC0DD33497EF4B66464E3DD331A130EFE5C256AB ();
// 0x0000003F System.Collections.Generic.List`1<PrimitivesPro.ThirdParty.P2T.DTSweepConstraint> PrimitivesPro.ThirdParty.P2T.TriangulationPoint::get_Edges()
extern void TriangulationPoint_get_Edges_m508DB95029BE1C1D677A3CDB55C453086B9E2EE4 ();
// 0x00000040 System.Void PrimitivesPro.ThirdParty.P2T.TriangulationPoint::set_Edges(System.Collections.Generic.List`1<PrimitivesPro.ThirdParty.P2T.DTSweepConstraint>)
extern void TriangulationPoint_set_Edges_mE8CBA698C533A1BC9DE202899C45731D3188A3B3 ();
// 0x00000041 System.Boolean PrimitivesPro.ThirdParty.P2T.TriangulationPoint::get_HasEdges()
extern void TriangulationPoint_get_HasEdges_m1C72029DD702D2159ADB3FB9C63CD54B0CB04C71 ();
// 0x00000042 System.Void PrimitivesPro.ThirdParty.P2T.TriangulationPoint::.ctor(System.Double,System.Double)
extern void TriangulationPoint__ctor_mCE3411925573AF432E94C34EFFE7E86F0C3B4113 ();
// 0x00000043 System.Void PrimitivesPro.ThirdParty.P2T.TriangulationPoint::.ctor(System.Double,System.Double,System.Double)
extern void TriangulationPoint__ctor_m0D812C57E8CD64696268D90600FA62F7D8B88CDD ();
// 0x00000044 System.String PrimitivesPro.ThirdParty.P2T.TriangulationPoint::ToString()
extern void TriangulationPoint_ToString_mAFCBBD8EC58575A36AA4F4746DC4DFCF3F37E032 ();
// 0x00000045 System.Int32 PrimitivesPro.ThirdParty.P2T.TriangulationPoint::GetHashCode()
extern void TriangulationPoint_GetHashCode_mC415499B9063B778E93F99574C95CFC8D2782493 ();
// 0x00000046 System.Boolean PrimitivesPro.ThirdParty.P2T.TriangulationPoint::Equals(System.Object)
extern void TriangulationPoint_Equals_m024917F3D0A0EA1EE0F42BDADBB2A89624953432 ();
// 0x00000047 System.UInt32 PrimitivesPro.ThirdParty.P2T.TriangulationPoint::CreateVertexCode(System.Double,System.Double,System.Double)
extern void TriangulationPoint_CreateVertexCode_m5409DED2525864711EB9A4166164E16348F969AF ();
// 0x00000048 System.Void PrimitivesPro.ThirdParty.P2T.TriangulationPoint::AddEdge(PrimitivesPro.ThirdParty.P2T.DTSweepConstraint)
extern void TriangulationPoint_AddEdge_m01A5642608CED5BE017C4CA175DD67CC92165ADC ();
// 0x00000049 System.Boolean PrimitivesPro.ThirdParty.P2T.TriangulationPoint::GetEdge(PrimitivesPro.ThirdParty.P2T.TriangulationPoint,PrimitivesPro.ThirdParty.P2T.DTSweepConstraint&)
extern void TriangulationPoint_GetEdge_m450280B566B60AD45C538188DD46C3DD600CC2AA ();
// 0x0000004A System.Void PrimitivesPro.ThirdParty.P2T.TriangulationPoint::.cctor()
extern void TriangulationPoint__cctor_mBC216B6DBEE711ACCD60F6995DA600E25D7F1A73 ();
// 0x0000004B System.Void PrimitivesPro.ThirdParty.P2T.PolygonPoint::.ctor(System.Double,System.Double)
extern void PolygonPoint__ctor_m989CE273B3EFFC1D232A64569480476F2BBBE6D7 ();
// 0x0000004C PrimitivesPro.ThirdParty.P2T.PolygonPoint PrimitivesPro.ThirdParty.P2T.PolygonPoint::get_Next()
extern void PolygonPoint_get_Next_m74CC34C59E388BB8650FE29D9320B8DF225E4A1F ();
// 0x0000004D System.Void PrimitivesPro.ThirdParty.P2T.PolygonPoint::set_Next(PrimitivesPro.ThirdParty.P2T.PolygonPoint)
extern void PolygonPoint_set_Next_mCCCD168C01DA0023A6BC039BBB01EE3C17291F18 ();
// 0x0000004E System.Void PrimitivesPro.ThirdParty.P2T.PolygonPoint::set_Previous(PrimitivesPro.ThirdParty.P2T.PolygonPoint)
extern void PolygonPoint_set_Previous_m86223679CE4AC6954FD2E3B74BBA732604799AF9 ();
// 0x0000004F System.Boolean PrimitivesPro.ThirdParty.P2T.MathUtil::AreValuesEqual(System.Double,System.Double)
extern void MathUtil_AreValuesEqual_mDC6A1425CB317266BD76EAE19436F3A66D877A0C ();
// 0x00000050 System.Boolean PrimitivesPro.ThirdParty.P2T.MathUtil::AreValuesEqual(System.Double,System.Double,System.Double)
extern void MathUtil_AreValuesEqual_mBDF75B6CEADBD3D76935E23F676D781C4110679E ();
// 0x00000051 System.Double PrimitivesPro.ThirdParty.P2T.MathUtil::RoundWithPrecision(System.Double,System.Double)
extern void MathUtil_RoundWithPrecision_mA14D4454AB15626A9176A7A16136794B1F55A2CC ();
// 0x00000052 System.UInt32 PrimitivesPro.ThirdParty.P2T.MathUtil::Jenkins32Hash(System.Byte[],System.UInt32)
extern void MathUtil_Jenkins32Hash_m4FC6BFF75B82C5B7263D09A2C71CC7BE3E212D7A ();
// 0x00000053 System.Void PrimitivesPro.ThirdParty.P2T.MathUtil::.cctor()
extern void MathUtil__cctor_m2DC318DFC8EC08D55BDD03D1CC5DFE8AC9584DED ();
// 0x00000054 System.Void PrimitivesPro.ThirdParty.P2T.Point2DEnumerator::.ctor(System.Collections.Generic.IList`1<PrimitivesPro.ThirdParty.P2T.Point2D>)
extern void Point2DEnumerator__ctor_mB4768F28D994C52B4397FE8C42D02C36555B226A ();
// 0x00000055 System.Boolean PrimitivesPro.ThirdParty.P2T.Point2DEnumerator::MoveNext()
extern void Point2DEnumerator_MoveNext_mD10601C858394AC65E79630113FD48329253B4DE ();
// 0x00000056 System.Void PrimitivesPro.ThirdParty.P2T.Point2DEnumerator::Reset()
extern void Point2DEnumerator_Reset_mEECB72A26DE52A729E0889479A7D5068440E2519 ();
// 0x00000057 System.Void PrimitivesPro.ThirdParty.P2T.Point2DEnumerator::System.IDisposable.Dispose()
extern void Point2DEnumerator_System_IDisposable_Dispose_m85DC61D28EA0DB78FD7DEA31B4A00AB7D23E4807 ();
// 0x00000058 System.Object PrimitivesPro.ThirdParty.P2T.Point2DEnumerator::System.Collections.IEnumerator.get_Current()
extern void Point2DEnumerator_System_Collections_IEnumerator_get_Current_m8506A2ADE3894AE5D4D1717F4738B155218F6B9D ();
// 0x00000059 PrimitivesPro.ThirdParty.P2T.Point2D PrimitivesPro.ThirdParty.P2T.Point2DEnumerator::get_Current()
extern void Point2DEnumerator_get_Current_m6FF3A7295C961F84FA15FD9C4E3B42D5B4B4FEE9 ();
// 0x0000005A PrimitivesPro.ThirdParty.P2T.FixedBitArray3 PrimitivesPro.ThirdParty.P2T.DelaunayTriangle::get_EdgeIsConstrained()
extern void DelaunayTriangle_get_EdgeIsConstrained_m43CDCB863F1AD4D0797A4CCC2C7534F56C5A5E05 ();
// 0x0000005B System.Boolean PrimitivesPro.ThirdParty.P2T.DelaunayTriangle::get_IsInterior()
extern void DelaunayTriangle_get_IsInterior_m21ABF3951BE04B7FB353B25DDB0380F14CEACAAB ();
// 0x0000005C System.Void PrimitivesPro.ThirdParty.P2T.DelaunayTriangle::set_IsInterior(System.Boolean)
extern void DelaunayTriangle_set_IsInterior_mAC40461F414D6C2CBA5B0F38B633E1E094B8C127 ();
// 0x0000005D System.Void PrimitivesPro.ThirdParty.P2T.DelaunayTriangle::.ctor(PrimitivesPro.ThirdParty.P2T.TriangulationPoint,PrimitivesPro.ThirdParty.P2T.TriangulationPoint,PrimitivesPro.ThirdParty.P2T.TriangulationPoint)
extern void DelaunayTriangle__ctor_m391BA3DF6547C24BC81DAC7893C77A25D1D56BBB ();
// 0x0000005E System.Int32 PrimitivesPro.ThirdParty.P2T.DelaunayTriangle::IndexOf(PrimitivesPro.ThirdParty.P2T.TriangulationPoint)
extern void DelaunayTriangle_IndexOf_m965BA829ED1857B3CFC4C76E574D4890170D0BF7 ();
// 0x0000005F System.Int32 PrimitivesPro.ThirdParty.P2T.DelaunayTriangle::IndexCCWFrom(PrimitivesPro.ThirdParty.P2T.TriangulationPoint)
extern void DelaunayTriangle_IndexCCWFrom_m0EF073F3D1ABBD375EA1BCFD60457B328EC02706 ();
// 0x00000060 System.Boolean PrimitivesPro.ThirdParty.P2T.DelaunayTriangle::Contains(PrimitivesPro.ThirdParty.P2T.TriangulationPoint)
extern void DelaunayTriangle_Contains_m0449465921A3CD4A4DA84FB87ECDA5A9D1E7DD2A ();
// 0x00000061 System.Void PrimitivesPro.ThirdParty.P2T.DelaunayTriangle::MarkNeighbor(PrimitivesPro.ThirdParty.P2T.TriangulationPoint,PrimitivesPro.ThirdParty.P2T.TriangulationPoint,PrimitivesPro.ThirdParty.P2T.DelaunayTriangle)
extern void DelaunayTriangle_MarkNeighbor_mD14498B563A956FAF2BF626298346FB01DA1909C ();
// 0x00000062 System.Void PrimitivesPro.ThirdParty.P2T.DelaunayTriangle::MarkNeighbor(PrimitivesPro.ThirdParty.P2T.DelaunayTriangle)
extern void DelaunayTriangle_MarkNeighbor_mEA89ACABF1529EA0F00BC25A55EA5652E33D3D42 ();
// 0x00000063 System.Void PrimitivesPro.ThirdParty.P2T.DelaunayTriangle::ClearNeighbors()
extern void DelaunayTriangle_ClearNeighbors_mDB9ED03657280B766E2BB284937ED6CF8ABDE56A ();
// 0x00000064 System.Void PrimitivesPro.ThirdParty.P2T.DelaunayTriangle::ClearNeighbor(PrimitivesPro.ThirdParty.P2T.DelaunayTriangle)
extern void DelaunayTriangle_ClearNeighbor_mB9FD553BB5C704D66BDC7BF011BFE326A3F6C83A ();
// 0x00000065 System.Void PrimitivesPro.ThirdParty.P2T.DelaunayTriangle::Clear()
extern void DelaunayTriangle_Clear_mE9982B9C967334C163613EDDDB374ADA0A9D5B55 ();
// 0x00000066 PrimitivesPro.ThirdParty.P2T.TriangulationPoint PrimitivesPro.ThirdParty.P2T.DelaunayTriangle::OppositePoint(PrimitivesPro.ThirdParty.P2T.DelaunayTriangle,PrimitivesPro.ThirdParty.P2T.TriangulationPoint)
extern void DelaunayTriangle_OppositePoint_m2B2EB7D75D1AB76E269CA58CA1F146B38C8ED87A ();
// 0x00000067 PrimitivesPro.ThirdParty.P2T.DelaunayTriangle PrimitivesPro.ThirdParty.P2T.DelaunayTriangle::NeighborCWFrom(PrimitivesPro.ThirdParty.P2T.TriangulationPoint)
extern void DelaunayTriangle_NeighborCWFrom_m70FEE1ECF0A83FDA895C7C6351486C39BA0456AA ();
// 0x00000068 PrimitivesPro.ThirdParty.P2T.DelaunayTriangle PrimitivesPro.ThirdParty.P2T.DelaunayTriangle::NeighborCCWFrom(PrimitivesPro.ThirdParty.P2T.TriangulationPoint)
extern void DelaunayTriangle_NeighborCCWFrom_mC56C9ECA1B9612D6CF4828101573E33B23025AA8 ();
// 0x00000069 PrimitivesPro.ThirdParty.P2T.DelaunayTriangle PrimitivesPro.ThirdParty.P2T.DelaunayTriangle::NeighborAcrossFrom(PrimitivesPro.ThirdParty.P2T.TriangulationPoint)
extern void DelaunayTriangle_NeighborAcrossFrom_m73B3C02C61A46A489B4051C199A7FF81110EA168 ();
// 0x0000006A PrimitivesPro.ThirdParty.P2T.TriangulationPoint PrimitivesPro.ThirdParty.P2T.DelaunayTriangle::PointCCWFrom(PrimitivesPro.ThirdParty.P2T.TriangulationPoint)
extern void DelaunayTriangle_PointCCWFrom_m86F9B9E1BE091816844F70CB17A559F12D5F5A3B ();
// 0x0000006B PrimitivesPro.ThirdParty.P2T.TriangulationPoint PrimitivesPro.ThirdParty.P2T.DelaunayTriangle::PointCWFrom(PrimitivesPro.ThirdParty.P2T.TriangulationPoint)
extern void DelaunayTriangle_PointCWFrom_mE1129FEFDBB77B48C94E1223B088AB2619D0A1A5 ();
// 0x0000006C System.Void PrimitivesPro.ThirdParty.P2T.DelaunayTriangle::RotateCW()
extern void DelaunayTriangle_RotateCW_m6FCD427D876B6B25D87269E1A4980FCD76B1D5DE ();
// 0x0000006D System.Void PrimitivesPro.ThirdParty.P2T.DelaunayTriangle::Legalize(PrimitivesPro.ThirdParty.P2T.TriangulationPoint,PrimitivesPro.ThirdParty.P2T.TriangulationPoint)
extern void DelaunayTriangle_Legalize_m72D55CEF1682FC0CC859F7119B716833E0D90BC3 ();
// 0x0000006E System.String PrimitivesPro.ThirdParty.P2T.DelaunayTriangle::ToString()
extern void DelaunayTriangle_ToString_m849BB6E5BA4CFAA42CFDBF5B25F0CBCAFC53B293 ();
// 0x0000006F System.Void PrimitivesPro.ThirdParty.P2T.DelaunayTriangle::MarkConstrainedEdge(System.Int32)
extern void DelaunayTriangle_MarkConstrainedEdge_m2D658606BDF79E9A537D385EFA8AB5D1D6572A49 ();
// 0x00000070 System.Void PrimitivesPro.ThirdParty.P2T.DelaunayTriangle::MarkConstrainedEdge(PrimitivesPro.ThirdParty.P2T.TriangulationPoint,PrimitivesPro.ThirdParty.P2T.TriangulationPoint)
extern void DelaunayTriangle_MarkConstrainedEdge_m18EE59D77B1810A49C701FE32D6D4434847FD019 ();
// 0x00000071 System.Int32 PrimitivesPro.ThirdParty.P2T.DelaunayTriangle::EdgeIndex(PrimitivesPro.ThirdParty.P2T.TriangulationPoint,PrimitivesPro.ThirdParty.P2T.TriangulationPoint)
extern void DelaunayTriangle_EdgeIndex_m0CB7D57E18D9B438493B6DFF7170D262468F2D91 ();
// 0x00000072 System.Boolean PrimitivesPro.ThirdParty.P2T.DelaunayTriangle::GetConstrainedEdgeCCW(PrimitivesPro.ThirdParty.P2T.TriangulationPoint)
extern void DelaunayTriangle_GetConstrainedEdgeCCW_m2745897B01D5ADB4E1A0F64BAD9ADD51804AA357 ();
// 0x00000073 System.Boolean PrimitivesPro.ThirdParty.P2T.DelaunayTriangle::GetConstrainedEdgeCW(PrimitivesPro.ThirdParty.P2T.TriangulationPoint)
extern void DelaunayTriangle_GetConstrainedEdgeCW_m930CC19491DAC8804945ACE1BE5D53DD932A4274 ();
// 0x00000074 System.Void PrimitivesPro.ThirdParty.P2T.DelaunayTriangle::SetConstrainedEdge(System.Int32,System.Boolean)
extern void DelaunayTriangle_SetConstrainedEdge_m4DFEC87E57ED5C3FD5B606655818197BD37089A9 ();
// 0x00000075 System.Void PrimitivesPro.ThirdParty.P2T.DelaunayTriangle::SetConstrainedEdgeCCW(PrimitivesPro.ThirdParty.P2T.TriangulationPoint,System.Boolean)
extern void DelaunayTriangle_SetConstrainedEdgeCCW_m927EED106119D52A2661BE145D0B31121BAF3E47 ();
// 0x00000076 System.Void PrimitivesPro.ThirdParty.P2T.DelaunayTriangle::SetConstrainedEdgeCW(PrimitivesPro.ThirdParty.P2T.TriangulationPoint,System.Boolean)
extern void DelaunayTriangle_SetConstrainedEdgeCW_m8C40C8F0F58195B1E320791F1D4D715EFD0ED843 ();
// 0x00000077 System.Void PrimitivesPro.ThirdParty.P2T.DelaunayTriangle::SetConstrainedEdgeAcross(PrimitivesPro.ThirdParty.P2T.TriangulationPoint,System.Boolean)
extern void DelaunayTriangle_SetConstrainedEdgeAcross_m4D8FD3EA097D1AD8DB30393FE055E01ABF0933A7 ();
// 0x00000078 System.Boolean PrimitivesPro.ThirdParty.P2T.DelaunayTriangle::GetDelaunayEdgeCCW(PrimitivesPro.ThirdParty.P2T.TriangulationPoint)
extern void DelaunayTriangle_GetDelaunayEdgeCCW_m751FB46AEB33A1CB1DA1F1C80BA98AC73BF6B37A ();
// 0x00000079 System.Boolean PrimitivesPro.ThirdParty.P2T.DelaunayTriangle::GetDelaunayEdgeCW(PrimitivesPro.ThirdParty.P2T.TriangulationPoint)
extern void DelaunayTriangle_GetDelaunayEdgeCW_mB93E19AEA3CEA2427D659CCB6C1A2145C09D011D ();
// 0x0000007A System.Void PrimitivesPro.ThirdParty.P2T.DelaunayTriangle::SetDelaunayEdgeCCW(PrimitivesPro.ThirdParty.P2T.TriangulationPoint,System.Boolean)
extern void DelaunayTriangle_SetDelaunayEdgeCCW_m417EC0CFD0BA2409E15BD868E5D4BD0746803F8E ();
// 0x0000007B System.Void PrimitivesPro.ThirdParty.P2T.DelaunayTriangle::SetDelaunayEdgeCW(PrimitivesPro.ThirdParty.P2T.TriangulationPoint,System.Boolean)
extern void DelaunayTriangle_SetDelaunayEdgeCW_m7A2EBF712028E9D61286A41D2121C4BB4B68F73A ();
// 0x0000007C System.Boolean PrimitivesPro.ThirdParty.P2T.DelaunayTriangle::GetEdge(System.Int32,PrimitivesPro.ThirdParty.P2T.DTSweepConstraint&)
extern void DelaunayTriangle_GetEdge_m09D9949B216BEAF817D943A3A4637DDFB80688C6 ();
// 0x0000007D System.Boolean PrimitivesPro.ThirdParty.P2T.DelaunayTriangle::GetEdgeCCW(PrimitivesPro.ThirdParty.P2T.TriangulationPoint,PrimitivesPro.ThirdParty.P2T.DTSweepConstraint&)
extern void DelaunayTriangle_GetEdgeCCW_m3D7390FCC753FD5FE5551C029012E92972DA66B1 ();
// 0x0000007E System.Void PrimitivesPro.ThirdParty.P2T.AdvancingFront::.ctor(PrimitivesPro.ThirdParty.P2T.AdvancingFrontNode,PrimitivesPro.ThirdParty.P2T.AdvancingFrontNode)
extern void AdvancingFront__ctor_m71D55C8CA4F8ECE281DE1FE1DACEDA41323D5454 ();
// 0x0000007F System.Void PrimitivesPro.ThirdParty.P2T.AdvancingFront::AddNode(PrimitivesPro.ThirdParty.P2T.AdvancingFrontNode)
extern void AdvancingFront_AddNode_mCF83AE886D4BECF3D3EF05854BAA36E6D1423EB2 ();
// 0x00000080 System.Void PrimitivesPro.ThirdParty.P2T.AdvancingFront::RemoveNode(PrimitivesPro.ThirdParty.P2T.AdvancingFrontNode)
extern void AdvancingFront_RemoveNode_m028BB13140232D399156CF22E602CB8F6863F08F ();
// 0x00000081 System.String PrimitivesPro.ThirdParty.P2T.AdvancingFront::ToString()
extern void AdvancingFront_ToString_m6A42EACA7715F4D65D0BC3944CBA9811654670A6 ();
// 0x00000082 PrimitivesPro.ThirdParty.P2T.AdvancingFrontNode PrimitivesPro.ThirdParty.P2T.AdvancingFront::FindSearchNode(System.Double)
extern void AdvancingFront_FindSearchNode_mA0946D85EB30ED9B6DFB9AAE082AF220049AB2CC ();
// 0x00000083 PrimitivesPro.ThirdParty.P2T.AdvancingFrontNode PrimitivesPro.ThirdParty.P2T.AdvancingFront::LocateNode(PrimitivesPro.ThirdParty.P2T.TriangulationPoint)
extern void AdvancingFront_LocateNode_mE9EC748AF627C3E0E5DF684228ED25E797DE8AAA ();
// 0x00000084 PrimitivesPro.ThirdParty.P2T.AdvancingFrontNode PrimitivesPro.ThirdParty.P2T.AdvancingFront::LocateNode(System.Double)
extern void AdvancingFront_LocateNode_m24006769077A098D4F2F3B624743993CAA630DBB ();
// 0x00000085 PrimitivesPro.ThirdParty.P2T.AdvancingFrontNode PrimitivesPro.ThirdParty.P2T.AdvancingFront::LocatePoint(PrimitivesPro.ThirdParty.P2T.TriangulationPoint)
extern void AdvancingFront_LocatePoint_mC4045400316995D48BB7FA719793C955E7A45E23 ();
// 0x00000086 System.Void PrimitivesPro.ThirdParty.P2T.AdvancingFrontNode::.ctor(PrimitivesPro.ThirdParty.P2T.TriangulationPoint)
extern void AdvancingFrontNode__ctor_m033871894C41844C1AF35A0900D0074CB60A06DF ();
// 0x00000087 System.Boolean PrimitivesPro.ThirdParty.P2T.AdvancingFrontNode::get_HasNext()
extern void AdvancingFrontNode_get_HasNext_mB0DB60E1801969F53F50AF643E3E41761F0D23D1 ();
// 0x00000088 System.Boolean PrimitivesPro.ThirdParty.P2T.AdvancingFrontNode::get_HasPrev()
extern void AdvancingFrontNode_get_HasPrev_m8A7D734850A4501F6AD11DD2FB1BDAD134D8ADCF ();
// 0x00000089 System.Void PrimitivesPro.ThirdParty.P2T.DTSweep::Triangulate(PrimitivesPro.ThirdParty.P2T.DTSweepContext)
extern void DTSweep_Triangulate_m29D2D3D267340BF6645544BDBCB7868F75599207 ();
// 0x0000008A System.Void PrimitivesPro.ThirdParty.P2T.DTSweep::Sweep(PrimitivesPro.ThirdParty.P2T.DTSweepContext)
extern void DTSweep_Sweep_mE5F98B4F041EDEFE69C64155CF492A6DFD20FE85 ();
// 0x0000008B System.Void PrimitivesPro.ThirdParty.P2T.DTSweep::FixupConstrainedEdges(PrimitivesPro.ThirdParty.P2T.DTSweepContext)
extern void DTSweep_FixupConstrainedEdges_m01890A267108A757004EB9B905F7363DE4308F44 ();
// 0x0000008C System.Void PrimitivesPro.ThirdParty.P2T.DTSweep::FinalizationConvexHull(PrimitivesPro.ThirdParty.P2T.DTSweepContext)
extern void DTSweep_FinalizationConvexHull_mE3F357D2FCB81B769CEC302232EECF0FF5FDEB35 ();
// 0x0000008D System.Void PrimitivesPro.ThirdParty.P2T.DTSweep::TurnAdvancingFrontConvex(PrimitivesPro.ThirdParty.P2T.DTSweepContext,PrimitivesPro.ThirdParty.P2T.AdvancingFrontNode,PrimitivesPro.ThirdParty.P2T.AdvancingFrontNode)
extern void DTSweep_TurnAdvancingFrontConvex_mF3EA49341B8E636656A15CE2BC41C07F288C3E82 ();
// 0x0000008E System.Void PrimitivesPro.ThirdParty.P2T.DTSweep::FinalizationPolygon(PrimitivesPro.ThirdParty.P2T.DTSweepContext)
extern void DTSweep_FinalizationPolygon_mB8700E3E3E3CEDB2F13B7CA412FCA74AAC2773AE ();
// 0x0000008F PrimitivesPro.ThirdParty.P2T.AdvancingFrontNode PrimitivesPro.ThirdParty.P2T.DTSweep::PointEvent(PrimitivesPro.ThirdParty.P2T.DTSweepContext,PrimitivesPro.ThirdParty.P2T.TriangulationPoint)
extern void DTSweep_PointEvent_m039B12E8B29ED919A2C2109FA47D6E7FB47B8BDF ();
// 0x00000090 PrimitivesPro.ThirdParty.P2T.AdvancingFrontNode PrimitivesPro.ThirdParty.P2T.DTSweep::NewFrontTriangle(PrimitivesPro.ThirdParty.P2T.DTSweepContext,PrimitivesPro.ThirdParty.P2T.TriangulationPoint,PrimitivesPro.ThirdParty.P2T.AdvancingFrontNode)
extern void DTSweep_NewFrontTriangle_m283E7A8F0F0566480EC5DB01383A77BF59900E7C ();
// 0x00000091 System.Void PrimitivesPro.ThirdParty.P2T.DTSweep::EdgeEvent(PrimitivesPro.ThirdParty.P2T.DTSweepContext,PrimitivesPro.ThirdParty.P2T.DTSweepConstraint,PrimitivesPro.ThirdParty.P2T.AdvancingFrontNode)
extern void DTSweep_EdgeEvent_m3D7F0B80DFD2C36523F3B8656A059989432843F3 ();
// 0x00000092 System.Void PrimitivesPro.ThirdParty.P2T.DTSweep::FillEdgeEvent(PrimitivesPro.ThirdParty.P2T.DTSweepContext,PrimitivesPro.ThirdParty.P2T.DTSweepConstraint,PrimitivesPro.ThirdParty.P2T.AdvancingFrontNode)
extern void DTSweep_FillEdgeEvent_mEC73F30FF1570E516E93BDE9E4C7B67636808550 ();
// 0x00000093 System.Void PrimitivesPro.ThirdParty.P2T.DTSweep::FillRightConcaveEdgeEvent(PrimitivesPro.ThirdParty.P2T.DTSweepContext,PrimitivesPro.ThirdParty.P2T.DTSweepConstraint,PrimitivesPro.ThirdParty.P2T.AdvancingFrontNode)
extern void DTSweep_FillRightConcaveEdgeEvent_m0D6423114C0920AD27EB22C742008EB54452ADEC ();
// 0x00000094 System.Void PrimitivesPro.ThirdParty.P2T.DTSweep::FillRightConvexEdgeEvent(PrimitivesPro.ThirdParty.P2T.DTSweepContext,PrimitivesPro.ThirdParty.P2T.DTSweepConstraint,PrimitivesPro.ThirdParty.P2T.AdvancingFrontNode)
extern void DTSweep_FillRightConvexEdgeEvent_mDF6C5F91010B7DB103E5F202E2A7249979DD7137 ();
// 0x00000095 System.Void PrimitivesPro.ThirdParty.P2T.DTSweep::FillRightBelowEdgeEvent(PrimitivesPro.ThirdParty.P2T.DTSweepContext,PrimitivesPro.ThirdParty.P2T.DTSweepConstraint,PrimitivesPro.ThirdParty.P2T.AdvancingFrontNode)
extern void DTSweep_FillRightBelowEdgeEvent_mAAB712E82FE66F686548BD610A0BEB1739734D20 ();
// 0x00000096 System.Void PrimitivesPro.ThirdParty.P2T.DTSweep::FillRightAboveEdgeEvent(PrimitivesPro.ThirdParty.P2T.DTSweepContext,PrimitivesPro.ThirdParty.P2T.DTSweepConstraint,PrimitivesPro.ThirdParty.P2T.AdvancingFrontNode)
extern void DTSweep_FillRightAboveEdgeEvent_mF8292855684EAD37B4BE49C342BEB2BB98C40B94 ();
// 0x00000097 System.Void PrimitivesPro.ThirdParty.P2T.DTSweep::FillLeftConvexEdgeEvent(PrimitivesPro.ThirdParty.P2T.DTSweepContext,PrimitivesPro.ThirdParty.P2T.DTSweepConstraint,PrimitivesPro.ThirdParty.P2T.AdvancingFrontNode)
extern void DTSweep_FillLeftConvexEdgeEvent_m00C19E23514044DA09B47E9237984DF20FCE454B ();
// 0x00000098 System.Void PrimitivesPro.ThirdParty.P2T.DTSweep::FillLeftConcaveEdgeEvent(PrimitivesPro.ThirdParty.P2T.DTSweepContext,PrimitivesPro.ThirdParty.P2T.DTSweepConstraint,PrimitivesPro.ThirdParty.P2T.AdvancingFrontNode)
extern void DTSweep_FillLeftConcaveEdgeEvent_m2F37BD80225923ED471442D03539483A1051A80C ();
// 0x00000099 System.Void PrimitivesPro.ThirdParty.P2T.DTSweep::FillLeftBelowEdgeEvent(PrimitivesPro.ThirdParty.P2T.DTSweepContext,PrimitivesPro.ThirdParty.P2T.DTSweepConstraint,PrimitivesPro.ThirdParty.P2T.AdvancingFrontNode)
extern void DTSweep_FillLeftBelowEdgeEvent_mB0EE24B5BC9ADEFC9BC77D8F3A30CDEC8D894CF1 ();
// 0x0000009A System.Void PrimitivesPro.ThirdParty.P2T.DTSweep::FillLeftAboveEdgeEvent(PrimitivesPro.ThirdParty.P2T.DTSweepContext,PrimitivesPro.ThirdParty.P2T.DTSweepConstraint,PrimitivesPro.ThirdParty.P2T.AdvancingFrontNode)
extern void DTSweep_FillLeftAboveEdgeEvent_m4E83C1DFFFA16851C8F73B09B456C0BD6D89D4B5 ();
// 0x0000009B System.Boolean PrimitivesPro.ThirdParty.P2T.DTSweep::IsEdgeSideOfTriangle(PrimitivesPro.ThirdParty.P2T.DelaunayTriangle,PrimitivesPro.ThirdParty.P2T.TriangulationPoint,PrimitivesPro.ThirdParty.P2T.TriangulationPoint)
extern void DTSweep_IsEdgeSideOfTriangle_mD21FBEB13B3523D3DC5F33842D299BA7F9B73364 ();
// 0x0000009C System.Void PrimitivesPro.ThirdParty.P2T.DTSweep::EdgeEvent(PrimitivesPro.ThirdParty.P2T.DTSweepContext,PrimitivesPro.ThirdParty.P2T.TriangulationPoint,PrimitivesPro.ThirdParty.P2T.TriangulationPoint,PrimitivesPro.ThirdParty.P2T.DelaunayTriangle,PrimitivesPro.ThirdParty.P2T.TriangulationPoint)
extern void DTSweep_EdgeEvent_m7ECBD96C68410BADCD13F21FE8FC3EBB0278E127 ();
// 0x0000009D System.Void PrimitivesPro.ThirdParty.P2T.DTSweep::FlipEdgeEvent(PrimitivesPro.ThirdParty.P2T.DTSweepContext,PrimitivesPro.ThirdParty.P2T.TriangulationPoint,PrimitivesPro.ThirdParty.P2T.TriangulationPoint,PrimitivesPro.ThirdParty.P2T.DelaunayTriangle,PrimitivesPro.ThirdParty.P2T.TriangulationPoint)
extern void DTSweep_FlipEdgeEvent_mEADA70526798643E75F3D82797853B115FBA8032 ();
// 0x0000009E System.Boolean PrimitivesPro.ThirdParty.P2T.DTSweep::NextFlipPoint(PrimitivesPro.ThirdParty.P2T.TriangulationPoint,PrimitivesPro.ThirdParty.P2T.TriangulationPoint,PrimitivesPro.ThirdParty.P2T.DelaunayTriangle,PrimitivesPro.ThirdParty.P2T.TriangulationPoint,PrimitivesPro.ThirdParty.P2T.TriangulationPoint&)
extern void DTSweep_NextFlipPoint_m16CADFA01A66D14E76D793E7DA75042AFA39E5D0 ();
// 0x0000009F PrimitivesPro.ThirdParty.P2T.DelaunayTriangle PrimitivesPro.ThirdParty.P2T.DTSweep::NextFlipTriangle(PrimitivesPro.ThirdParty.P2T.DTSweepContext,PrimitivesPro.ThirdParty.P2T.Orientation,PrimitivesPro.ThirdParty.P2T.DelaunayTriangle,PrimitivesPro.ThirdParty.P2T.DelaunayTriangle,PrimitivesPro.ThirdParty.P2T.TriangulationPoint,PrimitivesPro.ThirdParty.P2T.TriangulationPoint)
extern void DTSweep_NextFlipTriangle_m67892C4690C7EAB89C600EE00377BAC81896673F ();
// 0x000000A0 System.Void PrimitivesPro.ThirdParty.P2T.DTSweep::FlipScanEdgeEvent(PrimitivesPro.ThirdParty.P2T.DTSweepContext,PrimitivesPro.ThirdParty.P2T.TriangulationPoint,PrimitivesPro.ThirdParty.P2T.TriangulationPoint,PrimitivesPro.ThirdParty.P2T.DelaunayTriangle,PrimitivesPro.ThirdParty.P2T.DelaunayTriangle,PrimitivesPro.ThirdParty.P2T.TriangulationPoint)
extern void DTSweep_FlipScanEdgeEvent_m1C83A35EA86F4095B9F6F4DFC7A5326324C69AED ();
// 0x000000A1 System.Void PrimitivesPro.ThirdParty.P2T.DTSweep::FillAdvancingFront(PrimitivesPro.ThirdParty.P2T.DTSweepContext,PrimitivesPro.ThirdParty.P2T.AdvancingFrontNode)
extern void DTSweep_FillAdvancingFront_m5E77142CEA5630698BB933870D094208F79A34BF ();
// 0x000000A2 System.Void PrimitivesPro.ThirdParty.P2T.DTSweep::FillBasin(PrimitivesPro.ThirdParty.P2T.DTSweepContext,PrimitivesPro.ThirdParty.P2T.AdvancingFrontNode)
extern void DTSweep_FillBasin_m4CB3AEF7A8806D928E43E3602B047EA2DD36A5C8 ();
// 0x000000A3 System.Void PrimitivesPro.ThirdParty.P2T.DTSweep::FillBasinReq(PrimitivesPro.ThirdParty.P2T.DTSweepContext,PrimitivesPro.ThirdParty.P2T.AdvancingFrontNode)
extern void DTSweep_FillBasinReq_m882AD2B3A9BFA3A5925B39A2C859381B3B552A3B ();
// 0x000000A4 System.Boolean PrimitivesPro.ThirdParty.P2T.DTSweep::IsShallow(PrimitivesPro.ThirdParty.P2T.DTSweepContext,PrimitivesPro.ThirdParty.P2T.AdvancingFrontNode)
extern void DTSweep_IsShallow_mE4661D049823D56499CF290A0AD558E2ECA4F4AD ();
// 0x000000A5 System.Double PrimitivesPro.ThirdParty.P2T.DTSweep::HoleAngle(PrimitivesPro.ThirdParty.P2T.AdvancingFrontNode)
extern void DTSweep_HoleAngle_mCED0B6EB7235011601058721A33C9B9E83050AB9 ();
// 0x000000A6 System.Double PrimitivesPro.ThirdParty.P2T.DTSweep::BasinAngle(PrimitivesPro.ThirdParty.P2T.AdvancingFrontNode)
extern void DTSweep_BasinAngle_m3EA4A378CD8F2FE4F95A808DD09277D7B9EE32A9 ();
// 0x000000A7 System.Void PrimitivesPro.ThirdParty.P2T.DTSweep::Fill(PrimitivesPro.ThirdParty.P2T.DTSweepContext,PrimitivesPro.ThirdParty.P2T.AdvancingFrontNode)
extern void DTSweep_Fill_mA6F3C92740A6554212B28A687BD404B964FEC1E0 ();
// 0x000000A8 System.Boolean PrimitivesPro.ThirdParty.P2T.DTSweep::Legalize(PrimitivesPro.ThirdParty.P2T.DTSweepContext,PrimitivesPro.ThirdParty.P2T.DelaunayTriangle)
extern void DTSweep_Legalize_m8C9D043B85BD171F0FF5D85D21814B5AD3873ACA ();
// 0x000000A9 System.Void PrimitivesPro.ThirdParty.P2T.DTSweep::RotateTrianglePair(PrimitivesPro.ThirdParty.P2T.DelaunayTriangle,PrimitivesPro.ThirdParty.P2T.TriangulationPoint,PrimitivesPro.ThirdParty.P2T.DelaunayTriangle,PrimitivesPro.ThirdParty.P2T.TriangulationPoint)
extern void DTSweep_RotateTrianglePair_m50CBD22230D08F83E1B9A187A89E2B8E59A9691E ();
// 0x000000AA System.Void PrimitivesPro.ThirdParty.P2T.DTSweepBasin::.ctor()
extern void DTSweepBasin__ctor_mAE7F69D456F569B73F2A6632C8363EA9992C5ABF ();
// 0x000000AB System.Void PrimitivesPro.ThirdParty.P2T.Edge::.ctor()
extern void Edge__ctor_mFD68F79041ED5A55A9C87BBD7128C7CF1E0B95B3 ();
// 0x000000AC PrimitivesPro.ThirdParty.P2T.TriangulationPoint PrimitivesPro.ThirdParty.P2T.TriangulationConstraint::get_P()
extern void TriangulationConstraint_get_P_m855F7876A95994B46E8348A451FD5CCB48D9501F ();
// 0x000000AD PrimitivesPro.ThirdParty.P2T.TriangulationPoint PrimitivesPro.ThirdParty.P2T.TriangulationConstraint::get_Q()
extern void TriangulationConstraint_get_Q_mE468DF3F9AD48728D55748DAA858862E9F1790BF ();
// 0x000000AE System.Void PrimitivesPro.ThirdParty.P2T.TriangulationConstraint::set_Q(PrimitivesPro.ThirdParty.P2T.TriangulationPoint)
extern void TriangulationConstraint_set_Q_m50A5048475BA247138E794B11870F65E94C0FF85 ();
// 0x000000AF System.Void PrimitivesPro.ThirdParty.P2T.TriangulationConstraint::.ctor(PrimitivesPro.ThirdParty.P2T.TriangulationPoint,PrimitivesPro.ThirdParty.P2T.TriangulationPoint)
extern void TriangulationConstraint__ctor_m95E88E4EE39D44EC1DE440D192E070AB610C9B31 ();
// 0x000000B0 System.String PrimitivesPro.ThirdParty.P2T.TriangulationConstraint::ToString()
extern void TriangulationConstraint_ToString_m4083B70CF54890C77DCB8BE4E11EA7C996D6221C ();
// 0x000000B1 System.Void PrimitivesPro.ThirdParty.P2T.TriangulationConstraint::CalculateContraintCode()
extern void TriangulationConstraint_CalculateContraintCode_m19F423B34F3D3AB959E76CD88F1F4CDE79DF3513 ();
// 0x000000B2 System.UInt32 PrimitivesPro.ThirdParty.P2T.TriangulationConstraint::CalculateContraintCode(PrimitivesPro.ThirdParty.P2T.TriangulationPoint,PrimitivesPro.ThirdParty.P2T.TriangulationPoint)
extern void TriangulationConstraint_CalculateContraintCode_m961B106DBA1461FE443DD3C2691834FEEFC211B7 ();
// 0x000000B3 System.Void PrimitivesPro.ThirdParty.P2T.DTSweepConstraint::.ctor(PrimitivesPro.ThirdParty.P2T.TriangulationPoint,PrimitivesPro.ThirdParty.P2T.TriangulationPoint)
extern void DTSweepConstraint__ctor_m5029C27FE55BC067BAD4F2DF0AF671F33788CF13 ();
// 0x000000B4 PrimitivesPro.ThirdParty.P2T.TriangulationDebugContext PrimitivesPro.ThirdParty.P2T.TriangulationContext::get_DebugContext()
extern void TriangulationContext_get_DebugContext_m52C3B8438E5A82B206A7063AA7B9150891D59C8A ();
// 0x000000B5 PrimitivesPro.ThirdParty.P2T.TriangulationMode PrimitivesPro.ThirdParty.P2T.TriangulationContext::get_TriangulationMode()
extern void TriangulationContext_get_TriangulationMode_mA970183A9F2D38D6C76C6B5C5463E7A48CFB86D6 ();
// 0x000000B6 System.Void PrimitivesPro.ThirdParty.P2T.TriangulationContext::set_TriangulationMode(PrimitivesPro.ThirdParty.P2T.TriangulationMode)
extern void TriangulationContext_set_TriangulationMode_mCE1EC8AB1AC99408B76143E94D1A45A160B33A32 ();
// 0x000000B7 PrimitivesPro.ThirdParty.P2T.ITriangulatable PrimitivesPro.ThirdParty.P2T.TriangulationContext::get_Triangulatable()
extern void TriangulationContext_get_Triangulatable_mE0C4B9906E99A575E73680D782108F73A51E26E9 ();
// 0x000000B8 System.Void PrimitivesPro.ThirdParty.P2T.TriangulationContext::set_Triangulatable(PrimitivesPro.ThirdParty.P2T.ITriangulatable)
extern void TriangulationContext_set_Triangulatable_mD204D15468268C4498A236280AFC8E1BA25269D0 ();
// 0x000000B9 System.Int32 PrimitivesPro.ThirdParty.P2T.TriangulationContext::get_StepCount()
extern void TriangulationContext_get_StepCount_mC0A08AAACF640271453812FBA572E8333D0C16CD ();
// 0x000000BA System.Void PrimitivesPro.ThirdParty.P2T.TriangulationContext::set_StepCount(System.Int32)
extern void TriangulationContext_set_StepCount_m3556D8324E2775282E0F0F73D508E3C7FCA8395D ();
// 0x000000BB System.Void PrimitivesPro.ThirdParty.P2T.TriangulationContext::Done()
extern void TriangulationContext_Done_m03FA67533C4E89FE6EAD15C8D31E37B0287D9F67 ();
// 0x000000BC PrimitivesPro.ThirdParty.P2T.TriangulationAlgorithm PrimitivesPro.ThirdParty.P2T.TriangulationContext::get_Algorithm()
// 0x000000BD System.Void PrimitivesPro.ThirdParty.P2T.TriangulationContext::PrepareTriangulation(PrimitivesPro.ThirdParty.P2T.ITriangulatable)
extern void TriangulationContext_PrepareTriangulation_m020A14D85E763BEBE071792A2DF6AD5BF7164761 ();
// 0x000000BE PrimitivesPro.ThirdParty.P2T.TriangulationConstraint PrimitivesPro.ThirdParty.P2T.TriangulationContext::NewConstraint(PrimitivesPro.ThirdParty.P2T.TriangulationPoint,PrimitivesPro.ThirdParty.P2T.TriangulationPoint)
// 0x000000BF System.Void PrimitivesPro.ThirdParty.P2T.TriangulationContext::Update(System.String)
extern void TriangulationContext_Update_mA71FC16439998E72F5634A7A3B280CBED3EA9F8E ();
// 0x000000C0 System.Void PrimitivesPro.ThirdParty.P2T.TriangulationContext::Clear()
extern void TriangulationContext_Clear_m779BCF12782870D0FAC7EBC1FE7271229C529D3A ();
// 0x000000C1 System.Boolean PrimitivesPro.ThirdParty.P2T.TriangulationContext::get_IsDebugEnabled()
extern void TriangulationContext_get_IsDebugEnabled_mCD787517613DF64B91174EA4215E7296011E639B ();
// 0x000000C2 PrimitivesPro.ThirdParty.P2T.DTSweepDebugContext PrimitivesPro.ThirdParty.P2T.TriangulationContext::get_DTDebugContext()
extern void TriangulationContext_get_DTDebugContext_m99EC6D3A22EAC2521AAC9D4841377F8DBA94F1D5 ();
// 0x000000C3 System.Void PrimitivesPro.ThirdParty.P2T.TriangulationContext::.ctor()
extern void TriangulationContext__ctor_m531AF6F84B71E3D9FD10C3565D9D66726F7D6018 ();
// 0x000000C4 PrimitivesPro.ThirdParty.P2T.TriangulationPoint PrimitivesPro.ThirdParty.P2T.DTSweepContext::get_Head()
extern void DTSweepContext_get_Head_m0D22D5218F46539ABA49E99F873FA9ACCC7FA8DB ();
// 0x000000C5 System.Void PrimitivesPro.ThirdParty.P2T.DTSweepContext::set_Head(PrimitivesPro.ThirdParty.P2T.TriangulationPoint)
extern void DTSweepContext_set_Head_mA1683466FF06BC8FF5B4E179E7A93785A9BF06EE ();
// 0x000000C6 PrimitivesPro.ThirdParty.P2T.TriangulationPoint PrimitivesPro.ThirdParty.P2T.DTSweepContext::get_Tail()
extern void DTSweepContext_get_Tail_mF872F157EF07524F6DB1FA2875BC91B2F8EEA1D8 ();
// 0x000000C7 System.Void PrimitivesPro.ThirdParty.P2T.DTSweepContext::set_Tail(PrimitivesPro.ThirdParty.P2T.TriangulationPoint)
extern void DTSweepContext_set_Tail_mDB0B177857E6DD538C4D9C37F7642BCB4C88981E ();
// 0x000000C8 PrimitivesPro.ThirdParty.P2T.TriangulationAlgorithm PrimitivesPro.ThirdParty.P2T.DTSweepContext::get_Algorithm()
extern void DTSweepContext_get_Algorithm_mD6E03BC25D889B45F5183DDC5FE462E64BA52906 ();
// 0x000000C9 System.Void PrimitivesPro.ThirdParty.P2T.DTSweepContext::.ctor()
extern void DTSweepContext__ctor_m02C8A97C32D5B0398530598E84A51980E2460DC1 ();
// 0x000000CA System.Boolean PrimitivesPro.ThirdParty.P2T.DTSweepContext::get_IsDebugEnabled()
extern void DTSweepContext_get_IsDebugEnabled_m9358961553EEB2F9BC08DA2510F1A6D33AAFDB0A ();
// 0x000000CB System.Void PrimitivesPro.ThirdParty.P2T.DTSweepContext::RemoveFromList(PrimitivesPro.ThirdParty.P2T.DelaunayTriangle)
extern void DTSweepContext_RemoveFromList_m16CA1A4DF649218264003E1829C20AE566315465 ();
// 0x000000CC System.Void PrimitivesPro.ThirdParty.P2T.DTSweepContext::MeshClean(PrimitivesPro.ThirdParty.P2T.DelaunayTriangle)
extern void DTSweepContext_MeshClean_m7B305AFB2D89CDAF8C516DB564F5C4EC45666968 ();
// 0x000000CD System.Void PrimitivesPro.ThirdParty.P2T.DTSweepContext::MeshCleanReq(PrimitivesPro.ThirdParty.P2T.DelaunayTriangle)
extern void DTSweepContext_MeshCleanReq_m6E162634D0E67BDC5ABF6A6A4BB2046C2897F95E ();
// 0x000000CE System.Void PrimitivesPro.ThirdParty.P2T.DTSweepContext::Clear()
extern void DTSweepContext_Clear_mBD952966C84B9550332DF129BEF37387FE4205DF ();
// 0x000000CF System.Void PrimitivesPro.ThirdParty.P2T.DTSweepContext::AddNode(PrimitivesPro.ThirdParty.P2T.AdvancingFrontNode)
extern void DTSweepContext_AddNode_m53D02BEE664320D31F9283BF7677656778F2C82F ();
// 0x000000D0 System.Void PrimitivesPro.ThirdParty.P2T.DTSweepContext::RemoveNode(PrimitivesPro.ThirdParty.P2T.AdvancingFrontNode)
extern void DTSweepContext_RemoveNode_m130AD2D88A2B95197EF03E2DE2D050A53821BB3D ();
// 0x000000D1 PrimitivesPro.ThirdParty.P2T.AdvancingFrontNode PrimitivesPro.ThirdParty.P2T.DTSweepContext::LocateNode(PrimitivesPro.ThirdParty.P2T.TriangulationPoint)
extern void DTSweepContext_LocateNode_mE0B7AF6D0A54E2FBD830537072ADD2BF2E0612EA ();
// 0x000000D2 System.Void PrimitivesPro.ThirdParty.P2T.DTSweepContext::CreateAdvancingFront()
extern void DTSweepContext_CreateAdvancingFront_m825499992A24DD37D8C16094AB4ACEEFD1FB536B ();
// 0x000000D3 System.Void PrimitivesPro.ThirdParty.P2T.DTSweepContext::MapTriangleToNodes(PrimitivesPro.ThirdParty.P2T.DelaunayTriangle)
extern void DTSweepContext_MapTriangleToNodes_m08C05F6B768546D42E70A30D686B95B44C411CD7 ();
// 0x000000D4 System.Void PrimitivesPro.ThirdParty.P2T.DTSweepContext::PrepareTriangulation(PrimitivesPro.ThirdParty.P2T.ITriangulatable)
extern void DTSweepContext_PrepareTriangulation_mBFAD1789B07902FBBE1309E357A43582BF27C0CC ();
// 0x000000D5 System.Void PrimitivesPro.ThirdParty.P2T.DTSweepContext::FinalizeTriangulation()
extern void DTSweepContext_FinalizeTriangulation_mFD876B17EDCC555ABE5205E4C4958EDEB344F74A ();
// 0x000000D6 PrimitivesPro.ThirdParty.P2T.TriangulationConstraint PrimitivesPro.ThirdParty.P2T.DTSweepContext::NewConstraint(PrimitivesPro.ThirdParty.P2T.TriangulationPoint,PrimitivesPro.ThirdParty.P2T.TriangulationPoint)
extern void DTSweepContext_NewConstraint_m08429DA9F2A4FA06C14335FD4EE0EF7DB1944024 ();
// 0x000000D7 System.Void PrimitivesPro.ThirdParty.P2T.TriangulationDebugContext::Clear()
// 0x000000D8 System.Void PrimitivesPro.ThirdParty.P2T.DTSweepDebugContext::set_PrimaryTriangle(PrimitivesPro.ThirdParty.P2T.DelaunayTriangle)
extern void DTSweepDebugContext_set_PrimaryTriangle_mFA01409AEDBC8C4BE215F624F2D441E16B244E46 ();
// 0x000000D9 System.Void PrimitivesPro.ThirdParty.P2T.DTSweepDebugContext::set_SecondaryTriangle(PrimitivesPro.ThirdParty.P2T.DelaunayTriangle)
extern void DTSweepDebugContext_set_SecondaryTriangle_m0C9EA30C8E6F3AF557BD4A20985F067461602873 ();
// 0x000000DA System.Void PrimitivesPro.ThirdParty.P2T.DTSweepDebugContext::set_ActivePoint(PrimitivesPro.ThirdParty.P2T.TriangulationPoint)
extern void DTSweepDebugContext_set_ActivePoint_m697C295B279A96D36DDD2D81B28899DD2F2E391D ();
// 0x000000DB System.Void PrimitivesPro.ThirdParty.P2T.DTSweepDebugContext::set_ActiveNode(PrimitivesPro.ThirdParty.P2T.AdvancingFrontNode)
extern void DTSweepDebugContext_set_ActiveNode_mE7DD509697261B4F3E1D0CD02E20FCF3C92CA9BA ();
// 0x000000DC System.Void PrimitivesPro.ThirdParty.P2T.DTSweepDebugContext::set_ActiveConstraint(PrimitivesPro.ThirdParty.P2T.DTSweepConstraint)
extern void DTSweepDebugContext_set_ActiveConstraint_m0FF58DDE9A5807C32DFE3A06E10B8649063FFDD6 ();
// 0x000000DD System.Void PrimitivesPro.ThirdParty.P2T.DTSweepDebugContext::Clear()
extern void DTSweepDebugContext_Clear_mCF03DE9ECFB8D645C79F87B2C3551657770C2FBC ();
// 0x000000DE System.Void PrimitivesPro.ThirdParty.P2T.DTSweepEdgeEvent::.ctor()
extern void DTSweepEdgeEvent__ctor_mFBC207838654D0D22ADDD6A3D9FB6CD3E9833621 ();
// 0x000000DF System.Int32 PrimitivesPro.ThirdParty.P2T.DTSweepPointComparator::Compare(PrimitivesPro.ThirdParty.P2T.TriangulationPoint,PrimitivesPro.ThirdParty.P2T.TriangulationPoint)
extern void DTSweepPointComparator_Compare_mABAE83AE9ED56D95FDA4F1241E8E1012B7403C38 ();
// 0x000000E0 System.Void PrimitivesPro.ThirdParty.P2T.DTSweepPointComparator::.ctor()
extern void DTSweepPointComparator__ctor_m5580646DB2C3516CFF85FCA4AEBD5DDBE89EE054 ();
// 0x000000E1 System.Void PrimitivesPro.ThirdParty.P2T.PointOnEdgeException::.ctor(System.String,PrimitivesPro.ThirdParty.P2T.TriangulationPoint,PrimitivesPro.ThirdParty.P2T.TriangulationPoint,PrimitivesPro.ThirdParty.P2T.TriangulationPoint)
extern void PointOnEdgeException__ctor_m139D2F9C5AD1F975E3117B860292518F0767CCFA ();
// 0x000000E2 System.Void PrimitivesPro.ThirdParty.P2T.TriangulationPointEnumerator::.ctor(System.Collections.Generic.IList`1<PrimitivesPro.ThirdParty.P2T.Point2D>)
extern void TriangulationPointEnumerator__ctor_m6356F56A94E9CEC4EEE9CE2676BB62DE668FB249 ();
// 0x000000E3 System.Boolean PrimitivesPro.ThirdParty.P2T.TriangulationPointEnumerator::MoveNext()
extern void TriangulationPointEnumerator_MoveNext_mD234860436DEAF12F236865333C42D3A3648C5F1 ();
// 0x000000E4 System.Void PrimitivesPro.ThirdParty.P2T.TriangulationPointEnumerator::Reset()
extern void TriangulationPointEnumerator_Reset_m02488EF07DBC762296DDE8981F12531B63D8DBD4 ();
// 0x000000E5 System.Void PrimitivesPro.ThirdParty.P2T.TriangulationPointEnumerator::System.IDisposable.Dispose()
extern void TriangulationPointEnumerator_System_IDisposable_Dispose_mE75B9060EFD9A53C7DE7CB6C3B831D40D23C66CE ();
// 0x000000E6 System.Object PrimitivesPro.ThirdParty.P2T.TriangulationPointEnumerator::System.Collections.IEnumerator.get_Current()
extern void TriangulationPointEnumerator_System_Collections_IEnumerator_get_Current_mC546D525472F7B46139E443789A29CE511069615 ();
// 0x000000E7 PrimitivesPro.ThirdParty.P2T.TriangulationPoint PrimitivesPro.ThirdParty.P2T.TriangulationPointEnumerator::get_Current()
extern void TriangulationPointEnumerator_get_Current_mDDD0C37CE8657EE089BDF324BEC23555D0EBCCA6 ();
// 0x000000E8 System.Boolean PrimitivesPro.ThirdParty.P2T.TriangulationUtil::SmartIncircle(PrimitivesPro.ThirdParty.P2T.Point2D,PrimitivesPro.ThirdParty.P2T.Point2D,PrimitivesPro.ThirdParty.P2T.Point2D,PrimitivesPro.ThirdParty.P2T.Point2D)
extern void TriangulationUtil_SmartIncircle_m9D4C2FCA6878A8F0D0ECFC07B88E0262B9BE62EC ();
// 0x000000E9 System.Boolean PrimitivesPro.ThirdParty.P2T.TriangulationUtil::InScanArea(PrimitivesPro.ThirdParty.P2T.Point2D,PrimitivesPro.ThirdParty.P2T.Point2D,PrimitivesPro.ThirdParty.P2T.Point2D,PrimitivesPro.ThirdParty.P2T.Point2D)
extern void TriangulationUtil_InScanArea_mBBEB51B3DB9674E381096EA656A89AA570A53EF2 ();
// 0x000000EA PrimitivesPro.ThirdParty.P2T.Orientation PrimitivesPro.ThirdParty.P2T.TriangulationUtil::Orient2d(PrimitivesPro.ThirdParty.P2T.Point2D,PrimitivesPro.ThirdParty.P2T.Point2D,PrimitivesPro.ThirdParty.P2T.Point2D)
extern void TriangulationUtil_Orient2d_m9C42D7C8B832F913548302E729AF1642F731F546 ();
// 0x000000EB T PrimitivesPro.ThirdParty.P2T.FixedArray3`1::get_Item(System.Int32)
// 0x000000EC System.Void PrimitivesPro.ThirdParty.P2T.FixedArray3`1::set_Item(System.Int32,T)
// 0x000000ED System.Boolean PrimitivesPro.ThirdParty.P2T.FixedArray3`1::Contains(T)
// 0x000000EE System.Int32 PrimitivesPro.ThirdParty.P2T.FixedArray3`1::IndexOf(T)
// 0x000000EF System.Void PrimitivesPro.ThirdParty.P2T.FixedArray3`1::Clear()
// 0x000000F0 System.Collections.Generic.IEnumerable`1<T> PrimitivesPro.ThirdParty.P2T.FixedArray3`1::Enumerate()
// 0x000000F1 System.Collections.Generic.IEnumerator`1<T> PrimitivesPro.ThirdParty.P2T.FixedArray3`1::GetEnumerator()
// 0x000000F2 System.Collections.IEnumerator PrimitivesPro.ThirdParty.P2T.FixedArray3`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000F3 System.Collections.Generic.IEnumerator`1<T> PrimitivesPro.ThirdParty.P2T.FixedArray3`1_<Enumerate>d__0::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x000000F4 System.Collections.IEnumerator PrimitivesPro.ThirdParty.P2T.FixedArray3`1_<Enumerate>d__0::System.Collections.IEnumerable.GetEnumerator()
// 0x000000F5 System.Boolean PrimitivesPro.ThirdParty.P2T.FixedArray3`1_<Enumerate>d__0::MoveNext()
// 0x000000F6 T PrimitivesPro.ThirdParty.P2T.FixedArray3`1_<Enumerate>d__0::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x000000F7 System.Void PrimitivesPro.ThirdParty.P2T.FixedArray3`1_<Enumerate>d__0::System.Collections.IEnumerator.Reset()
// 0x000000F8 System.Void PrimitivesPro.ThirdParty.P2T.FixedArray3`1_<Enumerate>d__0::System.IDisposable.Dispose()
// 0x000000F9 System.Object PrimitivesPro.ThirdParty.P2T.FixedArray3`1_<Enumerate>d__0::System.Collections.IEnumerator.get_Current()
// 0x000000FA System.Void PrimitivesPro.ThirdParty.P2T.FixedArray3`1_<Enumerate>d__0::.ctor(System.Int32)
// 0x000000FB System.Boolean PrimitivesPro.ThirdParty.P2T.FixedBitArray3::get_Item(System.Int32)
extern void FixedBitArray3_get_Item_mB0EF59FA53ADE2AC72296FCE81E76A7FC18FFE95_AdjustorThunk ();
// 0x000000FC System.Void PrimitivesPro.ThirdParty.P2T.FixedBitArray3::set_Item(System.Int32,System.Boolean)
extern void FixedBitArray3_set_Item_m4FBF5FA8359BC6B74CEA6EACA66C41FC8F179E74_AdjustorThunk ();
// 0x000000FD System.Void PrimitivesPro.ThirdParty.P2T.FixedBitArray3::Clear()
extern void FixedBitArray3_Clear_m0910FBAB47CC90AE271299065800BE6FCD3D8AC9_AdjustorThunk ();
// 0x000000FE System.Collections.Generic.IEnumerable`1<System.Boolean> PrimitivesPro.ThirdParty.P2T.FixedBitArray3::Enumerate()
extern void FixedBitArray3_Enumerate_m8B0A508F19518F738FBE8B8BD2996E5B89E7B6A3_AdjustorThunk ();
// 0x000000FF System.Collections.Generic.IEnumerator`1<System.Boolean> PrimitivesPro.ThirdParty.P2T.FixedBitArray3::GetEnumerator()
extern void FixedBitArray3_GetEnumerator_m97856CA0A9D2498EE4FEA55CB3A1F12C5A44FA9B_AdjustorThunk ();
// 0x00000100 System.Collections.IEnumerator PrimitivesPro.ThirdParty.P2T.FixedBitArray3::System.Collections.IEnumerable.GetEnumerator()
extern void FixedBitArray3_System_Collections_IEnumerable_GetEnumerator_mABCED7BFEBCED2198E355F9B65D85F2C0C12EB9F_AdjustorThunk ();
// 0x00000101 System.Collections.Generic.IEnumerator`1<System.Boolean> PrimitivesPro.ThirdParty.P2T.FixedBitArray3_<Enumerate>d__0::System.Collections.Generic.IEnumerable<System.Boolean>.GetEnumerator()
extern void U3CEnumerateU3Ed__0_System_Collections_Generic_IEnumerableU3CSystem_BooleanU3E_GetEnumerator_m219A4943563FB9094037ECBD6A167704EA5FD765 ();
// 0x00000102 System.Collections.IEnumerator PrimitivesPro.ThirdParty.P2T.FixedBitArray3_<Enumerate>d__0::System.Collections.IEnumerable.GetEnumerator()
extern void U3CEnumerateU3Ed__0_System_Collections_IEnumerable_GetEnumerator_mEA103268B0CFF78CBF2198EC2EC711E76EDAEFAD ();
// 0x00000103 System.Boolean PrimitivesPro.ThirdParty.P2T.FixedBitArray3_<Enumerate>d__0::MoveNext()
extern void U3CEnumerateU3Ed__0_MoveNext_m2C48D5D611461D487BDA61063021CF289F7AE4D8 ();
// 0x00000104 System.Boolean PrimitivesPro.ThirdParty.P2T.FixedBitArray3_<Enumerate>d__0::System.Collections.Generic.IEnumerator<System.Boolean>.get_Current()
extern void U3CEnumerateU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_BooleanU3E_get_Current_mEA0D176E885859B13BF8693FE39E2137747EE953 ();
// 0x00000105 System.Void PrimitivesPro.ThirdParty.P2T.FixedBitArray3_<Enumerate>d__0::System.Collections.IEnumerator.Reset()
extern void U3CEnumerateU3Ed__0_System_Collections_IEnumerator_Reset_mA71EE57C1053730467EBEF7230A9AD7531EDD8CA ();
// 0x00000106 System.Void PrimitivesPro.ThirdParty.P2T.FixedBitArray3_<Enumerate>d__0::System.IDisposable.Dispose()
extern void U3CEnumerateU3Ed__0_System_IDisposable_Dispose_m4D522DC4C712F3555289C2FA3727AA0E41113D78 ();
// 0x00000107 System.Object PrimitivesPro.ThirdParty.P2T.FixedBitArray3_<Enumerate>d__0::System.Collections.IEnumerator.get_Current()
extern void U3CEnumerateU3Ed__0_System_Collections_IEnumerator_get_Current_mF27FB09FFABA15C0235914BBB0052893D77B238E ();
// 0x00000108 System.Void PrimitivesPro.ThirdParty.P2T.FixedBitArray3_<Enumerate>d__0::.ctor(System.Int32)
extern void U3CEnumerateU3Ed__0__ctor_mA0C036603D50423BA0803033BC54DFD7E602266E ();
// 0x00000109 System.Double PrimitivesPro.ThirdParty.P2T.Rect2D::get_MinX()
extern void Rect2D_get_MinX_m387B5536226E141F5FFA57D0B14232EA8B320E45 ();
// 0x0000010A System.Void PrimitivesPro.ThirdParty.P2T.Rect2D::set_MinX(System.Double)
extern void Rect2D_set_MinX_m8899671643B4CA1E0B64A2CACAD74B7B96523639 ();
// 0x0000010B System.Double PrimitivesPro.ThirdParty.P2T.Rect2D::get_MaxX()
extern void Rect2D_get_MaxX_m9AE619EEC2D7CFD5399C833AA1A8DF2A29E93ECD ();
// 0x0000010C System.Void PrimitivesPro.ThirdParty.P2T.Rect2D::set_MaxX(System.Double)
extern void Rect2D_set_MaxX_m792681F5328A64A00B3988ABC3F2F9E0EB53839E ();
// 0x0000010D System.Double PrimitivesPro.ThirdParty.P2T.Rect2D::get_MinY()
extern void Rect2D_get_MinY_mD3AE8474DDAB73C96C4D66A788C528B54E2AA529 ();
// 0x0000010E System.Void PrimitivesPro.ThirdParty.P2T.Rect2D::set_MinY(System.Double)
extern void Rect2D_set_MinY_mE123EF1D3417EB47966C9CB28EECBBF30ECB1BF8 ();
// 0x0000010F System.Double PrimitivesPro.ThirdParty.P2T.Rect2D::get_MaxY()
extern void Rect2D_get_MaxY_mCF6888323E0FC879FDA537E1B86A51E6A299039D ();
// 0x00000110 System.Void PrimitivesPro.ThirdParty.P2T.Rect2D::set_MaxY(System.Double)
extern void Rect2D_set_MaxY_mACA55DAC573FF7C8FD80D21CF45BCE56CF17DC51 ();
// 0x00000111 System.Double PrimitivesPro.ThirdParty.P2T.Rect2D::get_Left()
extern void Rect2D_get_Left_mFA8E2621FCB1723C6EDE9E0D687954557E76719A ();
// 0x00000112 System.Double PrimitivesPro.ThirdParty.P2T.Rect2D::get_Right()
extern void Rect2D_get_Right_m5F3FB5C1790ED5C1742B04FFEA8273AD86E31D83 ();
// 0x00000113 System.Double PrimitivesPro.ThirdParty.P2T.Rect2D::get_Top()
extern void Rect2D_get_Top_m6B628428B0DD50015B7F9C43390B46307CF012D9 ();
// 0x00000114 System.Double PrimitivesPro.ThirdParty.P2T.Rect2D::get_Bottom()
extern void Rect2D_get_Bottom_m143264A90F7367CEFA66A6D33759F71FB1AA4359 ();
// 0x00000115 System.Double PrimitivesPro.ThirdParty.P2T.Rect2D::get_Width()
extern void Rect2D_get_Width_m3ABBF7BAFA0CF4EF77677B27917A8FF450C5B689 ();
// 0x00000116 System.Double PrimitivesPro.ThirdParty.P2T.Rect2D::get_Height()
extern void Rect2D_get_Height_m4C858AEFCE3355485A85106C120D8895AD4216CE ();
// 0x00000117 System.Void PrimitivesPro.ThirdParty.P2T.Rect2D::.ctor()
extern void Rect2D__ctor_m80F3AD55F25FD5DC2CF2003A66B5971B94790432 ();
// 0x00000118 System.Int32 PrimitivesPro.ThirdParty.P2T.Rect2D::GetHashCode()
extern void Rect2D_GetHashCode_m81FB1CAFD260DFD1BA9BEE0BE92F8A4186420FB8 ();
// 0x00000119 System.Boolean PrimitivesPro.ThirdParty.P2T.Rect2D::Equals(System.Object)
extern void Rect2D_Equals_mF30DF1CDA45194B9606D28F7BF1EF028C24D2E31 ();
// 0x0000011A System.Boolean PrimitivesPro.ThirdParty.P2T.Rect2D::Equals(PrimitivesPro.ThirdParty.P2T.Rect2D)
extern void Rect2D_Equals_mDB7CB345CE65D03A850A2E1FF2663F0705A42A3D ();
// 0x0000011B System.Boolean PrimitivesPro.ThirdParty.P2T.Rect2D::Equals(PrimitivesPro.ThirdParty.P2T.Rect2D,System.Double)
extern void Rect2D_Equals_mA201AB750EF8D08C5A5CD256431243673704072C ();
// 0x0000011C System.Void PrimitivesPro.ThirdParty.P2T.Rect2D::Clear()
extern void Rect2D_Clear_mF26F5C64F8B2EEF7021AFBFB972DC143ED398278 ();
// 0x0000011D System.Void PrimitivesPro.ThirdParty.P2T.Rect2D::AddPoint(PrimitivesPro.ThirdParty.P2T.Point2D)
extern void Rect2D_AddPoint_m29B91BA4AFD36F376976E559EB8C35DD36730B28 ();
static Il2CppMethodPointer s_methodPointers[285] = 
{
	P2T_Triangulate_m2265E2FB4CFB0D5546346B1FDA7677B9D09F5E7B,
	P2T_CreateContext_m98D7C59587E9955306AA0EAFFAC18E95D60A6111,
	P2T_Triangulate_m56D486DD7FBCEDACEE70F362F091EE012FDA1D19,
	P2T_Triangulate_m9770BA5BA667B7D58C1253E110E76F4F4FDD6DD6,
	Point2DList_get_WindingOrder_m847E214B845C646EF7D229A919C8DEFE209DEB28,
	Point2DList_get_Item_m587E1CFAA4DDC871A7DC5D4F6F4F546228183AC1,
	Point2DList_set_Item_mD3702E57C7E3196880A12B5574069D100BDBFB79,
	Point2DList_get_Count_mA55165B5410DA07EA0B7CEFDAB022C653D61C8AC,
	Point2DList_get_IsReadOnly_mA7AC7EE2AA779418A0822BFEC2468C30BA839EE0,
	Point2DList__ctor_mF36BD99069E350E7E979A371F87A38E8C0D7287A,
	Point2DList_ToString_m1FC8770A4C9EE929F6BB8D0F0A5CAE801BAB5C57,
	Point2DList_System_Collections_IEnumerable_GetEnumerator_m9FD67933A634939438FD057641E72EC281659BC0,
	Point2DList_System_Collections_Generic_IEnumerableU3CPrimitivesPro_ThirdParty_P2T_Point2DU3E_GetEnumerator_m8D32FD791B82DAFE4EA591EF71EB6D9C78BB5D56,
	Point2DList_Clear_m160050443EC8A5ABFA0407DDEEA6FEA73603366F,
	Point2DList_IndexOf_m98A802D7C7FE3A8B31298CCFF8FD85DE3C3FC5A3,
	Point2DList_Add_m54B3C19571876AFE974EB3D69F8F82503F2DD98F,
	Point2DList_Add_m28B76A0449872AD25EFB39BC4CE6C46062A1020D,
	Point2DList_Insert_m69D1941D1846E3B0A5E9B8A634CBB7A37D084461,
	Point2DList_Remove_mE29EF7058DC9F5C576D8CD21ECA1536B1276E74C,
	Point2DList_RemoveAt_m8E8FF6C69E6769E369A9296B4BD74AA192DA49C4,
	Point2DList_Contains_mDB1A6AAF6F21133BE7D46B53B44F679CB4808B97,
	Point2DList_CopyTo_m70EC8A9DB00BBA04F9DF82455171C9B021B84B3E,
	Point2DList_CalculateBounds_m1D92DD47F0B60555E945ABC89D705E6E3CAF5AA6,
	Point2DList_CalculateEpsilon_m2AF4DE318D977DFE6A71BE6F37DC7A6010CC6EA6,
	Point2DList_CalculateWindingOrder_mB09CBDF4F024A46AECDAD89DAD2D80EF9A6BF568,
	Point2DList_GetSignedArea_m5DE1276773C4CE759D9090CD2EDDB368DF1C0657,
	Point2DList__cctor_mD49010C5D792101676D05212E90F683E84BAE9EC,
	NULL,
	NULL,
	NULL,
	NULL,
	Polygon_get_Triangles_mADFE36338A7A73E84F569982AEF15D69B80D592A,
	Polygon_get_TriangulationMode_m6E8E18B5060EBA99707D4C93041C9CE449838968,
	Polygon_get_Item_m6AB8811BAFA88F6F6372D1A7C1E52B59AED6B401,
	Polygon_set_Item_m8BA39DE0F9815993F056D2769A63B9CD09927622,
	Polygon__ctor_m41CC45797BE5018C5FE4D8EFCD78FFF9BB04BDC7,
	Polygon_System_Collections_Generic_IEnumerableU3CPrimitivesPro_ThirdParty_P2T_TriangulationPointU3E_GetEnumerator_m975A4E825027A62584079E20BD8C319D9D0BEADE,
	Polygon_IndexOf_m3A098F2506CEAD6DB7CF1F77CA6AB0A214C61E49,
	Polygon_Add_mD82493AA2218FCF1D22227D3CEE8345E6CEFC374,
	Polygon_Add_m67E8CF7034FC15DA0C755D3730A1CBAB71D49D10,
	Polygon_Add_m2CF2A9F4A37C16EFBC3729523A5DEC1402A67EF5,
	Polygon_AddRange_mD58133E337FA6C8D71B3BFB0D344E562EF3D3402,
	Polygon_Insert_m608054C6FD476F4A7F643B2685F3435436C5CD7A,
	Polygon_Remove_m140634EC0628DCE5D212085D23BC065229467DFD,
	Polygon_Contains_m49E2AFC71DCA6CEDBE44FD7DD20404E63A06B6C5,
	Polygon_CopyTo_mCC114FA7E6C0C697E5A67349FBEBBE584A369171,
	Polygon_AddHole_m509B7FC04488F6C09161EB94813E726091860B1C,
	Polygon_AddTriangle_m88213EC573B49503F1E6E120C3E1BCD8A1E5A9E1,
	Polygon_AddTriangles_m0749DDBC31BB532C20A546735521CFC458771C48,
	Polygon_Prepare_m7DB72852FA3F380E06402FE5F2E337073CF99B10,
	Point2D_get_X_mDA911B972ADD96890B6FEB380023B8F09C873E10,
	Point2D_get_Y_mFCAB6009E3D429D049837FFAC5599B781F14C87D,
	Point2D__ctor_m741E4A62812BBA5CBEC7FC71267341B9D1463A98,
	Point2D_ToString_mE2C8E8D18BF164965F65C22D1F89075132D383A9,
	Point2D_GetHashCode_mABA36DB7C5F9D1BE4A87781A11AC12FA10711A6D,
	Point2D_Equals_m953CB3C2A1285F07A28D30FA491FD16C32B38415,
	Point2D_Equals_m3D2CC40F1C83127B1C19759D8B387B2EDF0EB7F6,
	Point2D_Equals_mC7D83B9CC5ED24DC537758C51EAA2B625D9B205F,
	Point2D_CompareTo_mE7F4D52472AAC615E9E4046CF010B0949A7CC6CD,
	TriangulationPoint_get_X_mD30E9BDBDA9AFEA926A45DF666824687CADCB0FB,
	TriangulationPoint_get_Y_mD25900A541DB899FD060C2F1D836928C251AB4EC,
	TriangulationPoint_get_VertexCode_mCC0DD33497EF4B66464E3DD331A130EFE5C256AB,
	TriangulationPoint_get_Edges_m508DB95029BE1C1D677A3CDB55C453086B9E2EE4,
	TriangulationPoint_set_Edges_mE8CBA698C533A1BC9DE202899C45731D3188A3B3,
	TriangulationPoint_get_HasEdges_m1C72029DD702D2159ADB3FB9C63CD54B0CB04C71,
	TriangulationPoint__ctor_mCE3411925573AF432E94C34EFFE7E86F0C3B4113,
	TriangulationPoint__ctor_m0D812C57E8CD64696268D90600FA62F7D8B88CDD,
	TriangulationPoint_ToString_mAFCBBD8EC58575A36AA4F4746DC4DFCF3F37E032,
	TriangulationPoint_GetHashCode_mC415499B9063B778E93F99574C95CFC8D2782493,
	TriangulationPoint_Equals_m024917F3D0A0EA1EE0F42BDADBB2A89624953432,
	TriangulationPoint_CreateVertexCode_m5409DED2525864711EB9A4166164E16348F969AF,
	TriangulationPoint_AddEdge_m01A5642608CED5BE017C4CA175DD67CC92165ADC,
	TriangulationPoint_GetEdge_m450280B566B60AD45C538188DD46C3DD600CC2AA,
	TriangulationPoint__cctor_mBC216B6DBEE711ACCD60F6995DA600E25D7F1A73,
	PolygonPoint__ctor_m989CE273B3EFFC1D232A64569480476F2BBBE6D7,
	PolygonPoint_get_Next_m74CC34C59E388BB8650FE29D9320B8DF225E4A1F,
	PolygonPoint_set_Next_mCCCD168C01DA0023A6BC039BBB01EE3C17291F18,
	PolygonPoint_set_Previous_m86223679CE4AC6954FD2E3B74BBA732604799AF9,
	MathUtil_AreValuesEqual_mDC6A1425CB317266BD76EAE19436F3A66D877A0C,
	MathUtil_AreValuesEqual_mBDF75B6CEADBD3D76935E23F676D781C4110679E,
	MathUtil_RoundWithPrecision_mA14D4454AB15626A9176A7A16136794B1F55A2CC,
	MathUtil_Jenkins32Hash_m4FC6BFF75B82C5B7263D09A2C71CC7BE3E212D7A,
	MathUtil__cctor_m2DC318DFC8EC08D55BDD03D1CC5DFE8AC9584DED,
	Point2DEnumerator__ctor_mB4768F28D994C52B4397FE8C42D02C36555B226A,
	Point2DEnumerator_MoveNext_mD10601C858394AC65E79630113FD48329253B4DE,
	Point2DEnumerator_Reset_mEECB72A26DE52A729E0889479A7D5068440E2519,
	Point2DEnumerator_System_IDisposable_Dispose_m85DC61D28EA0DB78FD7DEA31B4A00AB7D23E4807,
	Point2DEnumerator_System_Collections_IEnumerator_get_Current_m8506A2ADE3894AE5D4D1717F4738B155218F6B9D,
	Point2DEnumerator_get_Current_m6FF3A7295C961F84FA15FD9C4E3B42D5B4B4FEE9,
	DelaunayTriangle_get_EdgeIsConstrained_m43CDCB863F1AD4D0797A4CCC2C7534F56C5A5E05,
	DelaunayTriangle_get_IsInterior_m21ABF3951BE04B7FB353B25DDB0380F14CEACAAB,
	DelaunayTriangle_set_IsInterior_mAC40461F414D6C2CBA5B0F38B633E1E094B8C127,
	DelaunayTriangle__ctor_m391BA3DF6547C24BC81DAC7893C77A25D1D56BBB,
	DelaunayTriangle_IndexOf_m965BA829ED1857B3CFC4C76E574D4890170D0BF7,
	DelaunayTriangle_IndexCCWFrom_m0EF073F3D1ABBD375EA1BCFD60457B328EC02706,
	DelaunayTriangle_Contains_m0449465921A3CD4A4DA84FB87ECDA5A9D1E7DD2A,
	DelaunayTriangle_MarkNeighbor_mD14498B563A956FAF2BF626298346FB01DA1909C,
	DelaunayTriangle_MarkNeighbor_mEA89ACABF1529EA0F00BC25A55EA5652E33D3D42,
	DelaunayTriangle_ClearNeighbors_mDB9ED03657280B766E2BB284937ED6CF8ABDE56A,
	DelaunayTriangle_ClearNeighbor_mB9FD553BB5C704D66BDC7BF011BFE326A3F6C83A,
	DelaunayTriangle_Clear_mE9982B9C967334C163613EDDDB374ADA0A9D5B55,
	DelaunayTriangle_OppositePoint_m2B2EB7D75D1AB76E269CA58CA1F146B38C8ED87A,
	DelaunayTriangle_NeighborCWFrom_m70FEE1ECF0A83FDA895C7C6351486C39BA0456AA,
	DelaunayTriangle_NeighborCCWFrom_mC56C9ECA1B9612D6CF4828101573E33B23025AA8,
	DelaunayTriangle_NeighborAcrossFrom_m73B3C02C61A46A489B4051C199A7FF81110EA168,
	DelaunayTriangle_PointCCWFrom_m86F9B9E1BE091816844F70CB17A559F12D5F5A3B,
	DelaunayTriangle_PointCWFrom_mE1129FEFDBB77B48C94E1223B088AB2619D0A1A5,
	DelaunayTriangle_RotateCW_m6FCD427D876B6B25D87269E1A4980FCD76B1D5DE,
	DelaunayTriangle_Legalize_m72D55CEF1682FC0CC859F7119B716833E0D90BC3,
	DelaunayTriangle_ToString_m849BB6E5BA4CFAA42CFDBF5B25F0CBCAFC53B293,
	DelaunayTriangle_MarkConstrainedEdge_m2D658606BDF79E9A537D385EFA8AB5D1D6572A49,
	DelaunayTriangle_MarkConstrainedEdge_m18EE59D77B1810A49C701FE32D6D4434847FD019,
	DelaunayTriangle_EdgeIndex_m0CB7D57E18D9B438493B6DFF7170D262468F2D91,
	DelaunayTriangle_GetConstrainedEdgeCCW_m2745897B01D5ADB4E1A0F64BAD9ADD51804AA357,
	DelaunayTriangle_GetConstrainedEdgeCW_m930CC19491DAC8804945ACE1BE5D53DD932A4274,
	DelaunayTriangle_SetConstrainedEdge_m4DFEC87E57ED5C3FD5B606655818197BD37089A9,
	DelaunayTriangle_SetConstrainedEdgeCCW_m927EED106119D52A2661BE145D0B31121BAF3E47,
	DelaunayTriangle_SetConstrainedEdgeCW_m8C40C8F0F58195B1E320791F1D4D715EFD0ED843,
	DelaunayTriangle_SetConstrainedEdgeAcross_m4D8FD3EA097D1AD8DB30393FE055E01ABF0933A7,
	DelaunayTriangle_GetDelaunayEdgeCCW_m751FB46AEB33A1CB1DA1F1C80BA98AC73BF6B37A,
	DelaunayTriangle_GetDelaunayEdgeCW_mB93E19AEA3CEA2427D659CCB6C1A2145C09D011D,
	DelaunayTriangle_SetDelaunayEdgeCCW_m417EC0CFD0BA2409E15BD868E5D4BD0746803F8E,
	DelaunayTriangle_SetDelaunayEdgeCW_m7A2EBF712028E9D61286A41D2121C4BB4B68F73A,
	DelaunayTriangle_GetEdge_m09D9949B216BEAF817D943A3A4637DDFB80688C6,
	DelaunayTriangle_GetEdgeCCW_m3D7390FCC753FD5FE5551C029012E92972DA66B1,
	AdvancingFront__ctor_m71D55C8CA4F8ECE281DE1FE1DACEDA41323D5454,
	AdvancingFront_AddNode_mCF83AE886D4BECF3D3EF05854BAA36E6D1423EB2,
	AdvancingFront_RemoveNode_m028BB13140232D399156CF22E602CB8F6863F08F,
	AdvancingFront_ToString_m6A42EACA7715F4D65D0BC3944CBA9811654670A6,
	AdvancingFront_FindSearchNode_mA0946D85EB30ED9B6DFB9AAE082AF220049AB2CC,
	AdvancingFront_LocateNode_mE9EC748AF627C3E0E5DF684228ED25E797DE8AAA,
	AdvancingFront_LocateNode_m24006769077A098D4F2F3B624743993CAA630DBB,
	AdvancingFront_LocatePoint_mC4045400316995D48BB7FA719793C955E7A45E23,
	AdvancingFrontNode__ctor_m033871894C41844C1AF35A0900D0074CB60A06DF,
	AdvancingFrontNode_get_HasNext_mB0DB60E1801969F53F50AF643E3E41761F0D23D1,
	AdvancingFrontNode_get_HasPrev_m8A7D734850A4501F6AD11DD2FB1BDAD134D8ADCF,
	DTSweep_Triangulate_m29D2D3D267340BF6645544BDBCB7868F75599207,
	DTSweep_Sweep_mE5F98B4F041EDEFE69C64155CF492A6DFD20FE85,
	DTSweep_FixupConstrainedEdges_m01890A267108A757004EB9B905F7363DE4308F44,
	DTSweep_FinalizationConvexHull_mE3F357D2FCB81B769CEC302232EECF0FF5FDEB35,
	DTSweep_TurnAdvancingFrontConvex_mF3EA49341B8E636656A15CE2BC41C07F288C3E82,
	DTSweep_FinalizationPolygon_mB8700E3E3E3CEDB2F13B7CA412FCA74AAC2773AE,
	DTSweep_PointEvent_m039B12E8B29ED919A2C2109FA47D6E7FB47B8BDF,
	DTSweep_NewFrontTriangle_m283E7A8F0F0566480EC5DB01383A77BF59900E7C,
	DTSweep_EdgeEvent_m3D7F0B80DFD2C36523F3B8656A059989432843F3,
	DTSweep_FillEdgeEvent_mEC73F30FF1570E516E93BDE9E4C7B67636808550,
	DTSweep_FillRightConcaveEdgeEvent_m0D6423114C0920AD27EB22C742008EB54452ADEC,
	DTSweep_FillRightConvexEdgeEvent_mDF6C5F91010B7DB103E5F202E2A7249979DD7137,
	DTSweep_FillRightBelowEdgeEvent_mAAB712E82FE66F686548BD610A0BEB1739734D20,
	DTSweep_FillRightAboveEdgeEvent_mF8292855684EAD37B4BE49C342BEB2BB98C40B94,
	DTSweep_FillLeftConvexEdgeEvent_m00C19E23514044DA09B47E9237984DF20FCE454B,
	DTSweep_FillLeftConcaveEdgeEvent_m2F37BD80225923ED471442D03539483A1051A80C,
	DTSweep_FillLeftBelowEdgeEvent_mB0EE24B5BC9ADEFC9BC77D8F3A30CDEC8D894CF1,
	DTSweep_FillLeftAboveEdgeEvent_m4E83C1DFFFA16851C8F73B09B456C0BD6D89D4B5,
	DTSweep_IsEdgeSideOfTriangle_mD21FBEB13B3523D3DC5F33842D299BA7F9B73364,
	DTSweep_EdgeEvent_m7ECBD96C68410BADCD13F21FE8FC3EBB0278E127,
	DTSweep_FlipEdgeEvent_mEADA70526798643E75F3D82797853B115FBA8032,
	DTSweep_NextFlipPoint_m16CADFA01A66D14E76D793E7DA75042AFA39E5D0,
	DTSweep_NextFlipTriangle_m67892C4690C7EAB89C600EE00377BAC81896673F,
	DTSweep_FlipScanEdgeEvent_m1C83A35EA86F4095B9F6F4DFC7A5326324C69AED,
	DTSweep_FillAdvancingFront_m5E77142CEA5630698BB933870D094208F79A34BF,
	DTSweep_FillBasin_m4CB3AEF7A8806D928E43E3602B047EA2DD36A5C8,
	DTSweep_FillBasinReq_m882AD2B3A9BFA3A5925B39A2C859381B3B552A3B,
	DTSweep_IsShallow_mE4661D049823D56499CF290A0AD558E2ECA4F4AD,
	DTSweep_HoleAngle_mCED0B6EB7235011601058721A33C9B9E83050AB9,
	DTSweep_BasinAngle_m3EA4A378CD8F2FE4F95A808DD09277D7B9EE32A9,
	DTSweep_Fill_mA6F3C92740A6554212B28A687BD404B964FEC1E0,
	DTSweep_Legalize_m8C9D043B85BD171F0FF5D85D21814B5AD3873ACA,
	DTSweep_RotateTrianglePair_m50CBD22230D08F83E1B9A187A89E2B8E59A9691E,
	DTSweepBasin__ctor_mAE7F69D456F569B73F2A6632C8363EA9992C5ABF,
	Edge__ctor_mFD68F79041ED5A55A9C87BBD7128C7CF1E0B95B3,
	TriangulationConstraint_get_P_m855F7876A95994B46E8348A451FD5CCB48D9501F,
	TriangulationConstraint_get_Q_mE468DF3F9AD48728D55748DAA858862E9F1790BF,
	TriangulationConstraint_set_Q_m50A5048475BA247138E794B11870F65E94C0FF85,
	TriangulationConstraint__ctor_m95E88E4EE39D44EC1DE440D192E070AB610C9B31,
	TriangulationConstraint_ToString_m4083B70CF54890C77DCB8BE4E11EA7C996D6221C,
	TriangulationConstraint_CalculateContraintCode_m19F423B34F3D3AB959E76CD88F1F4CDE79DF3513,
	TriangulationConstraint_CalculateContraintCode_m961B106DBA1461FE443DD3C2691834FEEFC211B7,
	DTSweepConstraint__ctor_m5029C27FE55BC067BAD4F2DF0AF671F33788CF13,
	TriangulationContext_get_DebugContext_m52C3B8438E5A82B206A7063AA7B9150891D59C8A,
	TriangulationContext_get_TriangulationMode_mA970183A9F2D38D6C76C6B5C5463E7A48CFB86D6,
	TriangulationContext_set_TriangulationMode_mCE1EC8AB1AC99408B76143E94D1A45A160B33A32,
	TriangulationContext_get_Triangulatable_mE0C4B9906E99A575E73680D782108F73A51E26E9,
	TriangulationContext_set_Triangulatable_mD204D15468268C4498A236280AFC8E1BA25269D0,
	TriangulationContext_get_StepCount_mC0A08AAACF640271453812FBA572E8333D0C16CD,
	TriangulationContext_set_StepCount_m3556D8324E2775282E0F0F73D508E3C7FCA8395D,
	TriangulationContext_Done_m03FA67533C4E89FE6EAD15C8D31E37B0287D9F67,
	NULL,
	TriangulationContext_PrepareTriangulation_m020A14D85E763BEBE071792A2DF6AD5BF7164761,
	NULL,
	TriangulationContext_Update_mA71FC16439998E72F5634A7A3B280CBED3EA9F8E,
	TriangulationContext_Clear_m779BCF12782870D0FAC7EBC1FE7271229C529D3A,
	TriangulationContext_get_IsDebugEnabled_mCD787517613DF64B91174EA4215E7296011E639B,
	TriangulationContext_get_DTDebugContext_m99EC6D3A22EAC2521AAC9D4841377F8DBA94F1D5,
	TriangulationContext__ctor_m531AF6F84B71E3D9FD10C3565D9D66726F7D6018,
	DTSweepContext_get_Head_m0D22D5218F46539ABA49E99F873FA9ACCC7FA8DB,
	DTSweepContext_set_Head_mA1683466FF06BC8FF5B4E179E7A93785A9BF06EE,
	DTSweepContext_get_Tail_mF872F157EF07524F6DB1FA2875BC91B2F8EEA1D8,
	DTSweepContext_set_Tail_mDB0B177857E6DD538C4D9C37F7642BCB4C88981E,
	DTSweepContext_get_Algorithm_mD6E03BC25D889B45F5183DDC5FE462E64BA52906,
	DTSweepContext__ctor_m02C8A97C32D5B0398530598E84A51980E2460DC1,
	DTSweepContext_get_IsDebugEnabled_m9358961553EEB2F9BC08DA2510F1A6D33AAFDB0A,
	DTSweepContext_RemoveFromList_m16CA1A4DF649218264003E1829C20AE566315465,
	DTSweepContext_MeshClean_m7B305AFB2D89CDAF8C516DB564F5C4EC45666968,
	DTSweepContext_MeshCleanReq_m6E162634D0E67BDC5ABF6A6A4BB2046C2897F95E,
	DTSweepContext_Clear_mBD952966C84B9550332DF129BEF37387FE4205DF,
	DTSweepContext_AddNode_m53D02BEE664320D31F9283BF7677656778F2C82F,
	DTSweepContext_RemoveNode_m130AD2D88A2B95197EF03E2DE2D050A53821BB3D,
	DTSweepContext_LocateNode_mE0B7AF6D0A54E2FBD830537072ADD2BF2E0612EA,
	DTSweepContext_CreateAdvancingFront_m825499992A24DD37D8C16094AB4ACEEFD1FB536B,
	DTSweepContext_MapTriangleToNodes_m08C05F6B768546D42E70A30D686B95B44C411CD7,
	DTSweepContext_PrepareTriangulation_mBFAD1789B07902FBBE1309E357A43582BF27C0CC,
	DTSweepContext_FinalizeTriangulation_mFD876B17EDCC555ABE5205E4C4958EDEB344F74A,
	DTSweepContext_NewConstraint_m08429DA9F2A4FA06C14335FD4EE0EF7DB1944024,
	NULL,
	DTSweepDebugContext_set_PrimaryTriangle_mFA01409AEDBC8C4BE215F624F2D441E16B244E46,
	DTSweepDebugContext_set_SecondaryTriangle_m0C9EA30C8E6F3AF557BD4A20985F067461602873,
	DTSweepDebugContext_set_ActivePoint_m697C295B279A96D36DDD2D81B28899DD2F2E391D,
	DTSweepDebugContext_set_ActiveNode_mE7DD509697261B4F3E1D0CD02E20FCF3C92CA9BA,
	DTSweepDebugContext_set_ActiveConstraint_m0FF58DDE9A5807C32DFE3A06E10B8649063FFDD6,
	DTSweepDebugContext_Clear_mCF03DE9ECFB8D645C79F87B2C3551657770C2FBC,
	DTSweepEdgeEvent__ctor_mFBC207838654D0D22ADDD6A3D9FB6CD3E9833621,
	DTSweepPointComparator_Compare_mABAE83AE9ED56D95FDA4F1241E8E1012B7403C38,
	DTSweepPointComparator__ctor_m5580646DB2C3516CFF85FCA4AEBD5DDBE89EE054,
	PointOnEdgeException__ctor_m139D2F9C5AD1F975E3117B860292518F0767CCFA,
	TriangulationPointEnumerator__ctor_m6356F56A94E9CEC4EEE9CE2676BB62DE668FB249,
	TriangulationPointEnumerator_MoveNext_mD234860436DEAF12F236865333C42D3A3648C5F1,
	TriangulationPointEnumerator_Reset_m02488EF07DBC762296DDE8981F12531B63D8DBD4,
	TriangulationPointEnumerator_System_IDisposable_Dispose_mE75B9060EFD9A53C7DE7CB6C3B831D40D23C66CE,
	TriangulationPointEnumerator_System_Collections_IEnumerator_get_Current_mC546D525472F7B46139E443789A29CE511069615,
	TriangulationPointEnumerator_get_Current_mDDD0C37CE8657EE089BDF324BEC23555D0EBCCA6,
	TriangulationUtil_SmartIncircle_m9D4C2FCA6878A8F0D0ECFC07B88E0262B9BE62EC,
	TriangulationUtil_InScanArea_mBBEB51B3DB9674E381096EA656A89AA570A53EF2,
	TriangulationUtil_Orient2d_m9C42D7C8B832F913548302E729AF1642F731F546,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	FixedBitArray3_get_Item_mB0EF59FA53ADE2AC72296FCE81E76A7FC18FFE95_AdjustorThunk,
	FixedBitArray3_set_Item_m4FBF5FA8359BC6B74CEA6EACA66C41FC8F179E74_AdjustorThunk,
	FixedBitArray3_Clear_m0910FBAB47CC90AE271299065800BE6FCD3D8AC9_AdjustorThunk,
	FixedBitArray3_Enumerate_m8B0A508F19518F738FBE8B8BD2996E5B89E7B6A3_AdjustorThunk,
	FixedBitArray3_GetEnumerator_m97856CA0A9D2498EE4FEA55CB3A1F12C5A44FA9B_AdjustorThunk,
	FixedBitArray3_System_Collections_IEnumerable_GetEnumerator_mABCED7BFEBCED2198E355F9B65D85F2C0C12EB9F_AdjustorThunk,
	U3CEnumerateU3Ed__0_System_Collections_Generic_IEnumerableU3CSystem_BooleanU3E_GetEnumerator_m219A4943563FB9094037ECBD6A167704EA5FD765,
	U3CEnumerateU3Ed__0_System_Collections_IEnumerable_GetEnumerator_mEA103268B0CFF78CBF2198EC2EC711E76EDAEFAD,
	U3CEnumerateU3Ed__0_MoveNext_m2C48D5D611461D487BDA61063021CF289F7AE4D8,
	U3CEnumerateU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_BooleanU3E_get_Current_mEA0D176E885859B13BF8693FE39E2137747EE953,
	U3CEnumerateU3Ed__0_System_Collections_IEnumerator_Reset_mA71EE57C1053730467EBEF7230A9AD7531EDD8CA,
	U3CEnumerateU3Ed__0_System_IDisposable_Dispose_m4D522DC4C712F3555289C2FA3727AA0E41113D78,
	U3CEnumerateU3Ed__0_System_Collections_IEnumerator_get_Current_mF27FB09FFABA15C0235914BBB0052893D77B238E,
	U3CEnumerateU3Ed__0__ctor_mA0C036603D50423BA0803033BC54DFD7E602266E,
	Rect2D_get_MinX_m387B5536226E141F5FFA57D0B14232EA8B320E45,
	Rect2D_set_MinX_m8899671643B4CA1E0B64A2CACAD74B7B96523639,
	Rect2D_get_MaxX_m9AE619EEC2D7CFD5399C833AA1A8DF2A29E93ECD,
	Rect2D_set_MaxX_m792681F5328A64A00B3988ABC3F2F9E0EB53839E,
	Rect2D_get_MinY_mD3AE8474DDAB73C96C4D66A788C528B54E2AA529,
	Rect2D_set_MinY_mE123EF1D3417EB47966C9CB28EECBBF30ECB1BF8,
	Rect2D_get_MaxY_mCF6888323E0FC879FDA537E1B86A51E6A299039D,
	Rect2D_set_MaxY_mACA55DAC573FF7C8FD80D21CF45BCE56CF17DC51,
	Rect2D_get_Left_mFA8E2621FCB1723C6EDE9E0D687954557E76719A,
	Rect2D_get_Right_m5F3FB5C1790ED5C1742B04FFEA8273AD86E31D83,
	Rect2D_get_Top_m6B628428B0DD50015B7F9C43390B46307CF012D9,
	Rect2D_get_Bottom_m143264A90F7367CEFA66A6D33759F71FB1AA4359,
	Rect2D_get_Width_m3ABBF7BAFA0CF4EF77677B27917A8FF450C5B689,
	Rect2D_get_Height_m4C858AEFCE3355485A85106C120D8895AD4216CE,
	Rect2D__ctor_m80F3AD55F25FD5DC2CF2003A66B5971B94790432,
	Rect2D_GetHashCode_m81FB1CAFD260DFD1BA9BEE0BE92F8A4186420FB8,
	Rect2D_Equals_mF30DF1CDA45194B9606D28F7BF1EF028C24D2E31,
	Rect2D_Equals_mDB7CB345CE65D03A850A2E1FF2663F0705A42A3D,
	Rect2D_Equals_mA201AB750EF8D08C5A5CD256431243673704072C,
	Rect2D_Clear_mF26F5C64F8B2EEF7021AFBFB972DC143ED398278,
	Rect2D_AddPoint_m29B91BA4AFD36F376976E559EB8C35DD36730B28,
};
static const int32_t s_InvokerIndices[285] = 
{
	111,
	43,
	519,
	111,
	10,
	34,
	62,
	10,
	102,
	23,
	14,
	14,
	14,
	23,
	104,
	26,
	299,
	62,
	9,
	32,
	9,
	124,
	23,
	394,
	10,
	394,
	3,
	10,
	26,
	26,
	26,
	14,
	10,
	34,
	62,
	26,
	14,
	104,
	26,
	26,
	299,
	124,
	62,
	9,
	9,
	124,
	26,
	26,
	26,
	26,
	394,
	394,
	1893,
	14,
	10,
	9,
	9,
	1894,
	104,
	394,
	394,
	10,
	14,
	26,
	102,
	1893,
	1895,
	14,
	10,
	9,
	1896,
	26,
	1897,
	3,
	1893,
	14,
	26,
	26,
	1898,
	1899,
	371,
	171,
	3,
	26,
	102,
	23,
	23,
	14,
	14,
	1900,
	102,
	31,
	156,
	104,
	104,
	9,
	156,
	26,
	23,
	26,
	23,
	101,
	28,
	28,
	28,
	28,
	28,
	23,
	27,
	14,
	32,
	27,
	41,
	9,
	9,
	577,
	390,
	390,
	390,
	9,
	9,
	390,
	390,
	1901,
	1897,
	27,
	26,
	26,
	14,
	607,
	28,
	607,
	28,
	26,
	102,
	102,
	111,
	111,
	111,
	111,
	144,
	111,
	1,
	2,
	144,
	144,
	144,
	144,
	144,
	144,
	144,
	144,
	144,
	144,
	298,
	1902,
	1902,
	1903,
	165,
	1904,
	122,
	122,
	122,
	99,
	302,
	302,
	122,
	99,
	1905,
	23,
	23,
	14,
	14,
	26,
	27,
	14,
	23,
	126,
	27,
	14,
	10,
	32,
	14,
	26,
	10,
	32,
	23,
	10,
	26,
	101,
	26,
	23,
	102,
	14,
	23,
	14,
	26,
	14,
	26,
	10,
	23,
	102,
	26,
	26,
	26,
	23,
	26,
	26,
	28,
	23,
	26,
	26,
	23,
	101,
	23,
	26,
	26,
	26,
	26,
	26,
	23,
	23,
	41,
	23,
	377,
	26,
	102,
	23,
	23,
	14,
	14,
	1906,
	1906,
	136,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	30,
	577,
	23,
	14,
	14,
	14,
	14,
	14,
	102,
	102,
	23,
	23,
	14,
	32,
	394,
	276,
	394,
	276,
	394,
	276,
	394,
	276,
	394,
	394,
	394,
	394,
	394,
	394,
	23,
	10,
	9,
	9,
	1894,
	23,
	26,
};
static const Il2CppTokenRangePair s_rgctxIndices[2] = 
{
	{ 0x02000020, { 0, 7 } },
	{ 0x02000021, { 7, 5 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[12] = 
{
	{ (Il2CppRGCTXDataType)3, 15384 },
	{ (Il2CppRGCTXDataType)2, 17782 },
	{ (Il2CppRGCTXDataType)2, 20473 },
	{ (Il2CppRGCTXDataType)3, 15385 },
	{ (Il2CppRGCTXDataType)3, 15386 },
	{ (Il2CppRGCTXDataType)2, 17783 },
	{ (Il2CppRGCTXDataType)3, 15387 },
	{ (Il2CppRGCTXDataType)2, 20474 },
	{ (Il2CppRGCTXDataType)3, 15388 },
	{ (Il2CppRGCTXDataType)3, 15389 },
	{ (Il2CppRGCTXDataType)3, 15390 },
	{ (Il2CppRGCTXDataType)2, 17789 },
};
extern const Il2CppCodeGenModule g_P2TCodeGenModule;
const Il2CppCodeGenModule g_P2TCodeGenModule = 
{
	"P2T.dll",
	285,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	2,
	s_rgctxIndices,
	12,
	s_rgctxValues,
	NULL,
};
