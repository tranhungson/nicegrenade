﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void UnityEngine.AudioSettings::InvokeOnAudioConfigurationChanged(System.Boolean)
extern void AudioSettings_InvokeOnAudioConfigurationChanged_m8D251791C6A402B12E93C22F43475DE3033FC8E7 ();
// 0x00000002 System.Boolean UnityEngine.AudioSettings::StartAudioOutput()
extern void AudioSettings_StartAudioOutput_m0D831FF470163273341701B1054B28FF962F7712 ();
// 0x00000003 System.Boolean UnityEngine.AudioSettings::StopAudioOutput()
extern void AudioSettings_StopAudioOutput_m7F6B15A6B8E9F0CEE05E4DA9B09EF4EC1158B588 ();
// 0x00000004 System.Void UnityEngine.AudioSettings_AudioConfigurationChangeHandler::.ctor(System.Object,System.IntPtr)
extern void AudioConfigurationChangeHandler__ctor_mF9399769D5BB18D740774B9E3129958868BD6D9A ();
// 0x00000005 System.Void UnityEngine.AudioSettings_AudioConfigurationChangeHandler::Invoke(System.Boolean)
extern void AudioConfigurationChangeHandler_Invoke_m62D72B397E1DC117C8C92A450D2C86C535A2BF49 ();
// 0x00000006 System.IAsyncResult UnityEngine.AudioSettings_AudioConfigurationChangeHandler::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern void AudioConfigurationChangeHandler_BeginInvoke_mB0B0ACF6281B999FA11037CA130CA3C72BEC7827 ();
// 0x00000007 System.Void UnityEngine.AudioSettings_AudioConfigurationChangeHandler::EndInvoke(System.IAsyncResult)
extern void AudioConfigurationChangeHandler_EndInvoke_mBB53599C34E3944D3A1DD71EFD2D73AF105CF830 ();
// 0x00000008 System.Boolean UnityEngine.AudioSettings_Mobile::get_muteState()
extern void Mobile_get_muteState_mD70625E75D05D14E2E6E18FACFFFFE324661906B ();
// 0x00000009 System.Void UnityEngine.AudioSettings_Mobile::set_muteState(System.Boolean)
extern void Mobile_set_muteState_m878F00ADEB5D9EAF6AD1B8BC427C505A89A4EDE4 ();
// 0x0000000A System.Boolean UnityEngine.AudioSettings_Mobile::get_stopAudioOutputOnMute()
extern void Mobile_get_stopAudioOutputOnMute_m2B8075BC7894966E20D0ED22A66FD39A884ECD81 ();
// 0x0000000B System.Void UnityEngine.AudioSettings_Mobile::InvokeOnMuteStateChanged(System.Boolean)
extern void Mobile_InvokeOnMuteStateChanged_m01961F68C19CCF813239484FB3B76EFC164173D1 ();
// 0x0000000C System.Void UnityEngine.AudioSettings_Mobile::StartAudioOutput()
extern void Mobile_StartAudioOutput_m19AF9680E60C92480171A84480DCB93487FFA6C6 ();
// 0x0000000D System.Void UnityEngine.AudioSettings_Mobile::StopAudioOutput()
extern void Mobile_StopAudioOutput_mA7D45E035C9ABF5E4FE67C93A018E14E05748745 ();
// 0x0000000E System.Void UnityEngine.AudioSettings_Mobile::.cctor()
extern void Mobile__cctor_m917C30310A8E4193197B8D2AA6EF256369258B6F ();
// 0x0000000F System.Void UnityEngine.AudioClip::.ctor()
extern void AudioClip__ctor_m52425138C3A036FC847A0E4C4ADA31CEF81CD10D ();
// 0x00000010 System.Void UnityEngine.AudioClip::InvokePCMReaderCallback_Internal(System.Single[])
extern void AudioClip_InvokePCMReaderCallback_Internal_mF087FCAD425EAC299C1156BA809DC535D00757F9 ();
// 0x00000011 System.Void UnityEngine.AudioClip::InvokePCMSetPositionCallback_Internal(System.Int32)
extern void AudioClip_InvokePCMSetPositionCallback_Internal_mBB8265A5BFF660F8AF39718DDB193319AB7EFA6F ();
// 0x00000012 System.Void UnityEngine.AudioClip_PCMReaderCallback::.ctor(System.Object,System.IntPtr)
extern void PCMReaderCallback__ctor_mF9EB2467704F5E13196BBA93F41FA275AC5432F6 ();
// 0x00000013 System.Void UnityEngine.AudioClip_PCMReaderCallback::Invoke(System.Single[])
extern void PCMReaderCallback_Invoke_m7B101820DB35BEFC8D2724DF96900367863B93B6 ();
// 0x00000014 System.IAsyncResult UnityEngine.AudioClip_PCMReaderCallback::BeginInvoke(System.Single[],System.AsyncCallback,System.Object)
extern void PCMReaderCallback_BeginInvoke_m94035E11B2B9BD6114EF3D7F4B7E367572E7AE1F ();
// 0x00000015 System.Void UnityEngine.AudioClip_PCMReaderCallback::EndInvoke(System.IAsyncResult)
extern void PCMReaderCallback_EndInvoke_m6730FD7DFD7246F137C437BC470F995D6C75E15B ();
// 0x00000016 System.Void UnityEngine.AudioClip_PCMSetPositionCallback::.ctor(System.Object,System.IntPtr)
extern void PCMSetPositionCallback__ctor_m31EA578C3CCFDFC9335B8C67353878AEE4B3905F ();
// 0x00000017 System.Void UnityEngine.AudioClip_PCMSetPositionCallback::Invoke(System.Int32)
extern void PCMSetPositionCallback_Invoke_m8EA4736B43191A8E6F95E1548AFF124519EC533C ();
// 0x00000018 System.IAsyncResult UnityEngine.AudioClip_PCMSetPositionCallback::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern void PCMSetPositionCallback_BeginInvoke_m88CDF70D75854621CA69ED3D53CD53B8206A5093 ();
// 0x00000019 System.Void UnityEngine.AudioClip_PCMSetPositionCallback::EndInvoke(System.IAsyncResult)
extern void PCMSetPositionCallback_EndInvoke_mB711E23CFD370348A1680B281A3DFE04F970792C ();
// 0x0000001A System.Single UnityEngine.AudioSource::GetPitch(UnityEngine.AudioSource)
extern void AudioSource_GetPitch_m76B68079F309EB1FE2BFF4A0B014B89780B383CA ();
// 0x0000001B System.Void UnityEngine.AudioSource::SetPitch(UnityEngine.AudioSource,System.Single)
extern void AudioSource_SetPitch_mB65EF59C56A49FD68CD3361B2C34E07F89F1244B ();
// 0x0000001C System.Void UnityEngine.AudioSource::PlayHelper(UnityEngine.AudioSource,System.UInt64)
extern void AudioSource_PlayHelper_m361C17B583E05D2A5FA0F03BD7CD98D74FBF83AC ();
// 0x0000001D System.Void UnityEngine.AudioSource::PlayOneShotHelper(UnityEngine.AudioSource,UnityEngine.AudioClip,System.Single)
extern void AudioSource_PlayOneShotHelper_mDAEDF5E0C56C665DE62CDF26E7B98149B4A71704 ();
// 0x0000001E System.Void UnityEngine.AudioSource::Stop(System.Boolean)
extern void AudioSource_Stop_mD3712B98BC6DBEA9CEEF778CE9CCB8DBA62F47A8 ();
// 0x0000001F System.Single UnityEngine.AudioSource::get_volume()
extern void AudioSource_get_volume_mBD65DB423F0520CDCB935CC593565343965A4CB0 ();
// 0x00000020 System.Void UnityEngine.AudioSource::set_volume(System.Single)
extern void AudioSource_set_volume_mF1757D70EE113871724334D13F70EF1ED033BA06 ();
// 0x00000021 System.Single UnityEngine.AudioSource::get_pitch()
extern void AudioSource_get_pitch_m70F33CAA7F869F88AB78BFEFA7385CF533F9B50D ();
// 0x00000022 System.Void UnityEngine.AudioSource::set_pitch(System.Single)
extern void AudioSource_set_pitch_mAB8F8CDB21A3139D3471784FEE9DBFA4CEDEE2E0 ();
// 0x00000023 System.Single UnityEngine.AudioSource::get_time()
extern void AudioSource_get_time_m9EE836ADDDAB3598FD7F6D198F847C43E0FFBF52 ();
// 0x00000024 System.Void UnityEngine.AudioSource::set_time(System.Single)
extern void AudioSource_set_time_m76B72F7AEF8B07469847BF8EFFC51991C9D695B1 ();
// 0x00000025 System.Int32 UnityEngine.AudioSource::get_timeSamples()
extern void AudioSource_get_timeSamples_m0E3467605A8D6CBFA36D6F167E4D663DA2D17EE7 ();
// 0x00000026 System.Void UnityEngine.AudioSource::set_timeSamples(System.Int32)
extern void AudioSource_set_timeSamples_m0080C96DD5EF182E40D63272E086FEE32F541CA0 ();
// 0x00000027 UnityEngine.AudioClip UnityEngine.AudioSource::get_clip()
extern void AudioSource_get_clip_m773ECEF5566EA64C74E316D7EF1A63AA01604643 ();
// 0x00000028 System.Void UnityEngine.AudioSource::set_clip(UnityEngine.AudioClip)
extern void AudioSource_set_clip_mF574231E0B749E0167CAF9E4FCBA06BAA0F9ED9B ();
// 0x00000029 UnityEngine.Audio.AudioMixerGroup UnityEngine.AudioSource::get_outputAudioMixerGroup()
extern void AudioSource_get_outputAudioMixerGroup_m6582826E5DBB0E210AE5DE0974F1D4BC945A78D7 ();
// 0x0000002A System.Void UnityEngine.AudioSource::set_outputAudioMixerGroup(UnityEngine.Audio.AudioMixerGroup)
extern void AudioSource_set_outputAudioMixerGroup_mC153530274A1109635889A23646E1F5BA5C150E1 ();
// 0x0000002B System.Void UnityEngine.AudioSource::Play()
extern void AudioSource_Play_m0BA206481892AA4AF7DB2900A0B0805076516164 ();
// 0x0000002C System.Void UnityEngine.AudioSource::PlayOneShot(UnityEngine.AudioClip)
extern void AudioSource_PlayOneShot_mFD68566752A61B9C54843650A5C6075DBBFC56CD ();
// 0x0000002D System.Void UnityEngine.AudioSource::PlayOneShot(UnityEngine.AudioClip,System.Single)
extern void AudioSource_PlayOneShot_mA65B809A4480039CD8337ABF45C0E57E137EED41 ();
// 0x0000002E System.Void UnityEngine.AudioSource::Stop()
extern void AudioSource_Stop_m488F7AA7F7067DE3EC92CEE3413E86C2E5940200 ();
// 0x0000002F System.Boolean UnityEngine.AudioSource::get_isPlaying()
extern void AudioSource_get_isPlaying_m5112A878573652681F40C82F0D8103C999978F3C ();
// 0x00000030 System.Boolean UnityEngine.AudioSource::get_loop()
extern void AudioSource_get_loop_m576259B36687CA2327106F945A3E49E3D74A7F90 ();
// 0x00000031 System.Void UnityEngine.AudioSource::set_loop(System.Boolean)
extern void AudioSource_set_loop_m4DEE785C31213E964D7014B633F0FFC7E98B79F4 ();
// 0x00000032 System.Boolean UnityEngine.AudioSource::get_ignoreListenerVolume()
extern void AudioSource_get_ignoreListenerVolume_mF12EEF4A236D621162CE225ECCD7B175B92E59BE ();
// 0x00000033 System.Void UnityEngine.AudioSource::set_ignoreListenerVolume(System.Boolean)
extern void AudioSource_set_ignoreListenerVolume_m62400A4AC76F3DD1DCBD9CEAA89460A022C64227 ();
// 0x00000034 System.Boolean UnityEngine.AudioSource::get_playOnAwake()
extern void AudioSource_get_playOnAwake_mFCCEA6BF78DC708ABD1DCC7BC8BE79EA0CFC3AE0 ();
// 0x00000035 System.Void UnityEngine.AudioSource::set_playOnAwake(System.Boolean)
extern void AudioSource_set_playOnAwake_m5E4C76260D66898EEFEB20E4F42B6249AACB4128 ();
// 0x00000036 System.Boolean UnityEngine.AudioSource::get_ignoreListenerPause()
extern void AudioSource_get_ignoreListenerPause_m6A940BC8FCDC83B97372B9BE244BFE58F5FC8FD8 ();
// 0x00000037 System.Void UnityEngine.AudioSource::set_ignoreListenerPause(System.Boolean)
extern void AudioSource_set_ignoreListenerPause_m12E096197FC8998A4C570442B4389299F7185238 ();
// 0x00000038 UnityEngine.AudioVelocityUpdateMode UnityEngine.AudioSource::get_velocityUpdateMode()
extern void AudioSource_get_velocityUpdateMode_m846BE29AA8C801B0631EEDE787B15456CF5C61F9 ();
// 0x00000039 System.Void UnityEngine.AudioSource::set_velocityUpdateMode(UnityEngine.AudioVelocityUpdateMode)
extern void AudioSource_set_velocityUpdateMode_m9D5B3C54EE308B05B103175E3D892F05134DF92D ();
// 0x0000003A System.Single UnityEngine.AudioSource::get_panStereo()
extern void AudioSource_get_panStereo_m3E7E4E78DF624E34E3630E0EF1BA4845F9C421DA ();
// 0x0000003B System.Void UnityEngine.AudioSource::set_panStereo(System.Single)
extern void AudioSource_set_panStereo_mBE007C40EDA2FEBA5F5192EB895039B07167BF60 ();
// 0x0000003C System.Single UnityEngine.AudioSource::get_spatialBlend()
extern void AudioSource_get_spatialBlend_mE43E63FB77B0BA35F2494ED234F16945D0801759 ();
// 0x0000003D System.Void UnityEngine.AudioSource::set_spatialBlend(System.Single)
extern void AudioSource_set_spatialBlend_mC144B8230F08743505D4B0A92C1B9F809BC6D9C5 ();
// 0x0000003E System.Boolean UnityEngine.AudioSource::get_spatialize()
extern void AudioSource_get_spatialize_m8CC430356EDE4FF4BD31A5F5509028B7DDCC4C11 ();
// 0x0000003F System.Void UnityEngine.AudioSource::set_spatialize(System.Boolean)
extern void AudioSource_set_spatialize_m67B01FA31D125E7A6E3E20A56A781D1E9200D8FD ();
// 0x00000040 System.Single UnityEngine.AudioSource::get_reverbZoneMix()
extern void AudioSource_get_reverbZoneMix_m7C3DD9F908BB0D48F6F1A29FD7A354C5542F111A ();
// 0x00000041 System.Void UnityEngine.AudioSource::set_reverbZoneMix(System.Single)
extern void AudioSource_set_reverbZoneMix_mF004D7B799EC7EDB38E5069A004A4C0C509DCB87 ();
// 0x00000042 System.Boolean UnityEngine.AudioSource::get_bypassEffects()
extern void AudioSource_get_bypassEffects_m611C4D7600941291B1AB76069BA9EA1F13D8B589 ();
// 0x00000043 System.Void UnityEngine.AudioSource::set_bypassEffects(System.Boolean)
extern void AudioSource_set_bypassEffects_m6E1BBD3084EEEA68DDD468C8D488DC85A1112AD3 ();
// 0x00000044 System.Boolean UnityEngine.AudioSource::get_bypassListenerEffects()
extern void AudioSource_get_bypassListenerEffects_m52C8B5B45ED4D909A2A6FE7C6D1382AAE3CE1A10 ();
// 0x00000045 System.Void UnityEngine.AudioSource::set_bypassListenerEffects(System.Boolean)
extern void AudioSource_set_bypassListenerEffects_m34B3031437F9BC2D8A99584B4906DD23FC73A16C ();
// 0x00000046 System.Boolean UnityEngine.AudioSource::get_bypassReverbZones()
extern void AudioSource_get_bypassReverbZones_m0022B61400FD43C234B35D5720545B8FD608D3C2 ();
// 0x00000047 System.Void UnityEngine.AudioSource::set_bypassReverbZones(System.Boolean)
extern void AudioSource_set_bypassReverbZones_m05084DBBFD9B2E7375DFA9573692D159579BE4AE ();
// 0x00000048 System.Single UnityEngine.AudioSource::get_dopplerLevel()
extern void AudioSource_get_dopplerLevel_mE282667E115B2372A3163EC9C9DF9BA69EEEC6EB ();
// 0x00000049 System.Void UnityEngine.AudioSource::set_dopplerLevel(System.Single)
extern void AudioSource_set_dopplerLevel_mA90937913103AB595C891B72B4B5724F8F54AB86 ();
// 0x0000004A System.Single UnityEngine.AudioSource::get_spread()
extern void AudioSource_get_spread_m35E2B95E4DB64DE94037272C7DCD2434CD928606 ();
// 0x0000004B System.Void UnityEngine.AudioSource::set_spread(System.Single)
extern void AudioSource_set_spread_m511F99128902DF9745AD5337D79E3D43E2BE3DBF ();
// 0x0000004C System.Int32 UnityEngine.AudioSource::get_priority()
extern void AudioSource_get_priority_m70CCC4BDAB1356902112C27B0C1BADAB23882077 ();
// 0x0000004D System.Void UnityEngine.AudioSource::set_priority(System.Int32)
extern void AudioSource_set_priority_m2560D32385E544B7706071B38F2A195C72E50AB6 ();
// 0x0000004E System.Boolean UnityEngine.AudioSource::get_mute()
extern void AudioSource_get_mute_mC420E7827CA751603A85430D849E8C3099FBD123 ();
// 0x0000004F System.Void UnityEngine.AudioSource::set_mute(System.Boolean)
extern void AudioSource_set_mute_m04D579849D7D37D6CC39DE31DB928176B2A9C2CF ();
// 0x00000050 System.Single UnityEngine.AudioSource::get_minDistance()
extern void AudioSource_get_minDistance_m5CC7E0BF063CE86D4A6B2D4C9D084ADC98B66081 ();
// 0x00000051 System.Void UnityEngine.AudioSource::set_minDistance(System.Single)
extern void AudioSource_set_minDistance_m6BE1B716538D146E7DAFED645624204A6F49496C ();
// 0x00000052 System.Single UnityEngine.AudioSource::get_maxDistance()
extern void AudioSource_get_maxDistance_m6CF9ECA5BE401525C918357FC56FDC03D9EA7742 ();
// 0x00000053 System.Void UnityEngine.AudioSource::set_maxDistance(System.Single)
extern void AudioSource_set_maxDistance_m28ACA41DEC146D471E3E5EC0CC4EC2013D46F5E0 ();
// 0x00000054 UnityEngine.AudioRolloffMode UnityEngine.AudioSource::get_rolloffMode()
extern void AudioSource_get_rolloffMode_m1AD680EF7696C27D41845EEDA0B0B5ED2EBCD5A7 ();
// 0x00000055 System.Void UnityEngine.AudioSource::set_rolloffMode(UnityEngine.AudioRolloffMode)
extern void AudioSource_set_rolloffMode_m1E420E7DC5D44A0001A22352C1CD59BEAED1794D ();
// 0x00000056 UnityEngine.Playables.PlayableHandle UnityEngine.Audio.AudioClipPlayable::GetHandle()
extern void AudioClipPlayable_GetHandle_mEE8F62E9DA2A0DDDB064A9AA2391909C425CB3B1_AdjustorThunk ();
// 0x00000057 System.Boolean UnityEngine.Audio.AudioClipPlayable::Equals(UnityEngine.Audio.AudioClipPlayable)
extern void AudioClipPlayable_Equals_mEB47B5F2E1C643D403FA916C8961F117593DCFC0_AdjustorThunk ();
// 0x00000058 System.Void UnityEngine.Audio.AudioMixerGroup::.ctor()
extern void AudioMixerGroup__ctor_mCE836CE43DAF42CC9F5A83E92E3965D22C2119A4 ();
// 0x00000059 UnityEngine.Playables.PlayableHandle UnityEngine.Audio.AudioMixerPlayable::GetHandle()
extern void AudioMixerPlayable_GetHandle_mDBC7135DF653E0E19675B6694EA89958E609587D_AdjustorThunk ();
// 0x0000005A System.Boolean UnityEngine.Audio.AudioMixerPlayable::Equals(UnityEngine.Audio.AudioMixerPlayable)
extern void AudioMixerPlayable_Equals_m6B84D1A5AEDEAAE12AEFB77319B2662506ABC9C4_AdjustorThunk ();
// 0x0000005B System.Void UnityEngine.Experimental.Audio.AudioSampleProvider::InvokeSampleFramesAvailable(System.Int32)
extern void AudioSampleProvider_InvokeSampleFramesAvailable_m7604AAF1AC01473A29DCDAD1AEC06165504BE832 ();
// 0x0000005C System.Void UnityEngine.Experimental.Audio.AudioSampleProvider::InvokeSampleFramesOverflow(System.Int32)
extern void AudioSampleProvider_InvokeSampleFramesOverflow_mC81A014388E535569EF02E3DA6B9831B0FB8A8D4 ();
// 0x0000005D System.Void UnityEngine.Experimental.Audio.AudioSampleProvider_SampleFramesHandler::.ctor(System.Object,System.IntPtr)
extern void SampleFramesHandler__ctor_mFDA0769E55F136D1B8EC8AA4B40EF43069934EB5 ();
// 0x0000005E System.Void UnityEngine.Experimental.Audio.AudioSampleProvider_SampleFramesHandler::Invoke(UnityEngine.Experimental.Audio.AudioSampleProvider,System.UInt32)
extern void SampleFramesHandler_Invoke_m52F0148F680B36E04A7F850E617FBEF1CA9809FD ();
// 0x0000005F System.IAsyncResult UnityEngine.Experimental.Audio.AudioSampleProvider_SampleFramesHandler::BeginInvoke(UnityEngine.Experimental.Audio.AudioSampleProvider,System.UInt32,System.AsyncCallback,System.Object)
extern void SampleFramesHandler_BeginInvoke_mE516B77CCC50738663D10DDD2D7BDB4391FDFF92 ();
// 0x00000060 System.Void UnityEngine.Experimental.Audio.AudioSampleProvider_SampleFramesHandler::EndInvoke(System.IAsyncResult)
extern void SampleFramesHandler_EndInvoke_mF5305B3BA179CE3C49836790DE3FEB02EB088D28 ();
static Il2CppMethodPointer s_methodPointers[96] = 
{
	AudioSettings_InvokeOnAudioConfigurationChanged_m8D251791C6A402B12E93C22F43475DE3033FC8E7,
	AudioSettings_StartAudioOutput_m0D831FF470163273341701B1054B28FF962F7712,
	AudioSettings_StopAudioOutput_m7F6B15A6B8E9F0CEE05E4DA9B09EF4EC1158B588,
	AudioConfigurationChangeHandler__ctor_mF9399769D5BB18D740774B9E3129958868BD6D9A,
	AudioConfigurationChangeHandler_Invoke_m62D72B397E1DC117C8C92A450D2C86C535A2BF49,
	AudioConfigurationChangeHandler_BeginInvoke_mB0B0ACF6281B999FA11037CA130CA3C72BEC7827,
	AudioConfigurationChangeHandler_EndInvoke_mBB53599C34E3944D3A1DD71EFD2D73AF105CF830,
	Mobile_get_muteState_mD70625E75D05D14E2E6E18FACFFFFE324661906B,
	Mobile_set_muteState_m878F00ADEB5D9EAF6AD1B8BC427C505A89A4EDE4,
	Mobile_get_stopAudioOutputOnMute_m2B8075BC7894966E20D0ED22A66FD39A884ECD81,
	Mobile_InvokeOnMuteStateChanged_m01961F68C19CCF813239484FB3B76EFC164173D1,
	Mobile_StartAudioOutput_m19AF9680E60C92480171A84480DCB93487FFA6C6,
	Mobile_StopAudioOutput_mA7D45E035C9ABF5E4FE67C93A018E14E05748745,
	Mobile__cctor_m917C30310A8E4193197B8D2AA6EF256369258B6F,
	AudioClip__ctor_m52425138C3A036FC847A0E4C4ADA31CEF81CD10D,
	AudioClip_InvokePCMReaderCallback_Internal_mF087FCAD425EAC299C1156BA809DC535D00757F9,
	AudioClip_InvokePCMSetPositionCallback_Internal_mBB8265A5BFF660F8AF39718DDB193319AB7EFA6F,
	PCMReaderCallback__ctor_mF9EB2467704F5E13196BBA93F41FA275AC5432F6,
	PCMReaderCallback_Invoke_m7B101820DB35BEFC8D2724DF96900367863B93B6,
	PCMReaderCallback_BeginInvoke_m94035E11B2B9BD6114EF3D7F4B7E367572E7AE1F,
	PCMReaderCallback_EndInvoke_m6730FD7DFD7246F137C437BC470F995D6C75E15B,
	PCMSetPositionCallback__ctor_m31EA578C3CCFDFC9335B8C67353878AEE4B3905F,
	PCMSetPositionCallback_Invoke_m8EA4736B43191A8E6F95E1548AFF124519EC533C,
	PCMSetPositionCallback_BeginInvoke_m88CDF70D75854621CA69ED3D53CD53B8206A5093,
	PCMSetPositionCallback_EndInvoke_mB711E23CFD370348A1680B281A3DFE04F970792C,
	AudioSource_GetPitch_m76B68079F309EB1FE2BFF4A0B014B89780B383CA,
	AudioSource_SetPitch_mB65EF59C56A49FD68CD3361B2C34E07F89F1244B,
	AudioSource_PlayHelper_m361C17B583E05D2A5FA0F03BD7CD98D74FBF83AC,
	AudioSource_PlayOneShotHelper_mDAEDF5E0C56C665DE62CDF26E7B98149B4A71704,
	AudioSource_Stop_mD3712B98BC6DBEA9CEEF778CE9CCB8DBA62F47A8,
	AudioSource_get_volume_mBD65DB423F0520CDCB935CC593565343965A4CB0,
	AudioSource_set_volume_mF1757D70EE113871724334D13F70EF1ED033BA06,
	AudioSource_get_pitch_m70F33CAA7F869F88AB78BFEFA7385CF533F9B50D,
	AudioSource_set_pitch_mAB8F8CDB21A3139D3471784FEE9DBFA4CEDEE2E0,
	AudioSource_get_time_m9EE836ADDDAB3598FD7F6D198F847C43E0FFBF52,
	AudioSource_set_time_m76B72F7AEF8B07469847BF8EFFC51991C9D695B1,
	AudioSource_get_timeSamples_m0E3467605A8D6CBFA36D6F167E4D663DA2D17EE7,
	AudioSource_set_timeSamples_m0080C96DD5EF182E40D63272E086FEE32F541CA0,
	AudioSource_get_clip_m773ECEF5566EA64C74E316D7EF1A63AA01604643,
	AudioSource_set_clip_mF574231E0B749E0167CAF9E4FCBA06BAA0F9ED9B,
	AudioSource_get_outputAudioMixerGroup_m6582826E5DBB0E210AE5DE0974F1D4BC945A78D7,
	AudioSource_set_outputAudioMixerGroup_mC153530274A1109635889A23646E1F5BA5C150E1,
	AudioSource_Play_m0BA206481892AA4AF7DB2900A0B0805076516164,
	AudioSource_PlayOneShot_mFD68566752A61B9C54843650A5C6075DBBFC56CD,
	AudioSource_PlayOneShot_mA65B809A4480039CD8337ABF45C0E57E137EED41,
	AudioSource_Stop_m488F7AA7F7067DE3EC92CEE3413E86C2E5940200,
	AudioSource_get_isPlaying_m5112A878573652681F40C82F0D8103C999978F3C,
	AudioSource_get_loop_m576259B36687CA2327106F945A3E49E3D74A7F90,
	AudioSource_set_loop_m4DEE785C31213E964D7014B633F0FFC7E98B79F4,
	AudioSource_get_ignoreListenerVolume_mF12EEF4A236D621162CE225ECCD7B175B92E59BE,
	AudioSource_set_ignoreListenerVolume_m62400A4AC76F3DD1DCBD9CEAA89460A022C64227,
	AudioSource_get_playOnAwake_mFCCEA6BF78DC708ABD1DCC7BC8BE79EA0CFC3AE0,
	AudioSource_set_playOnAwake_m5E4C76260D66898EEFEB20E4F42B6249AACB4128,
	AudioSource_get_ignoreListenerPause_m6A940BC8FCDC83B97372B9BE244BFE58F5FC8FD8,
	AudioSource_set_ignoreListenerPause_m12E096197FC8998A4C570442B4389299F7185238,
	AudioSource_get_velocityUpdateMode_m846BE29AA8C801B0631EEDE787B15456CF5C61F9,
	AudioSource_set_velocityUpdateMode_m9D5B3C54EE308B05B103175E3D892F05134DF92D,
	AudioSource_get_panStereo_m3E7E4E78DF624E34E3630E0EF1BA4845F9C421DA,
	AudioSource_set_panStereo_mBE007C40EDA2FEBA5F5192EB895039B07167BF60,
	AudioSource_get_spatialBlend_mE43E63FB77B0BA35F2494ED234F16945D0801759,
	AudioSource_set_spatialBlend_mC144B8230F08743505D4B0A92C1B9F809BC6D9C5,
	AudioSource_get_spatialize_m8CC430356EDE4FF4BD31A5F5509028B7DDCC4C11,
	AudioSource_set_spatialize_m67B01FA31D125E7A6E3E20A56A781D1E9200D8FD,
	AudioSource_get_reverbZoneMix_m7C3DD9F908BB0D48F6F1A29FD7A354C5542F111A,
	AudioSource_set_reverbZoneMix_mF004D7B799EC7EDB38E5069A004A4C0C509DCB87,
	AudioSource_get_bypassEffects_m611C4D7600941291B1AB76069BA9EA1F13D8B589,
	AudioSource_set_bypassEffects_m6E1BBD3084EEEA68DDD468C8D488DC85A1112AD3,
	AudioSource_get_bypassListenerEffects_m52C8B5B45ED4D909A2A6FE7C6D1382AAE3CE1A10,
	AudioSource_set_bypassListenerEffects_m34B3031437F9BC2D8A99584B4906DD23FC73A16C,
	AudioSource_get_bypassReverbZones_m0022B61400FD43C234B35D5720545B8FD608D3C2,
	AudioSource_set_bypassReverbZones_m05084DBBFD9B2E7375DFA9573692D159579BE4AE,
	AudioSource_get_dopplerLevel_mE282667E115B2372A3163EC9C9DF9BA69EEEC6EB,
	AudioSource_set_dopplerLevel_mA90937913103AB595C891B72B4B5724F8F54AB86,
	AudioSource_get_spread_m35E2B95E4DB64DE94037272C7DCD2434CD928606,
	AudioSource_set_spread_m511F99128902DF9745AD5337D79E3D43E2BE3DBF,
	AudioSource_get_priority_m70CCC4BDAB1356902112C27B0C1BADAB23882077,
	AudioSource_set_priority_m2560D32385E544B7706071B38F2A195C72E50AB6,
	AudioSource_get_mute_mC420E7827CA751603A85430D849E8C3099FBD123,
	AudioSource_set_mute_m04D579849D7D37D6CC39DE31DB928176B2A9C2CF,
	AudioSource_get_minDistance_m5CC7E0BF063CE86D4A6B2D4C9D084ADC98B66081,
	AudioSource_set_minDistance_m6BE1B716538D146E7DAFED645624204A6F49496C,
	AudioSource_get_maxDistance_m6CF9ECA5BE401525C918357FC56FDC03D9EA7742,
	AudioSource_set_maxDistance_m28ACA41DEC146D471E3E5EC0CC4EC2013D46F5E0,
	AudioSource_get_rolloffMode_m1AD680EF7696C27D41845EEDA0B0B5ED2EBCD5A7,
	AudioSource_set_rolloffMode_m1E420E7DC5D44A0001A22352C1CD59BEAED1794D,
	AudioClipPlayable_GetHandle_mEE8F62E9DA2A0DDDB064A9AA2391909C425CB3B1_AdjustorThunk,
	AudioClipPlayable_Equals_mEB47B5F2E1C643D403FA916C8961F117593DCFC0_AdjustorThunk,
	AudioMixerGroup__ctor_mCE836CE43DAF42CC9F5A83E92E3965D22C2119A4,
	AudioMixerPlayable_GetHandle_mDBC7135DF653E0E19675B6694EA89958E609587D_AdjustorThunk,
	AudioMixerPlayable_Equals_m6B84D1A5AEDEAAE12AEFB77319B2662506ABC9C4_AdjustorThunk,
	AudioSampleProvider_InvokeSampleFramesAvailable_m7604AAF1AC01473A29DCDAD1AEC06165504BE832,
	AudioSampleProvider_InvokeSampleFramesOverflow_mC81A014388E535569EF02E3DA6B9831B0FB8A8D4,
	SampleFramesHandler__ctor_mFDA0769E55F136D1B8EC8AA4B40EF43069934EB5,
	SampleFramesHandler_Invoke_m52F0148F680B36E04A7F850E617FBEF1CA9809FD,
	SampleFramesHandler_BeginInvoke_mE516B77CCC50738663D10DDD2D7BDB4391FDFF92,
	SampleFramesHandler_EndInvoke_mF5305B3BA179CE3C49836790DE3FEB02EB088D28,
};
static const int32_t s_InvokerIndices[96] = 
{
	775,
	49,
	49,
	163,
	31,
	1428,
	26,
	49,
	775,
	49,
	775,
	3,
	3,
	3,
	23,
	26,
	32,
	163,
	26,
	166,
	26,
	163,
	32,
	532,
	26,
	423,
	1218,
	108,
	1429,
	31,
	664,
	275,
	664,
	275,
	664,
	275,
	10,
	32,
	14,
	26,
	14,
	26,
	23,
	26,
	839,
	23,
	102,
	102,
	31,
	102,
	31,
	102,
	31,
	102,
	31,
	10,
	32,
	664,
	275,
	664,
	275,
	102,
	31,
	664,
	275,
	102,
	31,
	102,
	31,
	102,
	31,
	664,
	275,
	664,
	275,
	10,
	32,
	102,
	31,
	664,
	275,
	664,
	275,
	10,
	32,
	1290,
	1430,
	23,
	1290,
	1431,
	32,
	32,
	163,
	124,
	1009,
	26,
};
extern const Il2CppCodeGenModule g_UnityEngine_AudioModuleCodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_AudioModuleCodeGenModule = 
{
	"UnityEngine.AudioModule.dll",
	96,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
