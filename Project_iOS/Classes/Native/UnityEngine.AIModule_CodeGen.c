﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void UnityEngine.AI.NavMeshPath::.ctor()
extern void NavMeshPath__ctor_m97A90951BF188A25A9BD247CEDE6C2A8045A1B3C ();
// 0x00000002 System.Void UnityEngine.AI.NavMeshPath::Finalize()
extern void NavMeshPath_Finalize_m23F790F8E5479FBF2F8F1DF6154A882FE0D1E8C5 ();
// 0x00000003 System.IntPtr UnityEngine.AI.NavMeshPath::InitializeNavMeshPath()
extern void NavMeshPath_InitializeNavMeshPath_m813BB3AF473DA864BCACAA16EACA97FF754F5720 ();
// 0x00000004 System.Void UnityEngine.AI.NavMeshPath::DestroyNavMeshPath(System.IntPtr)
extern void NavMeshPath_DestroyNavMeshPath_mA533E9C84A96BCD0D10A3B0CB384378A2269E48F ();
// 0x00000005 UnityEngine.Vector3[] UnityEngine.AI.NavMeshPath::CalculateCornersInternal()
extern void NavMeshPath_CalculateCornersInternal_m0F634AB4FF3D313AB384965D6E7AD7EE6062F047 ();
// 0x00000006 System.Void UnityEngine.AI.NavMeshPath::ClearCornersInternal()
extern void NavMeshPath_ClearCornersInternal_mAA7B067EB75C4E3DC1B663053DBD6DFC87395277 ();
// 0x00000007 System.Void UnityEngine.AI.NavMeshPath::ClearCorners()
extern void NavMeshPath_ClearCorners_mB0B7FF49CE2AAD120C9C8279A9F47467C422C051 ();
// 0x00000008 System.Void UnityEngine.AI.NavMeshPath::CalculateCorners()
extern void NavMeshPath_CalculateCorners_mEB2CA95B66A63C50BFEDB7E68E62D58D30C0D740 ();
// 0x00000009 UnityEngine.Vector3[] UnityEngine.AI.NavMeshPath::get_corners()
extern void NavMeshPath_get_corners_mE33DBAFAD3C5E7E558138EA6CF026DD9AFEA1FCA ();
// 0x0000000A UnityEngine.AI.NavMeshPathStatus UnityEngine.AI.NavMeshPath::get_status()
extern void NavMeshPath_get_status_mD9455D8007F954FD0E487D468D3A3BABAE5FA8EE ();
// 0x0000000B System.Boolean UnityEngine.AI.NavMeshAgent::SetDestination(UnityEngine.Vector3)
extern void NavMeshAgent_SetDestination_mDCFFAB501EFCC89E17EA13F49A917D340AEF0BC8 ();
// 0x0000000C UnityEngine.Vector3 UnityEngine.AI.NavMeshAgent::get_velocity()
extern void NavMeshAgent_get_velocity_m1CF6CC11732234E2E3EC3B778B79C5492A990186 ();
// 0x0000000D System.Boolean UnityEngine.AI.NavMeshAgent::SetDestination_Injected(UnityEngine.Vector3&)
extern void NavMeshAgent_SetDestination_Injected_m03F00018E71D91B109510C2EF1F01A3ECD84FF89 ();
// 0x0000000E System.Void UnityEngine.AI.NavMeshAgent::get_velocity_Injected(UnityEngine.Vector3&)
extern void NavMeshAgent_get_velocity_Injected_mBBFC5D1D8F50C16F9ACD5FCC0A0E50A0FE525FB3 ();
// 0x0000000F UnityEngine.Vector3 UnityEngine.AI.NavMeshHit::get_position()
extern void NavMeshHit_get_position_m0AEED90D6B4FDE74045975E40B0A9C365F152F9E_AdjustorThunk ();
// 0x00000010 System.Void UnityEngine.AI.NavMesh::Internal_CallOnNavMeshPreUpdate()
extern void NavMesh_Internal_CallOnNavMeshPreUpdate_mED6CAB94A6CB61A5FD547B7026DB4C96F2AF5B60 ();
// 0x00000011 System.Boolean UnityEngine.AI.NavMesh::CalculatePath(UnityEngine.Vector3,UnityEngine.Vector3,System.Int32,UnityEngine.AI.NavMeshPath)
extern void NavMesh_CalculatePath_mCF9690B429137161B20FF8AEA81DB02A1D825D62 ();
// 0x00000012 System.Boolean UnityEngine.AI.NavMesh::CalculatePathInternal(UnityEngine.Vector3,UnityEngine.Vector3,System.Int32,UnityEngine.AI.NavMeshPath)
extern void NavMesh_CalculatePathInternal_m5D4F3A7F26D2A2FA51E85D29E279CA70919C764C ();
// 0x00000013 System.Boolean UnityEngine.AI.NavMesh::SamplePosition(UnityEngine.Vector3,UnityEngine.AI.NavMeshHit&,System.Single,System.Int32)
extern void NavMesh_SamplePosition_m783F8639449F56E7CA222A58E40323404E56C19E ();
// 0x00000014 System.Boolean UnityEngine.AI.NavMesh::CalculatePathInternal_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Int32,UnityEngine.AI.NavMeshPath)
extern void NavMesh_CalculatePathInternal_Injected_mF0ED4ECC69594E07FC412C14366F68BB29AE28F5 ();
// 0x00000015 System.Boolean UnityEngine.AI.NavMesh::SamplePosition_Injected(UnityEngine.Vector3&,UnityEngine.AI.NavMeshHit&,System.Single,System.Int32)
extern void NavMesh_SamplePosition_Injected_mF979357B944CC071BB59B94C8962F664F9390550 ();
// 0x00000016 System.Void UnityEngine.AI.NavMesh_OnNavMeshPreUpdate::.ctor(System.Object,System.IntPtr)
extern void OnNavMeshPreUpdate__ctor_mD019C429BD8D299B85C320A6EFB2FFEDC3F85F42 ();
// 0x00000017 System.Void UnityEngine.AI.NavMesh_OnNavMeshPreUpdate::Invoke()
extern void OnNavMeshPreUpdate_Invoke_mE56CD30B200FECFD94AD4B22923B32BD789D70F0 ();
// 0x00000018 System.IAsyncResult UnityEngine.AI.NavMesh_OnNavMeshPreUpdate::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnNavMeshPreUpdate_BeginInvoke_m67FA7767274E77169A57ADFE041EA9B914E752C6 ();
// 0x00000019 System.Void UnityEngine.AI.NavMesh_OnNavMeshPreUpdate::EndInvoke(System.IAsyncResult)
extern void OnNavMeshPreUpdate_EndInvoke_mB55765702AA123A6D7C3DF8DDC597E3DEBC79836 ();
static Il2CppMethodPointer s_methodPointers[25] = 
{
	NavMeshPath__ctor_m97A90951BF188A25A9BD247CEDE6C2A8045A1B3C,
	NavMeshPath_Finalize_m23F790F8E5479FBF2F8F1DF6154A882FE0D1E8C5,
	NavMeshPath_InitializeNavMeshPath_m813BB3AF473DA864BCACAA16EACA97FF754F5720,
	NavMeshPath_DestroyNavMeshPath_mA533E9C84A96BCD0D10A3B0CB384378A2269E48F,
	NavMeshPath_CalculateCornersInternal_m0F634AB4FF3D313AB384965D6E7AD7EE6062F047,
	NavMeshPath_ClearCornersInternal_mAA7B067EB75C4E3DC1B663053DBD6DFC87395277,
	NavMeshPath_ClearCorners_mB0B7FF49CE2AAD120C9C8279A9F47467C422C051,
	NavMeshPath_CalculateCorners_mEB2CA95B66A63C50BFEDB7E68E62D58D30C0D740,
	NavMeshPath_get_corners_mE33DBAFAD3C5E7E558138EA6CF026DD9AFEA1FCA,
	NavMeshPath_get_status_mD9455D8007F954FD0E487D468D3A3BABAE5FA8EE,
	NavMeshAgent_SetDestination_mDCFFAB501EFCC89E17EA13F49A917D340AEF0BC8,
	NavMeshAgent_get_velocity_m1CF6CC11732234E2E3EC3B778B79C5492A990186,
	NavMeshAgent_SetDestination_Injected_m03F00018E71D91B109510C2EF1F01A3ECD84FF89,
	NavMeshAgent_get_velocity_Injected_mBBFC5D1D8F50C16F9ACD5FCC0A0E50A0FE525FB3,
	NavMeshHit_get_position_m0AEED90D6B4FDE74045975E40B0A9C365F152F9E_AdjustorThunk,
	NavMesh_Internal_CallOnNavMeshPreUpdate_mED6CAB94A6CB61A5FD547B7026DB4C96F2AF5B60,
	NavMesh_CalculatePath_mCF9690B429137161B20FF8AEA81DB02A1D825D62,
	NavMesh_CalculatePathInternal_m5D4F3A7F26D2A2FA51E85D29E279CA70919C764C,
	NavMesh_SamplePosition_m783F8639449F56E7CA222A58E40323404E56C19E,
	NavMesh_CalculatePathInternal_Injected_mF0ED4ECC69594E07FC412C14366F68BB29AE28F5,
	NavMesh_SamplePosition_Injected_mF979357B944CC071BB59B94C8962F664F9390550,
	OnNavMeshPreUpdate__ctor_mD019C429BD8D299B85C320A6EFB2FFEDC3F85F42,
	OnNavMeshPreUpdate_Invoke_mE56CD30B200FECFD94AD4B22923B32BD789D70F0,
	OnNavMeshPreUpdate_BeginInvoke_m67FA7767274E77169A57ADFE041EA9B914E752C6,
	OnNavMeshPreUpdate_EndInvoke_mB55765702AA123A6D7C3DF8DDC597E3DEBC79836,
};
static const int32_t s_InvokerIndices[25] = 
{
	23,
	23,
	695,
	25,
	14,
	23,
	23,
	23,
	14,
	10,
	1062,
	1050,
	767,
	6,
	1050,
	3,
	1405,
	1405,
	1406,
	1407,
	1408,
	163,
	23,
	101,
	26,
};
extern const Il2CppCodeGenModule g_UnityEngine_AIModuleCodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_AIModuleCodeGenModule = 
{
	"UnityEngine.AIModule.dll",
	25,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
