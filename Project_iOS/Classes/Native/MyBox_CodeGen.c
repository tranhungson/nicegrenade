﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void FPSCounter::Awake()
extern void FPSCounter_Awake_mFA08B0C39F461F6086188612A72D3B0289AA1F27 ();
// 0x00000002 System.Void FPSCounter::Update()
extern void FPSCounter_Update_mEE757D79317F955F9E6048BCF09F1A7CC49E616B ();
// 0x00000003 System.Void FPSCounter::OnGUI()
extern void FPSCounter_OnGUI_m20B6796386A5539476094F040BC23A8636D73B43 ();
// 0x00000004 System.Void FPSCounter::.ctor()
extern void FPSCounter__ctor_m2321CBAE8182B0F73D59CF3E22DAA4880330C17B ();
// 0x00000005 System.Void MyBox.AutoPropertyAttribute::.ctor()
extern void AutoPropertyAttribute__ctor_m3E4A14BFDE523006274BF0D549311B8326D2E860 ();
// 0x00000006 System.Void MyBox.ButtonMethodAttribute::.ctor()
extern void ButtonMethodAttribute__ctor_m5366E2E19A7E56FA8F917B57A9D64DE558D49CDD ();
// 0x00000007 System.Void MyBox.ConditionalFieldAttribute::.ctor(System.String,System.Boolean,System.Object[])
extern void ConditionalFieldAttribute__ctor_mB97BC6EC587AA971732D98B3E391959ECAF6322E ();
// 0x00000008 System.Void MyBox.ConstantsSelectionAttribute::.ctor(System.Type)
extern void ConstantsSelectionAttribute__ctor_m262FFA961810A73A7ED21139765267A571EB5F03 ();
// 0x00000009 System.Void MyBox.DefinedValuesAttribute::.ctor(System.Object[])
extern void DefinedValuesAttribute__ctor_m003C14DAB0AAAB1B31465B77C144ACFEBE34B5A2 ();
// 0x0000000A System.Void MyBox.DisplayInspectorAttribute::.ctor(System.Boolean)
extern void DisplayInspectorAttribute__ctor_m03896D96B0C2F8CB847A22F00889A3B99C4DF39E ();
// 0x0000000B System.Void MyBox.FoldoutAttribute::.ctor(System.String,System.Boolean)
extern void FoldoutAttribute__ctor_m804D08161B302083333C35B4DF3129525FC72235 ();
// 0x0000000C System.Void MyBox.InitializationFieldAttribute::.ctor()
extern void InitializationFieldAttribute__ctor_m15AD3F636815F33D95C8811AA4810BC0CCE70B70 ();
// 0x0000000D System.Void MyBox.LayerAttribute::.ctor()
extern void LayerAttribute__ctor_m11FE5EC55826E5A57229B0E95946E32A40BC7DC9 ();
// 0x0000000E System.Void MyBox.MaxValueAttribute::.ctor(System.Single)
extern void MaxValueAttribute__ctor_m9AA32628F4B80102AA03445566035EA17534CB4E ();
// 0x0000000F System.Void MyBox.MaxValueAttribute::.ctor(System.Single,System.Single,System.Single)
extern void MaxValueAttribute__ctor_m4E556CF45BA8316EDDA523C7DFDCB13AB4C33A9F ();
// 0x00000010 System.Void MyBox.MinMaxRangeAttribute::.ctor(System.Single,System.Single)
extern void MinMaxRangeAttribute__ctor_m818492E883FCCBD2B1F0DB4F52701456587AEDAB ();
// 0x00000011 System.Void MyBox.RangedFloat::.ctor(System.Single,System.Single)
extern void RangedFloat__ctor_mC236D6100DBCB0566D22505BF94F4678BCF5830A_AdjustorThunk ();
// 0x00000012 System.Void MyBox.RangedInt::.ctor(System.Int32,System.Int32)
extern void RangedInt__ctor_m7B4EFB4893EE51FF3F4E867FE9C541E9F3028AA0_AdjustorThunk ();
// 0x00000013 System.Single MyBox.RangedExtensions::LerpFromRange(MyBox.RangedFloat,System.Single)
extern void RangedExtensions_LerpFromRange_m5FEB23824491E9349076387923ECE315283EE4D5 ();
// 0x00000014 System.Single MyBox.RangedExtensions::LerpFromRangeUnclamped(MyBox.RangedFloat,System.Single)
extern void RangedExtensions_LerpFromRangeUnclamped_m2081D990A861DEB6C971196B853BD8FDE2F5BB40 ();
// 0x00000015 System.Single MyBox.RangedExtensions::LerpFromRange(MyBox.RangedInt,System.Single)
extern void RangedExtensions_LerpFromRange_mE6001B003A19D275A9B4838EF099BBC6E6FC24A2 ();
// 0x00000016 System.Single MyBox.RangedExtensions::LerpFromRangeUnclamped(MyBox.RangedInt,System.Single)
extern void RangedExtensions_LerpFromRangeUnclamped_mB5340E6CE741C5541708D34138BFFC75AE21DC93 ();
// 0x00000017 System.Void MyBox.MinValueAttribute::.ctor(System.Single)
extern void MinValueAttribute__ctor_mDA90512B2FDAC2555FACD2E9AFA0F342108ADDFE ();
// 0x00000018 System.Void MyBox.MinValueAttribute::.ctor(System.Single,System.Single,System.Single)
extern void MinValueAttribute__ctor_m3096FCEC79271A554C3E86E5A7A4926A4D226372 ();
// 0x00000019 System.Void MyBox.MustBeAssignedAttribute::.ctor()
extern void MustBeAssignedAttribute__ctor_mFA6219167C7AA1A9FB449061A5C5F53E9F1558BB ();
// 0x0000001A System.Void MyBox.PositiveValueOnlyAttribute::.ctor()
extern void PositiveValueOnlyAttribute__ctor_mAEE97C245FBCBF2BA2406EF7DFCA63D35EA40B86 ();
// 0x0000001B System.Void MyBox.ReadOnlyAttribute::.ctor()
extern void ReadOnlyAttribute__ctor_m50FD5B931384C5619972A46E348C43215A2A4155 ();
// 0x0000001C System.Void MyBox.RequireLayerAttribute::.ctor(System.String)
extern void RequireLayerAttribute__ctor_m3F30A26CFBB3AA7D2F8A762DBD20B1679D3394A3 ();
// 0x0000001D System.Void MyBox.RequireLayerAttribute::.ctor(System.Int32)
extern void RequireLayerAttribute__ctor_mA1E6D1238623279134CC4CF7565DBF2641AD075B ();
// 0x0000001E System.Void MyBox.RequireTagAttribute::.ctor(System.String)
extern void RequireTagAttribute__ctor_m0A55A3FF705E1C76A7E818ADB91A2BC66231DEFE ();
// 0x0000001F System.Void MyBox.SearchableEnumAttribute::.ctor()
extern void SearchableEnumAttribute__ctor_mF10B8C35AACF7BE2E63F35BEB82006AF8F0071A9 ();
// 0x00000020 System.Void MyBox.SeparatorAttribute::.ctor()
extern void SeparatorAttribute__ctor_mBF2D9AC63E4C58E5F101B89F3D747FD5A6198D06 ();
// 0x00000021 System.Void MyBox.SeparatorAttribute::.ctor(System.String,System.Boolean)
extern void SeparatorAttribute__ctor_m1533B6572CFA6A9E96D976B5B6D1137F407DDBEA ();
// 0x00000022 System.Void MyBox.SpriteLayerAttribute::.ctor()
extern void SpriteLayerAttribute__ctor_m7DF525024FEC6543D24EE192BE3720A86197167A ();
// 0x00000023 System.Void MyBox.TagAttribute::.ctor()
extern void TagAttribute__ctor_m65BE09506FFEEBA7EFF0927683AB97A108277F53 ();
// 0x00000024 T[] MyBox.MyCollections::RemoveAt(T[],System.Int32)
// 0x00000025 T[] MyBox.MyCollections::InsertAt(T[],System.Int32)
// 0x00000026 T MyBox.MyCollections::GetRandom(T[])
// 0x00000027 T MyBox.MyCollections::GetRandom(System.Collections.Generic.IList`1<T>)
// 0x00000028 T MyBox.MyCollections::GetRandom(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000029 T[] MyBox.MyCollections::GetRandomCollection(System.Collections.Generic.IList`1<T>,System.Int32)
// 0x0000002A System.Boolean MyBox.MyCollections::IsNullOrEmpty(T[])
// 0x0000002B System.Boolean MyBox.MyCollections::IsNullOrEmpty(System.Collections.Generic.IList`1<T>)
// 0x0000002C System.Boolean MyBox.MyCollections::IsNullOrEmpty(System.Collections.Generic.IEnumerable`1<T>)
// 0x0000002D System.Int32 MyBox.MyCollections::NextIndexInCircle(T[],System.Int32)
// 0x0000002E System.Int32 MyBox.MyCollections::IndexOfItem(System.Collections.Generic.IEnumerable`1<T>,T)
// 0x0000002F System.Boolean MyBox.MyCollections::ContentsMatch(System.Collections.Generic.IEnumerable`1<T>,System.Collections.Generic.IEnumerable`1<T>)
// 0x00000030 System.Boolean MyBox.MyCollections::ContentsMatchKeys(MyBox.MyDictionary`2<T1,T2>,System.Collections.Generic.IEnumerable`1<T1>)
// 0x00000031 System.Boolean MyBox.MyCollections::ContentsMatchValues(MyBox.MyDictionary`2<T1,T2>,System.Collections.Generic.IEnumerable`1<T2>)
// 0x00000032 UnityEngine.Color MyBox.MyColor::get_RandomBright()
extern void MyColor_get_RandomBright_m817D5DABF8899074F147258FE1166BB176D2D42C ();
// 0x00000033 UnityEngine.Color MyBox.MyColor::get_RandomDim()
extern void MyColor_get_RandomDim_mBF56C3AFC8B5FB4BB98734B31D3DC45482A8C2E6 ();
// 0x00000034 UnityEngine.Color MyBox.MyColor::get_RandomColor()
extern void MyColor_get_RandomColor_mB7E521BC0C9FDB87B3DECE359A8F2665EA73FD47 ();
// 0x00000035 UnityEngine.Color MyBox.MyColor::WithAlphaSetTo(UnityEngine.Color,System.Single)
extern void MyColor_WithAlphaSetTo_mBED486A589BF5DDD4781EBC6ACC08A254AC1BB3E ();
// 0x00000036 System.Void MyBox.MyColor::SetAlpha(UnityEngine.UI.Graphic,System.Single)
extern void MyColor_SetAlpha_m105792C000ED51558D1C85149EA39D9A1BD7A007 ();
// 0x00000037 System.Void MyBox.MyColor::SetAlpha(UnityEngine.SpriteRenderer,System.Single)
extern void MyColor_SetAlpha_mD5114C9FF21D019B7905B9C3C46E27FC8E126E0F ();
// 0x00000038 System.String MyBox.MyColor::ToHex(UnityEngine.Color)
extern void MyColor_ToHex_m79C113AF586A9BA35BE5A3AB14E21A7761C712DE ();
// 0x00000039 UnityEngine.Color MyBox.MyColor::Lighter(UnityEngine.Color)
extern void MyColor_Lighter_mA7946E34AAF3D741A2A3B79E1F5F6BE38AAF0B0A ();
// 0x0000003A UnityEngine.Color MyBox.MyColor::Darker(UnityEngine.Color)
extern void MyColor_Darker_mEF66FF1F74A0A640257D5FAF73C4900847A60EFA ();
// 0x0000003B UnityEngine.Color MyBox.MyColor::BrightnessOffset(UnityEngine.Color,System.Single)
extern void MyColor_BrightnessOffset_m1A6A865D384A2395F8D099A69D85B2C6F0709B9D ();
// 0x0000003C MyBox.Internal.CoroutineOwner MyBox.MyCoroutines::get_CoroutineOwner()
extern void MyCoroutines_get_CoroutineOwner_m5C5716DA761A609FC2C3BA1C194492A26D49BA8F ();
// 0x0000003D UnityEngine.Coroutine MyBox.MyCoroutines::StartCoroutine(System.Collections.IEnumerator)
extern void MyCoroutines_StartCoroutine_m783479BBB5048F97A84408EDB5217479DEB66F35 ();
// 0x0000003E UnityEngine.Coroutine MyBox.MyCoroutines::StartNext(UnityEngine.Coroutine,System.Collections.IEnumerator)
extern void MyCoroutines_StartNext_m1E5046B9DD1A8E93759B164BE90BF0D38131BABB ();
// 0x0000003F System.Void MyBox.MyCoroutines::StopCoroutine(UnityEngine.Coroutine)
extern void MyCoroutines_StopCoroutine_m2880AB98AC4F83394615F7BABB20E92839EDFB68 ();
// 0x00000040 System.Void MyBox.MyCoroutines::StopAllCoroutines()
extern void MyCoroutines_StopAllCoroutines_m1A6FA8B2DEDDB1A0F5E10AAA636176FC8997C52C ();
// 0x00000041 MyBox.CoroutineGroup MyBox.MyCoroutines::CreateGroup(UnityEngine.MonoBehaviour)
extern void MyCoroutines_CreateGroup_mD71CF3EDDE1457A78B82C79315ED73BF82A7C420 ();
// 0x00000042 System.Collections.IEnumerator MyBox.MyCoroutines::StartNextCoroutine(UnityEngine.Coroutine,System.Collections.IEnumerator)
extern void MyCoroutines_StartNextCoroutine_m7E2DF6FDD877A6C8DAA3E7AEB8E761EB9149115E ();
// 0x00000043 System.Int32 MyBox.CoroutineGroup::get_ActiveCoroutinesAmount()
extern void CoroutineGroup_get_ActiveCoroutinesAmount_m87D50A0FA3110EA70123EE582101E9AF3AB95800 ();
// 0x00000044 System.Boolean MyBox.CoroutineGroup::get_AnyProcessing()
extern void CoroutineGroup_get_AnyProcessing_m0EE4D9534DAC1113EA2233FADFB367C6594FDC8B ();
// 0x00000045 System.Void MyBox.CoroutineGroup::.ctor(UnityEngine.MonoBehaviour)
extern void CoroutineGroup__ctor_mB399CB4D92FF6537A7B2F8F9A5F5D957703953F8 ();
// 0x00000046 UnityEngine.Coroutine MyBox.CoroutineGroup::StartCoroutine(System.Collections.IEnumerator)
extern void CoroutineGroup_StartCoroutine_m11E3BB54376ED5B8B6C18ED358A51CBCD9B0485A ();
// 0x00000047 System.Void MyBox.CoroutineGroup::StopAll()
extern void CoroutineGroup_StopAll_m5535F32FB0A0C15A15EA25F5FA620388CE67548F ();
// 0x00000048 System.Collections.IEnumerator MyBox.CoroutineGroup::DoStart(System.Collections.IEnumerator)
extern void CoroutineGroup_DoStart_m7CB20F6426FD0ECC462AD2FC85C4A82EF45FB7DE ();
// 0x00000049 System.Void MyBox.MyDebug::LogArray(T[])
// 0x0000004A System.Void MyBox.MyDebug::LogArray(System.Collections.Generic.IList`1<T>)
// 0x0000004B System.Void MyBox.MyDebug::LogColor(UnityEngine.Color)
extern void MyDebug_LogColor_m455E64D88FB8D0E536E99A5D0783A4FFB5F08880 ();
// 0x0000004C System.Void MyBox.MyDebug::DrawDebugBounds(UnityEngine.MeshFilter,UnityEngine.Color)
extern void MyDebug_DrawDebugBounds_mF536D11A5DB9B0A29B569806649D52FFDD168E02 ();
// 0x0000004D System.Void MyBox.MyDebug::DrawDebugBounds(UnityEngine.MeshRenderer,UnityEngine.Color)
extern void MyDebug_DrawDebugBounds_m99BCB28369061169A964BD312FB4459AAECBE57E ();
// 0x0000004E System.Void MyBox.MyDebug::DrawDebugBounds(UnityEngine.Bounds,UnityEngine.Color)
extern void MyDebug_DrawDebugBounds_m4965BC6915BA9B4158AB18085B8ED7A7884E1F0B ();
// 0x0000004F System.Void MyBox.MyDebug::DrawString(System.String,UnityEngine.Vector3,System.Nullable`1<UnityEngine.Color>)
extern void MyDebug_DrawString_m95C095A5D1D56DED8176AFCBC53F5C62DFCCEA29 ();
// 0x00000050 System.Void MyBox.MyDebug::DrawArrowRay(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single)
extern void MyDebug_DrawArrowRay_m1C906A616EF9A6FEA57D3BDF9B977315609F20CC ();
// 0x00000051 System.Void MyBox.MyDebug::DrawDimensionalCross(UnityEngine.Vector3,System.Single)
extern void MyDebug_DrawDimensionalCross_m6A742B091561C484BB594C3B854463008CF50E83 ();
// 0x00000052 System.Collections.IEnumerator MyBox.MyDelayedActions::DelayedAction(System.Single,System.Action,System.Boolean)
extern void MyDelayedActions_DelayedAction_m4FA4FA0830DA3F2D5F53D0742CFE33689AECE25A ();
// 0x00000053 UnityEngine.Coroutine MyBox.MyDelayedActions::DelayedAction(UnityEngine.MonoBehaviour,System.Single,System.Action,System.Boolean)
extern void MyDelayedActions_DelayedAction_m292153E87856CD6EBC3A061F7A72BE128A3C789B ();
// 0x00000054 System.Collections.IEnumerator MyBox.MyDelayedActions::DelayedAction(System.Action)
extern void MyDelayedActions_DelayedAction_m62252D7594BAC1BAB0AFDD2978FAA7C84CE080BF ();
// 0x00000055 UnityEngine.Coroutine MyBox.MyDelayedActions::DelayedAction(UnityEngine.MonoBehaviour,System.Action)
extern void MyDelayedActions_DelayedAction_mF6E227449304D56FB891492EEC9B1560E974614B ();
// 0x00000056 System.Collections.IEnumerator MyBox.MyDelayedActions::DelayedUiSelection(UnityEngine.GameObject)
extern void MyDelayedActions_DelayedUiSelection_m85A56C6540D220A48F879E4335A9385C21AAF88C ();
// 0x00000057 UnityEngine.Coroutine MyBox.MyDelayedActions::DelayedUiSelection(UnityEngine.MonoBehaviour,UnityEngine.GameObject)
extern void MyDelayedActions_DelayedUiSelection_m40F9CA173A9B60C8B5A0D08CACDE1CDACF484EEE ();
// 0x00000058 System.Void MyBox.MyExtensions::Swap(T[],System.Int32,System.Int32)
// 0x00000059 System.Boolean MyBox.MyExtensions::IsWorldPointInViewport(UnityEngine.Camera,UnityEngine.Vector3)
extern void MyExtensions_IsWorldPointInViewport_mA8FFEC19E91EA5B0D0DFED26BB55A8FDDBA8D6B2 ();
// 0x0000005A T MyBox.MyExtensions::GetOrAddComponent(UnityEngine.GameObject)
// 0x0000005B T MyBox.MyExtensions::GetOrAddComponent(UnityEngine.Component)
// 0x0000005C System.Boolean MyBox.MyExtensions::HasComponent(UnityEngine.GameObject)
// 0x0000005D System.Collections.Generic.List`1<UnityEngine.Transform> MyBox.MyExtensions::GetObjectsOfLayerInChilds(UnityEngine.GameObject,System.Int32)
extern void MyExtensions_GetObjectsOfLayerInChilds_mC4E3671941107D2302868AAD0DC9F875FB3AFB22 ();
// 0x0000005E System.Collections.Generic.List`1<UnityEngine.Transform> MyBox.MyExtensions::GetObjectsOfLayerInChilds(UnityEngine.Component,System.Int32)
extern void MyExtensions_GetObjectsOfLayerInChilds_m11101D58F06E02090083D0C257C2DD29FB2B8E48 ();
// 0x0000005F System.Void MyBox.MyExtensions::CheckChildsOfLayer(UnityEngine.Transform,System.Int32,System.Collections.Generic.List`1<UnityEngine.Transform>)
extern void MyExtensions_CheckChildsOfLayer_m4EE72B032DFD0142426D29D98A5F0B0A9B98C830 ();
// 0x00000060 System.Void MyBox.MyExtensions::SetBodyState(UnityEngine.Rigidbody,System.Boolean)
extern void MyExtensions_SetBodyState_mA1B41075A7AC7DE562EE358199A16BCA8380CAC7 ();
// 0x00000061 T[] MyBox.MyExtensions::FindObjectsOfInterface()
// 0x00000062 MyBox.MyExtensions_ComponentOfInterface`1<T>[] MyBox.MyExtensions::FindObjectsOfInterfaceAsComponents()
// 0x00000063 T[] MyBox.MyExtensions::OnePerInstance(T[])
// 0x00000064 UnityEngine.RaycastHit2D[] MyBox.MyExtensions::OneHitPerInstance(UnityEngine.RaycastHit2D[])
extern void MyExtensions_OneHitPerInstance_m36BC5888BC7EF019E57E27289ED1D0A1308C5AB6 ();
// 0x00000065 UnityEngine.Collider2D[] MyBox.MyExtensions::OneHitPerInstance(UnityEngine.Collider2D[])
extern void MyExtensions_OneHitPerInstance_m4DE2AB5013DDE40B613249197DFA09C79F7764B6 ();
// 0x00000066 System.Collections.Generic.List`1<UnityEngine.Collider2D> MyBox.MyExtensions::OneHitPerInstanceList(UnityEngine.Collider2D[])
extern void MyExtensions_OneHitPerInstanceList_m6125362A7C16EA48B234843CAED681FB399391DE ();
// 0x00000067 System.Void MyBox.MyGizmos::DrawArrow(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single)
extern void MyGizmos_DrawArrow_m26A0DDED8A21141092DC7F4376FECB31EBDC251E ();
// 0x00000068 System.Boolean MyBox.MyInput::GetNumberDown(System.Int32)
extern void MyInput_GetNumberDown_m354F75AB1C934EDC818C5CCF4EBFC8168191C278 ();
// 0x00000069 System.Int32 MyBox.MyInput::GetNumberDown(UnityEngine.KeyCode)
extern void MyInput_GetNumberDown_m66397623BD686D3554691C1552F48D6945825C1C ();
// 0x0000006A System.Int32 MyBox.MyInput::GetNumberDown()
extern void MyInput_GetNumberDown_mA94C332015341F501803AD4B965EA5FBC9FAD6FB ();
// 0x0000006B System.String MyBox.MyInput::ToReadableString(UnityEngine.KeyCode,System.Boolean)
extern void MyInput_ToReadableString_mC9AE855C5D14D5893AC6F1C06160BCF2303C5375 ();
// 0x0000006C System.Boolean MyBox.MyInput::AnyKeyDown(UnityEngine.KeyCode,UnityEngine.KeyCode)
extern void MyInput_AnyKeyDown_m1924CA3FD7FB5AC989677F24AB5DB03474D12081 ();
// 0x0000006D System.Boolean MyBox.MyInput::AnyKeyDown(UnityEngine.KeyCode,UnityEngine.KeyCode,UnityEngine.KeyCode)
extern void MyInput_AnyKeyDown_mCB76AE7B9BAAA3B834981802D078184F758B26A9 ();
// 0x0000006E System.Boolean MyBox.MyInput::IsLeft()
extern void MyInput_IsLeft_m9C7F5F8ECF17334D9A9374E70012AFEA380D49EB ();
// 0x0000006F System.Boolean MyBox.MyInput::IsRight()
extern void MyInput_IsRight_m2785745C7FC19D4CD956843FBAC55C784B4EF255 ();
// 0x00000070 System.Boolean MyBox.MyInput::IsUp()
extern void MyInput_IsUp_m71CC46BE1D496B432303A3FCD9BA25FBC2A05C69 ();
// 0x00000071 System.Boolean MyBox.MyInput::IsDown()
extern void MyInput_IsDown_mC1EA3CD0348A02B7AA37F5B19F1B008E7E34F782 ();
// 0x00000072 System.Int32 MyBox.MyInput::KeypadDirection()
extern void MyInput_KeypadDirection_mB71569D2DC9AE3D6EB657E867CD3BE115689F0D5 ();
// 0x00000073 System.Int32 MyBox.MyInput::KeypadX()
extern void MyInput_KeypadX_m39D890532838BA1816087D4EA062B62729A27981 ();
// 0x00000074 System.Int32 MyBox.MyInput::KeypadY()
extern void MyInput_KeypadY_m599B80A5B8E8AEAE95FD4A4AD85E8693485F94EB ();
// 0x00000075 UnityEngine.LayerMask MyBox.MyLayers::ToLayerMask(System.Int32)
extern void MyLayers_ToLayerMask_m0ECBDFF950DD80F1AF2C224A8A19D0BF0FBFF46E ();
// 0x00000076 System.Boolean MyBox.MyLayers::LayerInMask(UnityEngine.LayerMask,System.Int32)
extern void MyLayers_LayerInMask_mDF5135CD1DB6B9799D1802080EB4F795F4387167 ();
// 0x00000077 System.Void MyBox.MyMath::Swap(T&,T&)
// 0x00000078 System.Single MyBox.MyMath::Snap(System.Single,System.Single)
extern void MyMath_Snap_m4C5CD34E31BFF74F1A1285E020964AF2FBF162EE ();
// 0x00000079 System.Int32 MyBox.MyMath::Sign(System.Single)
extern void MyMath_Sign_m1064EEE400593360836952CF2F893C4390E2D242 ();
// 0x0000007A System.Boolean MyBox.MyMath::Approximately(System.Single,System.Single)
extern void MyMath_Approximately_m4CFA803628DA78871AD94AE4B6D357360FCB6123 ();
// 0x0000007B System.Boolean MyBox.MyMath::InRange01(System.Single)
extern void MyMath_InRange01_mA6FD66A9D00A24CB3FCF37CF9D709C328F311C0A ();
// 0x0000007C System.Boolean MyBox.MyMath::InRange(System.Single,System.Single,System.Single)
extern void MyMath_InRange_m7FD4EC6C15E3D17681EC3BAAB3EECE750843B0DE ();
// 0x0000007D System.Single MyBox.MyMath::NotInRange(System.Single,System.Single,System.Single)
extern void MyMath_NotInRange_mC6732ED3C948267990A9FE56E182A756A6BB56EF ();
// 0x0000007E System.Int32 MyBox.MyMath::NotInRange(System.Int32,System.Int32,System.Int32)
extern void MyMath_NotInRange_m5E635003BA5BB487127ECB5B18C4B0F1E710BB4C ();
// 0x0000007F System.Single MyBox.MyMath::ClosestPoint(System.Single,System.Single,System.Single)
extern void MyMath_ClosestPoint_m1C5307D63AC53C2FEEDBF2C1AD0FAB7C37B92FD9 ();
// 0x00000080 System.Boolean MyBox.MyMath::ClosestPointIsA(System.Single,System.Single,System.Single)
extern void MyMath_ClosestPointIsA_m3B2964FECCCE5F6CD8EBA0B638D801897A59E4D0 ();
// 0x00000081 System.Single MyBox.MyNavMesh::GetLength(UnityEngine.AI.NavMeshPath)
extern void MyNavMesh_GetLength_m9ABAB3948D7C6A666A61CA9FC5DB878588E4CA30 ();
// 0x00000082 System.Single MyBox.MyNavMesh::GetTimeToPass(UnityEngine.AI.NavMeshPath,System.Single)
extern void MyNavMesh_GetTimeToPass_m22CFCFC89F653C60BEAAAF5C2C5613A3C633596B ();
// 0x00000083 UnityEngine.Vector3 MyBox.MyNavMesh::GetPointOnPath(UnityEngine.AI.NavMeshPath,System.Single)
extern void MyNavMesh_GetPointOnPath_m7BB406068523A830453899DF0ACDB464B4F19CD0 ();
// 0x00000084 System.Collections.Generic.IEnumerable`1<UnityEngine.Vector3> MyBox.MyNavMesh::GetPointsOnPath(UnityEngine.AI.NavMeshPath,System.Single)
extern void MyNavMesh_GetPointsOnPath_mE13005E038B6EBF984ECFAAD8CD23F4DFF2CDF06 ();
// 0x00000085 System.String MyBox.MyString::ToCamelCase(System.String)
extern void MyString_ToCamelCase_m458CFC98FD54CE2790C9F88DA1F7AA547A359200 ();
// 0x00000086 System.String MyBox.MyString::SplitCamelCase(System.String)
extern void MyString_SplitCamelCase_mA650F8E359DB7694EC425EBC13DE70E5803556E6 ();
// 0x00000087 System.String MyBox.MyString::Colored(System.String,MyBox.Colors)
extern void MyString_Colored_m66F64FCA981C486ED1A58F8BEFD8B40C2E186311 ();
// 0x00000088 System.String MyBox.MyString::Colored(System.String,System.String)
extern void MyString_Colored_m4193201BE398463A2411E63FA614424BAF60432F ();
// 0x00000089 System.String MyBox.MyString::Sized(System.String,System.Int32)
extern void MyString_Sized_m59097FE6D5E3131FBBC2555A141F82BE4C1F0AC7 ();
// 0x0000008A System.String MyBox.MyString::Bold(System.String)
extern void MyString_Bold_m33911CCD879E5FC35038ED35331C70B8F842A3C4 ();
// 0x0000008B System.String MyBox.MyString::Italics(System.String)
extern void MyString_Italics_mB61E6E17B1E90D8C1F9AE810EC17C73AF1AC5812 ();
// 0x0000008C UnityEngine.Sprite MyBox.MyTexture::AsSprite(UnityEngine.Texture2D)
extern void MyTexture_AsSprite_m2D67D8D7677C82F07C7778DF6EE46FD97418702B ();
// 0x0000008D UnityEngine.Texture2D MyBox.MyTexture::Resample(UnityEngine.Texture2D,System.Int32,System.Int32)
extern void MyTexture_Resample_m68073A5249DC8F4C81F320B03E62B2768A8818C3 ();
// 0x0000008E UnityEngine.Texture2D MyBox.MyTexture::Crop(UnityEngine.Texture2D,System.Int32,System.Int32,System.Int32,System.Int32,System.Single)
extern void MyTexture_Crop_m5F378792E696623B12E8DE5DA38C482B3AB8637F ();
// 0x0000008F UnityEngine.Texture2D MyBox.MyTexture::WithSolidColor(UnityEngine.Texture2D,UnityEngine.Color)
extern void MyTexture_WithSolidColor_m5DB36791A03A823F7A7ABC6DF3BFE0E3B39C9AAD ();
// 0x00000090 System.Void MyBox.MyUI::SetCanvasState(UnityEngine.CanvasGroup,System.Boolean)
extern void MyUI_SetCanvasState_mA2FEAADC3FCCF77912C0FDB653E11AC6F5EF314B ();
// 0x00000091 System.Void MyBox.MyUI::SetState(UnityEngine.CanvasGroup,System.Boolean)
extern void MyUI_SetState_m783C357B2C2CFAF45782C36A23D37D4AD7357416 ();
// 0x00000092 UnityEngine.EventSystems.EventTrigger_Entry MyBox.MyUI::OnEventSubscribe(UnityEngine.EventSystems.EventTrigger,UnityEngine.EventSystems.EventTriggerType,System.Action`1<UnityEngine.EventSystems.BaseEventData>)
extern void MyUI_OnEventSubscribe_m9C52EB7AF91886B2B0172D8C09FAFACEE79FF264 ();
// 0x00000093 System.Void MyBox.MyUI::OnEventUnsubscribe(UnityEngine.EventSystems.EventTrigger,UnityEngine.EventSystems.EventTrigger_Entry)
extern void MyUI_OnEventUnsubscribe_m6B6DA8FC14A3977238DE1E665C88CD9B86520477 ();
// 0x00000094 UnityEngine.Vector3 MyBox.MyVectors::SetX(UnityEngine.Vector3,System.Single)
extern void MyVectors_SetX_m3405C1C90BED601F3CF7007FF78D88D95D663894 ();
// 0x00000095 UnityEngine.Vector2 MyBox.MyVectors::SetX(UnityEngine.Vector2,System.Single)
extern void MyVectors_SetX_m42305F6F08D24E4F3FFDF5F6498A805C0E8A76D5 ();
// 0x00000096 System.Void MyBox.MyVectors::SetX(UnityEngine.Transform,System.Single)
extern void MyVectors_SetX_mE96C86801B8F160933208A334FDAB0642A55D515 ();
// 0x00000097 UnityEngine.Vector3 MyBox.MyVectors::SetY(UnityEngine.Vector3,System.Single)
extern void MyVectors_SetY_m0F4AE843CF47A355C9CB0943F6799463237A1249 ();
// 0x00000098 UnityEngine.Vector2 MyBox.MyVectors::SetY(UnityEngine.Vector2,System.Single)
extern void MyVectors_SetY_m46B789BFAB9EDD43C4BC3308D83FF12CDAAF5558 ();
// 0x00000099 System.Void MyBox.MyVectors::SetY(UnityEngine.Transform,System.Single)
extern void MyVectors_SetY_mE3BCAD383F4DDCB62CDE1CEDE01BCED2422E2B8F ();
// 0x0000009A UnityEngine.Vector3 MyBox.MyVectors::SetZ(UnityEngine.Vector3,System.Single)
extern void MyVectors_SetZ_mF7DFCA564C09338114230E32CD7F9503B0599069 ();
// 0x0000009B System.Void MyBox.MyVectors::SetZ(UnityEngine.Transform,System.Single)
extern void MyVectors_SetZ_mD7D8EA55B30017EB9A531FDF7C3F4FDD32FFC2DC ();
// 0x0000009C UnityEngine.Vector3 MyBox.MyVectors::SetXY(UnityEngine.Vector3,System.Single,System.Single)
extern void MyVectors_SetXY_mC0FFD9B3FE6247010C8943F30BFD8EA58F73860C ();
// 0x0000009D System.Void MyBox.MyVectors::SetXY(UnityEngine.Transform,System.Single,System.Single)
extern void MyVectors_SetXY_m1D9860A735CBEA6DD18F1FF530754B4FC22F9F26 ();
// 0x0000009E UnityEngine.Vector3 MyBox.MyVectors::SetXZ(UnityEngine.Vector3,System.Single,System.Single)
extern void MyVectors_SetXZ_mBB3C075766B1166F15B62C8F5006A8D319965CE6 ();
// 0x0000009F System.Void MyBox.MyVectors::SetXZ(UnityEngine.Transform,System.Single,System.Single)
extern void MyVectors_SetXZ_mE527DA7FA61555B1556EBC09111920AE339D1C16 ();
// 0x000000A0 UnityEngine.Vector3 MyBox.MyVectors::SetYZ(UnityEngine.Vector3,System.Single,System.Single)
extern void MyVectors_SetYZ_m8085AEC40FAFAF3350D0EEC384F6D38453E55492 ();
// 0x000000A1 System.Void MyBox.MyVectors::SetYZ(UnityEngine.Transform,System.Single,System.Single)
extern void MyVectors_SetYZ_m13516711EC4E167E65D2E6C6BF77F5631A70F78E ();
// 0x000000A2 System.Void MyBox.MyVectors::ResetPosition(UnityEngine.Transform)
extern void MyVectors_ResetPosition_m4B46583B6CCFA983185D86BFE29CBCE7C7E36A7E ();
// 0x000000A3 System.Void MyBox.MyVectors::SetPositionX(UnityEngine.RectTransform,System.Single)
extern void MyVectors_SetPositionX_m7675C2F4F095243EDD8B8FCA1049BB173ADD29EA ();
// 0x000000A4 System.Void MyBox.MyVectors::SetPositionY(UnityEngine.RectTransform,System.Single)
extern void MyVectors_SetPositionY_m7430A9552DBE83B1718A5E03F85DBD06ECE50A2F ();
// 0x000000A5 System.Void MyBox.MyVectors::OffsetPositionX(UnityEngine.RectTransform,System.Single)
extern void MyVectors_OffsetPositionX_m8202442408A39D026F64514DB0B3C58A52A87ECE ();
// 0x000000A6 System.Void MyBox.MyVectors::OffsetPositionY(UnityEngine.RectTransform,System.Single)
extern void MyVectors_OffsetPositionY_mDC4354E402B0E09D41EF3A7462247126BB3B396A ();
// 0x000000A7 UnityEngine.Vector3 MyBox.MyVectors::Offset(UnityEngine.Vector3,UnityEngine.Vector2)
extern void MyVectors_Offset_m49661CDCD3ECC3FEB0073AA4325784E57EED276C ();
// 0x000000A8 UnityEngine.Vector3 MyBox.MyVectors::OffsetX(UnityEngine.Vector3,System.Single)
extern void MyVectors_OffsetX_m5F34C7527EB7179FD304DD3A0ED0327C0C7E9EEC ();
// 0x000000A9 UnityEngine.Vector2 MyBox.MyVectors::OffsetX(UnityEngine.Vector2,System.Single)
extern void MyVectors_OffsetX_mEE5F3A40B15B9CEA50B4BFB4CBFCA7CB036BD7BB ();
// 0x000000AA System.Void MyBox.MyVectors::OffsetX(UnityEngine.Transform,System.Single)
extern void MyVectors_OffsetX_mFB94CB9ED9AE13E59ABD4AC6A2F9CBAD9AB2CA05 ();
// 0x000000AB UnityEngine.Vector2 MyBox.MyVectors::OffsetY(UnityEngine.Vector2,System.Single)
extern void MyVectors_OffsetY_mAEBAFB497B2D79CB26B22552D803158AA45D79B9 ();
// 0x000000AC UnityEngine.Vector3 MyBox.MyVectors::OffsetY(UnityEngine.Vector3,System.Single)
extern void MyVectors_OffsetY_m97A48F7025DF5121DA1F66A5F321FC86489302BE ();
// 0x000000AD System.Void MyBox.MyVectors::OffsetY(UnityEngine.Transform,System.Single)
extern void MyVectors_OffsetY_m0FA091FC1437BAADF8033C85A211070AA231F5AB ();
// 0x000000AE UnityEngine.Vector3 MyBox.MyVectors::OffsetZ(UnityEngine.Vector3,System.Single)
extern void MyVectors_OffsetZ_mBC0904B4A15532FEE165F8EE2DE704AFDF590B02 ();
// 0x000000AF System.Void MyBox.MyVectors::OffsetZ(UnityEngine.Transform,System.Single)
extern void MyVectors_OffsetZ_mA1A020B413E1DFA581BAD2354D992183F5A41B7C ();
// 0x000000B0 UnityEngine.Vector3 MyBox.MyVectors::OffsetXY(UnityEngine.Vector3,System.Single,System.Single)
extern void MyVectors_OffsetXY_m015C9C537C227749A5C502C1B84B9418FFF9AF14 ();
// 0x000000B1 System.Void MyBox.MyVectors::OffsetXY(UnityEngine.Transform,System.Single,System.Single)
extern void MyVectors_OffsetXY_m0658268E7DA92AB57F403F54999B1F6BE50A014B ();
// 0x000000B2 UnityEngine.Vector2 MyBox.MyVectors::OffsetXY(UnityEngine.Vector2,System.Single,System.Single)
extern void MyVectors_OffsetXY_mCB2592F279AB3D22D1566711FF5A922309EB0E65 ();
// 0x000000B3 UnityEngine.Vector3 MyBox.MyVectors::OffsetXZ(UnityEngine.Vector3,System.Single,System.Single)
extern void MyVectors_OffsetXZ_mB054757EBC47C70EB21E4A6A5D48113D26F085DA ();
// 0x000000B4 System.Void MyBox.MyVectors::OffsetXZ(UnityEngine.Transform,System.Single,System.Single)
extern void MyVectors_OffsetXZ_m22366B7DA8A40CE22C1F5A6504FBDDB6C43B6BDD ();
// 0x000000B5 UnityEngine.Vector3 MyBox.MyVectors::OffsetYZ(UnityEngine.Vector3,System.Single,System.Single)
extern void MyVectors_OffsetYZ_m7D49F750DCFD20F9BF36E9264ABD4292850C8D31 ();
// 0x000000B6 System.Void MyBox.MyVectors::OffsetYZ(UnityEngine.Transform,System.Single,System.Single)
extern void MyVectors_OffsetYZ_mBC2D54FFEC565F15330B3C7CB6E32F73495FAAC4 ();
// 0x000000B7 UnityEngine.Vector3 MyBox.MyVectors::ClampX(UnityEngine.Vector3,System.Single,System.Single)
extern void MyVectors_ClampX_mD8AC72EF245B37611FC5B0E88BE4010D9D5C7A27 ();
// 0x000000B8 UnityEngine.Vector2 MyBox.MyVectors::ClampX(UnityEngine.Vector2,System.Single,System.Single)
extern void MyVectors_ClampX_m49407AB34D9D8CD347F5A0FB760F97BA1D56AFAE ();
// 0x000000B9 System.Void MyBox.MyVectors::ClampX(UnityEngine.Transform,System.Single,System.Single)
extern void MyVectors_ClampX_mB46D31968CCC7CC8F0FD51A8AFF626696E99D524 ();
// 0x000000BA UnityEngine.Vector3 MyBox.MyVectors::ClampY(UnityEngine.Vector3,System.Single,System.Single)
extern void MyVectors_ClampY_m9F0CD24B82DD94BDE6BF5AC22D1B9D9FF50A7EC6 ();
// 0x000000BB UnityEngine.Vector2 MyBox.MyVectors::ClampY(UnityEngine.Vector2,System.Single,System.Single)
extern void MyVectors_ClampY_m6606018A0AE8F7F4130A6123CAB4A8A2875211CF ();
// 0x000000BC System.Void MyBox.MyVectors::ClampY(UnityEngine.Transform,System.Single,System.Single)
extern void MyVectors_ClampY_m5AECCB05FB08A966A5F2F430387549575EAE8E04 ();
// 0x000000BD UnityEngine.Vector2 MyBox.MyVectors::InvertX(UnityEngine.Vector2)
extern void MyVectors_InvertX_m61393D26DDFF02BF30EED962A88786B1D6526DFE ();
// 0x000000BE UnityEngine.Vector2 MyBox.MyVectors::InvertY(UnityEngine.Vector2)
extern void MyVectors_InvertY_mC65BE045E9C769688E938B7BA4B4708723109007 ();
// 0x000000BF UnityEngine.Vector2 MyBox.MyVectors::ToVector2(UnityEngine.Vector3)
extern void MyVectors_ToVector2_m4533FDA30303E30E98027265197B9C6DFBAEA973 ();
// 0x000000C0 UnityEngine.Vector3 MyBox.MyVectors::ToVector3(UnityEngine.Vector2)
extern void MyVectors_ToVector3_m14108CB9726C654001EC019F30E208881D2E5D88 ();
// 0x000000C1 UnityEngine.Vector2 MyBox.MyVectors::ToVector2(UnityEngine.Vector2Int)
extern void MyVectors_ToVector2_mE6DA4185E96FF4AB1EE91C5D14AB6BEF55FA009F ();
// 0x000000C2 UnityEngine.Vector3 MyBox.MyVectors::ToVector3(UnityEngine.Vector3Int)
extern void MyVectors_ToVector3_mFC59FC44B9F1E40DED7D36D8E2EF7C0262B16028 ();
// 0x000000C3 UnityEngine.Vector2Int MyBox.MyVectors::ToVector2Int(UnityEngine.Vector2)
extern void MyVectors_ToVector2Int_mE8816298A6E38090DE5F852F72F94F05B9DCF80F ();
// 0x000000C4 UnityEngine.Vector3Int MyBox.MyVectors::ToVector3Int(UnityEngine.Vector3)
extern void MyVectors_ToVector3Int_m7C26D60846396251D79C2A8A5EEAEF549FD2E6FD ();
// 0x000000C5 UnityEngine.Vector3 MyBox.MyVectors::SnapValue(UnityEngine.Vector3,System.Single)
extern void MyVectors_SnapValue_m9C024E98A4F30D0C14927094A342B0DC1CA377D9 ();
// 0x000000C6 UnityEngine.Vector2 MyBox.MyVectors::SnapValue(UnityEngine.Vector2,System.Single)
extern void MyVectors_SnapValue_mDA6E3A30A379D0AFCF0183777B8910AFC60BC55B ();
// 0x000000C7 System.Void MyBox.MyVectors::SnapPosition(UnityEngine.Transform,System.Single)
extern void MyVectors_SnapPosition_m689E20A17DABC72D299684A70B23913704EF787D ();
// 0x000000C8 UnityEngine.Vector2 MyBox.MyVectors::SnapToOne(UnityEngine.Vector2)
extern void MyVectors_SnapToOne_m82C97D0EE1CC2C253DB49C4EC06BD3544422429A ();
// 0x000000C9 UnityEngine.Vector3 MyBox.MyVectors::SnapToOne(UnityEngine.Vector3)
extern void MyVectors_SnapToOne_m2DAA7BFADC82A91F6826853DBD84A547D7D1FCE9 ();
// 0x000000CA UnityEngine.Vector3 MyBox.MyVectors::AverageVector(UnityEngine.Vector3[])
extern void MyVectors_AverageVector_m25AFCF739FA96B50329B142C9FFECAAA560E9BD8 ();
// 0x000000CB UnityEngine.Vector2 MyBox.MyVectors::AverageVector(UnityEngine.Vector2[])
extern void MyVectors_AverageVector_m6C972069155883801613ECED9C063B7FB9573A57 ();
// 0x000000CC System.Boolean MyBox.MyVectors::Approximately(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void MyVectors_Approximately_m0B046C0FDF2CD7FA67844B3242015BFD0AC9093E ();
// 0x000000CD System.Boolean MyBox.MyVectors::Approximately(UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern void MyVectors_Approximately_mE9D2508F314B9ADFAF3F8D3D5D49F51A2DC9E633 ();
// 0x000000CE UnityEngine.Vector3 MyBox.MyVectors::GetClosest(UnityEngine.Vector3,System.Collections.Generic.IEnumerable`1<UnityEngine.Vector3>)
extern void MyVectors_GetClosest_mC65FE761151E99EADC45796C6804D5A7F2EA4B21 ();
// 0x000000CF UnityEngine.Vector3 MyBox.MyVectors::GetClosest(System.Collections.Generic.IEnumerable`1<UnityEngine.Vector3>,UnityEngine.Vector3)
extern void MyVectors_GetClosest_m6434C27367A4D3971584D63BBC032F5870BFDD4D ();
// 0x000000D0 System.Void MyBox.TransformShakeExtension::StartShake(UnityEngine.Transform,System.Single,System.Single,System.Boolean,System.Boolean)
extern void TransformShakeExtension_StartShake_m68B7E3DC89CF5B41DCBE6734C6D7FC19E1FF527C ();
// 0x000000D1 System.Void MyBox.TransformShakeExtension::StopShake(UnityEngine.Transform)
extern void TransformShakeExtension_StopShake_mA5E29BFF43E9E5D2C9A9D0F6611071D9033ABF76 ();
// 0x000000D2 System.Collections.IEnumerator MyBox.TransformShakeExtension::TransformShakeCoroutine(UnityEngine.Transform,System.Single,System.Single,System.Boolean,System.Boolean)
extern void TransformShakeExtension_TransformShakeCoroutine_mBDC5AA849A446D1321E6CF40D3C29BC624AB1879 ();
// 0x000000D3 System.Void MyBox.TransformShakeExtension::BreakShakeIfAny(UnityEngine.Transform)
extern void TransformShakeExtension_BreakShakeIfAny_m2232906FB35D6BFDDB2F97528AFDF3740007A563 ();
// 0x000000D4 System.Void MyBox.TransformShakeExtension::.cctor()
extern void TransformShakeExtension__cctor_mD3922B87E0EB5268BF778E82B99FFC34FA45CCBB ();
// 0x000000D5 System.Boolean MyBox.IPrepare::Prepare()
// 0x000000D6 UnityEngine.Texture2D MyBox.ImageStringConverter::ImageFromString(System.String,System.Int32,System.Int32)
extern void ImageStringConverter_ImageFromString_m9CE371B5A46F1304448DB14DB565CC759BD44947 ();
// 0x000000D7 MyBox.MySceneBundle_TransferSceneBundleOption MyBox.MySceneBundle::get_SceneBundleTransferOption()
extern void MySceneBundle_get_SceneBundleTransferOption_mC2847344EF5FA05C90E3620007C7CD3D9D382854 ();
// 0x000000D8 System.Void MyBox.MySceneBundle::set_SceneBundleTransferOption(MyBox.MySceneBundle_TransferSceneBundleOption)
extern void MySceneBundle_set_SceneBundleTransferOption_m76C69C20B4B794A5F311C33534BD58022B9CD46F ();
// 0x000000D9 System.Void MyBox.MySceneBundle::.cctor()
extern void MySceneBundle__cctor_m7D2211842FB317DCEB328EA06BCD10CF73C4A022 ();
// 0x000000DA System.Void MyBox.MySceneBundle::PrepareSceneBundleForNextSceneByTransferOptions(UnityEngine.SceneManagement.Scene)
extern void MySceneBundle_PrepareSceneBundleForNextSceneByTransferOptions_m0AA4E1FA0B1EFC3D0C1ADD3B0E08E3D63E9FB42F ();
// 0x000000DB System.Boolean MyBox.MySceneBundle::IsUnloadingLastScene()
extern void MySceneBundle_IsUnloadingLastScene_mFC1FF40BB15C97CCA4E557AB5969CE05AF1E0BF8 ();
// 0x000000DC System.Void MyBox.MySceneBundle::PrepareSceneBundleForNextScene()
extern void MySceneBundle_PrepareSceneBundleForNextScene_m06B562C48CE3AEFC08C808CF306E7044B8E8F05D ();
// 0x000000DD System.Void MyBox.MySceneBundle::CarryOverCurrentBundleToNextBundle(System.Boolean)
extern void MySceneBundle_CarryOverCurrentBundleToNextBundle_mB868D1EABBD62A250D3BBFE94201DF3FCAC6852B ();
// 0x000000DE System.Void MyBox.MySceneBundle::AddStringDataToBundle(System.String,System.String,System.Boolean)
extern void MySceneBundle_AddStringDataToBundle_mFF60273961FDD9CE45C568A0F43F9954E1F5EF79 ();
// 0x000000DF System.Void MyBox.MySceneBundle::AddFloatDataToBundle(System.String,System.Single,System.Boolean)
extern void MySceneBundle_AddFloatDataToBundle_mDEFA9561D59DD2F19938E86B9AC964B02389B7DD ();
// 0x000000E0 System.Void MyBox.MySceneBundle::AddIntDataToBundle(System.String,System.Int32,System.Boolean)
extern void MySceneBundle_AddIntDataToBundle_m9E059484392AB57BB1F258CA4FB05859045C8516 ();
// 0x000000E1 System.Void MyBox.MySceneBundle::AddBoolDataToBundle(System.String,System.Boolean,System.Boolean)
extern void MySceneBundle_AddBoolDataToBundle_mA7743482E39FC17CC8B8257FEF098F9EC4601335 ();
// 0x000000E2 System.Void MyBox.MySceneBundle::AddObjectDataToBundle(System.String,System.Object,System.Boolean)
extern void MySceneBundle_AddObjectDataToBundle_mA3454FC315298AE3C59BCC9412F0B7D938FD07F9 ();
// 0x000000E3 System.Boolean MyBox.MySceneBundle::TryGetStringDataFromBundle(System.String,System.String&)
extern void MySceneBundle_TryGetStringDataFromBundle_mB80CA88180CAE7F897C6D053FE734EAF03A6936D ();
// 0x000000E4 System.Boolean MyBox.MySceneBundle::TryGetFloatDataFromBundle(System.String,System.Single&)
extern void MySceneBundle_TryGetFloatDataFromBundle_m62BD842DD41E44683718E87E206229292ABE47DD ();
// 0x000000E5 System.Boolean MyBox.MySceneBundle::TryGetIntDataFromBundle(System.String,System.Int32&)
extern void MySceneBundle_TryGetIntDataFromBundle_mCEB2ECFE6908FCD323BA53D1D6AA30CE1FAD1C82 ();
// 0x000000E6 System.Boolean MyBox.MySceneBundle::TryGetBoolDataFromBundle(System.String,System.Boolean&)
extern void MySceneBundle_TryGetBoolDataFromBundle_m09917906D472350D1807BA3D50704C4116FD1A1F ();
// 0x000000E7 System.Boolean MyBox.MySceneBundle::TryGetObjectDataFromBundle(System.String,System.Object&)
extern void MySceneBundle_TryGetObjectDataFromBundle_mFC016564CD23D7A2A240F4B0685DE105A981D8F5 ();
// 0x000000E8 System.Void MyBox.TimeTest::.ctor(System.String,System.Boolean)
extern void TimeTest__ctor_m919D5E2B492A148DB4527283B3602BED546EE3C5 ();
// 0x000000E9 System.Void MyBox.TimeTest::Dispose()
extern void TimeTest_Dispose_m4A40E7B0C96709AC5BBE31E9CD9B2155C3D27C79 ();
// 0x000000EA System.Void MyBox.TimeTest::Start(System.String,System.Boolean)
extern void TimeTest_Start_mA5FCD405766BBB0624B38C6810E76C977B7B3089 ();
// 0x000000EB System.Void MyBox.TimeTest::Pause()
extern void TimeTest_Pause_mD9BA3C6E8CBA29F5543EA9B16AA5EFA721EBEE7D ();
// 0x000000EC System.Void MyBox.TimeTest::Pause(System.String)
extern void TimeTest_Pause_mD6DBE6CD0AEB3F0E63CF6EC29BA6384F4D3C8775 ();
// 0x000000ED System.Void MyBox.TimeTest::End()
extern void TimeTest_End_m1C2783EF46659E232169E3E2332C582F8294F938 ();
// 0x000000EE System.Void MyBox.TimeTest::End(System.String)
extern void TimeTest_End_m15F58442AB63304EAFEB2185EA53F26CEB70DC5C ();
// 0x000000EF System.Void MyBox.TimeTest::.cctor()
extern void TimeTest__cctor_m1A45F109C821101058BFD3A18E500B443C9963C3 ();
// 0x000000F0 System.Int32 MyBox.MyPhysics::CircleOverlap2D(UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Single)
extern void MyPhysics_CircleOverlap2D_m4F89EDFAEEBE7F5816D5BEB47CBFC8FCD864DDE5 ();
// 0x000000F1 System.Int32 MyBox.MyPhysics::Raycast2D(UnityEngine.Vector2,UnityEngine.Vector2)
extern void MyPhysics_Raycast2D_m2D2377DAB6C757B464ADEE51DDC3FFA2910B1953 ();
// 0x000000F2 System.Int32 MyBox.MyPhysics::Raycast2D(UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern void MyPhysics_Raycast2D_m3232E944363D51C44A592D3D197E5942E0E7B535 ();
// 0x000000F3 System.Int32 MyBox.MyPhysics::Raycast2D(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32)
extern void MyPhysics_Raycast2D_m890A55A8E840CDE0488CD949F383BC6A991E16BC ();
// 0x000000F4 System.Int32 MyBox.MyPhysics::Raycast2D(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Single)
extern void MyPhysics_Raycast2D_mE66A7A1A2265D78AD3395E4806C2615F1796F469 ();
// 0x000000F5 System.Void MyBox.MyPhysics::.ctor()
extern void MyPhysics__ctor_m80A2188048A3FF52448B3627DE3CEC640726403B ();
// 0x000000F6 System.Void MyBox.MyPhysics::.cctor()
extern void MyPhysics__cctor_mE0F15A51CAA15597B5A7A7B2B393E232A25A82FF ();
// 0x000000F7 System.Void MyBox.ActiveStateOnStart::Awake()
extern void ActiveStateOnStart_Awake_m5B6B721436A91BD8ED42B86F8CBAC2C1181D9621 ();
// 0x000000F8 System.Void MyBox.ActiveStateOnStart::.ctor()
extern void ActiveStateOnStart__ctor_mCCA589A04675BD6ABFF588B10D3068C2FDDD8602 ();
// 0x000000F9 System.String MyBox.AnimationStateReference::get_StateName()
extern void AnimationStateReference_get_StateName_m39935A57C12D9E268DF49A7FAF34639D30B7B597 ();
// 0x000000FA System.Boolean MyBox.AnimationStateReference::get_Assigned()
extern void AnimationStateReference_get_Assigned_m73F4C536C01A3C61065AB1729ECADECFD702EE23 ();
// 0x000000FB UnityEngine.Animator MyBox.AnimationStateReference::get_Animator()
extern void AnimationStateReference_get_Animator_m269632A38716BDBB34060042D32994E0110DD6AB ();
// 0x000000FC System.Void MyBox.AnimationStateReference::.ctor()
extern void AnimationStateReference__ctor_m9E5EE00D7471071972A630559D2443563DD4922B ();
// 0x000000FD System.Void MyBox.AnimationStateReferenceExtension::Play(UnityEngine.Animator,MyBox.AnimationStateReference)
extern void AnimationStateReferenceExtension_Play_mFDF618D6BBE1B9D9DFAAD45C34BED15479AEFAEC ();
// 0x000000FE System.Void MyBox.AnimationStateReferenceExtension::Play(MyBox.AnimationStateReference)
extern void AnimationStateReferenceExtension_Play_m4EB8228BF8C94B1D8886137A24B100DD70B82F85 ();
// 0x000000FF System.Void MyBox.AssetFolderPath::.ctor()
extern void AssetFolderPath__ctor_mC4E7EE8C241799BE0262E90D3284C8D979DB5FA6 ();
// 0x00000100 MyBox.AssetPath MyBox.AssetPath::WithExtension(System.String)
extern void AssetPath_WithExtension_m3329F19C4A90DEDB5F1653D3D6CAAF5EDEA52432 ();
// 0x00000101 System.Void MyBox.AssetPath::.ctor()
extern void AssetPath__ctor_m8774E9C34165F1248DCAC6B1788B2030872D84CE ();
// 0x00000102 UnityEngine.Transform MyBox.Billboard::get_ActiveFacedObject()
extern void Billboard_get_ActiveFacedObject_m7B5C190F81D95A538438AC482E212539CDFB0789 ();
// 0x00000103 System.Void MyBox.Billboard::Update()
extern void Billboard_Update_mE629EE5C37CFD8D40006E221CA25623A6CAE6583 ();
// 0x00000104 System.Void MyBox.Billboard::.ctor()
extern void Billboard__ctor_m5BFB894F26E2678F66FBFC23683F2C61C87C2C0F ();
// 0x00000105 System.Void MyBox.CollectionWrapper`1::.ctor()
// 0x00000106 System.Void MyBox.ColliderGizmo::.ctor()
extern void ColliderGizmo__ctor_m1D031DA19CC51394FC688A568D8F62606A88112F ();
// 0x00000107 System.Void MyBox.ColliderToMesh::Start()
extern void ColliderToMesh_Start_m681050093065C88D4B797E10EC209D3E37DEA6D1 ();
// 0x00000108 System.Void MyBox.ColliderToMesh::FillShape()
extern void ColliderToMesh_FillShape_mE011B63A4E81A281F19AAD4985A8AD4901DE6FE1 ();
// 0x00000109 System.Void MyBox.ColliderToMesh::ClearShape()
extern void ColliderToMesh_ClearShape_mACD8A027EEF8F21BBE763A0808B8DF76295950A5 ();
// 0x0000010A System.Void MyBox.ColliderToMesh::.ctor()
extern void ColliderToMesh__ctor_mEF80C2736597467FABB57406F050AB71DC855F53 ();
// 0x0000010B System.Guid MyBox.GuidComponent::get_Guid()
extern void GuidComponent_get_Guid_mA86CBFB0FD8B4C4E26B0B3A0A0005318F1A61037 ();
// 0x0000010C System.String MyBox.GuidComponent::get_GuidString()
extern void GuidComponent_get_GuidString_mE6582DBF61A41FDB7436BD9C191C0D15CA14AAF6 ();
// 0x0000010D System.Boolean MyBox.GuidComponent::IsGuidAssigned()
extern void GuidComponent_IsGuidAssigned_mF44315223EA21962F3DC3062543EC5697F62632D ();
// 0x0000010E System.Void MyBox.GuidComponent::CreateGuid()
extern void GuidComponent_CreateGuid_mC6F4AB933B8E4A2B92EEA161DDB85FF5EEF07527 ();
// 0x0000010F System.Void MyBox.GuidComponent::OnBeforeSerialize()
extern void GuidComponent_OnBeforeSerialize_m71CE729B94D69B3A861E27CD96836D6B4D4ED63C ();
// 0x00000110 System.Void MyBox.GuidComponent::OnAfterDeserialize()
extern void GuidComponent_OnAfterDeserialize_m9A712F2E79BFCEDA2C63CA013FB819B87532A2E9 ();
// 0x00000111 System.Void MyBox.GuidComponent::Awake()
extern void GuidComponent_Awake_mAD4792E45AF145477E9B70936E160AEA47CF3A12 ();
// 0x00000112 System.Void MyBox.GuidComponent::OnValidate()
extern void GuidComponent_OnValidate_m64490110AB37F0FA2CD86246BFD100B118CD7994 ();
// 0x00000113 System.Guid MyBox.GuidComponent::GetGuid()
extern void GuidComponent_GetGuid_m94864A42872AFA2CD6408D85516DDE276ECB99E6 ();
// 0x00000114 System.Void MyBox.GuidComponent::OnDestroy()
extern void GuidComponent_OnDestroy_m285D613AD4BE4B0F320C7EE37326B12BE52A433E ();
// 0x00000115 System.Void MyBox.GuidComponent::.ctor()
extern void GuidComponent__ctor_mCDAF38CFBDF2BC6F2FACE1B0E5AA6DD280F6E990 ();
// 0x00000116 System.Boolean MyBox.GuidManager::Add(MyBox.GuidComponent)
extern void GuidManager_Add_m0F1C1E40C522169ADC90202105101B993976D372 ();
// 0x00000117 System.Void MyBox.GuidManager::Remove(System.Guid)
extern void GuidManager_Remove_m3BF1B6DB5C0B33C320DC7C4BED8B58E0804126C5 ();
// 0x00000118 UnityEngine.GameObject MyBox.GuidManager::ResolveGuid(System.Guid,System.Action`1<UnityEngine.GameObject>,System.Action)
extern void GuidManager_ResolveGuid_mA40B172A1431BE4988290B1563046D584987775A ();
// 0x00000119 UnityEngine.GameObject MyBox.GuidManager::ResolveGuid(System.Guid,System.Action)
extern void GuidManager_ResolveGuid_mC20925311D8CCE797952852B2D9D94E4895CAC33 ();
// 0x0000011A UnityEngine.GameObject MyBox.GuidManager::ResolveGuid(System.Guid)
extern void GuidManager_ResolveGuid_m56B40C624416EAC95462BA1DE24F7694E8F26F5C ();
// 0x0000011B System.Void MyBox.GuidManager::.ctor()
extern void GuidManager__ctor_m3B3C35B84623213BB9BC0840CEEB72D0834F2DF4 ();
// 0x0000011C System.Boolean MyBox.GuidManager::InternalAdd(MyBox.GuidComponent)
extern void GuidManager_InternalAdd_m98DF543138ABDC74786CE298F8E2B5D49EB864CB ();
// 0x0000011D System.Void MyBox.GuidManager::InternalRemove(System.Guid)
extern void GuidManager_InternalRemove_mE8CA2FFFCE12C21C8ED33DFF05809332CE0FE95C ();
// 0x0000011E UnityEngine.GameObject MyBox.GuidManager::ResolveGuidInternal(System.Guid,System.Action`1<UnityEngine.GameObject>,System.Action)
extern void GuidManager_ResolveGuidInternal_m2E924BC0582B06145DFD609DD46B4A936CEC44C0 ();
// 0x0000011F System.Void MyBox.GuidReference::add_OnGuidAdded(System.Action`1<UnityEngine.GameObject>)
extern void GuidReference_add_OnGuidAdded_m1DC92FA0025FD96682D768D391497B99D0FCA40A ();
// 0x00000120 System.Void MyBox.GuidReference::remove_OnGuidAdded(System.Action`1<UnityEngine.GameObject>)
extern void GuidReference_remove_OnGuidAdded_m9FD815F908907DF18B14EBE78167D59392B8A3D7 ();
// 0x00000121 System.Void MyBox.GuidReference::add_OnGuidRemoved(System.Action)
extern void GuidReference_add_OnGuidRemoved_m65086DA71AACB2B7DC3C3E57170882BE2E8DB495 ();
// 0x00000122 System.Void MyBox.GuidReference::remove_OnGuidRemoved(System.Action)
extern void GuidReference_remove_OnGuidRemoved_mBDE606128B9A0F5B0900C9EA359C000C30515EE9 ();
// 0x00000123 UnityEngine.GameObject MyBox.GuidReference::get_gameObject()
extern void GuidReference_get_gameObject_m3364F58B5292006343066D32F9260BD3F080FE50 ();
// 0x00000124 System.Void MyBox.GuidReference::set_gameObject(UnityEngine.GameObject)
extern void GuidReference_set_gameObject_m631807E386355C61F835F3B5EE1CF8CA2ABAF6A9 ();
// 0x00000125 System.Void MyBox.GuidReference::.ctor()
extern void GuidReference__ctor_m0A88BD85C1F2795E05B43768A16393A94B26F1FD ();
// 0x00000126 System.Void MyBox.GuidReference::.ctor(MyBox.GuidComponent)
extern void GuidReference__ctor_mACA70397C506E81938C1FEEBDF6D6893FB3B429D ();
// 0x00000127 System.Void MyBox.GuidReference::GuidAdded(UnityEngine.GameObject)
extern void GuidReference_GuidAdded_mD4C897A88908182D459B2913152B76FD9D0A14DC ();
// 0x00000128 System.Void MyBox.GuidReference::GuidRemoved()
extern void GuidReference_GuidRemoved_m95ECE510A40463DBCD6FA88673ABCEAB1AF47887 ();
// 0x00000129 System.Void MyBox.GuidReference::OnBeforeSerialize()
extern void GuidReference_OnBeforeSerialize_mF58E26CEB7887021A3E6B432C404576375B9A9E2 ();
// 0x0000012A System.Void MyBox.GuidReference::OnAfterDeserialize()
extern void GuidReference_OnAfterDeserialize_m9EBCD04D6950A4F79C1068AEA6F3F8E06D4441D7 ();
// 0x0000012B System.Void MyBox.MinMaxFloat::.ctor(System.Single,System.Single)
extern void MinMaxFloat__ctor_mC18A650D47FA638AE253451E25B568620E764761_AdjustorThunk ();
// 0x0000012C System.Void MyBox.MinMaxInt::.ctor(System.Int32,System.Int32)
extern void MinMaxInt__ctor_m3B2A4FE3346665B5555E814D467EF71BCC6FB7D7_AdjustorThunk ();
// 0x0000012D System.Int32 MyBox.MinMaxExtensions::Clamp(MyBox.MinMaxInt,System.Int32)
extern void MinMaxExtensions_Clamp_mA37A08DF891DC56A011CD3F24BFF66578D7B8A29 ();
// 0x0000012E System.Single MyBox.MinMaxExtensions::Clamp(MyBox.MinMaxFloat,System.Single)
extern void MinMaxExtensions_Clamp_m5DCED77D3FC862D97B871D79E434E9FD4509AECF ();
// 0x0000012F System.Int32 MyBox.MinMaxExtensions::Length(MyBox.MinMaxInt)
extern void MinMaxExtensions_Length_mC760CB5D0AC7291646FB9BEF49679229B05C7CBD ();
// 0x00000130 System.Single MyBox.MinMaxExtensions::Length(MyBox.MinMaxFloat)
extern void MinMaxExtensions_Length_m10DA304D4AA2D8013B46547859D2400D9BC7DFBB ();
// 0x00000131 System.Int32 MyBox.MinMaxExtensions::MidPoint(MyBox.MinMaxInt)
extern void MinMaxExtensions_MidPoint_mC4AD6226C3A7F86111F6EAE87BE6EEAB14052362 ();
// 0x00000132 System.Single MyBox.MinMaxExtensions::MidPoint(MyBox.MinMaxFloat)
extern void MinMaxExtensions_MidPoint_m3BFAF42A3D2BC540DDF61ED7F4D5831E066AAEC0 ();
// 0x00000133 System.Single MyBox.MinMaxExtensions::Lerp(MyBox.MinMaxInt,System.Single)
extern void MinMaxExtensions_Lerp_m7F61D464BD1A125AE57B5BA8FB3BDC3BC0844532 ();
// 0x00000134 System.Single MyBox.MinMaxExtensions::Lerp(MyBox.MinMaxFloat,System.Single)
extern void MinMaxExtensions_Lerp_m1753C0AA26EC76F97E3039BF79ABDC3F4DF0DD7B ();
// 0x00000135 System.Single MyBox.MinMaxExtensions::LerpUnclamped(MyBox.MinMaxInt,System.Single)
extern void MinMaxExtensions_LerpUnclamped_m871A6B492BB7AEAC0EF7792BB8DF885D502EDF97 ();
// 0x00000136 System.Single MyBox.MinMaxExtensions::LerpUnclamped(MyBox.MinMaxFloat,System.Single)
extern void MinMaxExtensions_LerpUnclamped_m113B61CFC21764D5296533842F389D571B41CDF1 ();
// 0x00000137 System.Single MyBox.MinMaxExtensions::RandomInRange(MyBox.MinMaxInt)
extern void MinMaxExtensions_RandomInRange_m2609BEE398A6FB54B01C0FC7C0E53D25794C8CB0 ();
// 0x00000138 System.Single MyBox.MinMaxExtensions::RandomInRange(MyBox.MinMaxFloat)
extern void MinMaxExtensions_RandomInRange_m8249966E719EC550F27C76A056C6539B2A4D7AF7 ();
// 0x00000139 T MyBox.MonoSingleton`1::get_Instance()
// 0x0000013A System.Void MyBox.MonoSingleton`1::Awake()
// 0x0000013B System.Void MyBox.MonoSingleton`1::OnAwake()
// 0x0000013C System.Void MyBox.MonoSingleton`1::.ctor()
// 0x0000013D System.Void MyBox.MyCursor::ApplyAsLockedCursor()
extern void MyCursor_ApplyAsLockedCursor_m21BDFEA45287C55CCE304FAD204E0C101862B520_AdjustorThunk ();
// 0x0000013E System.Void MyBox.MyCursor::ApplyAsFreeCursor()
extern void MyCursor_ApplyAsFreeCursor_mC0158F78C8D36BA1F45A601D9064281B66587E0B_AdjustorThunk ();
// 0x0000013F System.Void MyBox.MyCursor::ApplyAsConfinedCursor()
extern void MyCursor_ApplyAsConfinedCursor_m68183A3FF74BC69C5EDA1A5008087797A1F308C3_AdjustorThunk ();
// 0x00000140 System.Collections.Generic.Dictionary`2<TKey,TValue> MyBox.MyDictionary`2::get_AsDictionary()
// 0x00000141 System.Int32 MyBox.MyDictionary`2::get_Count()
// 0x00000142 TValue MyBox.MyDictionary`2::get_Item(TKey,TValue)
// 0x00000143 TValue MyBox.MyDictionary`2::get_Item(TKey)
// 0x00000144 System.Void MyBox.MyDictionary`2::set_Item(TKey,TValue)
// 0x00000145 System.Void MyBox.MyDictionary`2::.ctor()
// 0x00000146 System.Void MyBox.MyDictionary`2::.ctor(System.Int32)
// 0x00000147 System.Void MyBox.MyDictionary`2::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x00000148 System.Void MyBox.MyDictionary`2::.ctor(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x00000149 System.Void MyBox.MyDictionary`2::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
// 0x0000014A System.Void MyBox.MyDictionary`2::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x0000014B System.Boolean MyBox.MyDictionary`2::ContainsValue(TValue)
// 0x0000014C System.Boolean MyBox.MyDictionary`2::ContainsKey(TKey)
// 0x0000014D System.Void MyBox.MyDictionary`2::Clear()
// 0x0000014E System.Void MyBox.MyDictionary`2::Add(TKey,TValue)
// 0x0000014F System.Void MyBox.MyDictionary`2::Resize(System.Int32,System.Boolean)
// 0x00000150 System.Void MyBox.MyDictionary`2::Resize()
// 0x00000151 System.Boolean MyBox.MyDictionary`2::Remove(TKey)
// 0x00000152 System.Void MyBox.MyDictionary`2::Insert(TKey,TValue,System.Boolean)
// 0x00000153 System.Void MyBox.MyDictionary`2::Initialize(System.Int32)
// 0x00000154 System.Int32 MyBox.MyDictionary`2::FindIndex(TKey)
// 0x00000155 System.Boolean MyBox.MyDictionary`2::TryGetValue(TKey,TValue&)
// 0x00000156 System.Collections.Generic.ICollection`1<TKey> MyBox.MyDictionary`2::get_Keys()
// 0x00000157 System.Collections.Generic.ICollection`1<TValue> MyBox.MyDictionary`2::get_Values()
// 0x00000158 System.Void MyBox.MyDictionary`2::Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
// 0x00000159 System.Boolean MyBox.MyDictionary`2::Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
// 0x0000015A System.Void MyBox.MyDictionary`2::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
// 0x0000015B System.Boolean MyBox.MyDictionary`2::get_IsReadOnly()
// 0x0000015C System.Boolean MyBox.MyDictionary`2::Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
// 0x0000015D MyBox.MyDictionary`2_Enumerator<TKey,TValue> MyBox.MyDictionary`2::GetEnumerator()
// 0x0000015E System.Collections.IEnumerator MyBox.MyDictionary`2::System.Collections.IEnumerable.GetEnumerator()
// 0x0000015F System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> MyBox.MyDictionary`2::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
// 0x00000160 MyBox.OptionalFloat MyBox.OptionalFloat::WithValue(System.Single)
extern void OptionalFloat_WithValue_m684A9D8F90A17E937FC2C7F4730563BF0EA8D070 ();
// 0x00000161 System.Void MyBox.OptionalFloat::.ctor()
extern void OptionalFloat__ctor_mC35934E753D4278C3AC2FEE1B27734623C13EC40 ();
// 0x00000162 MyBox.OptionalInt MyBox.OptionalInt::WithValue(System.Int32)
extern void OptionalInt_WithValue_m4FA80C58B5EF2E84A766D46DF5F52A52D01CE8A3 ();
// 0x00000163 System.Void MyBox.OptionalInt::.ctor()
extern void OptionalInt__ctor_m1CA3096DF41C9489372F18ED29B4539A26301CC3 ();
// 0x00000164 MyBox.OptionalString MyBox.OptionalString::WithValue(System.String)
extern void OptionalString_WithValue_m32CA0515EF734A0AFD87AE410812828A73E18999 ();
// 0x00000165 System.Void MyBox.OptionalString::.ctor()
extern void OptionalString__ctor_m190507886D56982B40EFE1F332216B5F7EDDCF9D ();
// 0x00000166 MyBox.OptionalKeyCode MyBox.OptionalKeyCode::WithValue(UnityEngine.KeyCode)
extern void OptionalKeyCode_WithValue_m4C0FB1A170AE7EE654E8513867EBFAFF3E1E149A ();
// 0x00000167 System.Void MyBox.OptionalKeyCode::.ctor()
extern void OptionalKeyCode__ctor_mCBEC6AF748D10C4D32F477747A241EAE687860F5 ();
// 0x00000168 System.Void MyBox.OptionalGameObject::.ctor()
extern void OptionalGameObject__ctor_m333AF58F6BBC6E448E3F9E07B5EB8B36A6854465 ();
// 0x00000169 System.Void MyBox.OptionalComponent::.ctor()
extern void OptionalComponent__ctor_m0C2271D0052B92F9DC8B4110DBEA83AAF3B0230F ();
// 0x0000016A System.Single MyBox.OptionalMinMax::GetFixed(System.Single)
extern void OptionalMinMax_GetFixed_mEA05EC0969CAFA01EDDF19ADCE8E8E69E3A592CD_AdjustorThunk ();
// 0x0000016B System.Void MyBox.OptionalMinMax::.ctor(System.Boolean,System.Boolean,System.Single,System.Single)
extern void OptionalMinMax__ctor_m5F655298C0C9D85DCEF347D2D097B07EBD37A9B6_AdjustorThunk ();
// 0x0000016C System.Void MyBox.ReorderableGameObject::.ctor()
extern void ReorderableGameObject__ctor_m336FA8C53BBD51C6BE976A7414C764F069DF0D08 ();
// 0x0000016D System.Void MyBox.ReorderableGameObjectList::.ctor()
extern void ReorderableGameObjectList__ctor_m123C7312A579EB137365A70D072E0DBDE7836340 ();
// 0x0000016E System.Void MyBox.ReorderableTransform::.ctor()
extern void ReorderableTransform__ctor_m81B1C5CD4556901BED022C55D9BD8CC52C2C2EFB ();
// 0x0000016F System.Void MyBox.ReorderableTransformList::.ctor()
extern void ReorderableTransformList__ctor_mBBDBE4E3CC12E47C1F5558B0EDFA94FD6786A5A7 ();
// 0x00000170 System.Int32 MyBox.Reorderable`1::get_Length()
// 0x00000171 T MyBox.Reorderable`1::get_Item(System.Int32)
// 0x00000172 System.Void MyBox.Reorderable`1::set_Item(System.Int32,T)
// 0x00000173 System.Void MyBox.Reorderable`1::.ctor()
// 0x00000174 System.Void MyBox.ReorderableList`1::.ctor()
// 0x00000175 System.Boolean MyBox.SceneReference::get_IsAssigned()
extern void SceneReference_get_IsAssigned_m6E44A8A337B6F6AF530BDD35258F000BD275C528 ();
// 0x00000176 System.Void MyBox.SceneReference::ValidateScene()
extern void SceneReference_ValidateScene_m8A0B2C6C268D7C937678DEB02A27D93CCB23D956 ();
// 0x00000177 System.Void MyBox.SceneReference::LoadScene(UnityEngine.SceneManagement.LoadSceneMode)
extern void SceneReference_LoadScene_m2FF5EDF4B15F8AAED899179508DDCF8D709E646E ();
// 0x00000178 UnityEngine.AsyncOperation MyBox.SceneReference::LoadSceneAsync(UnityEngine.SceneManagement.LoadSceneMode)
extern void SceneReference_LoadSceneAsync_m20E7774DD70F7FED0703AE8FC4850DDFF7000C51 ();
// 0x00000179 System.Void MyBox.SceneReference::OnBeforeSerialize()
extern void SceneReference_OnBeforeSerialize_m65A973515142B1355CD58D2A50399BAD5B53675C ();
// 0x0000017A System.Void MyBox.SceneReference::OnAfterDeserialize()
extern void SceneReference_OnAfterDeserialize_mDB4620AB2B05CE3CF7ABB7E048D6AC42749D6763 ();
// 0x0000017B System.Void MyBox.SceneReference::.ctor()
extern void SceneReference__ctor_m01FCEE5E570E41002B823884A3111B2F1890100A ();
// 0x0000017C T MyBox.Singleton`1::get_Instance()
// 0x0000017D System.Void MyBox.Singleton`1::.ctor()
// 0x0000017E System.Boolean MyBox.UIFollow::get_IsOffscreen()
extern void UIFollow_get_IsOffscreen_mCC8C9D86F15B120A356D6704E6792AACDB053B3E ();
// 0x0000017F UnityEngine.RectTransform MyBox.UIFollow::get_Transform()
extern void UIFollow_get_Transform_mC07145D14A73C89F49E88488E2E1893BBD261CA2 ();
// 0x00000180 UnityEngine.Vector2 MyBox.UIFollow::get_OffscreenOffset()
extern void UIFollow_get_OffscreenOffset_m9274B4C7E7131D605D67F5D829C6B3463B086B2B ();
// 0x00000181 System.Void MyBox.UIFollow::LateUpdate()
extern void UIFollow_LateUpdate_m871739453FAC2661771257E8E5DBDA1E50B7F696 ();
// 0x00000182 System.Void MyBox.UIFollow::ToggleCanvasOffscreen()
extern void UIFollow_ToggleCanvasOffscreen_mDC588698CF908A983D5BF54DC95394A034473684 ();
// 0x00000183 System.Void MyBox.UIFollow::.ctor()
extern void UIFollow__ctor_mF90B0DC994DF365FD7DED3C8F073B97CD11729FB ();
// 0x00000184 System.Boolean MyBox.UIImageBasedButton::get_AlternativeSpriteset()
extern void UIImageBasedButton_get_AlternativeSpriteset_m54B6B6B0AD4788FE12639989436D654464832F8C ();
// 0x00000185 System.Void MyBox.UIImageBasedButton::set_AlternativeSpriteset(System.Boolean)
extern void UIImageBasedButton_set_AlternativeSpriteset_m3DDF51EA95D5E1A6DF2E137AFA58353B5AEEFD02 ();
// 0x00000186 System.Void MyBox.UIImageBasedButton::Awake()
extern void UIImageBasedButton_Awake_m9E430554EBF16B600DE2712FB5346870B3437A9C ();
// 0x00000187 System.Void MyBox.UIImageBasedButton::OnEnable()
extern void UIImageBasedButton_OnEnable_mEC05A6C3BA15ED5D597CAA471B87F452AA84390B ();
// 0x00000188 System.Void MyBox.UIImageBasedButton::OnDisable()
extern void UIImageBasedButton_OnDisable_mD9E9794FB05AC82E9728D79CEADB02F8BA5E8FA2 ();
// 0x00000189 System.Void MyBox.UIImageBasedButton::ToggleSprites()
extern void UIImageBasedButton_ToggleSprites_m968169A48DF0DC78ED7946DBECAF60333C46F207 ();
// 0x0000018A System.Void MyBox.UIImageBasedButton::UpdateSprites()
extern void UIImageBasedButton_UpdateSprites_m06798558800E9551E4547CEBE53C1E26C90E19CD ();
// 0x0000018B System.Void MyBox.UIImageBasedButton::UpdateSprites(System.Boolean)
extern void UIImageBasedButton_UpdateSprites_m495685E8B67918B40DE8040F77A800172BBD588F ();
// 0x0000018C System.Void MyBox.UIImageBasedButton::OnSelect(UnityEngine.EventSystems.BaseEventData)
extern void UIImageBasedButton_OnSelect_mBD29DC04AC34BDC7C6A06C079A531D3FFE6F243F ();
// 0x0000018D System.Void MyBox.UIImageBasedButton::OnDeselect(UnityEngine.EventSystems.BaseEventData)
extern void UIImageBasedButton_OnDeselect_m937ABBABAD763B378602B777AAB47DBA74A4AF35 ();
// 0x0000018E System.Void MyBox.UIImageBasedButton::.ctor()
extern void UIImageBasedButton__ctor_mAF10D5F703F043DA23B0B308522365145D55217D ();
// 0x0000018F System.Void MyBox.UIImageBasedToggle::Start()
extern void UIImageBasedToggle_Start_m56B155E42643B804254E4F156339C949E199A693 ();
// 0x00000190 System.Void MyBox.UIImageBasedToggle::.ctor()
extern void UIImageBasedToggle__ctor_mB58E4CDD03C7949F6360E29AC729C85742F97E05 ();
// 0x00000191 System.Void MyBox.UIImageBasedToggle::<Start>b__7_0(System.Boolean)
extern void UIImageBasedToggle_U3CStartU3Eb__7_0_mACDDEAD6982E745BC39D3D4B99339C861C87F909 ();
// 0x00000192 System.Void MyBox.UIRelativePosition::Start()
extern void UIRelativePosition_Start_m4274E303A4D0BB4FF6C3B20AEFBDD756EE11E967 ();
// 0x00000193 System.Void MyBox.UIRelativePosition::LateUpdate()
extern void UIRelativePosition_LateUpdate_m11AD7EC6D9A45739D337C34296B2F068D704379F ();
// 0x00000194 System.Void MyBox.UIRelativePosition::.ctor()
extern void UIRelativePosition__ctor_m5BC7742BF6DA36065E0221B4DDA9ADEB503558FB ();
// 0x00000195 System.Void MyBox.UISizeBy::Start()
extern void UISizeBy_Start_m27F2357316075DC7872A7C390AF6DE9A3CA453C4 ();
// 0x00000196 System.Void MyBox.UISizeBy::LateUpdate()
extern void UISizeBy_LateUpdate_m9C86874746DC6DE8A12BDCD56F417BFFAE124489 ();
// 0x00000197 System.Void MyBox.UISizeBy::.ctor()
extern void UISizeBy__ctor_mE994E4B2FDD836F2E7D8F69B37A7E93C0ECD858A ();
// 0x00000198 System.Boolean MyBox.WaitForUnscaledSeconds::get_keepWaiting()
extern void WaitForUnscaledSeconds_get_keepWaiting_m098BE8DE9410178DB7798D73C76941B4960FD06C ();
// 0x00000199 System.Void MyBox.WaitForUnscaledSeconds::.ctor(System.Single)
extern void WaitForUnscaledSeconds__ctor_m87090939FDDBA544608DFF76C6E36AC0636C1379 ();
// 0x0000019A System.Void MyBox.Internal.AttributeBase::.ctor()
extern void AttributeBase__ctor_m742493FCE3450438182373229D1187A23E4DE724 ();
// 0x0000019B System.Void MyBox.Internal.CoroutineOwner::.ctor()
extern void CoroutineOwner__ctor_mD471960B33AA8DC0CB07B34A27AB4C760F7EE1F6 ();
// 0x0000019C System.Void MyBox.Internal.SceneBundle::.ctor()
extern void SceneBundle__ctor_mCD0025603FF33E97733D6BFE060F618D66499328 ();
// 0x0000019D MyBox.Internal.Bundle`1<System.String> MyBox.Internal.SceneBundle::get_StringData()
extern void SceneBundle_get_StringData_m62AB996F148B5138AA11AF621DC5B3340BB31E95 ();
// 0x0000019E MyBox.Internal.Bundle`1<System.Single> MyBox.Internal.SceneBundle::get_FloatData()
extern void SceneBundle_get_FloatData_m832BEB7033140016B9452BE307B8831F600D76B7 ();
// 0x0000019F MyBox.Internal.Bundle`1<System.Int32> MyBox.Internal.SceneBundle::get_IntData()
extern void SceneBundle_get_IntData_m8E57D39AD8172D7847E3D6EFF34D1B1227F7DF38 ();
// 0x000001A0 MyBox.Internal.Bundle`1<System.Boolean> MyBox.Internal.SceneBundle::get_BoolData()
extern void SceneBundle_get_BoolData_m476F31D14CAA80B988EFF6130FFAF89746809F9A ();
// 0x000001A1 MyBox.Internal.Bundle`1<System.Object> MyBox.Internal.SceneBundle::get_ObjectData()
extern void SceneBundle_get_ObjectData_m71B978EBCF786B3B554FF2CD54297876E137A081 ();
// 0x000001A2 System.Void MyBox.Internal.Bundle`1::.ctor()
// 0x000001A3 System.Void MyBox.Internal.Bundle`1::AddData(System.String,T,System.Boolean)
// 0x000001A4 System.Void MyBox.Internal.Bundle`1::AddData(System.Collections.Generic.KeyValuePair`2<System.String,T>,System.Boolean)
// 0x000001A5 System.Boolean MyBox.Internal.Bundle`1::TryGetData(System.String,T&)
// 0x000001A6 System.Boolean MyBox.Internal.Bundle`1::DataExists(System.String)
// 0x000001A7 System.Collections.Generic.Dictionary`2<System.String,T> MyBox.Internal.Bundle`1::GetBundleData()
// 0x000001A8 System.Void MyBox.Internal.Bundle`1::AddBundleData(System.Collections.Generic.Dictionary`2<System.String,T>,System.Boolean)
// 0x000001A9 System.Void MyBox.Internal.Bundle`1::AddBundleData(MyBox.Internal.Bundle`1<T>,System.Boolean)
// 0x000001AA System.Boolean MyBox.Internal.GLDraw::ClipTest(System.Single,System.Single,System.Single&,System.Single&)
extern void GLDraw_ClipTest_mDBF817FFC5A42844B52CEF75D93598E1B3A16B0C ();
// 0x000001AB System.Boolean MyBox.Internal.GLDraw::SegmentRectIntersection(UnityEngine.Rect,UnityEngine.Vector2&,UnityEngine.Vector2&)
extern void GLDraw_SegmentRectIntersection_mCFC384446A6832CAC13CE1DF372CB2A9D270DB09 ();
// 0x000001AC System.Void MyBox.Internal.GLDraw::BeginGroup(UnityEngine.Rect)
extern void GLDraw_BeginGroup_mF0D8AE8525734A580B3BB18ABDFB3895DA372267 ();
// 0x000001AD System.Void MyBox.Internal.GLDraw::EndGroup()
extern void GLDraw_EndGroup_m3B81E3B8788064835D5060AB9BFC71DE84D24A71 ();
// 0x000001AE System.Void MyBox.Internal.GLDraw::CreateMaterial()
extern void GLDraw_CreateMaterial_mD9494853CD9C35CB85AC41BF6FAC335BABB53C3C ();
// 0x000001AF System.Void MyBox.Internal.GLDraw::DrawLine(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Color,System.Single)
extern void GLDraw_DrawLine_m34AAB5BC05AA64790735E6024A939697B71EEF08 ();
// 0x000001B0 System.Void MyBox.Internal.GLDraw::DrawBox(UnityEngine.Rect,UnityEngine.Color,System.Single)
extern void GLDraw_DrawBox_mBA5A8535770885F005B7A58384FCA2C77F1FAC67 ();
// 0x000001B1 System.Void MyBox.Internal.GLDraw::DrawBox(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Color,System.Single)
extern void GLDraw_DrawBox_m58691F75A189039BC9FB3978C86AEF36A9690326 ();
// 0x000001B2 System.Void MyBox.Internal.GLDraw::DrawRoundedBox(UnityEngine.Rect,System.Single,UnityEngine.Color,System.Single)
extern void GLDraw_DrawRoundedBox_m6DA67A71279FB9E6C7F89D6EC6A211208FF23A62 ();
// 0x000001B3 System.Void MyBox.Internal.GLDraw::DrawConnectingCurve(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Color,System.Single)
extern void GLDraw_DrawConnectingCurve_m0E9A5256D82D781EB85040A705A4E5F502A5C7C8 ();
// 0x000001B4 System.Void MyBox.Internal.GLDraw::DrawBezier(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Color,System.Single)
extern void GLDraw_DrawBezier_m7A467516AF23890406D0E193CD79561B62CB16C6 ();
// 0x000001B5 System.Void MyBox.Internal.GLDraw::DrawBezier(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Color,System.Single,System.Int32)
extern void GLDraw_DrawBezier_m1E024FFE64F42A1D68452A03C9B6E983D8ECBC85 ();
// 0x000001B6 UnityEngine.Vector2 MyBox.Internal.GLDraw::CubeBezier(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern void GLDraw_CubeBezier_m171FDB596EB9E1CD1A57F21D389310032068CB85 ();
// 0x000001B7 System.Void MyBox.Internal.GLDraw::.ctor()
extern void GLDraw__ctor_m084F766934ED763B80C62AE52B542F159E400B31 ();
// 0x000001B8 System.String MyBox.Internal.MyLogger::get_Session()
extern void MyLogger_get_Session_mA0B76556AE6F9C907E083F20DC099E0E371FA72F ();
// 0x000001B9 System.Void MyBox.Internal.MyLogger::set_Session(System.String)
extern void MyLogger_set_Session_m446DF83F42CB5DB70FD7556E7FC3CD5E4A9EF075 ();
// 0x000001BA System.String MyBox.Internal.MyLogger::get_Version()
extern void MyLogger_get_Version_m6654665AD3FB2A991894A6614DDB08CB099A6B98 ();
// 0x000001BB System.Void MyBox.Internal.MyLogger::set_Version(System.String)
extern void MyLogger_set_Version_mFC8F3FC0FA648B9F202403159DE1BA86FAEFB419 ();
// 0x000001BC System.Void MyBox.Internal.MyLogger::.cctor()
extern void MyLogger__cctor_m6C681F1E84464A3D3FEDEE2044D6466663A1E287 ();
// 0x000001BD System.Void MyBox.Internal.MyLogger::Log(System.String)
extern void MyLogger_Log_m7FED437699EF97BFC9C1A6DB339CBF1A57D4BFD9 ();
// 0x000001BE System.String MyBox.Internal.MyLogger::GetCurrentTime()
extern void MyLogger_GetCurrentTime_mE32E6D23E811A8CCCD9CF16522F344907D3E4F55 ();
// 0x000001BF System.Void MyBox.Internal.MyLogger::Log(System.Exception)
extern void MyLogger_Log_m914B2AF381AAA432C2C6F01F219CD60D796B54CA ();
// 0x000001C0 System.Void MyBox.Internal.CollectionWrapperBase::.ctor()
extern void CollectionWrapperBase__ctor_m08594901DF9A3BEBAA364C7D0CD27D117E072A3E ();
// 0x000001C1 System.Void MyBox.Internal.CommentaryComponent::.ctor()
extern void CommentaryComponent__ctor_m9FF1D1D5FA7AA4D81639CDB4D02C51A0C0AD24B7 ();
// 0x000001C2 System.Void MyBox.Internal.Optional`1::.ctor()
// 0x000001C3 System.Void MyBox.Internal.OptionalParent::.ctor()
extern void OptionalParent__ctor_m405E7B28CFB8E3F4DB1CEE9421FCC4485CF1D53B ();
// 0x000001C4 System.Void MyBox.Internal.ReorderableBase::.ctor()
extern void ReorderableBase__ctor_mB6B0254A16026BFA84D929B8B9EFE284E9F718AB ();
// 0x000001C5 System.Void MyBox.ConditionalFieldAttribute_<>c::.cctor()
extern void U3CU3Ec__cctor_m54F5E9394BDD5322BEA8DDB257616766A57A39DD ();
// 0x000001C6 System.Void MyBox.ConditionalFieldAttribute_<>c::.ctor()
extern void U3CU3Ec__ctor_m474ABB4F8F02CD667A1743215517328571000A35 ();
// 0x000001C7 System.String MyBox.ConditionalFieldAttribute_<>c::<.ctor>b__3_0(System.Object)
extern void U3CU3Ec_U3C_ctorU3Eb__3_0_mE035A4D581B6A968960909135292244B33452CC0 ();
// 0x000001C8 System.Void MyBox.MyCoroutines_<StartNextCoroutine>d__8::.ctor(System.Int32)
extern void U3CStartNextCoroutineU3Ed__8__ctor_m4EA7B5DAC2C77CB6DCB634E3B1C3ADE9CF88098E ();
// 0x000001C9 System.Void MyBox.MyCoroutines_<StartNextCoroutine>d__8::System.IDisposable.Dispose()
extern void U3CStartNextCoroutineU3Ed__8_System_IDisposable_Dispose_mB67CCC57402558E537533D1E593DF02272C747FA ();
// 0x000001CA System.Boolean MyBox.MyCoroutines_<StartNextCoroutine>d__8::MoveNext()
extern void U3CStartNextCoroutineU3Ed__8_MoveNext_m4FA9630E2D51852EE797E4ED3B35A24718F7122C ();
// 0x000001CB System.Object MyBox.MyCoroutines_<StartNextCoroutine>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartNextCoroutineU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAF0E338B962405C9D7605BC847A294D9CA9C010F ();
// 0x000001CC System.Void MyBox.MyCoroutines_<StartNextCoroutine>d__8::System.Collections.IEnumerator.Reset()
extern void U3CStartNextCoroutineU3Ed__8_System_Collections_IEnumerator_Reset_mAF32506342041BC3FEAD7C2A50BEB401B35F28C3 ();
// 0x000001CD System.Object MyBox.MyCoroutines_<StartNextCoroutine>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CStartNextCoroutineU3Ed__8_System_Collections_IEnumerator_get_Current_m9142C3AD07AA8B083D5741F9141BAF7708DD1853 ();
// 0x000001CE System.Void MyBox.CoroutineGroup_<DoStart>d__9::.ctor(System.Int32)
extern void U3CDoStartU3Ed__9__ctor_m8682A110BD199E7CF14FB39FB6ADF0777F7C8AD6 ();
// 0x000001CF System.Void MyBox.CoroutineGroup_<DoStart>d__9::System.IDisposable.Dispose()
extern void U3CDoStartU3Ed__9_System_IDisposable_Dispose_m81CF7720E8EB16F125464F2ACDF68FF91220657C ();
// 0x000001D0 System.Boolean MyBox.CoroutineGroup_<DoStart>d__9::MoveNext()
extern void U3CDoStartU3Ed__9_MoveNext_mAA51C82D96BD869F3606470B2A061879A47DC367 ();
// 0x000001D1 System.Object MyBox.CoroutineGroup_<DoStart>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDoStartU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m756E95D64C2B3C05E77B580E0C966151206D0ADA ();
// 0x000001D2 System.Void MyBox.CoroutineGroup_<DoStart>d__9::System.Collections.IEnumerator.Reset()
extern void U3CDoStartU3Ed__9_System_Collections_IEnumerator_Reset_mE317143C8CF39E1AB8A1EDCD0707A093D00AECCB ();
// 0x000001D3 System.Object MyBox.CoroutineGroup_<DoStart>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CDoStartU3Ed__9_System_Collections_IEnumerator_get_Current_mB847852FEC4FB8D6D5EAFB6D083F72AAAA7205FF ();
// 0x000001D4 System.Void MyBox.MyDelayedActions_<DelayedAction>d__0::.ctor(System.Int32)
extern void U3CDelayedActionU3Ed__0__ctor_m410354A6F0C37F9E1ACC0B00F90BA88B2230FF3E ();
// 0x000001D5 System.Void MyBox.MyDelayedActions_<DelayedAction>d__0::System.IDisposable.Dispose()
extern void U3CDelayedActionU3Ed__0_System_IDisposable_Dispose_mBDA2088C2200087D5747B2A1E8C260FED1F64095 ();
// 0x000001D6 System.Boolean MyBox.MyDelayedActions_<DelayedAction>d__0::MoveNext()
extern void U3CDelayedActionU3Ed__0_MoveNext_mF63545F6BF1D2642B68F00E773D1D2049F17802C ();
// 0x000001D7 System.Object MyBox.MyDelayedActions_<DelayedAction>d__0::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDelayedActionU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2CEE6F6BA31B190478772FB8B032438499B490F8 ();
// 0x000001D8 System.Void MyBox.MyDelayedActions_<DelayedAction>d__0::System.Collections.IEnumerator.Reset()
extern void U3CDelayedActionU3Ed__0_System_Collections_IEnumerator_Reset_m4198FB47B3DFB33BB440FAE7AD3A579179641288 ();
// 0x000001D9 System.Object MyBox.MyDelayedActions_<DelayedAction>d__0::System.Collections.IEnumerator.get_Current()
extern void U3CDelayedActionU3Ed__0_System_Collections_IEnumerator_get_Current_mC548478119C694584F21E5142DCD41FBC28B03E9 ();
// 0x000001DA System.Void MyBox.MyDelayedActions_<DelayedAction>d__2::.ctor(System.Int32)
extern void U3CDelayedActionU3Ed__2__ctor_mF411A8B8612C8C5902F6127B60E012C5E69123FE ();
// 0x000001DB System.Void MyBox.MyDelayedActions_<DelayedAction>d__2::System.IDisposable.Dispose()
extern void U3CDelayedActionU3Ed__2_System_IDisposable_Dispose_m9127FEE171B2B5F7E069130108757EC032509798 ();
// 0x000001DC System.Boolean MyBox.MyDelayedActions_<DelayedAction>d__2::MoveNext()
extern void U3CDelayedActionU3Ed__2_MoveNext_m1F7D7041041A91DCEF0F10D36FD00B2B60DCCE50 ();
// 0x000001DD System.Object MyBox.MyDelayedActions_<DelayedAction>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDelayedActionU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m50F5D1A6316EAA5A70F043B65D12921D0FCEBDB6 ();
// 0x000001DE System.Void MyBox.MyDelayedActions_<DelayedAction>d__2::System.Collections.IEnumerator.Reset()
extern void U3CDelayedActionU3Ed__2_System_Collections_IEnumerator_Reset_m648EB4E8117E86A65346E0E64CD2537B81F85CC4 ();
// 0x000001DF System.Object MyBox.MyDelayedActions_<DelayedAction>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CDelayedActionU3Ed__2_System_Collections_IEnumerator_get_Current_m61BCD83EA935F94A3AF89F43C9B9C009A0625D16 ();
// 0x000001E0 System.Void MyBox.MyDelayedActions_<DelayedUiSelection>d__4::.ctor(System.Int32)
extern void U3CDelayedUiSelectionU3Ed__4__ctor_m180A12659FC29107343A4C29560A7E0CBF3C2CBD ();
// 0x000001E1 System.Void MyBox.MyDelayedActions_<DelayedUiSelection>d__4::System.IDisposable.Dispose()
extern void U3CDelayedUiSelectionU3Ed__4_System_IDisposable_Dispose_m17938442B04C755446EEEA90E472EACF199B2A32 ();
// 0x000001E2 System.Boolean MyBox.MyDelayedActions_<DelayedUiSelection>d__4::MoveNext()
extern void U3CDelayedUiSelectionU3Ed__4_MoveNext_m5B707A62A8045FB1B1427A41781120DE33B3FED9 ();
// 0x000001E3 System.Object MyBox.MyDelayedActions_<DelayedUiSelection>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDelayedUiSelectionU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA7926A0C17424E9CBACE702CAF8723A8AFF97D5F ();
// 0x000001E4 System.Void MyBox.MyDelayedActions_<DelayedUiSelection>d__4::System.Collections.IEnumerator.Reset()
extern void U3CDelayedUiSelectionU3Ed__4_System_Collections_IEnumerator_Reset_m5F9BD844EE59FA0FE069D6CD1A5594C9F575A73E ();
// 0x000001E5 System.Object MyBox.MyDelayedActions_<DelayedUiSelection>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CDelayedUiSelectionU3Ed__4_System_Collections_IEnumerator_get_Current_m2196AFDE3895C8A7AE267865ABBA2B41CC0FAC48 ();
// 0x000001E6 System.Void MyBox.MyExtensions_ComponentOfInterface`1::.ctor(UnityEngine.Component,T)
// 0x000001E7 System.Void MyBox.MyExtensions_<>c__9`1::.cctor()
// 0x000001E8 System.Void MyBox.MyExtensions_<>c__9`1::.ctor()
// 0x000001E9 UnityEngine.Component MyBox.MyExtensions_<>c__9`1::<FindObjectsOfInterface>b__9_0(UnityEngine.Transform)
// 0x000001EA System.Void MyBox.MyExtensions_<>c__10`1::.cctor()
// 0x000001EB System.Void MyBox.MyExtensions_<>c__10`1::.ctor()
// 0x000001EC System.Boolean MyBox.MyExtensions_<>c__10`1::<FindObjectsOfInterfaceAsComponents>b__10_0(UnityEngine.Component)
// 0x000001ED MyBox.MyExtensions_ComponentOfInterface`1<T> MyBox.MyExtensions_<>c__10`1::<FindObjectsOfInterfaceAsComponents>b__10_1(UnityEngine.Component)
// 0x000001EE System.Void MyBox.MyExtensions_<>c__12`1::.cctor()
// 0x000001EF System.Void MyBox.MyExtensions_<>c__12`1::.ctor()
// 0x000001F0 System.Int32 MyBox.MyExtensions_<>c__12`1::<OnePerInstance>b__12_0(T)
// 0x000001F1 T MyBox.MyExtensions_<>c__12`1::<OnePerInstance>b__12_1(System.Linq.IGrouping`2<System.Int32,T>)
// 0x000001F2 System.Void MyBox.MyExtensions_<>c::.cctor()
extern void U3CU3Ec__cctor_m635F7DC05C61849749EF9055BA3E657480AF1D52 ();
// 0x000001F3 System.Void MyBox.MyExtensions_<>c::.ctor()
extern void U3CU3Ec__ctor_m776DEEAD756C0853C4A0A7D362DEB19E80377491 ();
// 0x000001F4 System.Int32 MyBox.MyExtensions_<>c::<OneHitPerInstance>b__13_0(UnityEngine.RaycastHit2D)
extern void U3CU3Ec_U3COneHitPerInstanceU3Eb__13_0_m7C5A838B133842C2A683600F4BF7F126FDC64A83 ();
// 0x000001F5 UnityEngine.RaycastHit2D MyBox.MyExtensions_<>c::<OneHitPerInstance>b__13_1(System.Linq.IGrouping`2<System.Int32,UnityEngine.RaycastHit2D>)
extern void U3CU3Ec_U3COneHitPerInstanceU3Eb__13_1_m7C2A34C05CA7649D1AFA4B2AF8DB119C77BF770C ();
// 0x000001F6 System.Int32 MyBox.MyExtensions_<>c::<OneHitPerInstance>b__14_0(UnityEngine.Collider2D)
extern void U3CU3Ec_U3COneHitPerInstanceU3Eb__14_0_mAF054495E63D62A2915DD78A7D2EFFDB63BA20A6 ();
// 0x000001F7 UnityEngine.Collider2D MyBox.MyExtensions_<>c::<OneHitPerInstance>b__14_1(System.Linq.IGrouping`2<System.Int32,UnityEngine.Collider2D>)
extern void U3CU3Ec_U3COneHitPerInstanceU3Eb__14_1_mFA2CE68A5E613506C5AD4EE0A241FCC30C4DA552 ();
// 0x000001F8 System.Int32 MyBox.MyExtensions_<>c::<OneHitPerInstanceList>b__15_0(UnityEngine.Collider2D)
extern void U3CU3Ec_U3COneHitPerInstanceListU3Eb__15_0_mBFB4BB5A41B2142F33E80E6CF6AA487A68A4A998 ();
// 0x000001F9 UnityEngine.Collider2D MyBox.MyExtensions_<>c::<OneHitPerInstanceList>b__15_1(System.Linq.IGrouping`2<System.Int32,UnityEngine.Collider2D>)
extern void U3CU3Ec_U3COneHitPerInstanceListU3Eb__15_1_mB7E3FB4D506CF56150567172FEFCA5C3FF2743B5 ();
// 0x000001FA System.Void MyBox.MyNavMesh_<GetPointsOnPath>d__3::.ctor(System.Int32)
extern void U3CGetPointsOnPathU3Ed__3__ctor_m4251E7B3CDF45C48E565819D96D8CC60C1D2705E ();
// 0x000001FB System.Void MyBox.MyNavMesh_<GetPointsOnPath>d__3::System.IDisposable.Dispose()
extern void U3CGetPointsOnPathU3Ed__3_System_IDisposable_Dispose_mB3A665FFDD33285D096D6257A53CB67F03D7AECD ();
// 0x000001FC System.Boolean MyBox.MyNavMesh_<GetPointsOnPath>d__3::MoveNext()
extern void U3CGetPointsOnPathU3Ed__3_MoveNext_mA737621DA1FE6D3FECBB855C9A01883C1A000780 ();
// 0x000001FD UnityEngine.Vector3 MyBox.MyNavMesh_<GetPointsOnPath>d__3::System.Collections.Generic.IEnumerator<UnityEngine.Vector3>.get_Current()
extern void U3CGetPointsOnPathU3Ed__3_System_Collections_Generic_IEnumeratorU3CUnityEngine_Vector3U3E_get_Current_m74137D869B6BBA00A2AE35336714F6AB8A70275B ();
// 0x000001FE System.Void MyBox.MyNavMesh_<GetPointsOnPath>d__3::System.Collections.IEnumerator.Reset()
extern void U3CGetPointsOnPathU3Ed__3_System_Collections_IEnumerator_Reset_m807FC5C5B3ABC8157654882750BFA13107BE65E3 ();
// 0x000001FF System.Object MyBox.MyNavMesh_<GetPointsOnPath>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CGetPointsOnPathU3Ed__3_System_Collections_IEnumerator_get_Current_mB7820451AD957A79D629C15525DC05CE479C40CC ();
// 0x00000200 System.Collections.Generic.IEnumerator`1<UnityEngine.Vector3> MyBox.MyNavMesh_<GetPointsOnPath>d__3::System.Collections.Generic.IEnumerable<UnityEngine.Vector3>.GetEnumerator()
extern void U3CGetPointsOnPathU3Ed__3_System_Collections_Generic_IEnumerableU3CUnityEngine_Vector3U3E_GetEnumerator_m899F056E7D7467C537A75E0C7C6881BD73B8B7E1 ();
// 0x00000201 System.Collections.IEnumerator MyBox.MyNavMesh_<GetPointsOnPath>d__3::System.Collections.IEnumerable.GetEnumerator()
extern void U3CGetPointsOnPathU3Ed__3_System_Collections_IEnumerable_GetEnumerator_m73D0B5C76BE3AE1994964480CCDC5D765EFC90E3 ();
// 0x00000202 System.Void MyBox.TransformShakeExtension_<TransformShakeCoroutine>d__4::.ctor(System.Int32)
extern void U3CTransformShakeCoroutineU3Ed__4__ctor_m9C5DF0A45878D45904625FB6020BBA37B5AB797A ();
// 0x00000203 System.Void MyBox.TransformShakeExtension_<TransformShakeCoroutine>d__4::System.IDisposable.Dispose()
extern void U3CTransformShakeCoroutineU3Ed__4_System_IDisposable_Dispose_m70A5E22242A722C7DB3112A99E152DDFFA6645E5 ();
// 0x00000204 System.Boolean MyBox.TransformShakeExtension_<TransformShakeCoroutine>d__4::MoveNext()
extern void U3CTransformShakeCoroutineU3Ed__4_MoveNext_mCC641F6CE24D200BD6E35CFEBD6F3CADDFCA7765 ();
// 0x00000205 System.Object MyBox.TransformShakeExtension_<TransformShakeCoroutine>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTransformShakeCoroutineU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF96E6E635A2F1D4311019F72B68806E17D7B40BA ();
// 0x00000206 System.Void MyBox.TransformShakeExtension_<TransformShakeCoroutine>d__4::System.Collections.IEnumerator.Reset()
extern void U3CTransformShakeCoroutineU3Ed__4_System_Collections_IEnumerator_Reset_m7F91E0DF3F5CB69B1BAE9ADA622C447CF75BECEC ();
// 0x00000207 System.Object MyBox.TransformShakeExtension_<TransformShakeCoroutine>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CTransformShakeCoroutineU3Ed__4_System_Collections_IEnumerator_get_Current_m51CA98AB8E64A3F362226A0B3EB811FD9B759FB7 ();
// 0x00000208 System.Void MyBox.TimeTest_TimeTestData::.ctor(System.String,System.Boolean)
extern void TimeTestData__ctor_mA29F41B0CD9DA50242B657AA43EFC0315E8B24B0_AdjustorThunk ();
// 0x00000209 System.Void MyBox.TimeTest_TimeTestData::EndTest()
extern void TimeTestData_EndTest_mCE5E239CAC2EBC2A6FC7D1F63BEB4F102E6078A1_AdjustorThunk ();
// 0x0000020A System.Void MyBox.TimeTest_TimeTestData::.cctor()
extern void TimeTestData__cctor_m88BA2D77097807B256486732CED45580E7BAE60A ();
// 0x0000020B System.Void MyBox.ColliderToMesh_Triangulator::.ctor(UnityEngine.Vector2[])
extern void Triangulator__ctor_mCC4E5B8A141191F9E9E2BF5951235C2B6FBCFC97 ();
// 0x0000020C System.Int32[] MyBox.ColliderToMesh_Triangulator::Triangulate()
extern void Triangulator_Triangulate_m319BB31610579186F1E00A0114A5E2E85A324243 ();
// 0x0000020D System.Single MyBox.ColliderToMesh_Triangulator::Area()
extern void Triangulator_Area_mF2D92E1EB2F84254160367DE77B95B8A5C2BC534 ();
// 0x0000020E System.Boolean MyBox.ColliderToMesh_Triangulator::Snip(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32[])
extern void Triangulator_Snip_mB0BB348462761EA0FC53C371B2ABB3358CFAF076 ();
// 0x0000020F System.Boolean MyBox.ColliderToMesh_Triangulator::InsideTriangle(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2)
extern void Triangulator_InsideTriangle_m147A350D64C1AD44F354CC8E012D403627E341D5 ();
// 0x00000210 System.Void MyBox.GuidManager_GuidInfo::add_OnAdd(System.Action`1<UnityEngine.GameObject>)
extern void GuidInfo_add_OnAdd_m2B28E812EBE755F4A663153E507BBC9F63434D55_AdjustorThunk ();
// 0x00000211 System.Void MyBox.GuidManager_GuidInfo::remove_OnAdd(System.Action`1<UnityEngine.GameObject>)
extern void GuidInfo_remove_OnAdd_m38AABC1F4B41258A35B33DA22FBC75B1F375D6EC_AdjustorThunk ();
// 0x00000212 System.Void MyBox.GuidManager_GuidInfo::add_OnRemove(System.Action)
extern void GuidInfo_add_OnRemove_mF66D6196140B361802E6D2BD0650A0682783328A_AdjustorThunk ();
// 0x00000213 System.Void MyBox.GuidManager_GuidInfo::remove_OnRemove(System.Action)
extern void GuidInfo_remove_OnRemove_m8A90A28ABD79EC7EBE60ADF64B059A4906EC93D5_AdjustorThunk ();
// 0x00000214 System.Void MyBox.GuidManager_GuidInfo::.ctor(MyBox.GuidComponent)
extern void GuidInfo__ctor_m293487E8CDA99B98A55E395B2AB1AC9F32345129_AdjustorThunk ();
// 0x00000215 System.Void MyBox.GuidManager_GuidInfo::HandleAddCallback()
extern void GuidInfo_HandleAddCallback_m61011944455E0B2BB8B8CEEC9529C24DC358E583_AdjustorThunk ();
// 0x00000216 System.Void MyBox.GuidManager_GuidInfo::HandleRemoveCallback()
extern void GuidInfo_HandleRemoveCallback_mE8F42967EB67A24DA9D1BCB7E8727C6EE46779DE_AdjustorThunk ();
// 0x00000217 System.Void MyBox.GuidReference_<>c::.cctor()
extern void U3CU3Ec__cctor_mA80B134949C8C61E85A46BC5C256B895705CC5A5 ();
// 0x00000218 System.Void MyBox.GuidReference_<>c::.ctor()
extern void U3CU3Ec__ctor_m75E80CC08EE6FCDA9F657B26C4BD59A5D38385EF ();
// 0x00000219 System.Void MyBox.GuidReference_<>c::<.ctor>b__15_0(UnityEngine.GameObject)
extern void U3CU3Ec_U3C_ctorU3Eb__15_0_m38E337F86D67C6C0093386A2D7FAC3E3BABDB3E0 ();
// 0x0000021A System.Void MyBox.GuidReference_<>c::<.ctor>b__15_1()
extern void U3CU3Ec_U3C_ctorU3Eb__15_1_mBC1F2376332970D717A9433D9AB633D2BD7AA60F ();
// 0x0000021B System.Void MyBox.GuidReference_<>c::<.ctor>b__16_0(UnityEngine.GameObject)
extern void U3CU3Ec_U3C_ctorU3Eb__16_0_mF6EED94F79CA736C95AA6E12DE04CFA0173DF44B ();
// 0x0000021C System.Void MyBox.GuidReference_<>c::<.ctor>b__16_1()
extern void U3CU3Ec_U3C_ctorU3Eb__16_1_mFC856826D4B28B6557246A5A115245E4986FB483 ();
// 0x0000021D System.Boolean MyBox.MyDictionary`2_PrimeHelper::IsPrime(System.Int32)
// 0x0000021E System.Int32 MyBox.MyDictionary`2_PrimeHelper::GetPrime(System.Int32)
// 0x0000021F System.Int32 MyBox.MyDictionary`2_PrimeHelper::ExpandPrime(System.Int32)
// 0x00000220 System.Void MyBox.MyDictionary`2_PrimeHelper::.cctor()
// 0x00000221 System.Collections.Generic.KeyValuePair`2<TKey,TValue> MyBox.MyDictionary`2_Enumerator::get_Current()
// 0x00000222 System.Void MyBox.MyDictionary`2_Enumerator::set_Current(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
// 0x00000223 System.Void MyBox.MyDictionary`2_Enumerator::.ctor(MyBox.MyDictionary`2<TKey,TValue>)
// 0x00000224 System.Boolean MyBox.MyDictionary`2_Enumerator::MoveNext()
// 0x00000225 System.Void MyBox.MyDictionary`2_Enumerator::System.Collections.IEnumerator.Reset()
// 0x00000226 System.Object MyBox.MyDictionary`2_Enumerator::System.Collections.IEnumerator.get_Current()
// 0x00000227 System.Void MyBox.MyDictionary`2_Enumerator::Dispose()
// 0x00000228 System.Void MyBox.SceneReference_SceneLoadException::.ctor(System.String)
extern void SceneLoadException__ctor_m3C09859AC9597AFF6F7DB21EE619A7D22461727E ();
// 0x00000229 System.Void MyBox.Internal.MyLogger_<>c::.cctor()
extern void U3CU3Ec__cctor_mCB893FADA4B5DE504CDB3B9BAF595B9DAFC8D5B4 ();
// 0x0000022A System.Void MyBox.Internal.MyLogger_<>c::.ctor()
extern void U3CU3Ec__ctor_mD19E49692D7BD29724D3808AB9B12BF8DC8F7226 ();
// 0x0000022B System.Void MyBox.Internal.MyLogger_<>c::<.cctor>b__12_0(System.Object,System.UnhandledExceptionEventArgs)
extern void U3CU3Ec_U3C_cctorU3Eb__12_0_m70FEE30D109070E79586FC6C90945391CDCFFC02 ();
// 0x0000022C System.Void MyBox.Internal.MyLogger_<>c::<.cctor>b__12_1(System.String,System.String,UnityEngine.LogType)
extern void U3CU3Ec_U3C_cctorU3Eb__12_1_m22F2D61C2650896A266124700E65925FB97891A8 ();
static Il2CppMethodPointer s_methodPointers[556] = 
{
	FPSCounter_Awake_mFA08B0C39F461F6086188612A72D3B0289AA1F27,
	FPSCounter_Update_mEE757D79317F955F9E6048BCF09F1A7CC49E616B,
	FPSCounter_OnGUI_m20B6796386A5539476094F040BC23A8636D73B43,
	FPSCounter__ctor_m2321CBAE8182B0F73D59CF3E22DAA4880330C17B,
	AutoPropertyAttribute__ctor_m3E4A14BFDE523006274BF0D549311B8326D2E860,
	ButtonMethodAttribute__ctor_m5366E2E19A7E56FA8F917B57A9D64DE558D49CDD,
	ConditionalFieldAttribute__ctor_mB97BC6EC587AA971732D98B3E391959ECAF6322E,
	ConstantsSelectionAttribute__ctor_m262FFA961810A73A7ED21139765267A571EB5F03,
	DefinedValuesAttribute__ctor_m003C14DAB0AAAB1B31465B77C144ACFEBE34B5A2,
	DisplayInspectorAttribute__ctor_m03896D96B0C2F8CB847A22F00889A3B99C4DF39E,
	FoldoutAttribute__ctor_m804D08161B302083333C35B4DF3129525FC72235,
	InitializationFieldAttribute__ctor_m15AD3F636815F33D95C8811AA4810BC0CCE70B70,
	LayerAttribute__ctor_m11FE5EC55826E5A57229B0E95946E32A40BC7DC9,
	MaxValueAttribute__ctor_m9AA32628F4B80102AA03445566035EA17534CB4E,
	MaxValueAttribute__ctor_m4E556CF45BA8316EDDA523C7DFDCB13AB4C33A9F,
	MinMaxRangeAttribute__ctor_m818492E883FCCBD2B1F0DB4F52701456587AEDAB,
	RangedFloat__ctor_mC236D6100DBCB0566D22505BF94F4678BCF5830A_AdjustorThunk,
	RangedInt__ctor_m7B4EFB4893EE51FF3F4E867FE9C541E9F3028AA0_AdjustorThunk,
	RangedExtensions_LerpFromRange_m5FEB23824491E9349076387923ECE315283EE4D5,
	RangedExtensions_LerpFromRangeUnclamped_m2081D990A861DEB6C971196B853BD8FDE2F5BB40,
	RangedExtensions_LerpFromRange_mE6001B003A19D275A9B4838EF099BBC6E6FC24A2,
	RangedExtensions_LerpFromRangeUnclamped_mB5340E6CE741C5541708D34138BFFC75AE21DC93,
	MinValueAttribute__ctor_mDA90512B2FDAC2555FACD2E9AFA0F342108ADDFE,
	MinValueAttribute__ctor_m3096FCEC79271A554C3E86E5A7A4926A4D226372,
	MustBeAssignedAttribute__ctor_mFA6219167C7AA1A9FB449061A5C5F53E9F1558BB,
	PositiveValueOnlyAttribute__ctor_mAEE97C245FBCBF2BA2406EF7DFCA63D35EA40B86,
	ReadOnlyAttribute__ctor_m50FD5B931384C5619972A46E348C43215A2A4155,
	RequireLayerAttribute__ctor_m3F30A26CFBB3AA7D2F8A762DBD20B1679D3394A3,
	RequireLayerAttribute__ctor_mA1E6D1238623279134CC4CF7565DBF2641AD075B,
	RequireTagAttribute__ctor_m0A55A3FF705E1C76A7E818ADB91A2BC66231DEFE,
	SearchableEnumAttribute__ctor_mF10B8C35AACF7BE2E63F35BEB82006AF8F0071A9,
	SeparatorAttribute__ctor_mBF2D9AC63E4C58E5F101B89F3D747FD5A6198D06,
	SeparatorAttribute__ctor_m1533B6572CFA6A9E96D976B5B6D1137F407DDBEA,
	SpriteLayerAttribute__ctor_m7DF525024FEC6543D24EE192BE3720A86197167A,
	TagAttribute__ctor_m65BE09506FFEEBA7EFF0927683AB97A108277F53,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MyColor_get_RandomBright_m817D5DABF8899074F147258FE1166BB176D2D42C,
	MyColor_get_RandomDim_mBF56C3AFC8B5FB4BB98734B31D3DC45482A8C2E6,
	MyColor_get_RandomColor_mB7E521BC0C9FDB87B3DECE359A8F2665EA73FD47,
	MyColor_WithAlphaSetTo_mBED486A589BF5DDD4781EBC6ACC08A254AC1BB3E,
	MyColor_SetAlpha_m105792C000ED51558D1C85149EA39D9A1BD7A007,
	MyColor_SetAlpha_mD5114C9FF21D019B7905B9C3C46E27FC8E126E0F,
	MyColor_ToHex_m79C113AF586A9BA35BE5A3AB14E21A7761C712DE,
	MyColor_Lighter_mA7946E34AAF3D741A2A3B79E1F5F6BE38AAF0B0A,
	MyColor_Darker_mEF66FF1F74A0A640257D5FAF73C4900847A60EFA,
	MyColor_BrightnessOffset_m1A6A865D384A2395F8D099A69D85B2C6F0709B9D,
	MyCoroutines_get_CoroutineOwner_m5C5716DA761A609FC2C3BA1C194492A26D49BA8F,
	MyCoroutines_StartCoroutine_m783479BBB5048F97A84408EDB5217479DEB66F35,
	MyCoroutines_StartNext_m1E5046B9DD1A8E93759B164BE90BF0D38131BABB,
	MyCoroutines_StopCoroutine_m2880AB98AC4F83394615F7BABB20E92839EDFB68,
	MyCoroutines_StopAllCoroutines_m1A6FA8B2DEDDB1A0F5E10AAA636176FC8997C52C,
	MyCoroutines_CreateGroup_mD71CF3EDDE1457A78B82C79315ED73BF82A7C420,
	MyCoroutines_StartNextCoroutine_m7E2DF6FDD877A6C8DAA3E7AEB8E761EB9149115E,
	CoroutineGroup_get_ActiveCoroutinesAmount_m87D50A0FA3110EA70123EE582101E9AF3AB95800,
	CoroutineGroup_get_AnyProcessing_m0EE4D9534DAC1113EA2233FADFB367C6594FDC8B,
	CoroutineGroup__ctor_mB399CB4D92FF6537A7B2F8F9A5F5D957703953F8,
	CoroutineGroup_StartCoroutine_m11E3BB54376ED5B8B6C18ED358A51CBCD9B0485A,
	CoroutineGroup_StopAll_m5535F32FB0A0C15A15EA25F5FA620388CE67548F,
	CoroutineGroup_DoStart_m7CB20F6426FD0ECC462AD2FC85C4A82EF45FB7DE,
	NULL,
	NULL,
	MyDebug_LogColor_m455E64D88FB8D0E536E99A5D0783A4FFB5F08880,
	MyDebug_DrawDebugBounds_mF536D11A5DB9B0A29B569806649D52FFDD168E02,
	MyDebug_DrawDebugBounds_m99BCB28369061169A964BD312FB4459AAECBE57E,
	MyDebug_DrawDebugBounds_m4965BC6915BA9B4158AB18085B8ED7A7884E1F0B,
	MyDebug_DrawString_m95C095A5D1D56DED8176AFCBC53F5C62DFCCEA29,
	MyDebug_DrawArrowRay_m1C906A616EF9A6FEA57D3BDF9B977315609F20CC,
	MyDebug_DrawDimensionalCross_m6A742B091561C484BB594C3B854463008CF50E83,
	MyDelayedActions_DelayedAction_m4FA4FA0830DA3F2D5F53D0742CFE33689AECE25A,
	MyDelayedActions_DelayedAction_m292153E87856CD6EBC3A061F7A72BE128A3C789B,
	MyDelayedActions_DelayedAction_m62252D7594BAC1BAB0AFDD2978FAA7C84CE080BF,
	MyDelayedActions_DelayedAction_mF6E227449304D56FB891492EEC9B1560E974614B,
	MyDelayedActions_DelayedUiSelection_m85A56C6540D220A48F879E4335A9385C21AAF88C,
	MyDelayedActions_DelayedUiSelection_m40F9CA173A9B60C8B5A0D08CACDE1CDACF484EEE,
	NULL,
	MyExtensions_IsWorldPointInViewport_mA8FFEC19E91EA5B0D0DFED26BB55A8FDDBA8D6B2,
	NULL,
	NULL,
	NULL,
	MyExtensions_GetObjectsOfLayerInChilds_mC4E3671941107D2302868AAD0DC9F875FB3AFB22,
	MyExtensions_GetObjectsOfLayerInChilds_m11101D58F06E02090083D0C257C2DD29FB2B8E48,
	MyExtensions_CheckChildsOfLayer_m4EE72B032DFD0142426D29D98A5F0B0A9B98C830,
	MyExtensions_SetBodyState_mA1B41075A7AC7DE562EE358199A16BCA8380CAC7,
	NULL,
	NULL,
	NULL,
	MyExtensions_OneHitPerInstance_m36BC5888BC7EF019E57E27289ED1D0A1308C5AB6,
	MyExtensions_OneHitPerInstance_m4DE2AB5013DDE40B613249197DFA09C79F7764B6,
	MyExtensions_OneHitPerInstanceList_m6125362A7C16EA48B234843CAED681FB399391DE,
	MyGizmos_DrawArrow_m26A0DDED8A21141092DC7F4376FECB31EBDC251E,
	MyInput_GetNumberDown_m354F75AB1C934EDC818C5CCF4EBFC8168191C278,
	MyInput_GetNumberDown_m66397623BD686D3554691C1552F48D6945825C1C,
	MyInput_GetNumberDown_mA94C332015341F501803AD4B965EA5FBC9FAD6FB,
	MyInput_ToReadableString_mC9AE855C5D14D5893AC6F1C06160BCF2303C5375,
	MyInput_AnyKeyDown_m1924CA3FD7FB5AC989677F24AB5DB03474D12081,
	MyInput_AnyKeyDown_mCB76AE7B9BAAA3B834981802D078184F758B26A9,
	MyInput_IsLeft_m9C7F5F8ECF17334D9A9374E70012AFEA380D49EB,
	MyInput_IsRight_m2785745C7FC19D4CD956843FBAC55C784B4EF255,
	MyInput_IsUp_m71CC46BE1D496B432303A3FCD9BA25FBC2A05C69,
	MyInput_IsDown_mC1EA3CD0348A02B7AA37F5B19F1B008E7E34F782,
	MyInput_KeypadDirection_mB71569D2DC9AE3D6EB657E867CD3BE115689F0D5,
	MyInput_KeypadX_m39D890532838BA1816087D4EA062B62729A27981,
	MyInput_KeypadY_m599B80A5B8E8AEAE95FD4A4AD85E8693485F94EB,
	MyLayers_ToLayerMask_m0ECBDFF950DD80F1AF2C224A8A19D0BF0FBFF46E,
	MyLayers_LayerInMask_mDF5135CD1DB6B9799D1802080EB4F795F4387167,
	NULL,
	MyMath_Snap_m4C5CD34E31BFF74F1A1285E020964AF2FBF162EE,
	MyMath_Sign_m1064EEE400593360836952CF2F893C4390E2D242,
	MyMath_Approximately_m4CFA803628DA78871AD94AE4B6D357360FCB6123,
	MyMath_InRange01_mA6FD66A9D00A24CB3FCF37CF9D709C328F311C0A,
	MyMath_InRange_m7FD4EC6C15E3D17681EC3BAAB3EECE750843B0DE,
	MyMath_NotInRange_mC6732ED3C948267990A9FE56E182A756A6BB56EF,
	MyMath_NotInRange_m5E635003BA5BB487127ECB5B18C4B0F1E710BB4C,
	MyMath_ClosestPoint_m1C5307D63AC53C2FEEDBF2C1AD0FAB7C37B92FD9,
	MyMath_ClosestPointIsA_m3B2964FECCCE5F6CD8EBA0B638D801897A59E4D0,
	MyNavMesh_GetLength_m9ABAB3948D7C6A666A61CA9FC5DB878588E4CA30,
	MyNavMesh_GetTimeToPass_m22CFCFC89F653C60BEAAAF5C2C5613A3C633596B,
	MyNavMesh_GetPointOnPath_m7BB406068523A830453899DF0ACDB464B4F19CD0,
	MyNavMesh_GetPointsOnPath_mE13005E038B6EBF984ECFAAD8CD23F4DFF2CDF06,
	MyString_ToCamelCase_m458CFC98FD54CE2790C9F88DA1F7AA547A359200,
	MyString_SplitCamelCase_mA650F8E359DB7694EC425EBC13DE70E5803556E6,
	MyString_Colored_m66F64FCA981C486ED1A58F8BEFD8B40C2E186311,
	MyString_Colored_m4193201BE398463A2411E63FA614424BAF60432F,
	MyString_Sized_m59097FE6D5E3131FBBC2555A141F82BE4C1F0AC7,
	MyString_Bold_m33911CCD879E5FC35038ED35331C70B8F842A3C4,
	MyString_Italics_mB61E6E17B1E90D8C1F9AE810EC17C73AF1AC5812,
	MyTexture_AsSprite_m2D67D8D7677C82F07C7778DF6EE46FD97418702B,
	MyTexture_Resample_m68073A5249DC8F4C81F320B03E62B2768A8818C3,
	MyTexture_Crop_m5F378792E696623B12E8DE5DA38C482B3AB8637F,
	MyTexture_WithSolidColor_m5DB36791A03A823F7A7ABC6DF3BFE0E3B39C9AAD,
	MyUI_SetCanvasState_mA2FEAADC3FCCF77912C0FDB653E11AC6F5EF314B,
	MyUI_SetState_m783C357B2C2CFAF45782C36A23D37D4AD7357416,
	MyUI_OnEventSubscribe_m9C52EB7AF91886B2B0172D8C09FAFACEE79FF264,
	MyUI_OnEventUnsubscribe_m6B6DA8FC14A3977238DE1E665C88CD9B86520477,
	MyVectors_SetX_m3405C1C90BED601F3CF7007FF78D88D95D663894,
	MyVectors_SetX_m42305F6F08D24E4F3FFDF5F6498A805C0E8A76D5,
	MyVectors_SetX_mE96C86801B8F160933208A334FDAB0642A55D515,
	MyVectors_SetY_m0F4AE843CF47A355C9CB0943F6799463237A1249,
	MyVectors_SetY_m46B789BFAB9EDD43C4BC3308D83FF12CDAAF5558,
	MyVectors_SetY_mE3BCAD383F4DDCB62CDE1CEDE01BCED2422E2B8F,
	MyVectors_SetZ_mF7DFCA564C09338114230E32CD7F9503B0599069,
	MyVectors_SetZ_mD7D8EA55B30017EB9A531FDF7C3F4FDD32FFC2DC,
	MyVectors_SetXY_mC0FFD9B3FE6247010C8943F30BFD8EA58F73860C,
	MyVectors_SetXY_m1D9860A735CBEA6DD18F1FF530754B4FC22F9F26,
	MyVectors_SetXZ_mBB3C075766B1166F15B62C8F5006A8D319965CE6,
	MyVectors_SetXZ_mE527DA7FA61555B1556EBC09111920AE339D1C16,
	MyVectors_SetYZ_m8085AEC40FAFAF3350D0EEC384F6D38453E55492,
	MyVectors_SetYZ_m13516711EC4E167E65D2E6C6BF77F5631A70F78E,
	MyVectors_ResetPosition_m4B46583B6CCFA983185D86BFE29CBCE7C7E36A7E,
	MyVectors_SetPositionX_m7675C2F4F095243EDD8B8FCA1049BB173ADD29EA,
	MyVectors_SetPositionY_m7430A9552DBE83B1718A5E03F85DBD06ECE50A2F,
	MyVectors_OffsetPositionX_m8202442408A39D026F64514DB0B3C58A52A87ECE,
	MyVectors_OffsetPositionY_mDC4354E402B0E09D41EF3A7462247126BB3B396A,
	MyVectors_Offset_m49661CDCD3ECC3FEB0073AA4325784E57EED276C,
	MyVectors_OffsetX_m5F34C7527EB7179FD304DD3A0ED0327C0C7E9EEC,
	MyVectors_OffsetX_mEE5F3A40B15B9CEA50B4BFB4CBFCA7CB036BD7BB,
	MyVectors_OffsetX_mFB94CB9ED9AE13E59ABD4AC6A2F9CBAD9AB2CA05,
	MyVectors_OffsetY_mAEBAFB497B2D79CB26B22552D803158AA45D79B9,
	MyVectors_OffsetY_m97A48F7025DF5121DA1F66A5F321FC86489302BE,
	MyVectors_OffsetY_m0FA091FC1437BAADF8033C85A211070AA231F5AB,
	MyVectors_OffsetZ_mBC0904B4A15532FEE165F8EE2DE704AFDF590B02,
	MyVectors_OffsetZ_mA1A020B413E1DFA581BAD2354D992183F5A41B7C,
	MyVectors_OffsetXY_m015C9C537C227749A5C502C1B84B9418FFF9AF14,
	MyVectors_OffsetXY_m0658268E7DA92AB57F403F54999B1F6BE50A014B,
	MyVectors_OffsetXY_mCB2592F279AB3D22D1566711FF5A922309EB0E65,
	MyVectors_OffsetXZ_mB054757EBC47C70EB21E4A6A5D48113D26F085DA,
	MyVectors_OffsetXZ_m22366B7DA8A40CE22C1F5A6504FBDDB6C43B6BDD,
	MyVectors_OffsetYZ_m7D49F750DCFD20F9BF36E9264ABD4292850C8D31,
	MyVectors_OffsetYZ_mBC2D54FFEC565F15330B3C7CB6E32F73495FAAC4,
	MyVectors_ClampX_mD8AC72EF245B37611FC5B0E88BE4010D9D5C7A27,
	MyVectors_ClampX_m49407AB34D9D8CD347F5A0FB760F97BA1D56AFAE,
	MyVectors_ClampX_mB46D31968CCC7CC8F0FD51A8AFF626696E99D524,
	MyVectors_ClampY_m9F0CD24B82DD94BDE6BF5AC22D1B9D9FF50A7EC6,
	MyVectors_ClampY_m6606018A0AE8F7F4130A6123CAB4A8A2875211CF,
	MyVectors_ClampY_m5AECCB05FB08A966A5F2F430387549575EAE8E04,
	MyVectors_InvertX_m61393D26DDFF02BF30EED962A88786B1D6526DFE,
	MyVectors_InvertY_mC65BE045E9C769688E938B7BA4B4708723109007,
	MyVectors_ToVector2_m4533FDA30303E30E98027265197B9C6DFBAEA973,
	MyVectors_ToVector3_m14108CB9726C654001EC019F30E208881D2E5D88,
	MyVectors_ToVector2_mE6DA4185E96FF4AB1EE91C5D14AB6BEF55FA009F,
	MyVectors_ToVector3_mFC59FC44B9F1E40DED7D36D8E2EF7C0262B16028,
	MyVectors_ToVector2Int_mE8816298A6E38090DE5F852F72F94F05B9DCF80F,
	MyVectors_ToVector3Int_m7C26D60846396251D79C2A8A5EEAEF549FD2E6FD,
	MyVectors_SnapValue_m9C024E98A4F30D0C14927094A342B0DC1CA377D9,
	MyVectors_SnapValue_mDA6E3A30A379D0AFCF0183777B8910AFC60BC55B,
	MyVectors_SnapPosition_m689E20A17DABC72D299684A70B23913704EF787D,
	MyVectors_SnapToOne_m82C97D0EE1CC2C253DB49C4EC06BD3544422429A,
	MyVectors_SnapToOne_m2DAA7BFADC82A91F6826853DBD84A547D7D1FCE9,
	MyVectors_AverageVector_m25AFCF739FA96B50329B142C9FFECAAA560E9BD8,
	MyVectors_AverageVector_m6C972069155883801613ECED9C063B7FB9573A57,
	MyVectors_Approximately_m0B046C0FDF2CD7FA67844B3242015BFD0AC9093E,
	MyVectors_Approximately_mE9D2508F314B9ADFAF3F8D3D5D49F51A2DC9E633,
	MyVectors_GetClosest_mC65FE761151E99EADC45796C6804D5A7F2EA4B21,
	MyVectors_GetClosest_m6434C27367A4D3971584D63BBC032F5870BFDD4D,
	TransformShakeExtension_StartShake_m68B7E3DC89CF5B41DCBE6734C6D7FC19E1FF527C,
	TransformShakeExtension_StopShake_mA5E29BFF43E9E5D2C9A9D0F6611071D9033ABF76,
	TransformShakeExtension_TransformShakeCoroutine_mBDC5AA849A446D1321E6CF40D3C29BC624AB1879,
	TransformShakeExtension_BreakShakeIfAny_m2232906FB35D6BFDDB2F97528AFDF3740007A563,
	TransformShakeExtension__cctor_mD3922B87E0EB5268BF778E82B99FFC34FA45CCBB,
	NULL,
	ImageStringConverter_ImageFromString_m9CE371B5A46F1304448DB14DB565CC759BD44947,
	MySceneBundle_get_SceneBundleTransferOption_mC2847344EF5FA05C90E3620007C7CD3D9D382854,
	MySceneBundle_set_SceneBundleTransferOption_m76C69C20B4B794A5F311C33534BD58022B9CD46F,
	MySceneBundle__cctor_m7D2211842FB317DCEB328EA06BCD10CF73C4A022,
	MySceneBundle_PrepareSceneBundleForNextSceneByTransferOptions_m0AA4E1FA0B1EFC3D0C1ADD3B0E08E3D63E9FB42F,
	MySceneBundle_IsUnloadingLastScene_mFC1FF40BB15C97CCA4E557AB5969CE05AF1E0BF8,
	MySceneBundle_PrepareSceneBundleForNextScene_m06B562C48CE3AEFC08C808CF306E7044B8E8F05D,
	MySceneBundle_CarryOverCurrentBundleToNextBundle_mB868D1EABBD62A250D3BBFE94201DF3FCAC6852B,
	MySceneBundle_AddStringDataToBundle_mFF60273961FDD9CE45C568A0F43F9954E1F5EF79,
	MySceneBundle_AddFloatDataToBundle_mDEFA9561D59DD2F19938E86B9AC964B02389B7DD,
	MySceneBundle_AddIntDataToBundle_m9E059484392AB57BB1F258CA4FB05859045C8516,
	MySceneBundle_AddBoolDataToBundle_mA7743482E39FC17CC8B8257FEF098F9EC4601335,
	MySceneBundle_AddObjectDataToBundle_mA3454FC315298AE3C59BCC9412F0B7D938FD07F9,
	MySceneBundle_TryGetStringDataFromBundle_mB80CA88180CAE7F897C6D053FE734EAF03A6936D,
	MySceneBundle_TryGetFloatDataFromBundle_m62BD842DD41E44683718E87E206229292ABE47DD,
	MySceneBundle_TryGetIntDataFromBundle_mCEB2ECFE6908FCD323BA53D1D6AA30CE1FAD1C82,
	MySceneBundle_TryGetBoolDataFromBundle_m09917906D472350D1807BA3D50704C4116FD1A1F,
	MySceneBundle_TryGetObjectDataFromBundle_mFC016564CD23D7A2A240F4B0685DE105A981D8F5,
	TimeTest__ctor_m919D5E2B492A148DB4527283B3602BED546EE3C5,
	TimeTest_Dispose_m4A40E7B0C96709AC5BBE31E9CD9B2155C3D27C79,
	TimeTest_Start_mA5FCD405766BBB0624B38C6810E76C977B7B3089,
	TimeTest_Pause_mD9BA3C6E8CBA29F5543EA9B16AA5EFA721EBEE7D,
	TimeTest_Pause_mD6DBE6CD0AEB3F0E63CF6EC29BA6384F4D3C8775,
	TimeTest_End_m1C2783EF46659E232169E3E2332C582F8294F938,
	TimeTest_End_m15F58442AB63304EAFEB2185EA53F26CEB70DC5C,
	TimeTest__cctor_m1A45F109C821101058BFD3A18E500B443C9963C3,
	MyPhysics_CircleOverlap2D_m4F89EDFAEEBE7F5816D5BEB47CBFC8FCD864DDE5,
	MyPhysics_Raycast2D_m2D2377DAB6C757B464ADEE51DDC3FFA2910B1953,
	MyPhysics_Raycast2D_m3232E944363D51C44A592D3D197E5942E0E7B535,
	MyPhysics_Raycast2D_m890A55A8E840CDE0488CD949F383BC6A991E16BC,
	MyPhysics_Raycast2D_mE66A7A1A2265D78AD3395E4806C2615F1796F469,
	MyPhysics__ctor_m80A2188048A3FF52448B3627DE3CEC640726403B,
	MyPhysics__cctor_mE0F15A51CAA15597B5A7A7B2B393E232A25A82FF,
	ActiveStateOnStart_Awake_m5B6B721436A91BD8ED42B86F8CBAC2C1181D9621,
	ActiveStateOnStart__ctor_mCCA589A04675BD6ABFF588B10D3068C2FDDD8602,
	AnimationStateReference_get_StateName_m39935A57C12D9E268DF49A7FAF34639D30B7B597,
	AnimationStateReference_get_Assigned_m73F4C536C01A3C61065AB1729ECADECFD702EE23,
	AnimationStateReference_get_Animator_m269632A38716BDBB34060042D32994E0110DD6AB,
	AnimationStateReference__ctor_m9E5EE00D7471071972A630559D2443563DD4922B,
	AnimationStateReferenceExtension_Play_mFDF618D6BBE1B9D9DFAAD45C34BED15479AEFAEC,
	AnimationStateReferenceExtension_Play_m4EB8228BF8C94B1D8886137A24B100DD70B82F85,
	AssetFolderPath__ctor_mC4E7EE8C241799BE0262E90D3284C8D979DB5FA6,
	AssetPath_WithExtension_m3329F19C4A90DEDB5F1653D3D6CAAF5EDEA52432,
	AssetPath__ctor_m8774E9C34165F1248DCAC6B1788B2030872D84CE,
	Billboard_get_ActiveFacedObject_m7B5C190F81D95A538438AC482E212539CDFB0789,
	Billboard_Update_mE629EE5C37CFD8D40006E221CA25623A6CAE6583,
	Billboard__ctor_m5BFB894F26E2678F66FBFC23683F2C61C87C2C0F,
	NULL,
	ColliderGizmo__ctor_m1D031DA19CC51394FC688A568D8F62606A88112F,
	ColliderToMesh_Start_m681050093065C88D4B797E10EC209D3E37DEA6D1,
	ColliderToMesh_FillShape_mE011B63A4E81A281F19AAD4985A8AD4901DE6FE1,
	ColliderToMesh_ClearShape_mACD8A027EEF8F21BBE763A0808B8DF76295950A5,
	ColliderToMesh__ctor_mEF80C2736597467FABB57406F050AB71DC855F53,
	GuidComponent_get_Guid_mA86CBFB0FD8B4C4E26B0B3A0A0005318F1A61037,
	GuidComponent_get_GuidString_mE6582DBF61A41FDB7436BD9C191C0D15CA14AAF6,
	GuidComponent_IsGuidAssigned_mF44315223EA21962F3DC3062543EC5697F62632D,
	GuidComponent_CreateGuid_mC6F4AB933B8E4A2B92EEA161DDB85FF5EEF07527,
	GuidComponent_OnBeforeSerialize_m71CE729B94D69B3A861E27CD96836D6B4D4ED63C,
	GuidComponent_OnAfterDeserialize_m9A712F2E79BFCEDA2C63CA013FB819B87532A2E9,
	GuidComponent_Awake_mAD4792E45AF145477E9B70936E160AEA47CF3A12,
	GuidComponent_OnValidate_m64490110AB37F0FA2CD86246BFD100B118CD7994,
	GuidComponent_GetGuid_m94864A42872AFA2CD6408D85516DDE276ECB99E6,
	GuidComponent_OnDestroy_m285D613AD4BE4B0F320C7EE37326B12BE52A433E,
	GuidComponent__ctor_mCDAF38CFBDF2BC6F2FACE1B0E5AA6DD280F6E990,
	GuidManager_Add_m0F1C1E40C522169ADC90202105101B993976D372,
	GuidManager_Remove_m3BF1B6DB5C0B33C320DC7C4BED8B58E0804126C5,
	GuidManager_ResolveGuid_mA40B172A1431BE4988290B1563046D584987775A,
	GuidManager_ResolveGuid_mC20925311D8CCE797952852B2D9D94E4895CAC33,
	GuidManager_ResolveGuid_m56B40C624416EAC95462BA1DE24F7694E8F26F5C,
	GuidManager__ctor_m3B3C35B84623213BB9BC0840CEEB72D0834F2DF4,
	GuidManager_InternalAdd_m98DF543138ABDC74786CE298F8E2B5D49EB864CB,
	GuidManager_InternalRemove_mE8CA2FFFCE12C21C8ED33DFF05809332CE0FE95C,
	GuidManager_ResolveGuidInternal_m2E924BC0582B06145DFD609DD46B4A936CEC44C0,
	GuidReference_add_OnGuidAdded_m1DC92FA0025FD96682D768D391497B99D0FCA40A,
	GuidReference_remove_OnGuidAdded_m9FD815F908907DF18B14EBE78167D59392B8A3D7,
	GuidReference_add_OnGuidRemoved_m65086DA71AACB2B7DC3C3E57170882BE2E8DB495,
	GuidReference_remove_OnGuidRemoved_mBDE606128B9A0F5B0900C9EA359C000C30515EE9,
	GuidReference_get_gameObject_m3364F58B5292006343066D32F9260BD3F080FE50,
	GuidReference_set_gameObject_m631807E386355C61F835F3B5EE1CF8CA2ABAF6A9,
	GuidReference__ctor_m0A88BD85C1F2795E05B43768A16393A94B26F1FD,
	GuidReference__ctor_mACA70397C506E81938C1FEEBDF6D6893FB3B429D,
	GuidReference_GuidAdded_mD4C897A88908182D459B2913152B76FD9D0A14DC,
	GuidReference_GuidRemoved_m95ECE510A40463DBCD6FA88673ABCEAB1AF47887,
	GuidReference_OnBeforeSerialize_mF58E26CEB7887021A3E6B432C404576375B9A9E2,
	GuidReference_OnAfterDeserialize_m9EBCD04D6950A4F79C1068AEA6F3F8E06D4441D7,
	MinMaxFloat__ctor_mC18A650D47FA638AE253451E25B568620E764761_AdjustorThunk,
	MinMaxInt__ctor_m3B2A4FE3346665B5555E814D467EF71BCC6FB7D7_AdjustorThunk,
	MinMaxExtensions_Clamp_mA37A08DF891DC56A011CD3F24BFF66578D7B8A29,
	MinMaxExtensions_Clamp_m5DCED77D3FC862D97B871D79E434E9FD4509AECF,
	MinMaxExtensions_Length_mC760CB5D0AC7291646FB9BEF49679229B05C7CBD,
	MinMaxExtensions_Length_m10DA304D4AA2D8013B46547859D2400D9BC7DFBB,
	MinMaxExtensions_MidPoint_mC4AD6226C3A7F86111F6EAE87BE6EEAB14052362,
	MinMaxExtensions_MidPoint_m3BFAF42A3D2BC540DDF61ED7F4D5831E066AAEC0,
	MinMaxExtensions_Lerp_m7F61D464BD1A125AE57B5BA8FB3BDC3BC0844532,
	MinMaxExtensions_Lerp_m1753C0AA26EC76F97E3039BF79ABDC3F4DF0DD7B,
	MinMaxExtensions_LerpUnclamped_m871A6B492BB7AEAC0EF7792BB8DF885D502EDF97,
	MinMaxExtensions_LerpUnclamped_m113B61CFC21764D5296533842F389D571B41CDF1,
	MinMaxExtensions_RandomInRange_m2609BEE398A6FB54B01C0FC7C0E53D25794C8CB0,
	MinMaxExtensions_RandomInRange_m8249966E719EC550F27C76A056C6539B2A4D7AF7,
	NULL,
	NULL,
	NULL,
	NULL,
	MyCursor_ApplyAsLockedCursor_m21BDFEA45287C55CCE304FAD204E0C101862B520_AdjustorThunk,
	MyCursor_ApplyAsFreeCursor_mC0158F78C8D36BA1F45A601D9064281B66587E0B_AdjustorThunk,
	MyCursor_ApplyAsConfinedCursor_m68183A3FF74BC69C5EDA1A5008087797A1F308C3_AdjustorThunk,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	OptionalFloat_WithValue_m684A9D8F90A17E937FC2C7F4730563BF0EA8D070,
	OptionalFloat__ctor_mC35934E753D4278C3AC2FEE1B27734623C13EC40,
	OptionalInt_WithValue_m4FA80C58B5EF2E84A766D46DF5F52A52D01CE8A3,
	OptionalInt__ctor_m1CA3096DF41C9489372F18ED29B4539A26301CC3,
	OptionalString_WithValue_m32CA0515EF734A0AFD87AE410812828A73E18999,
	OptionalString__ctor_m190507886D56982B40EFE1F332216B5F7EDDCF9D,
	OptionalKeyCode_WithValue_m4C0FB1A170AE7EE654E8513867EBFAFF3E1E149A,
	OptionalKeyCode__ctor_mCBEC6AF748D10C4D32F477747A241EAE687860F5,
	OptionalGameObject__ctor_m333AF58F6BBC6E448E3F9E07B5EB8B36A6854465,
	OptionalComponent__ctor_m0C2271D0052B92F9DC8B4110DBEA83AAF3B0230F,
	OptionalMinMax_GetFixed_mEA05EC0969CAFA01EDDF19ADCE8E8E69E3A592CD_AdjustorThunk,
	OptionalMinMax__ctor_m5F655298C0C9D85DCEF347D2D097B07EBD37A9B6_AdjustorThunk,
	ReorderableGameObject__ctor_m336FA8C53BBD51C6BE976A7414C764F069DF0D08,
	ReorderableGameObjectList__ctor_m123C7312A579EB137365A70D072E0DBDE7836340,
	ReorderableTransform__ctor_m81B1C5CD4556901BED022C55D9BD8CC52C2C2EFB,
	ReorderableTransformList__ctor_mBBDBE4E3CC12E47C1F5558B0EDFA94FD6786A5A7,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	SceneReference_get_IsAssigned_m6E44A8A337B6F6AF530BDD35258F000BD275C528,
	SceneReference_ValidateScene_m8A0B2C6C268D7C937678DEB02A27D93CCB23D956,
	SceneReference_LoadScene_m2FF5EDF4B15F8AAED899179508DDCF8D709E646E,
	SceneReference_LoadSceneAsync_m20E7774DD70F7FED0703AE8FC4850DDFF7000C51,
	SceneReference_OnBeforeSerialize_m65A973515142B1355CD58D2A50399BAD5B53675C,
	SceneReference_OnAfterDeserialize_mDB4620AB2B05CE3CF7ABB7E048D6AC42749D6763,
	SceneReference__ctor_m01FCEE5E570E41002B823884A3111B2F1890100A,
	NULL,
	NULL,
	UIFollow_get_IsOffscreen_mCC8C9D86F15B120A356D6704E6792AACDB053B3E,
	UIFollow_get_Transform_mC07145D14A73C89F49E88488E2E1893BBD261CA2,
	UIFollow_get_OffscreenOffset_m9274B4C7E7131D605D67F5D829C6B3463B086B2B,
	UIFollow_LateUpdate_m871739453FAC2661771257E8E5DBDA1E50B7F696,
	UIFollow_ToggleCanvasOffscreen_mDC588698CF908A983D5BF54DC95394A034473684,
	UIFollow__ctor_mF90B0DC994DF365FD7DED3C8F073B97CD11729FB,
	UIImageBasedButton_get_AlternativeSpriteset_m54B6B6B0AD4788FE12639989436D654464832F8C,
	UIImageBasedButton_set_AlternativeSpriteset_m3DDF51EA95D5E1A6DF2E137AFA58353B5AEEFD02,
	UIImageBasedButton_Awake_m9E430554EBF16B600DE2712FB5346870B3437A9C,
	UIImageBasedButton_OnEnable_mEC05A6C3BA15ED5D597CAA471B87F452AA84390B,
	UIImageBasedButton_OnDisable_mD9E9794FB05AC82E9728D79CEADB02F8BA5E8FA2,
	UIImageBasedButton_ToggleSprites_m968169A48DF0DC78ED7946DBECAF60333C46F207,
	UIImageBasedButton_UpdateSprites_m06798558800E9551E4547CEBE53C1E26C90E19CD,
	UIImageBasedButton_UpdateSprites_m495685E8B67918B40DE8040F77A800172BBD588F,
	UIImageBasedButton_OnSelect_mBD29DC04AC34BDC7C6A06C079A531D3FFE6F243F,
	UIImageBasedButton_OnDeselect_m937ABBABAD763B378602B777AAB47DBA74A4AF35,
	UIImageBasedButton__ctor_mAF10D5F703F043DA23B0B308522365145D55217D,
	UIImageBasedToggle_Start_m56B155E42643B804254E4F156339C949E199A693,
	UIImageBasedToggle__ctor_mB58E4CDD03C7949F6360E29AC729C85742F97E05,
	UIImageBasedToggle_U3CStartU3Eb__7_0_mACDDEAD6982E745BC39D3D4B99339C861C87F909,
	UIRelativePosition_Start_m4274E303A4D0BB4FF6C3B20AEFBDD756EE11E967,
	UIRelativePosition_LateUpdate_m11AD7EC6D9A45739D337C34296B2F068D704379F,
	UIRelativePosition__ctor_m5BC7742BF6DA36065E0221B4DDA9ADEB503558FB,
	UISizeBy_Start_m27F2357316075DC7872A7C390AF6DE9A3CA453C4,
	UISizeBy_LateUpdate_m9C86874746DC6DE8A12BDCD56F417BFFAE124489,
	UISizeBy__ctor_mE994E4B2FDD836F2E7D8F69B37A7E93C0ECD858A,
	WaitForUnscaledSeconds_get_keepWaiting_m098BE8DE9410178DB7798D73C76941B4960FD06C,
	WaitForUnscaledSeconds__ctor_m87090939FDDBA544608DFF76C6E36AC0636C1379,
	AttributeBase__ctor_m742493FCE3450438182373229D1187A23E4DE724,
	CoroutineOwner__ctor_mD471960B33AA8DC0CB07B34A27AB4C760F7EE1F6,
	SceneBundle__ctor_mCD0025603FF33E97733D6BFE060F618D66499328,
	SceneBundle_get_StringData_m62AB996F148B5138AA11AF621DC5B3340BB31E95,
	SceneBundle_get_FloatData_m832BEB7033140016B9452BE307B8831F600D76B7,
	SceneBundle_get_IntData_m8E57D39AD8172D7847E3D6EFF34D1B1227F7DF38,
	SceneBundle_get_BoolData_m476F31D14CAA80B988EFF6130FFAF89746809F9A,
	SceneBundle_get_ObjectData_m71B978EBCF786B3B554FF2CD54297876E137A081,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	GLDraw_ClipTest_mDBF817FFC5A42844B52CEF75D93598E1B3A16B0C,
	GLDraw_SegmentRectIntersection_mCFC384446A6832CAC13CE1DF372CB2A9D270DB09,
	GLDraw_BeginGroup_mF0D8AE8525734A580B3BB18ABDFB3895DA372267,
	GLDraw_EndGroup_m3B81E3B8788064835D5060AB9BFC71DE84D24A71,
	GLDraw_CreateMaterial_mD9494853CD9C35CB85AC41BF6FAC335BABB53C3C,
	GLDraw_DrawLine_m34AAB5BC05AA64790735E6024A939697B71EEF08,
	GLDraw_DrawBox_mBA5A8535770885F005B7A58384FCA2C77F1FAC67,
	GLDraw_DrawBox_m58691F75A189039BC9FB3978C86AEF36A9690326,
	GLDraw_DrawRoundedBox_m6DA67A71279FB9E6C7F89D6EC6A211208FF23A62,
	GLDraw_DrawConnectingCurve_m0E9A5256D82D781EB85040A705A4E5F502A5C7C8,
	GLDraw_DrawBezier_m7A467516AF23890406D0E193CD79561B62CB16C6,
	GLDraw_DrawBezier_m1E024FFE64F42A1D68452A03C9B6E983D8ECBC85,
	GLDraw_CubeBezier_m171FDB596EB9E1CD1A57F21D389310032068CB85,
	GLDraw__ctor_m084F766934ED763B80C62AE52B542F159E400B31,
	MyLogger_get_Session_mA0B76556AE6F9C907E083F20DC099E0E371FA72F,
	MyLogger_set_Session_m446DF83F42CB5DB70FD7556E7FC3CD5E4A9EF075,
	MyLogger_get_Version_m6654665AD3FB2A991894A6614DDB08CB099A6B98,
	MyLogger_set_Version_mFC8F3FC0FA648B9F202403159DE1BA86FAEFB419,
	MyLogger__cctor_m6C681F1E84464A3D3FEDEE2044D6466663A1E287,
	MyLogger_Log_m7FED437699EF97BFC9C1A6DB339CBF1A57D4BFD9,
	MyLogger_GetCurrentTime_mE32E6D23E811A8CCCD9CF16522F344907D3E4F55,
	MyLogger_Log_m914B2AF381AAA432C2C6F01F219CD60D796B54CA,
	CollectionWrapperBase__ctor_m08594901DF9A3BEBAA364C7D0CD27D117E072A3E,
	CommentaryComponent__ctor_m9FF1D1D5FA7AA4D81639CDB4D02C51A0C0AD24B7,
	NULL,
	OptionalParent__ctor_m405E7B28CFB8E3F4DB1CEE9421FCC4485CF1D53B,
	ReorderableBase__ctor_mB6B0254A16026BFA84D929B8B9EFE284E9F718AB,
	U3CU3Ec__cctor_m54F5E9394BDD5322BEA8DDB257616766A57A39DD,
	U3CU3Ec__ctor_m474ABB4F8F02CD667A1743215517328571000A35,
	U3CU3Ec_U3C_ctorU3Eb__3_0_mE035A4D581B6A968960909135292244B33452CC0,
	U3CStartNextCoroutineU3Ed__8__ctor_m4EA7B5DAC2C77CB6DCB634E3B1C3ADE9CF88098E,
	U3CStartNextCoroutineU3Ed__8_System_IDisposable_Dispose_mB67CCC57402558E537533D1E593DF02272C747FA,
	U3CStartNextCoroutineU3Ed__8_MoveNext_m4FA9630E2D51852EE797E4ED3B35A24718F7122C,
	U3CStartNextCoroutineU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAF0E338B962405C9D7605BC847A294D9CA9C010F,
	U3CStartNextCoroutineU3Ed__8_System_Collections_IEnumerator_Reset_mAF32506342041BC3FEAD7C2A50BEB401B35F28C3,
	U3CStartNextCoroutineU3Ed__8_System_Collections_IEnumerator_get_Current_m9142C3AD07AA8B083D5741F9141BAF7708DD1853,
	U3CDoStartU3Ed__9__ctor_m8682A110BD199E7CF14FB39FB6ADF0777F7C8AD6,
	U3CDoStartU3Ed__9_System_IDisposable_Dispose_m81CF7720E8EB16F125464F2ACDF68FF91220657C,
	U3CDoStartU3Ed__9_MoveNext_mAA51C82D96BD869F3606470B2A061879A47DC367,
	U3CDoStartU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m756E95D64C2B3C05E77B580E0C966151206D0ADA,
	U3CDoStartU3Ed__9_System_Collections_IEnumerator_Reset_mE317143C8CF39E1AB8A1EDCD0707A093D00AECCB,
	U3CDoStartU3Ed__9_System_Collections_IEnumerator_get_Current_mB847852FEC4FB8D6D5EAFB6D083F72AAAA7205FF,
	U3CDelayedActionU3Ed__0__ctor_m410354A6F0C37F9E1ACC0B00F90BA88B2230FF3E,
	U3CDelayedActionU3Ed__0_System_IDisposable_Dispose_mBDA2088C2200087D5747B2A1E8C260FED1F64095,
	U3CDelayedActionU3Ed__0_MoveNext_mF63545F6BF1D2642B68F00E773D1D2049F17802C,
	U3CDelayedActionU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2CEE6F6BA31B190478772FB8B032438499B490F8,
	U3CDelayedActionU3Ed__0_System_Collections_IEnumerator_Reset_m4198FB47B3DFB33BB440FAE7AD3A579179641288,
	U3CDelayedActionU3Ed__0_System_Collections_IEnumerator_get_Current_mC548478119C694584F21E5142DCD41FBC28B03E9,
	U3CDelayedActionU3Ed__2__ctor_mF411A8B8612C8C5902F6127B60E012C5E69123FE,
	U3CDelayedActionU3Ed__2_System_IDisposable_Dispose_m9127FEE171B2B5F7E069130108757EC032509798,
	U3CDelayedActionU3Ed__2_MoveNext_m1F7D7041041A91DCEF0F10D36FD00B2B60DCCE50,
	U3CDelayedActionU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m50F5D1A6316EAA5A70F043B65D12921D0FCEBDB6,
	U3CDelayedActionU3Ed__2_System_Collections_IEnumerator_Reset_m648EB4E8117E86A65346E0E64CD2537B81F85CC4,
	U3CDelayedActionU3Ed__2_System_Collections_IEnumerator_get_Current_m61BCD83EA935F94A3AF89F43C9B9C009A0625D16,
	U3CDelayedUiSelectionU3Ed__4__ctor_m180A12659FC29107343A4C29560A7E0CBF3C2CBD,
	U3CDelayedUiSelectionU3Ed__4_System_IDisposable_Dispose_m17938442B04C755446EEEA90E472EACF199B2A32,
	U3CDelayedUiSelectionU3Ed__4_MoveNext_m5B707A62A8045FB1B1427A41781120DE33B3FED9,
	U3CDelayedUiSelectionU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA7926A0C17424E9CBACE702CAF8723A8AFF97D5F,
	U3CDelayedUiSelectionU3Ed__4_System_Collections_IEnumerator_Reset_m5F9BD844EE59FA0FE069D6CD1A5594C9F575A73E,
	U3CDelayedUiSelectionU3Ed__4_System_Collections_IEnumerator_get_Current_m2196AFDE3895C8A7AE267865ABBA2B41CC0FAC48,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__cctor_m635F7DC05C61849749EF9055BA3E657480AF1D52,
	U3CU3Ec__ctor_m776DEEAD756C0853C4A0A7D362DEB19E80377491,
	U3CU3Ec_U3COneHitPerInstanceU3Eb__13_0_m7C5A838B133842C2A683600F4BF7F126FDC64A83,
	U3CU3Ec_U3COneHitPerInstanceU3Eb__13_1_m7C2A34C05CA7649D1AFA4B2AF8DB119C77BF770C,
	U3CU3Ec_U3COneHitPerInstanceU3Eb__14_0_mAF054495E63D62A2915DD78A7D2EFFDB63BA20A6,
	U3CU3Ec_U3COneHitPerInstanceU3Eb__14_1_mFA2CE68A5E613506C5AD4EE0A241FCC30C4DA552,
	U3CU3Ec_U3COneHitPerInstanceListU3Eb__15_0_mBFB4BB5A41B2142F33E80E6CF6AA487A68A4A998,
	U3CU3Ec_U3COneHitPerInstanceListU3Eb__15_1_mB7E3FB4D506CF56150567172FEFCA5C3FF2743B5,
	U3CGetPointsOnPathU3Ed__3__ctor_m4251E7B3CDF45C48E565819D96D8CC60C1D2705E,
	U3CGetPointsOnPathU3Ed__3_System_IDisposable_Dispose_mB3A665FFDD33285D096D6257A53CB67F03D7AECD,
	U3CGetPointsOnPathU3Ed__3_MoveNext_mA737621DA1FE6D3FECBB855C9A01883C1A000780,
	U3CGetPointsOnPathU3Ed__3_System_Collections_Generic_IEnumeratorU3CUnityEngine_Vector3U3E_get_Current_m74137D869B6BBA00A2AE35336714F6AB8A70275B,
	U3CGetPointsOnPathU3Ed__3_System_Collections_IEnumerator_Reset_m807FC5C5B3ABC8157654882750BFA13107BE65E3,
	U3CGetPointsOnPathU3Ed__3_System_Collections_IEnumerator_get_Current_mB7820451AD957A79D629C15525DC05CE479C40CC,
	U3CGetPointsOnPathU3Ed__3_System_Collections_Generic_IEnumerableU3CUnityEngine_Vector3U3E_GetEnumerator_m899F056E7D7467C537A75E0C7C6881BD73B8B7E1,
	U3CGetPointsOnPathU3Ed__3_System_Collections_IEnumerable_GetEnumerator_m73D0B5C76BE3AE1994964480CCDC5D765EFC90E3,
	U3CTransformShakeCoroutineU3Ed__4__ctor_m9C5DF0A45878D45904625FB6020BBA37B5AB797A,
	U3CTransformShakeCoroutineU3Ed__4_System_IDisposable_Dispose_m70A5E22242A722C7DB3112A99E152DDFFA6645E5,
	U3CTransformShakeCoroutineU3Ed__4_MoveNext_mCC641F6CE24D200BD6E35CFEBD6F3CADDFCA7765,
	U3CTransformShakeCoroutineU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF96E6E635A2F1D4311019F72B68806E17D7B40BA,
	U3CTransformShakeCoroutineU3Ed__4_System_Collections_IEnumerator_Reset_m7F91E0DF3F5CB69B1BAE9ADA622C447CF75BECEC,
	U3CTransformShakeCoroutineU3Ed__4_System_Collections_IEnumerator_get_Current_m51CA98AB8E64A3F362226A0B3EB811FD9B759FB7,
	TimeTestData__ctor_mA29F41B0CD9DA50242B657AA43EFC0315E8B24B0_AdjustorThunk,
	TimeTestData_EndTest_mCE5E239CAC2EBC2A6FC7D1F63BEB4F102E6078A1_AdjustorThunk,
	TimeTestData__cctor_m88BA2D77097807B256486732CED45580E7BAE60A,
	Triangulator__ctor_mCC4E5B8A141191F9E9E2BF5951235C2B6FBCFC97,
	Triangulator_Triangulate_m319BB31610579186F1E00A0114A5E2E85A324243,
	Triangulator_Area_mF2D92E1EB2F84254160367DE77B95B8A5C2BC534,
	Triangulator_Snip_mB0BB348462761EA0FC53C371B2ABB3358CFAF076,
	Triangulator_InsideTriangle_m147A350D64C1AD44F354CC8E012D403627E341D5,
	GuidInfo_add_OnAdd_m2B28E812EBE755F4A663153E507BBC9F63434D55_AdjustorThunk,
	GuidInfo_remove_OnAdd_m38AABC1F4B41258A35B33DA22FBC75B1F375D6EC_AdjustorThunk,
	GuidInfo_add_OnRemove_mF66D6196140B361802E6D2BD0650A0682783328A_AdjustorThunk,
	GuidInfo_remove_OnRemove_m8A90A28ABD79EC7EBE60ADF64B059A4906EC93D5_AdjustorThunk,
	GuidInfo__ctor_m293487E8CDA99B98A55E395B2AB1AC9F32345129_AdjustorThunk,
	GuidInfo_HandleAddCallback_m61011944455E0B2BB8B8CEEC9529C24DC358E583_AdjustorThunk,
	GuidInfo_HandleRemoveCallback_mE8F42967EB67A24DA9D1BCB7E8727C6EE46779DE_AdjustorThunk,
	U3CU3Ec__cctor_mA80B134949C8C61E85A46BC5C256B895705CC5A5,
	U3CU3Ec__ctor_m75E80CC08EE6FCDA9F657B26C4BD59A5D38385EF,
	U3CU3Ec_U3C_ctorU3Eb__15_0_m38E337F86D67C6C0093386A2D7FAC3E3BABDB3E0,
	U3CU3Ec_U3C_ctorU3Eb__15_1_mBC1F2376332970D717A9433D9AB633D2BD7AA60F,
	U3CU3Ec_U3C_ctorU3Eb__16_0_mF6EED94F79CA736C95AA6E12DE04CFA0173DF44B,
	U3CU3Ec_U3C_ctorU3Eb__16_1_mFC856826D4B28B6557246A5A115245E4986FB483,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	SceneLoadException__ctor_m3C09859AC9597AFF6F7DB21EE619A7D22461727E,
	U3CU3Ec__cctor_mCB893FADA4B5DE504CDB3B9BAF595B9DAFC8D5B4,
	U3CU3Ec__ctor_mD19E49692D7BD29724D3808AB9B12BF8DC8F7226,
	U3CU3Ec_U3C_cctorU3Eb__12_0_m70FEE30D109070E79586FC6C90945391CDCFFC02,
	U3CU3Ec_U3C_cctorU3Eb__12_1_m22F2D61C2650896A266124700E65925FB97891A8,
};
static const int32_t s_InvokerIndices[556] = 
{
	23,
	23,
	23,
	23,
	23,
	23,
	675,
	26,
	26,
	31,
	390,
	23,
	23,
	275,
	1134,
	1024,
	1024,
	157,
	1838,
	1838,
	1839,
	1839,
	275,
	1134,
	23,
	23,
	23,
	26,
	32,
	26,
	23,
	23,
	390,
	23,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1141,
	1141,
	1141,
	1137,
	1218,
	1218,
	1146,
	1840,
	1840,
	1137,
	4,
	0,
	1,
	111,
	3,
	0,
	1,
	10,
	102,
	26,
	28,
	23,
	28,
	-1,
	-1,
	1070,
	1841,
	1841,
	1842,
	1843,
	1844,
	1069,
	1725,
	1845,
	0,
	1,
	0,
	1,
	-1,
	1846,
	-1,
	-1,
	-1,
	152,
	152,
	433,
	554,
	-1,
	-1,
	-1,
	0,
	0,
	0,
	1844,
	46,
	21,
	522,
	725,
	53,
	619,
	49,
	49,
	49,
	49,
	522,
	522,
	522,
	1222,
	1848,
	-1,
	376,
	218,
	1188,
	207,
	1849,
	1187,
	158,
	1187,
	1849,
	423,
	1219,
	1732,
	1730,
	0,
	0,
	152,
	1,
	152,
	0,
	0,
	0,
	151,
	1850,
	1851,
	554,
	554,
	205,
	122,
	1164,
	1195,
	1218,
	1164,
	1195,
	1218,
	1164,
	1218,
	1852,
	1853,
	1852,
	1853,
	1852,
	1853,
	111,
	1218,
	1218,
	1218,
	1218,
	1854,
	1164,
	1195,
	1218,
	1195,
	1164,
	1218,
	1164,
	1218,
	1852,
	1853,
	1855,
	1852,
	1853,
	1852,
	1853,
	1852,
	1855,
	1853,
	1852,
	1855,
	1853,
	1194,
	1194,
	1198,
	1199,
	1856,
	1857,
	1858,
	1859,
	1164,
	1195,
	1218,
	1194,
	1073,
	1860,
	1255,
	1347,
	1861,
	1862,
	1863,
	1864,
	111,
	1865,
	111,
	3,
	102,
	151,
	522,
	121,
	3,
	1266,
	49,
	3,
	775,
	1343,
	1727,
	1729,
	1570,
	1343,
	178,
	178,
	178,
	178,
	178,
	390,
	23,
	554,
	3,
	111,
	3,
	111,
	3,
	1866,
	1867,
	1868,
	1869,
	1870,
	23,
	3,
	23,
	23,
	14,
	102,
	14,
	23,
	122,
	111,
	23,
	0,
	23,
	14,
	23,
	23,
	-1,
	23,
	23,
	23,
	23,
	23,
	652,
	14,
	102,
	23,
	23,
	23,
	23,
	23,
	652,
	23,
	23,
	109,
	1873,
	1874,
	1875,
	1876,
	23,
	9,
	1216,
	1877,
	26,
	26,
	26,
	26,
	14,
	26,
	23,
	26,
	26,
	23,
	23,
	23,
	1024,
	157,
	1878,
	1879,
	1880,
	1881,
	1880,
	1881,
	1882,
	1879,
	1882,
	1879,
	1883,
	1881,
	-1,
	-1,
	-1,
	-1,
	23,
	23,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	89,
	23,
	43,
	23,
	0,
	23,
	43,
	23,
	23,
	23,
	1027,
	1884,
	23,
	23,
	23,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	102,
	23,
	32,
	34,
	23,
	23,
	23,
	-1,
	-1,
	102,
	14,
	1060,
	23,
	23,
	23,
	102,
	31,
	23,
	23,
	23,
	23,
	23,
	31,
	26,
	26,
	23,
	23,
	23,
	31,
	23,
	23,
	23,
	23,
	23,
	23,
	102,
	275,
	23,
	23,
	23,
	14,
	14,
	14,
	14,
	14,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1885,
	1886,
	1459,
	3,
	3,
	1887,
	1888,
	1887,
	1889,
	1887,
	1890,
	1891,
	1892,
	23,
	4,
	111,
	4,
	111,
	3,
	111,
	4,
	111,
	23,
	23,
	-1,
	23,
	23,
	3,
	23,
	28,
	32,
	23,
	102,
	14,
	23,
	14,
	32,
	23,
	102,
	14,
	23,
	14,
	32,
	23,
	102,
	14,
	23,
	14,
	32,
	23,
	102,
	14,
	23,
	14,
	32,
	23,
	102,
	14,
	23,
	14,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	3,
	23,
	1847,
	1669,
	104,
	28,
	104,
	28,
	32,
	23,
	102,
	1050,
	23,
	14,
	14,
	14,
	32,
	23,
	102,
	14,
	23,
	14,
	390,
	23,
	3,
	26,
	14,
	664,
	1871,
	1872,
	26,
	26,
	26,
	26,
	26,
	23,
	23,
	3,
	23,
	26,
	23,
	26,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	26,
	3,
	23,
	27,
	591,
};
static const Il2CppTokenRangePair s_rgctxIndices[29] = 
{
	{ 0x02000041, { 79, 5 } },
	{ 0x02000043, { 84, 39 } },
	{ 0x02000052, { 130, 4 } },
	{ 0x0200005C, { 134, 15 } },
	{ 0x0200006D, { 64, 4 } },
	{ 0x0200006E, { 68, 6 } },
	{ 0x0200006F, { 74, 5 } },
	{ 0x02000078, { 123, 3 } },
	{ 0x02000079, { 126, 4 } },
	{ 0x06000024, { 0, 1 } },
	{ 0x06000025, { 1, 1 } },
	{ 0x06000027, { 2, 2 } },
	{ 0x06000028, { 4, 2 } },
	{ 0x06000029, { 6, 3 } },
	{ 0x0600002B, { 9, 1 } },
	{ 0x0600002C, { 10, 1 } },
	{ 0x0600002D, { 11, 1 } },
	{ 0x0600002E, { 12, 4 } },
	{ 0x0600002F, { 16, 5 } },
	{ 0x06000030, { 21, 4 } },
	{ 0x06000031, { 25, 4 } },
	{ 0x06000049, { 29, 2 } },
	{ 0x0600004A, { 31, 4 } },
	{ 0x0600005A, { 35, 3 } },
	{ 0x0600005B, { 38, 3 } },
	{ 0x0600005C, { 41, 2 } },
	{ 0x06000061, { 43, 4 } },
	{ 0x06000062, { 47, 7 } },
	{ 0x06000063, { 54, 10 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[149] = 
{
	{ (Il2CppRGCTXDataType)2, 17325 },
	{ (Il2CppRGCTXDataType)2, 17327 },
	{ (Il2CppRGCTXDataType)2, 20443 },
	{ (Il2CppRGCTXDataType)2, 17331 },
	{ (Il2CppRGCTXDataType)3, 15297 },
	{ (Il2CppRGCTXDataType)3, 15298 },
	{ (Il2CppRGCTXDataType)2, 20444 },
	{ (Il2CppRGCTXDataType)2, 17337 },
	{ (Il2CppRGCTXDataType)2, 17335 },
	{ (Il2CppRGCTXDataType)2, 20445 },
	{ (Il2CppRGCTXDataType)3, 15299 },
	{ (Il2CppRGCTXDataType)3, 15300 },
	{ (Il2CppRGCTXDataType)3, 15301 },
	{ (Il2CppRGCTXDataType)2, 17346 },
	{ (Il2CppRGCTXDataType)2, 20446 },
	{ (Il2CppRGCTXDataType)2, 17347 },
	{ (Il2CppRGCTXDataType)3, 15302 },
	{ (Il2CppRGCTXDataType)3, 15303 },
	{ (Il2CppRGCTXDataType)2, 17348 },
	{ (Il2CppRGCTXDataType)2, 20447 },
	{ (Il2CppRGCTXDataType)3, 15304 },
	{ (Il2CppRGCTXDataType)3, 15305 },
	{ (Il2CppRGCTXDataType)3, 15306 },
	{ (Il2CppRGCTXDataType)3, 15307 },
	{ (Il2CppRGCTXDataType)3, 15308 },
	{ (Il2CppRGCTXDataType)3, 15309 },
	{ (Il2CppRGCTXDataType)3, 15310 },
	{ (Il2CppRGCTXDataType)3, 15311 },
	{ (Il2CppRGCTXDataType)3, 15312 },
	{ (Il2CppRGCTXDataType)1, 17378 },
	{ (Il2CppRGCTXDataType)2, 17378 },
	{ (Il2CppRGCTXDataType)2, 20450 },
	{ (Il2CppRGCTXDataType)1, 17380 },
	{ (Il2CppRGCTXDataType)2, 17379 },
	{ (Il2CppRGCTXDataType)2, 17380 },
	{ (Il2CppRGCTXDataType)3, 15313 },
	{ (Il2CppRGCTXDataType)2, 17393 },
	{ (Il2CppRGCTXDataType)3, 15314 },
	{ (Il2CppRGCTXDataType)3, 15315 },
	{ (Il2CppRGCTXDataType)2, 17394 },
	{ (Il2CppRGCTXDataType)3, 15316 },
	{ (Il2CppRGCTXDataType)3, 15317 },
	{ (Il2CppRGCTXDataType)2, 20451 },
	{ (Il2CppRGCTXDataType)2, 20452 },
	{ (Il2CppRGCTXDataType)3, 15318 },
	{ (Il2CppRGCTXDataType)3, 15319 },
	{ (Il2CppRGCTXDataType)3, 15320 },
	{ (Il2CppRGCTXDataType)2, 20453 },
	{ (Il2CppRGCTXDataType)3, 15321 },
	{ (Il2CppRGCTXDataType)3, 15322 },
	{ (Il2CppRGCTXDataType)2, 20454 },
	{ (Il2CppRGCTXDataType)3, 15323 },
	{ (Il2CppRGCTXDataType)3, 15324 },
	{ (Il2CppRGCTXDataType)3, 15325 },
	{ (Il2CppRGCTXDataType)2, 20455 },
	{ (Il2CppRGCTXDataType)3, 15326 },
	{ (Il2CppRGCTXDataType)2, 20456 },
	{ (Il2CppRGCTXDataType)3, 15327 },
	{ (Il2CppRGCTXDataType)3, 15328 },
	{ (Il2CppRGCTXDataType)3, 15329 },
	{ (Il2CppRGCTXDataType)2, 20457 },
	{ (Il2CppRGCTXDataType)3, 15330 },
	{ (Il2CppRGCTXDataType)3, 15331 },
	{ (Il2CppRGCTXDataType)3, 15332 },
	{ (Il2CppRGCTXDataType)2, 20459 },
	{ (Il2CppRGCTXDataType)3, 15333 },
	{ (Il2CppRGCTXDataType)2, 20459 },
	{ (Il2CppRGCTXDataType)1, 17410 },
	{ (Il2CppRGCTXDataType)2, 20460 },
	{ (Il2CppRGCTXDataType)3, 15334 },
	{ (Il2CppRGCTXDataType)2, 20460 },
	{ (Il2CppRGCTXDataType)2, 17415 },
	{ (Il2CppRGCTXDataType)2, 17414 },
	{ (Il2CppRGCTXDataType)3, 15335 },
	{ (Il2CppRGCTXDataType)2, 20461 },
	{ (Il2CppRGCTXDataType)3, 15336 },
	{ (Il2CppRGCTXDataType)2, 20461 },
	{ (Il2CppRGCTXDataType)2, 17421 },
	{ (Il2CppRGCTXDataType)3, 15337 },
	{ (Il2CppRGCTXDataType)2, 20462 },
	{ (Il2CppRGCTXDataType)2, 17540 },
	{ (Il2CppRGCTXDataType)3, 15338 },
	{ (Il2CppRGCTXDataType)1, 17540 },
	{ (Il2CppRGCTXDataType)3, 15339 },
	{ (Il2CppRGCTXDataType)2, 17546 },
	{ (Il2CppRGCTXDataType)3, 15340 },
	{ (Il2CppRGCTXDataType)3, 15341 },
	{ (Il2CppRGCTXDataType)2, 17547 },
	{ (Il2CppRGCTXDataType)3, 15342 },
	{ (Il2CppRGCTXDataType)3, 15343 },
	{ (Il2CppRGCTXDataType)3, 15344 },
	{ (Il2CppRGCTXDataType)3, 15345 },
	{ (Il2CppRGCTXDataType)2, 20463 },
	{ (Il2CppRGCTXDataType)3, 15346 },
	{ (Il2CppRGCTXDataType)2, 20270 },
	{ (Il2CppRGCTXDataType)2, 20271 },
	{ (Il2CppRGCTXDataType)2, 17557 },
	{ (Il2CppRGCTXDataType)3, 15347 },
	{ (Il2CppRGCTXDataType)3, 15348 },
	{ (Il2CppRGCTXDataType)3, 15349 },
	{ (Il2CppRGCTXDataType)2, 17548 },
	{ (Il2CppRGCTXDataType)3, 15350 },
	{ (Il2CppRGCTXDataType)2, 20464 },
	{ (Il2CppRGCTXDataType)3, 15351 },
	{ (Il2CppRGCTXDataType)2, 20465 },
	{ (Il2CppRGCTXDataType)2, 20466 },
	{ (Il2CppRGCTXDataType)2, 17549 },
	{ (Il2CppRGCTXDataType)3, 15352 },
	{ (Il2CppRGCTXDataType)2, 20467 },
	{ (Il2CppRGCTXDataType)3, 15353 },
	{ (Il2CppRGCTXDataType)3, 15354 },
	{ (Il2CppRGCTXDataType)3, 15355 },
	{ (Il2CppRGCTXDataType)3, 15356 },
	{ (Il2CppRGCTXDataType)3, 15357 },
	{ (Il2CppRGCTXDataType)3, 15358 },
	{ (Il2CppRGCTXDataType)3, 15359 },
	{ (Il2CppRGCTXDataType)3, 15360 },
	{ (Il2CppRGCTXDataType)2, 17554 },
	{ (Il2CppRGCTXDataType)3, 15361 },
	{ (Il2CppRGCTXDataType)3, 15362 },
	{ (Il2CppRGCTXDataType)2, 17556 },
	{ (Il2CppRGCTXDataType)3, 15363 },
	{ (Il2CppRGCTXDataType)3, 15364 },
	{ (Il2CppRGCTXDataType)2, 20468 },
	{ (Il2CppRGCTXDataType)3, 15365 },
	{ (Il2CppRGCTXDataType)3, 15366 },
	{ (Il2CppRGCTXDataType)3, 15367 },
	{ (Il2CppRGCTXDataType)2, 17565 },
	{ (Il2CppRGCTXDataType)3, 15368 },
	{ (Il2CppRGCTXDataType)3, 15369 },
	{ (Il2CppRGCTXDataType)2, 20471 },
	{ (Il2CppRGCTXDataType)2, 17612 },
	{ (Il2CppRGCTXDataType)3, 15370 },
	{ (Il2CppRGCTXDataType)1, 17612 },
	{ (Il2CppRGCTXDataType)2, 17646 },
	{ (Il2CppRGCTXDataType)3, 15371 },
	{ (Il2CppRGCTXDataType)3, 15372 },
	{ (Il2CppRGCTXDataType)3, 15373 },
	{ (Il2CppRGCTXDataType)3, 15374 },
	{ (Il2CppRGCTXDataType)3, 15375 },
	{ (Il2CppRGCTXDataType)3, 15376 },
	{ (Il2CppRGCTXDataType)3, 15377 },
	{ (Il2CppRGCTXDataType)3, 15378 },
	{ (Il2CppRGCTXDataType)3, 15379 },
	{ (Il2CppRGCTXDataType)3, 15380 },
	{ (Il2CppRGCTXDataType)3, 15381 },
	{ (Il2CppRGCTXDataType)3, 15382 },
	{ (Il2CppRGCTXDataType)2, 20472 },
	{ (Il2CppRGCTXDataType)3, 15383 },
};
extern const Il2CppCodeGenModule g_MyBoxCodeGenModule;
const Il2CppCodeGenModule g_MyBoxCodeGenModule = 
{
	"MyBox.dll",
	556,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	29,
	s_rgctxIndices,
	149,
	s_rgctxValues,
	NULL,
};
