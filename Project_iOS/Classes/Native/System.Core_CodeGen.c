﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7 ();
// 0x00000002 System.Exception System.Linq.Error::ArgumentOutOfRange(System.String)
extern void Error_ArgumentOutOfRange_mACFCB068F4E0C4EEF9E6EDDD59E798901C32C6C9 ();
// 0x00000003 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623 ();
// 0x00000004 System.Exception System.Linq.Error::NoElements()
extern void Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136 ();
// 0x00000005 System.Exception System.Linq.Error::NotSupported()
extern void Error_NotSupported_mD771E9977E8BE0B8298A582AB0BB74D1CF10900D ();
// 0x00000006 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000007 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
// 0x00000008 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000009 System.Func`2<TSource,TResult> System.Linq.Enumerable::CombineSelectors(System.Func`2<TSource,TMiddle>,System.Func`2<TMiddle,TResult>)
// 0x0000000A System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Take(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x0000000B System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::TakeIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x0000000C System.Collections.Generic.IEnumerable`1<System.Linq.IGrouping`2<TKey,TSource>> System.Linq.Enumerable::GroupBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000000D TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000E System.Collections.Generic.List`1<TSource> System.Linq.Enumerable::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000F System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::OfType(System.Collections.IEnumerable)
// 0x00000010 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::OfTypeIterator(System.Collections.IEnumerable)
// 0x00000011 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Cast(System.Collections.IEnumerable)
// 0x00000012 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::CastIterator(System.Collections.IEnumerable)
// 0x00000013 TSource System.Linq.Enumerable::First(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000014 TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000015 TSource System.Linq.Enumerable::ElementAt(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x00000016 System.Collections.Generic.IEnumerable`1<System.Int32> System.Linq.Enumerable::Range(System.Int32,System.Int32)
extern void Enumerable_Range_m7D095DA6BE22CF27F2556EF0DD41FB62B454BA7C ();
// 0x00000017 System.Collections.Generic.IEnumerable`1<System.Int32> System.Linq.Enumerable::RangeIterator(System.Int32,System.Int32)
extern void Enumerable_RangeIterator_m4E1661306100838EF4523D61D1F58E1F4E935A75 ();
// 0x00000018 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000019 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000001A System.Int32 System.Linq.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000001B System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource)
// 0x0000001C System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x0000001D System.Void System.Linq.Enumerable_Iterator`1::.ctor()
// 0x0000001E TSource System.Linq.Enumerable_Iterator`1::get_Current()
// 0x0000001F System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_Iterator`1::Clone()
// 0x00000020 System.Void System.Linq.Enumerable_Iterator`1::Dispose()
// 0x00000021 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_Iterator`1::GetEnumerator()
// 0x00000022 System.Boolean System.Linq.Enumerable_Iterator`1::MoveNext()
// 0x00000023 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_Iterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000024 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000025 System.Object System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x00000026 System.Collections.IEnumerator System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000027 System.Void System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.Reset()
// 0x00000028 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000029 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Clone()
// 0x0000002A System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::Dispose()
// 0x0000002B System.Boolean System.Linq.Enumerable_WhereEnumerableIterator`1::MoveNext()
// 0x0000002C System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereEnumerableIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000002D System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000002E System.Void System.Linq.Enumerable_WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x0000002F System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Clone()
// 0x00000030 System.Boolean System.Linq.Enumerable_WhereArrayIterator`1::MoveNext()
// 0x00000031 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereArrayIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000032 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000033 System.Void System.Linq.Enumerable_WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000034 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Clone()
// 0x00000035 System.Boolean System.Linq.Enumerable_WhereListIterator`1::MoveNext()
// 0x00000036 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereListIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000037 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000038 System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000039 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Clone()
// 0x0000003A System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Dispose()
// 0x0000003B System.Boolean System.Linq.Enumerable_WhereSelectEnumerableIterator`2::MoveNext()
// 0x0000003C System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x0000003D System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x0000003E System.Void System.Linq.Enumerable_WhereSelectArrayIterator`2::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x0000003F System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Clone()
// 0x00000040 System.Boolean System.Linq.Enumerable_WhereSelectArrayIterator`2::MoveNext()
// 0x00000041 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectArrayIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000042 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000043 System.Void System.Linq.Enumerable_WhereSelectListIterator`2::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000044 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Clone()
// 0x00000045 System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2::MoveNext()
// 0x00000046 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectListIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000047 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000048 System.Void System.Linq.Enumerable_<>c__DisplayClass6_0`1::.ctor()
// 0x00000049 System.Boolean System.Linq.Enumerable_<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x0000004A System.Void System.Linq.Enumerable_<>c__DisplayClass7_0`3::.ctor()
// 0x0000004B TResult System.Linq.Enumerable_<>c__DisplayClass7_0`3::<CombineSelectors>b__0(TSource)
// 0x0000004C System.Void System.Linq.Enumerable_<TakeIterator>d__25`1::.ctor(System.Int32)
// 0x0000004D System.Void System.Linq.Enumerable_<TakeIterator>d__25`1::System.IDisposable.Dispose()
// 0x0000004E System.Boolean System.Linq.Enumerable_<TakeIterator>d__25`1::MoveNext()
// 0x0000004F System.Void System.Linq.Enumerable_<TakeIterator>d__25`1::<>m__Finally1()
// 0x00000050 TSource System.Linq.Enumerable_<TakeIterator>d__25`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x00000051 System.Void System.Linq.Enumerable_<TakeIterator>d__25`1::System.Collections.IEnumerator.Reset()
// 0x00000052 System.Object System.Linq.Enumerable_<TakeIterator>d__25`1::System.Collections.IEnumerator.get_Current()
// 0x00000053 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_<TakeIterator>d__25`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x00000054 System.Collections.IEnumerator System.Linq.Enumerable_<TakeIterator>d__25`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000055 System.Void System.Linq.Enumerable_<OfTypeIterator>d__97`1::.ctor(System.Int32)
// 0x00000056 System.Void System.Linq.Enumerable_<OfTypeIterator>d__97`1::System.IDisposable.Dispose()
// 0x00000057 System.Boolean System.Linq.Enumerable_<OfTypeIterator>d__97`1::MoveNext()
// 0x00000058 System.Void System.Linq.Enumerable_<OfTypeIterator>d__97`1::<>m__Finally1()
// 0x00000059 TResult System.Linq.Enumerable_<OfTypeIterator>d__97`1::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x0000005A System.Void System.Linq.Enumerable_<OfTypeIterator>d__97`1::System.Collections.IEnumerator.Reset()
// 0x0000005B System.Object System.Linq.Enumerable_<OfTypeIterator>d__97`1::System.Collections.IEnumerator.get_Current()
// 0x0000005C System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable_<OfTypeIterator>d__97`1::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x0000005D System.Collections.IEnumerator System.Linq.Enumerable_<OfTypeIterator>d__97`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000005E System.Void System.Linq.Enumerable_<CastIterator>d__99`1::.ctor(System.Int32)
// 0x0000005F System.Void System.Linq.Enumerable_<CastIterator>d__99`1::System.IDisposable.Dispose()
// 0x00000060 System.Boolean System.Linq.Enumerable_<CastIterator>d__99`1::MoveNext()
// 0x00000061 System.Void System.Linq.Enumerable_<CastIterator>d__99`1::<>m__Finally1()
// 0x00000062 TResult System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x00000063 System.Void System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.IEnumerator.Reset()
// 0x00000064 System.Object System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.IEnumerator.get_Current()
// 0x00000065 System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x00000066 System.Collections.IEnumerator System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000067 System.Void System.Linq.Enumerable_<RangeIterator>d__115::.ctor(System.Int32)
extern void U3CRangeIteratorU3Ed__115__ctor_m5D6B6F8CA96D435FC1C8B6E92B93CAFB45F69228 ();
// 0x00000068 System.Void System.Linq.Enumerable_<RangeIterator>d__115::System.IDisposable.Dispose()
extern void U3CRangeIteratorU3Ed__115_System_IDisposable_Dispose_m2CAD68A0BE64B4A83FD83AC61594B867245C3032 ();
// 0x00000069 System.Boolean System.Linq.Enumerable_<RangeIterator>d__115::MoveNext()
extern void U3CRangeIteratorU3Ed__115_MoveNext_mD7DDF94B1115FD5F5CF1447CF3184E26070A0A10 ();
// 0x0000006A System.Int32 System.Linq.Enumerable_<RangeIterator>d__115::System.Collections.Generic.IEnumerator<System.Int32>.get_Current()
extern void U3CRangeIteratorU3Ed__115_System_Collections_Generic_IEnumeratorU3CSystem_Int32U3E_get_Current_m8C79E2C2C740D58C773C1E760851E13294D8BE0D ();
// 0x0000006B System.Void System.Linq.Enumerable_<RangeIterator>d__115::System.Collections.IEnumerator.Reset()
extern void U3CRangeIteratorU3Ed__115_System_Collections_IEnumerator_Reset_m040EAB9597A54F82BE40807057EC9ECE18F9D9D2 ();
// 0x0000006C System.Object System.Linq.Enumerable_<RangeIterator>d__115::System.Collections.IEnumerator.get_Current()
extern void U3CRangeIteratorU3Ed__115_System_Collections_IEnumerator_get_Current_mD80D27D2D8E8A607D471DC168F366334313DA9EB ();
// 0x0000006D System.Collections.Generic.IEnumerator`1<System.Int32> System.Linq.Enumerable_<RangeIterator>d__115::System.Collections.Generic.IEnumerable<System.Int32>.GetEnumerator()
extern void U3CRangeIteratorU3Ed__115_System_Collections_Generic_IEnumerableU3CSystem_Int32U3E_GetEnumerator_m959E99ACAAE05CD3EDD16779A9A802EB52CD6FB4 ();
// 0x0000006E System.Collections.IEnumerator System.Linq.Enumerable_<RangeIterator>d__115::System.Collections.IEnumerable.GetEnumerator()
extern void U3CRangeIteratorU3Ed__115_System_Collections_IEnumerable_GetEnumerator_mD02DC82E366D6EFEF966D2C6F98186A08ED0E880 ();
// 0x0000006F System.Func`2<TElement,TElement> System.Linq.IdentityFunction`1::get_Instance()
// 0x00000070 System.Void System.Linq.IdentityFunction`1_<>c::.cctor()
// 0x00000071 System.Void System.Linq.IdentityFunction`1_<>c::.ctor()
// 0x00000072 TElement System.Linq.IdentityFunction`1_<>c::<get_Instance>b__1_0(TElement)
// 0x00000073 System.Linq.Lookup`2<TKey,TElement> System.Linq.Lookup`2::Create(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>,System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x00000074 System.Void System.Linq.Lookup`2::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x00000075 System.Collections.Generic.IEnumerator`1<System.Linq.IGrouping`2<TKey,TElement>> System.Linq.Lookup`2::GetEnumerator()
// 0x00000076 System.Collections.IEnumerator System.Linq.Lookup`2::System.Collections.IEnumerable.GetEnumerator()
// 0x00000077 System.Int32 System.Linq.Lookup`2::InternalGetHashCode(TKey)
// 0x00000078 System.Linq.Lookup`2_Grouping<TKey,TElement> System.Linq.Lookup`2::GetGrouping(TKey,System.Boolean)
// 0x00000079 System.Void System.Linq.Lookup`2::Resize()
// 0x0000007A System.Void System.Linq.Lookup`2_Grouping::Add(TElement)
// 0x0000007B System.Collections.Generic.IEnumerator`1<TElement> System.Linq.Lookup`2_Grouping::GetEnumerator()
// 0x0000007C System.Collections.IEnumerator System.Linq.Lookup`2_Grouping::System.Collections.IEnumerable.GetEnumerator()
// 0x0000007D System.Int32 System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.get_Count()
// 0x0000007E System.Boolean System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.get_IsReadOnly()
// 0x0000007F System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.Add(TElement)
// 0x00000080 System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.Clear()
// 0x00000081 System.Boolean System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.Contains(TElement)
// 0x00000082 System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.CopyTo(TElement[],System.Int32)
// 0x00000083 System.Boolean System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.Remove(TElement)
// 0x00000084 System.Int32 System.Linq.Lookup`2_Grouping::System.Collections.Generic.IList<TElement>.IndexOf(TElement)
// 0x00000085 System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.IList<TElement>.Insert(System.Int32,TElement)
// 0x00000086 System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.IList<TElement>.RemoveAt(System.Int32)
// 0x00000087 TElement System.Linq.Lookup`2_Grouping::System.Collections.Generic.IList<TElement>.get_Item(System.Int32)
// 0x00000088 System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.IList<TElement>.set_Item(System.Int32,TElement)
// 0x00000089 System.Void System.Linq.Lookup`2_Grouping::.ctor()
// 0x0000008A System.Void System.Linq.Lookup`2_Grouping_<GetEnumerator>d__7::.ctor(System.Int32)
// 0x0000008B System.Void System.Linq.Lookup`2_Grouping_<GetEnumerator>d__7::System.IDisposable.Dispose()
// 0x0000008C System.Boolean System.Linq.Lookup`2_Grouping_<GetEnumerator>d__7::MoveNext()
// 0x0000008D TElement System.Linq.Lookup`2_Grouping_<GetEnumerator>d__7::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x0000008E System.Void System.Linq.Lookup`2_Grouping_<GetEnumerator>d__7::System.Collections.IEnumerator.Reset()
// 0x0000008F System.Object System.Linq.Lookup`2_Grouping_<GetEnumerator>d__7::System.Collections.IEnumerator.get_Current()
// 0x00000090 System.Void System.Linq.Lookup`2_<GetEnumerator>d__12::.ctor(System.Int32)
// 0x00000091 System.Void System.Linq.Lookup`2_<GetEnumerator>d__12::System.IDisposable.Dispose()
// 0x00000092 System.Boolean System.Linq.Lookup`2_<GetEnumerator>d__12::MoveNext()
// 0x00000093 System.Linq.IGrouping`2<TKey,TElement> System.Linq.Lookup`2_<GetEnumerator>d__12::System.Collections.Generic.IEnumerator<System.Linq.IGrouping<TKey,TElement>>.get_Current()
// 0x00000094 System.Void System.Linq.Lookup`2_<GetEnumerator>d__12::System.Collections.IEnumerator.Reset()
// 0x00000095 System.Object System.Linq.Lookup`2_<GetEnumerator>d__12::System.Collections.IEnumerator.get_Current()
// 0x00000096 System.Void System.Linq.GroupedEnumerable`3::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>,System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x00000097 System.Collections.Generic.IEnumerator`1<System.Linq.IGrouping`2<TKey,TElement>> System.Linq.GroupedEnumerable`3::GetEnumerator()
// 0x00000098 System.Collections.IEnumerator System.Linq.GroupedEnumerable`3::System.Collections.IEnumerable.GetEnumerator()
// 0x00000099 System.Void System.Linq.Buffer`1::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
// 0x0000009A TElement[] System.Linq.Buffer`1::ToArray()
// 0x0000009B System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x0000009C System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x0000009D System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEnumerable`1<T>)
// 0x0000009E System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEnumerable`1<T>,System.Collections.Generic.IEqualityComparer`1<T>)
// 0x0000009F System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x000000A0 System.Void System.Collections.Generic.HashSet`1::CopyFrom(System.Collections.Generic.HashSet`1<T>)
// 0x000000A1 System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x000000A2 System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x000000A3 System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x000000A4 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x000000A5 System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x000000A6 System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x000000A7 System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x000000A8 System.Collections.Generic.HashSet`1_Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x000000A9 System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x000000AA System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000AB System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x000000AC System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x000000AD System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x000000AE System.Void System.Collections.Generic.HashSet`1::UnionWith(System.Collections.Generic.IEnumerable`1<T>)
// 0x000000AF System.Void System.Collections.Generic.HashSet`1::ExceptWith(System.Collections.Generic.IEnumerable`1<T>)
// 0x000000B0 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x000000B1 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x000000B2 System.Collections.Generic.IEqualityComparer`1<T> System.Collections.Generic.HashSet`1::get_Comparer()
// 0x000000B3 System.Void System.Collections.Generic.HashSet`1::TrimExcess()
// 0x000000B4 System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x000000B5 System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x000000B6 System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x000000B7 System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x000000B8 System.Void System.Collections.Generic.HashSet`1::AddValue(System.Int32,System.Int32,T)
// 0x000000B9 System.Boolean System.Collections.Generic.HashSet`1::AreEqualityComparersEqual(System.Collections.Generic.HashSet`1<T>,System.Collections.Generic.HashSet`1<T>)
// 0x000000BA System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x000000BB System.Void System.Collections.Generic.HashSet`1_Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x000000BC System.Void System.Collections.Generic.HashSet`1_Enumerator::Dispose()
// 0x000000BD System.Boolean System.Collections.Generic.HashSet`1_Enumerator::MoveNext()
// 0x000000BE T System.Collections.Generic.HashSet`1_Enumerator::get_Current()
// 0x000000BF System.Object System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.get_Current()
// 0x000000C0 System.Void System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.Reset()
static Il2CppMethodPointer s_methodPointers[192] = 
{
	Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7,
	Error_ArgumentOutOfRange_mACFCB068F4E0C4EEF9E6EDDD59E798901C32C6C9,
	Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623,
	Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136,
	Error_NotSupported_mD771E9977E8BE0B8298A582AB0BB74D1CF10900D,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Enumerable_Range_m7D095DA6BE22CF27F2556EF0DD41FB62B454BA7C,
	Enumerable_RangeIterator_m4E1661306100838EF4523D61D1F58E1F4E935A75,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CRangeIteratorU3Ed__115__ctor_m5D6B6F8CA96D435FC1C8B6E92B93CAFB45F69228,
	U3CRangeIteratorU3Ed__115_System_IDisposable_Dispose_m2CAD68A0BE64B4A83FD83AC61594B867245C3032,
	U3CRangeIteratorU3Ed__115_MoveNext_mD7DDF94B1115FD5F5CF1447CF3184E26070A0A10,
	U3CRangeIteratorU3Ed__115_System_Collections_Generic_IEnumeratorU3CSystem_Int32U3E_get_Current_m8C79E2C2C740D58C773C1E760851E13294D8BE0D,
	U3CRangeIteratorU3Ed__115_System_Collections_IEnumerator_Reset_m040EAB9597A54F82BE40807057EC9ECE18F9D9D2,
	U3CRangeIteratorU3Ed__115_System_Collections_IEnumerator_get_Current_mD80D27D2D8E8A607D471DC168F366334313DA9EB,
	U3CRangeIteratorU3Ed__115_System_Collections_Generic_IEnumerableU3CSystem_Int32U3E_GetEnumerator_m959E99ACAAE05CD3EDD16779A9A802EB52CD6FB4,
	U3CRangeIteratorU3Ed__115_System_Collections_IEnumerable_GetEnumerator_mD02DC82E366D6EFEF966D2C6F98186A08ED0E880,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[192] = 
{
	0,
	0,
	4,
	4,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	523,
	523,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	32,
	23,
	102,
	10,
	23,
	14,
	14,
	14,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[49] = 
{
	{ 0x02000004, { 72, 4 } },
	{ 0x02000005, { 76, 9 } },
	{ 0x02000006, { 87, 7 } },
	{ 0x02000007, { 96, 10 } },
	{ 0x02000008, { 108, 11 } },
	{ 0x02000009, { 122, 9 } },
	{ 0x0200000A, { 134, 12 } },
	{ 0x0200000B, { 149, 1 } },
	{ 0x0200000C, { 150, 2 } },
	{ 0x0200000D, { 152, 8 } },
	{ 0x0200000E, { 160, 6 } },
	{ 0x0200000F, { 166, 6 } },
	{ 0x02000011, { 172, 4 } },
	{ 0x02000012, { 176, 3 } },
	{ 0x02000014, { 179, 17 } },
	{ 0x02000015, { 200, 5 } },
	{ 0x02000016, { 205, 1 } },
	{ 0x02000018, { 206, 4 } },
	{ 0x02000019, { 210, 4 } },
	{ 0x0200001A, { 214, 36 } },
	{ 0x0200001C, { 250, 2 } },
	{ 0x06000006, { 0, 10 } },
	{ 0x06000007, { 10, 10 } },
	{ 0x06000008, { 20, 5 } },
	{ 0x06000009, { 25, 5 } },
	{ 0x0600000A, { 30, 1 } },
	{ 0x0600000B, { 31, 2 } },
	{ 0x0600000C, { 33, 4 } },
	{ 0x0600000D, { 37, 3 } },
	{ 0x0600000E, { 40, 2 } },
	{ 0x0600000F, { 42, 1 } },
	{ 0x06000010, { 43, 2 } },
	{ 0x06000011, { 45, 2 } },
	{ 0x06000012, { 47, 2 } },
	{ 0x06000013, { 49, 4 } },
	{ 0x06000014, { 53, 3 } },
	{ 0x06000015, { 56, 3 } },
	{ 0x06000018, { 59, 1 } },
	{ 0x06000019, { 60, 3 } },
	{ 0x0600001A, { 63, 2 } },
	{ 0x0600001B, { 65, 2 } },
	{ 0x0600001C, { 67, 5 } },
	{ 0x0600002C, { 85, 2 } },
	{ 0x06000031, { 94, 2 } },
	{ 0x06000036, { 106, 2 } },
	{ 0x0600003C, { 119, 3 } },
	{ 0x06000041, { 131, 3 } },
	{ 0x06000046, { 146, 3 } },
	{ 0x06000073, { 196, 4 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[252] = 
{
	{ (Il2CppRGCTXDataType)2, 20343 },
	{ (Il2CppRGCTXDataType)3, 15026 },
	{ (Il2CppRGCTXDataType)2, 20344 },
	{ (Il2CppRGCTXDataType)2, 20345 },
	{ (Il2CppRGCTXDataType)3, 15027 },
	{ (Il2CppRGCTXDataType)2, 20346 },
	{ (Il2CppRGCTXDataType)2, 20347 },
	{ (Il2CppRGCTXDataType)3, 15028 },
	{ (Il2CppRGCTXDataType)2, 20348 },
	{ (Il2CppRGCTXDataType)3, 15029 },
	{ (Il2CppRGCTXDataType)2, 20349 },
	{ (Il2CppRGCTXDataType)3, 15030 },
	{ (Il2CppRGCTXDataType)2, 20350 },
	{ (Il2CppRGCTXDataType)2, 20351 },
	{ (Il2CppRGCTXDataType)3, 15031 },
	{ (Il2CppRGCTXDataType)2, 20352 },
	{ (Il2CppRGCTXDataType)2, 20353 },
	{ (Il2CppRGCTXDataType)3, 15032 },
	{ (Il2CppRGCTXDataType)2, 20354 },
	{ (Il2CppRGCTXDataType)3, 15033 },
	{ (Il2CppRGCTXDataType)2, 20355 },
	{ (Il2CppRGCTXDataType)3, 15034 },
	{ (Il2CppRGCTXDataType)3, 15035 },
	{ (Il2CppRGCTXDataType)2, 13720 },
	{ (Il2CppRGCTXDataType)3, 15036 },
	{ (Il2CppRGCTXDataType)2, 20356 },
	{ (Il2CppRGCTXDataType)3, 15037 },
	{ (Il2CppRGCTXDataType)3, 15038 },
	{ (Il2CppRGCTXDataType)2, 13727 },
	{ (Il2CppRGCTXDataType)3, 15039 },
	{ (Il2CppRGCTXDataType)3, 15040 },
	{ (Il2CppRGCTXDataType)2, 20357 },
	{ (Il2CppRGCTXDataType)3, 15041 },
	{ (Il2CppRGCTXDataType)3, 15042 },
	{ (Il2CppRGCTXDataType)2, 20358 },
	{ (Il2CppRGCTXDataType)2, 20359 },
	{ (Il2CppRGCTXDataType)3, 15043 },
	{ (Il2CppRGCTXDataType)2, 20360 },
	{ (Il2CppRGCTXDataType)3, 15044 },
	{ (Il2CppRGCTXDataType)3, 15045 },
	{ (Il2CppRGCTXDataType)2, 13743 },
	{ (Il2CppRGCTXDataType)3, 15046 },
	{ (Il2CppRGCTXDataType)3, 15047 },
	{ (Il2CppRGCTXDataType)2, 20361 },
	{ (Il2CppRGCTXDataType)3, 15048 },
	{ (Il2CppRGCTXDataType)2, 13748 },
	{ (Il2CppRGCTXDataType)3, 15049 },
	{ (Il2CppRGCTXDataType)2, 20362 },
	{ (Il2CppRGCTXDataType)3, 15050 },
	{ (Il2CppRGCTXDataType)2, 20363 },
	{ (Il2CppRGCTXDataType)2, 20364 },
	{ (Il2CppRGCTXDataType)2, 13752 },
	{ (Il2CppRGCTXDataType)2, 20365 },
	{ (Il2CppRGCTXDataType)2, 13754 },
	{ (Il2CppRGCTXDataType)2, 20366 },
	{ (Il2CppRGCTXDataType)3, 15051 },
	{ (Il2CppRGCTXDataType)2, 20367 },
	{ (Il2CppRGCTXDataType)2, 13757 },
	{ (Il2CppRGCTXDataType)2, 20368 },
	{ (Il2CppRGCTXDataType)2, 13759 },
	{ (Il2CppRGCTXDataType)2, 13761 },
	{ (Il2CppRGCTXDataType)2, 20369 },
	{ (Il2CppRGCTXDataType)3, 15052 },
	{ (Il2CppRGCTXDataType)2, 20370 },
	{ (Il2CppRGCTXDataType)2, 13764 },
	{ (Il2CppRGCTXDataType)2, 20371 },
	{ (Il2CppRGCTXDataType)3, 15053 },
	{ (Il2CppRGCTXDataType)3, 15054 },
	{ (Il2CppRGCTXDataType)2, 20372 },
	{ (Il2CppRGCTXDataType)2, 13768 },
	{ (Il2CppRGCTXDataType)2, 20373 },
	{ (Il2CppRGCTXDataType)2, 13770 },
	{ (Il2CppRGCTXDataType)3, 15055 },
	{ (Il2CppRGCTXDataType)3, 15056 },
	{ (Il2CppRGCTXDataType)2, 13773 },
	{ (Il2CppRGCTXDataType)3, 15057 },
	{ (Il2CppRGCTXDataType)3, 15058 },
	{ (Il2CppRGCTXDataType)2, 13785 },
	{ (Il2CppRGCTXDataType)2, 20374 },
	{ (Il2CppRGCTXDataType)3, 15059 },
	{ (Il2CppRGCTXDataType)3, 15060 },
	{ (Il2CppRGCTXDataType)2, 13787 },
	{ (Il2CppRGCTXDataType)2, 20256 },
	{ (Il2CppRGCTXDataType)3, 15061 },
	{ (Il2CppRGCTXDataType)3, 15062 },
	{ (Il2CppRGCTXDataType)2, 20375 },
	{ (Il2CppRGCTXDataType)3, 15063 },
	{ (Il2CppRGCTXDataType)3, 15064 },
	{ (Il2CppRGCTXDataType)2, 13797 },
	{ (Il2CppRGCTXDataType)2, 20376 },
	{ (Il2CppRGCTXDataType)3, 15065 },
	{ (Il2CppRGCTXDataType)3, 15066 },
	{ (Il2CppRGCTXDataType)3, 14630 },
	{ (Il2CppRGCTXDataType)3, 15067 },
	{ (Il2CppRGCTXDataType)2, 20377 },
	{ (Il2CppRGCTXDataType)3, 15068 },
	{ (Il2CppRGCTXDataType)3, 15069 },
	{ (Il2CppRGCTXDataType)2, 13809 },
	{ (Il2CppRGCTXDataType)2, 20378 },
	{ (Il2CppRGCTXDataType)3, 15070 },
	{ (Il2CppRGCTXDataType)3, 15071 },
	{ (Il2CppRGCTXDataType)3, 15072 },
	{ (Il2CppRGCTXDataType)3, 15073 },
	{ (Il2CppRGCTXDataType)3, 15074 },
	{ (Il2CppRGCTXDataType)3, 14636 },
	{ (Il2CppRGCTXDataType)3, 15075 },
	{ (Il2CppRGCTXDataType)2, 20379 },
	{ (Il2CppRGCTXDataType)3, 15076 },
	{ (Il2CppRGCTXDataType)3, 15077 },
	{ (Il2CppRGCTXDataType)2, 13822 },
	{ (Il2CppRGCTXDataType)2, 20380 },
	{ (Il2CppRGCTXDataType)3, 15078 },
	{ (Il2CppRGCTXDataType)3, 15079 },
	{ (Il2CppRGCTXDataType)2, 13824 },
	{ (Il2CppRGCTXDataType)2, 20381 },
	{ (Il2CppRGCTXDataType)3, 15080 },
	{ (Il2CppRGCTXDataType)3, 15081 },
	{ (Il2CppRGCTXDataType)2, 20382 },
	{ (Il2CppRGCTXDataType)3, 15082 },
	{ (Il2CppRGCTXDataType)3, 15083 },
	{ (Il2CppRGCTXDataType)2, 20383 },
	{ (Il2CppRGCTXDataType)3, 15084 },
	{ (Il2CppRGCTXDataType)3, 15085 },
	{ (Il2CppRGCTXDataType)2, 13839 },
	{ (Il2CppRGCTXDataType)2, 20384 },
	{ (Il2CppRGCTXDataType)3, 15086 },
	{ (Il2CppRGCTXDataType)3, 15087 },
	{ (Il2CppRGCTXDataType)3, 15088 },
	{ (Il2CppRGCTXDataType)3, 14647 },
	{ (Il2CppRGCTXDataType)2, 20385 },
	{ (Il2CppRGCTXDataType)3, 15089 },
	{ (Il2CppRGCTXDataType)3, 15090 },
	{ (Il2CppRGCTXDataType)2, 20386 },
	{ (Il2CppRGCTXDataType)3, 15091 },
	{ (Il2CppRGCTXDataType)3, 15092 },
	{ (Il2CppRGCTXDataType)2, 13855 },
	{ (Il2CppRGCTXDataType)2, 20387 },
	{ (Il2CppRGCTXDataType)3, 15093 },
	{ (Il2CppRGCTXDataType)3, 15094 },
	{ (Il2CppRGCTXDataType)3, 15095 },
	{ (Il2CppRGCTXDataType)3, 15096 },
	{ (Il2CppRGCTXDataType)3, 15097 },
	{ (Il2CppRGCTXDataType)3, 15098 },
	{ (Il2CppRGCTXDataType)3, 14653 },
	{ (Il2CppRGCTXDataType)2, 20388 },
	{ (Il2CppRGCTXDataType)3, 15099 },
	{ (Il2CppRGCTXDataType)3, 15100 },
	{ (Il2CppRGCTXDataType)2, 20389 },
	{ (Il2CppRGCTXDataType)3, 15101 },
	{ (Il2CppRGCTXDataType)3, 15102 },
	{ (Il2CppRGCTXDataType)3, 15103 },
	{ (Il2CppRGCTXDataType)3, 15104 },
	{ (Il2CppRGCTXDataType)3, 15105 },
	{ (Il2CppRGCTXDataType)2, 13889 },
	{ (Il2CppRGCTXDataType)2, 13884 },
	{ (Il2CppRGCTXDataType)3, 15106 },
	{ (Il2CppRGCTXDataType)2, 13883 },
	{ (Il2CppRGCTXDataType)2, 20390 },
	{ (Il2CppRGCTXDataType)3, 15107 },
	{ (Il2CppRGCTXDataType)3, 15108 },
	{ (Il2CppRGCTXDataType)3, 15109 },
	{ (Il2CppRGCTXDataType)2, 13893 },
	{ (Il2CppRGCTXDataType)3, 15110 },
	{ (Il2CppRGCTXDataType)2, 20391 },
	{ (Il2CppRGCTXDataType)3, 15111 },
	{ (Il2CppRGCTXDataType)3, 15112 },
	{ (Il2CppRGCTXDataType)3, 15113 },
	{ (Il2CppRGCTXDataType)2, 13901 },
	{ (Il2CppRGCTXDataType)3, 15114 },
	{ (Il2CppRGCTXDataType)2, 20392 },
	{ (Il2CppRGCTXDataType)3, 15115 },
	{ (Il2CppRGCTXDataType)3, 15116 },
	{ (Il2CppRGCTXDataType)2, 20393 },
	{ (Il2CppRGCTXDataType)3, 15117 },
	{ (Il2CppRGCTXDataType)2, 13911 },
	{ (Il2CppRGCTXDataType)3, 15118 },
	{ (Il2CppRGCTXDataType)2, 20394 },
	{ (Il2CppRGCTXDataType)3, 15119 },
	{ (Il2CppRGCTXDataType)2, 20394 },
	{ (Il2CppRGCTXDataType)2, 13931 },
	{ (Il2CppRGCTXDataType)3, 15120 },
	{ (Il2CppRGCTXDataType)3, 15121 },
	{ (Il2CppRGCTXDataType)3, 15122 },
	{ (Il2CppRGCTXDataType)3, 15123 },
	{ (Il2CppRGCTXDataType)2, 20395 },
	{ (Il2CppRGCTXDataType)2, 20396 },
	{ (Il2CppRGCTXDataType)2, 20397 },
	{ (Il2CppRGCTXDataType)3, 15124 },
	{ (Il2CppRGCTXDataType)3, 15125 },
	{ (Il2CppRGCTXDataType)2, 13927 },
	{ (Il2CppRGCTXDataType)2, 13930 },
	{ (Il2CppRGCTXDataType)3, 15126 },
	{ (Il2CppRGCTXDataType)3, 15127 },
	{ (Il2CppRGCTXDataType)2, 13934 },
	{ (Il2CppRGCTXDataType)3, 15128 },
	{ (Il2CppRGCTXDataType)2, 20398 },
	{ (Il2CppRGCTXDataType)2, 13924 },
	{ (Il2CppRGCTXDataType)2, 20399 },
	{ (Il2CppRGCTXDataType)3, 15129 },
	{ (Il2CppRGCTXDataType)3, 15130 },
	{ (Il2CppRGCTXDataType)3, 15131 },
	{ (Il2CppRGCTXDataType)2, 20400 },
	{ (Il2CppRGCTXDataType)3, 15132 },
	{ (Il2CppRGCTXDataType)3, 15133 },
	{ (Il2CppRGCTXDataType)3, 15134 },
	{ (Il2CppRGCTXDataType)2, 13949 },
	{ (Il2CppRGCTXDataType)3, 15135 },
	{ (Il2CppRGCTXDataType)2, 20401 },
	{ (Il2CppRGCTXDataType)3, 15136 },
	{ (Il2CppRGCTXDataType)3, 15137 },
	{ (Il2CppRGCTXDataType)2, 20402 },
	{ (Il2CppRGCTXDataType)2, 13988 },
	{ (Il2CppRGCTXDataType)2, 13986 },
	{ (Il2CppRGCTXDataType)2, 20403 },
	{ (Il2CppRGCTXDataType)3, 15138 },
	{ (Il2CppRGCTXDataType)2, 20404 },
	{ (Il2CppRGCTXDataType)3, 15139 },
	{ (Il2CppRGCTXDataType)3, 15140 },
	{ (Il2CppRGCTXDataType)2, 13995 },
	{ (Il2CppRGCTXDataType)3, 15141 },
	{ (Il2CppRGCTXDataType)2, 13995 },
	{ (Il2CppRGCTXDataType)3, 15142 },
	{ (Il2CppRGCTXDataType)2, 14012 },
	{ (Il2CppRGCTXDataType)3, 15143 },
	{ (Il2CppRGCTXDataType)3, 15144 },
	{ (Il2CppRGCTXDataType)3, 15145 },
	{ (Il2CppRGCTXDataType)2, 20405 },
	{ (Il2CppRGCTXDataType)3, 15146 },
	{ (Il2CppRGCTXDataType)3, 15147 },
	{ (Il2CppRGCTXDataType)3, 15148 },
	{ (Il2CppRGCTXDataType)2, 13992 },
	{ (Il2CppRGCTXDataType)3, 15149 },
	{ (Il2CppRGCTXDataType)3, 15150 },
	{ (Il2CppRGCTXDataType)2, 13997 },
	{ (Il2CppRGCTXDataType)3, 15151 },
	{ (Il2CppRGCTXDataType)1, 20406 },
	{ (Il2CppRGCTXDataType)2, 13996 },
	{ (Il2CppRGCTXDataType)3, 15152 },
	{ (Il2CppRGCTXDataType)1, 13996 },
	{ (Il2CppRGCTXDataType)1, 13992 },
	{ (Il2CppRGCTXDataType)2, 20405 },
	{ (Il2CppRGCTXDataType)2, 13996 },
	{ (Il2CppRGCTXDataType)2, 13994 },
	{ (Il2CppRGCTXDataType)2, 13998 },
	{ (Il2CppRGCTXDataType)3, 15153 },
	{ (Il2CppRGCTXDataType)3, 15154 },
	{ (Il2CppRGCTXDataType)3, 15155 },
	{ (Il2CppRGCTXDataType)3, 15156 },
	{ (Il2CppRGCTXDataType)3, 15157 },
	{ (Il2CppRGCTXDataType)2, 13993 },
	{ (Il2CppRGCTXDataType)3, 15158 },
	{ (Il2CppRGCTXDataType)2, 14008 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	192,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	49,
	s_rgctxIndices,
	252,
	s_rgctxValues,
	NULL,
};
