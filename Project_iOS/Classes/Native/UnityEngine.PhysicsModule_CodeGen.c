﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Single UnityEngine.SoftJointLimit::get_limit()
extern void SoftJointLimit_get_limit_m40164161D58BA46F6F1EDA382FC552607BFC7D03_AdjustorThunk ();
// 0x00000002 System.Void UnityEngine.SoftJointLimit::set_limit(System.Single)
extern void SoftJointLimit_set_limit_m65386F3B7DE799354F233666054E36B4E244FE26_AdjustorThunk ();
// 0x00000003 System.Single UnityEngine.SoftJointLimit::get_bounciness()
extern void SoftJointLimit_get_bounciness_m387D59B7712B1E70CFA337F9E311573C0D928846_AdjustorThunk ();
// 0x00000004 System.Void UnityEngine.SoftJointLimit::set_bounciness(System.Single)
extern void SoftJointLimit_set_bounciness_mC19E24774A8CF6C1229AE5D83AFE13F9F6D0938B_AdjustorThunk ();
// 0x00000005 System.Single UnityEngine.SoftJointLimitSpring::get_spring()
extern void SoftJointLimitSpring_get_spring_mC5D04E551048F33C874E4A7022E6FE582094AE21_AdjustorThunk ();
// 0x00000006 System.Void UnityEngine.SoftJointLimitSpring::set_spring(System.Single)
extern void SoftJointLimitSpring_set_spring_mCC90918E4E61E1DBCB55653A3357B168B3281232_AdjustorThunk ();
// 0x00000007 System.Single UnityEngine.SoftJointLimitSpring::get_damper()
extern void SoftJointLimitSpring_get_damper_mD1AAF428D25AFDF21E1CD5B349ACBE64D7C3A006_AdjustorThunk ();
// 0x00000008 System.Void UnityEngine.SoftJointLimitSpring::set_damper(System.Single)
extern void SoftJointLimitSpring_set_damper_m03BF5BE752A1620C6691D7DB663D05F0FFD55534_AdjustorThunk ();
// 0x00000009 System.Void UnityEngine.JointDrive::set_positionSpring(System.Single)
extern void JointDrive_set_positionSpring_mB6FF4730ECEB6B03BACB391B34381EB34B70A9F8_AdjustorThunk ();
// 0x0000000A System.Void UnityEngine.JointDrive::set_positionDamper(System.Single)
extern void JointDrive_set_positionDamper_mA07AE9A9384511BC160C19EF4CCB75260621C2CF_AdjustorThunk ();
// 0x0000000B System.Void UnityEngine.JointDrive::set_maximumForce(System.Single)
extern void JointDrive_set_maximumForce_m1D0AFF4DC2FB20093A1F29922550D88524CAE945_AdjustorThunk ();
// 0x0000000C System.Single UnityEngine.JointLimits::get_min()
extern void JointLimits_get_min_m7C30825582F94CECDD5DE097143F91B830103001_AdjustorThunk ();
// 0x0000000D System.Single UnityEngine.JointLimits::get_max()
extern void JointLimits_get_max_mFC3FC1E95BCBEA96DDC48D9B94BC8F3AE587CD2F_AdjustorThunk ();
// 0x0000000E System.Single UnityEngine.JointLimits::get_bounciness()
extern void JointLimits_get_bounciness_m29ABD9EAE433CAA892D823A26EE8C9423E4CDBB1_AdjustorThunk ();
// 0x0000000F UnityEngine.Rigidbody UnityEngine.Collision::get_rigidbody()
extern void Collision_get_rigidbody_m0D6A338F7DA1C6B62711034BBAF2444895D517C3 ();
// 0x00000010 UnityEngine.Collider UnityEngine.Collision::get_collider()
extern void Collision_get_collider_m52F32CFE0BC1925C72A5B8EB743BBEF628201352 ();
// 0x00000011 UnityEngine.GameObject UnityEngine.Collision::get_gameObject()
extern void Collision_get_gameObject_m9A7069ABE50D4BB957A8ED76E5F4A59ACEC57C49 ();
// 0x00000012 UnityEngine.ContactPoint[] UnityEngine.Collision::get_contacts()
extern void Collision_get_contacts_m3807F7784D655257D7153CB615EF1FF7FAEAE0CF ();
// 0x00000013 UnityEngine.Vector3 UnityEngine.Collision::get_impulse()
extern void Collision_get_impulse_mF9B3802CE7856CCE130B85BC383AE79674AE7432 ();
// 0x00000014 System.Void UnityEngine.PhysicMaterial::.ctor()
extern void PhysicMaterial__ctor_m1E6AB8A9A487E70DE44AF221946176BE96584DFC ();
// 0x00000015 System.Void UnityEngine.PhysicMaterial::Internal_CreateDynamicsMaterial(UnityEngine.PhysicMaterial,System.String)
extern void PhysicMaterial_Internal_CreateDynamicsMaterial_m477978C8F91BAC9E41E0BE575C0C0730C80AFE1C ();
// 0x00000016 System.Void UnityEngine.PhysicMaterial::set_bounciness(System.Single)
extern void PhysicMaterial_set_bounciness_m452DFF64A89EC0F7C1BE978BB99A6FCCEBFCC35B ();
// 0x00000017 System.Void UnityEngine.PhysicMaterial::set_dynamicFriction(System.Single)
extern void PhysicMaterial_set_dynamicFriction_m9FCE6D1715B7385FE3FB427C13C77643BECF635E ();
// 0x00000018 System.Void UnityEngine.PhysicMaterial::set_staticFriction(System.Single)
extern void PhysicMaterial_set_staticFriction_m6DC930416F9C22EBCA4661D58F3E369B4E779D3A ();
// 0x00000019 System.Void UnityEngine.PhysicMaterial::set_frictionCombine(UnityEngine.PhysicMaterialCombine)
extern void PhysicMaterial_set_frictionCombine_mB05C3A5D9A430E1BECADC0530C67C604E2F11696 ();
// 0x0000001A System.Void UnityEngine.PhysicMaterial::set_bounceCombine(UnityEngine.PhysicMaterialCombine)
extern void PhysicMaterial_set_bounceCombine_mE4B35BC7940DF0CC1C4D160F4676768914DE032F ();
// 0x0000001B UnityEngine.Collider UnityEngine.RaycastHit::get_collider()
extern void RaycastHit_get_collider_mE70B84C4312B567344F60992A6067855F2C3A7A9_AdjustorThunk ();
// 0x0000001C UnityEngine.Vector3 UnityEngine.RaycastHit::get_point()
extern void RaycastHit_get_point_m0E564B2A72C7A744B889AE9D596F3EFA55059001_AdjustorThunk ();
// 0x0000001D System.Void UnityEngine.RaycastHit::set_point(UnityEngine.Vector3)
extern void RaycastHit_set_point_mCB652835DA3A0AED8A8574B4A47FD9BF8F9A4191_AdjustorThunk ();
// 0x0000001E UnityEngine.Vector3 UnityEngine.RaycastHit::get_normal()
extern void RaycastHit_get_normal_mF736A6D09D98D63AB7E5BF10F38AEBFC177A1D94_AdjustorThunk ();
// 0x0000001F System.Void UnityEngine.RaycastHit::set_normal(UnityEngine.Vector3)
extern void RaycastHit_set_normal_mB21B55B92CE45F93EB140793D31DFE69693F7303_AdjustorThunk ();
// 0x00000020 System.Single UnityEngine.RaycastHit::get_distance()
extern void RaycastHit_get_distance_m1CBA60855C35F29BBC348D374BBC76386A243543_AdjustorThunk ();
// 0x00000021 UnityEngine.Rigidbody UnityEngine.RaycastHit::get_rigidbody()
extern void RaycastHit_get_rigidbody_m8E28BDE09DC588AAF0C15182AFF3C00EE11EB0FC_AdjustorThunk ();
// 0x00000022 UnityEngine.Vector3 UnityEngine.Rigidbody::get_velocity()
extern void Rigidbody_get_velocity_m584A6D79C3657C21AE9CAA56BEE05582B8D5A2B8 ();
// 0x00000023 System.Void UnityEngine.Rigidbody::set_velocity(UnityEngine.Vector3)
extern void Rigidbody_set_velocity_m8D129E88E62AD02AB81CFC8BE694C4A5A2B2B380 ();
// 0x00000024 UnityEngine.Vector3 UnityEngine.Rigidbody::get_angularVelocity()
extern void Rigidbody_get_angularVelocity_mA5D414D6E27755C944485A750F974BEA24CF27F0 ();
// 0x00000025 System.Void UnityEngine.Rigidbody::set_angularVelocity(UnityEngine.Vector3)
extern void Rigidbody_set_angularVelocity_m1839DCBC87B01EFD0B4936E84E503E38774B962C ();
// 0x00000026 System.Single UnityEngine.Rigidbody::get_drag()
extern void Rigidbody_get_drag_m2B304BB4C4A1A0E349C8B57C9085C0BC66DDE28E ();
// 0x00000027 System.Single UnityEngine.Rigidbody::get_angularDrag()
extern void Rigidbody_get_angularDrag_mD6C855353D256A0B08285433ACEA090A8460C14B ();
// 0x00000028 System.Single UnityEngine.Rigidbody::get_mass()
extern void Rigidbody_get_mass_m36189AE2961EE2C537D9CF5EC5881FD64CE43EB1 ();
// 0x00000029 System.Void UnityEngine.Rigidbody::set_mass(System.Single)
extern void Rigidbody_set_mass_mA8946A1A06B07CE6DFF2F1A9081A2E2AA406FDC9 ();
// 0x0000002A System.Boolean UnityEngine.Rigidbody::get_useGravity()
extern void Rigidbody_get_useGravity_m802E0C0B4F2C2B521D5369EA027325157A53FCAA ();
// 0x0000002B System.Void UnityEngine.Rigidbody::set_useGravity(System.Boolean)
extern void Rigidbody_set_useGravity_mB0D957A9D8A9819E18D2E81F465C5C0B60CBC6DA ();
// 0x0000002C System.Boolean UnityEngine.Rigidbody::get_isKinematic()
extern void Rigidbody_get_isKinematic_mCF624F7C1C78267224EFBEAF9B4FD72CDE56CEB2 ();
// 0x0000002D System.Void UnityEngine.Rigidbody::set_isKinematic(System.Boolean)
extern void Rigidbody_set_isKinematic_m856AB59E5A6207892C439AFC8DDF5620B941E71B ();
// 0x0000002E System.Void UnityEngine.Rigidbody::set_freezeRotation(System.Boolean)
extern void Rigidbody_set_freezeRotation_m946DD8BB0A2AD6F8C5CA22F506628168A3767D13 ();
// 0x0000002F System.Void UnityEngine.Rigidbody::set_constraints(UnityEngine.RigidbodyConstraints)
extern void Rigidbody_set_constraints_m6E6AACB03165E54952E7CFE13C07188205A7061F ();
// 0x00000030 UnityEngine.Vector3 UnityEngine.Rigidbody::get_centerOfMass()
extern void Rigidbody_get_centerOfMass_m616EAA0BFA8FB7413D6FCB18AD937009DC0929F6 ();
// 0x00000031 System.Void UnityEngine.Rigidbody::set_centerOfMass(UnityEngine.Vector3)
extern void Rigidbody_set_centerOfMass_m038CD9CF36C48D04C92AFCF15AB001789AD699D4 ();
// 0x00000032 UnityEngine.Vector3 UnityEngine.Rigidbody::get_worldCenterOfMass()
extern void Rigidbody_get_worldCenterOfMass_m4604E98FB98A4515E9F9AD775AB56E2BB98D6F2A ();
// 0x00000033 UnityEngine.Vector3 UnityEngine.Rigidbody::get_inertiaTensor()
extern void Rigidbody_get_inertiaTensor_m27F5982AB5B321AFA501C8453963A9EE8190C578 ();
// 0x00000034 System.Void UnityEngine.Rigidbody::set_inertiaTensor(UnityEngine.Vector3)
extern void Rigidbody_set_inertiaTensor_mEE7AF73CC252F44FD2693D22FE6AD8B8ED15D146 ();
// 0x00000035 System.Void UnityEngine.Rigidbody::set_detectCollisions(System.Boolean)
extern void Rigidbody_set_detectCollisions_mD6BE24EC78D298D6F737822E3A63123AF586045F ();
// 0x00000036 UnityEngine.Vector3 UnityEngine.Rigidbody::get_position()
extern void Rigidbody_get_position_m478D060638E43DE3AE9C931A42593484B8310113 ();
// 0x00000037 System.Void UnityEngine.Rigidbody::set_position(UnityEngine.Vector3)
extern void Rigidbody_set_position_m54EED7F2D5EC9D34937D94B671BD6DE356DD0E7F ();
// 0x00000038 UnityEngine.Quaternion UnityEngine.Rigidbody::get_rotation()
extern void Rigidbody_get_rotation_mD967DD98F16F80C0D74F8F1C25953D0609906BE5 ();
// 0x00000039 System.Void UnityEngine.Rigidbody::set_rotation(UnityEngine.Quaternion)
extern void Rigidbody_set_rotation_mFC6AD10748F2A0E04B6D2DBADEC168D60F90345B ();
// 0x0000003A UnityEngine.RigidbodyInterpolation UnityEngine.Rigidbody::get_interpolation()
extern void Rigidbody_get_interpolation_mCFC127818CB91952344873D5C0B089A61FAE5C95 ();
// 0x0000003B System.Void UnityEngine.Rigidbody::set_interpolation(UnityEngine.RigidbodyInterpolation)
extern void Rigidbody_set_interpolation_m34A9426028D5360BD8F8FBBB6A1DCB00DE8D1540 ();
// 0x0000003C System.Void UnityEngine.Rigidbody::set_solverIterations(System.Int32)
extern void Rigidbody_set_solverIterations_mF597BD259445EF2E8F5A5FA7566CA04B8D4946E7 ();
// 0x0000003D System.Void UnityEngine.Rigidbody::set_maxAngularVelocity(System.Single)
extern void Rigidbody_set_maxAngularVelocity_m0EF6E6142D8F4484B436019609F21637BB2E1142 ();
// 0x0000003E System.Void UnityEngine.Rigidbody::MovePosition(UnityEngine.Vector3)
extern void Rigidbody_MovePosition_m5807AA5CDEC1B8350618166B2DF56FCAAAFFF7C1 ();
// 0x0000003F System.Void UnityEngine.Rigidbody::MoveRotation(UnityEngine.Quaternion)
extern void Rigidbody_MoveRotation_mCBE2CF9F1B4A86C4BCB899AAB4C4EE8BBAD21C84 ();
// 0x00000040 System.Void UnityEngine.Rigidbody::Sleep()
extern void Rigidbody_Sleep_mFE36499136A22ED98FC53B9EC716B380D7A8AD7E ();
// 0x00000041 System.Boolean UnityEngine.Rigidbody::IsSleeping()
extern void Rigidbody_IsSleeping_m1B8DD08C5F9B360D3CA5C60500472FC17CA2DA99 ();
// 0x00000042 System.Void UnityEngine.Rigidbody::WakeUp()
extern void Rigidbody_WakeUp_m30E4CCC2A1A5829084881EBF52431ADDA4F2B336 ();
// 0x00000043 UnityEngine.Vector3 UnityEngine.Rigidbody::GetPointVelocity(UnityEngine.Vector3)
extern void Rigidbody_GetPointVelocity_mFA72C6F1FF5AEC64B25B0F7F7682BB9666D28AC4 ();
// 0x00000044 System.Void UnityEngine.Rigidbody::AddForce(UnityEngine.Vector3,UnityEngine.ForceMode)
extern void Rigidbody_AddForce_mD64ACF772614FE36CFD8A477A07A407B35DF1A54 ();
// 0x00000045 System.Void UnityEngine.Rigidbody::AddForce(UnityEngine.Vector3)
extern void Rigidbody_AddForce_mC8140D90B806634A733624F671C45AD7CDBEDB38 ();
// 0x00000046 System.Void UnityEngine.Rigidbody::AddTorque(UnityEngine.Vector3,UnityEngine.ForceMode)
extern void Rigidbody_AddTorque_m18EA054D294E8A26F6485C1E933E986BD5BE5F60 ();
// 0x00000047 System.Void UnityEngine.Rigidbody::AddTorque(UnityEngine.Vector3)
extern void Rigidbody_AddTorque_mB9B6AE5C84CCB626CDDAAA5307F31BC0D9589EE0 ();
// 0x00000048 System.Void UnityEngine.Rigidbody::AddForceAtPosition(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.ForceMode)
extern void Rigidbody_AddForceAtPosition_mF3B282490DFBD57EEA2A67AA44F602368A20D769 ();
// 0x00000049 System.Void UnityEngine.Rigidbody::AddForceAtPosition(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Rigidbody_AddForceAtPosition_m3A5DCFC3E79923C9D8E32A54BC4AAA1E48EEAD6C ();
// 0x0000004A System.Void UnityEngine.Rigidbody::AddExplosionForce(System.Single,UnityEngine.Vector3,System.Single,System.Single,UnityEngine.ForceMode)
extern void Rigidbody_AddExplosionForce_m8A2C25CE73AC51E1C255929733A4187089BA689A ();
// 0x0000004B System.Void UnityEngine.Rigidbody::AddExplosionForce(System.Single,UnityEngine.Vector3,System.Single)
extern void Rigidbody_AddExplosionForce_m1FCC884218DB2A0DC0899C6FE4F0B131341C1243 ();
// 0x0000004C System.Void UnityEngine.Rigidbody::.ctor()
extern void Rigidbody__ctor_m7E9FF7B44B8A3B05217339C2C852A4BF620A12E4 ();
// 0x0000004D System.Void UnityEngine.Rigidbody::get_velocity_Injected(UnityEngine.Vector3&)
extern void Rigidbody_get_velocity_Injected_m61C1628D08B48C8971E476FCBB9323CB5EB73DAC ();
// 0x0000004E System.Void UnityEngine.Rigidbody::set_velocity_Injected(UnityEngine.Vector3&)
extern void Rigidbody_set_velocity_Injected_mA0CBA4077CADA05CF30404564E8D6260EF60E07D ();
// 0x0000004F System.Void UnityEngine.Rigidbody::get_angularVelocity_Injected(UnityEngine.Vector3&)
extern void Rigidbody_get_angularVelocity_Injected_mF130145BC134034C2ECB1C4E9BF276F0A63EC0D1 ();
// 0x00000050 System.Void UnityEngine.Rigidbody::set_angularVelocity_Injected(UnityEngine.Vector3&)
extern void Rigidbody_set_angularVelocity_Injected_mC5F63763F3701505D5F8E41CCE7F6325F1024783 ();
// 0x00000051 System.Void UnityEngine.Rigidbody::get_centerOfMass_Injected(UnityEngine.Vector3&)
extern void Rigidbody_get_centerOfMass_Injected_m15590C0C367EB322CF67A40CFE2A28A814A8766F ();
// 0x00000052 System.Void UnityEngine.Rigidbody::set_centerOfMass_Injected(UnityEngine.Vector3&)
extern void Rigidbody_set_centerOfMass_Injected_m999B2BDCDFCAC0D049AF7A7BE5BEB7A4B75D1D0A ();
// 0x00000053 System.Void UnityEngine.Rigidbody::get_worldCenterOfMass_Injected(UnityEngine.Vector3&)
extern void Rigidbody_get_worldCenterOfMass_Injected_mCF61FE2285D07E76A226249DA7C877F6888C945F ();
// 0x00000054 System.Void UnityEngine.Rigidbody::get_inertiaTensor_Injected(UnityEngine.Vector3&)
extern void Rigidbody_get_inertiaTensor_Injected_m87F89C6379EB9C47DDB03A4F5AE47CBFF9CFD2FF ();
// 0x00000055 System.Void UnityEngine.Rigidbody::set_inertiaTensor_Injected(UnityEngine.Vector3&)
extern void Rigidbody_set_inertiaTensor_Injected_mB43DBD3F1C979D04B0955484E0B14A971C7AE838 ();
// 0x00000056 System.Void UnityEngine.Rigidbody::get_position_Injected(UnityEngine.Vector3&)
extern void Rigidbody_get_position_Injected_m8B581634C88AB745341D4ACF1CC12544769EC019 ();
// 0x00000057 System.Void UnityEngine.Rigidbody::set_position_Injected(UnityEngine.Vector3&)
extern void Rigidbody_set_position_Injected_m3210674B4144CD8E599226F6343BC541A9975581 ();
// 0x00000058 System.Void UnityEngine.Rigidbody::get_rotation_Injected(UnityEngine.Quaternion&)
extern void Rigidbody_get_rotation_Injected_m5FEC1EC2B3CFCE6585C63CD64ECAE39828B73C91 ();
// 0x00000059 System.Void UnityEngine.Rigidbody::set_rotation_Injected(UnityEngine.Quaternion&)
extern void Rigidbody_set_rotation_Injected_m52738493BB4ACEDC29288DA8905907C1918A99C3 ();
// 0x0000005A System.Void UnityEngine.Rigidbody::MovePosition_Injected(UnityEngine.Vector3&)
extern void Rigidbody_MovePosition_Injected_mFD8BDE08912A4C3FC240A8834C3394A88349C440 ();
// 0x0000005B System.Void UnityEngine.Rigidbody::MoveRotation_Injected(UnityEngine.Quaternion&)
extern void Rigidbody_MoveRotation_Injected_m283E5FB82CB1721DC5220311967ED0E6ED831BF0 ();
// 0x0000005C System.Void UnityEngine.Rigidbody::GetPointVelocity_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void Rigidbody_GetPointVelocity_Injected_mFFC7165D5058AA5B62F08545554F78EE25FACC2E ();
// 0x0000005D System.Void UnityEngine.Rigidbody::AddForce_Injected(UnityEngine.Vector3&,UnityEngine.ForceMode)
extern void Rigidbody_AddForce_Injected_mEB8DB621D03BE740C5CBD472E873FF8B6EF6C119 ();
// 0x0000005E System.Void UnityEngine.Rigidbody::AddTorque_Injected(UnityEngine.Vector3&,UnityEngine.ForceMode)
extern void Rigidbody_AddTorque_Injected_mBD4EFD1DF707E92D237322E8A48B5827D3E8AD6B ();
// 0x0000005F System.Void UnityEngine.Rigidbody::AddForceAtPosition_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.ForceMode)
extern void Rigidbody_AddForceAtPosition_Injected_m5A653201E107E36E0E70F6A9933B8CE5F3F31E1F ();
// 0x00000060 System.Void UnityEngine.Rigidbody::AddExplosionForce_Injected(System.Single,UnityEngine.Vector3&,System.Single,System.Single,UnityEngine.ForceMode)
extern void Rigidbody_AddExplosionForce_Injected_m52842CCC1B482C3713F35132507C8F9652BC5D07 ();
// 0x00000061 System.Boolean UnityEngine.Collider::get_enabled()
extern void Collider_get_enabled_mED644D98C6AC2DF95BD86145E8D31AD7081C76EB ();
// 0x00000062 System.Void UnityEngine.Collider::set_enabled(System.Boolean)
extern void Collider_set_enabled_mF84DE8B0C8CAF33ACDB7F29BC055D9C8CFACB57B ();
// 0x00000063 UnityEngine.Rigidbody UnityEngine.Collider::get_attachedRigidbody()
extern void Collider_get_attachedRigidbody_m9E3C688EAE2F6A76C9AC14968D96769D9A71B1E8 ();
// 0x00000064 System.Boolean UnityEngine.Collider::get_isTrigger()
extern void Collider_get_isTrigger_m08B7B55C34B99492CE923444B5253A7812BC8D8E ();
// 0x00000065 System.Void UnityEngine.Collider::set_isTrigger(System.Boolean)
extern void Collider_set_isTrigger_mD9EB1E99EA96B08398D68188F2DEB2434C1890C5 ();
// 0x00000066 UnityEngine.Bounds UnityEngine.Collider::get_bounds()
extern void Collider_get_bounds_mD3CB68E38FB998406193A88D18C01F510272058A ();
// 0x00000067 System.Void UnityEngine.Collider::set_material(UnityEngine.PhysicMaterial)
extern void Collider_set_material_m2978E803DF20381466E0BD1F41F759DA015C5E74 ();
// 0x00000068 System.Void UnityEngine.Collider::.ctor()
extern void Collider__ctor_m4E6D2F06C3BDB9CB006BD94C294049209D7563ED ();
// 0x00000069 System.Void UnityEngine.Collider::get_bounds_Injected(UnityEngine.Bounds&)
extern void Collider_get_bounds_Injected_m90D0FE433CCD44CC83E1089055A5A17286F9B1E6 ();
// 0x0000006A System.Boolean UnityEngine.CharacterController::SimpleMove(UnityEngine.Vector3)
extern void CharacterController_SimpleMove_mE56BC226845D3BAACAA61BCA150D6723AC897489 ();
// 0x0000006B UnityEngine.CollisionFlags UnityEngine.CharacterController::Move(UnityEngine.Vector3)
extern void CharacterController_Move_m31D77B4E934015FE3D6CE04BF8017A1DD0487434 ();
// 0x0000006C UnityEngine.Vector3 UnityEngine.CharacterController::get_velocity()
extern void CharacterController_get_velocity_m735C4159CF165335A5C51E2B059952601F8CA0CD ();
// 0x0000006D System.Boolean UnityEngine.CharacterController::get_isGrounded()
extern void CharacterController_get_isGrounded_m5BB5A50DD5942ED9AEF9F3A97CF52913591E077B ();
// 0x0000006E System.Boolean UnityEngine.CharacterController::SimpleMove_Injected(UnityEngine.Vector3&)
extern void CharacterController_SimpleMove_Injected_mA94FFCC3D0D3C4D6650237B77BA4F09B43C81AC0 ();
// 0x0000006F UnityEngine.CollisionFlags UnityEngine.CharacterController::Move_Injected(UnityEngine.Vector3&)
extern void CharacterController_Move_Injected_m729AD52E288CE0BF810D548AF6F9309EEB56A551 ();
// 0x00000070 System.Void UnityEngine.CharacterController::get_velocity_Injected(UnityEngine.Vector3&)
extern void CharacterController_get_velocity_Injected_m0E49CB063AAA4E43CBBF4F422E5E4CAABAFCA3A8 ();
// 0x00000071 System.Void UnityEngine.MeshCollider::set_sharedMesh(UnityEngine.Mesh)
extern void MeshCollider_set_sharedMesh_m88BE136B396DA960BC78F1A563BE0A810AA0FBBA ();
// 0x00000072 System.Void UnityEngine.MeshCollider::set_convex(System.Boolean)
extern void MeshCollider_set_convex_m9437496D05ABFBE619F8B2C6E80D6AECB95EAFE0 ();
// 0x00000073 UnityEngine.Vector3 UnityEngine.CapsuleCollider::get_center()
extern void CapsuleCollider_get_center_m415B40B8ADB6B1C29F3EF4C23839D5514BBA18AE ();
// 0x00000074 System.Void UnityEngine.CapsuleCollider::set_center(UnityEngine.Vector3)
extern void CapsuleCollider_set_center_mD2A261C23EB2DE8B0D9F946596BF64B72F7015C6 ();
// 0x00000075 System.Single UnityEngine.CapsuleCollider::get_radius()
extern void CapsuleCollider_get_radius_m8E753A625226A5AF557AAFEBF5B6D0720C00802D ();
// 0x00000076 System.Void UnityEngine.CapsuleCollider::set_radius(System.Single)
extern void CapsuleCollider_set_radius_mFE5E4E8C0EC1ECAC76CFDDA3EF9713027872E50D ();
// 0x00000077 System.Single UnityEngine.CapsuleCollider::get_height()
extern void CapsuleCollider_get_height_mA0F14683CEDB4F32B59D0262AB7507574228EF75 ();
// 0x00000078 System.Void UnityEngine.CapsuleCollider::set_height(System.Single)
extern void CapsuleCollider_set_height_m77D7E2FFC2A2D587B30746E00690A375D3E3D0F6 ();
// 0x00000079 System.Int32 UnityEngine.CapsuleCollider::get_direction()
extern void CapsuleCollider_get_direction_m699EB45D6B2D713374B163F59AA4CB61B2BFA3DF ();
// 0x0000007A System.Void UnityEngine.CapsuleCollider::set_direction(System.Int32)
extern void CapsuleCollider_set_direction_mBF666730647E4C553A011BC503EEDD4F9117626C ();
// 0x0000007B System.Void UnityEngine.CapsuleCollider::.ctor()
extern void CapsuleCollider__ctor_m4334AB0C7D4CA7745D4492117175FB180DF164A8 ();
// 0x0000007C System.Void UnityEngine.CapsuleCollider::get_center_Injected(UnityEngine.Vector3&)
extern void CapsuleCollider_get_center_Injected_mCD228988D6221C1AAB16F9A46FA6D3EBF02D7BB2 ();
// 0x0000007D System.Void UnityEngine.CapsuleCollider::set_center_Injected(UnityEngine.Vector3&)
extern void CapsuleCollider_set_center_Injected_mE6B65C8E5FC795B8C3E1666FB675897F3139AFB3 ();
// 0x0000007E UnityEngine.Vector3 UnityEngine.BoxCollider::get_center()
extern void BoxCollider_get_center_mA9164B9949F419A35CC949685F1DC14588BC6402 ();
// 0x0000007F System.Void UnityEngine.BoxCollider::set_center(UnityEngine.Vector3)
extern void BoxCollider_set_center_m8A871056CA383C9932A7694FE396A1EFA247FC69 ();
// 0x00000080 UnityEngine.Vector3 UnityEngine.BoxCollider::get_size()
extern void BoxCollider_get_size_m1C7DA815D3BA9DDB3D92A58BEEFE2FCBA5206FE2 ();
// 0x00000081 System.Void UnityEngine.BoxCollider::set_size(UnityEngine.Vector3)
extern void BoxCollider_set_size_m65F9B4BD610D3094313EC8D1C5CE58D1D345A176 ();
// 0x00000082 System.Void UnityEngine.BoxCollider::get_center_Injected(UnityEngine.Vector3&)
extern void BoxCollider_get_center_Injected_m04983F947C5E02756596EC89923061041DB7D5D8 ();
// 0x00000083 System.Void UnityEngine.BoxCollider::set_center_Injected(UnityEngine.Vector3&)
extern void BoxCollider_set_center_Injected_m3D018F7FF1507924176F2B8672B1AB6FE945D5E4 ();
// 0x00000084 System.Void UnityEngine.BoxCollider::get_size_Injected(UnityEngine.Vector3&)
extern void BoxCollider_get_size_Injected_m0102C89526BAADB06950B4BCF339C1B155449AD1 ();
// 0x00000085 System.Void UnityEngine.BoxCollider::set_size_Injected(UnityEngine.Vector3&)
extern void BoxCollider_set_size_Injected_mC0F1AC95BA07EF05E218E1A8EB5F49E211318943 ();
// 0x00000086 UnityEngine.Vector3 UnityEngine.SphereCollider::get_center()
extern void SphereCollider_get_center_mE7AD1AC46974FF23EEA621B872E2962E52A1DB00 ();
// 0x00000087 System.Void UnityEngine.SphereCollider::set_center(UnityEngine.Vector3)
extern void SphereCollider_set_center_m325070F5252B4A2EA567B653CAE3285F101FA3EE ();
// 0x00000088 System.Single UnityEngine.SphereCollider::get_radius()
extern void SphereCollider_get_radius_m255804173C17314FD9538AE45C4A46D4882BC094 ();
// 0x00000089 System.Void UnityEngine.SphereCollider::get_center_Injected(UnityEngine.Vector3&)
extern void SphereCollider_get_center_Injected_mE8ECA23236D73D0685D76F15CB21BA1C09C9F6DB ();
// 0x0000008A System.Void UnityEngine.SphereCollider::set_center_Injected(UnityEngine.Vector3&)
extern void SphereCollider_set_center_Injected_mB5E101BA19F903AF24B7FAE2D125BBCC781FF82C ();
// 0x0000008B UnityEngine.Rigidbody UnityEngine.Joint::get_connectedBody()
extern void Joint_get_connectedBody_m6CAB9002D1AAC30C6D5A7B88660C7713D8C34A73 ();
// 0x0000008C System.Void UnityEngine.Joint::set_connectedBody(UnityEngine.Rigidbody)
extern void Joint_set_connectedBody_mB5843F21494B4468A218DA96238467710126F762 ();
// 0x0000008D UnityEngine.Vector3 UnityEngine.Joint::get_axis()
extern void Joint_get_axis_m54A1BFEC3A3594CFFCF0C21FBEC15B5ADA7CB702 ();
// 0x0000008E System.Void UnityEngine.Joint::set_axis(UnityEngine.Vector3)
extern void Joint_set_axis_mD77BFEA4D264E9365979261520BA5B83D86E26BA ();
// 0x0000008F UnityEngine.Vector3 UnityEngine.Joint::get_anchor()
extern void Joint_get_anchor_m50C893690CD5C0E85C9A7F9AB66685BB990EF662 ();
// 0x00000090 System.Void UnityEngine.Joint::set_anchor(UnityEngine.Vector3)
extern void Joint_set_anchor_m9584AAAB1BA704C95E4C5C2E4844E45E3ADCA9D3 ();
// 0x00000091 UnityEngine.Vector3 UnityEngine.Joint::get_connectedAnchor()
extern void Joint_get_connectedAnchor_mA5051D7B6BE0F109E3AE454E05CDE706FCB39D46 ();
// 0x00000092 System.Void UnityEngine.Joint::set_connectedAnchor(UnityEngine.Vector3)
extern void Joint_set_connectedAnchor_m17111B0CD83F1F68D2E2323DFD9E5A01A57D8059 ();
// 0x00000093 System.Boolean UnityEngine.Joint::get_autoConfigureConnectedAnchor()
extern void Joint_get_autoConfigureConnectedAnchor_m34DB3B2F73BE4179BD5AE0D08872587564D65177 ();
// 0x00000094 System.Void UnityEngine.Joint::set_autoConfigureConnectedAnchor(System.Boolean)
extern void Joint_set_autoConfigureConnectedAnchor_m1D576D32DC45DD451CD436AF28B30BA7116D4959 ();
// 0x00000095 System.Single UnityEngine.Joint::get_breakForce()
extern void Joint_get_breakForce_m4421DE1B772CE45DEC21435610EF0E4F09BAC681 ();
// 0x00000096 System.Void UnityEngine.Joint::set_breakForce(System.Single)
extern void Joint_set_breakForce_mDB7D58CE20412259C5717872ADC9A0B780857BF7 ();
// 0x00000097 System.Single UnityEngine.Joint::get_breakTorque()
extern void Joint_get_breakTorque_mD9D60ADFBBDF2F0671CAAFBA1FBE8C5C4C655FDC ();
// 0x00000098 System.Void UnityEngine.Joint::set_breakTorque(System.Single)
extern void Joint_set_breakTorque_m586C72D45EC3F17E43D6842118512FA403C72641 ();
// 0x00000099 System.Boolean UnityEngine.Joint::get_enableCollision()
extern void Joint_get_enableCollision_m1B8589A969D1601AE8C2947885E974160D6338B4 ();
// 0x0000009A System.Void UnityEngine.Joint::set_enableCollision(System.Boolean)
extern void Joint_set_enableCollision_mB91FAA42BBFD9A36AA8B6AA482A8E5AFC9E14CAB ();
// 0x0000009B System.Void UnityEngine.Joint::get_axis_Injected(UnityEngine.Vector3&)
extern void Joint_get_axis_Injected_m2F5466A10251C5F740DFBFA597FDBAEA02A2AE02 ();
// 0x0000009C System.Void UnityEngine.Joint::set_axis_Injected(UnityEngine.Vector3&)
extern void Joint_set_axis_Injected_m7BFCEEDE9581D1B5F646E31F6AD3FA3990E3A9E4 ();
// 0x0000009D System.Void UnityEngine.Joint::get_anchor_Injected(UnityEngine.Vector3&)
extern void Joint_get_anchor_Injected_mD0B6F04D2157DDD14E48278FAADA44AC5AF0433B ();
// 0x0000009E System.Void UnityEngine.Joint::set_anchor_Injected(UnityEngine.Vector3&)
extern void Joint_set_anchor_Injected_mCDEEE334517B97D137C1AE4ADAAFAD9CD85399EB ();
// 0x0000009F System.Void UnityEngine.Joint::get_connectedAnchor_Injected(UnityEngine.Vector3&)
extern void Joint_get_connectedAnchor_Injected_m8DBF0F41D88D496175BE8F21660DE20653E3CC9C ();
// 0x000000A0 System.Void UnityEngine.Joint::set_connectedAnchor_Injected(UnityEngine.Vector3&)
extern void Joint_set_connectedAnchor_Injected_m4CCEF375EFC641A7521AC470B6770282D17DEDED ();
// 0x000000A1 UnityEngine.JointLimits UnityEngine.HingeJoint::get_limits()
extern void HingeJoint_get_limits_m988B1E8056689C22248ABB05272559518C3E6990 ();
// 0x000000A2 UnityEngine.JointSpring UnityEngine.HingeJoint::get_spring()
extern void HingeJoint_get_spring_m0BFA4E9ED60DBBDC97F43B4FF44C1F7E59E99789 ();
// 0x000000A3 System.Boolean UnityEngine.HingeJoint::get_useMotor()
extern void HingeJoint_get_useMotor_m2E14DFC0BA78C44C43B14A4012CF83F28703617B ();
// 0x000000A4 System.Boolean UnityEngine.HingeJoint::get_useLimits()
extern void HingeJoint_get_useLimits_mFF9EB175970941DB444545729AE808CEAD50CB55 ();
// 0x000000A5 System.Boolean UnityEngine.HingeJoint::get_useSpring()
extern void HingeJoint_get_useSpring_mDA9FBD711920418719E71DE98F3CB3CE95BEB776 ();
// 0x000000A6 System.Void UnityEngine.HingeJoint::get_limits_Injected(UnityEngine.JointLimits&)
extern void HingeJoint_get_limits_Injected_mBC5408A9C2B0381572A34180B58D3C8B4405B161 ();
// 0x000000A7 System.Void UnityEngine.HingeJoint::get_spring_Injected(UnityEngine.JointSpring&)
extern void HingeJoint_get_spring_Injected_mC61A3CC33620B8F1DF1E5F20CF89ED3E5861EB9A ();
// 0x000000A8 System.Single UnityEngine.SpringJoint::get_spring()
extern void SpringJoint_get_spring_mAA4F0F503D65A513A2B437ED7EB4E0D24973A28B ();
// 0x000000A9 System.Single UnityEngine.SpringJoint::get_damper()
extern void SpringJoint_get_damper_m13C4C3A0902157FB4F5768C2F7B5DF0AFD3B5019 ();
// 0x000000AA System.Single UnityEngine.SpringJoint::get_maxDistance()
extern void SpringJoint_get_maxDistance_mBA26CEC9272132A773229CC1CC929D6DC5BC047E ();
// 0x000000AB UnityEngine.Vector3 UnityEngine.CharacterJoint::get_swingAxis()
extern void CharacterJoint_get_swingAxis_m3D84F5C03ADEC6D798CD2F7D610CD4BED92B316F ();
// 0x000000AC System.Void UnityEngine.CharacterJoint::set_swingAxis(UnityEngine.Vector3)
extern void CharacterJoint_set_swingAxis_mE1A37DAAA9E81D2AC53717C3D14188EB60E2C401 ();
// 0x000000AD UnityEngine.SoftJointLimitSpring UnityEngine.CharacterJoint::get_twistLimitSpring()
extern void CharacterJoint_get_twistLimitSpring_m8D739C967A41819B9BED9E7175113ADDD333FB53 ();
// 0x000000AE UnityEngine.SoftJointLimitSpring UnityEngine.CharacterJoint::get_swingLimitSpring()
extern void CharacterJoint_get_swingLimitSpring_mDC584B04AFAE8E93EB9978AA4C2ED2F9DEC0F315 ();
// 0x000000AF UnityEngine.SoftJointLimit UnityEngine.CharacterJoint::get_lowTwistLimit()
extern void CharacterJoint_get_lowTwistLimit_mF14777D3BEE16FD317EA79D674B69686EB600579 ();
// 0x000000B0 System.Void UnityEngine.CharacterJoint::set_lowTwistLimit(UnityEngine.SoftJointLimit)
extern void CharacterJoint_set_lowTwistLimit_m3354D75D6F1A2703CDB94A0169D797FD374DA5CD ();
// 0x000000B1 UnityEngine.SoftJointLimit UnityEngine.CharacterJoint::get_highTwistLimit()
extern void CharacterJoint_get_highTwistLimit_mEAAFB8F58969DA1AE9C5FDB44012A990BB213A53 ();
// 0x000000B2 System.Void UnityEngine.CharacterJoint::set_highTwistLimit(UnityEngine.SoftJointLimit)
extern void CharacterJoint_set_highTwistLimit_mCB094A57AFBD1833966D8CCB3A4555E091A39D02 ();
// 0x000000B3 UnityEngine.SoftJointLimit UnityEngine.CharacterJoint::get_swing1Limit()
extern void CharacterJoint_get_swing1Limit_m0C3A7166C4AEE153CB663CF62F90E2E545B733AE ();
// 0x000000B4 System.Void UnityEngine.CharacterJoint::set_swing1Limit(UnityEngine.SoftJointLimit)
extern void CharacterJoint_set_swing1Limit_m238C86F635049095559A92BD4F3E32A152D14B71 ();
// 0x000000B5 UnityEngine.SoftJointLimit UnityEngine.CharacterJoint::get_swing2Limit()
extern void CharacterJoint_get_swing2Limit_mE128EB6EA89A3C0791EF032665FF6213CE574397 ();
// 0x000000B6 System.Void UnityEngine.CharacterJoint::set_swing2Limit(UnityEngine.SoftJointLimit)
extern void CharacterJoint_set_swing2Limit_m671879988AE029D2171C6F631B9D35E69C47DF43 ();
// 0x000000B7 System.Boolean UnityEngine.CharacterJoint::get_enableProjection()
extern void CharacterJoint_get_enableProjection_mCFE8DE15EBE057ED6FB50DCA4CBE7D15FB6C713C ();
// 0x000000B8 System.Single UnityEngine.CharacterJoint::get_projectionDistance()
extern void CharacterJoint_get_projectionDistance_m7FF0A9BA90DB6968D003A2A43E72E4A9C2319193 ();
// 0x000000B9 System.Single UnityEngine.CharacterJoint::get_projectionAngle()
extern void CharacterJoint_get_projectionAngle_mB959ED98AFFF713E0298C2C543A72EEEE97F25C0 ();
// 0x000000BA System.Void UnityEngine.CharacterJoint::get_swingAxis_Injected(UnityEngine.Vector3&)
extern void CharacterJoint_get_swingAxis_Injected_mB19190386E2A95FB348A8FC533259C03A518EA3D ();
// 0x000000BB System.Void UnityEngine.CharacterJoint::set_swingAxis_Injected(UnityEngine.Vector3&)
extern void CharacterJoint_set_swingAxis_Injected_mB47A75071514311FB1BB8ED3C18818D94F8A9B6F ();
// 0x000000BC System.Void UnityEngine.CharacterJoint::get_twistLimitSpring_Injected(UnityEngine.SoftJointLimitSpring&)
extern void CharacterJoint_get_twistLimitSpring_Injected_m5B4773EFAA80FE677F65BBE10FE24418A8EBDD5D ();
// 0x000000BD System.Void UnityEngine.CharacterJoint::get_swingLimitSpring_Injected(UnityEngine.SoftJointLimitSpring&)
extern void CharacterJoint_get_swingLimitSpring_Injected_mBE8094BE61445AFF3E08423076C0A91FBF9DED61 ();
// 0x000000BE System.Void UnityEngine.CharacterJoint::get_lowTwistLimit_Injected(UnityEngine.SoftJointLimit&)
extern void CharacterJoint_get_lowTwistLimit_Injected_m0C15C16C4B5D49128F3117A1DB8DAC4978B170BA ();
// 0x000000BF System.Void UnityEngine.CharacterJoint::set_lowTwistLimit_Injected(UnityEngine.SoftJointLimit&)
extern void CharacterJoint_set_lowTwistLimit_Injected_m99CF3E0755509747DEC5F57DE152DE525B76D3A5 ();
// 0x000000C0 System.Void UnityEngine.CharacterJoint::get_highTwistLimit_Injected(UnityEngine.SoftJointLimit&)
extern void CharacterJoint_get_highTwistLimit_Injected_mAEAFE737AF7A19AA5E52960A3D4FB4045EEA32AB ();
// 0x000000C1 System.Void UnityEngine.CharacterJoint::set_highTwistLimit_Injected(UnityEngine.SoftJointLimit&)
extern void CharacterJoint_set_highTwistLimit_Injected_m23184846DE1A86DC30D073767383B10F01AA5FD6 ();
// 0x000000C2 System.Void UnityEngine.CharacterJoint::get_swing1Limit_Injected(UnityEngine.SoftJointLimit&)
extern void CharacterJoint_get_swing1Limit_Injected_m38AEB2810965D21D04B007636ADAA87BA71C24EF ();
// 0x000000C3 System.Void UnityEngine.CharacterJoint::set_swing1Limit_Injected(UnityEngine.SoftJointLimit&)
extern void CharacterJoint_set_swing1Limit_Injected_m9E7B47F5A65D8C69FFC29820946652113AB2F8B0 ();
// 0x000000C4 System.Void UnityEngine.CharacterJoint::get_swing2Limit_Injected(UnityEngine.SoftJointLimit&)
extern void CharacterJoint_get_swing2Limit_Injected_m7C50B2E73B1C889969551CF2CF18EA349BD4B4E6 ();
// 0x000000C5 System.Void UnityEngine.CharacterJoint::set_swing2Limit_Injected(UnityEngine.SoftJointLimit&)
extern void CharacterJoint_set_swing2Limit_Injected_m9A8090BAAE2846A452B872448EE3179C660AF0F4 ();
// 0x000000C6 UnityEngine.Vector3 UnityEngine.ConfigurableJoint::get_secondaryAxis()
extern void ConfigurableJoint_get_secondaryAxis_mE9390C46782477183A7E1542AAEAA16F743C1139 ();
// 0x000000C7 System.Void UnityEngine.ConfigurableJoint::set_secondaryAxis(UnityEngine.Vector3)
extern void ConfigurableJoint_set_secondaryAxis_mC342ABE40AB6B5054B06ED4A8B712AAF79AB648A ();
// 0x000000C8 UnityEngine.ConfigurableJointMotion UnityEngine.ConfigurableJoint::get_xMotion()
extern void ConfigurableJoint_get_xMotion_mA0B0EB46F696B3F7AD21C8FCFE570F9204B843DC ();
// 0x000000C9 System.Void UnityEngine.ConfigurableJoint::set_xMotion(UnityEngine.ConfigurableJointMotion)
extern void ConfigurableJoint_set_xMotion_mC5CF8F90391EC7C38B5825FEDF17CA2DF9647310 ();
// 0x000000CA UnityEngine.ConfigurableJointMotion UnityEngine.ConfigurableJoint::get_yMotion()
extern void ConfigurableJoint_get_yMotion_m96FD055764DEC031E4028FEA73392AFC9B742AFD ();
// 0x000000CB System.Void UnityEngine.ConfigurableJoint::set_yMotion(UnityEngine.ConfigurableJointMotion)
extern void ConfigurableJoint_set_yMotion_mF331F91608B4578B02BE457098A5099B1D89D7D5 ();
// 0x000000CC UnityEngine.ConfigurableJointMotion UnityEngine.ConfigurableJoint::get_zMotion()
extern void ConfigurableJoint_get_zMotion_m6107BE4C3522026A26D89936A0E30B4124BCA7A9 ();
// 0x000000CD System.Void UnityEngine.ConfigurableJoint::set_zMotion(UnityEngine.ConfigurableJointMotion)
extern void ConfigurableJoint_set_zMotion_m9D2A9358472F9AC11A2FECDF9603BA79E004127A ();
// 0x000000CE UnityEngine.ConfigurableJointMotion UnityEngine.ConfigurableJoint::get_angularXMotion()
extern void ConfigurableJoint_get_angularXMotion_mCB43695BB1E2D10EB43CAC105C2F1E33418A5CE0 ();
// 0x000000CF System.Void UnityEngine.ConfigurableJoint::set_angularXMotion(UnityEngine.ConfigurableJointMotion)
extern void ConfigurableJoint_set_angularXMotion_mCC6D6951DC0D2BC40CE4AEA4F2DE224A39ED630E ();
// 0x000000D0 UnityEngine.ConfigurableJointMotion UnityEngine.ConfigurableJoint::get_angularYMotion()
extern void ConfigurableJoint_get_angularYMotion_mD22A8F18305F7ADCC9E17524CA8D46B289B127E8 ();
// 0x000000D1 System.Void UnityEngine.ConfigurableJoint::set_angularYMotion(UnityEngine.ConfigurableJointMotion)
extern void ConfigurableJoint_set_angularYMotion_m38E70B9932443F547BFE8C15F804ADA140BAB360 ();
// 0x000000D2 UnityEngine.ConfigurableJointMotion UnityEngine.ConfigurableJoint::get_angularZMotion()
extern void ConfigurableJoint_get_angularZMotion_m49D868A9CFE293E6A8D697ABDF952B613123FD28 ();
// 0x000000D3 System.Void UnityEngine.ConfigurableJoint::set_angularZMotion(UnityEngine.ConfigurableJointMotion)
extern void ConfigurableJoint_set_angularZMotion_mB4CAE9B73204E261174AECB3C45069FF2E0FBBDE ();
// 0x000000D4 System.Void UnityEngine.ConfigurableJoint::set_linearLimitSpring(UnityEngine.SoftJointLimitSpring)
extern void ConfigurableJoint_set_linearLimitSpring_m605DDD6622C8FBA039CC9B18C7747DB6F73EF14C ();
// 0x000000D5 System.Void UnityEngine.ConfigurableJoint::set_angularXLimitSpring(UnityEngine.SoftJointLimitSpring)
extern void ConfigurableJoint_set_angularXLimitSpring_m1EE955EED38E15A7E899F505A68DF34DFC3FAF60 ();
// 0x000000D6 System.Void UnityEngine.ConfigurableJoint::set_angularYZLimitSpring(UnityEngine.SoftJointLimitSpring)
extern void ConfigurableJoint_set_angularYZLimitSpring_m98FA9937FC9D0C05D54BD5A28CA56A324DB049ED ();
// 0x000000D7 System.Void UnityEngine.ConfigurableJoint::set_linearLimit(UnityEngine.SoftJointLimit)
extern void ConfigurableJoint_set_linearLimit_mF600A726E8D4E5A49DABE0756636AD77FF7F9406 ();
// 0x000000D8 System.Void UnityEngine.ConfigurableJoint::set_lowAngularXLimit(UnityEngine.SoftJointLimit)
extern void ConfigurableJoint_set_lowAngularXLimit_mF03F0EFDBBE85A29FE3D787E5607118DD08B71EB ();
// 0x000000D9 System.Void UnityEngine.ConfigurableJoint::set_highAngularXLimit(UnityEngine.SoftJointLimit)
extern void ConfigurableJoint_set_highAngularXLimit_m9CF8EFEE498A2328943E19E15BF9A7C4539A5132 ();
// 0x000000DA System.Void UnityEngine.ConfigurableJoint::set_angularYLimit(UnityEngine.SoftJointLimit)
extern void ConfigurableJoint_set_angularYLimit_m39CD3BD8ACCBB7F685BCF44B973E700AD11A3EAA ();
// 0x000000DB System.Void UnityEngine.ConfigurableJoint::set_angularZLimit(UnityEngine.SoftJointLimit)
extern void ConfigurableJoint_set_angularZLimit_m3A4C51AD727F48FE28E4886A8EC60508420B1DCC ();
// 0x000000DC System.Void UnityEngine.ConfigurableJoint::set_targetRotation(UnityEngine.Quaternion)
extern void ConfigurableJoint_set_targetRotation_m77F193BD35FBF52926CF40165B6A9D776C9103B6 ();
// 0x000000DD System.Void UnityEngine.ConfigurableJoint::set_targetAngularVelocity(UnityEngine.Vector3)
extern void ConfigurableJoint_set_targetAngularVelocity_m39DA1316EFFDA5D6B04A90587C488A4099F01A0B ();
// 0x000000DE System.Void UnityEngine.ConfigurableJoint::set_rotationDriveMode(UnityEngine.RotationDriveMode)
extern void ConfigurableJoint_set_rotationDriveMode_m9815890EA96A93E0BBCB5BD20EC7779D2E2A514A ();
// 0x000000DF System.Void UnityEngine.ConfigurableJoint::set_slerpDrive(UnityEngine.JointDrive)
extern void ConfigurableJoint_set_slerpDrive_m341A1BFCB9DCF19F036A678E8864AC62F77B4B55 ();
// 0x000000E0 System.Void UnityEngine.ConfigurableJoint::set_projectionMode(UnityEngine.JointProjectionMode)
extern void ConfigurableJoint_set_projectionMode_m5A4660083BA0AB076BAE52626CC46FAFC06B8262 ();
// 0x000000E1 System.Void UnityEngine.ConfigurableJoint::set_projectionDistance(System.Single)
extern void ConfigurableJoint_set_projectionDistance_m1789C1F4F493FF46507491C06EFD77F516C9E7B1 ();
// 0x000000E2 System.Void UnityEngine.ConfigurableJoint::set_projectionAngle(System.Single)
extern void ConfigurableJoint_set_projectionAngle_mD6D0C5890F88043862395E3329021B9BBA55DC95 ();
// 0x000000E3 System.Void UnityEngine.ConfigurableJoint::set_configuredInWorldSpace(System.Boolean)
extern void ConfigurableJoint_set_configuredInWorldSpace_m23EA574084D35FA87640897F04EAC3236754339E ();
// 0x000000E4 System.Void UnityEngine.ConfigurableJoint::get_secondaryAxis_Injected(UnityEngine.Vector3&)
extern void ConfigurableJoint_get_secondaryAxis_Injected_m6D51BC27202AACE1FA1BA20FF59298C66D794540 ();
// 0x000000E5 System.Void UnityEngine.ConfigurableJoint::set_secondaryAxis_Injected(UnityEngine.Vector3&)
extern void ConfigurableJoint_set_secondaryAxis_Injected_m8D630F896751C83CA9B07173DE8A120936733A91 ();
// 0x000000E6 System.Void UnityEngine.ConfigurableJoint::set_linearLimitSpring_Injected(UnityEngine.SoftJointLimitSpring&)
extern void ConfigurableJoint_set_linearLimitSpring_Injected_mE90C49B513E7B3EA7ABFA95A3B8ED2A73730C5AB ();
// 0x000000E7 System.Void UnityEngine.ConfigurableJoint::set_angularXLimitSpring_Injected(UnityEngine.SoftJointLimitSpring&)
extern void ConfigurableJoint_set_angularXLimitSpring_Injected_m01FA0164ABAF03566B4158C6DB7A068DEDA038BE ();
// 0x000000E8 System.Void UnityEngine.ConfigurableJoint::set_angularYZLimitSpring_Injected(UnityEngine.SoftJointLimitSpring&)
extern void ConfigurableJoint_set_angularYZLimitSpring_Injected_mC5AF9A234E7E867197988D78BFA271C8F0AE0B2C ();
// 0x000000E9 System.Void UnityEngine.ConfigurableJoint::set_linearLimit_Injected(UnityEngine.SoftJointLimit&)
extern void ConfigurableJoint_set_linearLimit_Injected_m07AFBF1C2812A55724A0D8877E7BC47DE4AD6236 ();
// 0x000000EA System.Void UnityEngine.ConfigurableJoint::set_lowAngularXLimit_Injected(UnityEngine.SoftJointLimit&)
extern void ConfigurableJoint_set_lowAngularXLimit_Injected_m8363CB59C106D8D98E1CE44974A2935AA2722A88 ();
// 0x000000EB System.Void UnityEngine.ConfigurableJoint::set_highAngularXLimit_Injected(UnityEngine.SoftJointLimit&)
extern void ConfigurableJoint_set_highAngularXLimit_Injected_m46820CB6DD56738EE2E83A2603EFE8A993332F77 ();
// 0x000000EC System.Void UnityEngine.ConfigurableJoint::set_angularYLimit_Injected(UnityEngine.SoftJointLimit&)
extern void ConfigurableJoint_set_angularYLimit_Injected_m7FFFFAA6B3057DD3B48DFB0DC557C81046D3292A ();
// 0x000000ED System.Void UnityEngine.ConfigurableJoint::set_angularZLimit_Injected(UnityEngine.SoftJointLimit&)
extern void ConfigurableJoint_set_angularZLimit_Injected_m1F8CA881EFF4CB5C14F56296F33F55446CE95ECA ();
// 0x000000EE System.Void UnityEngine.ConfigurableJoint::set_targetRotation_Injected(UnityEngine.Quaternion&)
extern void ConfigurableJoint_set_targetRotation_Injected_m9992253D130FFAB7F74C360A6874A30329799B38 ();
// 0x000000EF System.Void UnityEngine.ConfigurableJoint::set_targetAngularVelocity_Injected(UnityEngine.Vector3&)
extern void ConfigurableJoint_set_targetAngularVelocity_Injected_m400680616F59B0EA2BAB6B96DBFA28BF731DBFBF ();
// 0x000000F0 System.Void UnityEngine.ConfigurableJoint::set_slerpDrive_Injected(UnityEngine.JointDrive&)
extern void ConfigurableJoint_set_slerpDrive_Injected_mDFDC04EFC76F28B3EEE803CCFCAD620DDC06D1B6 ();
// 0x000000F1 UnityEngine.Vector3 UnityEngine.ContactPoint::get_point()
extern void ContactPoint_get_point_mB222EB009D8B3DBE5F248088167F074551B424F7_AdjustorThunk ();
// 0x000000F2 UnityEngine.Vector3 UnityEngine.ContactPoint::get_normal()
extern void ContactPoint_get_normal_m0C47576D775F12C2BED5B25ACC720180008EDA3E_AdjustorThunk ();
// 0x000000F3 System.String UnityEngine.PhysicsScene::ToString()
extern void PhysicsScene_ToString_m192392A2C486E2F76AAC736C519CB117BD187BE9_AdjustorThunk ();
// 0x000000F4 System.Int32 UnityEngine.PhysicsScene::GetHashCode()
extern void PhysicsScene_GetHashCode_m8B6554AA89D7DFD183573EA89E5ACF6B21E08654_AdjustorThunk ();
// 0x000000F5 System.Boolean UnityEngine.PhysicsScene::Equals(System.Object)
extern void PhysicsScene_Equals_m538C6A86CCC4EF0546590BAC51E5D08ACC2CBEF9_AdjustorThunk ();
// 0x000000F6 System.Boolean UnityEngine.PhysicsScene::Equals(UnityEngine.PhysicsScene)
extern void PhysicsScene_Equals_m25CC71614BEABB76B6FB618516B8807DAF8CDDF8_AdjustorThunk ();
// 0x000000F7 System.Boolean UnityEngine.PhysicsScene::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Raycast_mA90972A8E828722A26EBC754EABB3086BE6E2336_AdjustorThunk ();
// 0x000000F8 System.Boolean UnityEngine.PhysicsScene::Internal_RaycastTest(UnityEngine.PhysicsScene,UnityEngine.Ray,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Internal_RaycastTest_m09DD752D166094AC2A6F19302F7DDB2EA88DE67F ();
// 0x000000F9 System.Boolean UnityEngine.PhysicsScene::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Raycast_m90D1AE1F641D9344BEBEF32E7AC0BAC883024C0B_AdjustorThunk ();
// 0x000000FA System.Boolean UnityEngine.PhysicsScene::Internal_Raycast(UnityEngine.PhysicsScene,UnityEngine.Ray,System.Single,UnityEngine.RaycastHit&,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Internal_Raycast_m2769B78FDCAA21C6F609DD709BADFF6F4B07C529 ();
// 0x000000FB System.Int32 UnityEngine.PhysicsScene::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit[],System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Raycast_mFB4A1BB42BB1D950E3854A08DFE3ECCDEA3BCCB1_AdjustorThunk ();
// 0x000000FC System.Int32 UnityEngine.PhysicsScene::Internal_RaycastNonAlloc(UnityEngine.PhysicsScene,UnityEngine.Ray,UnityEngine.RaycastHit[],System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Internal_RaycastNonAlloc_m9E5DAC724FC74D6B618C82BC8F19980E9636D5B1 ();
// 0x000000FD System.Boolean UnityEngine.PhysicsScene::Query_SphereCast(UnityEngine.PhysicsScene,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,System.Single,UnityEngine.RaycastHit&,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Query_SphereCast_m6DD7B632CF528C867859E329E2C4807C2CC891D8 ();
// 0x000000FE System.Boolean UnityEngine.PhysicsScene::Internal_SphereCast(UnityEngine.PhysicsScene,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Internal_SphereCast_mAD5109B24E5C668DC9A49E434B5139CA8A43DBD0 ();
// 0x000000FF System.Boolean UnityEngine.PhysicsScene::SphereCast(UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_SphereCast_m99D2D1C789BB817406E7C7FBAA75F2019A13D6C1_AdjustorThunk ();
// 0x00000100 System.Boolean UnityEngine.PhysicsScene::Internal_RaycastTest_Injected(UnityEngine.PhysicsScene&,UnityEngine.Ray&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Internal_RaycastTest_Injected_m544D0227B2312D7431A9351ADA5082B390B4ADDF ();
// 0x00000101 System.Boolean UnityEngine.PhysicsScene::Internal_Raycast_Injected(UnityEngine.PhysicsScene&,UnityEngine.Ray&,System.Single,UnityEngine.RaycastHit&,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Internal_Raycast_Injected_m039B99FEFE39873FF8B0F60F44DE817BF43D48DA ();
// 0x00000102 System.Int32 UnityEngine.PhysicsScene::Internal_RaycastNonAlloc_Injected(UnityEngine.PhysicsScene&,UnityEngine.Ray&,UnityEngine.RaycastHit[],System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Internal_RaycastNonAlloc_Injected_m4A5BC29FB0D0AAFC1DE827838CE144F882FE632E ();
// 0x00000103 System.Boolean UnityEngine.PhysicsScene::Query_SphereCast_Injected(UnityEngine.PhysicsScene&,UnityEngine.Vector3&,System.Single,UnityEngine.Vector3&,System.Single,UnityEngine.RaycastHit&,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Query_SphereCast_Injected_mA3953A644D1ED0B7D37A22E44635A413A4A1A12F ();
// 0x00000104 UnityEngine.Vector3 UnityEngine.Physics::get_gravity()
extern void Physics_get_gravity_mC0F44371C49C1DD1455D6ADEC824BAE35E1E104B ();
// 0x00000105 UnityEngine.PhysicsScene UnityEngine.Physics::get_defaultPhysicsScene()
extern void Physics_get_defaultPhysicsScene_mEA96CE906577479223C956BFF0B8D0B79BB47B30 ();
// 0x00000106 System.Void UnityEngine.Physics::IgnoreCollision(UnityEngine.Collider,UnityEngine.Collider,System.Boolean)
extern void Physics_IgnoreCollision_mF7183C0761289A45400F0E2C1B223B980EBE5F8B ();
// 0x00000107 System.Void UnityEngine.Physics::IgnoreLayerCollision(System.Int32,System.Int32,System.Boolean)
extern void Physics_IgnoreLayerCollision_m0DFDA0BBDFA1F0737524272504E3D85989989C32 ();
// 0x00000108 System.Void UnityEngine.Physics::IgnoreLayerCollision(System.Int32,System.Int32)
extern void Physics_IgnoreLayerCollision_m21F8A3A611C8DB8CA7D80C3D7E9DB836126C0C2B ();
// 0x00000109 System.Boolean UnityEngine.Physics::GetIgnoreLayerCollision(System.Int32,System.Int32)
extern void Physics_GetIgnoreLayerCollision_mD79111559F93397C8516436A7456086FB5021961 ();
// 0x0000010A System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_Raycast_m511ECD9EF3223645EF104DDA50BB81C07F29717C ();
// 0x0000010B System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32)
extern void Physics_Raycast_mC87F52EDC44BBBAFFED0D9DC92B37B11C6E07BEA ();
// 0x0000010C System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void Physics_Raycast_m0583CCAA9E2F3BD031F12FA080837E9A48EEC16D ();
// 0x0000010D System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Physics_Raycast_m6FFBDC7166A2129B70027FC392D06C735F81C73E ();
// 0x0000010E System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_Raycast_m44270C2D7C8CD056B6D78DE60A4C91FCC0A225C1 ();
// 0x0000010F System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32)
extern void Physics_Raycast_mFB0018A196CC9E45CA8A238899EA8B093264B024 ();
// 0x00000110 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single)
extern void Physics_Raycast_mBEC747ED0A7660BB12AA48B663CCBF7B1EE93D6B ();
// 0x00000111 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&)
extern void Physics_Raycast_m000FC36D038952F8AC5E3E562E7D39BAA8D1E287 ();
// 0x00000112 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_Raycast_mF33E3BA9FDFE0D19B193D2AB05028B2086738D47 ();
// 0x00000113 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,System.Single,System.Int32)
extern void Physics_Raycast_m5BA453EA32F5B660CD9A537FED97B995971820E1 ();
// 0x00000114 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,System.Single)
extern void Physics_Raycast_m198FD20B3D1BC1634AE30E80E0FD1491D55417E5 ();
// 0x00000115 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray)
extern void Physics_Raycast_mF1112EC53F3393455B4ABA0E0092A58E3CD0114A ();
// 0x00000116 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_Raycast_m6B6C7CB6DDED17F9B1905C606035A56FF9AE88F8 ();
// 0x00000117 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single,System.Int32)
extern void Physics_Raycast_m9F5CAD8DA7923E897C5DCAF913BD411AD1A87950 ();
// 0x00000118 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single)
extern void Physics_Raycast_mE1590EE4E2DC950A9FC2437E98EE8CD2EC2DEE67 ();
// 0x00000119 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&)
extern void Physics_Raycast_mCE618579F667A62D15CB74CEF76147136EB2D495 ();
// 0x0000011A System.Boolean UnityEngine.Physics::Linecast(UnityEngine.Vector3,UnityEngine.Vector3,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_Linecast_m2CAAB76F9786AB83FCF6B9C751B6A754AAEA48FB ();
// 0x0000011B System.Boolean UnityEngine.Physics::Linecast(UnityEngine.Vector3,UnityEngine.Vector3,System.Int32)
extern void Physics_Linecast_m1286F99BB2A490E80EA48A6DF456C81293A35C66 ();
// 0x0000011C System.Boolean UnityEngine.Physics::Linecast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_Linecast_mCAA3A0F713478346F006ADF57D65F64C89A544C6 ();
// 0x0000011D System.Boolean UnityEngine.Physics::Linecast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Int32)
extern void Physics_Linecast_mBB78AB8D049EB835949B2940D511E1049F6D5758 ();
// 0x0000011E System.Boolean UnityEngine.Physics::Linecast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&)
extern void Physics_Linecast_mB0BFB97C5093E1D1C08B5C3CDE1062D46CEB2318 ();
// 0x0000011F System.Boolean UnityEngine.Physics::SphereCast(UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_SphereCast_m2D57A1F4DA048B95DC33939367F2556CEA22B8BD ();
// 0x00000120 System.Boolean UnityEngine.Physics::SphereCast(UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32)
extern void Physics_SphereCast_m41B360D0B25BAADAD64F5D4BEAC07F92AF911837 ();
// 0x00000121 System.Boolean UnityEngine.Physics::SphereCast(UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single)
extern void Physics_SphereCast_m121F1B6EABA580E1661DE00D1974BD614D10C773 ();
// 0x00000122 System.Boolean UnityEngine.Physics::SphereCast(UnityEngine.Ray,System.Single,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_SphereCast_m6B6F941FB5632998A000262CDC9337E2B46E3871 ();
// 0x00000123 System.Boolean UnityEngine.Physics::SphereCast(UnityEngine.Ray,System.Single,UnityEngine.RaycastHit&,System.Single,System.Int32)
extern void Physics_SphereCast_mE831E516D318F8928FD9020BF7E9FEAD95574D68 ();
// 0x00000124 UnityEngine.RaycastHit[] UnityEngine.Physics::Internal_RaycastAll(UnityEngine.PhysicsScene,UnityEngine.Ray,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_Internal_RaycastAll_m366801D3E9D3DE8E9A94C5243D1FB2132DFDD851 ();
// 0x00000125 UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_RaycastAll_m6627435AE46B6720066FEEA5A80923420EE8FD2D ();
// 0x00000126 UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32)
extern void Physics_RaycastAll_m4FC397E4B7FEF7B5C9D172F95D6FB32CFC0F66FC ();
// 0x00000127 UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void Physics_RaycastAll_m93B94F31C64E8F64ACE18231FC85168D519B7F04 ();
// 0x00000128 UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Physics_RaycastAll_mE2ED29FCF8983C212F2628A8934CFAEBA4B683DE ();
// 0x00000129 UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_RaycastAll_mF5DB872CCEFE1710439DB9939EACE1DCDEFE2A7D ();
// 0x0000012A UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray,System.Single,System.Int32)
extern void Physics_RaycastAll_mE9ACFB603E337E2195E4B9419099E178F7FF82F4 ();
// 0x0000012B UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray,System.Single)
extern void Physics_RaycastAll_m6D5629FD1D3E3B0001F2819C1AE8660A3949C51F ();
// 0x0000012C UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray)
extern void Physics_RaycastAll_m37C7F423486CE3ACBAB4F1A326B9EBD7572B4056 ();
// 0x0000012D System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit[],System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_RaycastNonAlloc_m606B435CE959801C9B7D7111E49071E893C951DE ();
// 0x0000012E System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit[],System.Single,System.Int32)
extern void Physics_RaycastNonAlloc_mBFDB05B1AA90C5D587A1BEA6BEE66FEF2DF89894 ();
// 0x0000012F System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit[],System.Single)
extern void Physics_RaycastNonAlloc_m0A9CEF12FB7EE9C790609A15CBFE975725365CF9 ();
// 0x00000130 System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit[])
extern void Physics_RaycastNonAlloc_m9CC4B976EA1936076FFA1339BF6FBE78A2DA593F ();
// 0x00000131 System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit[],System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_RaycastNonAlloc_m120ADF6BD8DF57A40E6B395299B03FAEE348973F ();
// 0x00000132 System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit[],System.Single,System.Int32)
extern void Physics_RaycastNonAlloc_mA795C150FCE1F242C9806AAAFF04125594B56DDB ();
// 0x00000133 System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit[],System.Single)
extern void Physics_RaycastNonAlloc_m63644DC59B0554846D62ED3BE3B5517BC97D3316 ();
// 0x00000134 System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit[])
extern void Physics_RaycastNonAlloc_mD4393134F4470C9B319603A6CD3FF601F5981C88 ();
// 0x00000135 UnityEngine.Collider[] UnityEngine.Physics::OverlapSphere_Internal(UnityEngine.PhysicsScene,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_OverlapSphere_Internal_m10C15A0F86303EF92B205F16591E344E5728B6D0 ();
// 0x00000136 UnityEngine.Collider[] UnityEngine.Physics::OverlapSphere(UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_OverlapSphere_m73878904F621A28F8DD8855CB113C690CF1E89C0 ();
// 0x00000137 UnityEngine.Collider[] UnityEngine.Physics::OverlapSphere(UnityEngine.Vector3,System.Single)
extern void Physics_OverlapSphere_m354A92672F7A6DE59EF1285D02D62247F46A5D84 ();
// 0x00000138 System.Boolean UnityEngine.Physics::get_autoSimulation()
extern void Physics_get_autoSimulation_m7E21DC24254AD53C7CAD20414F86F6F8E5FE47EC ();
// 0x00000139 System.Void UnityEngine.Physics::get_gravity_Injected(UnityEngine.Vector3&)
extern void Physics_get_gravity_Injected_m7B5CE9CF65B245C2C2EA078BE636E393225B0335 ();
// 0x0000013A System.Void UnityEngine.Physics::get_defaultPhysicsScene_Injected(UnityEngine.PhysicsScene&)
extern void Physics_get_defaultPhysicsScene_Injected_mEAB07CA5307D0DDB331A31E6A6BA28CF83009262 ();
// 0x0000013B UnityEngine.RaycastHit[] UnityEngine.Physics::Internal_RaycastAll_Injected(UnityEngine.PhysicsScene&,UnityEngine.Ray&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_Internal_RaycastAll_Injected_m1FD369E67F19BB09F85967481F69EB24B5247913 ();
// 0x0000013C UnityEngine.Collider[] UnityEngine.Physics::OverlapSphere_Internal_Injected(UnityEngine.PhysicsScene&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_OverlapSphere_Internal_Injected_m315C4BB796FD73B41F5710D1DB52A6BCB57C344D ();
static Il2CppMethodPointer s_methodPointers[316] = 
{
	SoftJointLimit_get_limit_m40164161D58BA46F6F1EDA382FC552607BFC7D03_AdjustorThunk,
	SoftJointLimit_set_limit_m65386F3B7DE799354F233666054E36B4E244FE26_AdjustorThunk,
	SoftJointLimit_get_bounciness_m387D59B7712B1E70CFA337F9E311573C0D928846_AdjustorThunk,
	SoftJointLimit_set_bounciness_mC19E24774A8CF6C1229AE5D83AFE13F9F6D0938B_AdjustorThunk,
	SoftJointLimitSpring_get_spring_mC5D04E551048F33C874E4A7022E6FE582094AE21_AdjustorThunk,
	SoftJointLimitSpring_set_spring_mCC90918E4E61E1DBCB55653A3357B168B3281232_AdjustorThunk,
	SoftJointLimitSpring_get_damper_mD1AAF428D25AFDF21E1CD5B349ACBE64D7C3A006_AdjustorThunk,
	SoftJointLimitSpring_set_damper_m03BF5BE752A1620C6691D7DB663D05F0FFD55534_AdjustorThunk,
	JointDrive_set_positionSpring_mB6FF4730ECEB6B03BACB391B34381EB34B70A9F8_AdjustorThunk,
	JointDrive_set_positionDamper_mA07AE9A9384511BC160C19EF4CCB75260621C2CF_AdjustorThunk,
	JointDrive_set_maximumForce_m1D0AFF4DC2FB20093A1F29922550D88524CAE945_AdjustorThunk,
	JointLimits_get_min_m7C30825582F94CECDD5DE097143F91B830103001_AdjustorThunk,
	JointLimits_get_max_mFC3FC1E95BCBEA96DDC48D9B94BC8F3AE587CD2F_AdjustorThunk,
	JointLimits_get_bounciness_m29ABD9EAE433CAA892D823A26EE8C9423E4CDBB1_AdjustorThunk,
	Collision_get_rigidbody_m0D6A338F7DA1C6B62711034BBAF2444895D517C3,
	Collision_get_collider_m52F32CFE0BC1925C72A5B8EB743BBEF628201352,
	Collision_get_gameObject_m9A7069ABE50D4BB957A8ED76E5F4A59ACEC57C49,
	Collision_get_contacts_m3807F7784D655257D7153CB615EF1FF7FAEAE0CF,
	Collision_get_impulse_mF9B3802CE7856CCE130B85BC383AE79674AE7432,
	PhysicMaterial__ctor_m1E6AB8A9A487E70DE44AF221946176BE96584DFC,
	PhysicMaterial_Internal_CreateDynamicsMaterial_m477978C8F91BAC9E41E0BE575C0C0730C80AFE1C,
	PhysicMaterial_set_bounciness_m452DFF64A89EC0F7C1BE978BB99A6FCCEBFCC35B,
	PhysicMaterial_set_dynamicFriction_m9FCE6D1715B7385FE3FB427C13C77643BECF635E,
	PhysicMaterial_set_staticFriction_m6DC930416F9C22EBCA4661D58F3E369B4E779D3A,
	PhysicMaterial_set_frictionCombine_mB05C3A5D9A430E1BECADC0530C67C604E2F11696,
	PhysicMaterial_set_bounceCombine_mE4B35BC7940DF0CC1C4D160F4676768914DE032F,
	RaycastHit_get_collider_mE70B84C4312B567344F60992A6067855F2C3A7A9_AdjustorThunk,
	RaycastHit_get_point_m0E564B2A72C7A744B889AE9D596F3EFA55059001_AdjustorThunk,
	RaycastHit_set_point_mCB652835DA3A0AED8A8574B4A47FD9BF8F9A4191_AdjustorThunk,
	RaycastHit_get_normal_mF736A6D09D98D63AB7E5BF10F38AEBFC177A1D94_AdjustorThunk,
	RaycastHit_set_normal_mB21B55B92CE45F93EB140793D31DFE69693F7303_AdjustorThunk,
	RaycastHit_get_distance_m1CBA60855C35F29BBC348D374BBC76386A243543_AdjustorThunk,
	RaycastHit_get_rigidbody_m8E28BDE09DC588AAF0C15182AFF3C00EE11EB0FC_AdjustorThunk,
	Rigidbody_get_velocity_m584A6D79C3657C21AE9CAA56BEE05582B8D5A2B8,
	Rigidbody_set_velocity_m8D129E88E62AD02AB81CFC8BE694C4A5A2B2B380,
	Rigidbody_get_angularVelocity_mA5D414D6E27755C944485A750F974BEA24CF27F0,
	Rigidbody_set_angularVelocity_m1839DCBC87B01EFD0B4936E84E503E38774B962C,
	Rigidbody_get_drag_m2B304BB4C4A1A0E349C8B57C9085C0BC66DDE28E,
	Rigidbody_get_angularDrag_mD6C855353D256A0B08285433ACEA090A8460C14B,
	Rigidbody_get_mass_m36189AE2961EE2C537D9CF5EC5881FD64CE43EB1,
	Rigidbody_set_mass_mA8946A1A06B07CE6DFF2F1A9081A2E2AA406FDC9,
	Rigidbody_get_useGravity_m802E0C0B4F2C2B521D5369EA027325157A53FCAA,
	Rigidbody_set_useGravity_mB0D957A9D8A9819E18D2E81F465C5C0B60CBC6DA,
	Rigidbody_get_isKinematic_mCF624F7C1C78267224EFBEAF9B4FD72CDE56CEB2,
	Rigidbody_set_isKinematic_m856AB59E5A6207892C439AFC8DDF5620B941E71B,
	Rigidbody_set_freezeRotation_m946DD8BB0A2AD6F8C5CA22F506628168A3767D13,
	Rigidbody_set_constraints_m6E6AACB03165E54952E7CFE13C07188205A7061F,
	Rigidbody_get_centerOfMass_m616EAA0BFA8FB7413D6FCB18AD937009DC0929F6,
	Rigidbody_set_centerOfMass_m038CD9CF36C48D04C92AFCF15AB001789AD699D4,
	Rigidbody_get_worldCenterOfMass_m4604E98FB98A4515E9F9AD775AB56E2BB98D6F2A,
	Rigidbody_get_inertiaTensor_m27F5982AB5B321AFA501C8453963A9EE8190C578,
	Rigidbody_set_inertiaTensor_mEE7AF73CC252F44FD2693D22FE6AD8B8ED15D146,
	Rigidbody_set_detectCollisions_mD6BE24EC78D298D6F737822E3A63123AF586045F,
	Rigidbody_get_position_m478D060638E43DE3AE9C931A42593484B8310113,
	Rigidbody_set_position_m54EED7F2D5EC9D34937D94B671BD6DE356DD0E7F,
	Rigidbody_get_rotation_mD967DD98F16F80C0D74F8F1C25953D0609906BE5,
	Rigidbody_set_rotation_mFC6AD10748F2A0E04B6D2DBADEC168D60F90345B,
	Rigidbody_get_interpolation_mCFC127818CB91952344873D5C0B089A61FAE5C95,
	Rigidbody_set_interpolation_m34A9426028D5360BD8F8FBBB6A1DCB00DE8D1540,
	Rigidbody_set_solverIterations_mF597BD259445EF2E8F5A5FA7566CA04B8D4946E7,
	Rigidbody_set_maxAngularVelocity_m0EF6E6142D8F4484B436019609F21637BB2E1142,
	Rigidbody_MovePosition_m5807AA5CDEC1B8350618166B2DF56FCAAAFFF7C1,
	Rigidbody_MoveRotation_mCBE2CF9F1B4A86C4BCB899AAB4C4EE8BBAD21C84,
	Rigidbody_Sleep_mFE36499136A22ED98FC53B9EC716B380D7A8AD7E,
	Rigidbody_IsSleeping_m1B8DD08C5F9B360D3CA5C60500472FC17CA2DA99,
	Rigidbody_WakeUp_m30E4CCC2A1A5829084881EBF52431ADDA4F2B336,
	Rigidbody_GetPointVelocity_mFA72C6F1FF5AEC64B25B0F7F7682BB9666D28AC4,
	Rigidbody_AddForce_mD64ACF772614FE36CFD8A477A07A407B35DF1A54,
	Rigidbody_AddForce_mC8140D90B806634A733624F671C45AD7CDBEDB38,
	Rigidbody_AddTorque_m18EA054D294E8A26F6485C1E933E986BD5BE5F60,
	Rigidbody_AddTorque_mB9B6AE5C84CCB626CDDAAA5307F31BC0D9589EE0,
	Rigidbody_AddForceAtPosition_mF3B282490DFBD57EEA2A67AA44F602368A20D769,
	Rigidbody_AddForceAtPosition_m3A5DCFC3E79923C9D8E32A54BC4AAA1E48EEAD6C,
	Rigidbody_AddExplosionForce_m8A2C25CE73AC51E1C255929733A4187089BA689A,
	Rigidbody_AddExplosionForce_m1FCC884218DB2A0DC0899C6FE4F0B131341C1243,
	Rigidbody__ctor_m7E9FF7B44B8A3B05217339C2C852A4BF620A12E4,
	Rigidbody_get_velocity_Injected_m61C1628D08B48C8971E476FCBB9323CB5EB73DAC,
	Rigidbody_set_velocity_Injected_mA0CBA4077CADA05CF30404564E8D6260EF60E07D,
	Rigidbody_get_angularVelocity_Injected_mF130145BC134034C2ECB1C4E9BF276F0A63EC0D1,
	Rigidbody_set_angularVelocity_Injected_mC5F63763F3701505D5F8E41CCE7F6325F1024783,
	Rigidbody_get_centerOfMass_Injected_m15590C0C367EB322CF67A40CFE2A28A814A8766F,
	Rigidbody_set_centerOfMass_Injected_m999B2BDCDFCAC0D049AF7A7BE5BEB7A4B75D1D0A,
	Rigidbody_get_worldCenterOfMass_Injected_mCF61FE2285D07E76A226249DA7C877F6888C945F,
	Rigidbody_get_inertiaTensor_Injected_m87F89C6379EB9C47DDB03A4F5AE47CBFF9CFD2FF,
	Rigidbody_set_inertiaTensor_Injected_mB43DBD3F1C979D04B0955484E0B14A971C7AE838,
	Rigidbody_get_position_Injected_m8B581634C88AB745341D4ACF1CC12544769EC019,
	Rigidbody_set_position_Injected_m3210674B4144CD8E599226F6343BC541A9975581,
	Rigidbody_get_rotation_Injected_m5FEC1EC2B3CFCE6585C63CD64ECAE39828B73C91,
	Rigidbody_set_rotation_Injected_m52738493BB4ACEDC29288DA8905907C1918A99C3,
	Rigidbody_MovePosition_Injected_mFD8BDE08912A4C3FC240A8834C3394A88349C440,
	Rigidbody_MoveRotation_Injected_m283E5FB82CB1721DC5220311967ED0E6ED831BF0,
	Rigidbody_GetPointVelocity_Injected_mFFC7165D5058AA5B62F08545554F78EE25FACC2E,
	Rigidbody_AddForce_Injected_mEB8DB621D03BE740C5CBD472E873FF8B6EF6C119,
	Rigidbody_AddTorque_Injected_mBD4EFD1DF707E92D237322E8A48B5827D3E8AD6B,
	Rigidbody_AddForceAtPosition_Injected_m5A653201E107E36E0E70F6A9933B8CE5F3F31E1F,
	Rigidbody_AddExplosionForce_Injected_m52842CCC1B482C3713F35132507C8F9652BC5D07,
	Collider_get_enabled_mED644D98C6AC2DF95BD86145E8D31AD7081C76EB,
	Collider_set_enabled_mF84DE8B0C8CAF33ACDB7F29BC055D9C8CFACB57B,
	Collider_get_attachedRigidbody_m9E3C688EAE2F6A76C9AC14968D96769D9A71B1E8,
	Collider_get_isTrigger_m08B7B55C34B99492CE923444B5253A7812BC8D8E,
	Collider_set_isTrigger_mD9EB1E99EA96B08398D68188F2DEB2434C1890C5,
	Collider_get_bounds_mD3CB68E38FB998406193A88D18C01F510272058A,
	Collider_set_material_m2978E803DF20381466E0BD1F41F759DA015C5E74,
	Collider__ctor_m4E6D2F06C3BDB9CB006BD94C294049209D7563ED,
	Collider_get_bounds_Injected_m90D0FE433CCD44CC83E1089055A5A17286F9B1E6,
	CharacterController_SimpleMove_mE56BC226845D3BAACAA61BCA150D6723AC897489,
	CharacterController_Move_m31D77B4E934015FE3D6CE04BF8017A1DD0487434,
	CharacterController_get_velocity_m735C4159CF165335A5C51E2B059952601F8CA0CD,
	CharacterController_get_isGrounded_m5BB5A50DD5942ED9AEF9F3A97CF52913591E077B,
	CharacterController_SimpleMove_Injected_mA94FFCC3D0D3C4D6650237B77BA4F09B43C81AC0,
	CharacterController_Move_Injected_m729AD52E288CE0BF810D548AF6F9309EEB56A551,
	CharacterController_get_velocity_Injected_m0E49CB063AAA4E43CBBF4F422E5E4CAABAFCA3A8,
	MeshCollider_set_sharedMesh_m88BE136B396DA960BC78F1A563BE0A810AA0FBBA,
	MeshCollider_set_convex_m9437496D05ABFBE619F8B2C6E80D6AECB95EAFE0,
	CapsuleCollider_get_center_m415B40B8ADB6B1C29F3EF4C23839D5514BBA18AE,
	CapsuleCollider_set_center_mD2A261C23EB2DE8B0D9F946596BF64B72F7015C6,
	CapsuleCollider_get_radius_m8E753A625226A5AF557AAFEBF5B6D0720C00802D,
	CapsuleCollider_set_radius_mFE5E4E8C0EC1ECAC76CFDDA3EF9713027872E50D,
	CapsuleCollider_get_height_mA0F14683CEDB4F32B59D0262AB7507574228EF75,
	CapsuleCollider_set_height_m77D7E2FFC2A2D587B30746E00690A375D3E3D0F6,
	CapsuleCollider_get_direction_m699EB45D6B2D713374B163F59AA4CB61B2BFA3DF,
	CapsuleCollider_set_direction_mBF666730647E4C553A011BC503EEDD4F9117626C,
	CapsuleCollider__ctor_m4334AB0C7D4CA7745D4492117175FB180DF164A8,
	CapsuleCollider_get_center_Injected_mCD228988D6221C1AAB16F9A46FA6D3EBF02D7BB2,
	CapsuleCollider_set_center_Injected_mE6B65C8E5FC795B8C3E1666FB675897F3139AFB3,
	BoxCollider_get_center_mA9164B9949F419A35CC949685F1DC14588BC6402,
	BoxCollider_set_center_m8A871056CA383C9932A7694FE396A1EFA247FC69,
	BoxCollider_get_size_m1C7DA815D3BA9DDB3D92A58BEEFE2FCBA5206FE2,
	BoxCollider_set_size_m65F9B4BD610D3094313EC8D1C5CE58D1D345A176,
	BoxCollider_get_center_Injected_m04983F947C5E02756596EC89923061041DB7D5D8,
	BoxCollider_set_center_Injected_m3D018F7FF1507924176F2B8672B1AB6FE945D5E4,
	BoxCollider_get_size_Injected_m0102C89526BAADB06950B4BCF339C1B155449AD1,
	BoxCollider_set_size_Injected_mC0F1AC95BA07EF05E218E1A8EB5F49E211318943,
	SphereCollider_get_center_mE7AD1AC46974FF23EEA621B872E2962E52A1DB00,
	SphereCollider_set_center_m325070F5252B4A2EA567B653CAE3285F101FA3EE,
	SphereCollider_get_radius_m255804173C17314FD9538AE45C4A46D4882BC094,
	SphereCollider_get_center_Injected_mE8ECA23236D73D0685D76F15CB21BA1C09C9F6DB,
	SphereCollider_set_center_Injected_mB5E101BA19F903AF24B7FAE2D125BBCC781FF82C,
	Joint_get_connectedBody_m6CAB9002D1AAC30C6D5A7B88660C7713D8C34A73,
	Joint_set_connectedBody_mB5843F21494B4468A218DA96238467710126F762,
	Joint_get_axis_m54A1BFEC3A3594CFFCF0C21FBEC15B5ADA7CB702,
	Joint_set_axis_mD77BFEA4D264E9365979261520BA5B83D86E26BA,
	Joint_get_anchor_m50C893690CD5C0E85C9A7F9AB66685BB990EF662,
	Joint_set_anchor_m9584AAAB1BA704C95E4C5C2E4844E45E3ADCA9D3,
	Joint_get_connectedAnchor_mA5051D7B6BE0F109E3AE454E05CDE706FCB39D46,
	Joint_set_connectedAnchor_m17111B0CD83F1F68D2E2323DFD9E5A01A57D8059,
	Joint_get_autoConfigureConnectedAnchor_m34DB3B2F73BE4179BD5AE0D08872587564D65177,
	Joint_set_autoConfigureConnectedAnchor_m1D576D32DC45DD451CD436AF28B30BA7116D4959,
	Joint_get_breakForce_m4421DE1B772CE45DEC21435610EF0E4F09BAC681,
	Joint_set_breakForce_mDB7D58CE20412259C5717872ADC9A0B780857BF7,
	Joint_get_breakTorque_mD9D60ADFBBDF2F0671CAAFBA1FBE8C5C4C655FDC,
	Joint_set_breakTorque_m586C72D45EC3F17E43D6842118512FA403C72641,
	Joint_get_enableCollision_m1B8589A969D1601AE8C2947885E974160D6338B4,
	Joint_set_enableCollision_mB91FAA42BBFD9A36AA8B6AA482A8E5AFC9E14CAB,
	Joint_get_axis_Injected_m2F5466A10251C5F740DFBFA597FDBAEA02A2AE02,
	Joint_set_axis_Injected_m7BFCEEDE9581D1B5F646E31F6AD3FA3990E3A9E4,
	Joint_get_anchor_Injected_mD0B6F04D2157DDD14E48278FAADA44AC5AF0433B,
	Joint_set_anchor_Injected_mCDEEE334517B97D137C1AE4ADAAFAD9CD85399EB,
	Joint_get_connectedAnchor_Injected_m8DBF0F41D88D496175BE8F21660DE20653E3CC9C,
	Joint_set_connectedAnchor_Injected_m4CCEF375EFC641A7521AC470B6770282D17DEDED,
	HingeJoint_get_limits_m988B1E8056689C22248ABB05272559518C3E6990,
	HingeJoint_get_spring_m0BFA4E9ED60DBBDC97F43B4FF44C1F7E59E99789,
	HingeJoint_get_useMotor_m2E14DFC0BA78C44C43B14A4012CF83F28703617B,
	HingeJoint_get_useLimits_mFF9EB175970941DB444545729AE808CEAD50CB55,
	HingeJoint_get_useSpring_mDA9FBD711920418719E71DE98F3CB3CE95BEB776,
	HingeJoint_get_limits_Injected_mBC5408A9C2B0381572A34180B58D3C8B4405B161,
	HingeJoint_get_spring_Injected_mC61A3CC33620B8F1DF1E5F20CF89ED3E5861EB9A,
	SpringJoint_get_spring_mAA4F0F503D65A513A2B437ED7EB4E0D24973A28B,
	SpringJoint_get_damper_m13C4C3A0902157FB4F5768C2F7B5DF0AFD3B5019,
	SpringJoint_get_maxDistance_mBA26CEC9272132A773229CC1CC929D6DC5BC047E,
	CharacterJoint_get_swingAxis_m3D84F5C03ADEC6D798CD2F7D610CD4BED92B316F,
	CharacterJoint_set_swingAxis_mE1A37DAAA9E81D2AC53717C3D14188EB60E2C401,
	CharacterJoint_get_twistLimitSpring_m8D739C967A41819B9BED9E7175113ADDD333FB53,
	CharacterJoint_get_swingLimitSpring_mDC584B04AFAE8E93EB9978AA4C2ED2F9DEC0F315,
	CharacterJoint_get_lowTwistLimit_mF14777D3BEE16FD317EA79D674B69686EB600579,
	CharacterJoint_set_lowTwistLimit_m3354D75D6F1A2703CDB94A0169D797FD374DA5CD,
	CharacterJoint_get_highTwistLimit_mEAAFB8F58969DA1AE9C5FDB44012A990BB213A53,
	CharacterJoint_set_highTwistLimit_mCB094A57AFBD1833966D8CCB3A4555E091A39D02,
	CharacterJoint_get_swing1Limit_m0C3A7166C4AEE153CB663CF62F90E2E545B733AE,
	CharacterJoint_set_swing1Limit_m238C86F635049095559A92BD4F3E32A152D14B71,
	CharacterJoint_get_swing2Limit_mE128EB6EA89A3C0791EF032665FF6213CE574397,
	CharacterJoint_set_swing2Limit_m671879988AE029D2171C6F631B9D35E69C47DF43,
	CharacterJoint_get_enableProjection_mCFE8DE15EBE057ED6FB50DCA4CBE7D15FB6C713C,
	CharacterJoint_get_projectionDistance_m7FF0A9BA90DB6968D003A2A43E72E4A9C2319193,
	CharacterJoint_get_projectionAngle_mB959ED98AFFF713E0298C2C543A72EEEE97F25C0,
	CharacterJoint_get_swingAxis_Injected_mB19190386E2A95FB348A8FC533259C03A518EA3D,
	CharacterJoint_set_swingAxis_Injected_mB47A75071514311FB1BB8ED3C18818D94F8A9B6F,
	CharacterJoint_get_twistLimitSpring_Injected_m5B4773EFAA80FE677F65BBE10FE24418A8EBDD5D,
	CharacterJoint_get_swingLimitSpring_Injected_mBE8094BE61445AFF3E08423076C0A91FBF9DED61,
	CharacterJoint_get_lowTwistLimit_Injected_m0C15C16C4B5D49128F3117A1DB8DAC4978B170BA,
	CharacterJoint_set_lowTwistLimit_Injected_m99CF3E0755509747DEC5F57DE152DE525B76D3A5,
	CharacterJoint_get_highTwistLimit_Injected_mAEAFE737AF7A19AA5E52960A3D4FB4045EEA32AB,
	CharacterJoint_set_highTwistLimit_Injected_m23184846DE1A86DC30D073767383B10F01AA5FD6,
	CharacterJoint_get_swing1Limit_Injected_m38AEB2810965D21D04B007636ADAA87BA71C24EF,
	CharacterJoint_set_swing1Limit_Injected_m9E7B47F5A65D8C69FFC29820946652113AB2F8B0,
	CharacterJoint_get_swing2Limit_Injected_m7C50B2E73B1C889969551CF2CF18EA349BD4B4E6,
	CharacterJoint_set_swing2Limit_Injected_m9A8090BAAE2846A452B872448EE3179C660AF0F4,
	ConfigurableJoint_get_secondaryAxis_mE9390C46782477183A7E1542AAEAA16F743C1139,
	ConfigurableJoint_set_secondaryAxis_mC342ABE40AB6B5054B06ED4A8B712AAF79AB648A,
	ConfigurableJoint_get_xMotion_mA0B0EB46F696B3F7AD21C8FCFE570F9204B843DC,
	ConfigurableJoint_set_xMotion_mC5CF8F90391EC7C38B5825FEDF17CA2DF9647310,
	ConfigurableJoint_get_yMotion_m96FD055764DEC031E4028FEA73392AFC9B742AFD,
	ConfigurableJoint_set_yMotion_mF331F91608B4578B02BE457098A5099B1D89D7D5,
	ConfigurableJoint_get_zMotion_m6107BE4C3522026A26D89936A0E30B4124BCA7A9,
	ConfigurableJoint_set_zMotion_m9D2A9358472F9AC11A2FECDF9603BA79E004127A,
	ConfigurableJoint_get_angularXMotion_mCB43695BB1E2D10EB43CAC105C2F1E33418A5CE0,
	ConfigurableJoint_set_angularXMotion_mCC6D6951DC0D2BC40CE4AEA4F2DE224A39ED630E,
	ConfigurableJoint_get_angularYMotion_mD22A8F18305F7ADCC9E17524CA8D46B289B127E8,
	ConfigurableJoint_set_angularYMotion_m38E70B9932443F547BFE8C15F804ADA140BAB360,
	ConfigurableJoint_get_angularZMotion_m49D868A9CFE293E6A8D697ABDF952B613123FD28,
	ConfigurableJoint_set_angularZMotion_mB4CAE9B73204E261174AECB3C45069FF2E0FBBDE,
	ConfigurableJoint_set_linearLimitSpring_m605DDD6622C8FBA039CC9B18C7747DB6F73EF14C,
	ConfigurableJoint_set_angularXLimitSpring_m1EE955EED38E15A7E899F505A68DF34DFC3FAF60,
	ConfigurableJoint_set_angularYZLimitSpring_m98FA9937FC9D0C05D54BD5A28CA56A324DB049ED,
	ConfigurableJoint_set_linearLimit_mF600A726E8D4E5A49DABE0756636AD77FF7F9406,
	ConfigurableJoint_set_lowAngularXLimit_mF03F0EFDBBE85A29FE3D787E5607118DD08B71EB,
	ConfigurableJoint_set_highAngularXLimit_m9CF8EFEE498A2328943E19E15BF9A7C4539A5132,
	ConfigurableJoint_set_angularYLimit_m39CD3BD8ACCBB7F685BCF44B973E700AD11A3EAA,
	ConfigurableJoint_set_angularZLimit_m3A4C51AD727F48FE28E4886A8EC60508420B1DCC,
	ConfigurableJoint_set_targetRotation_m77F193BD35FBF52926CF40165B6A9D776C9103B6,
	ConfigurableJoint_set_targetAngularVelocity_m39DA1316EFFDA5D6B04A90587C488A4099F01A0B,
	ConfigurableJoint_set_rotationDriveMode_m9815890EA96A93E0BBCB5BD20EC7779D2E2A514A,
	ConfigurableJoint_set_slerpDrive_m341A1BFCB9DCF19F036A678E8864AC62F77B4B55,
	ConfigurableJoint_set_projectionMode_m5A4660083BA0AB076BAE52626CC46FAFC06B8262,
	ConfigurableJoint_set_projectionDistance_m1789C1F4F493FF46507491C06EFD77F516C9E7B1,
	ConfigurableJoint_set_projectionAngle_mD6D0C5890F88043862395E3329021B9BBA55DC95,
	ConfigurableJoint_set_configuredInWorldSpace_m23EA574084D35FA87640897F04EAC3236754339E,
	ConfigurableJoint_get_secondaryAxis_Injected_m6D51BC27202AACE1FA1BA20FF59298C66D794540,
	ConfigurableJoint_set_secondaryAxis_Injected_m8D630F896751C83CA9B07173DE8A120936733A91,
	ConfigurableJoint_set_linearLimitSpring_Injected_mE90C49B513E7B3EA7ABFA95A3B8ED2A73730C5AB,
	ConfigurableJoint_set_angularXLimitSpring_Injected_m01FA0164ABAF03566B4158C6DB7A068DEDA038BE,
	ConfigurableJoint_set_angularYZLimitSpring_Injected_mC5AF9A234E7E867197988D78BFA271C8F0AE0B2C,
	ConfigurableJoint_set_linearLimit_Injected_m07AFBF1C2812A55724A0D8877E7BC47DE4AD6236,
	ConfigurableJoint_set_lowAngularXLimit_Injected_m8363CB59C106D8D98E1CE44974A2935AA2722A88,
	ConfigurableJoint_set_highAngularXLimit_Injected_m46820CB6DD56738EE2E83A2603EFE8A993332F77,
	ConfigurableJoint_set_angularYLimit_Injected_m7FFFFAA6B3057DD3B48DFB0DC557C81046D3292A,
	ConfigurableJoint_set_angularZLimit_Injected_m1F8CA881EFF4CB5C14F56296F33F55446CE95ECA,
	ConfigurableJoint_set_targetRotation_Injected_m9992253D130FFAB7F74C360A6874A30329799B38,
	ConfigurableJoint_set_targetAngularVelocity_Injected_m400680616F59B0EA2BAB6B96DBFA28BF731DBFBF,
	ConfigurableJoint_set_slerpDrive_Injected_mDFDC04EFC76F28B3EEE803CCFCAD620DDC06D1B6,
	ContactPoint_get_point_mB222EB009D8B3DBE5F248088167F074551B424F7_AdjustorThunk,
	ContactPoint_get_normal_m0C47576D775F12C2BED5B25ACC720180008EDA3E_AdjustorThunk,
	PhysicsScene_ToString_m192392A2C486E2F76AAC736C519CB117BD187BE9_AdjustorThunk,
	PhysicsScene_GetHashCode_m8B6554AA89D7DFD183573EA89E5ACF6B21E08654_AdjustorThunk,
	PhysicsScene_Equals_m538C6A86CCC4EF0546590BAC51E5D08ACC2CBEF9_AdjustorThunk,
	PhysicsScene_Equals_m25CC71614BEABB76B6FB618516B8807DAF8CDDF8_AdjustorThunk,
	PhysicsScene_Raycast_mA90972A8E828722A26EBC754EABB3086BE6E2336_AdjustorThunk,
	PhysicsScene_Internal_RaycastTest_m09DD752D166094AC2A6F19302F7DDB2EA88DE67F,
	PhysicsScene_Raycast_m90D1AE1F641D9344BEBEF32E7AC0BAC883024C0B_AdjustorThunk,
	PhysicsScene_Internal_Raycast_m2769B78FDCAA21C6F609DD709BADFF6F4B07C529,
	PhysicsScene_Raycast_mFB4A1BB42BB1D950E3854A08DFE3ECCDEA3BCCB1_AdjustorThunk,
	PhysicsScene_Internal_RaycastNonAlloc_m9E5DAC724FC74D6B618C82BC8F19980E9636D5B1,
	PhysicsScene_Query_SphereCast_m6DD7B632CF528C867859E329E2C4807C2CC891D8,
	PhysicsScene_Internal_SphereCast_mAD5109B24E5C668DC9A49E434B5139CA8A43DBD0,
	PhysicsScene_SphereCast_m99D2D1C789BB817406E7C7FBAA75F2019A13D6C1_AdjustorThunk,
	PhysicsScene_Internal_RaycastTest_Injected_m544D0227B2312D7431A9351ADA5082B390B4ADDF,
	PhysicsScene_Internal_Raycast_Injected_m039B99FEFE39873FF8B0F60F44DE817BF43D48DA,
	PhysicsScene_Internal_RaycastNonAlloc_Injected_m4A5BC29FB0D0AAFC1DE827838CE144F882FE632E,
	PhysicsScene_Query_SphereCast_Injected_mA3953A644D1ED0B7D37A22E44635A413A4A1A12F,
	Physics_get_gravity_mC0F44371C49C1DD1455D6ADEC824BAE35E1E104B,
	Physics_get_defaultPhysicsScene_mEA96CE906577479223C956BFF0B8D0B79BB47B30,
	Physics_IgnoreCollision_mF7183C0761289A45400F0E2C1B223B980EBE5F8B,
	Physics_IgnoreLayerCollision_m0DFDA0BBDFA1F0737524272504E3D85989989C32,
	Physics_IgnoreLayerCollision_m21F8A3A611C8DB8CA7D80C3D7E9DB836126C0C2B,
	Physics_GetIgnoreLayerCollision_mD79111559F93397C8516436A7456086FB5021961,
	Physics_Raycast_m511ECD9EF3223645EF104DDA50BB81C07F29717C,
	Physics_Raycast_mC87F52EDC44BBBAFFED0D9DC92B37B11C6E07BEA,
	Physics_Raycast_m0583CCAA9E2F3BD031F12FA080837E9A48EEC16D,
	Physics_Raycast_m6FFBDC7166A2129B70027FC392D06C735F81C73E,
	Physics_Raycast_m44270C2D7C8CD056B6D78DE60A4C91FCC0A225C1,
	Physics_Raycast_mFB0018A196CC9E45CA8A238899EA8B093264B024,
	Physics_Raycast_mBEC747ED0A7660BB12AA48B663CCBF7B1EE93D6B,
	Physics_Raycast_m000FC36D038952F8AC5E3E562E7D39BAA8D1E287,
	Physics_Raycast_mF33E3BA9FDFE0D19B193D2AB05028B2086738D47,
	Physics_Raycast_m5BA453EA32F5B660CD9A537FED97B995971820E1,
	Physics_Raycast_m198FD20B3D1BC1634AE30E80E0FD1491D55417E5,
	Physics_Raycast_mF1112EC53F3393455B4ABA0E0092A58E3CD0114A,
	Physics_Raycast_m6B6C7CB6DDED17F9B1905C606035A56FF9AE88F8,
	Physics_Raycast_m9F5CAD8DA7923E897C5DCAF913BD411AD1A87950,
	Physics_Raycast_mE1590EE4E2DC950A9FC2437E98EE8CD2EC2DEE67,
	Physics_Raycast_mCE618579F667A62D15CB74CEF76147136EB2D495,
	Physics_Linecast_m2CAAB76F9786AB83FCF6B9C751B6A754AAEA48FB,
	Physics_Linecast_m1286F99BB2A490E80EA48A6DF456C81293A35C66,
	Physics_Linecast_mCAA3A0F713478346F006ADF57D65F64C89A544C6,
	Physics_Linecast_mBB78AB8D049EB835949B2940D511E1049F6D5758,
	Physics_Linecast_mB0BFB97C5093E1D1C08B5C3CDE1062D46CEB2318,
	Physics_SphereCast_m2D57A1F4DA048B95DC33939367F2556CEA22B8BD,
	Physics_SphereCast_m41B360D0B25BAADAD64F5D4BEAC07F92AF911837,
	Physics_SphereCast_m121F1B6EABA580E1661DE00D1974BD614D10C773,
	Physics_SphereCast_m6B6F941FB5632998A000262CDC9337E2B46E3871,
	Physics_SphereCast_mE831E516D318F8928FD9020BF7E9FEAD95574D68,
	Physics_Internal_RaycastAll_m366801D3E9D3DE8E9A94C5243D1FB2132DFDD851,
	Physics_RaycastAll_m6627435AE46B6720066FEEA5A80923420EE8FD2D,
	Physics_RaycastAll_m4FC397E4B7FEF7B5C9D172F95D6FB32CFC0F66FC,
	Physics_RaycastAll_m93B94F31C64E8F64ACE18231FC85168D519B7F04,
	Physics_RaycastAll_mE2ED29FCF8983C212F2628A8934CFAEBA4B683DE,
	Physics_RaycastAll_mF5DB872CCEFE1710439DB9939EACE1DCDEFE2A7D,
	Physics_RaycastAll_mE9ACFB603E337E2195E4B9419099E178F7FF82F4,
	Physics_RaycastAll_m6D5629FD1D3E3B0001F2819C1AE8660A3949C51F,
	Physics_RaycastAll_m37C7F423486CE3ACBAB4F1A326B9EBD7572B4056,
	Physics_RaycastNonAlloc_m606B435CE959801C9B7D7111E49071E893C951DE,
	Physics_RaycastNonAlloc_mBFDB05B1AA90C5D587A1BEA6BEE66FEF2DF89894,
	Physics_RaycastNonAlloc_m0A9CEF12FB7EE9C790609A15CBFE975725365CF9,
	Physics_RaycastNonAlloc_m9CC4B976EA1936076FFA1339BF6FBE78A2DA593F,
	Physics_RaycastNonAlloc_m120ADF6BD8DF57A40E6B395299B03FAEE348973F,
	Physics_RaycastNonAlloc_mA795C150FCE1F242C9806AAAFF04125594B56DDB,
	Physics_RaycastNonAlloc_m63644DC59B0554846D62ED3BE3B5517BC97D3316,
	Physics_RaycastNonAlloc_mD4393134F4470C9B319603A6CD3FF601F5981C88,
	Physics_OverlapSphere_Internal_m10C15A0F86303EF92B205F16591E344E5728B6D0,
	Physics_OverlapSphere_m73878904F621A28F8DD8855CB113C690CF1E89C0,
	Physics_OverlapSphere_m354A92672F7A6DE59EF1285D02D62247F46A5D84,
	Physics_get_autoSimulation_m7E21DC24254AD53C7CAD20414F86F6F8E5FE47EC,
	Physics_get_gravity_Injected_m7B5CE9CF65B245C2C2EA078BE636E393225B0335,
	Physics_get_defaultPhysicsScene_Injected_mEAB07CA5307D0DDB331A31E6A6BA28CF83009262,
	Physics_Internal_RaycastAll_Injected_m1FD369E67F19BB09F85967481F69EB24B5247913,
	Physics_OverlapSphere_Internal_Injected_m315C4BB796FD73B41F5710D1DB52A6BCB57C344D,
};
static const int32_t s_InvokerIndices[316] = 
{
	664,
	275,
	664,
	275,
	664,
	275,
	664,
	275,
	275,
	275,
	275,
	664,
	664,
	664,
	14,
	14,
	14,
	14,
	1050,
	23,
	122,
	275,
	275,
	275,
	32,
	32,
	14,
	1050,
	1051,
	1050,
	1051,
	664,
	14,
	1050,
	1051,
	1050,
	1051,
	664,
	664,
	664,
	275,
	102,
	31,
	102,
	31,
	31,
	32,
	1050,
	1051,
	1050,
	1050,
	1051,
	31,
	1050,
	1051,
	1239,
	1240,
	10,
	32,
	32,
	275,
	1051,
	1240,
	23,
	102,
	23,
	1035,
	1241,
	1051,
	1241,
	1051,
	1314,
	1048,
	1315,
	1316,
	23,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	511,
	64,
	64,
	1317,
	1318,
	102,
	31,
	14,
	102,
	31,
	1082,
	26,
	23,
	6,
	1062,
	1319,
	1050,
	102,
	767,
	1320,
	6,
	26,
	31,
	1050,
	1051,
	664,
	275,
	664,
	275,
	10,
	32,
	23,
	6,
	6,
	1050,
	1051,
	1050,
	1051,
	6,
	6,
	6,
	6,
	1050,
	1051,
	664,
	6,
	6,
	14,
	26,
	1050,
	1051,
	1050,
	1051,
	1050,
	1051,
	102,
	31,
	664,
	275,
	664,
	275,
	102,
	31,
	6,
	6,
	6,
	6,
	6,
	6,
	1321,
	1322,
	102,
	102,
	102,
	6,
	6,
	664,
	664,
	664,
	1050,
	1051,
	1323,
	1323,
	1324,
	1325,
	1324,
	1325,
	1324,
	1325,
	1324,
	1325,
	102,
	664,
	664,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	1050,
	1051,
	10,
	32,
	10,
	32,
	10,
	32,
	10,
	32,
	10,
	32,
	10,
	32,
	1326,
	1326,
	1326,
	1325,
	1325,
	1325,
	1325,
	1325,
	1240,
	1051,
	32,
	1327,
	32,
	275,
	275,
	31,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	1050,
	1050,
	14,
	10,
	9,
	1328,
	1329,
	1330,
	1331,
	1332,
	1333,
	1334,
	1335,
	1336,
	1337,
	1338,
	1339,
	1340,
	1341,
	1166,
	1342,
	1343,
	1344,
	123,
	53,
	1345,
	1346,
	1347,
	1168,
	1348,
	1349,
	1350,
	1351,
	1352,
	1353,
	1354,
	1355,
	1356,
	1357,
	1358,
	1359,
	1360,
	1361,
	1362,
	1363,
	1351,
	1364,
	1365,
	1366,
	1367,
	1368,
	1369,
	1370,
	1371,
	1372,
	1373,
	1374,
	1375,
	1376,
	1377,
	1378,
	1379,
	1380,
	1381,
	1382,
	1383,
	1384,
	1385,
	1386,
	1387,
	1388,
	49,
	17,
	17,
	1389,
	1389,
};
extern const Il2CppCodeGenModule g_UnityEngine_PhysicsModuleCodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_PhysicsModuleCodeGenModule = 
{
	"UnityEngine.PhysicsModule.dll",
	316,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
