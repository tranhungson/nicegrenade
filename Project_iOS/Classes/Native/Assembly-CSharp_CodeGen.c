﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void Ally::OnEnable()
extern void Ally_OnEnable_m6A41660CF284185A92A6C25E074DF390E67A869C ();
// 0x00000002 System.Void Ally::OnDisable()
extern void Ally_OnDisable_m113302690F880359CE150C2544CE2F934055EDDB ();
// 0x00000003 System.Void Ally::GameOver()
extern void Ally_GameOver_mE9899D77788537C2703DAC76B432E9361CB72059 ();
// 0x00000004 System.Void Ally::.ctor()
extern void Ally__ctor_m03330EBA955FEF5C1FB9A6F5B68C37CC9AEB596F ();
// 0x00000005 System.Void ETFXProjectileScript::Start()
extern void ETFXProjectileScript_Start_m6E1A7FAF7955CF5D05D492FE2639FAF49C29F3A7 ();
// 0x00000006 System.Void ETFXProjectileScript::FixedUpdate()
extern void ETFXProjectileScript_FixedUpdate_mBA7580BD5AA2429A3884E902E0D954B9A07AF0BB ();
// 0x00000007 System.Void ETFXProjectileScript::.ctor()
extern void ETFXProjectileScript__ctor_m83347094E99EC927D58FE6F3A983665C1CFCF035 ();
// 0x00000008 System.Void ETFXSceneManager::LoadScene2DDemo()
extern void ETFXSceneManager_LoadScene2DDemo_mE781B655A5B96B0F7C79E6DA112287A1ACE21492 ();
// 0x00000009 System.Void ETFXSceneManager::LoadSceneCards()
extern void ETFXSceneManager_LoadSceneCards_mE6392B1EF78F3BD32D1808BAD0FDE93796CEA772 ();
// 0x0000000A System.Void ETFXSceneManager::LoadSceneCombat()
extern void ETFXSceneManager_LoadSceneCombat_m29FB4A70E76F7EB604F8EA2DA1891B378EF037E8 ();
// 0x0000000B System.Void ETFXSceneManager::LoadSceneDecals()
extern void ETFXSceneManager_LoadSceneDecals_m8F33C93462F032A6079FB6B31154638284190D6E ();
// 0x0000000C System.Void ETFXSceneManager::LoadSceneDecals2()
extern void ETFXSceneManager_LoadSceneDecals2_m50EDDC42545A5F7905C4C98CA72C8632C1F8E40D ();
// 0x0000000D System.Void ETFXSceneManager::LoadSceneEmojis()
extern void ETFXSceneManager_LoadSceneEmojis_m17E51BD74D6E346ED9A8D006E7C0BB10777F643A ();
// 0x0000000E System.Void ETFXSceneManager::LoadSceneEmojis2()
extern void ETFXSceneManager_LoadSceneEmojis2_m51651D4A6E9E6C7A1E5D268EFF5126F76B1759C7 ();
// 0x0000000F System.Void ETFXSceneManager::LoadSceneExplosions()
extern void ETFXSceneManager_LoadSceneExplosions_m908FEB716367413D6035609F6ED56D84A2B7BB1D ();
// 0x00000010 System.Void ETFXSceneManager::LoadSceneExplosions2()
extern void ETFXSceneManager_LoadSceneExplosions2_mD9DB30FBF938961AF70EC84549C958458CD4EA1F ();
// 0x00000011 System.Void ETFXSceneManager::LoadSceneFire()
extern void ETFXSceneManager_LoadSceneFire_m7FA403188E1BF5FBAC751B17506B33274420073A ();
// 0x00000012 System.Void ETFXSceneManager::LoadSceneFire2()
extern void ETFXSceneManager_LoadSceneFire2_m7D2466C93F199DC33C0A9E0C7A116F7DD9CD14F5 ();
// 0x00000013 System.Void ETFXSceneManager::LoadSceneFire3()
extern void ETFXSceneManager_LoadSceneFire3_mC270519E872C1871A22256FB0E9F129663E31035 ();
// 0x00000014 System.Void ETFXSceneManager::LoadSceneFireworks()
extern void ETFXSceneManager_LoadSceneFireworks_mBEBEE0D3702F454CF8A194804459FD975E10842B ();
// 0x00000015 System.Void ETFXSceneManager::LoadSceneFlares()
extern void ETFXSceneManager_LoadSceneFlares_m19F7F5565F02CB3093E28C777922D493EE36F909 ();
// 0x00000016 System.Void ETFXSceneManager::LoadSceneMagic()
extern void ETFXSceneManager_LoadSceneMagic_m61ED7F575FC3D2D8AA3D4C2575F10749C79EF60D ();
// 0x00000017 System.Void ETFXSceneManager::LoadSceneMagic2()
extern void ETFXSceneManager_LoadSceneMagic2_m5D07B15B6636CF65DC46915564C68B8210525668 ();
// 0x00000018 System.Void ETFXSceneManager::LoadSceneMagic3()
extern void ETFXSceneManager_LoadSceneMagic3_mBA147539B53E56AAEAD46361E3B26C9098D0FE4E ();
// 0x00000019 System.Void ETFXSceneManager::LoadSceneMainDemo()
extern void ETFXSceneManager_LoadSceneMainDemo_m288F240F4D782FDE5F2F13071F4F08E0D47B5FA2 ();
// 0x0000001A System.Void ETFXSceneManager::LoadSceneMissiles()
extern void ETFXSceneManager_LoadSceneMissiles_mC2146CC83899D75C87D58F26D94497417758C5B8 ();
// 0x0000001B System.Void ETFXSceneManager::LoadScenePortals()
extern void ETFXSceneManager_LoadScenePortals_mB05FB9FE782E3F284670F26DCF35F2870D12DBE5 ();
// 0x0000001C System.Void ETFXSceneManager::LoadScenePortals2()
extern void ETFXSceneManager_LoadScenePortals2_m6C0D1846A8F3B6E2F01937623BA6DDCF84F670B3 ();
// 0x0000001D System.Void ETFXSceneManager::LoadScenePowerups()
extern void ETFXSceneManager_LoadScenePowerups_mA4DDF427C8031375E2AD9A5A1B05EEF039088C86 ();
// 0x0000001E System.Void ETFXSceneManager::LoadScenePowerups2()
extern void ETFXSceneManager_LoadScenePowerups2_mA18C9E8AA8BB70F6F42B15355A475D29B9938A31 ();
// 0x0000001F System.Void ETFXSceneManager::LoadSceneSparkles()
extern void ETFXSceneManager_LoadSceneSparkles_m58BEC471FC1EED46A4B4C526F2D4AF67302D87C3 ();
// 0x00000020 System.Void ETFXSceneManager::LoadSceneSwordCombat()
extern void ETFXSceneManager_LoadSceneSwordCombat_m15F504EFC2BC5E16DD780555A63BEC9D9C67C0C8 ();
// 0x00000021 System.Void ETFXSceneManager::LoadSceneSwordCombat2()
extern void ETFXSceneManager_LoadSceneSwordCombat2_mFE31465497F70DC3237F68A820EFB39D1BB93994 ();
// 0x00000022 System.Void ETFXSceneManager::LoadSceneMoney()
extern void ETFXSceneManager_LoadSceneMoney_m287946AD46201A0CBA5D5F0C99762B7F060BCAA6 ();
// 0x00000023 System.Void ETFXSceneManager::LoadSceneHealing()
extern void ETFXSceneManager_LoadSceneHealing_m614159E20FA8FB9C1A49CA6173B1CCC59F56B555 ();
// 0x00000024 System.Void ETFXSceneManager::LoadSceneWind()
extern void ETFXSceneManager_LoadSceneWind_m3109735CDEBFC386685D2A27817B645D5FBF6AE1 ();
// 0x00000025 System.Void ETFXSceneManager::Update()
extern void ETFXSceneManager_Update_mB6494C8700F23FBEDC192E56A3EAB0E548F2EB80 ();
// 0x00000026 System.Void ETFXSceneManager::.ctor()
extern void ETFXSceneManager__ctor_m36D42D80C5078EC28655FD9179179556AE6DEAB5 ();
// 0x00000027 System.Void PEButtonScript::Start()
extern void PEButtonScript_Start_m8A547470C61210076A8406D0F018A040BF5B37A7 ();
// 0x00000028 System.Void PEButtonScript::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void PEButtonScript_OnPointerEnter_mADE18B90955FA7ADB1F15403FF64E97E1EF0A9D1 ();
// 0x00000029 System.Void PEButtonScript::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void PEButtonScript_OnPointerExit_m796601A33B6D76535AB02237627FA38A64522E36 ();
// 0x0000002A System.Void PEButtonScript::OnButtonClicked()
extern void PEButtonScript_OnButtonClicked_mD7B14EEB96DF66A13B83DDB5F60357CD617C06B5 ();
// 0x0000002B System.Void PEButtonScript::.ctor()
extern void PEButtonScript__ctor_mCEA81838780EC86A88EA1FEFF96809B92C3CF413 ();
// 0x0000002C System.Void ParticleEffectsLibrary::Awake()
extern void ParticleEffectsLibrary_Awake_mCE4F7DE403B8812636A82F733F174E78CEA892DD ();
// 0x0000002D System.Void ParticleEffectsLibrary::Start()
extern void ParticleEffectsLibrary_Start_mDB7425580D9DD052C79F59A4E0D4F49117CE8B64 ();
// 0x0000002E System.String ParticleEffectsLibrary::GetCurrentPENameString()
extern void ParticleEffectsLibrary_GetCurrentPENameString_m1691E2A36A3A021A52107351116FC6788DB26366 ();
// 0x0000002F System.Void ParticleEffectsLibrary::PreviousParticleEffect()
extern void ParticleEffectsLibrary_PreviousParticleEffect_m461C6A9FE69983C9D115A6FC794E0A27701691B6 ();
// 0x00000030 System.Void ParticleEffectsLibrary::NextParticleEffect()
extern void ParticleEffectsLibrary_NextParticleEffect_m7D6F09E81DA16D6B1FF5221D2258CBD17B6474B4 ();
// 0x00000031 System.Void ParticleEffectsLibrary::SpawnParticleEffect(UnityEngine.Vector3)
extern void ParticleEffectsLibrary_SpawnParticleEffect_mA27B745B4BB8FBEDBF02F2BDA7E8A3A6F169EB1A ();
// 0x00000032 System.Void ParticleEffectsLibrary::.ctor()
extern void ParticleEffectsLibrary__ctor_m6DDE8AB1042AFB41851A2F29B003E73087C217CA ();
// 0x00000033 System.Void UICanvasManager::Awake()
extern void UICanvasManager_Awake_m5E00062167821A732D364DDE4EF0D486F2AB15F1 ();
// 0x00000034 System.Void UICanvasManager::Start()
extern void UICanvasManager_Start_m52A5A4A6201A8851DAA745961E000F3129E7A470 ();
// 0x00000035 System.Void UICanvasManager::Update()
extern void UICanvasManager_Update_mE2DD2A793B2AAEC6417901769CA7429AAE9DF3D1 ();
// 0x00000036 System.Void UICanvasManager::UpdateToolTip(ButtonTypes)
extern void UICanvasManager_UpdateToolTip_m6F278FBA74B3F998D30C84CB45C58824A6DACEF5 ();
// 0x00000037 System.Void UICanvasManager::ClearToolTip()
extern void UICanvasManager_ClearToolTip_mCDCEC725E6C8F802D5EBBEA35475E4D0AD1030E3 ();
// 0x00000038 System.Void UICanvasManager::SelectPreviousPE()
extern void UICanvasManager_SelectPreviousPE_mE464573E1B2E606EC42AB776DDE7E80941E7C6AB ();
// 0x00000039 System.Void UICanvasManager::SelectNextPE()
extern void UICanvasManager_SelectNextPE_mA733B8357E23242CEFF99C340C29F6C18B815BA4 ();
// 0x0000003A System.Void UICanvasManager::SpawnCurrentParticleEffect()
extern void UICanvasManager_SpawnCurrentParticleEffect_mF0B7AB6F80B2C49D277774DEC620610309621F62 ();
// 0x0000003B System.Void UICanvasManager::UIButtonClick(ButtonTypes)
extern void UICanvasManager_UIButtonClick_m8B5AB9450814A38B39DF17657F45B1149CC0C765 ();
// 0x0000003C System.Void UICanvasManager::.ctor()
extern void UICanvasManager__ctor_m37771377D8BAC0CD9C48FAB83A4960EE3DBC04FF ();
// 0x0000003D System.Void QuickStart::Start()
extern void QuickStart_Start_mCC0C83C2D4568D036FEB485FD604535A330D1706 ();
// 0x0000003E System.Void QuickStart::.ctor()
extern void QuickStart__ctor_mB059290F71394E20F766C5B0B4433850D7D5E4F3 ();
// 0x0000003F System.Void TweenFragment::Start()
extern void TweenFragment_Start_mADFEF8A9F351FB03BFA2373DE816D80A807AC857 ();
// 0x00000040 System.Void TweenFragment::Update()
extern void TweenFragment_Update_mDD2E2C21F34CC6EFC25AD3CC257314B3B1B2E59A ();
// 0x00000041 System.Void TweenFragment::.ctor()
extern void TweenFragment__ctor_m2D985ED567FDBC6719F9C89E53E6F9FC26AE7442 ();
// 0x00000042 System.Void BreakableObstacle::OnTriggerEnter(UnityEngine.Collider)
extern void BreakableObstacle_OnTriggerEnter_mE0E398C367FAA6D9E584619E507538CE979B98A9 ();
// 0x00000043 System.Void BreakableObstacle::.ctor()
extern void BreakableObstacle__ctor_mC44EC03CDCC5634D7B55860767207D9D3743152F ();
// 0x00000044 System.Void CameraManager::Awake()
extern void CameraManager_Awake_m4F295CC2C61D64BAC928889D2E54C86FFC7DEC35 ();
// 0x00000045 System.Void CameraManager::.ctor()
extern void CameraManager__ctor_m8B63DCA86FB61304B4F9DE1A4323F653D350F4A1 ();
// 0x00000046 System.Void Character::Awake()
extern void Character_Awake_mD2FA19540897E4C0B7C046CDF27511AA687421B0 ();
// 0x00000047 System.Void Character::OnTriggerEnter(UnityEngine.Collider)
extern void Character_OnTriggerEnter_mF31790DC11B982E42DE90E2F34388370CA7807B7 ();
// 0x00000048 System.Void Character::.ctor()
extern void Character__ctor_mD5A6FF1938E86A2995DD8F4DBA0F9BFEF2965966 ();
// 0x00000049 System.Void CheatButton::OnEnable()
extern void CheatButton_OnEnable_m54C745E81DDDDAB74B4595393F8DE37F949C7AD2 ();
// 0x0000004A System.Void CheatButton::OnClick()
extern void CheatButton_OnClick_mCE8015B88A7F7284129916BBFD7DB63C018BDC09 ();
// 0x0000004B System.Void CheatButton::.ctor()
extern void CheatButton__ctor_mF07A675E295B77750986EF1C4AD655994AAC1688 ();
// 0x0000004C System.Void CheatStageButton::OnClick()
extern void CheatStageButton_OnClick_mBB77C149BC42F5273D8A357851D8071656122A4A ();
// 0x0000004D System.Void CheatStageButton::.ctor()
extern void CheatStageButton__ctor_m8549B66A977461D7DDF73691D3DDF9A3A9379C22 ();
// 0x0000004E System.Void DelayProgressBar::set_CurrentGrenade(Grenade)
extern void DelayProgressBar_set_CurrentGrenade_mC479322DE98D4CDB75163B43645462888689175E ();
// 0x0000004F Grenade DelayProgressBar::get_CurrentGrenade()
extern void DelayProgressBar_get_CurrentGrenade_mF1F28FF168441D61D3EC7FC0D4C61E8307870A81 ();
// 0x00000050 System.Void DelayProgressBar::set_CanThrow(System.Boolean)
extern void DelayProgressBar_set_CanThrow_m2E86ABF312904C7693E29826AE0550046799E251 ();
// 0x00000051 System.Boolean DelayProgressBar::get_CanThrow()
extern void DelayProgressBar_get_CanThrow_m2BFA4858F9A8E97BE3CA7C9D5736E6FE2F13B1FE ();
// 0x00000052 System.Void DelayProgressBar::set_ShouldExplode(System.Boolean)
extern void DelayProgressBar_set_ShouldExplode_m5C5E8A42760DAEE0E4A428E523B2E501F83E2307 ();
// 0x00000053 System.Boolean DelayProgressBar::get_ShouldExplode()
extern void DelayProgressBar_get_ShouldExplode_m9632C5B9397D9DEBD5D78DA0B73D41AB6DD7A4ED ();
// 0x00000054 System.Void DelayProgressBar::OnEnable()
extern void DelayProgressBar_OnEnable_m032431F7A91EBA54B56B80BC6A5486BB68085896 ();
// 0x00000055 System.Void DelayProgressBar::StartCountDown()
extern void DelayProgressBar_StartCountDown_m8F0A3F9F58FF779FF8FE36C41449D6AAC523A27A ();
// 0x00000056 System.Void DelayProgressBar::StopCountDown()
extern void DelayProgressBar_StopCountDown_m7B31D278964ACAFC9FC23AF08D31911AC01594BE ();
// 0x00000057 System.Void DelayProgressBar::.ctor()
extern void DelayProgressBar__ctor_mD0CEFD4F2AD21416DACADCC5CA4251C9D18605E8 ();
// 0x00000058 System.Single DelayProgressBar::<StartCountDown>b__18_0()
extern void DelayProgressBar_U3CStartCountDownU3Eb__18_0_mE2C76B265F85A9329A96D28E840C39E30E2BD7CF ();
// 0x00000059 System.Void DelayProgressBar::<StartCountDown>b__18_1(System.Single)
extern void DelayProgressBar_U3CStartCountDownU3Eb__18_1_mEAF455BD39A0ABF3AA24A4C75184406887734791 ();
// 0x0000005A System.Void DelayProgressBar::<StartCountDown>b__18_2()
extern void DelayProgressBar_U3CStartCountDownU3Eb__18_2_m79521654240E345DC67C7248BBA61701DBAAD394 ();
// 0x0000005B System.Void EditDelayCheat::OnEnable()
extern void EditDelayCheat_OnEnable_m00C0C75D7808C162FD2EF3A2B0DEC2F660F1072D ();
// 0x0000005C System.Void EditDelayCheat::ChangeDelayTime()
extern void EditDelayCheat_ChangeDelayTime_mAD54BCFC6FA8D28DE73CBC141359CA68F3887FDE ();
// 0x0000005D System.Void EditDelayCheat::.ctor()
extern void EditDelayCheat__ctor_mF1BF0CC3B6D4278E07B1D55E64E076FCCCC3FC82 ();
// 0x0000005E System.Void Enemy::OnEnable()
extern void Enemy_OnEnable_m01D2775026413EC6DF615D76DD967B7C94216FFC ();
// 0x0000005F System.Void Enemy::OnDisable()
extern void Enemy_OnDisable_m5576451CF694305A6D15D450ABD8D05591263373 ();
// 0x00000060 System.Void Enemy::Update()
extern void Enemy_Update_m4B43E4ED31FD86486FE3A8B0F65B9D69A46217E6 ();
// 0x00000061 System.Void Enemy::GameOver()
extern void Enemy_GameOver_mEC12CE3DE1FF19DD6831BBBF2EA75251AC102062 ();
// 0x00000062 System.Void Enemy::Idle()
extern void Enemy_Idle_mAD59C3DA5E59AB234CF02DB2A0B621C164CCB866 ();
// 0x00000063 System.Void Enemy::Walk()
extern void Enemy_Walk_m9A829016AE89992912F574B5F2B71A333ADD6A61 ();
// 0x00000064 System.Void Enemy::ThrowLeft()
extern void Enemy_ThrowLeft_mB7B1CD7BF802885A766637BFFB5888B3C929EE76 ();
// 0x00000065 System.Void Enemy::.ctor()
extern void Enemy__ctor_mA9911783A212E3706D7D51DC82C19D14CBDE5BE3 ();
// 0x00000066 System.Void Enemy::<Update>b__11_0()
extern void Enemy_U3CUpdateU3Eb__11_0_m99E25CBE40B95593EE60DA3C05B3C0A6733AFDFC ();
// 0x00000067 System.Void ExplostionFx::OnEnable()
extern void ExplostionFx_OnEnable_m6B57E0A01FBC5A0D87C77D95911D5FAAE58D08B5 ();
// 0x00000068 System.Void ExplostionFx::SelfDestroy()
extern void ExplostionFx_SelfDestroy_m8FD60E94C5340D0C69E6A13AF600EC462DCF2460 ();
// 0x00000069 System.Void ExplostionFx::.ctor()
extern void ExplostionFx__ctor_m022EA2C700692574BAAE3905C20B8F5B1FADFD88 ();
// 0x0000006A System.Void FerrisChair::Update()
extern void FerrisChair_Update_m0BD16FB497FB219AA0D7C7D8EC884B2153E27E6B ();
// 0x0000006B System.Void FerrisChair::.ctor()
extern void FerrisChair__ctor_mF166A1422B702A70D06377160EABAC111DDEA1E2 ();
// 0x0000006C System.Void GameController::set_GameStart(System.Boolean)
extern void GameController_set_GameStart_mE59E339E0213C61C1351D6CD16BA8F8A7F9F4B54 ();
// 0x0000006D System.Boolean GameController::get_GameStart()
extern void GameController_get_GameStart_m53BCF263D4C3722C9190CB5857AFBCD619DE36D1 ();
// 0x0000006E System.Void GameController::set_GameEnd(System.Boolean)
extern void GameController_set_GameEnd_m35EF4F5ECF459A73DCB5B0F23FB11079F52A7DB0 ();
// 0x0000006F System.Boolean GameController::get_GameEnd()
extern void GameController_get_GameEnd_m45E1DEA980A154C40333DB335F06CD10F46835BC ();
// 0x00000070 System.Void GameController::set_TargetCount(System.Int32)
extern void GameController_set_TargetCount_mFE2376019A69085F47BC0B6A66FEAF9609419273 ();
// 0x00000071 System.Int32 GameController::get_TargetCount()
extern void GameController_get_TargetCount_m8C77854AF4BC4A3776B9F6054D433DCF7529D6D1 ();
// 0x00000072 System.Void GameController::set_HintIndicator(UnityEngine.GameObject)
extern void GameController_set_HintIndicator_m10F046FFC927CA948D866A996131AC0DAC5FAC58 ();
// 0x00000073 UnityEngine.GameObject GameController::get_HintIndicator()
extern void GameController_get_HintIndicator_mF95C357B724D6AE8A3A6B072FE985579AF02264F ();
// 0x00000074 System.Void GameController::set_ThrowCount(System.Int32)
extern void GameController_set_ThrowCount_m7FA38B82CE0F0CE3176AE6E78AC160FF5DF88A1B ();
// 0x00000075 System.Int32 GameController::get_ThrowCount()
extern void GameController_get_ThrowCount_mA7670F2E17C7A073374FF1F2BD88F59CB4B07190 ();
// 0x00000076 System.Void GameController::OnEnable()
extern void GameController_OnEnable_m0AECD2ED9B514027A58EC0BC732C80AB901E17F4 ();
// 0x00000077 System.Void GameController::ShowHint()
extern void GameController_ShowHint_m8DE7C9ECACBCD2FD329D631E6E32D9FD698174CF ();
// 0x00000078 System.Void GameController::Explose(UnityEngine.Transform,System.Boolean)
extern void GameController_Explose_m90AB64A631F178DDD5F8890247C54FBD78514574 ();
// 0x00000079 System.Void GameController::ForceGameOver()
extern void GameController_ForceGameOver_m4A97F3D29F43E4E196C65FF31F9684AA1628BEAD ();
// 0x0000007A System.Void GameController::.ctor()
extern void GameController__ctor_m6C90AFC15A0F46BF2901D879C39FE7E50AE7CDB3 ();
// 0x0000007B System.Void Grenade::set_ActiveBlastZone(System.Boolean)
extern void Grenade_set_ActiveBlastZone_mBD2A19909BF1E94DDB05BB5BE1DFC73BDA9C063E ();
// 0x0000007C System.Boolean Grenade::get_ActiveBlastZone()
extern void Grenade_get_ActiveBlastZone_mBE3283B53B2F6FB12AAD9205D641398C205C584F ();
// 0x0000007D System.Void Grenade::Explode()
extern void Grenade_Explode_m8697ACE2B00F4F41565957A9557A2CDBF0D36E5E ();
// 0x0000007E System.Void Grenade::OnTriggerEnter(UnityEngine.Collider)
extern void Grenade_OnTriggerEnter_mE73AF1BC0C410DE23A915A3F1C98F384CD05A137 ();
// 0x0000007F System.Void Grenade::OnCollisionEnter(UnityEngine.Collision)
extern void Grenade_OnCollisionEnter_m3DFE013A1F3360E744952388E563F639620B5227 ();
// 0x00000080 System.Void Grenade::.ctor()
extern void Grenade__ctor_mF6563EEA4F8B30F92D150495AE3703A76742AE6C ();
// 0x00000081 System.Void HelpFinger::Start()
extern void HelpFinger_Start_m2363DA6BAE13D4B7DD00B4487E8970E709EF5449 ();
// 0x00000082 System.Void HelpFinger::Animation()
extern void HelpFinger_Animation_m4AB5C0439E3F8734466C97C2F562782175111DCC ();
// 0x00000083 System.Void HelpFinger::OnDisable()
extern void HelpFinger_OnDisable_mA9DB38D127A274937517D2E486513BD0FBD77377 ();
// 0x00000084 System.Void HelpFinger::.ctor()
extern void HelpFinger__ctor_mE6053A467D059683B7234C9B6A833D61A99C411D ();
// 0x00000085 System.Void HelpFinger::<Animation>b__5_0()
extern void HelpFinger_U3CAnimationU3Eb__5_0_m7966801C81B7383570A124399F1FC7B34F278D70 ();
// 0x00000086 System.Void HelpFinger::<Animation>b__5_1()
extern void HelpFinger_U3CAnimationU3Eb__5_1_mEAB22B915FE8C633F97B194E579DB4653611C3B4 ();
// 0x00000087 System.Void HelpFinger::<Animation>b__5_2()
extern void HelpFinger_U3CAnimationU3Eb__5_2_m4A4F7D5E4EDDD8DC06788DDFBF3B8CDA736AB3BF ();
// 0x00000088 System.Void HelpFinger::<Animation>b__5_3()
extern void HelpFinger_U3CAnimationU3Eb__5_3_m90B29EFE63D31BEC88FD59F16B309885F7D93AD3 ();
// 0x00000089 System.Void HelpFinger::<Animation>b__5_4()
extern void HelpFinger_U3CAnimationU3Eb__5_4_mE1034B2780EDA827D07A650C9EC8BCA5259B2F7E ();
// 0x0000008A System.Void HelpFinger::<Animation>b__5_5()
extern void HelpFinger_U3CAnimationU3Eb__5_5_m0DAACD364CA9636D3D0016E98D07DBC24BB282FC ();
// 0x0000008B System.Void Hint::Awake()
extern void Hint_Awake_m5A912001BB4DE311DBF4E9D4B357CC94A3EB6608 ();
// 0x0000008C System.Void Hint::.ctor()
extern void Hint__ctor_m36E4D8E419E289FEE404EB8B51470CDAF2F1A810 ();
// 0x0000008D System.Void HintButton::OnClick()
extern void HintButton_OnClick_m5FD3BDAF2EDACC9F60C2B61C7E175DA361F378C5 ();
// 0x0000008E System.Void HintButton::.ctor()
extern void HintButton__ctor_mF0B74E033CD5ACC2904A77229C733E13B864EC91 ();
// 0x0000008F System.Void HintSuggestion::Awake()
extern void HintSuggestion_Awake_mFE3596DD8C6C81FA3C1656C4636F99369A2C3F2D ();
// 0x00000090 System.Void HintSuggestion::OnEnable()
extern void HintSuggestion_OnEnable_m97A37B4B6E3EB8D48DFEB55D9CBC60A4ED026CA1 ();
// 0x00000091 System.Void HintSuggestion::Move()
extern void HintSuggestion_Move_mB1730A0FB040AC560A7773F9FB24593F11B6AF14 ();
// 0x00000092 System.Void HintSuggestion::OnDisable()
extern void HintSuggestion_OnDisable_mFE72A8A53197023CB2F442908A008119DBBAA6CE ();
// 0x00000093 System.Void HintSuggestion::.ctor()
extern void HintSuggestion__ctor_mCF14DFE0FA051C30892DEF4C7BAE740A1895D697 ();
// 0x00000094 System.Void HintSuggestion::<Move>b__7_0()
extern void HintSuggestion_U3CMoveU3Eb__7_0_m3F091493D90598D3AE0222BE15229CB41F1C8878 ();
// 0x00000095 System.Void HintSuggestion::<Move>b__7_1()
extern void HintSuggestion_U3CMoveU3Eb__7_1_m8F9C586207E3C7AECE29FBFE888DD21626C14FE2 ();
// 0x00000096 System.Void HintSuggestion::<Move>b__7_2()
extern void HintSuggestion_U3CMoveU3Eb__7_2_m4AEC6BC293588FD6BE3514AA095EF1F82ED11033 ();
// 0x00000097 System.Void HintSuggestion::<Move>b__7_3()
extern void HintSuggestion_U3CMoveU3Eb__7_3_m3CD4BAFF03198B940845F1390E707CA1CFABA314 ();
// 0x00000098 System.Void HintSuggestion::<Move>b__7_4()
extern void HintSuggestion_U3CMoveU3Eb__7_4_mDB5A42A4058B85437122DF626B5588A198DBAFAD ();
// 0x00000099 System.Void EnvelopContent::Start()
extern void EnvelopContent_Start_mA8CF0807E269B510ED4A0E1FCA6B554BB78ABF0E ();
// 0x0000009A System.Void EnvelopContent::OnEnable()
extern void EnvelopContent_OnEnable_m3FF58C03E1CC5C6AD93428CA886AD4766EC1764A ();
// 0x0000009B System.Void EnvelopContent::Execute()
extern void EnvelopContent_Execute_mBCF0EFE00A3030FF3DFBFBC3AE6474BCC36362DE ();
// 0x0000009C System.Void EnvelopContent::.ctor()
extern void EnvelopContent__ctor_m73FAC025B320878F40783C918E4FF9C150DBDE23 ();
// 0x0000009D System.Void IgnoreTweenEffected::.ctor()
extern void IgnoreTweenEffected__ctor_m47A2087DECE858369787CA24ABBF8DD3219B876E ();
// 0x0000009E System.Void LanguageSelection::Awake()
extern void LanguageSelection_Awake_mCA6261683F546AFC4EDA7CD2F075D914E53C995F ();
// 0x0000009F System.Void LanguageSelection::Start()
extern void LanguageSelection_Start_mCE78C383E54769AD9A827EAFDF791EC73B584E95 ();
// 0x000000A0 System.Void LanguageSelection::OnEnable()
extern void LanguageSelection_OnEnable_mE542564DF175F4F5F2C83EEAA134318A0F5D71CC ();
// 0x000000A1 System.Void LanguageSelection::Refresh()
extern void LanguageSelection_Refresh_m5E08F1E09D1450F2A2939EC5F234D1222F8BCFAD ();
// 0x000000A2 System.Void LanguageSelection::OnLocalize()
extern void LanguageSelection_OnLocalize_mEE0511C277F910C0A1CC1C5A35A153848B1FCA41 ();
// 0x000000A3 System.Void LanguageSelection::.ctor()
extern void LanguageSelection__ctor_m3F3565EE45E63D6B61CF77BBC5C8541DD7508281 ();
// 0x000000A4 System.Boolean TypewriterEffect::get_isActive()
extern void TypewriterEffect_get_isActive_mDF744890861759BFACD85BDD0D9EF5F6B7A3D2A3 ();
// 0x000000A5 System.Void TypewriterEffect::ResetToBeginning()
extern void TypewriterEffect_ResetToBeginning_mEB2D990202B941ABC8F3F6DA4FA6926A5044AE05 ();
// 0x000000A6 System.Void TypewriterEffect::Finish()
extern void TypewriterEffect_Finish_m445F6F2DA6F1FDCDFE1E4C1F862C3115C970AFDE ();
// 0x000000A7 System.Void TypewriterEffect::OnEnable()
extern void TypewriterEffect_OnEnable_m4D0D919F0B7D1FBFBD767B9BABEADFDA17C83A8B ();
// 0x000000A8 System.Void TypewriterEffect::OnDisable()
extern void TypewriterEffect_OnDisable_mBFBB51E629E4FAE306FCE63009EFB2DDFD59D18C ();
// 0x000000A9 System.Void TypewriterEffect::Update()
extern void TypewriterEffect_Update_m6084CDED8728BD6FF5B899D62ED9AA16C4DA4E6D ();
// 0x000000AA System.Void TypewriterEffect::.ctor()
extern void TypewriterEffect__ctor_m089AEF3649474C1323B991956EA28A76A48A38D1 ();
// 0x000000AB System.Boolean UIButton::get_isEnabled()
extern void UIButton_get_isEnabled_m2FD4683F7A2D9B3C32F16788ABDA873A9EFEF7D4 ();
// 0x000000AC System.Void UIButton::set_isEnabled(System.Boolean)
extern void UIButton_set_isEnabled_mE662DB68D88B5E53E9B34000190C7DAC760444C4 ();
// 0x000000AD System.String UIButton::get_normalSprite()
extern void UIButton_get_normalSprite_m3C45AFB9B0997AB1451EFE23F68908BD8639E0A5 ();
// 0x000000AE System.Void UIButton::set_normalSprite(System.String)
extern void UIButton_set_normalSprite_m273701A19035BAFCB2C96FC37E1072D079C073B9 ();
// 0x000000AF UnityEngine.Sprite UIButton::get_normalSprite2D()
extern void UIButton_get_normalSprite2D_m535472D38151E04B292629B04E2954D7814D99F7 ();
// 0x000000B0 System.Void UIButton::set_normalSprite2D(UnityEngine.Sprite)
extern void UIButton_set_normalSprite2D_m35BB2077977AC84EE2B605778AA0959F1B00A40C ();
// 0x000000B1 System.Void UIButton::OnInit()
extern void UIButton_OnInit_m6E5F2C43371BA6A7A38CFA01927FAA8ACAD7F35F ();
// 0x000000B2 System.Void UIButton::OnEnable()
extern void UIButton_OnEnable_mE00AD702E2526B9F40365EF0085F58BFAD02117E ();
// 0x000000B3 System.Void UIButton::OnDragOver()
extern void UIButton_OnDragOver_m618DC0180DD5E34585384469B0C044DB4CCB6DE5 ();
// 0x000000B4 System.Void UIButton::OnDragOut()
extern void UIButton_OnDragOut_mD31DB94A7777AED7E5AE6BBF766D640CB918627A ();
// 0x000000B5 System.Void UIButton::OnClick()
extern void UIButton_OnClick_m746B0FBF08CD2A1766CBF99E4B9C938A204334B2 ();
// 0x000000B6 System.Void UIButton::SetState(UIButtonColor_State,System.Boolean)
extern void UIButton_SetState_m1CEE62E7179C5F5CACB4FCFCCBF95025590EBA58 ();
// 0x000000B7 System.Void UIButton::SetSprite(System.String)
extern void UIButton_SetSprite_m36E8BBE78C879C75B3E8A2998EBD5AA1D4D89D2E ();
// 0x000000B8 System.Void UIButton::SetSprite(UnityEngine.Sprite)
extern void UIButton_SetSprite_mDA23A834E7F63528027A1003682AC554F17C68CD ();
// 0x000000B9 System.Void UIButton::.ctor()
extern void UIButton__ctor_m4A0F43C37DC2224AB2BC2762ABA286C62BC9D153 ();
// 0x000000BA System.Void UIButtonActivate::OnClick()
extern void UIButtonActivate_OnClick_mE3187B7834A657827F7208994138D1FD602D2E01 ();
// 0x000000BB System.Void UIButtonActivate::.ctor()
extern void UIButtonActivate__ctor_m2431A96B4B37B5FE049A74CF7DAD6BC12DA502D5 ();
// 0x000000BC UIButtonColor_State UIButtonColor::get_state()
extern void UIButtonColor_get_state_mB3651F93248BA070FA1821134AE823B0883D7619 ();
// 0x000000BD System.Void UIButtonColor::set_state(UIButtonColor_State)
extern void UIButtonColor_set_state_m3828E9F4B0866A761C8A501FD82D3B3B071C23D4 ();
// 0x000000BE UnityEngine.Color UIButtonColor::get_defaultColor()
extern void UIButtonColor_get_defaultColor_mEA5A7C42A0838BDD46BD6DCA6F15F29277B29C07 ();
// 0x000000BF System.Void UIButtonColor::set_defaultColor(UnityEngine.Color)
extern void UIButtonColor_set_defaultColor_m7088FFC1DD176E70B501BAB7E9ECEE39DE37EFA1 ();
// 0x000000C0 System.Boolean UIButtonColor::get_isEnabled()
extern void UIButtonColor_get_isEnabled_m16C957509CE78C76C19F666529EB5636E16F88D2 ();
// 0x000000C1 System.Void UIButtonColor::set_isEnabled(System.Boolean)
extern void UIButtonColor_set_isEnabled_m878A12A0297E4D440BB8B3169DE5A843249D362F ();
// 0x000000C2 System.Void UIButtonColor::ResetDefaultColor()
extern void UIButtonColor_ResetDefaultColor_mAC5E5E1A3014392857EF6707CDAC257843484DC8 ();
// 0x000000C3 System.Void UIButtonColor::CacheDefaultColor()
extern void UIButtonColor_CacheDefaultColor_mAFD95B3C8997A8AD6FD538727EAB1D1BF7241C9B ();
// 0x000000C4 System.Void UIButtonColor::Start()
extern void UIButtonColor_Start_m20D055773BB3381859B9B7482EDFDDB65EFD5A29 ();
// 0x000000C5 System.Void UIButtonColor::OnInit()
extern void UIButtonColor_OnInit_m83DDB1F906298BBD5881E4AFE40A72A78B2175A7 ();
// 0x000000C6 System.Void UIButtonColor::OnEnable()
extern void UIButtonColor_OnEnable_m8E7643BA61DA89F819DDD84A44E9A7FDADA129CC ();
// 0x000000C7 System.Void UIButtonColor::OnDisable()
extern void UIButtonColor_OnDisable_mD3ED869565E278ACE08DF48D0BEC3E7CCD0737EF ();
// 0x000000C8 System.Void UIButtonColor::OnHover(System.Boolean)
extern void UIButtonColor_OnHover_m0DD8436B10509F870371B55B675705E8F0DE9F9B ();
// 0x000000C9 System.Void UIButtonColor::OnPress(System.Boolean)
extern void UIButtonColor_OnPress_mABF30CB6E52447D4AC0D70CDFD2A7672D0C9627F ();
// 0x000000CA System.Void UIButtonColor::OnDragOver()
extern void UIButtonColor_OnDragOver_m1965D57809CF7E7289152497C19C328C0DDB7548 ();
// 0x000000CB System.Void UIButtonColor::OnDragOut()
extern void UIButtonColor_OnDragOut_m9B567FA50C7750C94BAD1B2F00D9E3C3CD6FBF92 ();
// 0x000000CC System.Void UIButtonColor::SetState(UIButtonColor_State,System.Boolean)
extern void UIButtonColor_SetState_mF83DE6B610A0256276DEF3B5BFCF7EFF186061F3 ();
// 0x000000CD System.Void UIButtonColor::UpdateColor(System.Boolean)
extern void UIButtonColor_UpdateColor_m535110FC72BADD526DB3A331D807873D7879A310 ();
// 0x000000CE System.Void UIButtonColor::.ctor()
extern void UIButtonColor__ctor_m6E3A936A8B53EDA2A0D6BFD284F0199606BA4908 ();
// 0x000000CF System.Void UIButtonKeys::OnEnable()
extern void UIButtonKeys_OnEnable_m17C90D674890A41BBF6E83C3032BBAFC1CE539F9 ();
// 0x000000D0 System.Void UIButtonKeys::Upgrade()
extern void UIButtonKeys_Upgrade_m33AB6F795B993DF91DF32BEDCF6E272E36E9417D ();
// 0x000000D1 System.Void UIButtonKeys::.ctor()
extern void UIButtonKeys__ctor_m685CF371629C9012C2B021E28F6D57916247C075 ();
// 0x000000D2 System.Void UIButtonMessage::Start()
extern void UIButtonMessage_Start_mDF22F5BD1742161BE235AC386FDBB69CE43B20FD ();
// 0x000000D3 System.Void UIButtonMessage::OnEnable()
extern void UIButtonMessage_OnEnable_m1DDCB7253D6E6079474F1457A4645BD05049440B ();
// 0x000000D4 System.Void UIButtonMessage::OnHover(System.Boolean)
extern void UIButtonMessage_OnHover_m81033B505E5848850764D01E10CD7CB3AE42EA05 ();
// 0x000000D5 System.Void UIButtonMessage::OnPress(System.Boolean)
extern void UIButtonMessage_OnPress_mEEB6F5D016E2DDC8CEBC9597548C29F100AF08FE ();
// 0x000000D6 System.Void UIButtonMessage::OnSelect(System.Boolean)
extern void UIButtonMessage_OnSelect_mA36D45554ADF2B3FF34C1C93D45AD1405A65855A ();
// 0x000000D7 System.Void UIButtonMessage::OnClick()
extern void UIButtonMessage_OnClick_mD58CEDF275E0F3DDC7BC26B189647B8950AD951A ();
// 0x000000D8 System.Void UIButtonMessage::OnDoubleClick()
extern void UIButtonMessage_OnDoubleClick_m096113586EB46039FBFA09C70CC67EB455A843CD ();
// 0x000000D9 System.Void UIButtonMessage::Send()
extern void UIButtonMessage_Send_mF7FE4BEA344C5D44DDB4BBD6EB09D42A019C1A88 ();
// 0x000000DA System.Void UIButtonMessage::.ctor()
extern void UIButtonMessage__ctor_mF10D96603C7FBB1D663903AD1B20609E0D3BFFC1 ();
// 0x000000DB System.Void UIButtonOffset::Start()
extern void UIButtonOffset_Start_mDFBE32D11820643E0B82524383C02F53AEAD8970 ();
// 0x000000DC System.Void UIButtonOffset::OnEnable()
extern void UIButtonOffset_OnEnable_m3092801FE1B4F76042C9675F03396F041B123C93 ();
// 0x000000DD System.Void UIButtonOffset::OnDisable()
extern void UIButtonOffset_OnDisable_m7D9253A01CAFC9546CDA5D020ECE5F9F4CD92048 ();
// 0x000000DE System.Void UIButtonOffset::OnPress(System.Boolean)
extern void UIButtonOffset_OnPress_m91DD6DE47B3434E870C3945F24CBBA313E6AB870 ();
// 0x000000DF System.Void UIButtonOffset::OnHover(System.Boolean)
extern void UIButtonOffset_OnHover_mDBBE95D597887E6A8D25554957CC64EAE8273C03 ();
// 0x000000E0 System.Void UIButtonOffset::OnDragOver()
extern void UIButtonOffset_OnDragOver_m7A617E029D09FB0897A326BF8877BED9C226E30D ();
// 0x000000E1 System.Void UIButtonOffset::OnDragOut()
extern void UIButtonOffset_OnDragOut_mA5913462E172BA63DE6B91458FF99D29DF08D38B ();
// 0x000000E2 System.Void UIButtonOffset::OnSelect(System.Boolean)
extern void UIButtonOffset_OnSelect_mA469F17DEF1120EACABD7334D5D02CFDFEAAF660 ();
// 0x000000E3 System.Void UIButtonOffset::.ctor()
extern void UIButtonOffset__ctor_mE752B40BD3A400AB9AFFF7EE2116D1DA8745547A ();
// 0x000000E4 System.Void UIButtonRotation::Start()
extern void UIButtonRotation_Start_m6F29B40D5D1725CBE08138D04DAD47E369088802 ();
// 0x000000E5 System.Void UIButtonRotation::OnEnable()
extern void UIButtonRotation_OnEnable_mBF8EE246CDA2FF6DB3A7870C159EBA972C893862 ();
// 0x000000E6 System.Void UIButtonRotation::OnDisable()
extern void UIButtonRotation_OnDisable_m1E9418C958E284454674025BD4EB881FBAA197E3 ();
// 0x000000E7 System.Void UIButtonRotation::OnPress(System.Boolean)
extern void UIButtonRotation_OnPress_m803095BFD4365B62AA559A18B882A14FD6765EEE ();
// 0x000000E8 System.Void UIButtonRotation::OnHover(System.Boolean)
extern void UIButtonRotation_OnHover_mF7CF948826EFA7F857CE7D399149A4B104F7A963 ();
// 0x000000E9 System.Void UIButtonRotation::OnSelect(System.Boolean)
extern void UIButtonRotation_OnSelect_mF85D4B056F10529A6A33F6E7030C143EF0A1B87F ();
// 0x000000EA System.Void UIButtonRotation::.ctor()
extern void UIButtonRotation__ctor_mB962AC4EB462AD10C56531EFB77816A1212672F6 ();
// 0x000000EB System.Void UIButtonScale::Start()
extern void UIButtonScale_Start_m36D879E62C1FE5C6DE04E3EF2220AA633A47CD3D ();
// 0x000000EC System.Void UIButtonScale::OnEnable()
extern void UIButtonScale_OnEnable_m33E03B7D9F4E1EF8665F6874AC3FA76D5337651B ();
// 0x000000ED System.Void UIButtonScale::OnDisable()
extern void UIButtonScale_OnDisable_mF6D957FB5764B99A819FAA9C1414CC429C249143 ();
// 0x000000EE System.Void UIButtonScale::OnPress(System.Boolean)
extern void UIButtonScale_OnPress_m00B00D9CC5347A11681A0CFC59A6AEA3A7A3F7F9 ();
// 0x000000EF System.Void UIButtonScale::OnHover(System.Boolean)
extern void UIButtonScale_OnHover_mA915CE55994AF1D8A3590BE45B7A01F5B3E72983 ();
// 0x000000F0 System.Void UIButtonScale::OnSelect(System.Boolean)
extern void UIButtonScale_OnSelect_m9AC99C68F49BB6BFB7007CEAAA101554639DFA43 ();
// 0x000000F1 System.Void UIButtonScale::.ctor()
extern void UIButtonScale__ctor_m6CAEC36DE2F3FD0126712E5995B30B19C3674928 ();
// 0x000000F2 UnityEngine.GameObject UICenterOnChild::get_centeredObject()
extern void UICenterOnChild_get_centeredObject_m92EF5A688C6DFF9D5D2A74E2B429FF45FDF34C8A ();
// 0x000000F3 System.Void UICenterOnChild::Start()
extern void UICenterOnChild_Start_mE8706D1144B2BF50DF9422F8C2C9CC8C78260FCE ();
// 0x000000F4 System.Void UICenterOnChild::OnEnable()
extern void UICenterOnChild_OnEnable_m1D49EE117F49FEDCEB61DB0026D8F5653F34EA63 ();
// 0x000000F5 System.Void UICenterOnChild::OnDisable()
extern void UICenterOnChild_OnDisable_m199DF6AF8CD243CB25EEFE6DE55187F21E9F3637 ();
// 0x000000F6 System.Void UICenterOnChild::OnDragFinished()
extern void UICenterOnChild_OnDragFinished_m8D041F88EDE902CF5D9EE5610CDE6231237428F1 ();
// 0x000000F7 System.Void UICenterOnChild::OnValidate()
extern void UICenterOnChild_OnValidate_m124C1647A3B6FE7D023A93FBF1E10C7E9892DB8E ();
// 0x000000F8 System.Void UICenterOnChild::Recenter()
extern void UICenterOnChild_Recenter_m45576A58178C385C2FAA4DC834B6772DFFBB83EC ();
// 0x000000F9 System.Void UICenterOnChild::CenterOn(UnityEngine.Transform,UnityEngine.Vector3)
extern void UICenterOnChild_CenterOn_mC44AE09332F4A7F84D6859E4E74BBE4E8E7EE115 ();
// 0x000000FA System.Void UICenterOnChild::CenterOn(UnityEngine.Transform)
extern void UICenterOnChild_CenterOn_m177289AF868A04AA055AC40B6E29D1E2ECB07D7D ();
// 0x000000FB System.Void UICenterOnChild::.ctor()
extern void UICenterOnChild__ctor_m810084F508F62E3CF933B72CE2AFB508749CD742 ();
// 0x000000FC System.Void UICenterOnClick::OnClick()
extern void UICenterOnClick_OnClick_m51CC1D8EF3C4BE8E018A622E2ED7B21356783FC8 ();
// 0x000000FD System.Void UICenterOnClick::.ctor()
extern void UICenterOnClick__ctor_m697B670D4BE91E2987C372EEC4BBE57EB297A02D ();
// 0x000000FE System.Void UIDragCamera::Awake()
extern void UIDragCamera_Awake_m95ADD3B90A53A782F613A44A2656AF30A3DCB42A ();
// 0x000000FF System.Void UIDragCamera::OnPress(System.Boolean)
extern void UIDragCamera_OnPress_mF402CBCBCBD736FD68A81B3540CB7006A58CFF87 ();
// 0x00000100 System.Void UIDragCamera::OnDrag(UnityEngine.Vector2)
extern void UIDragCamera_OnDrag_m4BAD847B3118436329C543C4969D60A053B94345 ();
// 0x00000101 System.Void UIDragCamera::OnScroll(System.Single)
extern void UIDragCamera_OnScroll_mDF2D5C560FFD4DF6AA5A8FA29C0C137D7538FE6B ();
// 0x00000102 System.Void UIDragCamera::.ctor()
extern void UIDragCamera__ctor_m36303D8AD1BF425A1A7BE58620F3206336EA6326 ();
// 0x00000103 System.Void UIDragDropContainer::Start()
extern void UIDragDropContainer_Start_m5453BC269F8E4FF6682E272EA9838BFD5A117AC9 ();
// 0x00000104 System.Void UIDragDropContainer::.ctor()
extern void UIDragDropContainer__ctor_m2E8A0DD94AE9D7F9505DC463B9CA439BBDCACE4E ();
// 0x00000105 System.Boolean UIDragDropItem::IsDragged(UnityEngine.GameObject)
extern void UIDragDropItem_IsDragged_mE1E78A0B2A397AF142EF1373083FB6831CE7B225 ();
// 0x00000106 System.Void UIDragDropItem::Awake()
extern void UIDragDropItem_Awake_mCDAEF910EBF0A243F6E813948E2A4E63895FD440 ();
// 0x00000107 System.Void UIDragDropItem::OnEnable()
extern void UIDragDropItem_OnEnable_mAFB3E46F47A04E77E1554926C764BCABE9F3398D ();
// 0x00000108 System.Void UIDragDropItem::OnDisable()
extern void UIDragDropItem_OnDisable_mF20A65223E8B8137292EF81AAD27ED862DA5475D ();
// 0x00000109 System.Void UIDragDropItem::Start()
extern void UIDragDropItem_Start_m33D099E2C7A4527AE8178F1148CDDDE828508165 ();
// 0x0000010A System.Void UIDragDropItem::OnPress(System.Boolean)
extern void UIDragDropItem_OnPress_mDEA5BFE97C7BAFB93A488A096737DC1E2B91368E ();
// 0x0000010B System.Void UIDragDropItem::OnClick()
extern void UIDragDropItem_OnClick_m9040BCABD2AD9B39D28D05F7DE0126DDD3E76DB7 ();
// 0x0000010C System.Void UIDragDropItem::OnGlobalPress(UnityEngine.GameObject,System.Boolean)
extern void UIDragDropItem_OnGlobalPress_m0720D23935D08E98EAD778051B6E9A55E702DA57 ();
// 0x0000010D System.Void UIDragDropItem::OnGlobalClick(UnityEngine.GameObject)
extern void UIDragDropItem_OnGlobalClick_mEAFD5B7A71E29059589AE76F24E4845720F580F9 ();
// 0x0000010E System.Void UIDragDropItem::Update()
extern void UIDragDropItem_Update_m53789A239B178DE89DBF4332A6BAA8A77DCF5F72 ();
// 0x0000010F System.Void UIDragDropItem::OnDragStart()
extern void UIDragDropItem_OnDragStart_mD37DB37FB57E96BA5001EC8DB1ACE2BC982A0D6D ();
// 0x00000110 UIDragDropItem UIDragDropItem::StartDragging()
extern void UIDragDropItem_StartDragging_m42BB2CE0982A32CD7283C5D4BF56487030DA577F ();
// 0x00000111 System.Void UIDragDropItem::OnClone(UnityEngine.GameObject)
extern void UIDragDropItem_OnClone_m85C6B32F66A39D8FF6F4F16BE2B17A69E2E711AF ();
// 0x00000112 System.Void UIDragDropItem::OnDrag(UnityEngine.Vector2)
extern void UIDragDropItem_OnDrag_m4AF72B38E3A548B52DAC86E787FFB9B3C5D017DF ();
// 0x00000113 System.Void UIDragDropItem::OnDragEnd()
extern void UIDragDropItem_OnDragEnd_m6FB99544DA12345D0983356D028857833B8ED487 ();
// 0x00000114 System.Void UIDragDropItem::StopDragging(UnityEngine.GameObject)
extern void UIDragDropItem_StopDragging_m6A46A851FF35A4FF187FA15FB9A815A2F4134795 ();
// 0x00000115 System.Void UIDragDropItem::OnDragDropStart()
extern void UIDragDropItem_OnDragDropStart_m514076B707C1A000E511C1DB8A3AFDA04CC3A2C5 ();
// 0x00000116 System.Void UIDragDropItem::OnDragDropMove(UnityEngine.Vector2)
extern void UIDragDropItem_OnDragDropMove_mDD2EDA276D367D7D2AF2DF6E85BD54BD61032601 ();
// 0x00000117 System.Void UIDragDropItem::OnDragDropRelease(UnityEngine.GameObject)
extern void UIDragDropItem_OnDragDropRelease_m1A8A2EB71DA9BCA028E3C17E54A5F524ACC3F0F3 ();
// 0x00000118 System.Void UIDragDropItem::DestroySelf()
extern void UIDragDropItem_DestroySelf_m42EBE971183894A7EF733EC61B9789BD0DBACCD2 ();
// 0x00000119 System.Void UIDragDropItem::OnDragDropEnd(UnityEngine.GameObject)
extern void UIDragDropItem_OnDragDropEnd_m60B8E30891F91817DAFCDC224F17C168ED44771E ();
// 0x0000011A System.Void UIDragDropItem::EnableDragScrollView()
extern void UIDragDropItem_EnableDragScrollView_m7A8710D5021A4C47A1DDE247B2BF602B82812FEA ();
// 0x0000011B System.Void UIDragDropItem::OnApplicationFocus(System.Boolean)
extern void UIDragDropItem_OnApplicationFocus_mC8E8D35B42C2427B220B1F169CE741FBF5717CE7 ();
// 0x0000011C System.Void UIDragDropItem::.ctor()
extern void UIDragDropItem__ctor_mCD97905B117E6D8FEC9D6D64C067F0E777D6F56C ();
// 0x0000011D System.Void UIDragDropItem::.cctor()
extern void UIDragDropItem__cctor_m1BD9A105F3EC2EDAA34EBBCA86BBE2A924CEA80A ();
// 0x0000011E System.Void UIDragDropRoot::OnEnable()
extern void UIDragDropRoot_OnEnable_mCB2E357733A98E0723ED04AC9ACFDBF82C5A5060 ();
// 0x0000011F System.Void UIDragDropRoot::OnDisable()
extern void UIDragDropRoot_OnDisable_mF81CC1DAAE2FF0D46FF209AAD17AB170341C30E5 ();
// 0x00000120 System.Void UIDragDropRoot::.ctor()
extern void UIDragDropRoot__ctor_m0C290E6E3C6F2BA3F0501B8EC20520F1080D5929 ();
// 0x00000121 UnityEngine.Vector3 UIDragObject::get_dragMovement()
extern void UIDragObject_get_dragMovement_mCA1FCD436283E9972AD6104DCB4233AF1CA59C6F ();
// 0x00000122 System.Void UIDragObject::set_dragMovement(UnityEngine.Vector3)
extern void UIDragObject_set_dragMovement_m1E4F6ADEDE33B18E0E6227B6DA8D735106C52E06 ();
// 0x00000123 System.Void UIDragObject::OnEnable()
extern void UIDragObject_OnEnable_m8C491A36D8D8D61AC2375D014C4D56843FD7DDDF ();
// 0x00000124 System.Void UIDragObject::OnDisable()
extern void UIDragObject_OnDisable_m693588068171A0E5AFD5E8F9ED1DB82E993A6661 ();
// 0x00000125 System.Void UIDragObject::FindPanel()
extern void UIDragObject_FindPanel_m19635BCEE5E51CF6BEE9DD060CCDABE94F7D56B4 ();
// 0x00000126 System.Void UIDragObject::UpdateBounds()
extern void UIDragObject_UpdateBounds_m907D191F4938DCD09BD58489BA63BE8B073146B4 ();
// 0x00000127 System.Void UIDragObject::OnPress(System.Boolean)
extern void UIDragObject_OnPress_m045D6CDC167D7A6AACFFDD2625E73B92A74A2BA2 ();
// 0x00000128 System.Void UIDragObject::OnDrag(UnityEngine.Vector2)
extern void UIDragObject_OnDrag_m69D848FA076E42F5EB81B522164AF94605FC6BFA ();
// 0x00000129 System.Void UIDragObject::Move(UnityEngine.Vector3)
extern void UIDragObject_Move_m8E7037F042BFBB1660AE551DEB59734FEA350827 ();
// 0x0000012A System.Void UIDragObject::LateUpdate()
extern void UIDragObject_LateUpdate_mA1AA5422382AF68FFA6FC7F14EA37682C81C4A23 ();
// 0x0000012B System.Void UIDragObject::CancelMovement()
extern void UIDragObject_CancelMovement_mBA59993B6A2DDCDFF562772E8DF963A8DD8182C9 ();
// 0x0000012C System.Void UIDragObject::CancelSpring()
extern void UIDragObject_CancelSpring_mF00B5F50C39C4A081AA4D3FAEC8AAFE8A9C92C0C ();
// 0x0000012D System.Void UIDragObject::OnScroll(System.Single)
extern void UIDragObject_OnScroll_m92C12F1EBEFCB447815DDB97A9BC9C3145558943 ();
// 0x0000012E System.Void UIDragObject::.ctor()
extern void UIDragObject__ctor_m193EC1DDAA69B9C5E3013FFAEAE10AC5B568268E ();
// 0x0000012F System.Void UIDragResize::OnDragStart()
extern void UIDragResize_OnDragStart_mF818E2D19872230AED998A1B718DABAE4E6D2AD9 ();
// 0x00000130 System.Void UIDragResize::OnDrag(UnityEngine.Vector2)
extern void UIDragResize_OnDrag_m2EA378CDD9A209CE12E884061A98FEB0C9197D18 ();
// 0x00000131 System.Void UIDragResize::OnDragEnd()
extern void UIDragResize_OnDragEnd_mFF975FCF3C136FC43E8F9F49609814F7D7C32507 ();
// 0x00000132 System.Void UIDragResize::.ctor()
extern void UIDragResize__ctor_mFE27213B79114D72224661F75FA435425F94527C ();
// 0x00000133 System.Void UIDragScrollView::OnEnable()
extern void UIDragScrollView_OnEnable_m6C3469996BF75B08C0FC21A35635D2F71581F2DE ();
// 0x00000134 System.Void UIDragScrollView::Start()
extern void UIDragScrollView_Start_m51F1E990719813E0E24F6A672AA29F7FBC9516DF ();
// 0x00000135 System.Void UIDragScrollView::FindScrollView()
extern void UIDragScrollView_FindScrollView_m749EF4217ADDBEA6A92494F49EC2E1013DDD33B4 ();
// 0x00000136 System.Void UIDragScrollView::OnDisable()
extern void UIDragScrollView_OnDisable_mD0AABC43FC6B20F5D092D6D21364EF2217B71335 ();
// 0x00000137 System.Void UIDragScrollView::OnPress(System.Boolean)
extern void UIDragScrollView_OnPress_mAAF609E5262143CD168D6A097863EBABB539C1A2 ();
// 0x00000138 System.Void UIDragScrollView::OnDrag(UnityEngine.Vector2)
extern void UIDragScrollView_OnDrag_m4D1ABF2963D4C0A2854D38D6E8828665DAA98FAB ();
// 0x00000139 System.Void UIDragScrollView::OnScroll(System.Single)
extern void UIDragScrollView_OnScroll_m8E6A33985153C6A43BB2706170E395FCFA192617 ();
// 0x0000013A System.Void UIDragScrollView::OnPan(UnityEngine.Vector2)
extern void UIDragScrollView_OnPan_mCB6964497DB8037216F02254A7AC372765DDDA5D ();
// 0x0000013B System.Void UIDragScrollView::.ctor()
extern void UIDragScrollView__ctor_m9F05CE583A0D9F656CFA79B81833E4230E88C4A8 ();
// 0x0000013C UnityEngine.Vector2 UIDraggableCamera::get_currentMomentum()
extern void UIDraggableCamera_get_currentMomentum_mF30D9D816EB25DA296B660FAAEBFEE958ADDBA8E ();
// 0x0000013D System.Void UIDraggableCamera::set_currentMomentum(UnityEngine.Vector2)
extern void UIDraggableCamera_set_currentMomentum_m5FDF2C53A844B9A699B1660D8E60919CFADB8E9E ();
// 0x0000013E System.Void UIDraggableCamera::Start()
extern void UIDraggableCamera_Start_m6F68B8F67C8C7586905962A5EFDC7AC4225F7E47 ();
// 0x0000013F UnityEngine.Vector3 UIDraggableCamera::CalculateConstrainOffset()
extern void UIDraggableCamera_CalculateConstrainOffset_m4C828D5602CC2066922C5E3A2B60914BB2F507A8 ();
// 0x00000140 System.Boolean UIDraggableCamera::ConstrainToBounds(System.Boolean)
extern void UIDraggableCamera_ConstrainToBounds_m8525F6CAD58BF5AE230B20DFCB2953A7A665011A ();
// 0x00000141 System.Void UIDraggableCamera::Press(System.Boolean)
extern void UIDraggableCamera_Press_mA1AFD4D1C0235A2E795741D64F56A896BBE5A426 ();
// 0x00000142 System.Void UIDraggableCamera::Drag(UnityEngine.Vector2)
extern void UIDraggableCamera_Drag_mF3798CDE126AFBDFC2A6CFD75AC48A9193B65657 ();
// 0x00000143 System.Void UIDraggableCamera::Scroll(System.Single)
extern void UIDraggableCamera_Scroll_mE5C69C7C65BC569901A3ED876054E409BB0378C2 ();
// 0x00000144 System.Void UIDraggableCamera::Update()
extern void UIDraggableCamera_Update_mBF387AFB3953557D0EADFD615019C84C96B4AEEF ();
// 0x00000145 System.Void UIDraggableCamera::.ctor()
extern void UIDraggableCamera__ctor_m0E19665CE642E764FC54144E0A6A2EE2FF84F8BD ();
// 0x00000146 System.Boolean UIEventTrigger::get_isColliderEnabled()
extern void UIEventTrigger_get_isColliderEnabled_m23DB8A580D131F0451E62F309E9870E51D9663D5 ();
// 0x00000147 System.Void UIEventTrigger::OnHover(System.Boolean)
extern void UIEventTrigger_OnHover_m463B3B1DA12F79C7F094E68C136C385EB408D1BA ();
// 0x00000148 System.Void UIEventTrigger::OnPress(System.Boolean)
extern void UIEventTrigger_OnPress_mCC225ADA0D55161ED99B1B313324DC7DDFA80F54 ();
// 0x00000149 System.Void UIEventTrigger::OnSelect(System.Boolean)
extern void UIEventTrigger_OnSelect_mE5CFF980D84B7BFF090A78D5D6608D3F7CD35A0C ();
// 0x0000014A System.Void UIEventTrigger::OnClick()
extern void UIEventTrigger_OnClick_mA5CCEE27F2C20989CDE8AD83ADCDB5EE9FCE2FF2 ();
// 0x0000014B System.Void UIEventTrigger::OnDoubleClick()
extern void UIEventTrigger_OnDoubleClick_m526DACAE338AAF593A7FE0C70A94291304FFE9DD ();
// 0x0000014C System.Void UIEventTrigger::OnDragStart()
extern void UIEventTrigger_OnDragStart_m8666A2F6982DA59A0098D3DE2D8BD32AADFA5E91 ();
// 0x0000014D System.Void UIEventTrigger::OnDragEnd()
extern void UIEventTrigger_OnDragEnd_m59DF96E13CF3D00EE54D9065D921636C5D5AC1B4 ();
// 0x0000014E System.Void UIEventTrigger::OnDragOver(UnityEngine.GameObject)
extern void UIEventTrigger_OnDragOver_mE6FAA2E310292B6EDDA183D2E4DB2CEC462127D8 ();
// 0x0000014F System.Void UIEventTrigger::OnDragOut(UnityEngine.GameObject)
extern void UIEventTrigger_OnDragOut_m77AFA2AB1F5CC006F95FEAEB287EDC4AF490A56B ();
// 0x00000150 System.Void UIEventTrigger::OnDrag(UnityEngine.Vector2)
extern void UIEventTrigger_OnDrag_m74D73947A3517E880ED7578D97F750131CC8E8AE ();
// 0x00000151 System.Void UIEventTrigger::.ctor()
extern void UIEventTrigger__ctor_m5DF2C3EF6CE4BC07880FAE6837C12A358EB23A38 ();
// 0x00000152 System.Void UIForwardEvents::OnHover(System.Boolean)
extern void UIForwardEvents_OnHover_m1C909C7194F715A9B1BA7076F937F2649204FDAA ();
// 0x00000153 System.Void UIForwardEvents::OnPress(System.Boolean)
extern void UIForwardEvents_OnPress_m87E7A0353B6DB1CC8005CA068F59E8DAF41E8E3B ();
// 0x00000154 System.Void UIForwardEvents::OnClick()
extern void UIForwardEvents_OnClick_m8CFC1DAF5B53F896662812DB1F675423856DE20C ();
// 0x00000155 System.Void UIForwardEvents::OnDoubleClick()
extern void UIForwardEvents_OnDoubleClick_m388D025345368BFDFA2EBD33926328D7BA36B3FF ();
// 0x00000156 System.Void UIForwardEvents::OnSelect(System.Boolean)
extern void UIForwardEvents_OnSelect_m5B5EDB7D032F1F78CEDA02AED8DA1737CF92DDE8 ();
// 0x00000157 System.Void UIForwardEvents::OnDrag(UnityEngine.Vector2)
extern void UIForwardEvents_OnDrag_mB2AF4038A62C555675E76C39112F2319B149BAEA ();
// 0x00000158 System.Void UIForwardEvents::OnDrop(UnityEngine.GameObject)
extern void UIForwardEvents_OnDrop_m0751CBC285C2C68C93AA8FC65FC4CE98ED09A91B ();
// 0x00000159 System.Void UIForwardEvents::OnSubmit()
extern void UIForwardEvents_OnSubmit_mD4ACF667A9D3A7F4022B9D1E1F2785AFBE593CC3 ();
// 0x0000015A System.Void UIForwardEvents::OnScroll(System.Single)
extern void UIForwardEvents_OnScroll_m3A4DEEEDF7794BB86BDA70D67A3607D5899FDD47 ();
// 0x0000015B System.Void UIForwardEvents::.ctor()
extern void UIForwardEvents__ctor_mA981BE30A04DAAF48FC05B3497E5249365112952 ();
// 0x0000015C System.Void UIGrid::set_repositionNow(System.Boolean)
extern void UIGrid_set_repositionNow_m1603D14191A19572B6A3E67B4B36136191B3EB5A ();
// 0x0000015D System.Collections.Generic.List`1<UnityEngine.Transform> UIGrid::GetChildList()
extern void UIGrid_GetChildList_mBF55B31A144A258BFCE0A41D4DC2FB8A6D9E41B3 ();
// 0x0000015E UnityEngine.Transform UIGrid::GetChild(System.Int32)
extern void UIGrid_GetChild_mDB3728401ACA24DD26F2E05470C856A98A5A0FB8 ();
// 0x0000015F System.Int32 UIGrid::GetIndex(UnityEngine.Transform)
extern void UIGrid_GetIndex_m304D1E3DACBE9D093902C5E80242CDA9AF836416 ();
// 0x00000160 System.Void UIGrid::AddChild(UnityEngine.Transform)
extern void UIGrid_AddChild_m97F2941FDA82984D2DF2FBACDB687A2B4FC7C8F5 ();
// 0x00000161 System.Void UIGrid::AddChild(UnityEngine.Transform,System.Boolean)
extern void UIGrid_AddChild_mC323E94BBFDE2C9D13D3B41573D4D4F7C2AFB1EE ();
// 0x00000162 System.Boolean UIGrid::RemoveChild(UnityEngine.Transform)
extern void UIGrid_RemoveChild_m3E986D2F0ADAF45C4EBEA0A703D566F327F2EED9 ();
// 0x00000163 System.Void UIGrid::Init()
extern void UIGrid_Init_mA3ACD66F71796BFA10DC122BA6028291EC8D2A15 ();
// 0x00000164 System.Void UIGrid::Start()
extern void UIGrid_Start_mCC4298198BC3B8C8DD837EE50F9132F6BFC7AFDD ();
// 0x00000165 System.Void UIGrid::Update()
extern void UIGrid_Update_m3BE0E55A2DDF97B6DF247EA0C8188901AA31138F ();
// 0x00000166 System.Void UIGrid::OnValidate()
extern void UIGrid_OnValidate_m415C190A86A6C78162050FCF51E2B4373E8CD9C1 ();
// 0x00000167 System.Int32 UIGrid::SortByName(UnityEngine.Transform,UnityEngine.Transform)
extern void UIGrid_SortByName_mBD267987C8F95E72644759FD9DE0620EC114A80A ();
// 0x00000168 System.Int32 UIGrid::SortHorizontal(UnityEngine.Transform,UnityEngine.Transform)
extern void UIGrid_SortHorizontal_mA4BF98B85E30F2254DBC3D3CB2B643D7BBC22A30 ();
// 0x00000169 System.Int32 UIGrid::SortVertical(UnityEngine.Transform,UnityEngine.Transform)
extern void UIGrid_SortVertical_m255D05FB979DE2367E3DF2F23ABDCB287C90800C ();
// 0x0000016A System.Void UIGrid::Sort(System.Collections.Generic.List`1<UnityEngine.Transform>)
extern void UIGrid_Sort_m1BBBAC6C5D2E9FDF6DBE2D48D322AA38B80856A6 ();
// 0x0000016B System.Void UIGrid::Reposition()
extern void UIGrid_Reposition_m1C13C5769255924877A5296C1DAC3B4AC4AD6D7C ();
// 0x0000016C System.Void UIGrid::ConstrainWithinPanel()
extern void UIGrid_ConstrainWithinPanel_m0038539A9EDCE670A8749B239E554326E490A66B ();
// 0x0000016D System.Void UIGrid::ResetPosition(System.Collections.Generic.List`1<UnityEngine.Transform>)
extern void UIGrid_ResetPosition_m32DFAA4A1BF7056C029013816B4281C36651D86F ();
// 0x0000016E System.Void UIGrid::.ctor()
extern void UIGrid__ctor_mDED7BFAAE41A54D80267902DCD4398D849E2E6CA ();
// 0x0000016F System.Boolean UIImageButton::get_isEnabled()
extern void UIImageButton_get_isEnabled_mBBD878BA79DE396D0DA9BC0E32D5E9D13268E1C6 ();
// 0x00000170 System.Void UIImageButton::set_isEnabled(System.Boolean)
extern void UIImageButton_set_isEnabled_m0C7FBFBEDD222E03211092784D1466E1A0CBFED8 ();
// 0x00000171 System.Void UIImageButton::OnEnable()
extern void UIImageButton_OnEnable_mF111BD167EF1033B4A03B17F51A1F100DD0C4413 ();
// 0x00000172 System.Void UIImageButton::OnValidate()
extern void UIImageButton_OnValidate_mEDE63BD019577A82276BFDCB1414AFB461113C02 ();
// 0x00000173 System.Void UIImageButton::UpdateImage()
extern void UIImageButton_UpdateImage_m3A472A2A805AF619671F5CB3FCEC3F8FB629FC7F ();
// 0x00000174 System.Void UIImageButton::OnHover(System.Boolean)
extern void UIImageButton_OnHover_m6CF694AA9B9175236C90F094F02A5908D00C03C7 ();
// 0x00000175 System.Void UIImageButton::OnPress(System.Boolean)
extern void UIImageButton_OnPress_m211D6BFE8734ADBA5BE0AF2F3C90A569CD11834C ();
// 0x00000176 System.Void UIImageButton::SetSprite(System.String)
extern void UIImageButton_SetSprite_m526B94C262EE51C1D9A34788788E6578666EF60C ();
// 0x00000177 System.Void UIImageButton::.ctor()
extern void UIImageButton__ctor_mEF98213CF7C8FD0FBA3C6484CAB4470967E51B8A ();
// 0x00000178 System.String UIKeyBinding::get_captionText()
extern void UIKeyBinding_get_captionText_m56B9C2FACA7424C4FD409F25C74E8E70D3144E0D ();
// 0x00000179 System.Boolean UIKeyBinding::IsBound(UnityEngine.KeyCode)
extern void UIKeyBinding_IsBound_mABC0B5C68F107C310412EBF2A77C69DF663917B6 ();
// 0x0000017A System.Void UIKeyBinding::OnEnable()
extern void UIKeyBinding_OnEnable_m5312CA135C64596E02E33022834D07CB414D1B30 ();
// 0x0000017B System.Void UIKeyBinding::OnDisable()
extern void UIKeyBinding_OnDisable_mAC878B7DE84BF7AA15A25AA456CF122EE2A21083 ();
// 0x0000017C System.Void UIKeyBinding::Start()
extern void UIKeyBinding_Start_mB0A66CA38CD9769B28BF081515B19295418A561B ();
// 0x0000017D System.Void UIKeyBinding::OnSubmit()
extern void UIKeyBinding_OnSubmit_mDE45D3D1F751CF98D9056E3D44460FD841C0675C ();
// 0x0000017E System.Boolean UIKeyBinding::IsModifierActive()
extern void UIKeyBinding_IsModifierActive_m13AE9B598C78B5CAA418367F91428438117D7AC0 ();
// 0x0000017F System.Boolean UIKeyBinding::IsModifierActive(UIKeyBinding_Modifier)
extern void UIKeyBinding_IsModifierActive_m154E18402BF0DBFB099BBB6C25F9BABEAD85426B ();
// 0x00000180 System.Void UIKeyBinding::Update()
extern void UIKeyBinding_Update_m2EC70818B5A3A0214E08AD5589A9010CAFD6E280 ();
// 0x00000181 System.Void UIKeyBinding::OnBindingPress(System.Boolean)
extern void UIKeyBinding_OnBindingPress_m64538DB62A864D1FD240CD6368A5C84CD15FDB93 ();
// 0x00000182 System.Void UIKeyBinding::OnBindingClick()
extern void UIKeyBinding_OnBindingClick_m283E30A69811B905EE00E8C50061936157A6B11C ();
// 0x00000183 System.String UIKeyBinding::ToString()
extern void UIKeyBinding_ToString_mA9FA9DD4425B9D3414D1C11AEE89D88895BBCE5F ();
// 0x00000184 System.String UIKeyBinding::GetString(UnityEngine.KeyCode,UIKeyBinding_Modifier)
extern void UIKeyBinding_GetString_m8FC3EA682C6E4ACFED2BF6827E9D1F2DA5E9F9E7 ();
// 0x00000185 System.Boolean UIKeyBinding::GetKeyCode(System.String,UnityEngine.KeyCode&,UIKeyBinding_Modifier&)
extern void UIKeyBinding_GetKeyCode_m2A2400ABE3FB5AF4B0953515272E5641C438E6BB ();
// 0x00000186 UIKeyBinding_Modifier UIKeyBinding::GetActiveModifier()
extern void UIKeyBinding_GetActiveModifier_mDFABC18138E6120AA25771C4B5130BDE96E13497 ();
// 0x00000187 System.Void UIKeyBinding::.ctor()
extern void UIKeyBinding__ctor_m1199987FEE9D64A3F1D0835F67BC826FFC1A78EF ();
// 0x00000188 System.Void UIKeyBinding::.cctor()
extern void UIKeyBinding__cctor_m9A833F0FF39867E9D7C248AB69FEC9747600C2CB ();
// 0x00000189 UIKeyNavigation UIKeyNavigation::get_current()
extern void UIKeyNavigation_get_current_m65C7A3877CABAA1870E386E8FBAF6812EC8232D9 ();
// 0x0000018A System.Boolean UIKeyNavigation::get_isColliderEnabled()
extern void UIKeyNavigation_get_isColliderEnabled_m108E320475DDE432668DD1FD2CE715F5DD9DD92E ();
// 0x0000018B System.Void UIKeyNavigation::OnEnable()
extern void UIKeyNavigation_OnEnable_m1172E73D7617D4576F6926810DBF81A2FFED976D ();
// 0x0000018C System.Void UIKeyNavigation::Start()
extern void UIKeyNavigation_Start_m0D774F0CB0D8C8206B978D1A901F69C480B02C31 ();
// 0x0000018D System.Void UIKeyNavigation::OnDisable()
extern void UIKeyNavigation_OnDisable_m07813944DF55C4897672BEBBC4E352EF0146C7FB ();
// 0x0000018E System.Boolean UIKeyNavigation::IsActive(UnityEngine.GameObject)
extern void UIKeyNavigation_IsActive_m2AD4400A6E9E5B6D1E7CCEF27C73A6CC66A0D0D0 ();
// 0x0000018F UnityEngine.GameObject UIKeyNavigation::GetLeft()
extern void UIKeyNavigation_GetLeft_mF88E89F8202975E34B29C955F4C202660739D307 ();
// 0x00000190 UnityEngine.GameObject UIKeyNavigation::GetRight()
extern void UIKeyNavigation_GetRight_mE2CA0A37F544EB81D82E67CDBDB91573C9FF9F53 ();
// 0x00000191 UnityEngine.GameObject UIKeyNavigation::GetUp()
extern void UIKeyNavigation_GetUp_mD86582EEC1D594339B22B02FA8DF282A71700023 ();
// 0x00000192 UnityEngine.GameObject UIKeyNavigation::GetDown()
extern void UIKeyNavigation_GetDown_m0435F8557C9DFBB6BE5B4E720F3E345FCD835440 ();
// 0x00000193 UnityEngine.GameObject UIKeyNavigation::Get(UnityEngine.Vector3,System.Single,System.Single)
extern void UIKeyNavigation_Get_m0D8A264AED2B206D279F396BDBA0F05505DEA03F ();
// 0x00000194 UnityEngine.Vector3 UIKeyNavigation::GetCenter(UnityEngine.GameObject)
extern void UIKeyNavigation_GetCenter_m536C911EB7FD8D0A1D106337F4DD4978450D7869 ();
// 0x00000195 System.Void UIKeyNavigation::OnNavigate(UnityEngine.KeyCode)
extern void UIKeyNavigation_OnNavigate_m72C501354CC0C5F15ADE87F01E93A9E178EA5CE8 ();
// 0x00000196 System.Void UIKeyNavigation::OnKey(UnityEngine.KeyCode)
extern void UIKeyNavigation_OnKey_mAEC0DED4792AA8974C8AB4FBC48CDADA23755417 ();
// 0x00000197 System.Void UIKeyNavigation::OnClick()
extern void UIKeyNavigation_OnClick_m1DFEFA51E2912B60ABE698416E309449E265E654 ();
// 0x00000198 System.Void UIKeyNavigation::.ctor()
extern void UIKeyNavigation__ctor_mCA7ED4685797DE931EC550E459A9F239C4D4C9BE ();
// 0x00000199 System.Void UIKeyNavigation::.cctor()
extern void UIKeyNavigation__cctor_mB4571D5DB13FE2BC4AD60983D893834773F5D43E ();
// 0x0000019A System.Boolean UIPlayAnimation::get_dualState()
extern void UIPlayAnimation_get_dualState_m83DDC90751A08E92E1486BC34EF0343D9AFAA187 ();
// 0x0000019B System.Void UIPlayAnimation::Awake()
extern void UIPlayAnimation_Awake_m9564666A1A68750944C6512D0B3E28085D819EC3 ();
// 0x0000019C System.Void UIPlayAnimation::Start()
extern void UIPlayAnimation_Start_mED36B8D375A1072918BC5AF46CBA7F54CC2B0FAB ();
// 0x0000019D System.Void UIPlayAnimation::OnEnable()
extern void UIPlayAnimation_OnEnable_m01ADAD12440E02FCDC144DE6C2F37C4F989F0EBA ();
// 0x0000019E System.Void UIPlayAnimation::OnDisable()
extern void UIPlayAnimation_OnDisable_m307B66FA29FEFDC0E81C8A5EC4604E3C01DA7751 ();
// 0x0000019F System.Void UIPlayAnimation::OnHover(System.Boolean)
extern void UIPlayAnimation_OnHover_m9919217BF7107602DCB58803E3B510F75B2A68CA ();
// 0x000001A0 System.Void UIPlayAnimation::OnPress(System.Boolean)
extern void UIPlayAnimation_OnPress_m0F6FB02D193B2C1A9EFE3B2D348828690105F04A ();
// 0x000001A1 System.Void UIPlayAnimation::OnClick()
extern void UIPlayAnimation_OnClick_m31DA948FE943384BACC7B5AE95E8D5AE75941828 ();
// 0x000001A2 System.Void UIPlayAnimation::OnDoubleClick()
extern void UIPlayAnimation_OnDoubleClick_mC94A00C839EFD501D9755BFE13DC68694AB2FA9F ();
// 0x000001A3 System.Void UIPlayAnimation::OnSelect(System.Boolean)
extern void UIPlayAnimation_OnSelect_m93BA74A034306C3F1F7A294B27174C3A67C7E563 ();
// 0x000001A4 System.Void UIPlayAnimation::OnToggle()
extern void UIPlayAnimation_OnToggle_mB8D4D03A1E25DC269A279019F6B789033CF60C29 ();
// 0x000001A5 System.Void UIPlayAnimation::OnDragOver()
extern void UIPlayAnimation_OnDragOver_m14F14F8403F548DFEED277F35349D45A2C6543ED ();
// 0x000001A6 System.Void UIPlayAnimation::OnDragOut()
extern void UIPlayAnimation_OnDragOut_m89FB9389A0FC68A82537486E8DE8915C09EE400A ();
// 0x000001A7 System.Void UIPlayAnimation::OnDrop(UnityEngine.GameObject)
extern void UIPlayAnimation_OnDrop_m196A91AB7278E64F3E2E6102D4A50E051F209F5D ();
// 0x000001A8 System.Void UIPlayAnimation::Play(System.Boolean)
extern void UIPlayAnimation_Play_m07F867F76B1986FD50243CBE7460F508B180FA19 ();
// 0x000001A9 System.Void UIPlayAnimation::Play(System.Boolean,System.Boolean)
extern void UIPlayAnimation_Play_m61A0B09B3033385A0AF976040DDF43462783CD4F ();
// 0x000001AA System.Void UIPlayAnimation::PlayForward()
extern void UIPlayAnimation_PlayForward_mB8C590FCFA9911968C2490BD9B316A69906C5706 ();
// 0x000001AB System.Void UIPlayAnimation::PlayReverse()
extern void UIPlayAnimation_PlayReverse_m5FA91CB92118F00CEC9D6A1B51A01C289D736374 ();
// 0x000001AC System.Void UIPlayAnimation::OnFinished()
extern void UIPlayAnimation_OnFinished_m94EC659F1D7E9F21844785ABC4903B7B36EF0E74 ();
// 0x000001AD System.Void UIPlayAnimation::.ctor()
extern void UIPlayAnimation__ctor_m2146F6096F3DAFB2F8AFE128549652D800F6FA76 ();
// 0x000001AE System.Void UIPlayAnimation::.cctor()
extern void UIPlayAnimation__cctor_m1DE4207BE5018BCEA2A5B8EB8A0E7DB40567FDB7 ();
// 0x000001AF System.Boolean UIPlaySound::get_canPlay()
extern void UIPlaySound_get_canPlay_mBDB2CF6B1ADA75D33674CEEAD709E061F58F46D9 ();
// 0x000001B0 System.Void UIPlaySound::OnEnable()
extern void UIPlaySound_OnEnable_m5A56FAC10F2754D0282EC5E311DF0944DD41E3C7 ();
// 0x000001B1 System.Void UIPlaySound::OnDisable()
extern void UIPlaySound_OnDisable_m11F7C7C1B0DA6EF6DA7B184D34B17B4FAADAB01A ();
// 0x000001B2 System.Void UIPlaySound::OnHover(System.Boolean)
extern void UIPlaySound_OnHover_m8CEA76E2B0EA1206EEB9CA38EBD279C229F69301 ();
// 0x000001B3 System.Void UIPlaySound::OnPress(System.Boolean)
extern void UIPlaySound_OnPress_m4117AE3A33D6339313587E5F8994E4093B0004E8 ();
// 0x000001B4 System.Void UIPlaySound::OnClick()
extern void UIPlaySound_OnClick_mC93B0B25E001F5AD9E7FA0CF94A65741D7CC14C7 ();
// 0x000001B5 System.Void UIPlaySound::OnSelect(System.Boolean)
extern void UIPlaySound_OnSelect_m4415DEA521A271BDA8DF0DDC9910C1B487DEBAE1 ();
// 0x000001B6 System.Void UIPlaySound::Play()
extern void UIPlaySound_Play_m0061729E81BD3C86A8E316B6CBD045E0190E5024 ();
// 0x000001B7 System.Void UIPlaySound::.ctor()
extern void UIPlaySound__ctor_m10048EF46A70C46BBAE439F081D1656BD4E1BB84 ();
// 0x000001B8 System.Void UIPlayTween::Awake()
extern void UIPlayTween_Awake_mB4024D84F8D908016270AF5663F02BC388E31420 ();
// 0x000001B9 System.Void UIPlayTween::Start()
extern void UIPlayTween_Start_m3C1C9594B2FAB886CE09A1A9E0473490AA3107FC ();
// 0x000001BA System.Void UIPlayTween::OnEnable()
extern void UIPlayTween_OnEnable_m9F967299AAC0E593925086890D3DD5497EEB401D ();
// 0x000001BB System.Void UIPlayTween::OnDisable()
extern void UIPlayTween_OnDisable_m79251DCB9A38E1C87E527683DBBE85A0FC608DC6 ();
// 0x000001BC System.Void UIPlayTween::OnDragOver()
extern void UIPlayTween_OnDragOver_mB51A3EE1985165060047280089F22F88FDCA0207 ();
// 0x000001BD System.Void UIPlayTween::OnHover(System.Boolean)
extern void UIPlayTween_OnHover_mFEAA07AEC61E31BA01E2DFD7B6F07F7DF022D98A ();
// 0x000001BE System.Void UIPlayTween::CustomHoverListener(UnityEngine.GameObject,System.Boolean)
extern void UIPlayTween_CustomHoverListener_mE8D16EF27FBE16D0F779A6D17B21EEED77A5DD6F ();
// 0x000001BF System.Void UIPlayTween::OnDragOut()
extern void UIPlayTween_OnDragOut_mEF4C68CAD90C11C0898F5D311F9B0A39FDAA5ABF ();
// 0x000001C0 System.Void UIPlayTween::OnPress(System.Boolean)
extern void UIPlayTween_OnPress_mCD60ED4013F3D41281C39933E8981311C58206FD ();
// 0x000001C1 System.Void UIPlayTween::OnClick()
extern void UIPlayTween_OnClick_m4724BC7614AB4B5568C556BBD8C65C21F429F249 ();
// 0x000001C2 System.Void UIPlayTween::OnDoubleClick()
extern void UIPlayTween_OnDoubleClick_m0DCAAACF00E76F3DB35E00954C20433A3159BAD3 ();
// 0x000001C3 System.Void UIPlayTween::OnSelect(System.Boolean)
extern void UIPlayTween_OnSelect_mCBCDC9965D0B4820EC0AF64FB9A101F3DB8398FA ();
// 0x000001C4 System.Void UIPlayTween::OnToggle()
extern void UIPlayTween_OnToggle_m6586ED60804F4F7F7316D18AA675C8D8B2767D10 ();
// 0x000001C5 System.Void UIPlayTween::Update()
extern void UIPlayTween_Update_mC7162C3A2C6C3603BDE98FBF1A91BADB6D0764DC ();
// 0x000001C6 System.Void UIPlayTween::Play()
extern void UIPlayTween_Play_mF79D3A65ED63A957518677123613E54E0892696F ();
// 0x000001C7 System.Void UIPlayTween::Play(System.Boolean)
extern void UIPlayTween_Play_m888612B3E1748B69815F4EB39C54CF32DF874569 ();
// 0x000001C8 System.Void UIPlayTween::OnFinished()
extern void UIPlayTween_OnFinished_m3059ED2205994D0527D91F5708EEBB43B6FF332D ();
// 0x000001C9 System.Void UIPlayTween::.ctor()
extern void UIPlayTween__ctor_m92C7977B6139F6027BD56752B30B365C28786A10 ();
// 0x000001CA INGUIFont UIPopupList::get_font()
extern void UIPopupList_get_font_m755720AB763E79DE52E6FC79C8E6A1949C43BD6F ();
// 0x000001CB System.Void UIPopupList::set_font(INGUIFont)
extern void UIPopupList_set_font_m978876D116A04B98B7A29893355D0C26B25B6CC3 ();
// 0x000001CC UnityEngine.Object UIPopupList::get_ambigiousFont()
extern void UIPopupList_get_ambigiousFont_m48C069DF661E2A6C56022EDD7AF73964622AD346 ();
// 0x000001CD System.Void UIPopupList::set_ambigiousFont(UnityEngine.Object)
extern void UIPopupList_set_ambigiousFont_m881EC17717EAFFCA75F4C303FAEBB426F55601E0 ();
// 0x000001CE UIPopupList_LegacyEvent UIPopupList::get_onSelectionChange()
extern void UIPopupList_get_onSelectionChange_m6C84E18D2517E443A7B379AFB7443F47F39CD046 ();
// 0x000001CF System.Void UIPopupList::set_onSelectionChange(UIPopupList_LegacyEvent)
extern void UIPopupList_set_onSelectionChange_mF27E47B196F197A221A2C2CC514C2FA393B35538 ();
// 0x000001D0 System.Boolean UIPopupList::get_isOpen()
extern void UIPopupList_get_isOpen_mB51CDDAEC4E0D6470E74DD451092686C1D1AB302 ();
// 0x000001D1 System.String UIPopupList::get_value()
extern void UIPopupList_get_value_m1C19DDCB845052B3EFC6DDBD7588FC515B97D2C0 ();
// 0x000001D2 System.Void UIPopupList::set_value(System.String)
extern void UIPopupList_set_value_m403169A41570EA13671458E80A1B0B5BDC9C2B16 ();
// 0x000001D3 System.Object UIPopupList::get_data()
extern void UIPopupList_get_data_mA99BD1BE26DDCD95FC9B9AE0F1EC2ABB35F5E73C ();
// 0x000001D4 System.Action UIPopupList::get_callback()
extern void UIPopupList_get_callback_mCF9BC77A1209AAA6F984E87648C2A4C9ABC87A04 ();
// 0x000001D5 System.Boolean UIPopupList::get_isColliderEnabled()
extern void UIPopupList_get_isColliderEnabled_m00D0B325D3D364599C7533871D194F84299620F3 ();
// 0x000001D6 System.Void UIPopupList::set_isColliderEnabled(System.Boolean)
extern void UIPopupList_set_isColliderEnabled_m222979567114E4CE149F034E39CC56C1341C4BE0 ();
// 0x000001D7 System.Boolean UIPopupList::get_isValid()
extern void UIPopupList_get_isValid_mD489AAB9CDA735A923FDA5159E31516138129D70 ();
// 0x000001D8 System.Int32 UIPopupList::get_activeFontSize()
extern void UIPopupList_get_activeFontSize_m7E73F86FB67DFB514A7618AD8017B9EF8937B497 ();
// 0x000001D9 System.Single UIPopupList::get_activeFontScale()
extern void UIPopupList_get_activeFontScale_m71154DD0A5DC1CA0F374AE5C75F1435670118C08 ();
// 0x000001DA System.Single UIPopupList::get_fitScale()
extern void UIPopupList_get_fitScale_m1155731EE721FF1AC95FF963DC656645E12E34F8 ();
// 0x000001DB System.Void UIPopupList::Set(System.String,System.Boolean)
extern void UIPopupList_Set_mBE6012BD9249FEFDCCCF4B943D740FFF6675BD6D ();
// 0x000001DC System.Void UIPopupList::Clear()
extern void UIPopupList_Clear_m3D4E7F208FCF79B1426BFD9AAFB8E5E7DB261714 ();
// 0x000001DD System.Void UIPopupList::AddItem(System.String)
extern void UIPopupList_AddItem_mB5C3FEBB2714BBC7ED9804B780EB287BEF9BF2C8 ();
// 0x000001DE System.Void UIPopupList::AddItem(System.String,System.Action)
extern void UIPopupList_AddItem_mA55C04144AC64E3718067FF5A03C78B4D30FD176 ();
// 0x000001DF System.Void UIPopupList::AddItem(System.String,System.Object,System.Action)
extern void UIPopupList_AddItem_m73B4F5B3195E6B01B3FA3E59E755EBF4BD5F47F2 ();
// 0x000001E0 System.Void UIPopupList::RemoveItem(System.String)
extern void UIPopupList_RemoveItem_mBE5C047AB372A3D24127376473557B966A5277CA ();
// 0x000001E1 System.Void UIPopupList::RemoveItemByData(System.Object)
extern void UIPopupList_RemoveItemByData_mA6029D6BBA6C7A8D5A7F93FEEF02B97D4C75DE49 ();
// 0x000001E2 System.Void UIPopupList::TriggerCallbacks()
extern void UIPopupList_TriggerCallbacks_m87E94A5DEFA3C0F3E765DA5241084883652B40E6 ();
// 0x000001E3 System.Void UIPopupList::OnEnable()
extern void UIPopupList_OnEnable_m6C8B586098867B2DA4DBCEB212B8C5C63225CB88 ();
// 0x000001E4 System.Void UIPopupList::Start()
extern void UIPopupList_Start_mAD3DED4EB90FC055E0C3C57E7267567B00E6E4E8 ();
// 0x000001E5 System.Void UIPopupList::OnLocalize()
extern void UIPopupList_OnLocalize_mF5A594CF60C14F42CBC6C3E5E7831B0D2D657CC1 ();
// 0x000001E6 System.Void UIPopupList::Highlight(UILabel,System.Boolean)
extern void UIPopupList_Highlight_mA7BE703328D8EB549E75BC128D4D56926366B321 ();
// 0x000001E7 UnityEngine.Vector3 UIPopupList::GetHighlightPosition()
extern void UIPopupList_GetHighlightPosition_m73C3CADC407B047873CD1D34655581CCFF961E12 ();
// 0x000001E8 System.Collections.IEnumerator UIPopupList::UpdateTweenPosition()
extern void UIPopupList_UpdateTweenPosition_mC24F423BA8DA83DC85E71C50C36FB6E170F3BD12 ();
// 0x000001E9 System.Void UIPopupList::OnItemHover(UnityEngine.GameObject,System.Boolean)
extern void UIPopupList_OnItemHover_mC4AC80D54BAB749E3302510EDD4F8286EEC0844E ();
// 0x000001EA System.Void UIPopupList::OnItemPress(UnityEngine.GameObject,System.Boolean)
extern void UIPopupList_OnItemPress_m36425671A749880B1FC68C94BF19162080679028 ();
// 0x000001EB System.Void UIPopupList::OnItemClick(UnityEngine.GameObject)
extern void UIPopupList_OnItemClick_m98D5368EB2D40A31A7E5C167DF07774063BECC75 ();
// 0x000001EC System.Void UIPopupList::Select(UILabel,System.Boolean)
extern void UIPopupList_Select_mED1D3BC731A577FC06E44168AA634F4ABF103317 ();
// 0x000001ED System.Void UIPopupList::OnNavigate(UnityEngine.KeyCode)
extern void UIPopupList_OnNavigate_m7599F3271C6478C282C1910254E85CE093D086A9 ();
// 0x000001EE System.Void UIPopupList::OnKey(UnityEngine.KeyCode)
extern void UIPopupList_OnKey_m4614738622514015966205385C0E24A2E9C76D88 ();
// 0x000001EF System.Void UIPopupList::OnDisable()
extern void UIPopupList_OnDisable_m83D67149CBF39DC86A8705BD6824FCF33C0374F5 ();
// 0x000001F0 System.Void UIPopupList::OnSelect(System.Boolean)
extern void UIPopupList_OnSelect_mAF448DE1B33D14613B5F6BD1386C507B6DAC7C31 ();
// 0x000001F1 System.Void UIPopupList::Close()
extern void UIPopupList_Close_m35F5771BDA0C53348DA9AD3CBBBB52FACA59601D ();
// 0x000001F2 System.Void UIPopupList::CloseSelf()
extern void UIPopupList_CloseSelf_m83F0042AA52C04D24FF32C2BE8404C5C4CF8035F ();
// 0x000001F3 System.Void UIPopupList::AnimateColor(UIWidget)
extern void UIPopupList_AnimateColor_m4ED86B9114C57B4C8975ED62624063321056C9AC ();
// 0x000001F4 System.Void UIPopupList::AnimatePosition(UIWidget,System.Boolean,System.Single)
extern void UIPopupList_AnimatePosition_m11B23948D451B788118062868D041533F47D36EA ();
// 0x000001F5 System.Void UIPopupList::AnimateScale(UIWidget,System.Boolean,System.Single)
extern void UIPopupList_AnimateScale_m2E4ECC3ABB390E842990A177C75A3CC5601FB3C5 ();
// 0x000001F6 System.Void UIPopupList::Animate(UIWidget,System.Boolean,System.Single)
extern void UIPopupList_Animate_m79B7576CC5ECCD8351481F69E2346FBB05F48B4A ();
// 0x000001F7 System.Void UIPopupList::OnClick()
extern void UIPopupList_OnClick_m3384F644A33C286AA8D23CE05ADA0B52467D4AD0 ();
// 0x000001F8 System.Void UIPopupList::OnDoubleClick()
extern void UIPopupList_OnDoubleClick_mF585EFFF9617A9917063EA96ED60BBB7AE001C5F ();
// 0x000001F9 System.Collections.IEnumerator UIPopupList::CloseIfUnselected()
extern void UIPopupList_CloseIfUnselected_m4B9BD54464443307CA04BC39707770CEF7075B25 ();
// 0x000001FA System.Void UIPopupList::Show()
extern void UIPopupList_Show_mAC249A84A4C98CBE4C0CC58A535AA6F281E4E347 ();
// 0x000001FB System.Void UIPopupList::.ctor()
extern void UIPopupList__ctor_mBD0934F3E3ED9F35F63552D69C2582E1A334CFAD ();
// 0x000001FC System.Void UIPopupList::.cctor()
extern void UIPopupList__cctor_m987BDCA54E76C94A9A07E4D5EE6B09C74CE0D34E ();
// 0x000001FD UnityEngine.Transform UIProgressBar::get_cachedTransform()
extern void UIProgressBar_get_cachedTransform_mD7DE982C606F78FCDF337E59371622A29C787067 ();
// 0x000001FE UnityEngine.Camera UIProgressBar::get_cachedCamera()
extern void UIProgressBar_get_cachedCamera_m5A10D1B37EC075D77ACB558690B254F64409D290 ();
// 0x000001FF UIWidget UIProgressBar::get_foregroundWidget()
extern void UIProgressBar_get_foregroundWidget_m882A8610FCA789B8AB1246C71412B442C94CFAE3 ();
// 0x00000200 System.Void UIProgressBar::set_foregroundWidget(UIWidget)
extern void UIProgressBar_set_foregroundWidget_m8DBFBB429307818EB08B53690CA3C3923B10C6B3 ();
// 0x00000201 UIWidget UIProgressBar::get_backgroundWidget()
extern void UIProgressBar_get_backgroundWidget_mBA7EF2D274318625A0F4E87D9F368DF248F6AE42 ();
// 0x00000202 System.Void UIProgressBar::set_backgroundWidget(UIWidget)
extern void UIProgressBar_set_backgroundWidget_m126F7CC67F0B611484675F15C2A2831002ADF519 ();
// 0x00000203 UIProgressBar_FillDirection UIProgressBar::get_fillDirection()
extern void UIProgressBar_get_fillDirection_mCEDB865FF0FFD5E06A784E1F268752881F1ED97D ();
// 0x00000204 System.Void UIProgressBar::set_fillDirection(UIProgressBar_FillDirection)
extern void UIProgressBar_set_fillDirection_m4F2A2C8AA75C919769F33F0F7AE17E73C4A61668 ();
// 0x00000205 System.Single UIProgressBar::get_value()
extern void UIProgressBar_get_value_m699C5C061AFE565F8095732DD062CA322D765DF0 ();
// 0x00000206 System.Void UIProgressBar::set_value(System.Single)
extern void UIProgressBar_set_value_mD1D63D223C225E444CC8DC15D7EEC393DDDC32A8 ();
// 0x00000207 System.Single UIProgressBar::get_alpha()
extern void UIProgressBar_get_alpha_m4A869A292B27D99F6876609A17EC6C0EE9DE1F89 ();
// 0x00000208 System.Void UIProgressBar::set_alpha(System.Single)
extern void UIProgressBar_set_alpha_mC812C7BA892D114EDCFA2077B5EAB426265DAB36 ();
// 0x00000209 System.Boolean UIProgressBar::get_isHorizontal()
extern void UIProgressBar_get_isHorizontal_mE293BEEE3B0258914EF7D5FF06AE918C64977E2E ();
// 0x0000020A System.Boolean UIProgressBar::get_isInverted()
extern void UIProgressBar_get_isInverted_mE8605106AFDEFC114520CFC482741B19527B9F6A ();
// 0x0000020B System.Void UIProgressBar::Set(System.Single,System.Boolean)
extern void UIProgressBar_Set_m9A5919514AF78A9A2F404F04BA0F70C29C77BDCD ();
// 0x0000020C System.Void UIProgressBar::Start()
extern void UIProgressBar_Start_mF51EEB55D7E42E0ACE23E7C3D26DB9E76EFE82B5 ();
// 0x0000020D System.Void UIProgressBar::Upgrade()
extern void UIProgressBar_Upgrade_mCE09BA75A233DB56656DE8A1936424AAA0E87973 ();
// 0x0000020E System.Void UIProgressBar::OnStart()
extern void UIProgressBar_OnStart_mA5D54E6E0D8186F000117DE9F833BBFA33F0E20F ();
// 0x0000020F System.Void UIProgressBar::Update()
extern void UIProgressBar_Update_m793451FD6DEAFDEB1A72582F9269C48DACFA5158 ();
// 0x00000210 System.Void UIProgressBar::OnValidate()
extern void UIProgressBar_OnValidate_m55BA7D9E9A9409142C62E88FCE7493C9202583E9 ();
// 0x00000211 System.Single UIProgressBar::ScreenToValue(UnityEngine.Vector2)
extern void UIProgressBar_ScreenToValue_mD7B29ABF2509EEBD04B08510BC0B13B7E14A8E66 ();
// 0x00000212 System.Single UIProgressBar::LocalToValue(UnityEngine.Vector2)
extern void UIProgressBar_LocalToValue_mC27A9C225DAEF60BD4F6BE105211DAF4B287147F ();
// 0x00000213 System.Void UIProgressBar::ForceUpdate()
extern void UIProgressBar_ForceUpdate_mBF2C86E17B05B059F74F1070C0EFF51296E7A26C ();
// 0x00000214 System.Void UIProgressBar::SetThumbPosition(UnityEngine.Vector3)
extern void UIProgressBar_SetThumbPosition_m74D1684C22D95CA4E91B3EB5ED93DFD6E85B4403 ();
// 0x00000215 System.Void UIProgressBar::OnPan(UnityEngine.Vector2)
extern void UIProgressBar_OnPan_m3FF3967608E363680E540F267D912CB3F9B3D640 ();
// 0x00000216 System.Void UIProgressBar::.ctor()
extern void UIProgressBar__ctor_m5095406C309F7394B74EA09DC60AE32226DC05D4 ();
// 0x00000217 System.String UISavedOption::get_key()
extern void UISavedOption_get_key_m86E41D01D9DA6FDFFDD8A716BD9B7AA1117D8A8E ();
// 0x00000218 System.Void UISavedOption::Awake()
extern void UISavedOption_Awake_m42E1758B75900FA1739C6CDE9101C39CB299909B ();
// 0x00000219 System.Void UISavedOption::OnEnable()
extern void UISavedOption_OnEnable_m291C21B78688457C2F2167FFADFCC0A71B309BAA ();
// 0x0000021A System.Void UISavedOption::OnDisable()
extern void UISavedOption_OnDisable_mC5C2E938995AF2B255A6B14E3EF4389B8833C80C ();
// 0x0000021B System.Void UISavedOption::SaveSelection()
extern void UISavedOption_SaveSelection_mC3811EBC90DD5F5BC495957FBE392CF6A8EBB254 ();
// 0x0000021C System.Void UISavedOption::SaveState()
extern void UISavedOption_SaveState_mE6E14BAF5DDA5A6B2F702AD3282603DD43A87404 ();
// 0x0000021D System.Void UISavedOption::SaveProgress()
extern void UISavedOption_SaveProgress_m458AF27605D934EF021ED68113FF0983AFDD0EC0 ();
// 0x0000021E System.Void UISavedOption::.ctor()
extern void UISavedOption__ctor_mC20F94CB762AC3F25CF51939E8993CF344B89988 ();
// 0x0000021F System.Single UIScrollBar::get_scrollValue()
extern void UIScrollBar_get_scrollValue_m314FD042F4A15212FE0F2764CB66118CA8858F42 ();
// 0x00000220 System.Void UIScrollBar::set_scrollValue(System.Single)
extern void UIScrollBar_set_scrollValue_mD0F3AA7F365EEC998F54DB0686155F540D48CC49 ();
// 0x00000221 System.Single UIScrollBar::get_barSize()
extern void UIScrollBar_get_barSize_mA1108445228E3A20DB15C71CD893949A2C60681C ();
// 0x00000222 System.Void UIScrollBar::set_barSize(System.Single)
extern void UIScrollBar_set_barSize_m81BF2B4DA3555EE652DC863FDB3D9565419C7160 ();
// 0x00000223 System.Void UIScrollBar::Upgrade()
extern void UIScrollBar_Upgrade_m78A2C6461A6E06C66C745EDF2602CA3FFBE0D8BC ();
// 0x00000224 System.Void UIScrollBar::OnStart()
extern void UIScrollBar_OnStart_m165344171BA9FB54C6A365B73724BE9CA335D49A ();
// 0x00000225 System.Single UIScrollBar::LocalToValue(UnityEngine.Vector2)
extern void UIScrollBar_LocalToValue_m6F66172F63A5E014E022563EDCEC1CFB4C8C9A0E ();
// 0x00000226 System.Void UIScrollBar::ForceUpdate()
extern void UIScrollBar_ForceUpdate_m4C911A795F125F47004238B77FD2B62D52CF6F76 ();
// 0x00000227 System.Void UIScrollBar::.ctor()
extern void UIScrollBar__ctor_m0B9BFB5F18CF2A635C77A75B8E04409A14193311 ();
// 0x00000228 UIPanel UIScrollView::get_panel()
extern void UIScrollView_get_panel_mBEBFAC8C6485164F313407970A380BF988F6AF71 ();
// 0x00000229 System.Boolean UIScrollView::get_isDragging()
extern void UIScrollView_get_isDragging_mA53417F352E0A0C81BD88E00CC12C89B3BD9E62C ();
// 0x0000022A UnityEngine.Bounds UIScrollView::get_bounds()
extern void UIScrollView_get_bounds_mBF6A9B72999F6F8755A1E70F84685AACD1A91A4B ();
// 0x0000022B System.Boolean UIScrollView::get_canMoveHorizontally()
extern void UIScrollView_get_canMoveHorizontally_m2C9058155249173D5981DB2113C81E543D0EAF00 ();
// 0x0000022C System.Boolean UIScrollView::get_canMoveVertically()
extern void UIScrollView_get_canMoveVertically_mE5B7092487BBF99C0BDAEB88289CB25D6850F20F ();
// 0x0000022D System.Boolean UIScrollView::get_shouldMoveHorizontally()
extern void UIScrollView_get_shouldMoveHorizontally_m47390D0E106B7DF080E957098C721C28AF0727F2 ();
// 0x0000022E System.Boolean UIScrollView::get_shouldMoveVertically()
extern void UIScrollView_get_shouldMoveVertically_m340321A638704FC5EAACD5D01AD589A2ED55E78E ();
// 0x0000022F System.Boolean UIScrollView::get_shouldMove()
extern void UIScrollView_get_shouldMove_m6B19565989A0BD3CE3AB013E7B4683B02F09209A ();
// 0x00000230 UnityEngine.Vector3 UIScrollView::get_currentMomentum()
extern void UIScrollView_get_currentMomentum_m408526CA7F71FD0A1175BABBAE7A2D027C726814 ();
// 0x00000231 System.Void UIScrollView::set_currentMomentum(UnityEngine.Vector3)
extern void UIScrollView_set_currentMomentum_mFC18218DB6BDCF4605E6933878478E061FC7AEF9 ();
// 0x00000232 System.Void UIScrollView::Awake()
extern void UIScrollView_Awake_m7295B914C25B194A0A4F81B60459D8F5C4885195 ();
// 0x00000233 System.Void UIScrollView::OnEnable()
extern void UIScrollView_OnEnable_mD20CE2B33343B4DD3D718ED106FE3C3CB8C9701E ();
// 0x00000234 System.Void UIScrollView::Start()
extern void UIScrollView_Start_m6EA7138BB60DB53D10664D744DAAEFC4DED0AFBC ();
// 0x00000235 System.Void UIScrollView::CheckScrollbars()
extern void UIScrollView_CheckScrollbars_m2A2816D362EFED9F8304B06011839B1E29E310F3 ();
// 0x00000236 System.Void UIScrollView::OnDisable()
extern void UIScrollView_OnDisable_m55F4556E05DDCA4CD85FB925F70AFC132E451437 ();
// 0x00000237 System.Boolean UIScrollView::RestrictWithinBounds(System.Boolean)
extern void UIScrollView_RestrictWithinBounds_m0B820840FDB7F16339785002BF3FE3293CAB1EA1 ();
// 0x00000238 System.Boolean UIScrollView::RestrictWithinBounds(System.Boolean,System.Boolean,System.Boolean)
extern void UIScrollView_RestrictWithinBounds_m5F2F17DDA262BBBAE4551CBF4E291A8CD2AC0D74 ();
// 0x00000239 System.Void UIScrollView::DisableSpring()
extern void UIScrollView_DisableSpring_m6C872CDD237F74E822BD205DE738D527D665C4C1 ();
// 0x0000023A System.Void UIScrollView::UpdateScrollbars()
extern void UIScrollView_UpdateScrollbars_m29528D9AA53BBA21460CA1E5A00608578A2C6D7E ();
// 0x0000023B System.Void UIScrollView::UpdateScrollbars(System.Boolean)
extern void UIScrollView_UpdateScrollbars_m76A528016BC440FD86EBA9B691CEF7EB614229EB ();
// 0x0000023C System.Void UIScrollView::UpdateScrollbars(UIProgressBar,System.Single,System.Single,System.Single,System.Single,System.Boolean)
extern void UIScrollView_UpdateScrollbars_m04AEC8B3A773ACF98C99F692F6C4692A56436678 ();
// 0x0000023D System.Void UIScrollView::SetDragAmount(System.Single,System.Single,System.Boolean)
extern void UIScrollView_SetDragAmount_m863417E515472947D7FDACD185F240776DF3B60B ();
// 0x0000023E System.Void UIScrollView::InvalidateBounds()
extern void UIScrollView_InvalidateBounds_m0C3454AD82F27FCA7EA897517783E18B29E85592 ();
// 0x0000023F System.Void UIScrollView::ResetPosition()
extern void UIScrollView_ResetPosition_mAA9839171EAD3D4073F9EE00001A954B26EFAB45 ();
// 0x00000240 System.Void UIScrollView::UpdatePosition()
extern void UIScrollView_UpdatePosition_mBE3EBBC23F8BBED583A2B7AD6A0517BD0324C9C2 ();
// 0x00000241 System.Void UIScrollView::OnScrollBar()
extern void UIScrollView_OnScrollBar_m4165B96DF32D2787E7C2CEA6D63EE1356E86E205 ();
// 0x00000242 System.Void UIScrollView::MoveRelative(UnityEngine.Vector3)
extern void UIScrollView_MoveRelative_mF335FACD5434833D15DD4157DAA0D9ABF011AB1A ();
// 0x00000243 System.Void UIScrollView::MoveAbsolute(UnityEngine.Vector3)
extern void UIScrollView_MoveAbsolute_m318FB6944EA70202DBE292CAD35CFC267789A152 ();
// 0x00000244 System.Void UIScrollView::Press(System.Boolean)
extern void UIScrollView_Press_m8FB3F2E26551E9BF55C04D853B4ACDFAACD2C420 ();
// 0x00000245 System.Void UIScrollView::Drag()
extern void UIScrollView_Drag_m4349CD8FAA0965D4E8619C24EED191E5C4D507F5 ();
// 0x00000246 System.Void UIScrollView::Scroll(System.Single)
extern void UIScrollView_Scroll_m9B842E6ACFD14B49CE867825367E617E59CB039B ();
// 0x00000247 System.Void UIScrollView::LateUpdate()
extern void UIScrollView_LateUpdate_m705488392BB76FEA2FFF325EBD955A13E37F4488 ();
// 0x00000248 System.Void UIScrollView::OnPan(UnityEngine.Vector2)
extern void UIScrollView_OnPan_m04B62FC0B5D78764CE663AE26A5AF653D9362633 ();
// 0x00000249 System.Void UIScrollView::.ctor()
extern void UIScrollView__ctor_m36E58295828F705ABAA790E4365944DEC3381769 ();
// 0x0000024A System.Void UIScrollView::.cctor()
extern void UIScrollView__cctor_mF0B30A0767415B93986ED7F16E4119728C8C9DE9 ();
// 0x0000024B System.Void UIShowControlScheme::OnEnable()
extern void UIShowControlScheme_OnEnable_m3ED41636221A84FDF524EBECD46ECFDD4106140B ();
// 0x0000024C System.Void UIShowControlScheme::OnDisable()
extern void UIShowControlScheme_OnDisable_m247356D75EE328CF8D921ADCD8655DAF5EF5E7DA ();
// 0x0000024D System.Void UIShowControlScheme::OnScheme()
extern void UIShowControlScheme_OnScheme_m0AB8F0C1C07977B9C58E6A4943F30B2DF6BF4626 ();
// 0x0000024E System.Void UIShowControlScheme::.ctor()
extern void UIShowControlScheme__ctor_m19EE086C803B61E3D1D757976EC40B7AD3ACC33C ();
// 0x0000024F System.Boolean UISlider::get_isColliderEnabled()
extern void UISlider_get_isColliderEnabled_mD8731E3D4F98178438824C8820C3019C951A6FA9 ();
// 0x00000250 System.Single UISlider::get_sliderValue()
extern void UISlider_get_sliderValue_mB3365FB79A545CF25FD1F8B33955E30079A31E89 ();
// 0x00000251 System.Void UISlider::set_sliderValue(System.Single)
extern void UISlider_set_sliderValue_m0C87B150722DCD0E07234BE9694A87D0D630A16A ();
// 0x00000252 System.Boolean UISlider::get_inverted()
extern void UISlider_get_inverted_mB32E4232541CF31CDDDA1B33F1ADED9675381D51 ();
// 0x00000253 System.Void UISlider::set_inverted(System.Boolean)
extern void UISlider_set_inverted_mCE4E0EEEA1AE0A0E16B2D0005A6FA4EB295C8C93 ();
// 0x00000254 System.Void UISlider::Upgrade()
extern void UISlider_Upgrade_mC6E1E2E9EA43620412270889D8A3916B4D5D3B07 ();
// 0x00000255 System.Void UISlider::OnStart()
extern void UISlider_OnStart_m0359CE5BDBE6E9C8A7CD00AE3029AC02D9D4B0B7 ();
// 0x00000256 System.Void UISlider::OnPressBackground(UnityEngine.GameObject,System.Boolean)
extern void UISlider_OnPressBackground_mBBCE0234A1703305BAAA2E5CB01677BAA17E1E11 ();
// 0x00000257 System.Void UISlider::OnDragBackground(UnityEngine.GameObject,UnityEngine.Vector2)
extern void UISlider_OnDragBackground_m563C0088C6569CD409895E23EA3D51A1B501E163 ();
// 0x00000258 System.Void UISlider::OnPressForeground(UnityEngine.GameObject,System.Boolean)
extern void UISlider_OnPressForeground_m22FA341E6DBB260B2D6B411E5125A03695AC189A ();
// 0x00000259 System.Void UISlider::OnDragForeground(UnityEngine.GameObject,UnityEngine.Vector2)
extern void UISlider_OnDragForeground_m8E6BB58DF5DCA83DAF73D5FE7C7AB9CE8E6F46DF ();
// 0x0000025A System.Void UISlider::OnPan(UnityEngine.Vector2)
extern void UISlider_OnPan_mFD2A89310F736DA871B10F84A094BF9EEE81762A ();
// 0x0000025B System.Void UISlider::.ctor()
extern void UISlider__ctor_m75EB0A507F3981D2FC96B967180F5E65C3B9AEFC ();
// 0x0000025C System.Void UISoundVolume::Awake()
extern void UISoundVolume_Awake_m29698F4342445E9CE8E60AAF834EEB86698A6560 ();
// 0x0000025D System.Void UISoundVolume::OnChange()
extern void UISoundVolume_OnChange_mFCCD9F9801C3211995D7793CFF9097846F02D079 ();
// 0x0000025E System.Void UISoundVolume::.ctor()
extern void UISoundVolume__ctor_m9FCBCEA790666168C55B8F9CB076096476BDB15D ();
// 0x0000025F System.Void UITable::set_repositionNow(System.Boolean)
extern void UITable_set_repositionNow_mD243457E53C1801B27B6E4B93118AD1BD5B0BA3B ();
// 0x00000260 System.Collections.Generic.List`1<UnityEngine.Transform> UITable::GetChildList()
extern void UITable_GetChildList_m85DC97B2F1E3151DC052BEE30D5F886740507CA1 ();
// 0x00000261 System.Void UITable::Sort(System.Collections.Generic.List`1<UnityEngine.Transform>)
extern void UITable_Sort_m8616A1E456FF09FA9D7BE78B31CB68629D91FA6B ();
// 0x00000262 System.Void UITable::Start()
extern void UITable_Start_m09E81216A8E277230EC07AC9D9867FAADDBB3745 ();
// 0x00000263 System.Void UITable::Init()
extern void UITable_Init_m199C860085804BC483B4D4EE6865A1EA53152B7F ();
// 0x00000264 System.Void UITable::LateUpdate()
extern void UITable_LateUpdate_mF505A9867E42BA7870A83B638D52E3A6A19CF22C ();
// 0x00000265 System.Void UITable::OnValidate()
extern void UITable_OnValidate_m34108C4BAE9EDF3FA70080C80B2000B46B8DB7F5 ();
// 0x00000266 System.Void UITable::RepositionVariableSize(System.Collections.Generic.List`1<UnityEngine.Transform>)
extern void UITable_RepositionVariableSize_mDC876B3F4A8A34862EE6C9E8CF28EA1B0A1D9B75 ();
// 0x00000267 System.Void UITable::Reposition()
extern void UITable_Reposition_m2076F5DC73CDE16433A37E421E2ACE0901D5C9EF ();
// 0x00000268 System.Void UITable::.ctor()
extern void UITable__ctor_mAA178AEC3F50F43FD42C6C02A0E709603CE60ABB ();
// 0x00000269 System.Boolean UIToggle::get_value()
extern void UIToggle_get_value_mCBCD4247DE67C2C61E1E75D7150D637D10CAB873 ();
// 0x0000026A System.Void UIToggle::set_value(System.Boolean)
extern void UIToggle_set_value_mBDCDF8D43B37CB5B05C26DC54E7A91FA96C7A18D ();
// 0x0000026B System.Boolean UIToggle::get_isColliderEnabled()
extern void UIToggle_get_isColliderEnabled_mFE8F96770DB7C6118C81802CAA541ABB8DA7C9D9 ();
// 0x0000026C System.Boolean UIToggle::get_isChecked()
extern void UIToggle_get_isChecked_m45EC1A78C22706AA014CFE0C8CBA805BB5CC4889 ();
// 0x0000026D System.Void UIToggle::set_isChecked(System.Boolean)
extern void UIToggle_set_isChecked_m938534C39FFD0BA8A19D7F78E77189E87AB514DE ();
// 0x0000026E UIToggle UIToggle::GetActiveToggle(System.Int32)
extern void UIToggle_GetActiveToggle_m9DF963C68A5D9DC21AA0A9381ED184F986148FEC ();
// 0x0000026F System.Void UIToggle::OnEnable()
extern void UIToggle_OnEnable_m413F9ABA8FB12D64A4718E1888B2878ED4BD228F ();
// 0x00000270 System.Void UIToggle::OnDisable()
extern void UIToggle_OnDisable_m089E1063FAFA02649309FCBF3D6660108526EB90 ();
// 0x00000271 System.Void UIToggle::Start()
extern void UIToggle_Start_mB1AD437D182C3709E0BBAEDAD89E05A2FFD71C9B ();
// 0x00000272 System.Void UIToggle::OnClick()
extern void UIToggle_OnClick_m7840D881C17D9F90D4331AADCFD23ACE8B168C85 ();
// 0x00000273 System.Void UIToggle::Set(System.Boolean,System.Boolean)
extern void UIToggle_Set_mBDD54986DE8114C86E07C2F969F1EA272C1369D7 ();
// 0x00000274 System.Void UIToggle::.ctor()
extern void UIToggle__ctor_mFA6C18C6C693D7626E3D9411613DFA438990CD72 ();
// 0x00000275 System.Void UIToggle::.cctor()
extern void UIToggle__cctor_mD563A9197DA664347C7FE2F323891B308F9C7A45 ();
// 0x00000276 System.Void UIToggledComponents::Awake()
extern void UIToggledComponents_Awake_m5ADE134BEA8D7C1CCCDA5D09B5661E8FDBFC082B ();
// 0x00000277 System.Void UIToggledComponents::Toggle()
extern void UIToggledComponents_Toggle_m0D515DC5CF4E74528862199EBC10317EB43BF6AE ();
// 0x00000278 System.Void UIToggledComponents::.ctor()
extern void UIToggledComponents__ctor_m1ECF5B565594EC3369F95DF8D80D80C6C57029AB ();
// 0x00000279 System.Void UIToggledObjects::Awake()
extern void UIToggledObjects_Awake_m21EF4F75E646C7ECD81CEC091058D7F5347C2279 ();
// 0x0000027A System.Void UIToggledObjects::Toggle()
extern void UIToggledObjects_Toggle_m201BF3998B25E7D07CB8206002C63F4356903B63 ();
// 0x0000027B System.Void UIToggledObjects::Set(UnityEngine.GameObject,System.Boolean)
extern void UIToggledObjects_Set_mC721A1CD5FE868EC21E8C66293BDB27EE20A9C4F ();
// 0x0000027C System.Void UIToggledObjects::.ctor()
extern void UIToggledObjects__ctor_m1FFD830F1D0BBEF3899B86565999C8080AF9A584 ();
// 0x0000027D System.Void UIWidgetContainer::.ctor()
extern void UIWidgetContainer__ctor_mFF8C191910B00DCBBFA8127FE6F73E1290D3BB1F ();
// 0x0000027E System.Void UIWrapContent::Start()
extern void UIWrapContent_Start_mFB10931CDAB8A64E3E2FCD275FDDA6A47CD98B56 ();
// 0x0000027F System.Void UIWrapContent::OnMove(UIPanel)
extern void UIWrapContent_OnMove_m005C3049D8133096F1C3E0DC5ABA597F5355FC31 ();
// 0x00000280 System.Void UIWrapContent::SortBasedOnScrollMovement()
extern void UIWrapContent_SortBasedOnScrollMovement_m6C5ABDD7B3DC0B08FA051EC2EFA819D00F97A305 ();
// 0x00000281 System.Void UIWrapContent::SortAlphabetically()
extern void UIWrapContent_SortAlphabetically_m647C29FF33BBCF7108C0952D01D1F7159BB776E2 ();
// 0x00000282 System.Boolean UIWrapContent::CacheScrollView()
extern void UIWrapContent_CacheScrollView_m01B46417F30D6B1563D532BA2A932291864D0F80 ();
// 0x00000283 System.Void UIWrapContent::ResetChildPositions()
extern void UIWrapContent_ResetChildPositions_m2C37A68BD98BCF688146FF4D4EFCA282D6D70E00 ();
// 0x00000284 System.Void UIWrapContent::WrapContent()
extern void UIWrapContent_WrapContent_m87249CBB87BCC28A254D028A3B17C27D6C4DCD68 ();
// 0x00000285 System.Void UIWrapContent::OnValidate()
extern void UIWrapContent_OnValidate_mA6F75556072FAE490A3865186CD5F949671D812D ();
// 0x00000286 System.Void UIWrapContent::UpdateItem(UnityEngine.Transform,System.Int32)
extern void UIWrapContent_UpdateItem_m248467386580B4928EAACB1F98882A626E5ADE00 ();
// 0x00000287 System.Void UIWrapContent::.ctor()
extern void UIWrapContent__ctor_mFEF5B56E9EA20E46D362AA341CCA578389381DA0 ();
// 0x00000288 System.Single ActiveAnimation::get_playbackTime()
extern void ActiveAnimation_get_playbackTime_mB4231908ABE26152EA9D648F1313DBCB53C13E96 ();
// 0x00000289 System.Boolean ActiveAnimation::get_isPlaying()
extern void ActiveAnimation_get_isPlaying_mA3D22F5B957AA018A280581357F596D32F8DD506 ();
// 0x0000028A System.Void ActiveAnimation::Finish()
extern void ActiveAnimation_Finish_mB13A2535FEED77E472B4D95678C6B47ECDE09BAF ();
// 0x0000028B System.Void ActiveAnimation::Reset()
extern void ActiveAnimation_Reset_mDC610FE95826B0775C0A255BF549E2E1F91FFDFE ();
// 0x0000028C System.Void ActiveAnimation::Start()
extern void ActiveAnimation_Start_m3493D2687CCBE1F9231CF9A55FDB6EB4F337FBA3 ();
// 0x0000028D System.Void ActiveAnimation::Update()
extern void ActiveAnimation_Update_m947AFA115B90BB5DB3AF6C8F197066654BD15AE1 ();
// 0x0000028E System.Void ActiveAnimation::Play(System.String,AnimationOrTween.Direction)
extern void ActiveAnimation_Play_m9BE80E95A87BA8B324B6C3CA7E7DB2EBBCE43A41 ();
// 0x0000028F ActiveAnimation ActiveAnimation::Play(UnityEngine.Animation,System.String,AnimationOrTween.Direction,AnimationOrTween.EnableCondition,AnimationOrTween.DisableCondition)
extern void ActiveAnimation_Play_m00DF9A80DC52AA343ABE75DBF9B2C8F401A039DD ();
// 0x00000290 ActiveAnimation ActiveAnimation::Play(UnityEngine.Animation,System.String,AnimationOrTween.Direction)
extern void ActiveAnimation_Play_m6A32F3485CE5F30F1492952073D0E3361195661E ();
// 0x00000291 ActiveAnimation ActiveAnimation::Play(UnityEngine.Animation,AnimationOrTween.Direction)
extern void ActiveAnimation_Play_m800182CE5C1880480446B73EDA4F3E5A725EEE2F ();
// 0x00000292 ActiveAnimation ActiveAnimation::Play(UnityEngine.Animator,System.String,AnimationOrTween.Direction,AnimationOrTween.EnableCondition,AnimationOrTween.DisableCondition)
extern void ActiveAnimation_Play_m5C0D794AC0768BBD7E8095980F9C644A1F3021EB ();
// 0x00000293 System.Void ActiveAnimation::.ctor()
extern void ActiveAnimation__ctor_mD1D441A7E0F6609A51D3E77B10EB69040D339551 ();
// 0x00000294 System.Boolean BMFont::get_isValid()
extern void BMFont_get_isValid_m9B125D4290CAC19C7A6E64C98B0A8480637EB6FC ();
// 0x00000295 System.Int32 BMFont::get_charSize()
extern void BMFont_get_charSize_mF94B61A5B60AE675C337C9CCF41C6D4AD1A72456 ();
// 0x00000296 System.Void BMFont::set_charSize(System.Int32)
extern void BMFont_set_charSize_m0ED5B6DF647142B61FD36FF6B72F37C7FD4D0629 ();
// 0x00000297 System.Int32 BMFont::get_baseOffset()
extern void BMFont_get_baseOffset_mDF77D32CEFC81A50C0672A980FB78B01AAD2990F ();
// 0x00000298 System.Void BMFont::set_baseOffset(System.Int32)
extern void BMFont_set_baseOffset_mB2314B2EFB01BEE62B67F8B148C51A7670204648 ();
// 0x00000299 System.Int32 BMFont::get_texWidth()
extern void BMFont_get_texWidth_mD603000A1F55EB7766122485C2BC111A383CFC78 ();
// 0x0000029A System.Void BMFont::set_texWidth(System.Int32)
extern void BMFont_set_texWidth_mFB3A87272B5F4E911F5449393D1B5A3FEB4CA52A ();
// 0x0000029B System.Int32 BMFont::get_texHeight()
extern void BMFont_get_texHeight_mCA5D3F525A071793A5E0BFA94E0660590954C097 ();
// 0x0000029C System.Void BMFont::set_texHeight(System.Int32)
extern void BMFont_set_texHeight_m210344B5B2CD973F6E5ECF2B45A1DE775213256C ();
// 0x0000029D System.Int32 BMFont::get_glyphCount()
extern void BMFont_get_glyphCount_mD07E143B2F77735D3148E531704D08F047C9031C ();
// 0x0000029E System.String BMFont::get_spriteName()
extern void BMFont_get_spriteName_m8CDC1C0D10819E65D1DB199F5CFE01D99C0BE84F ();
// 0x0000029F System.Void BMFont::set_spriteName(System.String)
extern void BMFont_set_spriteName_mE6749A3966F3370D11C8848D70C96B530D26EADA ();
// 0x000002A0 System.Collections.Generic.List`1<BMGlyph> BMFont::get_glyphs()
extern void BMFont_get_glyphs_m47ABD209BC853FB1D3426B9D99A35C4D50DC23BC ();
// 0x000002A1 BMGlyph BMFont::GetGlyph(System.Int32,System.Boolean)
extern void BMFont_GetGlyph_mA73323B40435EF19882139F4ED97117DA416810A ();
// 0x000002A2 BMGlyph BMFont::GetGlyph(System.Int32)
extern void BMFont_GetGlyph_mB1937D47E81474AEEBF021A3029D38B8E2EF6899 ();
// 0x000002A3 System.Void BMFont::Clear()
extern void BMFont_Clear_mB387DEA2628E59E201745D472541703840BB5DC8 ();
// 0x000002A4 System.Void BMFont::Trim(System.Int32,System.Int32,System.Int32,System.Int32)
extern void BMFont_Trim_m3C5A6B7B92DA838E88584C0A978C01D9165DCBE6 ();
// 0x000002A5 System.Void BMFont::.ctor()
extern void BMFont__ctor_m928AD21F5469E72F562A908EA6A317AB0609F9D5 ();
// 0x000002A6 System.Int32 BMGlyph::GetKerning(System.Int32)
extern void BMGlyph_GetKerning_mF08433129A11E73C01FD36B6645EEA69689390FF ();
// 0x000002A7 System.Void BMGlyph::SetKerning(System.Int32,System.Int32)
extern void BMGlyph_SetKerning_mEDC70ACF28AEFEBE627C759BF6C2DE951E81449E ();
// 0x000002A8 System.Void BMGlyph::Trim(System.Int32,System.Int32,System.Int32,System.Int32)
extern void BMGlyph_Trim_m0660D92C8F7128EA2039359C616C22CE6B73C3A6 ();
// 0x000002A9 System.Void BMGlyph::.ctor()
extern void BMGlyph__ctor_m05B7C263C9DA5E93FBCF30200A0155407471932D ();
// 0x000002AA System.Int32 BMSymbol::get_length()
extern void BMSymbol_get_length_mF7E671E546D5ADDEF7BD1BAA3FEC7777927D77A0 ();
// 0x000002AB System.Int32 BMSymbol::get_offsetX()
extern void BMSymbol_get_offsetX_m1EAC99E62D751D406391493BF91F103687E12ABA ();
// 0x000002AC System.Int32 BMSymbol::get_offsetY()
extern void BMSymbol_get_offsetY_mFFD914698FCEDAB5EFDF9932A5279ADE96E43F7E ();
// 0x000002AD System.Int32 BMSymbol::get_width()
extern void BMSymbol_get_width_m2E6B707652F8A9C8BA488CC1C5C60BACC8F449BF ();
// 0x000002AE System.Int32 BMSymbol::get_height()
extern void BMSymbol_get_height_m8FB6159133574894FC9D07A6BECC2F0AA2505E2E ();
// 0x000002AF System.Int32 BMSymbol::get_advance()
extern void BMSymbol_get_advance_m70CCC18605558A24565A705C275271F44729B665 ();
// 0x000002B0 UnityEngine.Rect BMSymbol::get_uvRect()
extern void BMSymbol_get_uvRect_mE008BD67DCFF3DC4FFB39958ABE71BFD185491CF ();
// 0x000002B1 System.Void BMSymbol::MarkAsChanged()
extern void BMSymbol_MarkAsChanged_m904B4E50E1AE8651B0A23D04C09FB72FDFC14D14 ();
// 0x000002B2 System.Boolean BMSymbol::Validate(INGUIAtlas)
extern void BMSymbol_Validate_m3EAC3E8EF4077C4527F713F91297CB4FCF943DB9 ();
// 0x000002B3 System.Void BMSymbol::.ctor()
extern void BMSymbol__ctor_mEA6BAE427D60F27B404FBD1B4BB06680F49A5F06 ();
// 0x000002B4 System.Collections.Generic.IEnumerator`1<T> BetterList`1::GetEnumerator()
// 0x000002B5 T BetterList`1::get_Item(System.Int32)
// 0x000002B6 System.Void BetterList`1::set_Item(System.Int32,T)
// 0x000002B7 System.Void BetterList`1::AllocateMore()
// 0x000002B8 System.Void BetterList`1::Trim()
// 0x000002B9 System.Void BetterList`1::Clear()
// 0x000002BA System.Void BetterList`1::Release()
// 0x000002BB System.Void BetterList`1::Add(T)
// 0x000002BC System.Void BetterList`1::Insert(System.Int32,T)
// 0x000002BD System.Boolean BetterList`1::Contains(T)
// 0x000002BE System.Int32 BetterList`1::IndexOf(T)
// 0x000002BF System.Boolean BetterList`1::Remove(T)
// 0x000002C0 System.Void BetterList`1::RemoveAt(System.Int32)
// 0x000002C1 T BetterList`1::Pop()
// 0x000002C2 T[] BetterList`1::ToArray()
// 0x000002C3 System.Void BetterList`1::Sort(BetterList`1_CompareFunc<T>)
// 0x000002C4 System.Void BetterList`1::.ctor()
// 0x000002C5 System.Void ByteReader::.ctor(System.Byte[])
extern void ByteReader__ctor_mC57C833BDDA43C8A3237E4B8551272512BCD52DE ();
// 0x000002C6 System.Void ByteReader::.ctor(UnityEngine.TextAsset)
extern void ByteReader__ctor_mE7A78D898DD013A16CB2F7AAB5C483CF4814CDAD ();
// 0x000002C7 ByteReader ByteReader::Open(System.String)
extern void ByteReader_Open_m09A1AB6FABAE5CC92F2C8C89F6C539B6A7BB5CEA ();
// 0x000002C8 System.Boolean ByteReader::get_canRead()
extern void ByteReader_get_canRead_m7E9FDBCFA90FC4DECCEEFF44428AC5A3073F190F ();
// 0x000002C9 System.String ByteReader::ReadLine(System.Byte[],System.Int32,System.Int32)
extern void ByteReader_ReadLine_m59CA3836BC437C38F2369F8E3A91BF7DF823ECAA ();
// 0x000002CA System.String ByteReader::ReadLine()
extern void ByteReader_ReadLine_mEE4F1184E75A4B8CD5188F904ECB99BB539F84A0 ();
// 0x000002CB System.String ByteReader::ReadLine(System.Boolean)
extern void ByteReader_ReadLine_m304B98DF8293705C51851056704CEF8777707897 ();
// 0x000002CC System.Collections.Generic.Dictionary`2<System.String,System.String> ByteReader::ReadDictionary()
extern void ByteReader_ReadDictionary_m5BE7FEEB9A92EF7B6C1A29225BF62861AF56AA16 ();
// 0x000002CD BetterList`1<System.String> ByteReader::ReadCSV()
extern void ByteReader_ReadCSV_mB5DE9807684878995C27E44EB45BC12AC11252E5 ();
// 0x000002CE System.Void ByteReader::.cctor()
extern void ByteReader__cctor_m56913D7334EECA5D144E16A177BC3F583E5E9230 ();
// 0x000002CF UnityEngine.MonoBehaviour EventDelegate::get_target()
extern void EventDelegate_get_target_m64100D73B6BAA1EC13A76681D4654854A103A458 ();
// 0x000002D0 System.Void EventDelegate::set_target(UnityEngine.MonoBehaviour)
extern void EventDelegate_set_target_m60EE1DC693FF5308E4EF29A809ABBE9A1AA6F02C ();
// 0x000002D1 System.String EventDelegate::get_methodName()
extern void EventDelegate_get_methodName_mEC14CB3A6099BA9C66536F1CA47FB5F00C3D35FD ();
// 0x000002D2 System.Void EventDelegate::set_methodName(System.String)
extern void EventDelegate_set_methodName_m7A456BB24021946A5579BF57CC92D75AFDE879B2 ();
// 0x000002D3 EventDelegate_Parameter[] EventDelegate::get_parameters()
extern void EventDelegate_get_parameters_mA66E82D047579F2EC9E83FCAD9C40524F2A97D85 ();
// 0x000002D4 System.Boolean EventDelegate::get_isValid()
extern void EventDelegate_get_isValid_m635C54235E25C5F2F5F74B0B141AD3F980697511 ();
// 0x000002D5 System.Boolean EventDelegate::get_isEnabled()
extern void EventDelegate_get_isEnabled_mBDE1841EB2E68865A46A2FC90B83C71C6566078B ();
// 0x000002D6 System.Void EventDelegate::.ctor()
extern void EventDelegate__ctor_m5ACD5C32F319DB0A1774A43ADD07793B06FF48D9 ();
// 0x000002D7 System.Void EventDelegate::.ctor(EventDelegate_Callback)
extern void EventDelegate__ctor_m3C62112679619038F9B7358C8405E3DFA521A9CE ();
// 0x000002D8 System.Void EventDelegate::.ctor(UnityEngine.MonoBehaviour,System.String)
extern void EventDelegate__ctor_m1741C4C5A485A545E665FEECB04CE9B4A90B1971 ();
// 0x000002D9 System.String EventDelegate::GetMethodName(EventDelegate_Callback)
extern void EventDelegate_GetMethodName_m0BB18FA0518A4F10622DE49BE30171EC319D3CF6 ();
// 0x000002DA System.Boolean EventDelegate::IsValid(EventDelegate_Callback)
extern void EventDelegate_IsValid_mFCD0EF3F90EC174DE6EE4D660DB7F7938FA4DDFC ();
// 0x000002DB System.Boolean EventDelegate::Equals(System.Object)
extern void EventDelegate_Equals_m40396381EDE962BDD8A1F50C35E06DADF0E79000 ();
// 0x000002DC System.Int32 EventDelegate::GetHashCode()
extern void EventDelegate_GetHashCode_mD59E2B2A9A8997ED2112D5FAC2B6D6B628DB883C ();
// 0x000002DD System.Void EventDelegate::Set(EventDelegate_Callback)
extern void EventDelegate_Set_mE7A976C05D1CA3D3DF1CE1F2176BDAB3BE37BF7E ();
// 0x000002DE System.Void EventDelegate::Set(UnityEngine.MonoBehaviour,System.String)
extern void EventDelegate_Set_mA3314D0C08AA946539C6A539C7F1EA027D080488 ();
// 0x000002DF System.Void EventDelegate::Cache()
extern void EventDelegate_Cache_m471C84DF5FEE39FAB8B235EAD46B1E7E59EA56BD ();
// 0x000002E0 System.Boolean EventDelegate::Execute()
extern void EventDelegate_Execute_m9E230204342FD3A857AF2E7B0266061E75FBB1A5 ();
// 0x000002E1 System.Void EventDelegate::Clear()
extern void EventDelegate_Clear_mC23B80E3B455A8616D3A14ECE442409F4F16E5A9 ();
// 0x000002E2 System.String EventDelegate::ToString()
extern void EventDelegate_ToString_m8F383BE25754ED3769B71A0D0D0CCB1811C5F5CC ();
// 0x000002E3 System.Void EventDelegate::Execute(System.Collections.Generic.List`1<EventDelegate>)
extern void EventDelegate_Execute_m7B555B745DAEB6500E03634B1093007BD36776D8 ();
// 0x000002E4 System.Boolean EventDelegate::IsValid(System.Collections.Generic.List`1<EventDelegate>)
extern void EventDelegate_IsValid_m48BDBF4FD26762BFD48E11EC74E7E11CB036ED0B ();
// 0x000002E5 EventDelegate EventDelegate::Set(System.Collections.Generic.List`1<EventDelegate>,EventDelegate_Callback)
extern void EventDelegate_Set_m03EF9C4BE928E0423BD51A39EFBBF1B2D2BB0F72 ();
// 0x000002E6 System.Void EventDelegate::Set(System.Collections.Generic.List`1<EventDelegate>,EventDelegate)
extern void EventDelegate_Set_mF1D2C876A89CAF69516BF8B77FFB1AAB8A0EE675 ();
// 0x000002E7 EventDelegate EventDelegate::Add(System.Collections.Generic.List`1<EventDelegate>,EventDelegate_Callback)
extern void EventDelegate_Add_m05C3EC1B00F45DB2403A862632409B4963A4CE00 ();
// 0x000002E8 EventDelegate EventDelegate::Add(System.Collections.Generic.List`1<EventDelegate>,EventDelegate_Callback,System.Boolean)
extern void EventDelegate_Add_m1FBEFBED683C8C23190887D275D8C95859D75971 ();
// 0x000002E9 System.Void EventDelegate::Add(System.Collections.Generic.List`1<EventDelegate>,EventDelegate)
extern void EventDelegate_Add_m1C5FC53DCB2B17970E30FDF0228DD0200557385E ();
// 0x000002EA System.Void EventDelegate::Add(System.Collections.Generic.List`1<EventDelegate>,EventDelegate,System.Boolean)
extern void EventDelegate_Add_mA778BB95D83429A65992CBBAD670EBB4B8E3674F ();
// 0x000002EB System.Boolean EventDelegate::Remove(System.Collections.Generic.List`1<EventDelegate>,EventDelegate_Callback)
extern void EventDelegate_Remove_mC6D065C13DDEE21813B986611B80ED58D9EE4E90 ();
// 0x000002EC System.Boolean EventDelegate::Remove(System.Collections.Generic.List`1<EventDelegate>,EventDelegate)
extern void EventDelegate_Remove_m65EE562FAC029B4000B555EA359C7572E0F9A8A6 ();
// 0x000002ED System.Void EventDelegate::.cctor()
extern void EventDelegate__cctor_mB40F0710B88A4BC1E033281AAC31A75660125D55 ();
// 0x000002EE System.Collections.Generic.Dictionary`2<System.String,System.String[]> Localization::get_dictionary()
extern void Localization_get_dictionary_mFF8C19565326BB07E2107AAA44BE57382626396F ();
// 0x000002EF System.Void Localization::set_dictionary(System.Collections.Generic.Dictionary`2<System.String,System.String[]>)
extern void Localization_set_dictionary_mE8F643C11684DB5EF671DB78E84131189050005B ();
// 0x000002F0 System.String[] Localization::get_knownLanguages()
extern void Localization_get_knownLanguages_mE4590B5169C400B20AA8F0FC9A9C3149D9537595 ();
// 0x000002F1 System.String Localization::get_language()
extern void Localization_get_language_mADBA19E8969CFECCF39053DCEDB525A11A525707 ();
// 0x000002F2 System.Void Localization::set_language(System.String)
extern void Localization_set_language_m1F5F1680D27449B5D01494CF6F8A14DFCE6C87B5 ();
// 0x000002F3 System.Boolean Localization::Reload()
extern void Localization_Reload_mB33B2338EC994F7D9D833D3A2AB74A61146662C0 ();
// 0x000002F4 System.Boolean Localization::LoadDictionary(System.String,System.Boolean)
extern void Localization_LoadDictionary_mCF9CC76FF322B699B00029989AD865C01E4BBBD1 ();
// 0x000002F5 System.Boolean Localization::LoadAndSelect(System.String)
extern void Localization_LoadAndSelect_m3EEC72108629346072BE2C71E8185BE528DA9FC9 ();
// 0x000002F6 System.Void Localization::Load(UnityEngine.TextAsset)
extern void Localization_Load_m03451F8EC3824FB903581DD4A2FC532267540DCF ();
// 0x000002F7 System.Void Localization::Set(System.String,System.Byte[])
extern void Localization_Set_m8F52E1F48A73F9B1A6F29FC0CF102CFC0DADB3B7 ();
// 0x000002F8 System.Void Localization::ReplaceKey(System.String,System.String)
extern void Localization_ReplaceKey_m34296577488CF411A46BFC15809C8630DD1223A0 ();
// 0x000002F9 System.Void Localization::ClearReplacements()
extern void Localization_ClearReplacements_mDA3A10B66B078FAA00069C604F16301AD3BD2A3D ();
// 0x000002FA System.Boolean Localization::LoadCSV(UnityEngine.TextAsset,System.Boolean)
extern void Localization_LoadCSV_m43DCD17C97D7895FFE4E6071E03E1518439A5F48 ();
// 0x000002FB System.Boolean Localization::LoadCSV(System.Byte[],System.Boolean)
extern void Localization_LoadCSV_mB3ACDFDABB0FBD2D29B3E3199D85B137F5A2CFFF ();
// 0x000002FC System.Boolean Localization::HasLanguage(System.String)
extern void Localization_HasLanguage_m5F50B7C458ECCBC9DCFF17208A911B7A7E3D433F ();
// 0x000002FD System.Boolean Localization::LoadCSV(System.Byte[],UnityEngine.TextAsset,System.Boolean)
extern void Localization_LoadCSV_m4251DE7B38764BE812BD3DC6AC542DBFDFD05EC7 ();
// 0x000002FE System.Void Localization::AddCSV(BetterList`1<System.String>,System.String[],System.Collections.Generic.Dictionary`2<System.String,System.Int32>)
extern void Localization_AddCSV_m977CC734F5FB3CB0DB69EE03E1504A66EC71DE69 ();
// 0x000002FF System.String[] Localization::ExtractStrings(BetterList`1<System.String>,System.String[],System.Collections.Generic.Dictionary`2<System.String,System.Int32>)
extern void Localization_ExtractStrings_mB11F2DA6BF373EAF434B425290032049299084A0 ();
// 0x00000300 System.Boolean Localization::SelectLanguage(System.String)
extern void Localization_SelectLanguage_mE648D7AB86D49836218CCD747E01CFF17B650273 ();
// 0x00000301 System.Void Localization::Set(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void Localization_Set_mCC12AF0DA8490FD29D53D3DABB884C108425BCEA ();
// 0x00000302 System.Void Localization::Set(System.String,System.String)
extern void Localization_Set_m17FB27B9C1CD4C77C7AAFB389E68C1DCA3C97F09 ();
// 0x00000303 System.Boolean Localization::Has(System.String)
extern void Localization_Has_m460661310031466BB615F2F992D5007AB753835E ();
// 0x00000304 System.String Localization::Get(System.String,System.Boolean)
extern void Localization_Get_m71CC6E0E507E872895945D6B388F7FAC5024913E ();
// 0x00000305 System.String Localization::Format(System.String,System.Object)
extern void Localization_Format_m49B9F233C30ED48145A93CA9A19099336AE8C3DE ();
// 0x00000306 System.String Localization::Format(System.String,System.Object,System.Object)
extern void Localization_Format_mC66AC7E9CB915F8ED574AC4F82F444B93A79D5B1 ();
// 0x00000307 System.String Localization::Format(System.String,System.Object,System.Object,System.Object)
extern void Localization_Format_mF6FB2474A569C8FB1BBCB6153C66DEF4177AFD08 ();
// 0x00000308 System.String Localization::Format(System.String,System.Object[])
extern void Localization_Format_m5A368A8E2B17350ADB073BFC4D0B9B89A920EFD6 ();
// 0x00000309 System.Boolean Localization::get_isActive()
extern void Localization_get_isActive_mD584542029D9FD1B0E79B606EF26F56DDADEC07A ();
// 0x0000030A System.String Localization::Localize(System.String)
extern void Localization_Localize_mD3DCDD1267C199CFFB61426D19ADEBF3C8DE6198 ();
// 0x0000030B System.Boolean Localization::Exists(System.String)
extern void Localization_Exists_m7B989D009B9976E6B461709265B68DD11CD64EA3 ();
// 0x0000030C System.Void Localization::Set(System.String,System.String,System.String)
extern void Localization_Set_mBE0687CFE601D8635670BCC21FE3D87693BCCDA2 ();
// 0x0000030D System.Void Localization::.cctor()
extern void Localization__cctor_m7C61BF1A06EAFAEC19C732AA4AAEC1A24EC10F4E ();
// 0x0000030E System.Void MinMaxRangeAttribute::.ctor(System.Single,System.Single)
extern void MinMaxRangeAttribute__ctor_m4822F2C1A15E7655CA6CAA98274FEFEA0AEF6BB1 ();
// 0x0000030F System.Boolean NGUIDebug::get_debugRaycast()
extern void NGUIDebug_get_debugRaycast_m7B6339838BA680A6F8FAE912CF8520FC0576B7F1 ();
// 0x00000310 System.Void NGUIDebug::set_debugRaycast(System.Boolean)
extern void NGUIDebug_set_debugRaycast_m91F3BF5053C67912F320B253370858F65D29CA73 ();
// 0x00000311 System.Void NGUIDebug::CreateInstance()
extern void NGUIDebug_CreateInstance_m0763327AC4776C0A2A129B194FDA044968ADBF2E ();
// 0x00000312 System.Void NGUIDebug::LogString(System.String)
extern void NGUIDebug_LogString_m2B9C97B9C80339CE18357E45CCAE5C88C757407B ();
// 0x00000313 System.Void NGUIDebug::Log(System.Object[])
extern void NGUIDebug_Log_m764C362ECAD787205A481F1F579F4636B4932C8F ();
// 0x00000314 System.Void NGUIDebug::Log(System.String)
extern void NGUIDebug_Log_m2CFC6A766618564F72A605D59DE3F35D7F9B1EF8 ();
// 0x00000315 System.Void NGUIDebug::Clear()
extern void NGUIDebug_Clear_m468D4B1B97B70BC83E2D61D1529D53681B6DF3E4 ();
// 0x00000316 System.Void NGUIDebug::DrawBounds(UnityEngine.Bounds)
extern void NGUIDebug_DrawBounds_m350A38CAEF75B00AB9B49DBF6FD064913A598B11 ();
// 0x00000317 System.Void NGUIDebug::OnGUI()
extern void NGUIDebug_OnGUI_mCC04476B04DBE3B5E2CB51384F787D296B741338 ();
// 0x00000318 System.Void NGUIDebug::.ctor()
extern void NGUIDebug__ctor_m76E094AD4979720C242DC5430F8D1ACE55AED195 ();
// 0x00000319 System.Void NGUIDebug::.cctor()
extern void NGUIDebug__cctor_mA2137581359544F4A3EC5C04263B5B49A577D880 ();
// 0x0000031A System.Single NGUIMath::Lerp(System.Single,System.Single,System.Single)
extern void NGUIMath_Lerp_m2EED4A1074799416C2CFA8FDB87A237DDEAC467E ();
// 0x0000031B System.Int32 NGUIMath::ClampIndex(System.Int32,System.Int32)
extern void NGUIMath_ClampIndex_m7EE0BC19807698B1E5CBDC1F7B0EC3A19352499D ();
// 0x0000031C System.Int32 NGUIMath::RepeatIndex(System.Int32,System.Int32)
extern void NGUIMath_RepeatIndex_m368D29F79815222AD1DFFEF08F0CD29AECB58B71 ();
// 0x0000031D System.Single NGUIMath::WrapAngle(System.Single)
extern void NGUIMath_WrapAngle_mBDB1BE35C813A501B2CF741A76DA6EC5F65EADCA ();
// 0x0000031E System.Single NGUIMath::Wrap01(System.Single)
extern void NGUIMath_Wrap01_mDFB9E75D7AC3EF27636A73B03F7520F6677DA5D2 ();
// 0x0000031F System.Int32 NGUIMath::HexToDecimal(System.Char)
extern void NGUIMath_HexToDecimal_mBBC3AEEBF2A228355D182DC89ABFFB0907712979 ();
// 0x00000320 System.Char NGUIMath::DecimalToHexChar(System.Int32)
extern void NGUIMath_DecimalToHexChar_mE11A73B7E013C4C7DEAAA1B2CFF1F6FF173AB430 ();
// 0x00000321 System.String NGUIMath::DecimalToHex8(System.Int32)
extern void NGUIMath_DecimalToHex8_m75865AF6898DB774B134AC1F0A1F760E3F5E41D7 ();
// 0x00000322 System.String NGUIMath::DecimalToHex24(System.Int32)
extern void NGUIMath_DecimalToHex24_m0B7CF1ACE3A9EEDB133B8FDA7574ECE3D0247DA6 ();
// 0x00000323 System.String NGUIMath::DecimalToHex32(System.Int32)
extern void NGUIMath_DecimalToHex32_m7C2CCC1305527FA9F37B0D810AF70342324B46E6 ();
// 0x00000324 System.Int32 NGUIMath::ColorToInt(UnityEngine.Color)
extern void NGUIMath_ColorToInt_m3CBDFF71F0BF42CF6F69D44632359EFD6A570F88 ();
// 0x00000325 UnityEngine.Color NGUIMath::IntToColor(System.Int32)
extern void NGUIMath_IntToColor_mD7622360E472860D708B0C76F4BE05B7DA2FCEBC ();
// 0x00000326 System.String NGUIMath::IntToBinary(System.Int32,System.Int32)
extern void NGUIMath_IntToBinary_m33DB0591E46D47B4FD2423489FB0D119061F467A ();
// 0x00000327 UnityEngine.Color NGUIMath::HexToColor(System.UInt32)
extern void NGUIMath_HexToColor_mFEA0304A100B6077081374CB207FEAA0337F16D9 ();
// 0x00000328 UnityEngine.Rect NGUIMath::ConvertToTexCoords(UnityEngine.Rect,System.Int32,System.Int32)
extern void NGUIMath_ConvertToTexCoords_mAB40AFD9262379C4240BDBEC43330C012D0EBA51 ();
// 0x00000329 UnityEngine.Rect NGUIMath::ConvertToPixels(UnityEngine.Rect,System.Int32,System.Int32,System.Boolean)
extern void NGUIMath_ConvertToPixels_m1EBD6AC96038346B0979E2D43947E0744E7F89A9 ();
// 0x0000032A UnityEngine.Rect NGUIMath::MakePixelPerfect(UnityEngine.Rect)
extern void NGUIMath_MakePixelPerfect_m1E5D10A2F3EA9A4FB7E1A4774053D1C2EC5F3607 ();
// 0x0000032B UnityEngine.Rect NGUIMath::MakePixelPerfect(UnityEngine.Rect,System.Int32,System.Int32)
extern void NGUIMath_MakePixelPerfect_m4CE089AFAFDF677309B131EB3824130479E43133 ();
// 0x0000032C UnityEngine.Vector2 NGUIMath::ConstrainRect(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2)
extern void NGUIMath_ConstrainRect_mE4D2859AE2B58FA1CD60A59FEADB26D4FE7093F7 ();
// 0x0000032D UnityEngine.Bounds NGUIMath::CalculateAbsoluteWidgetBounds(UnityEngine.Transform)
extern void NGUIMath_CalculateAbsoluteWidgetBounds_m20840230FC0377284C55A966B1398F6396BC4036 ();
// 0x0000032E UnityEngine.Bounds NGUIMath::CalculateRelativeWidgetBounds(UnityEngine.Transform)
extern void NGUIMath_CalculateRelativeWidgetBounds_m6765D9DB4BA7AE48402A5599A9EB1C5BBCD88A2D ();
// 0x0000032F UnityEngine.Bounds NGUIMath::CalculateRelativeWidgetBounds(UnityEngine.Transform,System.Boolean)
extern void NGUIMath_CalculateRelativeWidgetBounds_mD1628188670A35267678BA4C7F7A9712C6D21229 ();
// 0x00000330 UnityEngine.Bounds NGUIMath::CalculateRelativeWidgetBounds(UnityEngine.Transform,UnityEngine.Transform)
extern void NGUIMath_CalculateRelativeWidgetBounds_mECD250EF93DF225D203CB3CB915AFBDBDB65837B ();
// 0x00000331 UnityEngine.Bounds NGUIMath::CalculateRelativeWidgetBounds(UnityEngine.Transform,UnityEngine.Transform,System.Boolean,System.Boolean)
extern void NGUIMath_CalculateRelativeWidgetBounds_m3EFA53D7118C36252444564139163860A69D6BD2 ();
// 0x00000332 System.Void NGUIMath::CalculateRelativeWidgetBounds(UnityEngine.Transform,System.Boolean,System.Boolean,UnityEngine.Matrix4x4&,UnityEngine.Vector3&,UnityEngine.Vector3&,System.Boolean&,System.Boolean)
extern void NGUIMath_CalculateRelativeWidgetBounds_m208D60E5212CAD43A40A44A0A92A56D6CBFC1D84 ();
// 0x00000333 UnityEngine.Vector3 NGUIMath::SpringDampen(UnityEngine.Vector3&,System.Single,System.Single)
extern void NGUIMath_SpringDampen_m16C4F9D01E9FC95A74DFD4B09FED5BDDCFC42D5F ();
// 0x00000334 UnityEngine.Vector2 NGUIMath::SpringDampen(UnityEngine.Vector2&,System.Single,System.Single)
extern void NGUIMath_SpringDampen_m0ADC6C40DE14969744C4BDD81677B5CE801787E8 ();
// 0x00000335 System.Single NGUIMath::SpringLerp(System.Single,System.Single)
extern void NGUIMath_SpringLerp_mE6EA4341572FB93DBF7DC36000F5D36F44F34715 ();
// 0x00000336 System.Single NGUIMath::SpringLerp(System.Single,System.Single,System.Single,System.Single)
extern void NGUIMath_SpringLerp_m437999C602267292344F929141FAC1DE756ABD57 ();
// 0x00000337 UnityEngine.Vector2 NGUIMath::SpringLerp(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Single)
extern void NGUIMath_SpringLerp_mA5E775C9C8E24C83580FF72DC92B60CB6E3EBEA0 ();
// 0x00000338 UnityEngine.Vector3 NGUIMath::SpringLerp(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single)
extern void NGUIMath_SpringLerp_m129F1E552965EDFA853295EDB6D9DFB585CBFCF5 ();
// 0x00000339 UnityEngine.Quaternion NGUIMath::SpringLerp(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single,System.Single)
extern void NGUIMath_SpringLerp_mCBD6397870CC8132AAE1ABA05A5429F639ABEF54 ();
// 0x0000033A System.Single NGUIMath::RotateTowards(System.Single,System.Single,System.Single)
extern void NGUIMath_RotateTowards_m4481031E35F471F0B7767C9643060CB22A4E46BA ();
// 0x0000033B System.Single NGUIMath::DistancePointToLineSegment(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2)
extern void NGUIMath_DistancePointToLineSegment_m625089FF3C4B943FADE141E7CAC417F1180966CB ();
// 0x0000033C System.Single NGUIMath::DistanceToRectangle(UnityEngine.Vector2[],UnityEngine.Vector2)
extern void NGUIMath_DistanceToRectangle_m030D10A1651544F2F132EFCEC60668EC786E2D5C ();
// 0x0000033D System.Single NGUIMath::DistanceToRectangle(UnityEngine.Vector3[],UnityEngine.Vector2,UnityEngine.Camera)
extern void NGUIMath_DistanceToRectangle_m3F4215071D4CE8DCA1A33722E84233F8A5E24E88 ();
// 0x0000033E UnityEngine.Vector2 NGUIMath::GetPivotOffset(UIWidget_Pivot)
extern void NGUIMath_GetPivotOffset_m40AF14CF6F049D898A91B2656F815B7057C4C0CF ();
// 0x0000033F UIWidget_Pivot NGUIMath::GetPivot(UnityEngine.Vector2)
extern void NGUIMath_GetPivot_m3668F84D0DA993DE33E3F5CB7D9C6B8D0CABE25C ();
// 0x00000340 System.Void NGUIMath::MoveWidget(UIRect,System.Single,System.Single)
extern void NGUIMath_MoveWidget_m9EBC47ECB2BF2C5FA47377000B4134AFCA48E95A ();
// 0x00000341 System.Void NGUIMath::MoveRect(UIRect,System.Single,System.Single)
extern void NGUIMath_MoveRect_mBC4206AC00AE46EF8316D8946449255C19A00806 ();
// 0x00000342 System.Void NGUIMath::ResizeWidget(UIWidget,UIWidget_Pivot,System.Single,System.Single,System.Int32,System.Int32)
extern void NGUIMath_ResizeWidget_m973F23A481B05C9AE3C8FD59989853463ACDAF28 ();
// 0x00000343 System.Void NGUIMath::ResizeWidget(UIWidget,UIWidget_Pivot,System.Single,System.Single,System.Int32,System.Int32,System.Int32,System.Int32)
extern void NGUIMath_ResizeWidget_mE33235768C91781D6180C15F137387D4321F75C9 ();
// 0x00000344 System.Void NGUIMath::AdjustWidget(UIWidget,System.Single,System.Single,System.Single,System.Single)
extern void NGUIMath_AdjustWidget_mA32AB12D8F097F18115D1201C9024DD7F88CD40A ();
// 0x00000345 System.Void NGUIMath::AdjustWidget(UIWidget,System.Single,System.Single,System.Single,System.Single,System.Int32,System.Int32)
extern void NGUIMath_AdjustWidget_mDDEB41C0D8A85130B54490D1D5D0C56D067052B4 ();
// 0x00000346 System.Void NGUIMath::AdjustWidget(UIWidget,System.Single,System.Single,System.Single,System.Single,System.Int32,System.Int32,System.Int32,System.Int32)
extern void NGUIMath_AdjustWidget_m4C912A48D2C859780C64394F55B2371DAB77F78A ();
// 0x00000347 System.Int32 NGUIMath::AdjustByDPI(System.Single)
extern void NGUIMath_AdjustByDPI_mA03EDDDE3B31DC484054CD507C9D06CBE557E875 ();
// 0x00000348 UnityEngine.Vector2 NGUIMath::ScreenToPixels(UnityEngine.Vector2,UnityEngine.Transform)
extern void NGUIMath_ScreenToPixels_m8155245ED90B4FCB19EB0F00C7421501E9010F08 ();
// 0x00000349 UnityEngine.Vector2 NGUIMath::ScreenToParentPixels(UnityEngine.Vector2,UnityEngine.Transform)
extern void NGUIMath_ScreenToParentPixels_mFF790DBD884F1AE1117BBA81EF480D41A9EE2F69 ();
// 0x0000034A UnityEngine.Vector3 NGUIMath::WorldToLocalPoint(UnityEngine.Vector3,UnityEngine.Camera,UnityEngine.Camera,UnityEngine.Transform)
extern void NGUIMath_WorldToLocalPoint_m4C0D9B131E366654B1B851B7F72CCB6417756EEC ();
// 0x0000034B System.Void NGUIMath::OverlayPosition(UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.Camera,UnityEngine.Camera)
extern void NGUIMath_OverlayPosition_m442BE422B85C6EB870FDC763D8E02FEF2BE1EF4B ();
// 0x0000034C System.Void NGUIMath::OverlayPosition(UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.Camera)
extern void NGUIMath_OverlayPosition_m70BEAA845E1B4F02F90A2EF30F87BDFC6B3C39D9 ();
// 0x0000034D System.Void NGUIMath::OverlayPosition(UnityEngine.Transform,UnityEngine.Transform)
extern void NGUIMath_OverlayPosition_m2DEBC646044C5B528A9542C9872685C9E00F0044 ();
// 0x0000034E System.Boolean NGUIText::get_isDynamic()
extern void NGUIText_get_isDynamic_m780510753DE4882F7AF1C4DD2C3EC1E29A8494F2 ();
// 0x0000034F System.Void NGUIText::Update()
extern void NGUIText_Update_mE369A19305ED9342FB6E2A98152BE461452EBDE0 ();
// 0x00000350 System.Void NGUIText::Update(System.Boolean)
extern void NGUIText_Update_m69C14C6289E33186C52365E0A44028CFDC6C21F6 ();
// 0x00000351 System.Void NGUIText::Prepare(System.String)
extern void NGUIText_Prepare_m9A9984E177BA6C03004ACD6F1E222CA983600C2E ();
// 0x00000352 BMSymbol NGUIText::GetSymbol(System.String,System.Int32,System.Int32)
extern void NGUIText_GetSymbol_mD98969ECBC92C8B79B9EB310DBEB4FA624040C43 ();
// 0x00000353 System.Single NGUIText::GetGlyphWidth(System.Int32,System.Int32,System.Single)
extern void NGUIText_GetGlyphWidth_mD4416AEA2814E7329737102D83BE4FB69EC8AA1A ();
// 0x00000354 NGUIText_GlyphInfo NGUIText::GetGlyph(System.Int32,System.Int32,System.Single)
extern void NGUIText_GetGlyph_m24C826AFAD5F0F682B4F95938C7298F74D68C0A8 ();
// 0x00000355 System.Single NGUIText::ParseAlpha(System.String,System.Int32)
extern void NGUIText_ParseAlpha_m9B835C744BFC2DC351249E970DD36FF270F3C366 ();
// 0x00000356 UnityEngine.Color NGUIText::ParseColor(System.String,System.Int32)
extern void NGUIText_ParseColor_m225EC449AB5C5F1D7B30113B7531FB7AFA40B20A ();
// 0x00000357 UnityEngine.Color NGUIText::ParseColor24(System.String,System.Int32)
extern void NGUIText_ParseColor24_mF89976BF321124A8A1854565CCD44F00D920B7DE ();
// 0x00000358 UnityEngine.Color NGUIText::ParseColor32(System.String,System.Int32)
extern void NGUIText_ParseColor32_mB62FDEA11F9E679EA64317C3D9BBCBCC7C73F456 ();
// 0x00000359 System.String NGUIText::EncodeColor(UnityEngine.Color)
extern void NGUIText_EncodeColor_m011A9601C09CC5FA3000CAA842CAEC7379627F20 ();
// 0x0000035A System.String NGUIText::EncodeColor(System.String,UnityEngine.Color)
extern void NGUIText_EncodeColor_mF8C970A6835B583AAB973FAD7D6983F428A3BF2A ();
// 0x0000035B System.String NGUIText::EncodeAlpha(System.Single)
extern void NGUIText_EncodeAlpha_mE25E4FC809E3A5A59B28C37F0A4D0F7A1B7E8D6B ();
// 0x0000035C System.String NGUIText::EncodeColor24(UnityEngine.Color)
extern void NGUIText_EncodeColor24_m235CEBE72CE923B37AFEB5D2DE73AC8FFFEFEC02 ();
// 0x0000035D System.String NGUIText::EncodeColor32(UnityEngine.Color)
extern void NGUIText_EncodeColor32_m0FDD28904C23461D2C6B78585BBE0064500642C2 ();
// 0x0000035E System.Boolean NGUIText::ParseSymbol(System.String,System.Int32&)
extern void NGUIText_ParseSymbol_m0337AE49871B56C70921B9784958CE580DAD8944 ();
// 0x0000035F System.Boolean NGUIText::IsHex(System.Char)
extern void NGUIText_IsHex_m470ADC792E8979C599D07E20F397A0FFE95D10F2 ();
// 0x00000360 System.Boolean NGUIText::ParseSymbol(System.String,System.Int32&,BetterList`1<UnityEngine.Color>,System.Boolean,System.Int32&,System.Boolean&,System.Boolean&,System.Boolean&,System.Boolean&,System.Boolean&)
extern void NGUIText_ParseSymbol_mCD0ED77878630C02B324B806DB3F8D44391A82DC ();
// 0x00000361 System.String NGUIText::StripSymbols(System.String)
extern void NGUIText_StripSymbols_mB13F3FCED52C5FC7C5205783327890DC868A2A67 ();
// 0x00000362 System.Void NGUIText::Align(System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Int32,System.Single,System.Int32)
extern void NGUIText_Align_m3413DCE9025E2C1D8D019DA6D42C569F7FA30A51 ();
// 0x00000363 System.Int32 NGUIText::GetExactCharacterIndex(System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<System.Int32>,UnityEngine.Vector2)
extern void NGUIText_GetExactCharacterIndex_m77BD5AF911D011DFBC83980E1510956A16053EDA ();
// 0x00000364 System.Int32 NGUIText::GetApproximateCharacterIndex(System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<System.Int32>,UnityEngine.Vector2)
extern void NGUIText_GetApproximateCharacterIndex_m0E3F211380066E85EC3E505E7FA902B0B935A242 ();
// 0x00000365 System.Boolean NGUIText::IsSpace(System.Int32)
extern void NGUIText_IsSpace_mD2CEA5A154FF53C705FC8461953040FED79DFE31 ();
// 0x00000366 System.Void NGUIText::EndLine(System.Text.StringBuilder&)
extern void NGUIText_EndLine_m9615733C96E90E4D4E2DBFAEBD9B7D0C8AD5F31D ();
// 0x00000367 System.Void NGUIText::ReplaceSpaceWithNewline(System.Text.StringBuilder&)
extern void NGUIText_ReplaceSpaceWithNewline_mEAD85D43D2DEEB91C7B1B654B22C896270C1E0D7 ();
// 0x00000368 UnityEngine.Vector2 NGUIText::CalculatePrintedSize(System.String)
extern void NGUIText_CalculatePrintedSize_mF99DB023DEA045B412D44A620263FE98BCFA8A1C ();
// 0x00000369 System.Int32 NGUIText::CalculateOffsetToFit(System.String)
extern void NGUIText_CalculateOffsetToFit_m62C351293EE347F5554A04614C67B15F3C3A81B5 ();
// 0x0000036A System.String NGUIText::GetEndOfLineThatFits(System.String)
extern void NGUIText_GetEndOfLineThatFits_mFF6FA627178687096C2CABC37F15B205712C9476 ();
// 0x0000036B System.Boolean NGUIText::WrapText(System.String,System.String&,System.Boolean)
extern void NGUIText_WrapText_m1A0A9B4777FA039F301810ED5D9EF00211C5E73C ();
// 0x0000036C System.Boolean NGUIText::WrapText(System.String,System.String&,System.Boolean,System.Boolean,System.Boolean)
extern void NGUIText_WrapText_m4AB7C61367E1EF8FAA4D74B02A5ABA18A7F3F9ED ();
// 0x0000036D System.Void NGUIText::Print(System.String,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Color>)
extern void NGUIText_Print_m371BBA726ACEAB5020C39F15B9046C883F0765D5 ();
// 0x0000036E System.Void NGUIText::PrintApproximateCharacterPositions(System.String,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<System.Int32>)
extern void NGUIText_PrintApproximateCharacterPositions_m72278C40BC9DE14F86EA26C90DFA207386EE2FAA ();
// 0x0000036F System.Void NGUIText::PrintExactCharacterPositions(System.String,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<System.Int32>)
extern void NGUIText_PrintExactCharacterPositions_m21527F461DDFA756363BD8A763D71555F1CFBB3A ();
// 0x00000370 System.Void NGUIText::PrintCaretAndSelection(System.String,System.Int32,System.Int32,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern void NGUIText_PrintCaretAndSelection_mFE0865718C9A689E7D263E134DA405C1B568A62D ();
// 0x00000371 System.Boolean NGUIText::ReplaceLink(System.String&,System.Int32&,System.String,System.String,System.String)
extern void NGUIText_ReplaceLink_mF63BC9A5B1A713444BB1572E026DD60CDA57EBA7 ();
// 0x00000372 System.Boolean NGUIText::InsertHyperlink(System.String&,System.Int32&,System.String,System.String,System.String,System.String)
extern void NGUIText_InsertHyperlink_mB791B0473D15DC9BB3B3D4EF9BA4E952CD811852 ();
// 0x00000373 System.Void NGUIText::ReplaceLinks(System.String&,System.String,System.String)
extern void NGUIText_ReplaceLinks_m745CDD8F505B22369E0E6139A7799BD98EB4C7E7 ();
// 0x00000374 System.Void NGUIText::.cctor()
extern void NGUIText__cctor_mBD477BFB0F1905FCD16A32FACD183CD848BD76C8 ();
// 0x00000375 System.Void DoNotObfuscateNGUI::.ctor()
extern void DoNotObfuscateNGUI__ctor_m15746C13EEFFCFB36FDEA5050E96CDB075773B6E ();
// 0x00000376 System.Single NGUITools::get_soundVolume()
extern void NGUITools_get_soundVolume_m8B36EA3014465E4E4E68CEE829186908CF8897B2 ();
// 0x00000377 System.Void NGUITools::set_soundVolume(System.Single)
extern void NGUITools_set_soundVolume_m076BA16321F46BCB67D1A4C0A4E4C7EBAC87C42B ();
// 0x00000378 System.Boolean NGUITools::get_fileAccess()
extern void NGUITools_get_fileAccess_m6DB81B3644EA842EA31FDB23682FD2F774076A59 ();
// 0x00000379 UnityEngine.AudioSource NGUITools::PlaySound(UnityEngine.AudioClip)
extern void NGUITools_PlaySound_m13921F71D25334857BD4FC597C9D71FED6987952 ();
// 0x0000037A UnityEngine.AudioSource NGUITools::PlaySound(UnityEngine.AudioClip,System.Single)
extern void NGUITools_PlaySound_mE505C2D1EF05FEDA1C0BF2A766B8CA44DF9362B5 ();
// 0x0000037B UnityEngine.AudioSource NGUITools::PlaySound(UnityEngine.AudioClip,System.Single,System.Single)
extern void NGUITools_PlaySound_m627B89CD6021BDC4EB52FACDC2C43193A0B8FF82 ();
// 0x0000037C System.Int32 NGUITools::RandomRange(System.Int32,System.Int32)
extern void NGUITools_RandomRange_m8CDD0EF7BF81569C2CD5DA5FDF3C7865E09E62C9 ();
// 0x0000037D System.String NGUITools::GetHierarchy(UnityEngine.GameObject)
extern void NGUITools_GetHierarchy_m83DE7FB3C71FE4688CFB53A75853B5CDF022E1CA ();
// 0x0000037E T[] NGUITools::FindActive()
// 0x0000037F UnityEngine.Camera NGUITools::FindCameraForLayer(System.Int32)
extern void NGUITools_FindCameraForLayer_m330D0A272025D6790CCF61B63D30F84768B0C8A2 ();
// 0x00000380 System.Void NGUITools::AddWidgetCollider(UnityEngine.GameObject)
extern void NGUITools_AddWidgetCollider_m8CFBB0F5268602E760059475FD4EE58E566647EC ();
// 0x00000381 System.Void NGUITools::AddWidgetCollider(UnityEngine.GameObject,System.Boolean)
extern void NGUITools_AddWidgetCollider_m4EC12D8F558528F2361C9FD32C3CDE3F34B6A8E4 ();
// 0x00000382 System.Void NGUITools::UpdateWidgetCollider(UnityEngine.GameObject)
extern void NGUITools_UpdateWidgetCollider_m11B3A31EFF911C0F2AF4A744AD5608673677B6CD ();
// 0x00000383 System.Void NGUITools::UpdateWidgetCollider(UnityEngine.GameObject,System.Boolean)
extern void NGUITools_UpdateWidgetCollider_m98F16A789D99224D2902A62D958764FC6050090B ();
// 0x00000384 System.Void NGUITools::UpdateWidgetCollider(UnityEngine.BoxCollider,System.Boolean)
extern void NGUITools_UpdateWidgetCollider_mC1D53041E75E8A81684B7B1CDEAD63C44D9F5458 ();
// 0x00000385 System.Void NGUITools::UpdateWidgetCollider(UIWidget)
extern void NGUITools_UpdateWidgetCollider_m5FF73B8CCAC3885C2BF00204537E8662DB4F9D04 ();
// 0x00000386 System.Void NGUITools::UpdateWidgetCollider(UIWidget,UnityEngine.BoxCollider)
extern void NGUITools_UpdateWidgetCollider_m59F646FD254F98ED72AC09DF16AF099FB22C9C31 ();
// 0x00000387 System.Void NGUITools::UpdateWidgetCollider(UIWidget,UnityEngine.BoxCollider2D)
extern void NGUITools_UpdateWidgetCollider_m58BD1B135748D9B205DD695BB5350F7C829EAA27 ();
// 0x00000388 System.Void NGUITools::UpdateWidgetCollider(UnityEngine.BoxCollider2D,System.Boolean)
extern void NGUITools_UpdateWidgetCollider_m62D63D54DDCDF13D5B909C3EA5D7C36851BB95D5 ();
// 0x00000389 System.String NGUITools::GetTypeName()
// 0x0000038A System.String NGUITools::GetTypeName(UnityEngine.Object)
extern void NGUITools_GetTypeName_m27C25D4702E642A45D511F83DF697242E016BA0F ();
// 0x0000038B System.Void NGUITools::RegisterUndo(UnityEngine.Object,System.String)
extern void NGUITools_RegisterUndo_mC3CBCB74073640D5DED8EBA9C6FF65BA74F910DC ();
// 0x0000038C System.Void NGUITools::SetDirty(UnityEngine.Object,System.String)
extern void NGUITools_SetDirty_m56E3BE49DE56664FC27CF194F3F98D1445DDDAC1 ();
// 0x0000038D System.Void NGUITools::CheckForPrefabStage(UnityEngine.GameObject)
extern void NGUITools_CheckForPrefabStage_m52DAAC25CCE8C06D556C58318A55B2225B4FD438 ();
// 0x0000038E UnityEngine.GameObject NGUITools::AddChild(UnityEngine.GameObject)
extern void NGUITools_AddChild_m26744EB18FD0C1EA3EB556D84BF95EB91F180025 ();
// 0x0000038F UnityEngine.GameObject NGUITools::AddChild(UnityEngine.GameObject,System.Int32)
extern void NGUITools_AddChild_m6AA072199772F2A8B24D3F144B35EF9EEF0D96B3 ();
// 0x00000390 UnityEngine.GameObject NGUITools::AddChild(UnityEngine.GameObject,System.Boolean)
extern void NGUITools_AddChild_m33281A355D65241E84D9C21B0D3C7319C0E0D8B0 ();
// 0x00000391 UnityEngine.GameObject NGUITools::AddChild(UnityEngine.GameObject,System.Boolean,System.Int32)
extern void NGUITools_AddChild_mEA942D7347A48418B17EEFF287C0E4B97180EA2F ();
// 0x00000392 UnityEngine.GameObject NGUITools::AddChild(UnityEngine.Transform,UnityEngine.GameObject)
extern void NGUITools_AddChild_m855D0A43C7FBB3A2F4773AF69C173908D89993CF ();
// 0x00000393 UnityEngine.GameObject NGUITools::AddChild(UnityEngine.GameObject,UnityEngine.GameObject)
extern void NGUITools_AddChild_mED3563EE99363B2C1F37B1D3BB0F97C5AA5D8506 ();
// 0x00000394 UnityEngine.GameObject NGUITools::AddChild(UnityEngine.GameObject,UnityEngine.GameObject,System.Int32)
extern void NGUITools_AddChild_m02DAED8952ADFD1606A58555E5850FED5CC3FB39 ();
// 0x00000395 System.Int32 NGUITools::CalculateRaycastDepth(UnityEngine.GameObject)
extern void NGUITools_CalculateRaycastDepth_mF86576E100A96A60CD282825FF79360AF500175E ();
// 0x00000396 System.Int32 NGUITools::CalculateNextDepth(UnityEngine.GameObject)
extern void NGUITools_CalculateNextDepth_m8D47CF3FFE33AC951500D06C76066D7FE3C47D9A ();
// 0x00000397 System.Int32 NGUITools::CalculateNextDepth(UnityEngine.GameObject,System.Boolean)
extern void NGUITools_CalculateNextDepth_m8ADB7C3AE7CE74C9171119BB7E613DEA2DE21139 ();
// 0x00000398 System.Int32 NGUITools::AdjustDepth(UnityEngine.GameObject,System.Int32)
extern void NGUITools_AdjustDepth_m1C7C44D01816FFD49D27E76D509551EB9A54B428 ();
// 0x00000399 System.Void NGUITools::BringForward(UnityEngine.GameObject)
extern void NGUITools_BringForward_mDB83861BC0E52402DCC774B775107776A662C276 ();
// 0x0000039A System.Void NGUITools::PushBack(UnityEngine.GameObject)
extern void NGUITools_PushBack_mE9224A3B30F72C6F60562FEB23AB39AE991011CE ();
// 0x0000039B System.Void NGUITools::NormalizeDepths()
extern void NGUITools_NormalizeDepths_m38FA489E6B23C5AEB3C0AF6EA2F5F153719D16E2 ();
// 0x0000039C System.Void NGUITools::NormalizeWidgetDepths()
extern void NGUITools_NormalizeWidgetDepths_m9F11D11EDAE57043D00C91DAA9FDE30D8978DDBA ();
// 0x0000039D System.Void NGUITools::NormalizeWidgetDepths(UnityEngine.GameObject)
extern void NGUITools_NormalizeWidgetDepths_m0C5D5D0DF42673310C7079D727D25D0FE6D8C1F5 ();
// 0x0000039E System.Void NGUITools::NormalizeWidgetDepths(UIWidget[])
extern void NGUITools_NormalizeWidgetDepths_m1BF8512C755F24A225EFC67AFFD26BBD858677E7 ();
// 0x0000039F System.Void NGUITools::NormalizePanelDepths()
extern void NGUITools_NormalizePanelDepths_mD531F1736A693D4555E7642F67E82136FEC15EFC ();
// 0x000003A0 UIPanel NGUITools::CreateUI(System.Boolean)
extern void NGUITools_CreateUI_m557D90666C2859E14000D4E1450885002EF57175 ();
// 0x000003A1 UIPanel NGUITools::CreateUI(System.Boolean,System.Int32)
extern void NGUITools_CreateUI_m9392D044FA8440EABE8DD0188A9838E5FE9506A9 ();
// 0x000003A2 UIPanel NGUITools::CreateUI(UnityEngine.Transform,System.Boolean,System.Int32)
extern void NGUITools_CreateUI_m025B5545FF7F78C94B786048237882158429B504 ();
// 0x000003A3 System.Void NGUITools::SetChildLayer(UnityEngine.Transform,System.Int32)
extern void NGUITools_SetChildLayer_mBC9096C1694BB3615E3A65E4EA78B294CFDCB1E4 ();
// 0x000003A4 T NGUITools::AddChild(UnityEngine.GameObject)
// 0x000003A5 T NGUITools::AddChild(UnityEngine.GameObject,System.Boolean)
// 0x000003A6 T NGUITools::AddWidget(UnityEngine.GameObject,System.Int32)
// 0x000003A7 UISprite NGUITools::AddSprite(UnityEngine.GameObject,INGUIAtlas,System.String,System.Int32)
extern void NGUITools_AddSprite_m06A7AFEFD7B4DA8E0A0C9A408FA444E4FC761E9B ();
// 0x000003A8 UnityEngine.GameObject NGUITools::GetRoot(UnityEngine.GameObject)
extern void NGUITools_GetRoot_m16F9BE91AD3851F2BD4BF9455AABA0DF63B9FDE1 ();
// 0x000003A9 T NGUITools::FindInParents(UnityEngine.GameObject)
// 0x000003AA T NGUITools::FindInParents(UnityEngine.Transform)
// 0x000003AB System.Void NGUITools::Destroy(UnityEngine.Object)
extern void NGUITools_Destroy_mD9ADA30042B1708E977F9F0B45359CD4943DA9C2 ();
// 0x000003AC System.Void NGUITools::DestroyChildren(UnityEngine.Transform)
extern void NGUITools_DestroyChildren_m61517522CC6B7540469A83EB86896622085AD3E9 ();
// 0x000003AD System.Void NGUITools::DestroyImmediate(UnityEngine.Object)
extern void NGUITools_DestroyImmediate_mD8B1758EBC7454307B05B76887BCFBC68445F111 ();
// 0x000003AE System.Void NGUITools::Broadcast(System.String)
extern void NGUITools_Broadcast_mEC242003D0D28A2EB9C1A65C0494AE72ED0ED0AE ();
// 0x000003AF System.Void NGUITools::Broadcast(System.String,System.Object)
extern void NGUITools_Broadcast_mFE37CF943011EDEADED5F9F25867EB8339E48EC3 ();
// 0x000003B0 System.Boolean NGUITools::IsChild(UnityEngine.Transform,UnityEngine.Transform)
extern void NGUITools_IsChild_m71A1365BB9A0C8BBB3E849ED3A187A6F5337C9BA ();
// 0x000003B1 System.Void NGUITools::Activate(UnityEngine.Transform)
extern void NGUITools_Activate_m2BBEB6AA413580B3F092989BE5D28CB2F7827E7D ();
// 0x000003B2 System.Void NGUITools::Activate(UnityEngine.Transform,System.Boolean)
extern void NGUITools_Activate_mAE71BEA8E563A110B4433A660B259FFD94498B3D ();
// 0x000003B3 System.Void NGUITools::Deactivate(UnityEngine.Transform)
extern void NGUITools_Deactivate_mC619B19A8FEE6D9A26F867C23C513A551DF1FBD3 ();
// 0x000003B4 System.Void NGUITools::SetActive(UnityEngine.GameObject,System.Boolean)
extern void NGUITools_SetActive_mA3F5F2C7B320D7C7A3C6C79E01BA07FD247B5C00 ();
// 0x000003B5 System.Void NGUITools::SetActive(UnityEngine.GameObject,System.Boolean,System.Boolean)
extern void NGUITools_SetActive_m7628B941BF60C630DD3CBB324E0C001F836956C6 ();
// 0x000003B6 System.Void NGUITools::CallCreatePanel(UnityEngine.Transform)
extern void NGUITools_CallCreatePanel_m52EC705DB98219A5642E07F5F4CF5A983CE71FBD ();
// 0x000003B7 System.Void NGUITools::SetActiveChildren(UnityEngine.GameObject,System.Boolean)
extern void NGUITools_SetActiveChildren_m7F43096CF750662B768300E6B14EB6E5542E39BE ();
// 0x000003B8 System.Boolean NGUITools::IsActive(UnityEngine.Behaviour)
extern void NGUITools_IsActive_m1BAD84A628550FA6DC419B19DE684F5567E55EA7 ();
// 0x000003B9 System.Boolean NGUITools::GetActive(UnityEngine.Behaviour)
extern void NGUITools_GetActive_mA792F8D7A43B84E538D372B34D0C571F1DB3CD5A ();
// 0x000003BA System.Boolean NGUITools::GetActive(UnityEngine.GameObject)
extern void NGUITools_GetActive_m9BDF3B08048AA4374F9090241978364AEADACD65 ();
// 0x000003BB System.Void NGUITools::SetActiveSelf(UnityEngine.GameObject,System.Boolean)
extern void NGUITools_SetActiveSelf_m741DCDD7D59423BFF92BD245F6D716D9F624350B ();
// 0x000003BC System.Void NGUITools::SetLayer(UnityEngine.GameObject,System.Int32)
extern void NGUITools_SetLayer_mF54601184A83316756227AB6425A3D3AE50FA464 ();
// 0x000003BD UnityEngine.Vector3 NGUITools::Round(UnityEngine.Vector3)
extern void NGUITools_Round_mFEC5681CFF4269FA8B66E411CB4BECA0166D311D ();
// 0x000003BE System.Void NGUITools::MakePixelPerfect(UnityEngine.Transform)
extern void NGUITools_MakePixelPerfect_m11A3ADA948631461B38871B31A36BF4729E58D62 ();
// 0x000003BF System.Void NGUITools::FitOnScreen(UnityEngine.Camera,UnityEngine.Transform,System.Boolean,System.Boolean)
extern void NGUITools_FitOnScreen_m2FA418A87C8BE7CD77A0168C80819D5E7DF235DA ();
// 0x000003C0 System.Void NGUITools::FitOnScreen(UnityEngine.Camera,UnityEngine.Transform,UnityEngine.Vector3)
extern void NGUITools_FitOnScreen_mF5B07F2E6B28B5D3EDB276ADA1D9A56DEBFC7FA8 ();
// 0x000003C1 System.Void NGUITools::FitOnScreen(UnityEngine.Camera,UnityEngine.Transform,UnityEngine.Transform,UnityEngine.Vector3,System.Boolean)
extern void NGUITools_FitOnScreen_m6E90A47BE5D9A5ADE89A95C29C69970201DFA830 ();
// 0x000003C2 System.Void NGUITools::FitOnScreen(UnityEngine.Camera,UnityEngine.Transform,UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.Bounds&,System.Boolean)
extern void NGUITools_FitOnScreen_m982791D2D71449AF3103DA332C7D45CBF5440BAA ();
// 0x000003C3 System.Boolean NGUITools::Save(System.String,System.Byte[])
extern void NGUITools_Save_m75EBF3CAC8A64AB8796CD89F43535554868DA7D4 ();
// 0x000003C4 System.Byte[] NGUITools::Load(System.String)
extern void NGUITools_Load_m7A4FB722B3A07A81D586F37C183FAB625C133903 ();
// 0x000003C5 UnityEngine.Color NGUITools::ApplyPMA(UnityEngine.Color)
extern void NGUITools_ApplyPMA_m4BCE7D3EF3CD4E3043DE84FF6FC71E0CDF1C030B ();
// 0x000003C6 System.Void NGUITools::MarkParentAsChanged(UnityEngine.GameObject)
extern void NGUITools_MarkParentAsChanged_mECD9DF8737E79B0E8D7E537260449C8B1D66BADE ();
// 0x000003C7 System.String NGUITools::get_clipboard()
extern void NGUITools_get_clipboard_m6B0886D12FF4D2774B8D78BC510FEFC8C3EA146C ();
// 0x000003C8 System.Void NGUITools::set_clipboard(System.String)
extern void NGUITools_set_clipboard_mB52C5201B1ECF57BA90D98640C3A871F811F2BEF ();
// 0x000003C9 System.String NGUITools::EncodeColor(UnityEngine.Color)
extern void NGUITools_EncodeColor_m36EFE9140D9FED4F66D48259AFBD6917E1F9ED60 ();
// 0x000003CA UnityEngine.Color NGUITools::ParseColor(System.String,System.Int32)
extern void NGUITools_ParseColor_m39F32594C7972D9666F2D1709D8FB285A0C0AF94 ();
// 0x000003CB System.String NGUITools::StripSymbols(System.String)
extern void NGUITools_StripSymbols_m4DE0071CCDFC1D4E1B945ACD6E918EAB4E03DC13 ();
// 0x000003CC T NGUITools::AddMissingComponent(UnityEngine.GameObject)
// 0x000003CD UnityEngine.Vector3[] NGUITools::GetSides(UnityEngine.Camera)
extern void NGUITools_GetSides_m8508A7DFF97136776B53C046E4BB80BD7E2CEA88 ();
// 0x000003CE UnityEngine.Vector3[] NGUITools::GetSides(UnityEngine.Camera,System.Single)
extern void NGUITools_GetSides_mC7957AE9AAB137D2615EC5F7B7D966969A667F1D ();
// 0x000003CF UnityEngine.Vector3[] NGUITools::GetSides(UnityEngine.Camera,UnityEngine.Transform)
extern void NGUITools_GetSides_m1059978E86338E089E9C9CD8769B6456ADF267CB ();
// 0x000003D0 UnityEngine.Vector3[] NGUITools::GetSides(UnityEngine.Camera,System.Single,UnityEngine.Transform)
extern void NGUITools_GetSides_m4B27189ECD06B8CBB53C5F7087DA9BC6865C300D ();
// 0x000003D1 UnityEngine.Vector3[] NGUITools::GetWorldCorners(UnityEngine.Camera)
extern void NGUITools_GetWorldCorners_m536DC3D351DE13E1F6CF1D5CD1C89210A547FA64 ();
// 0x000003D2 UnityEngine.Vector3[] NGUITools::GetWorldCorners(UnityEngine.Camera,System.Single)
extern void NGUITools_GetWorldCorners_m3AE6A529ED232232219CA0C3CC49E7C5FF9B93DE ();
// 0x000003D3 UnityEngine.Vector3[] NGUITools::GetWorldCorners(UnityEngine.Camera,UnityEngine.Transform)
extern void NGUITools_GetWorldCorners_m6D62820674215DE638728FA89E8024F9EEE5CEB9 ();
// 0x000003D4 UnityEngine.Vector3[] NGUITools::GetWorldCorners(UnityEngine.Camera,System.Single,UnityEngine.Transform)
extern void NGUITools_GetWorldCorners_mA4113611BDF3B6D9D6E2B94856936D837ADA306F ();
// 0x000003D5 System.String NGUITools::GetFuncName(System.Object,System.String)
extern void NGUITools_GetFuncName_m73534015C7EACB5B30CED8B992A4074ACC860AA2 ();
// 0x000003D6 System.Void NGUITools::Execute(UnityEngine.GameObject,System.String)
// 0x000003D7 System.Void NGUITools::ExecuteAll(UnityEngine.GameObject,System.String)
// 0x000003D8 System.Void NGUITools::ImmediatelyCreateDrawCalls(UnityEngine.GameObject)
extern void NGUITools_ImmediatelyCreateDrawCalls_mFC2097BDDA3ED4DF432DAA0D633814798F43E334 ();
// 0x000003D9 UnityEngine.Vector2 NGUITools::get_screenSize()
extern void NGUITools_get_screenSize_m26698F4FE12B2DD3E4E9AB0D001AAE4FDC0E4546 ();
// 0x000003DA System.String NGUITools::KeyToCaption(UnityEngine.KeyCode)
extern void NGUITools_KeyToCaption_mFD6B276F406358A10EE24A3788D867664BEEC9BF ();
// 0x000003DB UnityEngine.KeyCode NGUITools::CaptionToKey(System.String)
extern void NGUITools_CaptionToKey_mC69B30C9D024DBDC91DE7B3D551C6F397CD6D283 ();
// 0x000003DC T NGUITools::Draw(System.String,NGUITools_OnInitFunc`1<T>)
// 0x000003DD UnityEngine.Color NGUITools::GammaToLinearSpace(UnityEngine.Color)
extern void NGUITools_GammaToLinearSpace_m47F873E42A757A1859A17FFB9B6BEA240CCA2665 ();
// 0x000003DE UnityEngine.Color NGUITools::LinearToGammaSpace(UnityEngine.Color)
extern void NGUITools_LinearToGammaSpace_mC4B2E07A841F7EB74CAD023E14C398361FE7A6E6 ();
// 0x000003DF System.Boolean NGUITools::CheckIfRelated(INGUIAtlas,INGUIAtlas)
extern void NGUITools_CheckIfRelated_m270D412CCEE6F414425078202656693CE8220C01 ();
// 0x000003E0 System.Void NGUITools::Replace(INGUIAtlas,INGUIAtlas)
extern void NGUITools_Replace_m237FCE492C5F2B818791838AC7A7C9BF7A472F36 ();
// 0x000003E1 System.Boolean NGUITools::CheckIfRelated(INGUIFont,INGUIFont)
extern void NGUITools_CheckIfRelated_m103205B040C5B8B1B0087C7CE47A9E037BF381B4 ();
// 0x000003E2 System.Void NGUITools::.cctor()
extern void NGUITools__cctor_mAE14E665B28058BAB815945C5D3ED76F1F49C273 ();
// 0x000003E3 System.Void PropertyBinding::Start()
extern void PropertyBinding_Start_mD9CAFC7B705BA1A90F345AA187704520041AB18F ();
// 0x000003E4 System.Void PropertyBinding::Update()
extern void PropertyBinding_Update_mDDA363BFD1AF3F91890B6E6C0F7C10E5CC737132 ();
// 0x000003E5 System.Void PropertyBinding::LateUpdate()
extern void PropertyBinding_LateUpdate_mC8953375B82664BCE207383C2C093B93EDC17545 ();
// 0x000003E6 System.Void PropertyBinding::FixedUpdate()
extern void PropertyBinding_FixedUpdate_m4952127DC536922ED7B7AC0303684531BB924013 ();
// 0x000003E7 System.Void PropertyBinding::OnValidate()
extern void PropertyBinding_OnValidate_mA902B30FC154921451C3E5F3E1222861FAF3FDD7 ();
// 0x000003E8 System.Void PropertyBinding::UpdateTarget()
extern void PropertyBinding_UpdateTarget_m0528CCC45E246CE2E0FA48679BF34D38FFB0453E ();
// 0x000003E9 System.Void PropertyBinding::.ctor()
extern void PropertyBinding__ctor_m452E88CBCA0CFB1B74555630B6999DBE7C36269F ();
// 0x000003EA UnityEngine.Component PropertyReference::get_target()
extern void PropertyReference_get_target_m26C1C45FDE1517E75A056198FDA5DC8A337B7498 ();
// 0x000003EB System.Void PropertyReference::set_target(UnityEngine.Component)
extern void PropertyReference_set_target_mD9CD8E816FA2D2A0350D4585EACCF770CAB8EDA1 ();
// 0x000003EC System.String PropertyReference::get_name()
extern void PropertyReference_get_name_m6983C9F764BCEE796D25A87E266AB45FAF12412D ();
// 0x000003ED System.Void PropertyReference::set_name(System.String)
extern void PropertyReference_set_name_m4ECCACF8292D1D2D65959195EEECB6D580456C21 ();
// 0x000003EE System.Boolean PropertyReference::get_isValid()
extern void PropertyReference_get_isValid_mBA99CC4CFAFD4234F036CE1FC92F1AE45C75C5F6 ();
// 0x000003EF System.Boolean PropertyReference::get_isEnabled()
extern void PropertyReference_get_isEnabled_mBDEFB1622052F55C31AC2273979DE95CA7EDEC51 ();
// 0x000003F0 System.Void PropertyReference::.ctor()
extern void PropertyReference__ctor_mA7100BA4D5EAB83995BD81DE418C3D99E4C7C3FE ();
// 0x000003F1 System.Void PropertyReference::.ctor(UnityEngine.Component,System.String)
extern void PropertyReference__ctor_m1FC7E50E91AF0F2873D95F6F4FA5BBF5B6A9BB09 ();
// 0x000003F2 System.Type PropertyReference::GetPropertyType()
extern void PropertyReference_GetPropertyType_mDF09D3B305E4F9C534E5452458431AA75F513931 ();
// 0x000003F3 System.Boolean PropertyReference::Equals(System.Object)
extern void PropertyReference_Equals_m31CA1608DF03D4ACA040D2798BC65C4C4B6E9321 ();
// 0x000003F4 System.Int32 PropertyReference::GetHashCode()
extern void PropertyReference_GetHashCode_m616DA1A03A461503716871C3C84DB62DC8F0D652 ();
// 0x000003F5 System.Void PropertyReference::Set(UnityEngine.Component,System.String)
extern void PropertyReference_Set_mC00BAA968F4E8265C8704DD4997366DE06DF993F ();
// 0x000003F6 System.Void PropertyReference::Clear()
extern void PropertyReference_Clear_m6D321CD3E4D4A94D994C0D93FB84A127C7297107 ();
// 0x000003F7 System.Void PropertyReference::Reset()
extern void PropertyReference_Reset_m74D4A8794AEE517BF8B1D8ECD873DFAE20728C6E ();
// 0x000003F8 System.String PropertyReference::ToString()
extern void PropertyReference_ToString_m26483E1A4A796010E8AB723164F55A7C44EDFD8E ();
// 0x000003F9 System.String PropertyReference::ToString(UnityEngine.Component,System.String)
extern void PropertyReference_ToString_m3251091C9B11F87155479E0325E6A654CE0C321B ();
// 0x000003FA System.Object PropertyReference::Get()
extern void PropertyReference_Get_mD1097CB119895113A378F62B9E954B0105FFFA52 ();
// 0x000003FB System.Boolean PropertyReference::Set(System.Object)
extern void PropertyReference_Set_m1B70D84FFDCDE18FDDD8A341CDC3F079F290C9FB ();
// 0x000003FC System.Boolean PropertyReference::Cache()
extern void PropertyReference_Cache_m52615000874052288578C880582A67E269410C28 ();
// 0x000003FD System.Boolean PropertyReference::Convert(System.Object&)
extern void PropertyReference_Convert_m400EFA3A3B8F92CE77336BC77A401ED6ACB525EB ();
// 0x000003FE System.Boolean PropertyReference::Convert(System.Type,System.Type)
extern void PropertyReference_Convert_m37199A7C52CE10DA9E040CCC666D5DC58139D415 ();
// 0x000003FF System.Boolean PropertyReference::Convert(System.Object,System.Type)
extern void PropertyReference_Convert_m5CE6E53BE8A8DDFED08ECAD0C32969CE3FC5894F ();
// 0x00000400 System.Boolean PropertyReference::Convert(System.Object&,System.Type,System.Type)
extern void PropertyReference_Convert_m46315B2118CB3E8D121398E9DA379E5F0563CB6E ();
// 0x00000401 System.Void PropertyReference::.cctor()
extern void PropertyReference__cctor_m9596D23D59E4F76F63B84CC19A28D4D6567587FB ();
// 0x00000402 System.Single RealTime::get_time()
extern void RealTime_get_time_mEEC08DDB8D7C7059920471D5749243D9F7F5008B ();
// 0x00000403 System.Single RealTime::get_deltaTime()
extern void RealTime_get_deltaTime_m83F26A5D9973F1E8C0189D36D2C5E7F0754B0B14 ();
// 0x00000404 System.Void RealTime::.ctor()
extern void RealTime__ctor_m92A02D01407864F2F503A8E3FA0A20F1B0941742 ();
// 0x00000405 System.Void SpringPanel::Start()
extern void SpringPanel_Start_m90CDC1BFAA83C929E62A5D0060A427B58232AC58 ();
// 0x00000406 System.Void SpringPanel::Update()
extern void SpringPanel_Update_mBC20B42931484D676163943C799358BCA829A4FA ();
// 0x00000407 System.Void SpringPanel::AdvanceTowardsPosition()
extern void SpringPanel_AdvanceTowardsPosition_mDE7937BE1037B29C9DF0A86BC57C60B844E9617B ();
// 0x00000408 SpringPanel SpringPanel::Begin(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern void SpringPanel_Begin_mA1D1CD392CDA4DE2E470EA5BE63771F096500B75 ();
// 0x00000409 SpringPanel SpringPanel::Stop(UnityEngine.GameObject)
extern void SpringPanel_Stop_mB4F476CCCDCACB21E2B3FC16DA507C3846FACB75 ();
// 0x0000040A System.Void SpringPanel::.ctor()
extern void SpringPanel__ctor_m8224B71A7D7AB90511A98AD3C2E99709C2611CC9 ();
// 0x0000040B UIBasicSprite_Type UIBasicSprite::get_type()
extern void UIBasicSprite_get_type_m29FF9568B93BD441C5FC949264A9BFC778DEA70A ();
// 0x0000040C System.Void UIBasicSprite::set_type(UIBasicSprite_Type)
extern void UIBasicSprite_set_type_mDB4F7080CB30FEAF7342BC95F60E65539FB44CDA ();
// 0x0000040D UIBasicSprite_Flip UIBasicSprite::get_flip()
extern void UIBasicSprite_get_flip_mE9DE042BB3A55E603713133D04CAC4BB77B39843 ();
// 0x0000040E System.Void UIBasicSprite::set_flip(UIBasicSprite_Flip)
extern void UIBasicSprite_set_flip_mF98BFB561338B5C8BBE5466663E7C4DF12C3D354 ();
// 0x0000040F UIBasicSprite_FillDirection UIBasicSprite::get_fillDirection()
extern void UIBasicSprite_get_fillDirection_mF9E1721DC32A7D9D5C5170FFD03FB118181F3723 ();
// 0x00000410 System.Void UIBasicSprite::set_fillDirection(UIBasicSprite_FillDirection)
extern void UIBasicSprite_set_fillDirection_m5CC00536CC90D9444DEBF72B8995006653585B30 ();
// 0x00000411 System.Single UIBasicSprite::get_fillAmount()
extern void UIBasicSprite_get_fillAmount_m5FB3F2797728A61FF6322BFC603F333EF8818CA8 ();
// 0x00000412 System.Void UIBasicSprite::set_fillAmount(System.Single)
extern void UIBasicSprite_set_fillAmount_m16AB3631F6082CCBC958EF87DF2571B277ED171E ();
// 0x00000413 System.Int32 UIBasicSprite::get_minWidth()
extern void UIBasicSprite_get_minWidth_m6E399B2CAB1B3EDBF694FC3F51EDB28B808B7187 ();
// 0x00000414 System.Int32 UIBasicSprite::get_minHeight()
extern void UIBasicSprite_get_minHeight_mB1553A5B58A580BB6711DF659D4194358B6B7687 ();
// 0x00000415 System.Boolean UIBasicSprite::get_invert()
extern void UIBasicSprite_get_invert_mBDE341041B97AC361C9363AC8899B4B79B595D38 ();
// 0x00000416 System.Void UIBasicSprite::set_invert(System.Boolean)
extern void UIBasicSprite_set_invert_m4B20059B094D00FD55CE0334D18B8A61BEB1FEFF ();
// 0x00000417 System.Boolean UIBasicSprite::get_hasBorder()
extern void UIBasicSprite_get_hasBorder_mFEF250B1396D0DDAB3DBE96F58FEF92B1D92104D ();
// 0x00000418 System.Boolean UIBasicSprite::get_premultipliedAlpha()
extern void UIBasicSprite_get_premultipliedAlpha_m51A1BD83502BF223EEA38E5D0169317795B17EA7 ();
// 0x00000419 System.Single UIBasicSprite::get_pixelSize()
extern void UIBasicSprite_get_pixelSize_m217448D733B5487985117AB9DE59AA94BBAACBFE ();
// 0x0000041A UnityEngine.Vector4 UIBasicSprite::get_padding()
extern void UIBasicSprite_get_padding_mE1DEFC28DE35CBC34C9C93ABB89AF9390D10D209 ();
// 0x0000041B UnityEngine.Vector4 UIBasicSprite::get_drawingUVs()
extern void UIBasicSprite_get_drawingUVs_m49F26ED67F674D1EF77B8977C4A53CDFE28CCA1D ();
// 0x0000041C UnityEngine.Color UIBasicSprite::get_drawingColor()
extern void UIBasicSprite_get_drawingColor_m5941D60718207F3B5DFE460B0A3F19FAB7019C88 ();
// 0x0000041D System.Void UIBasicSprite::Fill(System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Color>,UnityEngine.Rect,UnityEngine.Rect)
extern void UIBasicSprite_Fill_mC8DEC548E594A6F0D455AED31585DCB715612B11 ();
// 0x0000041E System.Void UIBasicSprite::SimpleFill(System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Color>,UnityEngine.Vector4&,UnityEngine.Vector4&,UnityEngine.Color&)
extern void UIBasicSprite_SimpleFill_m9BE49C3B027F4340581BA2B90F6A52C903015507 ();
// 0x0000041F System.Void UIBasicSprite::SlicedFill(System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Color>,UnityEngine.Vector4&,UnityEngine.Vector4&,UnityEngine.Color&)
extern void UIBasicSprite_SlicedFill_mA46200B0DB756115D0A21BE55F995E983F20E79D ();
// 0x00000420 System.Void UIBasicSprite::AddVertexColours(System.Collections.Generic.List`1<UnityEngine.Color>,UnityEngine.Color&,System.Int32,System.Int32)
extern void UIBasicSprite_AddVertexColours_m3EBB3AD76F05573B102240F5C6C7713FA997FA89 ();
// 0x00000421 System.Void UIBasicSprite::TiledFill(System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Color>,UnityEngine.Vector4&,UnityEngine.Color&)
extern void UIBasicSprite_TiledFill_m603771E6CE6A189AA0CFB6DAF7491604F4A79D17 ();
// 0x00000422 System.Void UIBasicSprite::FilledFill(System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Color>,UnityEngine.Vector4&,UnityEngine.Vector4&,UnityEngine.Color&)
extern void UIBasicSprite_FilledFill_m0E56B32B5B1FDC93E5E3F80AFD99E6415A3BD213 ();
// 0x00000423 System.Void UIBasicSprite::AdvancedFill(System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Color>,UnityEngine.Vector4&,UnityEngine.Vector4&,UnityEngine.Color&)
extern void UIBasicSprite_AdvancedFill_m8901C983EE70ECC99EF7AF15EED06C0CB0BC228E ();
// 0x00000424 System.Boolean UIBasicSprite::RadialCut(UnityEngine.Vector2[],UnityEngine.Vector2[],System.Single,System.Boolean,System.Int32)
extern void UIBasicSprite_RadialCut_m0FDE6060DC5D747FE0532B5A1916EB9163F047EF ();
// 0x00000425 System.Void UIBasicSprite::RadialCut(UnityEngine.Vector2[],System.Single,System.Single,System.Boolean,System.Int32)
extern void UIBasicSprite_RadialCut_mD90282CEDF1D3243D8BB53DE8E3E603529E2FBF7 ();
// 0x00000426 System.Void UIBasicSprite::Fill(System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Color>,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,UnityEngine.Color)
extern void UIBasicSprite_Fill_mC02E2AB202D36EF962CCEF28E60D4D1FB5AAA9C8 ();
// 0x00000427 System.Void UIBasicSprite::.ctor()
extern void UIBasicSprite__ctor_m9F4D2EEBC2F7106D7E563855FAB1061822C0040C ();
// 0x00000428 System.Void UIBasicSprite::.cctor()
extern void UIBasicSprite__cctor_mE7682C9273E48A8442D54244E686B7933FB9931B ();
// 0x00000429 BetterList`1<UIDrawCall> UIDrawCall::get_list()
extern void UIDrawCall_get_list_mAA51D26856381B325FFF209F53148B7EDA639A57 ();
// 0x0000042A BetterList`1<UIDrawCall> UIDrawCall::get_activeList()
extern void UIDrawCall_get_activeList_m84068B1AD19D00D0BD5CA87AE5B40D2797030748 ();
// 0x0000042B BetterList`1<UIDrawCall> UIDrawCall::get_inactiveList()
extern void UIDrawCall_get_inactiveList_mB920CB654104145871090A5FE724C26DD2448B94 ();
// 0x0000042C System.Int32 UIDrawCall::get_renderQueue()
extern void UIDrawCall_get_renderQueue_m87C110B9DFD0D4DE598B269DBDC1D295EFACF5D8 ();
// 0x0000042D System.Void UIDrawCall::set_renderQueue(System.Int32)
extern void UIDrawCall_set_renderQueue_m618A6DC9DAB0DDEF635D4881C45DB649B66F0118 ();
// 0x0000042E System.Int32 UIDrawCall::get_sortingOrder()
extern void UIDrawCall_get_sortingOrder_m171E0DD0EF311883D70C7B08C2EDE05979801F44 ();
// 0x0000042F System.Void UIDrawCall::set_sortingOrder(System.Int32)
extern void UIDrawCall_set_sortingOrder_m665E9519E643571503E54005B5BD06E720CD6132 ();
// 0x00000430 System.String UIDrawCall::get_sortingLayerName()
extern void UIDrawCall_get_sortingLayerName_m59938A4F120A40160AAB499FCD02A79E72BAE7DE ();
// 0x00000431 System.Void UIDrawCall::set_sortingLayerName(System.String)
extern void UIDrawCall_set_sortingLayerName_m31F3738F9F0B96E082C5F1C7FB82F3AA26742756 ();
// 0x00000432 System.Int32 UIDrawCall::get_finalRenderQueue()
extern void UIDrawCall_get_finalRenderQueue_mA77D4F632EBDD2E0361FF964008767262520BF55 ();
// 0x00000433 UnityEngine.Transform UIDrawCall::get_cachedTransform()
extern void UIDrawCall_get_cachedTransform_m1A4BC491540E297E21D2A7A127D9F31EC66AE41F ();
// 0x00000434 UnityEngine.Material UIDrawCall::get_baseMaterial()
extern void UIDrawCall_get_baseMaterial_mA6F9A0CE878C29E25FE7AE96B5E263B57CCE7448 ();
// 0x00000435 System.Void UIDrawCall::set_baseMaterial(UnityEngine.Material)
extern void UIDrawCall_set_baseMaterial_m9E6F8EF6355777A5D1EC2E8442A72D6B9E18B13C ();
// 0x00000436 UnityEngine.Material UIDrawCall::get_dynamicMaterial()
extern void UIDrawCall_get_dynamicMaterial_m1AA012B8409EEB8FBDCD5328ABCBB71BA8717B24 ();
// 0x00000437 UnityEngine.Texture UIDrawCall::get_mainTexture()
extern void UIDrawCall_get_mainTexture_mDE560C837E90BE4FCA2383CA09191F4F393BD377 ();
// 0x00000438 System.Void UIDrawCall::set_mainTexture(UnityEngine.Texture)
extern void UIDrawCall_set_mainTexture_m2D33224D302AFD8ABEFE0A7F454BFAB68C135A02 ();
// 0x00000439 UnityEngine.Shader UIDrawCall::get_shader()
extern void UIDrawCall_get_shader_m84C71254C08A93B962BFC1575E5880FEA4142738 ();
// 0x0000043A System.Void UIDrawCall::set_shader(UnityEngine.Shader)
extern void UIDrawCall_set_shader_m81AF5264D64624CB9C00E866E80BEA4A761CE172 ();
// 0x0000043B UIDrawCall_ShadowMode UIDrawCall::get_shadowMode()
extern void UIDrawCall_get_shadowMode_m39B09F5E737ADE903FB8AEB78823BCBB755381F0 ();
// 0x0000043C System.Void UIDrawCall::set_shadowMode(UIDrawCall_ShadowMode)
extern void UIDrawCall_set_shadowMode_mDD7676C2C05532D1B4202049CA4B7A83C3840ED3 ();
// 0x0000043D System.Int32 UIDrawCall::get_triangles()
extern void UIDrawCall_get_triangles_m33965C4D2F6A02615D35C415B913F4D35B49A28E ();
// 0x0000043E System.Boolean UIDrawCall::get_isClipped()
extern void UIDrawCall_get_isClipped_mCFC48B4E6954BBD9488FF5B0B38AAFBCD2DF2368 ();
// 0x0000043F System.Void UIDrawCall::CreateMaterial()
extern void UIDrawCall_CreateMaterial_m8435FC9185576B2B01F9F71128CCDFF4BC147F27 ();
// 0x00000440 UnityEngine.Material UIDrawCall::RebuildMaterial()
extern void UIDrawCall_RebuildMaterial_mD551DFCA8EA4F634C21FD500FCFF4803FD17034C ();
// 0x00000441 System.Void UIDrawCall::UpdateMaterials()
extern void UIDrawCall_UpdateMaterials_mA72BDDC781FD8D97333ABCC8CD3E8DD0281602EA ();
// 0x00000442 System.Void UIDrawCall::UpdateGeometry(System.Int32,System.Boolean)
extern void UIDrawCall_UpdateGeometry_m12702B26C210365F07AE3EDE314F4BC7A338CC81 ();
// 0x00000443 System.Int32[] UIDrawCall::GenerateCachedIndexBuffer(System.Int32,System.Int32)
extern void UIDrawCall_GenerateCachedIndexBuffer_m211BFF47762288D80F1BD0D0C45F8BBFC2507C6A ();
// 0x00000444 System.Void UIDrawCall::OnWillRenderObject()
extern void UIDrawCall_OnWillRenderObject_mD53A340502BFA1C2A8351C1630D6FF65D39C4613 ();
// 0x00000445 System.Void UIDrawCall::SetClipping(System.Int32,UnityEngine.Vector4,UnityEngine.Vector2,System.Single)
extern void UIDrawCall_SetClipping_m8BC425B59C1CC1115FF9365ED9A61460270C8198 ();
// 0x00000446 System.Void UIDrawCall::Awake()
extern void UIDrawCall_Awake_mEB28B18B6CCA80682ABA33421D69A9D42ACA6A3C ();
// 0x00000447 System.Void UIDrawCall::OnEnable()
extern void UIDrawCall_OnEnable_mDD12737CB69291920FE0D0B9FC42D62DD826ED83 ();
// 0x00000448 System.Void UIDrawCall::OnDisable()
extern void UIDrawCall_OnDisable_m514E2D2FA74D43DC2D6651C8D0AF042C350EB7B3 ();
// 0x00000449 System.Void UIDrawCall::OnDestroy()
extern void UIDrawCall_OnDestroy_mF16581CEFBD428187F0BB38134C539D39268AA5C ();
// 0x0000044A UIDrawCall UIDrawCall::Create(UIPanel,UnityEngine.Material,UnityEngine.Texture,UnityEngine.Shader)
extern void UIDrawCall_Create_mE4871F283BB145EDC6ACAED444FB57E7061E13AE ();
// 0x0000044B UIDrawCall UIDrawCall::Create(System.String,UIPanel,UnityEngine.Material,UnityEngine.Texture,UnityEngine.Shader)
extern void UIDrawCall_Create_mADFA7B0C975A6D38765179456F4E9A022CDE996B ();
// 0x0000044C UIDrawCall UIDrawCall::Create(System.String)
extern void UIDrawCall_Create_mD1AA7F6884E16952448094F6D81E43A3F1CAC031 ();
// 0x0000044D System.Void UIDrawCall::ClearAll()
extern void UIDrawCall_ClearAll_mF0DE9E526F92E66EBF483FA0F6CCF8B3B8EAC78E ();
// 0x0000044E System.Void UIDrawCall::ReleaseAll()
extern void UIDrawCall_ReleaseAll_mDD5E0D328A025E6FB9F56B08DCE4883A3748F198 ();
// 0x0000044F System.Void UIDrawCall::ReleaseInactive()
extern void UIDrawCall_ReleaseInactive_mA40CEAAB0610335980ECE349405AAA5A47ABEA0C ();
// 0x00000450 System.Int32 UIDrawCall::Count(UIPanel)
extern void UIDrawCall_Count_m46C70D0447EA1E62433F5E37E54E6C7E0B0C4B65 ();
// 0x00000451 System.Void UIDrawCall::Destroy(UIDrawCall)
extern void UIDrawCall_Destroy_mCD892DCFA60C02A6CD743A2187A07FFDBBC5E4A5 ();
// 0x00000452 System.Void UIDrawCall::MoveToScene(UnityEngine.SceneManagement.Scene)
extern void UIDrawCall_MoveToScene_m29A229ED9AE12CCFD12B3CF834FB0A74D87A99FB ();
// 0x00000453 System.Void UIDrawCall::.ctor()
extern void UIDrawCall__ctor_m6BC9991506E910F83FB56F3BA6F8CD64F75B086B ();
// 0x00000454 System.Void UIDrawCall::.cctor()
extern void UIDrawCall__cctor_mDF5CBC8C3BC96B51BBB50CF19705209A817EE218 ();
// 0x00000455 System.Boolean UIEventListener::get_isColliderEnabled()
extern void UIEventListener_get_isColliderEnabled_m58C64C6ED90803135E0094A41B4B66B01A680D1B ();
// 0x00000456 System.Void UIEventListener::OnSubmit()
extern void UIEventListener_OnSubmit_m42D15B693FFB67630A32B1198B8BFCCB0C1FC989 ();
// 0x00000457 System.Void UIEventListener::OnClick()
extern void UIEventListener_OnClick_mCFC2FFDFC520B0A538142A557616C23412AD6A3E ();
// 0x00000458 System.Void UIEventListener::OnDoubleClick()
extern void UIEventListener_OnDoubleClick_mC68B1193B99A86DC45102DB3FB2065753DAEDF08 ();
// 0x00000459 System.Void UIEventListener::OnHover(System.Boolean)
extern void UIEventListener_OnHover_mB2874768BFA67CF168DAE249A789E5CBD3C52983 ();
// 0x0000045A System.Void UIEventListener::OnPress(System.Boolean)
extern void UIEventListener_OnPress_m666E9E985F4B661E673C54D310F3EA3D6BBDF1F2 ();
// 0x0000045B System.Void UIEventListener::OnSelect(System.Boolean)
extern void UIEventListener_OnSelect_mBDBCFFC0704E34FC5675E71AD84B34B547D7DDD7 ();
// 0x0000045C System.Void UIEventListener::OnScroll(System.Single)
extern void UIEventListener_OnScroll_m51EDED0E28A387501E3F19BF9F382A124777F59E ();
// 0x0000045D System.Void UIEventListener::OnDragStart()
extern void UIEventListener_OnDragStart_mB51C5C3C75BF6D2D81180E28795170F6E0D06638 ();
// 0x0000045E System.Void UIEventListener::OnDrag(UnityEngine.Vector2)
extern void UIEventListener_OnDrag_m9B3B91DB98AB2038B4E8A2E0CD3B86EDB0F9000D ();
// 0x0000045F System.Void UIEventListener::OnDragOver()
extern void UIEventListener_OnDragOver_m3C587416B2D8281A2D0DBAF2200B10252E35398E ();
// 0x00000460 System.Void UIEventListener::OnDragOut()
extern void UIEventListener_OnDragOut_m2CC8C0E96FCAB4AA01BE2FA733EC82909455EF5B ();
// 0x00000461 System.Void UIEventListener::OnDragEnd()
extern void UIEventListener_OnDragEnd_m7E6ACACA000DBE210B55ABD2D20AC775F8E6E84A ();
// 0x00000462 System.Void UIEventListener::OnDrop(UnityEngine.GameObject)
extern void UIEventListener_OnDrop_m59664431C43B6B26C30ACE5C34BFF918A8EC722E ();
// 0x00000463 System.Void UIEventListener::OnKey(UnityEngine.KeyCode)
extern void UIEventListener_OnKey_m20C14FCCD294FD737FE644058F075160B5E4436B ();
// 0x00000464 System.Void UIEventListener::OnTooltip(System.Boolean)
extern void UIEventListener_OnTooltip_mE4787CE6622861C67B3302EFBD045E6A7B0E6802 ();
// 0x00000465 System.Void UIEventListener::Clear()
extern void UIEventListener_Clear_m2C1156719ED590E606A60AFC6D7AB1C9C27BBDEC ();
// 0x00000466 UIEventListener UIEventListener::Get(UnityEngine.GameObject)
extern void UIEventListener_Get_m71055B6D20CD09BC66FE2E355EB299092998CA41 ();
// 0x00000467 System.Void UIEventListener::.ctor()
extern void UIEventListener__ctor_m88FE9E7BC2488312D114EB82FE1B52C03C26ACB4 ();
// 0x00000468 System.Boolean UIGeometry::get_hasVertices()
extern void UIGeometry_get_hasVertices_m722414C27996059EEA1374D02F12F18A26C5E4E4 ();
// 0x00000469 System.Boolean UIGeometry::get_hasTransformed()
extern void UIGeometry_get_hasTransformed_m12119A5B276829468B262A3B3A486CB201C2E36D ();
// 0x0000046A System.Void UIGeometry::Clear()
extern void UIGeometry_Clear_mDB258FE13320B70040D95AA4D1B9B466825F227A ();
// 0x0000046B System.Void UIGeometry::ApplyTransform(UnityEngine.Matrix4x4,System.Boolean)
extern void UIGeometry_ApplyTransform_m71DAA81EA50FB9C3F5276521CA924B61968A0732 ();
// 0x0000046C System.Void UIGeometry::WriteToBuffers(System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Color>,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector4>,System.Collections.Generic.List`1<UnityEngine.Vector4>)
extern void UIGeometry_WriteToBuffers_m667B28DC786D12C544A9209212A13802529FC3F8 ();
// 0x0000046D System.Void UIGeometry::.ctor()
extern void UIGeometry__ctor_m3945EB3D2B37C74A5A1FEC4C992EDCC2E4EA37D1 ();
// 0x0000046E UnityEngine.GameObject UIRect::get_cachedGameObject()
extern void UIRect_get_cachedGameObject_m1A7B6A2EC0DE8F70AE5CC11C935AF89EC8E33F6D ();
// 0x0000046F UnityEngine.Transform UIRect::get_cachedTransform()
extern void UIRect_get_cachedTransform_m8A41449F84B2729D818288E33C95E4C145625BD5 ();
// 0x00000470 UnityEngine.Camera UIRect::get_anchorCamera()
extern void UIRect_get_anchorCamera_m907B5D2030FF5D3E3075B47CC27D99B027C86F3F ();
// 0x00000471 System.Boolean UIRect::get_isFullyAnchored()
extern void UIRect_get_isFullyAnchored_m5A0B7C115AB49AF0E8C297BCCAD39A43B14A24B3 ();
// 0x00000472 System.Boolean UIRect::get_isAnchoredHorizontally()
extern void UIRect_get_isAnchoredHorizontally_m6B309D3B834CE8F0F0E82151E9E15F70666661EE ();
// 0x00000473 System.Boolean UIRect::get_isAnchoredVertically()
extern void UIRect_get_isAnchoredVertically_mBF3BB773CED851B99789C9220B8182E991016B88 ();
// 0x00000474 System.Boolean UIRect::get_canBeAnchored()
extern void UIRect_get_canBeAnchored_m4AC7995E9246046AFB100FA0195EDC011D4B5CE0 ();
// 0x00000475 UIRect UIRect::get_parent()
extern void UIRect_get_parent_mCEA314C6C528BC8A24338F2BBACCB141A39435B7 ();
// 0x00000476 UIRoot UIRect::get_root()
extern void UIRect_get_root_mF478C1D6F5C7307F88E612355586336FAFB7AE3C ();
// 0x00000477 System.Boolean UIRect::get_isAnchored()
extern void UIRect_get_isAnchored_mC1C3AD3C5FCE76CAFB729C22D96EAC8F024E2FF3 ();
// 0x00000478 System.Single UIRect::get_alpha()
// 0x00000479 System.Void UIRect::set_alpha(System.Single)
// 0x0000047A System.Single UIRect::CalculateFinalAlpha(System.Int32)
// 0x0000047B UnityEngine.Vector3[] UIRect::get_localCorners()
// 0x0000047C UnityEngine.Vector3[] UIRect::get_worldCorners()
// 0x0000047D System.Single UIRect::get_cameraRayDistance()
extern void UIRect_get_cameraRayDistance_m9A48AA8DDF69E9D2D866A6175F3C77B0C7911547 ();
// 0x0000047E System.Void UIRect::Invalidate(System.Boolean)
extern void UIRect_Invalidate_m02C0FADEAE441B0595154F013CCCC1CDDB08AB12 ();
// 0x0000047F UnityEngine.Vector3[] UIRect::GetSides(UnityEngine.Transform)
extern void UIRect_GetSides_m27B77E7183E2A4CF39938B7EA3D53E89202FC7BD ();
// 0x00000480 UnityEngine.Vector3 UIRect::GetLocalPos(UIRect_AnchorPoint,UnityEngine.Transform)
extern void UIRect_GetLocalPos_mF222005B8D4547B2EFB11A92BE8479BA2D863416 ();
// 0x00000481 System.Void UIRect::OnEnable()
extern void UIRect_OnEnable_m399CFA9D0D4BCA82B3ABF836B61E4F0A6A0D9DE2 ();
// 0x00000482 System.Void UIRect::OnInit()
extern void UIRect_OnInit_mC7B43D2B22C2DB6084C04FC1C98C1FD18854A3A1 ();
// 0x00000483 System.Void UIRect::OnDisable()
extern void UIRect_OnDisable_m748451148A210DAFC88100CE263932B84AA3AFED ();
// 0x00000484 System.Void UIRect::Awake()
extern void UIRect_Awake_mCF2BF0CF81FACEDC52245B9FC4B5A17A9C384D13 ();
// 0x00000485 System.Void UIRect::Start()
extern void UIRect_Start_m4AA2087E77AFFD4D5E083F831778C75715DDFE40 ();
// 0x00000486 System.Void UIRect::Update()
extern void UIRect_Update_m95C76C26D889B788CD71B50C85072F59F5CA1D93 ();
// 0x00000487 System.Void UIRect::UpdateAnchorsInternal(System.Int32)
extern void UIRect_UpdateAnchorsInternal_m38944C5493670E57282865776914A972C2840E96 ();
// 0x00000488 System.Void UIRect::UpdateAnchors()
extern void UIRect_UpdateAnchors_m3830127DBCC659E78D9BA8E7C86B310ACEB2EB82 ();
// 0x00000489 System.Void UIRect::OnAnchor()
// 0x0000048A System.Void UIRect::SetAnchor(UnityEngine.Transform)
extern void UIRect_SetAnchor_m991813CC6FB904412E1E025ABD47F77AE822A58E ();
// 0x0000048B System.Void UIRect::SetAnchor(UnityEngine.GameObject)
extern void UIRect_SetAnchor_m5B66336950CE93D3B834CED30DDCFD429901D0AE ();
// 0x0000048C System.Void UIRect::SetAnchor(UnityEngine.GameObject,System.Int32,System.Int32,System.Int32,System.Int32)
extern void UIRect_SetAnchor_m3C2C4507D2EE7A5E8E844B9968CBA90D1862861F ();
// 0x0000048D System.Void UIRect::SetAnchor(UnityEngine.GameObject,System.Single,System.Single,System.Single,System.Single)
extern void UIRect_SetAnchor_m240D5B0AA7416BAF2983329A1D1DED99067D30F4 ();
// 0x0000048E System.Void UIRect::SetAnchor(UnityEngine.GameObject,System.Single,System.Int32,System.Single,System.Int32,System.Single,System.Int32,System.Single,System.Int32)
extern void UIRect_SetAnchor_m10AD9DD59F4A2AAA9ED4C1A39518E7EAB8CF9C43 ();
// 0x0000048F System.Void UIRect::SetAnchor(System.Single,System.Int32,System.Single,System.Int32,System.Single,System.Int32,System.Single,System.Int32)
extern void UIRect_SetAnchor_mF494D6010E6C4FF436BBBDFDB423557F68AD48CA ();
// 0x00000490 System.Void UIRect::SetScreenRect(System.Int32,System.Int32,System.Int32,System.Int32)
extern void UIRect_SetScreenRect_m23FB016991B65C0533AC7BFE69E34947E25F10FD ();
// 0x00000491 System.Void UIRect::ResetAnchors()
extern void UIRect_ResetAnchors_m1F700DEA18803E60FBDC0B04F63C3BBD3F3D171B ();
// 0x00000492 System.Void UIRect::ResetAndUpdateAnchors()
extern void UIRect_ResetAndUpdateAnchors_m84C47AEFBC9D8300E87B61CFB5D2EBBF5CAD8253 ();
// 0x00000493 System.Void UIRect::SetRect(System.Single,System.Single,System.Single,System.Single)
// 0x00000494 System.Void UIRect::FindCameraFor(UIRect_AnchorPoint)
extern void UIRect_FindCameraFor_mC174180C24476849A38A17E80DD2A78A9CB83182 ();
// 0x00000495 System.Void UIRect::ParentHasChanged()
extern void UIRect_ParentHasChanged_m763B6930F47DD0636A5BF71B669A573CFA44258A ();
// 0x00000496 System.Void UIRect::OnStart()
// 0x00000497 System.Void UIRect::OnUpdate()
extern void UIRect_OnUpdate_m1BD123C0A9340F341367552436003C2989BF3A06 ();
// 0x00000498 System.Void UIRect::.ctor()
extern void UIRect__ctor_m02F3ECDE7B0D3FA4C2B57C1FCC54DB394E7CA242 ();
// 0x00000499 System.Void UIRect::.cctor()
extern void UIRect__cctor_m02BA4169A62737040D35ADBDF92DBC52A412AEE0 ();
// 0x0000049A System.Void UISnapshotPoint::Start()
extern void UISnapshotPoint_Start_m311469C05ECA65FE7AB2D81655EC8544D8C23A0C ();
// 0x0000049B System.Void UISnapshotPoint::.ctor()
extern void UISnapshotPoint__ctor_m93EEB0F30696CFA341AD25C1C73F777C7365DDA2 ();
// 0x0000049C UIDrawCall_OnRenderCallback UIWidget::get_onRender()
extern void UIWidget_get_onRender_mBDA41A911F499DE240C2DD41C93FC1E3D75D9EA9 ();
// 0x0000049D System.Void UIWidget::set_onRender(UIDrawCall_OnRenderCallback)
extern void UIWidget_set_onRender_m852C95058BF0FEC623103E833C730589D0C750E5 ();
// 0x0000049E UnityEngine.Vector4 UIWidget::get_drawRegion()
extern void UIWidget_get_drawRegion_m9BB02BDEFE1D64D18F40D34A003345B7B474A1BD ();
// 0x0000049F System.Void UIWidget::set_drawRegion(UnityEngine.Vector4)
extern void UIWidget_set_drawRegion_m436D5C91934F94B4A97311EC9A6A94CE0BE4F5C6 ();
// 0x000004A0 UnityEngine.Vector2 UIWidget::get_pivotOffset()
extern void UIWidget_get_pivotOffset_m283BBEBB36F14E121D6ABDD43073FC6D5534DF8E ();
// 0x000004A1 System.Int32 UIWidget::get_width()
extern void UIWidget_get_width_m501FB5DA7B41280C91F52126ABB5EB74E85C3489 ();
// 0x000004A2 System.Void UIWidget::set_width(System.Int32)
extern void UIWidget_set_width_mE85FCDFE759D90433DAF81FC24847DB59E3F1398 ();
// 0x000004A3 System.Int32 UIWidget::get_height()
extern void UIWidget_get_height_mFBC3202F87974968FB377C58614CE88C94B1EBEE ();
// 0x000004A4 System.Void UIWidget::set_height(System.Int32)
extern void UIWidget_set_height_m74F387C207606753DC06FE86860FDF2FE4BCD810 ();
// 0x000004A5 UnityEngine.Color UIWidget::get_color()
extern void UIWidget_get_color_m95AC0009FA1B810799E9CD13059FB81C512E1185 ();
// 0x000004A6 System.Void UIWidget::set_color(UnityEngine.Color)
extern void UIWidget_set_color_mF4F8AF441D7B036BA06490D6CC2F30B3221D4077 ();
// 0x000004A7 System.Void UIWidget::SetColorNoAlpha(UnityEngine.Color)
extern void UIWidget_SetColorNoAlpha_m51412375A9A5A2F10BD7421081E539DB2CBA9F97 ();
// 0x000004A8 System.Single UIWidget::get_alpha()
extern void UIWidget_get_alpha_mD79751A78C033D10D4A1FAE1C8BC787C3776258E ();
// 0x000004A9 System.Void UIWidget::set_alpha(System.Single)
extern void UIWidget_set_alpha_m1D2F49C76228E691436E84F6CC135766F3746C60 ();
// 0x000004AA System.Boolean UIWidget::get_isVisible()
extern void UIWidget_get_isVisible_m764764ED516E85D271BCB0366F77B11AE63188BD ();
// 0x000004AB System.Boolean UIWidget::get_hasVertices()
extern void UIWidget_get_hasVertices_m160C255D37C5C5A659DF777E13A8DE1CD98FE88D ();
// 0x000004AC UIWidget_Pivot UIWidget::get_rawPivot()
extern void UIWidget_get_rawPivot_m6AA7D43224C4BBE0659206928389101544C97B76 ();
// 0x000004AD System.Void UIWidget::set_rawPivot(UIWidget_Pivot)
extern void UIWidget_set_rawPivot_m0D416A11B5210671B18175F9AB97A1A714C261A9 ();
// 0x000004AE UIWidget_Pivot UIWidget::get_pivot()
extern void UIWidget_get_pivot_m81FE06F39AFFB8521ECAA3F510848B809840C916 ();
// 0x000004AF System.Void UIWidget::set_pivot(UIWidget_Pivot)
extern void UIWidget_set_pivot_m23B38DCDF14101C72316255F873935DD1812DAA9 ();
// 0x000004B0 System.Int32 UIWidget::get_depth()
extern void UIWidget_get_depth_m1B6831E51E344D0C7E199D75F0F588FE1D438AFD ();
// 0x000004B1 System.Void UIWidget::set_depth(System.Int32)
extern void UIWidget_set_depth_mA805C0FCA1F9E599A47E8645C4628E409CE432BC ();
// 0x000004B2 System.Int32 UIWidget::get_raycastDepth()
extern void UIWidget_get_raycastDepth_m8B56D2845FF838A957BBF816FCE515D69AF8CC7C ();
// 0x000004B3 UnityEngine.Vector3[] UIWidget::get_localCorners()
extern void UIWidget_get_localCorners_mBDB209C1D75558B3F569BA4C69322B5F60A3CF6C ();
// 0x000004B4 UnityEngine.Vector2 UIWidget::get_localSize()
extern void UIWidget_get_localSize_m99A52F3D2520E1A0CC77FD56A67457A9C0D9E517 ();
// 0x000004B5 UnityEngine.Vector3 UIWidget::get_localCenter()
extern void UIWidget_get_localCenter_m0CEC08EE307E9B32FB6EC082E8605A98E3EDE9CA ();
// 0x000004B6 UnityEngine.Vector3[] UIWidget::get_worldCorners()
extern void UIWidget_get_worldCorners_m63B7184AA06A85263751DF402382B70A0976AB5D ();
// 0x000004B7 UnityEngine.Vector3 UIWidget::get_worldCenter()
extern void UIWidget_get_worldCenter_m657AD490E77213650E2C7A31017DD2DB38FC955C ();
// 0x000004B8 UnityEngine.Vector4 UIWidget::get_drawingDimensions()
extern void UIWidget_get_drawingDimensions_mD8B72C1D855684425D58542BC30ECC06CEB19FB3 ();
// 0x000004B9 UnityEngine.Material UIWidget::get_material()
extern void UIWidget_get_material_m607983EA24B3AD623F9A110C51F7337A624E16EA ();
// 0x000004BA System.Void UIWidget::set_material(UnityEngine.Material)
extern void UIWidget_set_material_m66AC8B7DA43F78BD1726D742C889C233D2209BE1 ();
// 0x000004BB UnityEngine.Texture UIWidget::get_mainTexture()
extern void UIWidget_get_mainTexture_mF6BDF21DE8C1EA1B98A2A5F3B1D22D7201020D71 ();
// 0x000004BC System.Void UIWidget::set_mainTexture(UnityEngine.Texture)
extern void UIWidget_set_mainTexture_m8CEA5C773D19E54E296922FD0C77C44C7598F05C ();
// 0x000004BD UnityEngine.Shader UIWidget::get_shader()
extern void UIWidget_get_shader_mE5DFB5182640DB01B77899B93C6DA1809564718B ();
// 0x000004BE System.Void UIWidget::set_shader(UnityEngine.Shader)
extern void UIWidget_set_shader_mA86118B1459911418C05000FC471017DE3582C32 ();
// 0x000004BF UnityEngine.Vector2 UIWidget::get_relativeSize()
extern void UIWidget_get_relativeSize_mD3680D627133FB1B33C8944CAE9C411D94839191 ();
// 0x000004C0 System.Boolean UIWidget::get_hasBoxCollider()
extern void UIWidget_get_hasBoxCollider_m7C303CAD2047600C2C2DF29FAE561B0954C4C920 ();
// 0x000004C1 System.Void UIWidget::SetDimensions(System.Int32,System.Int32)
extern void UIWidget_SetDimensions_m260BF1D7A9BD6B4D7491DD3EDD20083516D7FA8A ();
// 0x000004C2 UnityEngine.Vector3[] UIWidget::GetSides(UnityEngine.Transform)
extern void UIWidget_GetSides_mEDC6F8316A3D231C0284404717BCF05A048371D3 ();
// 0x000004C3 System.Single UIWidget::CalculateFinalAlpha(System.Int32)
extern void UIWidget_CalculateFinalAlpha_mA607C68C3F35DD5D0417A00D23A3C7815A68A853 ();
// 0x000004C4 System.Void UIWidget::UpdateFinalAlpha(System.Int32)
extern void UIWidget_UpdateFinalAlpha_mC1D3449F86ACA063636179BDCDEF61DF669B809B ();
// 0x000004C5 System.Void UIWidget::Invalidate(System.Boolean)
extern void UIWidget_Invalidate_m0CF4AEB0CC2711DEA16F9BCB0AEAFA1D783E8AB8 ();
// 0x000004C6 System.Single UIWidget::CalculateCumulativeAlpha(System.Int32)
extern void UIWidget_CalculateCumulativeAlpha_m38AB15C3779B1952455C4D93A6ABA5891739C322 ();
// 0x000004C7 System.Void UIWidget::SetRect(System.Single,System.Single,System.Single,System.Single)
extern void UIWidget_SetRect_m9C723841936E57B4FECE645A06787B488C8563C4 ();
// 0x000004C8 System.Void UIWidget::ResizeCollider()
extern void UIWidget_ResizeCollider_m06950674D2A67F789A2AEF65BDCB44AFAF00859D ();
// 0x000004C9 System.Int32 UIWidget::FullCompareFunc(UIWidget,UIWidget)
extern void UIWidget_FullCompareFunc_m8A26746E8D923DC005B0CEA3E5397E1920D50FAD ();
// 0x000004CA System.Int32 UIWidget::PanelCompareFunc(UIWidget,UIWidget)
extern void UIWidget_PanelCompareFunc_m5243EE27D249C9FF4E4AD40361F0FA38467256A7 ();
// 0x000004CB UnityEngine.Bounds UIWidget::CalculateBounds()
extern void UIWidget_CalculateBounds_m278A87A3BDC729D6CBDDEF81862B2B38C6046539 ();
// 0x000004CC UnityEngine.Bounds UIWidget::CalculateBounds(UnityEngine.Transform)
extern void UIWidget_CalculateBounds_m31352A2014D1D939D91A536B592858F8EAB101C3 ();
// 0x000004CD System.Void UIWidget::SetDirty()
extern void UIWidget_SetDirty_m614ED3BCF9237CED22AE8F2B1AEB2534038C5C8B ();
// 0x000004CE System.Void UIWidget::RemoveFromPanel()
extern void UIWidget_RemoveFromPanel_mD0AF4138918814C39C8F5D191FC6F767D6F17B11 ();
// 0x000004CF System.Void UIWidget::MarkAsChanged()
extern void UIWidget_MarkAsChanged_m6A78B19F42D8F95784ABD03DB72229299F436B1D ();
// 0x000004D0 UIPanel UIWidget::CreatePanel()
extern void UIWidget_CreatePanel_m81420CE0B4E84CB13C0FB52D73B5025472AC8BB9 ();
// 0x000004D1 System.Void UIWidget::CheckLayer()
extern void UIWidget_CheckLayer_mA2985B207BAA98BC6EB38BB69241A9B6CAA28808 ();
// 0x000004D2 System.Void UIWidget::ParentHasChanged()
extern void UIWidget_ParentHasChanged_m4B96CF7868A9907DBA59FE21F906190B44578DBF ();
// 0x000004D3 System.Void UIWidget::Awake()
extern void UIWidget_Awake_mC5FA326FE922950136C565AFEA1D342C0737DCD2 ();
// 0x000004D4 System.Void UIWidget::OnInit()
extern void UIWidget_OnInit_mD879CDBC6DE015A4F7EAA024F17EE442ACD5E2A9 ();
// 0x000004D5 System.Void UIWidget::UpgradeFrom265()
extern void UIWidget_UpgradeFrom265_m7768FBCFBEAAB25266555DCB45AE0ACABD30B27E ();
// 0x000004D6 System.Void UIWidget::OnStart()
extern void UIWidget_OnStart_m90F5F491D3276B1B6E0743294732B19DFEB2197E ();
// 0x000004D7 System.Void UIWidget::OnAnchor()
extern void UIWidget_OnAnchor_mB8FE129D9EF30107EE9F0C048A491C50DCBB889A ();
// 0x000004D8 System.Void UIWidget::OnUpdate()
extern void UIWidget_OnUpdate_m03E4F19607AB2951F0EF8E51AE44DDF0693DDC0B ();
// 0x000004D9 System.Void UIWidget::OnApplicationPause(System.Boolean)
extern void UIWidget_OnApplicationPause_m811E558A524339D324106CAF3BBF7CB2410DFFA8 ();
// 0x000004DA System.Void UIWidget::OnDisable()
extern void UIWidget_OnDisable_m126EB62FBB31E51C61E7939457D4A6A70D568EA4 ();
// 0x000004DB System.Void UIWidget::OnDestroy()
extern void UIWidget_OnDestroy_m345F7E030B89EF3620773EE2E52E4A3D6649892A ();
// 0x000004DC System.Boolean UIWidget::UpdateVisibility(System.Boolean,System.Boolean)
extern void UIWidget_UpdateVisibility_m3E95D95A568E0A843964AB4F51123152FD556E62 ();
// 0x000004DD System.Boolean UIWidget::UpdateTransform(System.Int32)
extern void UIWidget_UpdateTransform_m8567B2240B9820C5B368A4F9D779C0C50822A8D7 ();
// 0x000004DE System.Boolean UIWidget::UpdateGeometry(System.Int32)
extern void UIWidget_UpdateGeometry_m4ADC52D29C6448BB6E5517AFB9F2001E6520625C ();
// 0x000004DF System.Void UIWidget::WriteToBuffers(System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Color>,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector4>,System.Collections.Generic.List`1<UnityEngine.Vector4>)
extern void UIWidget_WriteToBuffers_m6C94C9A12E5F880E8B5CBDB8765554BF4A16B36E ();
// 0x000004E0 System.Void UIWidget::MakePixelPerfect()
extern void UIWidget_MakePixelPerfect_mB46E1643B60029E008E2F1BA090335552D5894CE ();
// 0x000004E1 System.Int32 UIWidget::get_minWidth()
extern void UIWidget_get_minWidth_m049D3D3CB7AE4A99563C2A378542F95651B20931 ();
// 0x000004E2 System.Int32 UIWidget::get_minHeight()
extern void UIWidget_get_minHeight_m9D24A8447F54190325A75A8A45DAE237F3E232C0 ();
// 0x000004E3 UnityEngine.Vector4 UIWidget::get_border()
extern void UIWidget_get_border_mDE84BECB08267330656B68F6B4282C525B160D87 ();
// 0x000004E4 System.Void UIWidget::set_border(UnityEngine.Vector4)
extern void UIWidget_set_border_m194A7EFB88428D44F5F274C578AF2222A0437C53 ();
// 0x000004E5 System.Void UIWidget::OnFill(System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Color>)
extern void UIWidget_OnFill_mF01AB6B21201740BE6D8088CF7160FAEA93C9D18 ();
// 0x000004E6 System.Void UIWidget::.ctor()
extern void UIWidget__ctor_m15381CA62FD8A713D2EB69D28E00736380E754DD ();
// 0x000004E7 System.Void AnimatedAlpha::OnEnable()
extern void AnimatedAlpha_OnEnable_m6C0815A157256CED25AC8313849F7BC7E7708524 ();
// 0x000004E8 System.Void AnimatedAlpha::LateUpdate()
extern void AnimatedAlpha_LateUpdate_mFECA92DEE54250A6F9C1FE0FED8C386820D56AB9 ();
// 0x000004E9 System.Void AnimatedAlpha::.ctor()
extern void AnimatedAlpha__ctor_m3829C89E80A80D83C080F1AC70897C3A20922921 ();
// 0x000004EA System.Void AnimatedColor::OnEnable()
extern void AnimatedColor_OnEnable_mD9AD7365458EA540CFF3C689D5863BD14A5E0378 ();
// 0x000004EB System.Void AnimatedColor::LateUpdate()
extern void AnimatedColor_LateUpdate_m6EEADB4F286634FD81851493E19A374470F655D0 ();
// 0x000004EC System.Void AnimatedColor::.ctor()
extern void AnimatedColor__ctor_m4E73A4B9D7A696A78704AB5791446599E6718B03 ();
// 0x000004ED System.Void AnimatedWidget::OnEnable()
extern void AnimatedWidget_OnEnable_m1986C9E721179C012C944759A9697BD2CD5D5496 ();
// 0x000004EE System.Void AnimatedWidget::LateUpdate()
extern void AnimatedWidget_LateUpdate_m747109EA5292C8F2F906E543A1B706AD12008DDB ();
// 0x000004EF System.Void AnimatedWidget::.ctor()
extern void AnimatedWidget__ctor_m763F9820C197B22A513D2C3EACABF8CEC58F2758 ();
// 0x000004F0 System.Void SpringPosition::Start()
extern void SpringPosition_Start_m9FDD77240C52F824A049EF39C95EA544DCF9D530 ();
// 0x000004F1 System.Void SpringPosition::Update()
extern void SpringPosition_Update_m67158C1AC0CE51326612A9090F812865A6BB7E51 ();
// 0x000004F2 System.Void SpringPosition::NotifyListeners()
extern void SpringPosition_NotifyListeners_m5F6B54D296417CE6037D41B103D215731DA59320 ();
// 0x000004F3 SpringPosition SpringPosition::Begin(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern void SpringPosition_Begin_mF79F82F871D4AAA0859457A0CE2619EDF043B23E ();
// 0x000004F4 System.Void SpringPosition::.ctor()
extern void SpringPosition__ctor_mF923DF4E721AE5866561DD017F3418CC7CAE0C2B ();
// 0x000004F5 System.Single TweenAlpha::get_alpha()
extern void TweenAlpha_get_alpha_m241A3DE3B689A5495C7575E4A162431D6F0E3F49 ();
// 0x000004F6 System.Void TweenAlpha::set_alpha(System.Single)
extern void TweenAlpha_set_alpha_mB243AA3AA7C729556964FBCED9CD80FC4679336C ();
// 0x000004F7 System.Void TweenAlpha::Cache()
extern void TweenAlpha_Cache_m280CDA7B07703BA0A908BD38E925823EDD75EC91 ();
// 0x000004F8 System.Single TweenAlpha::get_value()
extern void TweenAlpha_get_value_m2BBD968CEFEC382D45D693B178C8AC447074E717 ();
// 0x000004F9 System.Void TweenAlpha::set_value(System.Single)
extern void TweenAlpha_set_value_mD9A5EB274087A83C15FE8E74D6960F72D5ACC805 ();
// 0x000004FA System.Void TweenAlpha::OnUpdate(System.Single,System.Boolean)
extern void TweenAlpha_OnUpdate_m1B06AD33092E3AF2C592F60843683474D37699D2 ();
// 0x000004FB TweenAlpha TweenAlpha::Begin(UnityEngine.GameObject,System.Single,System.Single,System.Single)
extern void TweenAlpha_Begin_mE1FF3A63EFD6AEE469BB14FDBE9E351067ED6C01 ();
// 0x000004FC System.Void TweenAlpha::SetStartToCurrentValue()
extern void TweenAlpha_SetStartToCurrentValue_m2B2791243929FA8923AB80B71C8295D1FD365E4F ();
// 0x000004FD System.Void TweenAlpha::SetEndToCurrentValue()
extern void TweenAlpha_SetEndToCurrentValue_m49A9948E21A6C1B4E8F5A951F1E3978D138C8154 ();
// 0x000004FE System.Void TweenAlpha::.ctor()
extern void TweenAlpha__ctor_m22F3A2D9EBB8A4DFC9F4EEA615676AD835D20DB3 ();
// 0x000004FF System.Void TweenColor::Cache()
extern void TweenColor_Cache_m5B2C0D509EBE5A61E94A80E94A57E5A22EC940A2 ();
// 0x00000500 UnityEngine.Color TweenColor::get_color()
extern void TweenColor_get_color_mC4882992104276229F31C2D1414B9B50B33E0D82 ();
// 0x00000501 System.Void TweenColor::set_color(UnityEngine.Color)
extern void TweenColor_set_color_m502A9790AD360BEE3BC20A42A46A7E8D24C77DE7 ();
// 0x00000502 UnityEngine.Color TweenColor::get_value()
extern void TweenColor_get_value_m6469D6798A7E9A9E614FBB8B2E92095061B9C6BC ();
// 0x00000503 System.Void TweenColor::set_value(UnityEngine.Color)
extern void TweenColor_set_value_m6380219EF737E3A407FD3D5588A4FC35DB6B96F5 ();
// 0x00000504 System.Void TweenColor::OnUpdate(System.Single,System.Boolean)
extern void TweenColor_OnUpdate_mCCB17D6395229F4F74F22E07003F450B5BC18EB8 ();
// 0x00000505 TweenColor TweenColor::Begin(UnityEngine.GameObject,System.Single,UnityEngine.Color)
extern void TweenColor_Begin_m65D6BCFD276DC396075D9518627E8CEAC679D899 ();
// 0x00000506 System.Void TweenColor::SetStartToCurrentValue()
extern void TweenColor_SetStartToCurrentValue_mAB515CB1BAD0BF3C01C74E8AFF7BA62F7DC5A162 ();
// 0x00000507 System.Void TweenColor::SetEndToCurrentValue()
extern void TweenColor_SetEndToCurrentValue_m4B038B025D85A77E1F15B3E8270E9BE8AEC5B3DA ();
// 0x00000508 System.Void TweenColor::SetCurrentValueToStart()
extern void TweenColor_SetCurrentValueToStart_mA752953F3F6B5458E080EE5F3EAF3F9B75FE9EF9 ();
// 0x00000509 System.Void TweenColor::SetCurrentValueToEnd()
extern void TweenColor_SetCurrentValueToEnd_m734F7B3B9CEDB9628A2F232396722304FE239C72 ();
// 0x0000050A System.Void TweenColor::.ctor()
extern void TweenColor__ctor_m176916136FD9F154B9F38A6E00A141A2DC38FD5C ();
// 0x0000050B UnityEngine.Camera TweenFOV::get_cachedCamera()
extern void TweenFOV_get_cachedCamera_m3C6E5F5297C5478D6D9549D2D41DCB16016C6A47 ();
// 0x0000050C System.Single TweenFOV::get_fov()
extern void TweenFOV_get_fov_m985E0BC21ACC9DE2E516D36C1515D84670D3D6C3 ();
// 0x0000050D System.Void TweenFOV::set_fov(System.Single)
extern void TweenFOV_set_fov_m517FF589EA6818D7E7BDCD9E0A2AF7F71FB5A9FC ();
// 0x0000050E System.Single TweenFOV::get_value()
extern void TweenFOV_get_value_m909E3DAFBB8370D106059B74A800F52EEB6F35DA ();
// 0x0000050F System.Void TweenFOV::set_value(System.Single)
extern void TweenFOV_set_value_mB40D3869A0033C22F8F061D6123A815414A70F6A ();
// 0x00000510 System.Void TweenFOV::OnUpdate(System.Single,System.Boolean)
extern void TweenFOV_OnUpdate_m7B02517C94F74A8CA26A3CB245E66DBE65D96AB7 ();
// 0x00000511 TweenFOV TweenFOV::Begin(UnityEngine.GameObject,System.Single,System.Single)
extern void TweenFOV_Begin_m0B33120E302DA8F2FFA12CD3D0CCCC604E816E11 ();
// 0x00000512 System.Void TweenFOV::SetStartToCurrentValue()
extern void TweenFOV_SetStartToCurrentValue_m6A50F02A2C8EB082F5DD3634236794B11AB8EE36 ();
// 0x00000513 System.Void TweenFOV::SetEndToCurrentValue()
extern void TweenFOV_SetEndToCurrentValue_m254CFA3A2989551F99B28A7BFBC36D5C85C9D47C ();
// 0x00000514 System.Void TweenFOV::SetCurrentValueToStart()
extern void TweenFOV_SetCurrentValueToStart_mE1EF1423889BB213BAC664A42232D439E97387BE ();
// 0x00000515 System.Void TweenFOV::SetCurrentValueToEnd()
extern void TweenFOV_SetCurrentValueToEnd_mBD66AC40DC4D88061562402A3085AA7174EF5013 ();
// 0x00000516 System.Void TweenFOV::.ctor()
extern void TweenFOV__ctor_m9174D5780F0069158ED940CE5E7D26615A654811 ();
// 0x00000517 UIWidget TweenHeight::get_cachedWidget()
extern void TweenHeight_get_cachedWidget_m9FF65A8C9656E8A1BEB4AF48AFDEC9C1AC07C516 ();
// 0x00000518 System.Int32 TweenHeight::get_height()
extern void TweenHeight_get_height_m9143FD7EB40879C6E220D691EA9EBBB5E0D621EF ();
// 0x00000519 System.Void TweenHeight::set_height(System.Int32)
extern void TweenHeight_set_height_m76A2903A1579102011242B0288B364AC53B6EDF8 ();
// 0x0000051A System.Int32 TweenHeight::get_value()
extern void TweenHeight_get_value_m3DACA554DA23F36B09747BCA1571D94969F04A30 ();
// 0x0000051B System.Void TweenHeight::set_value(System.Int32)
extern void TweenHeight_set_value_mECBCDCC2862182A965CD149C296DDA61D2B88D0D ();
// 0x0000051C System.Void TweenHeight::OnUpdate(System.Single,System.Boolean)
extern void TweenHeight_OnUpdate_m323DB8FAF329D780B06F10CF4DF03641AF7F00F8 ();
// 0x0000051D TweenHeight TweenHeight::Begin(UIWidget,System.Single,System.Int32)
extern void TweenHeight_Begin_m66080CB04417EB0C583E90F01390ABC148AC1D8C ();
// 0x0000051E System.Void TweenHeight::SetStartToCurrentValue()
extern void TweenHeight_SetStartToCurrentValue_m0AB942FBFF965357B04FB6595B3D4C57D95988FB ();
// 0x0000051F System.Void TweenHeight::SetEndToCurrentValue()
extern void TweenHeight_SetEndToCurrentValue_m71159A7422124D2E4E3128AAE2073C850F4D4EEC ();
// 0x00000520 System.Void TweenHeight::SetCurrentValueToStart()
extern void TweenHeight_SetCurrentValueToStart_mC2600BBAA9979B2DADCC4065D39FABEB6B0895B7 ();
// 0x00000521 System.Void TweenHeight::SetCurrentValueToEnd()
extern void TweenHeight_SetCurrentValueToEnd_mAB75F2579AC546DBAF8D2C20684463462273AFA4 ();
// 0x00000522 System.Void TweenHeight::.ctor()
extern void TweenHeight__ctor_m3C10BC44777615275ECBCB1099B7C9531C5B39DE ();
// 0x00000523 UnityEngine.Camera TweenOrthoSize::get_cachedCamera()
extern void TweenOrthoSize_get_cachedCamera_m9E6C25FDE7223FC92576584B871FFF4903FFF4EA ();
// 0x00000524 System.Single TweenOrthoSize::get_orthoSize()
extern void TweenOrthoSize_get_orthoSize_mB470EB9A10757D1AF9EF03A4D3601489349E0DBB ();
// 0x00000525 System.Void TweenOrthoSize::set_orthoSize(System.Single)
extern void TweenOrthoSize_set_orthoSize_m2AF287C5605DE356B81667EE34070B3CAD4DE8BA ();
// 0x00000526 System.Single TweenOrthoSize::get_value()
extern void TweenOrthoSize_get_value_mECDCAC759AFC9E67C9C11EDF1D6833A15CD3CCA2 ();
// 0x00000527 System.Void TweenOrthoSize::set_value(System.Single)
extern void TweenOrthoSize_set_value_m242135017A5F3E2106A473B6B5E3CBA28CD16F69 ();
// 0x00000528 System.Void TweenOrthoSize::OnUpdate(System.Single,System.Boolean)
extern void TweenOrthoSize_OnUpdate_m0D3C5A18BD427D81341F482DED181E7679E8C99B ();
// 0x00000529 TweenOrthoSize TweenOrthoSize::Begin(UnityEngine.GameObject,System.Single,System.Single)
extern void TweenOrthoSize_Begin_m42526FCD991983BB2C57F693FA9EF521144713BF ();
// 0x0000052A System.Void TweenOrthoSize::SetStartToCurrentValue()
extern void TweenOrthoSize_SetStartToCurrentValue_m6FAC81624BEE1115F12A74A31262A0560A8DA3A7 ();
// 0x0000052B System.Void TweenOrthoSize::SetEndToCurrentValue()
extern void TweenOrthoSize_SetEndToCurrentValue_m93BDD784EFBD63892128E898D7151D023465B0F8 ();
// 0x0000052C System.Void TweenOrthoSize::.ctor()
extern void TweenOrthoSize__ctor_m5E0059FD7D7D04CA7D83FC2CFCD68CC17EC8B005 ();
// 0x0000052D UnityEngine.Transform TweenPosition::get_cachedTransform()
extern void TweenPosition_get_cachedTransform_mB2FCF9D2A16216C43C7F09EE60A42B832F7E0CBC ();
// 0x0000052E UnityEngine.Vector3 TweenPosition::get_position()
extern void TweenPosition_get_position_m00DC112C2CE10ED7693369CC2F828F7C81480634 ();
// 0x0000052F System.Void TweenPosition::set_position(UnityEngine.Vector3)
extern void TweenPosition_set_position_mBC49D42592B74F08FE88B042801A0871C3954D80 ();
// 0x00000530 UnityEngine.Vector3 TweenPosition::get_value()
extern void TweenPosition_get_value_m296E543A78B604623145F9228E2B0E35EA0B7F7D ();
// 0x00000531 System.Void TweenPosition::set_value(UnityEngine.Vector3)
extern void TweenPosition_set_value_m604E6D107E23EB538924C0D3DC071E75D3893F04 ();
// 0x00000532 System.Void TweenPosition::Awake()
extern void TweenPosition_Awake_m26EC5559F86B3B98ECB9474E0519FE6C81732D45 ();
// 0x00000533 System.Void TweenPosition::OnUpdate(System.Single,System.Boolean)
extern void TweenPosition_OnUpdate_m8A16A139AB92E4BFC693D6000329DAECE0FE8765 ();
// 0x00000534 TweenPosition TweenPosition::Begin(UnityEngine.GameObject,System.Single,UnityEngine.Vector3)
extern void TweenPosition_Begin_m6135BC77DE682E8876EC3877E395D2CF2A75FC7D ();
// 0x00000535 TweenPosition TweenPosition::Begin(UnityEngine.GameObject,System.Single,UnityEngine.Vector3,System.Boolean)
extern void TweenPosition_Begin_m591145BC123A1F1A2665D50BD86F7034D467E619 ();
// 0x00000536 System.Void TweenPosition::SetStartToCurrentValue()
extern void TweenPosition_SetStartToCurrentValue_m1C352FB520CD418931B79962DB8685A875C96E60 ();
// 0x00000537 System.Void TweenPosition::SetEndToCurrentValue()
extern void TweenPosition_SetEndToCurrentValue_m614F820F0F9B4B8E2A39801993851D5CEB78D4B6 ();
// 0x00000538 System.Void TweenPosition::SetCurrentValueToStart()
extern void TweenPosition_SetCurrentValueToStart_mD14B1B78476ABFC0AD1B46292DDEFF631822636A ();
// 0x00000539 System.Void TweenPosition::SetCurrentValueToEnd()
extern void TweenPosition_SetCurrentValueToEnd_mDE034DEEA71D656A066DEC62DFF940BC0CA8C184 ();
// 0x0000053A System.Void TweenPosition::.ctor()
extern void TweenPosition__ctor_mC4196F443C4C32EECEC4178DE3137F6E9312796D ();
// 0x0000053B UnityEngine.Transform TweenRotation::get_cachedTransform()
extern void TweenRotation_get_cachedTransform_m87532B2684B73386C3D4DF98CE65317E0AD4B364 ();
// 0x0000053C UnityEngine.Quaternion TweenRotation::get_rotation()
extern void TweenRotation_get_rotation_mC4729B6D220AC1C541D162C700C1FF7BF4954524 ();
// 0x0000053D System.Void TweenRotation::set_rotation(UnityEngine.Quaternion)
extern void TweenRotation_set_rotation_mE8DB816A26B36EEA996D0D5605A2AFC563BBF235 ();
// 0x0000053E UnityEngine.Quaternion TweenRotation::get_value()
extern void TweenRotation_get_value_m665460E9B94843F0DB261C0BA2438752304FBFF6 ();
// 0x0000053F System.Void TweenRotation::set_value(UnityEngine.Quaternion)
extern void TweenRotation_set_value_mB752EA5C054FE0FA83F0252BA972DBC58E7791EA ();
// 0x00000540 System.Void TweenRotation::OnUpdate(System.Single,System.Boolean)
extern void TweenRotation_OnUpdate_m527FE3D82DA24C23868C604B3E1ED30688BEF552 ();
// 0x00000541 TweenRotation TweenRotation::Begin(UnityEngine.GameObject,System.Single,UnityEngine.Quaternion)
extern void TweenRotation_Begin_mAE089BEC2488CF55B668AEC690D4B542CEBC0939 ();
// 0x00000542 System.Void TweenRotation::SetStartToCurrentValue()
extern void TweenRotation_SetStartToCurrentValue_m5C3EFA9589AF8077E539952BB230273C2E6D5785 ();
// 0x00000543 System.Void TweenRotation::SetEndToCurrentValue()
extern void TweenRotation_SetEndToCurrentValue_m1A7322B155DC25A3D81F13F0C5121F409EC2CDC3 ();
// 0x00000544 System.Void TweenRotation::SetCurrentValueToStart()
extern void TweenRotation_SetCurrentValueToStart_m328908EFF06B3F61731F867D7537375F39498494 ();
// 0x00000545 System.Void TweenRotation::SetCurrentValueToEnd()
extern void TweenRotation_SetCurrentValueToEnd_mEE5106A73CF13E2169074BAA9257850173D683B1 ();
// 0x00000546 System.Void TweenRotation::.ctor()
extern void TweenRotation__ctor_m895A60EBF361716F220AD20AF389709BDD8D7DFD ();
// 0x00000547 UnityEngine.Transform TweenScale::get_cachedTransform()
extern void TweenScale_get_cachedTransform_mD061040C5BC8A0AB87C973A2EC095EDEDE57010F ();
// 0x00000548 UnityEngine.Vector3 TweenScale::get_value()
extern void TweenScale_get_value_m5A2EFCD0945BEB1FD231CD08455B64A789C812A6 ();
// 0x00000549 System.Void TweenScale::set_value(UnityEngine.Vector3)
extern void TweenScale_set_value_m1E2E524B665E2A0624611E789A8FBC9CE3305DCD ();
// 0x0000054A UnityEngine.Vector3 TweenScale::get_scale()
extern void TweenScale_get_scale_mBB781570E66E516598C0A54F3BC61161E6BF1097 ();
// 0x0000054B System.Void TweenScale::set_scale(UnityEngine.Vector3)
extern void TweenScale_set_scale_m37F3F7D20F94FF56341557000728D3AAF7E7345F ();
// 0x0000054C System.Void TweenScale::OnUpdate(System.Single,System.Boolean)
extern void TweenScale_OnUpdate_mCD87CF488CC8A7A7CB782A47475CB8D5D26F0BC5 ();
// 0x0000054D TweenScale TweenScale::Begin(UnityEngine.GameObject,System.Single,UnityEngine.Vector3)
extern void TweenScale_Begin_m019929488D58C79F89C5B0D3382320892CC0437B ();
// 0x0000054E System.Void TweenScale::SetStartToCurrentValue()
extern void TweenScale_SetStartToCurrentValue_m9BB35FD691576B05165CBCAA0DE57601C92653F1 ();
// 0x0000054F System.Void TweenScale::SetEndToCurrentValue()
extern void TweenScale_SetEndToCurrentValue_mAE858AE6B3EBB2C2BFA265F1DB516C21C321D5A8 ();
// 0x00000550 System.Void TweenScale::SetCurrentValueToStart()
extern void TweenScale_SetCurrentValueToStart_mA51B39E40B5529A8FFF35E3F29082B66357C7400 ();
// 0x00000551 System.Void TweenScale::SetCurrentValueToEnd()
extern void TweenScale_SetCurrentValueToEnd_m0C38F938A6C8660215E8D098DCA20650BE087CD9 ();
// 0x00000552 System.Void TweenScale::.ctor()
extern void TweenScale__ctor_m7219C6C5E1868123E67F63D8598F0A57139212C0 ();
// 0x00000553 System.Void TweenTransform::OnUpdate(System.Single,System.Boolean)
extern void TweenTransform_OnUpdate_m52473C365740E2FA94F845E41F934F0DF19B569E ();
// 0x00000554 TweenTransform TweenTransform::Begin(UnityEngine.GameObject,System.Single,UnityEngine.Transform)
extern void TweenTransform_Begin_mDBDDA6E537B99AA6607DC4230AA4E570A455986F ();
// 0x00000555 TweenTransform TweenTransform::Begin(UnityEngine.GameObject,System.Single,UnityEngine.Transform,UnityEngine.Transform)
extern void TweenTransform_Begin_mDAE53BFBE8B832C60F778EE2394AA746350947F8 ();
// 0x00000556 System.Void TweenTransform::.ctor()
extern void TweenTransform__ctor_m52C49BE4FC52B1832C7BECAFE31411E83A22E647 ();
// 0x00000557 UnityEngine.AudioSource TweenVolume::get_audioSource()
extern void TweenVolume_get_audioSource_m1BA5B5577257343FCE4A8A52D4196A66EF6F5DD2 ();
// 0x00000558 System.Single TweenVolume::get_volume()
extern void TweenVolume_get_volume_mD615AEC4892EAE0046F615D645C81E9C5980DC17 ();
// 0x00000559 System.Void TweenVolume::set_volume(System.Single)
extern void TweenVolume_set_volume_m5C4C08541ED5EBC481582A6D42124378EB77C477 ();
// 0x0000055A System.Single TweenVolume::get_value()
extern void TweenVolume_get_value_m24832F7ABB8E323E73C9E8BC244E51D1CD526F48 ();
// 0x0000055B System.Void TweenVolume::set_value(System.Single)
extern void TweenVolume_set_value_mFB9AEB0318843005F486FC3FE9E8140C14B82434 ();
// 0x0000055C System.Void TweenVolume::OnUpdate(System.Single,System.Boolean)
extern void TweenVolume_OnUpdate_m1588B4A1E4B102A989C7ECA9F794194ECDBAB3CB ();
// 0x0000055D TweenVolume TweenVolume::Begin(UnityEngine.GameObject,System.Single,System.Single)
extern void TweenVolume_Begin_m267765236E46AFDC84367988BCD109E97AA883F0 ();
// 0x0000055E System.Void TweenVolume::SetStartToCurrentValue()
extern void TweenVolume_SetStartToCurrentValue_mD8DC5FDDB9D46DB226D74F80ECC452C63542EE14 ();
// 0x0000055F System.Void TweenVolume::SetEndToCurrentValue()
extern void TweenVolume_SetEndToCurrentValue_m4A4E08C23651D2B2C61C0DB7A5C2C7E0B20A5D67 ();
// 0x00000560 System.Void TweenVolume::.ctor()
extern void TweenVolume__ctor_m26D151D7586233B1A7EFF58B0D1635055B94FD49 ();
// 0x00000561 UIWidget TweenWidth::get_cachedWidget()
extern void TweenWidth_get_cachedWidget_m72509E0558F2E27BB10504878FBD9CC810B0A791 ();
// 0x00000562 System.Int32 TweenWidth::get_width()
extern void TweenWidth_get_width_m67FECA3AC27B21668C5EEBB506AF20F2D07A738F ();
// 0x00000563 System.Void TweenWidth::set_width(System.Int32)
extern void TweenWidth_set_width_mA47685DF8F52E1BBD8823C7347FE6CBDC62E523A ();
// 0x00000564 System.Int32 TweenWidth::get_value()
extern void TweenWidth_get_value_mC551FDE56EE1702FBE2638BD0ADE6E5B5AE1BFAA ();
// 0x00000565 System.Void TweenWidth::set_value(System.Int32)
extern void TweenWidth_set_value_mFC8876905F6E2B0834A765B208638A6611233EB0 ();
// 0x00000566 System.Void TweenWidth::OnUpdate(System.Single,System.Boolean)
extern void TweenWidth_OnUpdate_m58C9902103707FB825567C69CF181A8FDDBA700A ();
// 0x00000567 TweenWidth TweenWidth::Begin(UIWidget,System.Single,System.Int32)
extern void TweenWidth_Begin_m905BAC85272A971D9EF2BC39B72FAF7E5AB1594E ();
// 0x00000568 System.Void TweenWidth::SetStartToCurrentValue()
extern void TweenWidth_SetStartToCurrentValue_mC6F3EA47282F91292A7DD6C2E2E3440FB1FBED71 ();
// 0x00000569 System.Void TweenWidth::SetEndToCurrentValue()
extern void TweenWidth_SetEndToCurrentValue_m8557A0AD508E8BB0F2688241CB1F80E5321EA5CE ();
// 0x0000056A System.Void TweenWidth::SetCurrentValueToStart()
extern void TweenWidth_SetCurrentValueToStart_m44F1FA22E1258529E9DF7EFE69349C6FA09B4A53 ();
// 0x0000056B System.Void TweenWidth::SetCurrentValueToEnd()
extern void TweenWidth_SetCurrentValueToEnd_m90549399D2EF73A921E6AC47F9233ADD1EE44A52 ();
// 0x0000056C System.Void TweenWidth::.ctor()
extern void TweenWidth__ctor_mBDC99A01803DCD2B693FAF3F0CAC66A89C40E5CD ();
// 0x0000056D System.Single UITweener::get_amountPerDelta()
extern void UITweener_get_amountPerDelta_m77B2354E2E70C5FA72168D57B30E6A459DE48060 ();
// 0x0000056E System.Single UITweener::get_tweenFactor()
extern void UITweener_get_tweenFactor_m3346C3BB3EE33A6174DA477E6A6E6789E2672895 ();
// 0x0000056F System.Void UITweener::set_tweenFactor(System.Single)
extern void UITweener_set_tweenFactor_m516564702FF65697C5C56C2BD94E830D37EE349B ();
// 0x00000570 AnimationOrTween.Direction UITweener::get_direction()
extern void UITweener_get_direction_mD7D9A2722D8D06E3F0E0A7DD84A32C0872347489 ();
// 0x00000571 System.Void UITweener::Reset()
extern void UITweener_Reset_m2EDAE6A6D123A65701D2532978923E1DFC419E7C ();
// 0x00000572 System.Void UITweener::Start()
extern void UITweener_Start_mE61DC78452BBD1908CC10983BD9CDCFDD5DC41D6 ();
// 0x00000573 System.Void UITweener::Update()
extern void UITweener_Update_mD62A939BB0B2EF7532C2AAF373A9EE9CB2DEE413 ();
// 0x00000574 System.Void UITweener::FixedUpdate()
extern void UITweener_FixedUpdate_m4949D2C6B3F02CBB3998CE393F5B60AB8EBFAE55 ();
// 0x00000575 System.Void UITweener::DoUpdate()
extern void UITweener_DoUpdate_mCBC23E1C682F2433AA66814578DAED98A5742D83 ();
// 0x00000576 System.Void UITweener::SetOnFinished(EventDelegate_Callback)
extern void UITweener_SetOnFinished_m89D7AF68B4B1BF36551863203D89EED6B43A32E3 ();
// 0x00000577 System.Void UITweener::SetOnFinished(EventDelegate)
extern void UITweener_SetOnFinished_m2BB03E9F562235D38B1F4B37B0A2CA43BAB72371 ();
// 0x00000578 System.Void UITweener::AddOnFinished(EventDelegate_Callback)
extern void UITweener_AddOnFinished_m77E1D38EB82A475F720ECFD1FB04EF8A10DD1CD0 ();
// 0x00000579 System.Void UITweener::AddOnFinished(EventDelegate)
extern void UITweener_AddOnFinished_m73730C3868832054C989BC69353B243AEA8BC4D7 ();
// 0x0000057A System.Void UITweener::RemoveOnFinished(EventDelegate)
extern void UITweener_RemoveOnFinished_m8E7FF445CE4B9B6FA7204E45F1B95D6C67CDE821 ();
// 0x0000057B System.Void UITweener::OnDisable()
extern void UITweener_OnDisable_m8E3FEAC9A8D2D597400583256B4FAECA89EC4510 ();
// 0x0000057C System.Void UITweener::Finish()
extern void UITweener_Finish_m4F0B71FB3872B2E27DA2D58C392A9D2BA1221EF3 ();
// 0x0000057D System.Void UITweener::Sample(System.Single,System.Boolean)
extern void UITweener_Sample_m933EC29AC5E22697706F7D6925570A748844C90C ();
// 0x0000057E System.Single UITweener::BounceLogic(System.Single)
extern void UITweener_BounceLogic_m8B6A27F4B57234ECE1A06C67605544A25F818A4C ();
// 0x0000057F System.Void UITweener::Play()
extern void UITweener_Play_m2502B40C91058DA380DDEB7178784423E31C14D3 ();
// 0x00000580 System.Void UITweener::PlayForward()
extern void UITweener_PlayForward_m1A6444E46C0C2DE3EC94A873536267D8FEDC3C5A ();
// 0x00000581 System.Void UITweener::PlayReverse()
extern void UITweener_PlayReverse_mCA94F29B817DE004B53865058FEFCC1A2CC492B2 ();
// 0x00000582 System.Void UITweener::Play(System.Boolean)
extern void UITweener_Play_m247329644D7824009B46D1C455FA25FDE0AD7FD5 ();
// 0x00000583 System.Void UITweener::ResetToBeginning()
extern void UITweener_ResetToBeginning_m6A91A7B8A286385D7A4F37D26395626752AA23D8 ();
// 0x00000584 System.Void UITweener::Toggle()
extern void UITweener_Toggle_m55CDC4B5E24C2A0F700BE3809B71548263DC082A ();
// 0x00000585 System.Void UITweener::OnUpdate(System.Single,System.Boolean)
// 0x00000586 T UITweener::Begin(UnityEngine.GameObject,System.Single,System.Single)
// 0x00000587 System.Void UITweener::SetStartToCurrentValue()
extern void UITweener_SetStartToCurrentValue_m52C28B8160DFA7CDB155D2E15157E7CED713D15F ();
// 0x00000588 System.Void UITweener::SetEndToCurrentValue()
extern void UITweener_SetEndToCurrentValue_m75C4AC06978337DCB1D52F1D45EF7A7C48ECC2AB ();
// 0x00000589 System.Void UITweener::.ctor()
extern void UITweener__ctor_m58E649975D51FCCD968F001173043618F9E190DC ();
// 0x0000058A UnityEngine.Material INGUIAtlas::get_spriteMaterial()
// 0x0000058B System.Void INGUIAtlas::set_spriteMaterial(UnityEngine.Material)
// 0x0000058C System.Collections.Generic.List`1<UISpriteData> INGUIAtlas::get_spriteList()
// 0x0000058D System.Void INGUIAtlas::set_spriteList(System.Collections.Generic.List`1<UISpriteData>)
// 0x0000058E UnityEngine.Texture INGUIAtlas::get_texture()
// 0x0000058F System.Single INGUIAtlas::get_pixelSize()
// 0x00000590 System.Void INGUIAtlas::set_pixelSize(System.Single)
// 0x00000591 System.Boolean INGUIAtlas::get_premultipliedAlpha()
// 0x00000592 INGUIAtlas INGUIAtlas::get_replacement()
// 0x00000593 System.Void INGUIAtlas::set_replacement(INGUIAtlas)
// 0x00000594 UISpriteData INGUIAtlas::GetSprite(System.String)
// 0x00000595 BetterList`1<System.String> INGUIAtlas::GetListOfSprites()
// 0x00000596 BetterList`1<System.String> INGUIAtlas::GetListOfSprites(System.String)
// 0x00000597 System.Boolean INGUIAtlas::References(INGUIAtlas)
// 0x00000598 System.Void INGUIAtlas::MarkAsChanged()
// 0x00000599 System.Void INGUIAtlas::SortAlphabetically()
// 0x0000059A UnityEngine.Material NGUIAtlas::get_spriteMaterial()
extern void NGUIAtlas_get_spriteMaterial_mDC91C1E0B3E5CA4178BE2ABC94222DBD30A07683 ();
// 0x0000059B System.Void NGUIAtlas::set_spriteMaterial(UnityEngine.Material)
extern void NGUIAtlas_set_spriteMaterial_mC0DB3C2B430BA7011DCEB7EFBE1CA939D3FFB4B2 ();
// 0x0000059C System.Boolean NGUIAtlas::get_premultipliedAlpha()
extern void NGUIAtlas_get_premultipliedAlpha_mE842C894EB7DEFA1B0072F1DDC7A7545ECD6854A ();
// 0x0000059D System.Collections.Generic.List`1<UISpriteData> NGUIAtlas::get_spriteList()
extern void NGUIAtlas_get_spriteList_mAAC68E66640C86E858C1EE3725F857D8890777EF ();
// 0x0000059E System.Void NGUIAtlas::set_spriteList(System.Collections.Generic.List`1<UISpriteData>)
extern void NGUIAtlas_set_spriteList_mDEB029FCDF8FA751D0DB0DAEC217E9B9390DDEED ();
// 0x0000059F UnityEngine.Texture NGUIAtlas::get_texture()
extern void NGUIAtlas_get_texture_m21FBD3CBF56A4AB5E9632A191CB31BFEA9DA0FC0 ();
// 0x000005A0 System.Single NGUIAtlas::get_pixelSize()
extern void NGUIAtlas_get_pixelSize_m8EC36284EE10157940943E51C54E6E3AC4407A60 ();
// 0x000005A1 System.Void NGUIAtlas::set_pixelSize(System.Single)
extern void NGUIAtlas_set_pixelSize_m36B7B79A352B71684734CB1832177F3FCC5FE411 ();
// 0x000005A2 INGUIAtlas NGUIAtlas::get_replacement()
extern void NGUIAtlas_get_replacement_mFBC9665D6D46D096DDEC7571584456CD7464646B ();
// 0x000005A3 System.Void NGUIAtlas::set_replacement(INGUIAtlas)
extern void NGUIAtlas_set_replacement_m1B0D15C5951B9AEBDA4A45CA21510647DE98590D ();
// 0x000005A4 UISpriteData NGUIAtlas::GetSprite(System.String)
extern void NGUIAtlas_GetSprite_mD03D8C5763FB4410EE71F4B698722B86074C82F6 ();
// 0x000005A5 System.Void NGUIAtlas::MarkSpriteListAsChanged()
extern void NGUIAtlas_MarkSpriteListAsChanged_m08CBC5DC15CAC2BE4C08D74FF7DF7A0A3CC1B050 ();
// 0x000005A6 System.Void NGUIAtlas::SortAlphabetically()
extern void NGUIAtlas_SortAlphabetically_m1F8F9ADC4A8EF1168B676BD99768FA3A62FB6314 ();
// 0x000005A7 BetterList`1<System.String> NGUIAtlas::GetListOfSprites()
extern void NGUIAtlas_GetListOfSprites_mBF286BE86D2213E398312349279E5BDAB96D4DBE ();
// 0x000005A8 BetterList`1<System.String> NGUIAtlas::GetListOfSprites(System.String)
extern void NGUIAtlas_GetListOfSprites_m72887872FA71580D08FB35CAA3FC651DC37CB815 ();
// 0x000005A9 System.Boolean NGUIAtlas::References(INGUIAtlas)
extern void NGUIAtlas_References_mA33F20928BAFE8B08E11CA2A52A8B33879F6F0E8 ();
// 0x000005AA System.Void NGUIAtlas::MarkAsChanged()
extern void NGUIAtlas_MarkAsChanged_mD47E21DFA9AD5C8F6A7988F3C52034EFD2E4913E ();
// 0x000005AB System.Void NGUIAtlas::.ctor()
extern void NGUIAtlas__ctor_mC5C882DEA68246DC8691BF24461D7F70C891EDC2 ();
// 0x000005AC BMFont INGUIFont::get_bmFont()
// 0x000005AD System.Void INGUIFont::set_bmFont(BMFont)
// 0x000005AE System.Int32 INGUIFont::get_texWidth()
// 0x000005AF System.Void INGUIFont::set_texWidth(System.Int32)
// 0x000005B0 System.Int32 INGUIFont::get_texHeight()
// 0x000005B1 System.Void INGUIFont::set_texHeight(System.Int32)
// 0x000005B2 System.Boolean INGUIFont::get_hasSymbols()
// 0x000005B3 System.Collections.Generic.List`1<BMSymbol> INGUIFont::get_symbols()
// 0x000005B4 System.Void INGUIFont::set_symbols(System.Collections.Generic.List`1<BMSymbol>)
// 0x000005B5 INGUIAtlas INGUIFont::get_atlas()
// 0x000005B6 System.Void INGUIFont::set_atlas(INGUIAtlas)
// 0x000005B7 UISpriteData INGUIFont::GetSprite(System.String)
// 0x000005B8 UnityEngine.Material INGUIFont::get_material()
// 0x000005B9 System.Void INGUIFont::set_material(UnityEngine.Material)
// 0x000005BA System.Boolean INGUIFont::get_premultipliedAlphaShader()
// 0x000005BB System.Boolean INGUIFont::get_packedFontShader()
// 0x000005BC UnityEngine.Texture2D INGUIFont::get_texture()
// 0x000005BD UnityEngine.Rect INGUIFont::get_uvRect()
// 0x000005BE System.Void INGUIFont::set_uvRect(UnityEngine.Rect)
// 0x000005BF System.String INGUIFont::get_spriteName()
// 0x000005C0 System.Void INGUIFont::set_spriteName(System.String)
// 0x000005C1 System.Boolean INGUIFont::get_isValid()
// 0x000005C2 System.Int32 INGUIFont::get_defaultSize()
// 0x000005C3 System.Void INGUIFont::set_defaultSize(System.Int32)
// 0x000005C4 UISpriteData INGUIFont::get_sprite()
// 0x000005C5 INGUIFont INGUIFont::get_replacement()
// 0x000005C6 System.Void INGUIFont::set_replacement(INGUIFont)
// 0x000005C7 INGUIFont INGUIFont::get_finalFont()
// 0x000005C8 System.Boolean INGUIFont::get_isDynamic()
// 0x000005C9 UnityEngine.Font INGUIFont::get_dynamicFont()
// 0x000005CA System.Void INGUIFont::set_dynamicFont(UnityEngine.Font)
// 0x000005CB UnityEngine.FontStyle INGUIFont::get_dynamicFontStyle()
// 0x000005CC System.Void INGUIFont::set_dynamicFontStyle(UnityEngine.FontStyle)
// 0x000005CD System.Boolean INGUIFont::References(INGUIFont)
// 0x000005CE System.Void INGUIFont::MarkAsChanged()
// 0x000005CF System.Void INGUIFont::UpdateUVRect()
// 0x000005D0 BMSymbol INGUIFont::MatchSymbol(System.String,System.Int32,System.Int32)
// 0x000005D1 System.Void INGUIFont::AddSymbol(System.String,System.String)
// 0x000005D2 System.Void INGUIFont::RemoveSymbol(System.String)
// 0x000005D3 System.Void INGUIFont::RenameSymbol(System.String,System.String)
// 0x000005D4 System.Boolean INGUIFont::UsesSprite(System.String)
// 0x000005D5 BMFont NGUIFont::get_bmFont()
extern void NGUIFont_get_bmFont_m7243FD4762DE6DDEF271C71E3475C47401155087 ();
// 0x000005D6 System.Void NGUIFont::set_bmFont(BMFont)
extern void NGUIFont_set_bmFont_mB6FEE1B76F32B3C29EB641F547380A9327E1D332 ();
// 0x000005D7 System.Int32 NGUIFont::get_texWidth()
extern void NGUIFont_get_texWidth_m3AED6D163B214B4A57E4E72A92447DC8CA015471 ();
// 0x000005D8 System.Void NGUIFont::set_texWidth(System.Int32)
extern void NGUIFont_set_texWidth_m4C74CF4EA349DE26CA0E97E8A4BB0A743A3090C3 ();
// 0x000005D9 System.Int32 NGUIFont::get_texHeight()
extern void NGUIFont_get_texHeight_mEA3A280AA9DFB8792483E6E41452382AF9F69FA5 ();
// 0x000005DA System.Void NGUIFont::set_texHeight(System.Int32)
extern void NGUIFont_set_texHeight_m1E58F3C23EF1E30CE426E93C568F368C03DB470B ();
// 0x000005DB System.Boolean NGUIFont::get_hasSymbols()
extern void NGUIFont_get_hasSymbols_mE5EBE4E83D3CAE6A77CBFB052D7CEFDF735F5B15 ();
// 0x000005DC System.Collections.Generic.List`1<BMSymbol> NGUIFont::get_symbols()
extern void NGUIFont_get_symbols_m32FCD752223135FAEFFB6F29CCED681CB7AE652A ();
// 0x000005DD System.Void NGUIFont::set_symbols(System.Collections.Generic.List`1<BMSymbol>)
extern void NGUIFont_set_symbols_m7BBE356A18D55DC6C890A4E90B19B870C8F89B06 ();
// 0x000005DE INGUIAtlas NGUIFont::get_atlas()
extern void NGUIFont_get_atlas_mF6359BF502AEFB82E8FA349AA259390EE5E45095 ();
// 0x000005DF System.Void NGUIFont::set_atlas(INGUIAtlas)
extern void NGUIFont_set_atlas_mADE8DFD6BFEC11C965896A19214417427E23448B ();
// 0x000005E0 UISpriteData NGUIFont::GetSprite(System.String)
extern void NGUIFont_GetSprite_m647C0B34A7008A4F76FB0037CD6D88C3382058F1 ();
// 0x000005E1 UnityEngine.Material NGUIFont::get_material()
extern void NGUIFont_get_material_mDA3ABB8F8937F16A141E3804BB87217AA16C4887 ();
// 0x000005E2 System.Void NGUIFont::set_material(UnityEngine.Material)
extern void NGUIFont_set_material_mD609E59B02BE8DBE2FFF7609A5AB3CE98A1FC0B2 ();
// 0x000005E3 System.Boolean NGUIFont::get_premultipliedAlpha()
extern void NGUIFont_get_premultipliedAlpha_mB28902FCA401E930BF46C38EF14A3272128EE177 ();
// 0x000005E4 System.Boolean NGUIFont::get_premultipliedAlphaShader()
extern void NGUIFont_get_premultipliedAlphaShader_m6513460C466A7760DA90F8873AD61E9D473B1B92 ();
// 0x000005E5 System.Boolean NGUIFont::get_packedFontShader()
extern void NGUIFont_get_packedFontShader_m751904214ED2D7500BD15E4B0B9D351F3F1EEC32 ();
// 0x000005E6 UnityEngine.Texture2D NGUIFont::get_texture()
extern void NGUIFont_get_texture_mB35A71CE9254E38B4E25BD3DA0A0A676882F4502 ();
// 0x000005E7 UnityEngine.Rect NGUIFont::get_uvRect()
extern void NGUIFont_get_uvRect_mC4E244D5EB6ECA7A2F24C5DAD790D3A7BCB62C2E ();
// 0x000005E8 System.Void NGUIFont::set_uvRect(UnityEngine.Rect)
extern void NGUIFont_set_uvRect_m4298757026765B7CB4B25DCAE9A276DF08875A48 ();
// 0x000005E9 System.String NGUIFont::get_spriteName()
extern void NGUIFont_get_spriteName_mF9E0E6C23790A5CFECAE040000BF4FCFEDC9DE53 ();
// 0x000005EA System.Void NGUIFont::set_spriteName(System.String)
extern void NGUIFont_set_spriteName_m1D809833B35A3B648D922E4FD7551ACA82B0AF12 ();
// 0x000005EB System.Boolean NGUIFont::get_isValid()
extern void NGUIFont_get_isValid_m67569FEDDBA8CC479728639EFE3A0EE0491F86FF ();
// 0x000005EC System.Int32 NGUIFont::get_size()
extern void NGUIFont_get_size_mC6C5528804EE4FE48251F47F529C58183ED1B28C ();
// 0x000005ED System.Void NGUIFont::set_size(System.Int32)
extern void NGUIFont_set_size_m77727F6D4B96BE6518A0911FB1ECF2BF415CEE2B ();
// 0x000005EE System.Int32 NGUIFont::get_defaultSize()
extern void NGUIFont_get_defaultSize_m50BF70C4E1E9292357FACF29F834A43994EB394F ();
// 0x000005EF System.Void NGUIFont::set_defaultSize(System.Int32)
extern void NGUIFont_set_defaultSize_mEB4287D6086A9988A2901B72AE287E5A03C47C48 ();
// 0x000005F0 UISpriteData NGUIFont::get_sprite()
extern void NGUIFont_get_sprite_m61E43BFD2E3DEEDB7D397DD219A52C8EA21A93FA ();
// 0x000005F1 INGUIFont NGUIFont::get_replacement()
extern void NGUIFont_get_replacement_m6C72BDEE4BB2FCBB67E7F50E05CA3667F67EB509 ();
// 0x000005F2 System.Void NGUIFont::set_replacement(INGUIFont)
extern void NGUIFont_set_replacement_m5408CC2234318B687ADE5E9574C41FBF8A577D64 ();
// 0x000005F3 INGUIFont NGUIFont::get_finalFont()
extern void NGUIFont_get_finalFont_m0A06FDD03A1538E83B0E209964ACA3D10A95B80D ();
// 0x000005F4 System.Boolean NGUIFont::get_isDynamic()
extern void NGUIFont_get_isDynamic_m20C51A821C667A0C696A238FCABC3DCC85B62B76 ();
// 0x000005F5 UnityEngine.Font NGUIFont::get_dynamicFont()
extern void NGUIFont_get_dynamicFont_m636ED1D01EF0DC1DD3913596290F78F3478D8ABB ();
// 0x000005F6 System.Void NGUIFont::set_dynamicFont(UnityEngine.Font)
extern void NGUIFont_set_dynamicFont_m3189483B77C2890260C67E74D953100C21062B60 ();
// 0x000005F7 UnityEngine.FontStyle NGUIFont::get_dynamicFontStyle()
extern void NGUIFont_get_dynamicFontStyle_m22268921B171DB4C363FB390B409B58E44CD89C2 ();
// 0x000005F8 System.Void NGUIFont::set_dynamicFontStyle(UnityEngine.FontStyle)
extern void NGUIFont_set_dynamicFontStyle_mF10FBF57AB22AAF6743EB77297DB3A0E1E339C2C ();
// 0x000005F9 System.Void NGUIFont::Trim()
extern void NGUIFont_Trim_m53A0DF44A82E85BC96FEC42425B78677F76B1605 ();
// 0x000005FA System.Boolean NGUIFont::References(INGUIFont)
extern void NGUIFont_References_m72981A19BF112EA2E80E20B6320F0C725784FEB6 ();
// 0x000005FB System.Void NGUIFont::MarkAsChanged()
extern void NGUIFont_MarkAsChanged_mA3FB55C62DFD4DA5254E83C0BA3C7FE41148E045 ();
// 0x000005FC System.Void NGUIFont::UpdateUVRect()
extern void NGUIFont_UpdateUVRect_m0BAA77B591F68585BB743DD3DBD6A0E9C5474155 ();
// 0x000005FD BMSymbol NGUIFont::GetSymbol(System.String,System.Boolean)
extern void NGUIFont_GetSymbol_m6A0174EABFC155CCC7DA4458BDA8FCB24FD6087A ();
// 0x000005FE BMSymbol NGUIFont::MatchSymbol(System.String,System.Int32,System.Int32)
extern void NGUIFont_MatchSymbol_m2D00EE38464398EA6DEEE99B46A32525275330CA ();
// 0x000005FF System.Void NGUIFont::AddSymbol(System.String,System.String)
extern void NGUIFont_AddSymbol_mB604A233937C71214B9A9D6A446757D401FD91FE ();
// 0x00000600 System.Void NGUIFont::RemoveSymbol(System.String)
extern void NGUIFont_RemoveSymbol_mA6636E9B6DE56974B60138B06D16253BB084223D ();
// 0x00000601 System.Void NGUIFont::RenameSymbol(System.String,System.String)
extern void NGUIFont_RenameSymbol_mD35210C918828593C8072CAF4B8ECF449C560E61 ();
// 0x00000602 System.Boolean NGUIFont::UsesSprite(System.String)
extern void NGUIFont_UsesSprite_mDD3989EFCB3A0091765B7508A00E8AC1167C9FDD ();
// 0x00000603 System.Void NGUIFont::.ctor()
extern void NGUIFont__ctor_mA560F53147845E3ECBC7C40689EA2DAB36BC967B ();
// 0x00000604 UnityEngine.Sprite UI2DSprite::get_sprite2D()
extern void UI2DSprite_get_sprite2D_m1E1284E1490F600C822136B18468DD0C4D64DB20 ();
// 0x00000605 System.Void UI2DSprite::set_sprite2D(UnityEngine.Sprite)
extern void UI2DSprite_set_sprite2D_mAC7A95FBE25D09FD85F3D3BD95C0EE75B3DC8396 ();
// 0x00000606 UnityEngine.Material UI2DSprite::get_material()
extern void UI2DSprite_get_material_m6266CF7CD1E1FFB64E28F3561CB6D0CFDB0A0652 ();
// 0x00000607 System.Void UI2DSprite::set_material(UnityEngine.Material)
extern void UI2DSprite_set_material_mBB0582819D293C98DDA514C68CBA7688ED34AA99 ();
// 0x00000608 UnityEngine.Shader UI2DSprite::get_shader()
extern void UI2DSprite_get_shader_mBF9C277319B43B6D2A8E00DCE73ABDF2F15229B7 ();
// 0x00000609 System.Void UI2DSprite::set_shader(UnityEngine.Shader)
extern void UI2DSprite_set_shader_m3BC7A2EED4C05E1EE11615FDE584E6A4AB6FA70B ();
// 0x0000060A UnityEngine.Texture UI2DSprite::get_mainTexture()
extern void UI2DSprite_get_mainTexture_m42F3FB94C316EF4F62C1E66FBB79FF2CAF78DED0 ();
// 0x0000060B System.Boolean UI2DSprite::get_fixedAspect()
extern void UI2DSprite_get_fixedAspect_m8B6416DD2C5A1361B2F76405D2CB8302873DEB27 ();
// 0x0000060C System.Void UI2DSprite::set_fixedAspect(System.Boolean)
extern void UI2DSprite_set_fixedAspect_m2920944190915B89F55E87ABF7342CDE8075439A ();
// 0x0000060D System.Boolean UI2DSprite::get_premultipliedAlpha()
extern void UI2DSprite_get_premultipliedAlpha_mE7FDA2263FF7480B90644BCEFF25A7D802A09A45 ();
// 0x0000060E System.Single UI2DSprite::get_pixelSize()
extern void UI2DSprite_get_pixelSize_m55F2DF2F35BB32ACAAF870F1B93F378C76C33355 ();
// 0x0000060F UnityEngine.Vector4 UI2DSprite::get_drawingDimensions()
extern void UI2DSprite_get_drawingDimensions_m4C8B77E29E8F4D6D353A3CA3D196B8B1DE53746B ();
// 0x00000610 UnityEngine.Vector4 UI2DSprite::get_border()
extern void UI2DSprite_get_border_mD46482AA2F62A70CCF4E2C05FED74269CD8B5D3E ();
// 0x00000611 System.Void UI2DSprite::set_border(UnityEngine.Vector4)
extern void UI2DSprite_set_border_m8F1A4F8FBC42FBA96DB23D3FCF74299555201ACC ();
// 0x00000612 System.Void UI2DSprite::OnUpdate()
extern void UI2DSprite_OnUpdate_m0B277BD47412AA5F8E79D099DEA03EFC20C28C49 ();
// 0x00000613 System.Void UI2DSprite::MakePixelPerfect()
extern void UI2DSprite_MakePixelPerfect_m7631B7CBC156F5766F2AC96198F56D0B0D301380 ();
// 0x00000614 System.Void UI2DSprite::OnFill(System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Color>)
extern void UI2DSprite_OnFill_m2ECB3D234798945398A91D4C9F93503CC8B09C96 ();
// 0x00000615 System.Void UI2DSprite::.ctor()
extern void UI2DSprite__ctor_m49FEA47A62C512EA3EFDB302B798AF031FC99E3D ();
// 0x00000616 System.Boolean UI2DSpriteAnimation::get_isPlaying()
extern void UI2DSpriteAnimation_get_isPlaying_m6B91DE9C26AC0835676402D223400136469AC2D3 ();
// 0x00000617 System.Int32 UI2DSpriteAnimation::get_framesPerSecond()
extern void UI2DSpriteAnimation_get_framesPerSecond_m35E5264A030E87AC6C36D181A4D60445A2BAD65A ();
// 0x00000618 System.Void UI2DSpriteAnimation::set_framesPerSecond(System.Int32)
extern void UI2DSpriteAnimation_set_framesPerSecond_m7F440608C75F05F53EC34A96BFFF6DB73830BD80 ();
// 0x00000619 System.Void UI2DSpriteAnimation::Play()
extern void UI2DSpriteAnimation_Play_mD4A2041842B0F7262F7C9B8164EDAEF7317967DF ();
// 0x0000061A System.Void UI2DSpriteAnimation::Pause()
extern void UI2DSpriteAnimation_Pause_mA19F460F1AF9E37EB5F9B13A45D32EF0651860C9 ();
// 0x0000061B System.Void UI2DSpriteAnimation::ResetToBeginning()
extern void UI2DSpriteAnimation_ResetToBeginning_m216B696F56ACF81CED283724709AC4D4B3827805 ();
// 0x0000061C System.Void UI2DSpriteAnimation::Start()
extern void UI2DSpriteAnimation_Start_mEE2D459BFCA4CCA1603D1C0F597022449D7AC029 ();
// 0x0000061D System.Void UI2DSpriteAnimation::Update()
extern void UI2DSpriteAnimation_Update_m7B4D97123C0BED9408FAD9CB8E13FFB9AB00E3E2 ();
// 0x0000061E System.Void UI2DSpriteAnimation::UpdateSprite()
extern void UI2DSpriteAnimation_UpdateSprite_m8400E5E064EF6CE941F8E44FA3DF48F8063202B2 ();
// 0x0000061F System.Void UI2DSpriteAnimation::.ctor()
extern void UI2DSpriteAnimation__ctor_m553CB9535E79BEE9EB33CE394417572E533B8876 ();
// 0x00000620 System.Void UIAnchor::OnEnable()
extern void UIAnchor_OnEnable_m87C5C94DD470BDC63F2F19CA86BADE170654CF69 ();
// 0x00000621 System.Void UIAnchor::OnDisable()
extern void UIAnchor_OnDisable_mBDF4A12356A7512878C8AEF6F2321E971BF596B4 ();
// 0x00000622 System.Void UIAnchor::ScreenSizeChanged()
extern void UIAnchor_ScreenSizeChanged_mCD05D6AAACD945B7C6406C2CA61F9F49929D8EFD ();
// 0x00000623 System.Void UIAnchor::Start()
extern void UIAnchor_Start_m3C2265A24196A042BB15A69B362F3B78E185B8BB ();
// 0x00000624 System.Void UIAnchor::Update()
extern void UIAnchor_Update_m00FA68CBB09AE49A550EBF2FAA49DD70151E4591 ();
// 0x00000625 System.Void UIAnchor::.ctor()
extern void UIAnchor__ctor_m7EA683F444FA30CF0158A5EF1A63C760985794CD ();
// 0x00000626 UnityEngine.Material UIAtlas::get_spriteMaterial()
extern void UIAtlas_get_spriteMaterial_m7371EEDA6F2273E5F939C5ED397446183CBDA923 ();
// 0x00000627 System.Void UIAtlas::set_spriteMaterial(UnityEngine.Material)
extern void UIAtlas_set_spriteMaterial_m6273EC1924D97E1AD1E2C7B2FF3A6FCDC315868A ();
// 0x00000628 System.Boolean UIAtlas::get_premultipliedAlpha()
extern void UIAtlas_get_premultipliedAlpha_m5774627993086F87F282C6EA68E7FC1F012EA219 ();
// 0x00000629 System.Collections.Generic.List`1<UISpriteData> UIAtlas::get_spriteList()
extern void UIAtlas_get_spriteList_m07EA1B091518CAB738F9AEB75AE6B681F777133A ();
// 0x0000062A System.Void UIAtlas::set_spriteList(System.Collections.Generic.List`1<UISpriteData>)
extern void UIAtlas_set_spriteList_m2F58785C0632D40772650F12C95A8767893BAF73 ();
// 0x0000062B UnityEngine.Texture UIAtlas::get_texture()
extern void UIAtlas_get_texture_m9FE3AED0F265F5E46D2C5249D3BBB48107EC5328 ();
// 0x0000062C System.Single UIAtlas::get_pixelSize()
extern void UIAtlas_get_pixelSize_m76F4624BAC553F89295601A4E986E4E3635EDACC ();
// 0x0000062D System.Void UIAtlas::set_pixelSize(System.Single)
extern void UIAtlas_set_pixelSize_m05161F9ED542CC2D94F4182BD91D2165AA92237F ();
// 0x0000062E INGUIAtlas UIAtlas::get_replacement()
extern void UIAtlas_get_replacement_m1FD1ABA22D29C761064F38E617355813CDBB52E6 ();
// 0x0000062F System.Void UIAtlas::set_replacement(INGUIAtlas)
extern void UIAtlas_set_replacement_mD10C3CAE1EAF8DE25F85468059A148DD4DEE5577 ();
// 0x00000630 UISpriteData UIAtlas::GetSprite(System.String)
extern void UIAtlas_GetSprite_m37CFA130126C3727653AA3F6680AD724A566F9BC ();
// 0x00000631 System.Void UIAtlas::MarkSpriteListAsChanged()
extern void UIAtlas_MarkSpriteListAsChanged_mA300FAD6DA9BE967C2DE425755CF6E1B6AA70BB2 ();
// 0x00000632 System.Void UIAtlas::SortAlphabetically()
extern void UIAtlas_SortAlphabetically_mF6D665166E733EB92646196733A22628152EA636 ();
// 0x00000633 BetterList`1<System.String> UIAtlas::GetListOfSprites()
extern void UIAtlas_GetListOfSprites_mBBECAEBF4A615D4E9322A542938B8FA985E07CBD ();
// 0x00000634 BetterList`1<System.String> UIAtlas::GetListOfSprites(System.String)
extern void UIAtlas_GetListOfSprites_m866707A85E4F30B9E36EB386BF902D5CB4F14509 ();
// 0x00000635 System.Boolean UIAtlas::References(INGUIAtlas)
extern void UIAtlas_References_mC91AD50368654CC0785D111991267920F102C004 ();
// 0x00000636 System.Void UIAtlas::MarkAsChanged()
extern void UIAtlas_MarkAsChanged_m53C35D8CCB3EC410E821BDC19F8C2F48FAA33D78 ();
// 0x00000637 System.Boolean UIAtlas::Upgrade()
extern void UIAtlas_Upgrade_m8E9984FE90DE7C88B9998117FAC1255D869171AB ();
// 0x00000638 System.Void UIAtlas::.ctor()
extern void UIAtlas__ctor_m0362C4C0F41843BA4D10D3589ED6A52E0206B2E6 ();
// 0x00000639 System.Boolean UICamera::get_stickyPress()
extern void UICamera_get_stickyPress_m4289E4BC47B126F40FF3D94ACAE0CDEDE6A96975 ();
// 0x0000063A System.Boolean UICamera::get_disableController()
extern void UICamera_get_disableController_m53D285BB8CC81822F59956C453FB8EC2FDEEA526 ();
// 0x0000063B System.Void UICamera::set_disableController(System.Boolean)
extern void UICamera_set_disableController_mD0DFCEA1D399DDCAEEC6610992FB416833BFD94A ();
// 0x0000063C UnityEngine.Vector2 UICamera::get_lastTouchPosition()
extern void UICamera_get_lastTouchPosition_m55F7C6E5D2C7A851A7AE555A97C83EF1462D9BDF ();
// 0x0000063D System.Void UICamera::set_lastTouchPosition(UnityEngine.Vector2)
extern void UICamera_set_lastTouchPosition_m6497F86F5EC29DE8D635766474495DF4854C4377 ();
// 0x0000063E UnityEngine.Vector2 UICamera::get_lastEventPosition()
extern void UICamera_get_lastEventPosition_mF96ACC6364A504251DC5A35DA40EEA747584F4A7 ();
// 0x0000063F System.Void UICamera::set_lastEventPosition(UnityEngine.Vector2)
extern void UICamera_set_lastEventPosition_m9691322578126A5FA41B454C2E0F9987E3D6AC36 ();
// 0x00000640 UICamera UICamera::get_first()
extern void UICamera_get_first_m350ED998D44F98B449B0238F07FD9CA93ADD1D72 ();
// 0x00000641 UICamera_ControlScheme UICamera::get_currentScheme()
extern void UICamera_get_currentScheme_mF9535F4CA9EA5AA19806D5C6DEAA0B542B219E0C ();
// 0x00000642 System.Void UICamera::set_currentScheme(UICamera_ControlScheme)
extern void UICamera_set_currentScheme_m313A1E0951B8E2ED0E60F230B383400AB8BE47EC ();
// 0x00000643 UnityEngine.KeyCode UICamera::get_currentKey()
extern void UICamera_get_currentKey_mC322520CC1398D25DE63B15AEC6809283098CF05 ();
// 0x00000644 System.Void UICamera::set_currentKey(UnityEngine.KeyCode)
extern void UICamera_set_currentKey_mA37CD11C4A5C833FAB5936D36C8D0C8A2F340A26 ();
// 0x00000645 UnityEngine.Ray UICamera::get_currentRay()
extern void UICamera_get_currentRay_mE89B1AC7039F1E5559C61E37DE5EB4BD8BC872C3 ();
// 0x00000646 System.Boolean UICamera::get_inputHasFocus()
extern void UICamera_get_inputHasFocus_mAD29557111E3C2F2B06BFA0E1432E4C015D66CDF ();
// 0x00000647 UnityEngine.GameObject UICamera::get_genericEventHandler()
extern void UICamera_get_genericEventHandler_mCF068433F16A810812C355743FDD6851673564C3 ();
// 0x00000648 System.Void UICamera::set_genericEventHandler(UnityEngine.GameObject)
extern void UICamera_set_genericEventHandler_m273F1E600062BE1CB82F5D0F6E81BDE9CB49FDE0 ();
// 0x00000649 UICamera_MouseOrTouch UICamera::get_mouse0()
extern void UICamera_get_mouse0_m9426DED7833A196B9EDA70357D5DE9955CD8382F ();
// 0x0000064A UICamera_MouseOrTouch UICamera::get_mouse1()
extern void UICamera_get_mouse1_m769FD46B12D60B2815043DF679DE29DE7B8A26AB ();
// 0x0000064B UICamera_MouseOrTouch UICamera::get_mouse2()
extern void UICamera_get_mouse2_mAECAC667003790D721AC8D7E48DC5B7F90DC7E49 ();
// 0x0000064C System.Boolean UICamera::get_handlesEvents()
extern void UICamera_get_handlesEvents_mF529A27FCA66621F66D40A8DAAB27778682162D2 ();
// 0x0000064D UnityEngine.Camera UICamera::get_cachedCamera()
extern void UICamera_get_cachedCamera_m5D9A047CC4DEB7E47277D855AB4618F6B5B39718 ();
// 0x0000064E UnityEngine.GameObject UICamera::get_tooltipObject()
extern void UICamera_get_tooltipObject_m757350B10B875BDBAD81A89951AC02EC8256C3AA ();
// 0x0000064F System.Void UICamera::set_tooltipObject(UnityEngine.GameObject)
extern void UICamera_set_tooltipObject_mE48BBEA24C26C5135165AB3A230C95A48944A59A ();
// 0x00000650 System.Boolean UICamera::IsPartOfUI(UnityEngine.GameObject)
extern void UICamera_IsPartOfUI_m3E5DB6029CC731162F8A1076F597CAA65CE89CDB ();
// 0x00000651 System.Boolean UICamera::get_isOverUI()
extern void UICamera_get_isOverUI_m3E925F361D4B998199661AE94082C5F89C02CD1B ();
// 0x00000652 System.Boolean UICamera::get_uiHasFocus()
extern void UICamera_get_uiHasFocus_m141895F2E0C394581CF456FCEF6DCC7FB98263D8 ();
// 0x00000653 System.Boolean UICamera::get_interactingWithUI()
extern void UICamera_get_interactingWithUI_m8EE9692990230B8F75413AFDF8825E6D59CD00AE ();
// 0x00000654 UnityEngine.GameObject UICamera::get_hoveredObject()
extern void UICamera_get_hoveredObject_m6187088315284EB93BF5173A9C949592A70C56BC ();
// 0x00000655 System.Void UICamera::set_hoveredObject(UnityEngine.GameObject)
extern void UICamera_set_hoveredObject_mE36A960DC8BF0955CA25F2EBF977C2C2F09E7C69 ();
// 0x00000656 UnityEngine.GameObject UICamera::get_controllerNavigationObject()
extern void UICamera_get_controllerNavigationObject_mB4678E1FFBDA02C02876193A1CCC916B04DFD360 ();
// 0x00000657 System.Void UICamera::set_controllerNavigationObject(UnityEngine.GameObject)
extern void UICamera_set_controllerNavigationObject_mF3A475C9842B26EC3A0DF5EA018A782EDBF3FC80 ();
// 0x00000658 UnityEngine.GameObject UICamera::get_selectedObject()
extern void UICamera_get_selectedObject_mF2C7E24A50234C79FE6ADC9FD163286B468B7312 ();
// 0x00000659 System.Void UICamera::set_selectedObject(UnityEngine.GameObject)
extern void UICamera_set_selectedObject_m684E76F4F6FC35B63D9969E494FFD7EDAFF7553E ();
// 0x0000065A System.Boolean UICamera::IsPressed(UnityEngine.GameObject)
extern void UICamera_IsPressed_m451DF6B0635A5B247D8DF707DA5277247D75E9B5 ();
// 0x0000065B System.Int32 UICamera::get_touchCount()
extern void UICamera_get_touchCount_m5ABC8F8CD89D38D495D38C09BFC5151FF6449BF5 ();
// 0x0000065C System.Int32 UICamera::CountInputSources()
extern void UICamera_CountInputSources_mACC286DD14458DA213056B1C4FDEBA477652EF27 ();
// 0x0000065D System.Int32 UICamera::get_dragCount()
extern void UICamera_get_dragCount_mD4E72C1C288F18C9899A331D4E6103F5B8F34C26 ();
// 0x0000065E UnityEngine.Camera UICamera::get_mainCamera()
extern void UICamera_get_mainCamera_m608FDDCB5F90D887B187F9415C6CE379704DD96D ();
// 0x0000065F UICamera UICamera::get_eventHandler()
extern void UICamera_get_eventHandler_mE028D2D3F68A17FAC0B0501C16CD1AFE65BA6246 ();
// 0x00000660 System.Int32 UICamera::CompareFunc(UICamera,UICamera)
extern void UICamera_CompareFunc_m1768E988EDD3D9E68C0EC119FA01EA989FA16DBC ();
// 0x00000661 UnityEngine.Rigidbody UICamera::FindRootRigidbody(UnityEngine.Transform)
extern void UICamera_FindRootRigidbody_mBCB9EFDA741DA78F111FF29D4D1C976CDC9C88CB ();
// 0x00000662 UnityEngine.Rigidbody2D UICamera::FindRootRigidbody2D(UnityEngine.Transform)
extern void UICamera_FindRootRigidbody2D_mEF6B37400FE4FF381C2EA4907D63AF2720C6DB82 ();
// 0x00000663 System.Void UICamera::Raycast(UICamera_MouseOrTouch)
extern void UICamera_Raycast_m99FD036EF140E8B716C6AE4AB42145653AA065F1 ();
// 0x00000664 System.Boolean UICamera::Raycast(UnityEngine.Vector3)
extern void UICamera_Raycast_m9A633A8F7041E28AA90E07B2D74AC8EBFB2F1848 ();
// 0x00000665 System.Boolean UICamera::IsVisible(UnityEngine.Vector3,UnityEngine.GameObject)
extern void UICamera_IsVisible_mFF8DF92B236B0F365E684A1AA235F031EE21273A ();
// 0x00000666 System.Boolean UICamera::IsVisible(UICamera_DepthEntry&)
extern void UICamera_IsVisible_mFA2D887599BF69450F4EE5967157CE7AADB8E9EA ();
// 0x00000667 System.Boolean UICamera::IsHighlighted(UnityEngine.GameObject)
extern void UICamera_IsHighlighted_m780968D78E50807FFAAD96FD75AFB2424D4AAF74 ();
// 0x00000668 UICamera UICamera::FindCameraForLayer(System.Int32)
extern void UICamera_FindCameraForLayer_m697DF5ADBF9DE45F2AC644CC2E54BE66E137B2E0 ();
// 0x00000669 System.Int32 UICamera::GetDirection(UnityEngine.KeyCode,UnityEngine.KeyCode)
extern void UICamera_GetDirection_mE127CF716DC7F15886CB351696C3C997FECE6018 ();
// 0x0000066A System.Int32 UICamera::GetDirection(UnityEngine.KeyCode,UnityEngine.KeyCode,UnityEngine.KeyCode,UnityEngine.KeyCode)
extern void UICamera_GetDirection_mF9D6D9D93B850A86BFA3D59B4A551BCBFC092B41 ();
// 0x0000066B System.Int32 UICamera::GetDirection(System.String)
extern void UICamera_GetDirection_mD76C8CE5D965D1BAE36373360DADE0C63BBADB67 ();
// 0x0000066C System.Void UICamera::Notify(UnityEngine.GameObject,System.String,System.Object)
extern void UICamera_Notify_m332E4C0410D4FFF5BE5D37AA14AF49D304790690 ();
// 0x0000066D System.Void UICamera::Awake()
extern void UICamera_Awake_mFB78DA7BFE9F4FF67A16C805F4E27E1596A7E54B ();
// 0x0000066E System.Void UICamera::OnEnable()
extern void UICamera_OnEnable_m3CDBB9AF0B2BB140FF2C92FBA7E8CDF9D0845A26 ();
// 0x0000066F System.Void UICamera::OnDisable()
extern void UICamera_OnDisable_mB5EBF7DCA72ECC1AC9D7F6C876843A561A220DBC ();
// 0x00000670 System.Void UICamera::Start()
extern void UICamera_Start_m5232F49E693490097B1F56A5AF8BE4E5D24E3F82 ();
// 0x00000671 System.Void UICamera::StartIgnoring()
extern void UICamera_StartIgnoring_m22CE508E12581B0D83A132179D8CC4B3768B39B4 ();
// 0x00000672 System.Void UICamera::StopIgnoring()
extern void UICamera_StopIgnoring_m83B4332C8763E8F899FEB81342E5BC872D6CC853 ();
// 0x00000673 System.Void UICamera::Update()
extern void UICamera_Update_m362960F50925D887D20B13BF23D6262423BA41B5 ();
// 0x00000674 System.Void UICamera::LateUpdate()
extern void UICamera_LateUpdate_mDBFB6BCDF00A1BABFD19880860815CE2B0C50B75 ();
// 0x00000675 System.Void UICamera::ProcessEvents()
extern void UICamera_ProcessEvents_mC44E0C86E7ED295D1EC6561526525F608A5D5BDE ();
// 0x00000676 System.Void UICamera::ProcessMouse()
extern void UICamera_ProcessMouse_m36664D44B02381C1C09D0327BC4C0DADF01FE932 ();
// 0x00000677 System.Void UICamera::ProcessTouches()
extern void UICamera_ProcessTouches_m3103656DD27C522902BC5132CACA50B6F6C1576D ();
// 0x00000678 System.Void UICamera::ProcessFakeTouches()
extern void UICamera_ProcessFakeTouches_m0ADEFB69FC9D4906D09D2F9268C642584ADF1A6C ();
// 0x00000679 System.Void UICamera::ProcessOthers()
extern void UICamera_ProcessOthers_mA438599E5E33F3186C08740A90A3F53E691E23D0 ();
// 0x0000067A System.Void UICamera::ProcessPress(System.Boolean,System.Single,System.Single)
extern void UICamera_ProcessPress_mA6292ABA2A1EC20CD7E6192690724F7E8F411B3E ();
// 0x0000067B System.Void UICamera::ProcessRelease(System.Boolean,System.Single)
extern void UICamera_ProcessRelease_m8B5D117C3E8FB1CE9ADAE3664BFF13E621F68E9B ();
// 0x0000067C System.Boolean UICamera::HasCollider(UnityEngine.GameObject)
extern void UICamera_HasCollider_mF5E5787E537DE8D0F5DA2CFED3A21B49814DC4F7 ();
// 0x0000067D System.Void UICamera::ProcessTouch(System.Boolean,System.Boolean)
extern void UICamera_ProcessTouch_mE0567F856F14E5BA7E82DBDB4C294ED6B409C5D6 ();
// 0x0000067E System.Void UICamera::CancelNextTooltip()
extern void UICamera_CancelNextTooltip_m9EAD7EF6FF656B04FD5EF27B3C944E0B94DE8C35 ();
// 0x0000067F System.Boolean UICamera::ShowTooltip(UnityEngine.GameObject)
extern void UICamera_ShowTooltip_mBBA6E4250B0311FA799E12A0395D102CEBE4B2B8 ();
// 0x00000680 System.Boolean UICamera::HideTooltip()
extern void UICamera_HideTooltip_m685D7DC6784E7C44CB9C918D1F9505B4EE5D1836 ();
// 0x00000681 System.Void UICamera::ResetTooltip(System.Single)
extern void UICamera_ResetTooltip_mD702FB5126BA49F3E2B33998CE92A23257CAA518 ();
// 0x00000682 System.Void UICamera::.ctor()
extern void UICamera__ctor_mE7572544B4650B5DE19D7E3A09A23F704517AB0B ();
// 0x00000683 System.Void UICamera::.cctor()
extern void UICamera__cctor_m043C3AE3CE3F46EF088D788BB4A0044FEC23C9D6 ();
// 0x00000684 System.Void UIColorPicker::Start()
extern void UIColorPicker_Start_m68B322FC9D63307BF5DE74DE713161882BC1BF25 ();
// 0x00000685 System.Void UIColorPicker::OnDestroy()
extern void UIColorPicker_OnDestroy_mFBC918AC4A99F2DE546735699D4EED5BACF51A31 ();
// 0x00000686 System.Void UIColorPicker::OnPress(System.Boolean)
extern void UIColorPicker_OnPress_m151965D6BFD35D6369AA68CAEBAC54FBA9D2924B ();
// 0x00000687 System.Void UIColorPicker::OnDrag(UnityEngine.Vector2)
extern void UIColorPicker_OnDrag_m8E9B5228C31485E6AE67CD39EA9EAE67F409A88D ();
// 0x00000688 System.Void UIColorPicker::OnPan(UnityEngine.Vector2)
extern void UIColorPicker_OnPan_m39EF615BA031305C91EC68A4D498F44796D34DEE ();
// 0x00000689 System.Void UIColorPicker::Sample()
extern void UIColorPicker_Sample_m32A569A7282A96708168619C7E1459F69E2D0757 ();
// 0x0000068A System.Void UIColorPicker::Select(UnityEngine.Vector2)
extern void UIColorPicker_Select_m9D1918E9E9E621102580F2B386B9BED8765D7A84 ();
// 0x0000068B UnityEngine.Vector2 UIColorPicker::Select(UnityEngine.Color)
extern void UIColorPicker_Select_m42E458C8FC3533B5B0672A2E959DA1964F27F3A4 ();
// 0x0000068C UnityEngine.Color UIColorPicker::Sample(System.Single,System.Single)
extern void UIColorPicker_Sample_mFE2340C551B0657CCCF4B44869DAF04716168ED0 ();
// 0x0000068D System.Void UIColorPicker::.ctor()
extern void UIColorPicker__ctor_m42FB46A9DEE608917930B107779CEB578B4A2045 ();
// 0x0000068E BMFont UIFont::get_bmFont()
extern void UIFont_get_bmFont_mC28A1C77FB375BB603767DD8FBCD38321F294190 ();
// 0x0000068F System.Void UIFont::set_bmFont(BMFont)
extern void UIFont_set_bmFont_m649C8858576E44B19E38026266BEB89F56B0D807 ();
// 0x00000690 System.Int32 UIFont::get_texWidth()
extern void UIFont_get_texWidth_m8171B683D5E10948F56150A74E04D5B8373B14FE ();
// 0x00000691 System.Void UIFont::set_texWidth(System.Int32)
extern void UIFont_set_texWidth_m96FD34E962B2631F3FB8876905A4D3625031356B ();
// 0x00000692 System.Int32 UIFont::get_texHeight()
extern void UIFont_get_texHeight_m3D7529E1503D3FF9645F1E51DF589D66246602FC ();
// 0x00000693 System.Void UIFont::set_texHeight(System.Int32)
extern void UIFont_set_texHeight_m86957EA62E7BB14860753CA9001703C0942DC7B1 ();
// 0x00000694 System.Boolean UIFont::get_hasSymbols()
extern void UIFont_get_hasSymbols_mD6DC06D619AAE2A67FF7076F4F0724ACD67B4B89 ();
// 0x00000695 System.Collections.Generic.List`1<BMSymbol> UIFont::get_symbols()
extern void UIFont_get_symbols_mEAF54452A3B1FB3B713D1069AAD988AA2A589F0C ();
// 0x00000696 System.Void UIFont::set_symbols(System.Collections.Generic.List`1<BMSymbol>)
extern void UIFont_set_symbols_mAEE3169A0F3B35F495871A7AFBDE58862E6168BE ();
// 0x00000697 INGUIAtlas UIFont::get_atlas()
extern void UIFont_get_atlas_mDD89B2869A9D95B763B576C6A5B1BEBCBE4C06AB ();
// 0x00000698 System.Void UIFont::set_atlas(INGUIAtlas)
extern void UIFont_set_atlas_mFC022BAF28AC1FBC3FABE5B1244440BDC1D9C808 ();
// 0x00000699 UISpriteData UIFont::GetSprite(System.String)
extern void UIFont_GetSprite_mD3A6303F857F0E99FD0E75B1A1A733E47D154AEE ();
// 0x0000069A UnityEngine.Material UIFont::get_material()
extern void UIFont_get_material_mF4DCD06F529651B25C7D23E0EF5F555261BE8186 ();
// 0x0000069B System.Void UIFont::set_material(UnityEngine.Material)
extern void UIFont_set_material_m28450BA18EB15FDAEA9C7803F58060F087ECA75E ();
// 0x0000069C System.Boolean UIFont::get_premultipliedAlpha()
extern void UIFont_get_premultipliedAlpha_m051130ACAF345DD391144C7739EF3BB3DC4A69A8 ();
// 0x0000069D System.Boolean UIFont::get_premultipliedAlphaShader()
extern void UIFont_get_premultipliedAlphaShader_mDFC54BF4554A8E70426C8037CF4FBB8AE2897FA3 ();
// 0x0000069E System.Boolean UIFont::get_packedFontShader()
extern void UIFont_get_packedFontShader_m0F406BCED91B77429389A87172D3E9042E9DC3B0 ();
// 0x0000069F UnityEngine.Texture2D UIFont::get_texture()
extern void UIFont_get_texture_m253AAA33E8525FF29C25C200B0FC611E257F70A5 ();
// 0x000006A0 UnityEngine.Rect UIFont::get_uvRect()
extern void UIFont_get_uvRect_m8814C374D7B3AE121D694F3B304DA94AA68B5ABF ();
// 0x000006A1 System.Void UIFont::set_uvRect(UnityEngine.Rect)
extern void UIFont_set_uvRect_mF7BDBA704CBB413BC4C8F7E9C15F5BB116204390 ();
// 0x000006A2 System.String UIFont::get_spriteName()
extern void UIFont_get_spriteName_m45C8F68EC9DC8BC4F0295A6FEAB4FF531AE98CEF ();
// 0x000006A3 System.Void UIFont::set_spriteName(System.String)
extern void UIFont_set_spriteName_mED22D5D38B739741538D545FC37831E23ECA1777 ();
// 0x000006A4 System.Boolean UIFont::get_isValid()
extern void UIFont_get_isValid_m67D1FC72810346A47A9F8C482E715C46302C0137 ();
// 0x000006A5 System.Int32 UIFont::get_size()
extern void UIFont_get_size_m4090157802E2D48AB95694386DB661E73F1B50DC ();
// 0x000006A6 System.Void UIFont::set_size(System.Int32)
extern void UIFont_set_size_m9AD684715540EA94C211227E2BFD5FBEAAE2150E ();
// 0x000006A7 System.Int32 UIFont::get_defaultSize()
extern void UIFont_get_defaultSize_m19D708D60FD82A82CE6A3438F26A96EDE7C61442 ();
// 0x000006A8 System.Void UIFont::set_defaultSize(System.Int32)
extern void UIFont_set_defaultSize_mD67475EF3DCA22D69EA34EA520CBAB0047C42B9B ();
// 0x000006A9 UISpriteData UIFont::get_sprite()
extern void UIFont_get_sprite_m416D73C6CA299A01525211CA9AA003972534943C ();
// 0x000006AA INGUIFont UIFont::get_replacement()
extern void UIFont_get_replacement_m20F942AD6CB79776A2FBA6A84C0F905FD4B34BCD ();
// 0x000006AB System.Void UIFont::set_replacement(INGUIFont)
extern void UIFont_set_replacement_mEE0DFE7CAA5622405708974A09757BB5DE25614D ();
// 0x000006AC INGUIFont UIFont::get_finalFont()
extern void UIFont_get_finalFont_mD85A4FC36E28745F3215FF1A621DB0CD129C99B8 ();
// 0x000006AD System.Boolean UIFont::get_isDynamic()
extern void UIFont_get_isDynamic_mCE3444BEFA124649D682C71A2F360A0C24782166 ();
// 0x000006AE UnityEngine.Font UIFont::get_dynamicFont()
extern void UIFont_get_dynamicFont_m4BCB8A0FD57D0E01071B611B44FFF99D4844FE72 ();
// 0x000006AF System.Void UIFont::set_dynamicFont(UnityEngine.Font)
extern void UIFont_set_dynamicFont_m3412D992F4387FBFB84DCAB7080D21216EBAB1F0 ();
// 0x000006B0 UnityEngine.FontStyle UIFont::get_dynamicFontStyle()
extern void UIFont_get_dynamicFontStyle_m6DAAB335734B6C579D3C4023231BBDFB81D8EFE9 ();
// 0x000006B1 System.Void UIFont::set_dynamicFontStyle(UnityEngine.FontStyle)
extern void UIFont_set_dynamicFontStyle_mFDFDC51D35A9E047541920A2982DCA8C63261544 ();
// 0x000006B2 System.Void UIFont::Trim()
extern void UIFont_Trim_m36A261EC90F7BDF9B5180F875533286614311148 ();
// 0x000006B3 System.Boolean UIFont::References(INGUIFont)
extern void UIFont_References_m719B1E019DBB1C36123BC847D3AD71E706E55D52 ();
// 0x000006B4 System.Void UIFont::MarkAsChanged()
extern void UIFont_MarkAsChanged_mDCF5F46D989B1F3DF6CCB2BB5A86DCC831B68D76 ();
// 0x000006B5 System.Void UIFont::UpdateUVRect()
extern void UIFont_UpdateUVRect_m803C299339722F39162A863B2C91D13C4053531C ();
// 0x000006B6 BMSymbol UIFont::GetSymbol(System.String,System.Boolean)
extern void UIFont_GetSymbol_m3F84B8F520FC0A5424679E1295D248C4919EE2FA ();
// 0x000006B7 BMSymbol UIFont::MatchSymbol(System.String,System.Int32,System.Int32)
extern void UIFont_MatchSymbol_m2841B326B52B200AF7602D4292C0C0AB5703B3E5 ();
// 0x000006B8 System.Void UIFont::AddSymbol(System.String,System.String)
extern void UIFont_AddSymbol_mDA2B8876C3F2B135ECD5B1F298A2F0FFC4570351 ();
// 0x000006B9 System.Void UIFont::RemoveSymbol(System.String)
extern void UIFont_RemoveSymbol_m10FCAF527E1899BC7FD9D659A724418A262F1C38 ();
// 0x000006BA System.Void UIFont::RenameSymbol(System.String,System.String)
extern void UIFont_RenameSymbol_m213E4B1A36A75DE8BB96B4998A3C39595A4DF85B ();
// 0x000006BB System.Boolean UIFont::UsesSprite(System.String)
extern void UIFont_UsesSprite_m368DDE708F7733FE6A9099FC31EED0C8AE1A37E3 ();
// 0x000006BC System.Void UIFont::.ctor()
extern void UIFont__ctor_m1031C618915F5981EC733CF8AB86E761C08A4BB6 ();
// 0x000006BD System.String UIInput::get_defaultText()
extern void UIInput_get_defaultText_mC4CFCE41D7070571CD81D3C11FCCE935A829E4FE ();
// 0x000006BE System.Void UIInput::set_defaultText(System.String)
extern void UIInput_set_defaultText_mDBFC354722CE3FD6051FF5E0D95DEA85D03415BF ();
// 0x000006BF UnityEngine.Color UIInput::get_defaultColor()
extern void UIInput_get_defaultColor_mCA3071A424BB59184F419AA5BFEAE8DFC6AC2F7C ();
// 0x000006C0 System.Void UIInput::set_defaultColor(UnityEngine.Color)
extern void UIInput_set_defaultColor_mA0F435DCC00644E9BF26E4BBA11EF1B67C2500EC ();
// 0x000006C1 System.Boolean UIInput::get_inputShouldBeHidden()
extern void UIInput_get_inputShouldBeHidden_m6BADEE0A29B13D2BC74CF5D13C651B1DDD96959A ();
// 0x000006C2 System.String UIInput::get_text()
extern void UIInput_get_text_m0B29D843E5E408FA5DC4F6D800B81F8ACBEC1DAF ();
// 0x000006C3 System.Void UIInput::set_text(System.String)
extern void UIInput_set_text_m9FC1CEDD041D2F390CDD508686DDD738148EC4B6 ();
// 0x000006C4 System.String UIInput::get_value()
extern void UIInput_get_value_m9CC94B340302716271E787645BA343BB7DD30822 ();
// 0x000006C5 System.Void UIInput::set_value(System.String)
extern void UIInput_set_value_m22AEF2CA93D0D2EF636639C3C54369AA6FF6865A ();
// 0x000006C6 System.Void UIInput::Set(System.String,System.Boolean)
extern void UIInput_Set_m674B5853322FC95305072B79FF4CA2B6693C4E16 ();
// 0x000006C7 System.Boolean UIInput::get_selected()
extern void UIInput_get_selected_m98C81B59877EDC8AC5A9B0265DC7A846BFBBA197 ();
// 0x000006C8 System.Void UIInput::set_selected(System.Boolean)
extern void UIInput_set_selected_mA59C56E0AAE479AF5559945E449AF8623EDD9CD9 ();
// 0x000006C9 System.Boolean UIInput::get_isSelected()
extern void UIInput_get_isSelected_m65688CF6B2FB314F5EB9683AD4E751148B3E7E3E ();
// 0x000006CA System.Void UIInput::set_isSelected(System.Boolean)
extern void UIInput_set_isSelected_m6B7CA7FB02CB028754E998EF6938155874629658 ();
// 0x000006CB System.Int32 UIInput::get_cursorPosition()
extern void UIInput_get_cursorPosition_mEFB1BC5B0AFAA18ED2064A25065CC8BCC7274977 ();
// 0x000006CC System.Void UIInput::set_cursorPosition(System.Int32)
extern void UIInput_set_cursorPosition_mB11DFB23912B68B76397A54A1614A6199E24515A ();
// 0x000006CD System.Int32 UIInput::get_selectionStart()
extern void UIInput_get_selectionStart_mFA8DBC990E80A7B4362CCA75B526B1A351FFCE92 ();
// 0x000006CE System.Void UIInput::set_selectionStart(System.Int32)
extern void UIInput_set_selectionStart_mC0CDAE4ECDE7AEE00291525E4DA32EC451B5275F ();
// 0x000006CF System.Int32 UIInput::get_selectionEnd()
extern void UIInput_get_selectionEnd_m5BEFA5A7F3E9E77959A58F6BD91F34237BBEAC58 ();
// 0x000006D0 System.Void UIInput::set_selectionEnd(System.Int32)
extern void UIInput_set_selectionEnd_m9A01C1EB5CC1370635D1431AFF4A6329DF1DB607 ();
// 0x000006D1 UITexture UIInput::get_caret()
extern void UIInput_get_caret_m3FF87B4360152100654340DFD779C7BF38999098 ();
// 0x000006D2 System.String UIInput::Validate(System.String)
extern void UIInput_Validate_m1D4347A872FE65DC9C30AA343E2838A39E90C9BD ();
// 0x000006D3 System.Void UIInput::Start()
extern void UIInput_Start_m3A3189CB3B77EE464A87B58FFEC96CF5BF877695 ();
// 0x000006D4 System.Void UIInput::Init()
extern void UIInput_Init_m1DFF06A31927382F25BF529B72B972FCE566FAE8 ();
// 0x000006D5 System.Void UIInput::SaveToPlayerPrefs(System.String)
extern void UIInput_SaveToPlayerPrefs_m01151DE065D19ECE5EDA77F2436A6036A35C2B93 ();
// 0x000006D6 System.Void UIInput::OnSelect(System.Boolean)
extern void UIInput_OnSelect_m7C57C9C3D6503E5BC77D005A85D91D4A371A1A6E ();
// 0x000006D7 System.Void UIInput::OnSelectEvent()
extern void UIInput_OnSelectEvent_m64384473BC16227D1CFD95643E62D2B394EB1DCE ();
// 0x000006D8 System.Void UIInput::OnDeselectEvent()
extern void UIInput_OnDeselectEvent_mB30D2D6200FEB86023134E569687002D8F464618 ();
// 0x000006D9 System.Void UIInput::Update()
extern void UIInput_Update_m104AA1C2C1312057BFD5658926E9838826BFA505 ();
// 0x000006DA System.Void UIInput::OnKey(UnityEngine.KeyCode)
extern void UIInput_OnKey_m281CCB262AFB8A1927D25B2D903CB690A6B01CED ();
// 0x000006DB System.Void UIInput::DoBackspace()
extern void UIInput_DoBackspace_m0FD7585FDDA2E1E125E476D25F93247B9DF63D78 ();
// 0x000006DC System.Void UIInput::Insert(System.String)
extern void UIInput_Insert_mEC3E4E9AFA08E098B313B966D878365C6E6CEA42 ();
// 0x000006DD System.String UIInput::GetLeftText()
extern void UIInput_GetLeftText_m2A8CA69FC45D63E7792E5C869EAC4370C7A926F7 ();
// 0x000006DE System.String UIInput::GetRightText()
extern void UIInput_GetRightText_m68C9EC25592809A9DF9B18A90692BB6EDCF8ADF8 ();
// 0x000006DF System.String UIInput::GetSelection()
extern void UIInput_GetSelection_m52082544051670074501EB3530743C9EF76B3B58 ();
// 0x000006E0 System.Int32 UIInput::GetCharUnderMouse()
extern void UIInput_GetCharUnderMouse_mA1CEA29FE6B148C00D4BC639B979C007B6F45BD8 ();
// 0x000006E1 System.Void UIInput::OnPress(System.Boolean)
extern void UIInput_OnPress_m629B8D7D32D568CA83A6F41D6C0BEEDCE4A8353D ();
// 0x000006E2 System.Void UIInput::OnDrag(UnityEngine.Vector2)
extern void UIInput_OnDrag_m3A8192AE8CE107A68B7F2AECB706AF63254B0F2B ();
// 0x000006E3 System.Void UIInput::OnDisable()
extern void UIInput_OnDisable_m18581657A834C4B1222633DFBCB74A08D361EB4C ();
// 0x000006E4 System.Void UIInput::Cleanup()
extern void UIInput_Cleanup_mFA5EC55D1AB273CA320970EF6971439E88FD94C7 ();
// 0x000006E5 System.Void UIInput::Submit()
extern void UIInput_Submit_mA1C05CA28B7987013C701326ABFB334619659DD5 ();
// 0x000006E6 System.Void UIInput::UpdateLabel()
extern void UIInput_UpdateLabel_mB11D23F3CD774EFB2A074D5AE1051548BCB77C4F ();
// 0x000006E7 System.Char UIInput::Validate(System.String,System.Int32,System.Char)
extern void UIInput_Validate_m6C7AEADDCEBDFB34DDBDF0064E88829F376968C5 ();
// 0x000006E8 System.Void UIInput::ExecuteOnChange()
extern void UIInput_ExecuteOnChange_m33FD9EECE3D7273FA3369DF986D48C91BF65A585 ();
// 0x000006E9 System.Void UIInput::RemoveFocus()
extern void UIInput_RemoveFocus_m1F30C0FAB4C00CB90ADD0022CEB608DA7E44AF20 ();
// 0x000006EA System.Void UIInput::SaveValue()
extern void UIInput_SaveValue_m554783D1C941704F5B1F116488FF824026385BCD ();
// 0x000006EB System.Void UIInput::LoadValue()
extern void UIInput_LoadValue_mCE1A321E6FD24681D47682121E5986AACC14AD73 ();
// 0x000006EC System.Void UIInput::.ctor()
extern void UIInput__ctor_m3CDC658FEB019232D87CBBE33B1C149217B193D8 ();
// 0x000006ED System.Void UIInput::.cctor()
extern void UIInput__cctor_mF543F47D7E1AC8A886FC24A11093CBA7E962D6DA ();
// 0x000006EE System.Void UIInputOnGUI::.ctor()
extern void UIInputOnGUI__ctor_mCFE0D3FB40D24D4F331B5B8169E73FBF2983D1AB ();
// 0x000006EF System.Int32 UILabel::get_finalFontSize()
extern void UILabel_get_finalFontSize_mAF390ABBDDE8BB18741F9AF14335AA2A80A502E9 ();
// 0x000006F0 System.Boolean UILabel::get_shouldBeProcessed()
extern void UILabel_get_shouldBeProcessed_m33C1F4726DDBB8BC43D470478878AEC6030AA7B2 ();
// 0x000006F1 System.Void UILabel::set_shouldBeProcessed(System.Boolean)
extern void UILabel_set_shouldBeProcessed_mF848DAE2C439A028441FF0A81305B2128C4F21C0 ();
// 0x000006F2 System.Boolean UILabel::get_isAnchoredHorizontally()
extern void UILabel_get_isAnchoredHorizontally_m0D122A26474BBEA20960ACB929B0E4A92907DCBC ();
// 0x000006F3 System.Boolean UILabel::get_isAnchoredVertically()
extern void UILabel_get_isAnchoredVertically_m79815EE93669C2DFFC451EA2A8CE050B7EFD56FD ();
// 0x000006F4 UnityEngine.Material UILabel::get_material()
extern void UILabel_get_material_mF2A0909977FA3E11070DC5A44083A445C0DF9378 ();
// 0x000006F5 System.Void UILabel::set_material(UnityEngine.Material)
extern void UILabel_set_material_m93F8E638A30CD42581F70B0EACD4E584145431EC ();
// 0x000006F6 UnityEngine.Texture UILabel::get_mainTexture()
extern void UILabel_get_mainTexture_mB1BAADF9F8E5538BCE8FA346519512D2C7FD11FC ();
// 0x000006F7 System.Void UILabel::set_mainTexture(UnityEngine.Texture)
extern void UILabel_set_mainTexture_m915AB05BE02CB8742D3CCD4E8FA3CF4DB7021B31 ();
// 0x000006F8 UnityEngine.Object UILabel::get_font()
extern void UILabel_get_font_m8C1C7EBE46424B46AE0576E133DFF27E37AD0791 ();
// 0x000006F9 System.Void UILabel::set_font(UnityEngine.Object)
extern void UILabel_set_font_mA8BF3F820A14C36DD4F37F876514079675EC6226 ();
// 0x000006FA INGUIFont UILabel::get_bitmapFont()
extern void UILabel_get_bitmapFont_mA29FACDD7C70E457B7BE3484DDBD2569387D69F7 ();
// 0x000006FB System.Void UILabel::set_bitmapFont(INGUIFont)
extern void UILabel_set_bitmapFont_mCD3850E87E521E5F50B0E0F81807200661415A2F ();
// 0x000006FC INGUIAtlas UILabel::get_atlas()
extern void UILabel_get_atlas_mD37F618C7D05CE1806640D50323F86DE2E206E5E ();
// 0x000006FD System.Void UILabel::set_atlas(INGUIAtlas)
extern void UILabel_set_atlas_m306A4AF6EF74D768200B3363EC7E477365D278F8 ();
// 0x000006FE UnityEngine.Font UILabel::get_trueTypeFont()
extern void UILabel_get_trueTypeFont_m892287B19700E96C4556CFB992B072D28FF2E12E ();
// 0x000006FF System.Void UILabel::set_trueTypeFont(UnityEngine.Font)
extern void UILabel_set_trueTypeFont_mE22949BACA92A29B3A4070FD892F27842FEDEBFC ();
// 0x00000700 UnityEngine.Object UILabel::get_ambigiousFont()
extern void UILabel_get_ambigiousFont_m9F2ABC34155E9B33D7D5F09633127D6B683A9B1E ();
// 0x00000701 System.Void UILabel::set_ambigiousFont(UnityEngine.Object)
extern void UILabel_set_ambigiousFont_mFC4CFF650E6384FCB2F0099F4368DC07C50ED97A ();
// 0x00000702 System.String UILabel::get_text()
extern void UILabel_get_text_m0EB04C8F82985701EF0FBB2F86B9063C687E5ED7 ();
// 0x00000703 System.Void UILabel::set_text(System.String)
extern void UILabel_set_text_m4B5BE8EAD657BEEA4CC32A0272AECA3ACB8C35CE ();
// 0x00000704 System.Int32 UILabel::get_defaultFontSize()
extern void UILabel_get_defaultFontSize_m52C50D86BE44627A7D2E49A44946D99C810AC9FF ();
// 0x00000705 System.Int32 UILabel::get_fontSize()
extern void UILabel_get_fontSize_mAFE1540C0E7FF7CA1B3F5824B723352B94C065C6 ();
// 0x00000706 System.Void UILabel::set_fontSize(System.Int32)
extern void UILabel_set_fontSize_m1FDCFC9AB8B93443B49C5B2F13A5DDDDCCD4EE8A ();
// 0x00000707 UnityEngine.FontStyle UILabel::get_fontStyle()
extern void UILabel_get_fontStyle_m0B943B3852820F157ED474E153E8603C79EB845E ();
// 0x00000708 System.Void UILabel::set_fontStyle(UnityEngine.FontStyle)
extern void UILabel_set_fontStyle_mBB60ACF5E137DE2792DDBF728669F8021AAB4E36 ();
// 0x00000709 NGUIText_Alignment UILabel::get_alignment()
extern void UILabel_get_alignment_m7770D32D5AB8EE1DB49F4D9B32C3A38BEB699C78 ();
// 0x0000070A System.Void UILabel::set_alignment(NGUIText_Alignment)
extern void UILabel_set_alignment_m97721ECA2AE471EEFCBD9025BE767F730AADC6EB ();
// 0x0000070B System.Boolean UILabel::get_applyGradient()
extern void UILabel_get_applyGradient_m3F01DF13EA8F80E50D994606E4F2C34BDC5F25D2 ();
// 0x0000070C System.Void UILabel::set_applyGradient(System.Boolean)
extern void UILabel_set_applyGradient_m0DFA68C382126B67FF91D9BDEA108D4C3C823DD8 ();
// 0x0000070D UnityEngine.Color UILabel::get_gradientTop()
extern void UILabel_get_gradientTop_mC15B476DA2E352D32BFE380396D376303A0F144F ();
// 0x0000070E System.Void UILabel::set_gradientTop(UnityEngine.Color)
extern void UILabel_set_gradientTop_m815A5CEB2B89B0E4CA23713573D442A5C28C3903 ();
// 0x0000070F UnityEngine.Color UILabel::get_gradientBottom()
extern void UILabel_get_gradientBottom_mC0A2E338763967C5D3A9DA18AAB44C6509783C45 ();
// 0x00000710 System.Void UILabel::set_gradientBottom(UnityEngine.Color)
extern void UILabel_set_gradientBottom_mC7B0A53A69F93210FB6E355D482818864B0F3C0C ();
// 0x00000711 System.Int32 UILabel::get_spacingX()
extern void UILabel_get_spacingX_m609D8B4986FA798B1ACABCE7BC352D4ED81B3FBF ();
// 0x00000712 System.Void UILabel::set_spacingX(System.Int32)
extern void UILabel_set_spacingX_mC1CBF8632EAB40D17815EBA40C7F5FA501BBE5CC ();
// 0x00000713 System.Int32 UILabel::get_spacingY()
extern void UILabel_get_spacingY_mF57CBFA7E9F0CB0C6C1212E35268A01B50DBEE38 ();
// 0x00000714 System.Void UILabel::set_spacingY(System.Int32)
extern void UILabel_set_spacingY_m10C8032A82E11F87BECC258BC851E6A7D4849806 ();
// 0x00000715 System.Boolean UILabel::get_useFloatSpacing()
extern void UILabel_get_useFloatSpacing_m2243942162004C708515D07AC83AADA8A18478F4 ();
// 0x00000716 System.Void UILabel::set_useFloatSpacing(System.Boolean)
extern void UILabel_set_useFloatSpacing_mB3E81185115B4096CE597AE7B08BD169D0C1C2B5 ();
// 0x00000717 System.Single UILabel::get_floatSpacingX()
extern void UILabel_get_floatSpacingX_mDBDE3F3B8BC329BDE893F3BC0D773DD582047D0A ();
// 0x00000718 System.Void UILabel::set_floatSpacingX(System.Single)
extern void UILabel_set_floatSpacingX_mD51501C33AAD498146B8B9C93D50E9A1B850B616 ();
// 0x00000719 System.Single UILabel::get_floatSpacingY()
extern void UILabel_get_floatSpacingY_m3CC8271888CC0AE018B5B88438FA89E81EFD20CD ();
// 0x0000071A System.Void UILabel::set_floatSpacingY(System.Single)
extern void UILabel_set_floatSpacingY_mF5F1637FCC5DC038BFBC9E856E48C014F10AF072 ();
// 0x0000071B System.Single UILabel::get_effectiveSpacingY()
extern void UILabel_get_effectiveSpacingY_m7DADF35B3837C245CAD56AB2AD5D622550A5E941 ();
// 0x0000071C System.Single UILabel::get_effectiveSpacingX()
extern void UILabel_get_effectiveSpacingX_m474A2765705892FACE4834E0A630BFA1BFE4005D ();
// 0x0000071D System.Boolean UILabel::get_overflowEllipsis()
extern void UILabel_get_overflowEllipsis_m7FE9D42C8BA7087E6D0385342E2CD2DC1D73976B ();
// 0x0000071E System.Void UILabel::set_overflowEllipsis(System.Boolean)
extern void UILabel_set_overflowEllipsis_mF0BBAD32DB161BCC4A0203B8225D5E8B7547ABC5 ();
// 0x0000071F System.Int32 UILabel::get_overflowWidth()
extern void UILabel_get_overflowWidth_mC8333C28DEAD3371320DD42B797AC30A1A91FE02 ();
// 0x00000720 System.Void UILabel::set_overflowWidth(System.Int32)
extern void UILabel_set_overflowWidth_m423D40C345B0AEA3835ABFBD39E08E0024F154E1 ();
// 0x00000721 System.Int32 UILabel::get_overflowHeight()
extern void UILabel_get_overflowHeight_m2068162AAC1EBED03473F06230734374FDBD2FC1 ();
// 0x00000722 System.Void UILabel::set_overflowHeight(System.Int32)
extern void UILabel_set_overflowHeight_m4B42C34948AC594F5250EB39A68E5EE33254976C ();
// 0x00000723 System.Boolean UILabel::get_keepCrisp()
extern void UILabel_get_keepCrisp_m4D81A25254F5CCC6404CE7F60263E76F48907DFA ();
// 0x00000724 System.Boolean UILabel::get_supportEncoding()
extern void UILabel_get_supportEncoding_mC69D57B55567FAEA0C72659882C197AE88216227 ();
// 0x00000725 System.Void UILabel::set_supportEncoding(System.Boolean)
extern void UILabel_set_supportEncoding_mCE8A5BE4F0E6349D2DEF47A4B432B77EAE889C7C ();
// 0x00000726 NGUIText_SymbolStyle UILabel::get_symbolStyle()
extern void UILabel_get_symbolStyle_m56E0FAEC4981028324306A9889DB8B58424BED55 ();
// 0x00000727 System.Void UILabel::set_symbolStyle(NGUIText_SymbolStyle)
extern void UILabel_set_symbolStyle_mCF23BA041D2FAFF79E9C2CC1B2D607B49863E3D8 ();
// 0x00000728 UILabel_Overflow UILabel::get_overflowMethod()
extern void UILabel_get_overflowMethod_m1E54E5A775CDFE66ECE147CA27119FCF962744AA ();
// 0x00000729 System.Void UILabel::set_overflowMethod(UILabel_Overflow)
extern void UILabel_set_overflowMethod_m76DEBA8BADD21ECFA0E1AC6BC83B28FD32806B5F ();
// 0x0000072A System.Int32 UILabel::get_lineWidth()
extern void UILabel_get_lineWidth_m7BCFDD110ED48402D67E802212C1E57E93F97454 ();
// 0x0000072B System.Void UILabel::set_lineWidth(System.Int32)
extern void UILabel_set_lineWidth_mF676B776531B9342C7D84411060AE9698DE47525 ();
// 0x0000072C System.Int32 UILabel::get_lineHeight()
extern void UILabel_get_lineHeight_mD10D5DF298FFB886CAABBF70FE218EB81E291F98 ();
// 0x0000072D System.Void UILabel::set_lineHeight(System.Int32)
extern void UILabel_set_lineHeight_m9CEBA80B805D30F7DB5155C26421EC0DE3B47E9A ();
// 0x0000072E System.Boolean UILabel::get_multiLine()
extern void UILabel_get_multiLine_m68209F29D6A94F7D612DACA1EC26BDACBD454E66 ();
// 0x0000072F System.Void UILabel::set_multiLine(System.Boolean)
extern void UILabel_set_multiLine_mDFDBBF8F9E4F5D6DD41587C3A002B12650D63FDF ();
// 0x00000730 UnityEngine.Vector3[] UILabel::get_localCorners()
extern void UILabel_get_localCorners_mE8FF29B0289DE24A3431C493C4643DD0AE0D3FE9 ();
// 0x00000731 UnityEngine.Vector3[] UILabel::get_worldCorners()
extern void UILabel_get_worldCorners_mE6DD227FEF24C44B64C2FC696E33D557FB0ED31C ();
// 0x00000732 UnityEngine.Vector4 UILabel::get_drawingDimensions()
extern void UILabel_get_drawingDimensions_mB1B1B20B64B1C4E5B54381AADA80D08E6261A56D ();
// 0x00000733 System.Int32 UILabel::get_maxLineCount()
extern void UILabel_get_maxLineCount_m0442B08F374BC98D9A5421F95B561A0EE33B491A ();
// 0x00000734 System.Void UILabel::set_maxLineCount(System.Int32)
extern void UILabel_set_maxLineCount_m3EA762A5FC20417DCA8DFC158CF9CC8C6EFB211C ();
// 0x00000735 UILabel_Effect UILabel::get_effectStyle()
extern void UILabel_get_effectStyle_m05B562546779D566345AA6F5CCA50936CC950C21 ();
// 0x00000736 System.Void UILabel::set_effectStyle(UILabel_Effect)
extern void UILabel_set_effectStyle_m23DD614F34C1A279974639D448AD6B4FCC85562E ();
// 0x00000737 UnityEngine.Color UILabel::get_effectColor()
extern void UILabel_get_effectColor_m9F3837FF21E48B0F6C0B4E9BCC29C4B2BD0555BF ();
// 0x00000738 System.Void UILabel::set_effectColor(UnityEngine.Color)
extern void UILabel_set_effectColor_m981FF1AB2ED11C1963791E98C4FD47FD80A2C7D3 ();
// 0x00000739 UnityEngine.Vector2 UILabel::get_effectDistance()
extern void UILabel_get_effectDistance_mDD6FA8522835D3FD3DDE79E9C5A61842FE8541AB ();
// 0x0000073A System.Void UILabel::set_effectDistance(UnityEngine.Vector2)
extern void UILabel_set_effectDistance_mE3C1259F88612CE8D5E0164DA567E029D26A374D ();
// 0x0000073B System.Int32 UILabel::get_quadsPerCharacter()
extern void UILabel_get_quadsPerCharacter_m458402E82E7957A321AEE0E86E0845B150311F5F ();
// 0x0000073C System.Boolean UILabel::get_shrinkToFit()
extern void UILabel_get_shrinkToFit_mDE4EE110C1E0E25538F660A4F4C9E5C72C2A60B3 ();
// 0x0000073D System.Void UILabel::set_shrinkToFit(System.Boolean)
extern void UILabel_set_shrinkToFit_m6DA253D0F72B4B2E5745E21DA0C274AE11566387 ();
// 0x0000073E System.String UILabel::get_processedText()
extern void UILabel_get_processedText_mFDCDD3B1998962C592DD3FA6CEBE5E739F6CCECD ();
// 0x0000073F UnityEngine.Vector2 UILabel::get_printedSize()
extern void UILabel_get_printedSize_m53D88DF4EC0DBEC9F9DB364D0973C037DDD6C980 ();
// 0x00000740 UnityEngine.Vector2 UILabel::get_localSize()
extern void UILabel_get_localSize_mC499FA67CADDD2394C880BB0A63F15CD7F52CD0C ();
// 0x00000741 System.Boolean UILabel::get_isValid()
extern void UILabel_get_isValid_mADF18ED9792FEE19DADAFDCB5C312630A527D3A3 ();
// 0x00000742 UILabel_Modifier UILabel::get_modifier()
extern void UILabel_get_modifier_mE073C4C51C8D28B50C70CA0C7CD25C25DD6C30AC ();
// 0x00000743 System.Void UILabel::set_modifier(UILabel_Modifier)
extern void UILabel_set_modifier_m31232967F05F7EEC6955C5DA8E15D63AA25B71FB ();
// 0x00000744 System.Void UILabel::OnInit()
extern void UILabel_OnInit_m10702A8FBD0AD3CC40D70CFB4325CFAD0677ADDB ();
// 0x00000745 System.Void UILabel::OnDisable()
extern void UILabel_OnDisable_m9080321EF3A6753C2E6E0D1E0D35E4ED400074A4 ();
// 0x00000746 System.Void UILabel::SetActiveFont(UnityEngine.Font)
extern void UILabel_SetActiveFont_m48B2C2CB902EBF9A41FB0AB08D7EE1499B1823C5 ();
// 0x00000747 System.String UILabel::get_printedText()
extern void UILabel_get_printedText_m1686A6ADD71E8B6AB98C6F1F31CCE14B1A43E34D ();
// 0x00000748 System.Void UILabel::OnFontChanged(UnityEngine.Font)
extern void UILabel_OnFontChanged_mDDCC30B359E77AE600BF4CACD7395FFE4C439EE8 ();
// 0x00000749 UnityEngine.Vector3[] UILabel::GetSides(UnityEngine.Transform)
extern void UILabel_GetSides_m8000A6BD42C770208288B9CB3EC0266476764672 ();
// 0x0000074A System.Void UILabel::UpgradeFrom265()
extern void UILabel_UpgradeFrom265_mDFD3CC8010B20A35A02813CA1F25ED4707D7121F ();
// 0x0000074B System.Void UILabel::OnAnchor()
extern void UILabel_OnAnchor_m63724935E7257F159C1BEFE875B33EA5AB220445 ();
// 0x0000074C System.Void UILabel::ProcessAndRequest()
extern void UILabel_ProcessAndRequest_m3B789AE5AC7D1FF051AFD9DCC64670B6C7D8EDB5 ();
// 0x0000074D System.Void UILabel::OnEnable()
extern void UILabel_OnEnable_m0E0A34FC734BED1B7A41C5FADA3D8921CC34E4A0 ();
// 0x0000074E System.Void UILabel::OnStart()
extern void UILabel_OnStart_m441D9E57A34B524DB87B3BC877CD570E33581B2E ();
// 0x0000074F System.Void UILabel::MarkAsChanged()
extern void UILabel_MarkAsChanged_m5B64C88BADA2BEC46E7EE07CCE9B58EB23BFA125 ();
// 0x00000750 System.Void UILabel::ProcessText(System.Boolean,System.Boolean)
extern void UILabel_ProcessText_m82330948205E17A92811E9383C8EBC1391952F78 ();
// 0x00000751 System.Void UILabel::MakePixelPerfect()
extern void UILabel_MakePixelPerfect_m82A15F103B04464A2E76F4A9C0480DBE1329F4BD ();
// 0x00000752 System.Void UILabel::AssumeNaturalSize()
extern void UILabel_AssumeNaturalSize_mF1324FCD4B32348F66817766956FABD247BB4A2C ();
// 0x00000753 System.Int32 UILabel::GetCharacterIndex(UnityEngine.Vector3)
extern void UILabel_GetCharacterIndex_mE7125B8911407156D42CF41544C66903727C2094 ();
// 0x00000754 System.Int32 UILabel::GetCharacterIndex(UnityEngine.Vector2)
extern void UILabel_GetCharacterIndex_m2865253F6EE6016BD1C784BFC1A73F13CD92849E ();
// 0x00000755 System.Int32 UILabel::GetCharacterIndexAtPosition(UnityEngine.Vector3,System.Boolean)
extern void UILabel_GetCharacterIndexAtPosition_m32E03E672CB0C233CB3D0FFDCB9917259E347D7C ();
// 0x00000756 System.Int32 UILabel::GetCharacterIndexAtPosition(UnityEngine.Vector2,System.Boolean)
extern void UILabel_GetCharacterIndexAtPosition_mCA1DC0D44FF43CC8E9A594E089AADBC381428E9F ();
// 0x00000757 System.String UILabel::GetWordAtPosition(UnityEngine.Vector3)
extern void UILabel_GetWordAtPosition_m4CEC10A264CAAD906BDE6E583B65E4AE799499ED ();
// 0x00000758 System.String UILabel::GetWordAtPosition(UnityEngine.Vector2)
extern void UILabel_GetWordAtPosition_m926FDEAFB751D695F2E67CD1716868705D38CED7 ();
// 0x00000759 System.String UILabel::GetWordAtCharacterIndex(System.Int32)
extern void UILabel_GetWordAtCharacterIndex_m5600856D0F5E779F8F500B6132DF9A97D408D5D4 ();
// 0x0000075A System.String UILabel::GetUrlAtPosition(UnityEngine.Vector3)
extern void UILabel_GetUrlAtPosition_mAFE9227F267EDE243F5232A9277ADFEC327389D8 ();
// 0x0000075B System.String UILabel::GetUrlAtPosition(UnityEngine.Vector2)
extern void UILabel_GetUrlAtPosition_m7BB1DED8A33347D1664934BAA6B4757C392047D2 ();
// 0x0000075C System.String UILabel::GetUrlAtCharacterIndex(System.Int32)
extern void UILabel_GetUrlAtCharacterIndex_m8539B8DFAEA172C1A803CA32FC3FDF927FB5E6EC ();
// 0x0000075D System.Int32 UILabel::GetCharacterIndex(System.Int32,UnityEngine.KeyCode)
extern void UILabel_GetCharacterIndex_m5FF236BB03945FC9B37177D7745B76B056DDF8ED ();
// 0x0000075E System.Void UILabel::PrintOverlay(System.Int32,System.Int32,UIGeometry,UIGeometry,UnityEngine.Color,UnityEngine.Color)
extern void UILabel_PrintOverlay_m836BCD2E57F9732957961FAAE0E1638C3068526A ();
// 0x0000075F System.Boolean UILabel::get_premultipliedAlphaShader()
extern void UILabel_get_premultipliedAlphaShader_m5590D5A7ECC42ABE7F994E160779D3286F3C9D55 ();
// 0x00000760 System.Boolean UILabel::get_packedFontShader()
extern void UILabel_get_packedFontShader_mFC17F1718AECD33B807321E866DE6DF7DA3E283A ();
// 0x00000761 System.Void UILabel::OnFill(System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Color>)
extern void UILabel_OnFill_m23EC55001D8447A7BEE6C39929AC289F9D42FAF3 ();
// 0x00000762 UnityEngine.Vector2 UILabel::ApplyOffset(System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Int32)
extern void UILabel_ApplyOffset_mD5795B9F6279BA5CFE2C3D8E172561F098511F1F ();
// 0x00000763 System.Void UILabel::ApplyShadow(System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Color>,System.Int32,System.Int32,System.Single,System.Single)
extern void UILabel_ApplyShadow_mD4549F661AC02336ADD0D9168486D2CC318330FA ();
// 0x00000764 System.Int32 UILabel::CalculateOffsetToFit(System.String)
extern void UILabel_CalculateOffsetToFit_mD5672BBEF542E0B468D34753606603D27B85A4CF ();
// 0x00000765 System.Void UILabel::SetCurrentProgress()
extern void UILabel_SetCurrentProgress_m0290C82DE70EF6F8740001BA8F7C04838244EE56 ();
// 0x00000766 System.Void UILabel::SetCurrentPercent()
extern void UILabel_SetCurrentPercent_m200E3B12EB531978AA3804B07F779128C56F9018 ();
// 0x00000767 System.Void UILabel::SetCurrentSelection()
extern void UILabel_SetCurrentSelection_m7744138C63FF8E90AFFC02CC096A87EE41E5337B ();
// 0x00000768 System.Boolean UILabel::Wrap(System.String,System.String&)
extern void UILabel_Wrap_m2B4F318D2634E38FB37BF54B979B22DFAECCA622 ();
// 0x00000769 System.Boolean UILabel::Wrap(System.String,System.String&,System.Int32)
extern void UILabel_Wrap_m69EABF139770D4897AE427794E439DE1E28B7796 ();
// 0x0000076A System.Void UILabel::UpdateNGUIText()
extern void UILabel_UpdateNGUIText_mA40E569AA3EDF6839F53A943ADA8E3F3F1E1E2CC ();
// 0x0000076B System.Void UILabel::OnApplicationPause(System.Boolean)
extern void UILabel_OnApplicationPause_mF629177B1B9749A9F23944A040A562CDA24556DB ();
// 0x0000076C System.Void UILabel::.ctor()
extern void UILabel__ctor_m0FD77A6187149DCF9D74E727D91E04A2B7435091 ();
// 0x0000076D System.Void UILabel::.cctor()
extern void UILabel__cctor_mBB14C2E315EDE3FE9A56E10702AB513E88DE125F ();
// 0x0000076E System.Void UILocalize::set_value(System.String)
extern void UILocalize_set_value_mCF4380A57C322974EB8E0D034778014600748863 ();
// 0x0000076F System.Void UILocalize::OnEnable()
extern void UILocalize_OnEnable_m7EFE7B9DDCC0098830C2C2CE23ACF30AD2BB6249 ();
// 0x00000770 System.Void UILocalize::Start()
extern void UILocalize_Start_m1E49B9AB04E898FA798ADFE9315D03CD5ADCB9DD ();
// 0x00000771 System.Void UILocalize::OnLocalize()
extern void UILocalize_OnLocalize_m8101CF98F513D77BD48546ED246036D182C5F740 ();
// 0x00000772 System.Void UILocalize::.ctor()
extern void UILocalize__ctor_mFA32627FA59A7E2FDED902CFF2EDD4A430697A3A ();
// 0x00000773 System.Void UIOrthoCamera::Start()
extern void UIOrthoCamera_Start_m70DDFBD29B19561B0C8413843A7E5A8808013CA6 ();
// 0x00000774 System.Void UIOrthoCamera::Update()
extern void UIOrthoCamera_Update_mE09BB1BE105D76633BD46FE7D6557E55C873CFB6 ();
// 0x00000775 System.Void UIOrthoCamera::.ctor()
extern void UIOrthoCamera__ctor_mAF564418B2FD201C4D83317CFEDAE3A31F3D08F4 ();
// 0x00000776 System.String UIPanel::get_sortingLayerName()
extern void UIPanel_get_sortingLayerName_m8B0E3515CC9E31C35250D3FC256ADE8DD036F998 ();
// 0x00000777 System.Void UIPanel::set_sortingLayerName(System.String)
extern void UIPanel_set_sortingLayerName_mA3D08DEDBCF5F89AD147123DD1BB0FAB49367619 ();
// 0x00000778 System.Int32 UIPanel::get_nextUnusedDepth()
extern void UIPanel_get_nextUnusedDepth_m4228488B7CF6C6ABA52878FB2EA28F0E8096ACA2 ();
// 0x00000779 System.Boolean UIPanel::get_canBeAnchored()
extern void UIPanel_get_canBeAnchored_m553923A05FF0606586FA5F2975208660FCD43BC6 ();
// 0x0000077A System.Single UIPanel::get_alpha()
extern void UIPanel_get_alpha_mFA80442F426A556D87A9784CAA47E16FDCD2C5E2 ();
// 0x0000077B System.Void UIPanel::set_alpha(System.Single)
extern void UIPanel_set_alpha_m984DFAEB0F4BB7C23CB03397928B8A272718732A ();
// 0x0000077C System.Int32 UIPanel::get_depth()
extern void UIPanel_get_depth_m88BC62787C7185453A1C286561040BE1C73E3484 ();
// 0x0000077D System.Void UIPanel::set_depth(System.Int32)
extern void UIPanel_set_depth_m79CAD98E1D9EB0C866884DC135175DEF5719FBFC ();
// 0x0000077E System.Int32 UIPanel::get_sortingOrder()
extern void UIPanel_get_sortingOrder_mF60C35883A649F39CF0C6BA4AEF0053F2786202D ();
// 0x0000077F System.Void UIPanel::set_sortingOrder(System.Int32)
extern void UIPanel_set_sortingOrder_m96AF7D91D0A41287F190279F1AD4A237C81ABD74 ();
// 0x00000780 System.Int32 UIPanel::CompareFunc(UIPanel,UIPanel)
extern void UIPanel_CompareFunc_m3C08EC37535CDB9CBF2AB38846E60E3A538A2637 ();
// 0x00000781 System.Single UIPanel::get_width()
extern void UIPanel_get_width_m2C4C0BCB71C09CB23A73211AC0752F7DAEA972E8 ();
// 0x00000782 System.Single UIPanel::get_height()
extern void UIPanel_get_height_m84B34BF13710F4356A8EF4DBACC176D9ABC7935F ();
// 0x00000783 System.Boolean UIPanel::get_halfPixelOffset()
extern void UIPanel_get_halfPixelOffset_mF37839F26FE6AB9075A225579EAC9EDB4C8551F7 ();
// 0x00000784 System.Boolean UIPanel::get_usedForUI()
extern void UIPanel_get_usedForUI_mF26D72FF5EDB90B83351FB46F6A6C4DC5F766B2B ();
// 0x00000785 UnityEngine.Vector3 UIPanel::get_drawCallOffset()
extern void UIPanel_get_drawCallOffset_m498BA3A16182DC147E3854F951E05CEFF539DD4A ();
// 0x00000786 UIDrawCall_Clipping UIPanel::get_clipping()
extern void UIPanel_get_clipping_mC43FFA658D783CC5DE9D670489685E2036F1AEE3 ();
// 0x00000787 System.Void UIPanel::set_clipping(UIDrawCall_Clipping)
extern void UIPanel_set_clipping_mBA3ED0F32C491F5848D5415E4B9260D7150B1CB9 ();
// 0x00000788 UIPanel UIPanel::get_parentPanel()
extern void UIPanel_get_parentPanel_m4283512358000B37774FFC21C25D3EEE88DD3FB8 ();
// 0x00000789 System.Int32 UIPanel::get_clipCount()
extern void UIPanel_get_clipCount_m482891B77676C1B48882E345E66C640DCE9798BF ();
// 0x0000078A System.Boolean UIPanel::get_hasClipping()
extern void UIPanel_get_hasClipping_m4FDD3EA5877D6F03761EA68800C7E7E8A024F18E ();
// 0x0000078B System.Boolean UIPanel::get_hasCumulativeClipping()
extern void UIPanel_get_hasCumulativeClipping_m25F0B03722CCBB12D56CEBCE3F8048D09C84CC8E ();
// 0x0000078C System.Boolean UIPanel::get_clipsChildren()
extern void UIPanel_get_clipsChildren_m8F8217EE112B3EFA28A76080A527C8CB8A101021 ();
// 0x0000078D UnityEngine.Vector2 UIPanel::get_clipOffset()
extern void UIPanel_get_clipOffset_m4742A2DC94BBCE038453F9DCEA7EC65A9A86C336 ();
// 0x0000078E System.Void UIPanel::set_clipOffset(UnityEngine.Vector2)
extern void UIPanel_set_clipOffset_mBD95718836342A55C6F3F7C512B10F9A2126C04D ();
// 0x0000078F System.Void UIPanel::InvalidateClipping()
extern void UIPanel_InvalidateClipping_m5BBACD88E6E0502ECAABC49652870DCCE3E90DD2 ();
// 0x00000790 UnityEngine.Texture2D UIPanel::get_clipTexture()
extern void UIPanel_get_clipTexture_m495A41C7760FA01113BD52B717506B6A6D59880B ();
// 0x00000791 System.Void UIPanel::set_clipTexture(UnityEngine.Texture2D)
extern void UIPanel_set_clipTexture_mD1C16C42DCF33B9362597D1B12BA6B7211E449FB ();
// 0x00000792 UnityEngine.Vector4 UIPanel::get_clipRange()
extern void UIPanel_get_clipRange_m23D7DDEAD0BB61337073F2BC739BE75431646368 ();
// 0x00000793 System.Void UIPanel::set_clipRange(UnityEngine.Vector4)
extern void UIPanel_set_clipRange_m50A1BC3CCC1FF3E77B7D2BC7F2197FE39CF78717 ();
// 0x00000794 UnityEngine.Vector4 UIPanel::get_baseClipRegion()
extern void UIPanel_get_baseClipRegion_mEAEF9F90FB2DEAD0566C3EE09381AA4D29D4BA41 ();
// 0x00000795 System.Void UIPanel::set_baseClipRegion(UnityEngine.Vector4)
extern void UIPanel_set_baseClipRegion_m5FB0796888890EAEE58EF88C795B366636C41E79 ();
// 0x00000796 UnityEngine.Vector4 UIPanel::get_finalClipRegion()
extern void UIPanel_get_finalClipRegion_m4DBF5E39EFBC20B0209695636A2F3E430E29FA11 ();
// 0x00000797 UnityEngine.Vector2 UIPanel::get_clipSoftness()
extern void UIPanel_get_clipSoftness_m3108AC74F24FD406592CCDC3723C2B460CBF94F9 ();
// 0x00000798 System.Void UIPanel::set_clipSoftness(UnityEngine.Vector2)
extern void UIPanel_set_clipSoftness_mA25DF6A704E4FC98977B6AA8C7F07C15F6858988 ();
// 0x00000799 UnityEngine.Vector3[] UIPanel::get_localCorners()
extern void UIPanel_get_localCorners_mE1EFA3EEA7997B9220B80144307FCF761131623F ();
// 0x0000079A UnityEngine.Vector3[] UIPanel::get_worldCorners()
extern void UIPanel_get_worldCorners_m083821095477F607162A3974BE64B5C242F34F00 ();
// 0x0000079B UnityEngine.Vector3[] UIPanel::GetSides(UnityEngine.Transform)
extern void UIPanel_GetSides_mA1D0BEA4283D746C74E3BC01EF61E9EE4EDEFC8E ();
// 0x0000079C System.Void UIPanel::Invalidate(System.Boolean)
extern void UIPanel_Invalidate_mF105304D3FB743D14403A94F3C5872AC91DDD651 ();
// 0x0000079D System.Single UIPanel::CalculateFinalAlpha(System.Int32)
extern void UIPanel_CalculateFinalAlpha_mC772E36375B8D9BFC2F67498FA4D8B9CBA763441 ();
// 0x0000079E System.Void UIPanel::SetRect(System.Single,System.Single,System.Single,System.Single)
extern void UIPanel_SetRect_m0EB4AE4A724641F3E8D89982875465CF0B1BC07D ();
// 0x0000079F System.Boolean UIPanel::IsVisible(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void UIPanel_IsVisible_mBDD72A90BF87CE5A634815F21D82526CB23C7F5E ();
// 0x000007A0 System.Boolean UIPanel::IsVisible(UnityEngine.Vector3)
extern void UIPanel_IsVisible_m6B9769623337CCB0307FBE83E034580B83C920B8 ();
// 0x000007A1 System.Boolean UIPanel::IsVisible(UIWidget)
extern void UIPanel_IsVisible_mD145F68E37F9214F882E98523F0A65953270C354 ();
// 0x000007A2 System.Boolean UIPanel::Affects(UIWidget)
extern void UIPanel_Affects_m73BA14034B7F33D499BDC38610F79C8856436F63 ();
// 0x000007A3 System.Void UIPanel::RebuildAllDrawCalls()
extern void UIPanel_RebuildAllDrawCalls_m0A792521A131A1216228539581D9738CCB12F0E4 ();
// 0x000007A4 System.Void UIPanel::SetDirty()
extern void UIPanel_SetDirty_m5911794DE0E914B8E8634196644CE227ACC7A296 ();
// 0x000007A5 System.Void UIPanel::Awake()
extern void UIPanel_Awake_m2B5CCDAF5CBB25F396B1E61994F2EE20079AFD3C ();
// 0x000007A6 System.Void UIPanel::FindParent()
extern void UIPanel_FindParent_mC2A07AE1503FA975CC541FEEDECE10FED0D99F7F ();
// 0x000007A7 System.Void UIPanel::ParentHasChanged()
extern void UIPanel_ParentHasChanged_m94569D8424190D11BA9EE0D68019D1DA9626017C ();
// 0x000007A8 System.Void UIPanel::OnStart()
extern void UIPanel_OnStart_m4D72A0AEA70F86409861419382BC60B7322AC0F9 ();
// 0x000007A9 System.Void UIPanel::OnEnable()
extern void UIPanel_OnEnable_m551698B999EF662526E3AE549E30CE9F8981EA21 ();
// 0x000007AA System.Void UIPanel::OnInit()
extern void UIPanel_OnInit_mEC4A5A7BE5BCC782B689D513A52EAEEB8DEE66CA ();
// 0x000007AB System.Void UIPanel::OnDisable()
extern void UIPanel_OnDisable_m7D5F1A3952F2F11D27D4415846697BC77B727433 ();
// 0x000007AC System.Void UIPanel::UpdateTransformMatrix()
extern void UIPanel_UpdateTransformMatrix_m44F5D5A9F7F688E2398D7701141CB6BB40DD86D2 ();
// 0x000007AD System.Void UIPanel::OnAnchor()
extern void UIPanel_OnAnchor_mA68366862152A7D98563B3490622D87CB89A66CF ();
// 0x000007AE System.Void UIPanel::LateUpdate()
extern void UIPanel_LateUpdate_m4CB3805CC27F78BCBD646E1055753E0D5A78305B ();
// 0x000007AF System.Void UIPanel::UpdateSelf()
extern void UIPanel_UpdateSelf_m8DE5C71719B72C73F3B3A40E39684AFAFBF87976 ();
// 0x000007B0 System.Void UIPanel::SortWidgets()
extern void UIPanel_SortWidgets_mA57FBDB50143F8D0BFC1CF08961DB96F15BEF892 ();
// 0x000007B1 System.Void UIPanel::FillAllDrawCalls()
extern void UIPanel_FillAllDrawCalls_mADEAE89AF1D8989A6A5462569BDDF4B0A8C42B79 ();
// 0x000007B2 System.Boolean UIPanel::FillDrawCall(UIDrawCall)
extern void UIPanel_FillDrawCall_m6FA14A8A2EF4501D7DED1F26BD8FCB8178F6F4F0 ();
// 0x000007B3 System.Boolean UIPanel::FillDrawCall(UIDrawCall,System.Boolean)
extern void UIPanel_FillDrawCall_m46968901118FFA9DB30E6E12CA6DC7FEC76FB9B0 ();
// 0x000007B4 System.Void UIPanel::UpdateDrawCalls(System.Int32)
extern void UIPanel_UpdateDrawCalls_m20088FC7413030FA856B5952C21BFAF982347A1C ();
// 0x000007B5 System.Void UIPanel::UpdateLayers()
extern void UIPanel_UpdateLayers_m57F19D6A36E4D29FE28BC573133D936399CEFE92 ();
// 0x000007B6 System.Void UIPanel::UpdateWidgets()
extern void UIPanel_UpdateWidgets_m2471993CD370BA7C8696C57972571BD4C2B2DFB4 ();
// 0x000007B7 UIDrawCall UIPanel::FindDrawCall(UIWidget)
extern void UIPanel_FindDrawCall_m4DA51D74681E513B85972FDBB95E857996CC4003 ();
// 0x000007B8 System.Void UIPanel::AddWidget(UIWidget)
extern void UIPanel_AddWidget_mCF202233DE7CEE30DBC4872B84041A37F896B78E ();
// 0x000007B9 System.Void UIPanel::RemoveWidget(UIWidget)
extern void UIPanel_RemoveWidget_m40CB5A093558B799DF7986A3D9A7608307BAB24C ();
// 0x000007BA System.Void UIPanel::Refresh()
extern void UIPanel_Refresh_mD2181152F37B7388E417D9AF3ABD652CC809D709 ();
// 0x000007BB UnityEngine.Vector3 UIPanel::CalculateConstrainOffset(UnityEngine.Vector2,UnityEngine.Vector2)
extern void UIPanel_CalculateConstrainOffset_m124E27D6AE24E8D65AA8F0BC44AB75270E1D62D4 ();
// 0x000007BC System.Boolean UIPanel::ConstrainTargetToBounds(UnityEngine.Transform,UnityEngine.Bounds&,System.Boolean)
extern void UIPanel_ConstrainTargetToBounds_m90265D32706FC4701E36DCE7E186DB92E0E2DBBC ();
// 0x000007BD System.Boolean UIPanel::ConstrainTargetToBounds(UnityEngine.Transform,System.Boolean)
extern void UIPanel_ConstrainTargetToBounds_m9CB610609953CE96EB2F809D379A283B88C706AE ();
// 0x000007BE UIPanel UIPanel::Find(UnityEngine.Transform)
extern void UIPanel_Find_m10E2FDD197287FC1639148980B27449105617179 ();
// 0x000007BF UIPanel UIPanel::Find(UnityEngine.Transform,System.Boolean)
extern void UIPanel_Find_m8BB3DBA1209E71BAD27605B8379308EAE86A17AC ();
// 0x000007C0 UIPanel UIPanel::Find(UnityEngine.Transform,System.Boolean,System.Int32)
extern void UIPanel_Find_mB7445A2AA23832506BCB1563B0ECF01C9AC45A23 ();
// 0x000007C1 UnityEngine.Vector2 UIPanel::GetWindowSize()
extern void UIPanel_GetWindowSize_m1EA9BC985171E34ACD89AF9E944AB21EC71067C3 ();
// 0x000007C2 UnityEngine.Vector2 UIPanel::GetViewSize()
extern void UIPanel_GetViewSize_m824189FA54F8A5DB2B0D7F8CC6180C4975DBED23 ();
// 0x000007C3 System.Void UIPanel::.ctor()
extern void UIPanel__ctor_m3BAC36854F0F03ED0344C22C2E018C450E4E9A97 ();
// 0x000007C4 System.Void UIPanel::.cctor()
extern void UIPanel__cctor_mE999C4979E412E9F52DA893E5A92A4DB7E16C17D ();
// 0x000007C5 UIRoot_Constraint UIRoot::get_constraint()
extern void UIRoot_get_constraint_mE04720CF42D6169961FC90454F14BA20C1862E8C ();
// 0x000007C6 UIRoot_Scaling UIRoot::get_activeScaling()
extern void UIRoot_get_activeScaling_m5BED634E4655B6D5573E996B3760FFF62B85670F ();
// 0x000007C7 System.Int32 UIRoot::get_activeHeight()
extern void UIRoot_get_activeHeight_m2C5F916BE4A224C08084F81491BB783AEB1D1B45 ();
// 0x000007C8 System.Single UIRoot::get_pixelSizeAdjustment()
extern void UIRoot_get_pixelSizeAdjustment_mDF35D2B941B25A2EC91A211A5D78C4E04DADECC3 ();
// 0x000007C9 System.Single UIRoot::GetPixelSizeAdjustment(UnityEngine.GameObject)
extern void UIRoot_GetPixelSizeAdjustment_m8C744D98FACB42F815DB458F8725B83CCFD3FB92 ();
// 0x000007CA System.Single UIRoot::GetPixelSizeAdjustment(System.Int32)
extern void UIRoot_GetPixelSizeAdjustment_m4395C9A983CDE15D1186A9870D5D2D1C1212E0CD ();
// 0x000007CB System.Void UIRoot::Awake()
extern void UIRoot_Awake_m71C5FC9FE8D0F33923C0F04D4B4E2DD27737489E ();
// 0x000007CC System.Void UIRoot::OnEnable()
extern void UIRoot_OnEnable_mD80982A107124EA829909032E2F03EE345D7E7D7 ();
// 0x000007CD System.Void UIRoot::OnDisable()
extern void UIRoot_OnDisable_m9ADC555C5B69FEDE4A66FE53856DA4C94FDF48A8 ();
// 0x000007CE System.Void UIRoot::Start()
extern void UIRoot_Start_m77FBEBFB0B0F6FF24D50173CB67C6489835C8CAA ();
// 0x000007CF System.Void UIRoot::Update()
extern void UIRoot_Update_m7A26DDC2AB2DEDC22458119D5CFDC8F4E318DC07 ();
// 0x000007D0 System.Void UIRoot::UpdateScale(System.Boolean)
extern void UIRoot_UpdateScale_m81966BAAF56ECFDB0B53974085EBBCF301D313C5 ();
// 0x000007D1 System.Void UIRoot::Broadcast(System.String)
extern void UIRoot_Broadcast_mBBD4CDA3176E4B6945018AA12C5D62564C54EB99 ();
// 0x000007D2 System.Void UIRoot::Broadcast(System.String,System.Object)
extern void UIRoot_Broadcast_mD7DDAEF575314F73624997017D2E8735AE0EBF77 ();
// 0x000007D3 System.Void UIRoot::.ctor()
extern void UIRoot__ctor_m9CB2488CEDA6AAB980E9536281470B441708EEB8 ();
// 0x000007D4 System.Void UIRoot::.cctor()
extern void UIRoot__cctor_m8368600793C8792CB53DD2915CB547DB34B56C24 ();
// 0x000007D5 UnityEngine.Texture UISprite::get_mainTexture()
extern void UISprite_get_mainTexture_m3D041319076D28C2EB775CA5270AEC2FF55BC813 ();
// 0x000007D6 System.Void UISprite::set_mainTexture(UnityEngine.Texture)
extern void UISprite_set_mainTexture_m70096D175CA97F5D5C2D7D69765C10760441904F ();
// 0x000007D7 UnityEngine.Material UISprite::get_material()
extern void UISprite_get_material_mEBEA1101E0231B68F6EFB56730B85D4EF66554FF ();
// 0x000007D8 System.Void UISprite::set_material(UnityEngine.Material)
extern void UISprite_set_material_m737821A629675F73CE8CF2A452F478E69C1905D1 ();
// 0x000007D9 INGUIAtlas UISprite::get_atlas()
extern void UISprite_get_atlas_m184550E0493869A2B1DA7001318A029ABF4731A6 ();
// 0x000007DA System.Void UISprite::set_atlas(INGUIAtlas)
extern void UISprite_set_atlas_m2E4D75239AFE7A57D161DF690F74CFD5A32DC8D1 ();
// 0x000007DB System.Boolean UISprite::get_fixedAspect()
extern void UISprite_get_fixedAspect_mDE1608D6CACD31ECA9D2BF519CCFB0814537CB83 ();
// 0x000007DC System.Void UISprite::set_fixedAspect(System.Boolean)
extern void UISprite_set_fixedAspect_mAB937B623E7DAEC4C49C2003404D3CC660801E04 ();
// 0x000007DD UISpriteData UISprite::GetSprite(System.String)
extern void UISprite_GetSprite_mD603D0467F40E6D5A707925BDAB69E43FE44C9C8 ();
// 0x000007DE System.Void UISprite::MarkAsChanged()
extern void UISprite_MarkAsChanged_mA9A52798F45DDF4BDED5423CC1FF25420BE67CDB ();
// 0x000007DF System.String UISprite::get_spriteName()
extern void UISprite_get_spriteName_mE93043CE57A6A6E741A09A224133F17D647CF2BF ();
// 0x000007E0 System.Void UISprite::set_spriteName(System.String)
extern void UISprite_set_spriteName_mFC4DAE1E73BAEE79EA2B8E91A7207F700C82DC49 ();
// 0x000007E1 System.Boolean UISprite::get_isValid()
extern void UISprite_get_isValid_mB6FBF9E8408AA63D5B0FBDEB222791BB28DC9A3B ();
// 0x000007E2 System.Boolean UISprite::get_fillCenter()
extern void UISprite_get_fillCenter_m63E0868E2C649CA60005287E165B5F6DDC14B658 ();
// 0x000007E3 System.Void UISprite::set_fillCenter(System.Boolean)
extern void UISprite_set_fillCenter_m9369872265E934761ACE12AED6471D5568A00413 ();
// 0x000007E4 System.Boolean UISprite::get_applyGradient()
extern void UISprite_get_applyGradient_mF1C3811235040880426FCADFDD1CB5A11C4541E9 ();
// 0x000007E5 System.Void UISprite::set_applyGradient(System.Boolean)
extern void UISprite_set_applyGradient_m4C151ED8EF160829A6AD5877B166857EFBC97C1F ();
// 0x000007E6 UnityEngine.Color UISprite::get_gradientTop()
extern void UISprite_get_gradientTop_mE8DAEBFA68DAA3D1F33333961774FD2C08C03057 ();
// 0x000007E7 System.Void UISprite::set_gradientTop(UnityEngine.Color)
extern void UISprite_set_gradientTop_m37B8A855993DA88F590478BDE369C69655E3C0DB ();
// 0x000007E8 UnityEngine.Color UISprite::get_gradientBottom()
extern void UISprite_get_gradientBottom_mE82D76F259E77C8D3713E2E71552713C3FDA9C7B ();
// 0x000007E9 System.Void UISprite::set_gradientBottom(UnityEngine.Color)
extern void UISprite_set_gradientBottom_mB03786E52C807BCA04987BB00B473F8F45E3A7C5 ();
// 0x000007EA UnityEngine.Vector4 UISprite::get_border()
extern void UISprite_get_border_m147BD8A84448866E904BBEBEE58D925F3EBD65C4 ();
// 0x000007EB UnityEngine.Vector4 UISprite::get_padding()
extern void UISprite_get_padding_m57EFE3DCCC4E818CC2451183BE4277EED46DB112 ();
// 0x000007EC System.Single UISprite::get_pixelSize()
extern void UISprite_get_pixelSize_m07298F7ED10135F3774EF6C8906F8B20A810FEF4 ();
// 0x000007ED System.Int32 UISprite::get_minWidth()
extern void UISprite_get_minWidth_m07D45C7BDFD697D3DB689F09CFEF7DB03B3B1302 ();
// 0x000007EE System.Int32 UISprite::get_minHeight()
extern void UISprite_get_minHeight_mBE846FF9FCE59AAA3B0A1F9F477154A4EC45583F ();
// 0x000007EF UnityEngine.Vector4 UISprite::get_drawingDimensions()
extern void UISprite_get_drawingDimensions_m89BA1E4DF4969B785737D227B82C406BAD1CC64D ();
// 0x000007F0 System.Boolean UISprite::get_premultipliedAlpha()
extern void UISprite_get_premultipliedAlpha_mDE94D1C68BF997DA020001E053D91496F7561FF1 ();
// 0x000007F1 UISpriteData UISprite::GetAtlasSprite()
extern void UISprite_GetAtlasSprite_mB292BF9A63EF7818987C7A720EED7C1E20135A84 ();
// 0x000007F2 System.Void UISprite::SetAtlasSprite(UISpriteData)
extern void UISprite_SetAtlasSprite_mA069E8AD2BAC97FEC8F355FFDDD741B927488232 ();
// 0x000007F3 System.Void UISprite::MakePixelPerfect()
extern void UISprite_MakePixelPerfect_mB50E6CD8DFC3E31C970F808DA79894746F31CD29 ();
// 0x000007F4 System.Void UISprite::OnInit()
extern void UISprite_OnInit_m033D118E6906F024EDC10AEAF003BCFEB077DE47 ();
// 0x000007F5 System.Void UISprite::OnUpdate()
extern void UISprite_OnUpdate_m3A7AEB5EFAEC51D5D25680905EA6C372B7BFBE5A ();
// 0x000007F6 System.Void UISprite::OnFill(System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Color>)
extern void UISprite_OnFill_m14C57CCD491F01BDB4F2EA7CD671F91366898147 ();
// 0x000007F7 System.Void UISprite::.ctor()
extern void UISprite__ctor_mA7126752BE57D55ABCA80281522ED1D5394718CA ();
// 0x000007F8 System.Int32 UISpriteAnimation::get_frames()
extern void UISpriteAnimation_get_frames_mDA0940CA772AA0C3F0A787A250ABC5F5C74497DA ();
// 0x000007F9 System.Int32 UISpriteAnimation::get_framesPerSecond()
extern void UISpriteAnimation_get_framesPerSecond_m19B5091FC686501D1B8CE377367B4CDF5E57A945 ();
// 0x000007FA System.Void UISpriteAnimation::set_framesPerSecond(System.Int32)
extern void UISpriteAnimation_set_framesPerSecond_mAF9DFC76130001D15FDC75E5CC458E51816D20CA ();
// 0x000007FB System.String UISpriteAnimation::get_namePrefix()
extern void UISpriteAnimation_get_namePrefix_m16DEA930951E2F028881CA0D58E26B53D80EC95D ();
// 0x000007FC System.Void UISpriteAnimation::set_namePrefix(System.String)
extern void UISpriteAnimation_set_namePrefix_m446197DE95AF730F9350566C0C2EFB4292599643 ();
// 0x000007FD System.Boolean UISpriteAnimation::get_loop()
extern void UISpriteAnimation_get_loop_mEF8AF1ED52B3AD9152924BBEF96C291BF383B89B ();
// 0x000007FE System.Void UISpriteAnimation::set_loop(System.Boolean)
extern void UISpriteAnimation_set_loop_m24D8DD84396A3A7D7006E1C806B54A26B2578445 ();
// 0x000007FF System.Boolean UISpriteAnimation::get_isPlaying()
extern void UISpriteAnimation_get_isPlaying_mDE4468AE08B65ED9D56EA8BD69E579C32B37C157 ();
// 0x00000800 System.Void UISpriteAnimation::Start()
extern void UISpriteAnimation_Start_m598603BDF435243A5FCCAC35CDC3E8FEB012C262 ();
// 0x00000801 System.Void UISpriteAnimation::Update()
extern void UISpriteAnimation_Update_m813E2428DD4A7A28F17AECB44C1155CEE8B7FFE4 ();
// 0x00000802 System.Void UISpriteAnimation::RebuildSpriteList()
extern void UISpriteAnimation_RebuildSpriteList_mFAC99E1E01E10D583D217E72A6868DF72B6AD097 ();
// 0x00000803 System.Void UISpriteAnimation::Play()
extern void UISpriteAnimation_Play_m0505EDE6FE6CF5BF1974043CD66AED1CDAEF21F9 ();
// 0x00000804 System.Void UISpriteAnimation::Pause()
extern void UISpriteAnimation_Pause_m32D1BD303DDFC3BA55F02828AF2A24EAD26B320A ();
// 0x00000805 System.Void UISpriteAnimation::ResetToBeginning()
extern void UISpriteAnimation_ResetToBeginning_m8E3E5657A5B97E7175F73E8B32A662B94D73B112 ();
// 0x00000806 System.Void UISpriteAnimation::.ctor()
extern void UISpriteAnimation__ctor_m4BBC2AB1E88BAA1003303521D9EBC3DD9635446A ();
// 0x00000807 UnityEngine.Texture UISpriteCollection::get_mainTexture()
extern void UISpriteCollection_get_mainTexture_m5F5B307A574A973FB82D67041676972FF9EEC3D6 ();
// 0x00000808 System.Void UISpriteCollection::set_mainTexture(UnityEngine.Texture)
extern void UISpriteCollection_set_mainTexture_m1035AFDC8D4EB9CF9ABF0C978BDF64F9F45B4B4B ();
// 0x00000809 UnityEngine.Material UISpriteCollection::get_material()
extern void UISpriteCollection_get_material_mE22B7B6CB0AFD96A880A6C7C9F56C41820B624D4 ();
// 0x0000080A System.Void UISpriteCollection::set_material(UnityEngine.Material)
extern void UISpriteCollection_set_material_mF91756475404288BCC8D94733DB18153A6B9B02F ();
// 0x0000080B INGUIAtlas UISpriteCollection::get_atlas()
extern void UISpriteCollection_get_atlas_m10F04A942D79B881722E1B9CA7E60E11C280000A ();
// 0x0000080C System.Void UISpriteCollection::set_atlas(INGUIAtlas)
extern void UISpriteCollection_set_atlas_mC0E89DDE8F278D71D2CF43D93826680C7B261BFC ();
// 0x0000080D System.Single UISpriteCollection::get_pixelSize()
extern void UISpriteCollection_get_pixelSize_m6598AF8AE5BEDF8FEC9B9BBBFAFCF88EF20480C9 ();
// 0x0000080E System.Boolean UISpriteCollection::get_premultipliedAlpha()
extern void UISpriteCollection_get_premultipliedAlpha_m0662857555FD016B863E77952C1D4A4D18EED1CE ();
// 0x0000080F UnityEngine.Vector4 UISpriteCollection::get_border()
extern void UISpriteCollection_get_border_m185E35067CFD5DE21E838EB086693115D931D100 ();
// 0x00000810 UnityEngine.Vector4 UISpriteCollection::get_padding()
extern void UISpriteCollection_get_padding_m04F321702CA6B16DF4B05007BB3F9CEA2F126253 ();
// 0x00000811 System.Void UISpriteCollection::OnFill(System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Color>)
extern void UISpriteCollection_OnFill_m9073ECA04973233C7042649680174B9D8081B03E ();
// 0x00000812 System.Void UISpriteCollection::Add(System.Object,System.String,UnityEngine.Vector2,System.Single,System.Single)
extern void UISpriteCollection_Add_m1C02581B58AFA2106D87197F6A0B16F334A2E640 ();
// 0x00000813 System.Void UISpriteCollection::Add(System.Object,System.String,UnityEngine.Vector2,System.Single,System.Single,UnityEngine.Color32)
extern void UISpriteCollection_Add_m04598866B1131788EAB5475D9AF9FDE21DC7FE5B ();
// 0x00000814 System.Void UISpriteCollection::AddSprite(System.Object,System.String,UnityEngine.Vector2,System.Single,System.Single,UnityEngine.Color32,UnityEngine.Vector2,System.Single,UIBasicSprite_Type,UIBasicSprite_Flip,System.Boolean)
extern void UISpriteCollection_AddSprite_m284615DFC84BCD672B16A32BF3ACBC276CA11D86 ();
// 0x00000815 System.Nullable`1<UISpriteCollection_Sprite> UISpriteCollection::GetSprite(System.Object)
extern void UISpriteCollection_GetSprite_mFAC7CA782F746A6BDBE36C8EC027A205FB08756E ();
// 0x00000816 System.Boolean UISpriteCollection::RemoveSprite(System.Object)
extern void UISpriteCollection_RemoveSprite_mFB776652CCA0ED5BA7BBDB948CC5AF57FF88EBDB ();
// 0x00000817 System.Boolean UISpriteCollection::SetSprite(System.Object,UISpriteCollection_Sprite)
extern void UISpriteCollection_SetSprite_m341A5C6AE9B8AEE35C63A06F06F43440C9DCBC56 ();
// 0x00000818 System.Void UISpriteCollection::Clear()
extern void UISpriteCollection_Clear_m87E2C53349B91BF73C4C85A9F87BEEF051FD5EEA ();
// 0x00000819 System.Boolean UISpriteCollection::IsActive(System.Object)
extern void UISpriteCollection_IsActive_m721A1758AFDE9B634CAE46954B89AA0342C5E880 ();
// 0x0000081A System.Boolean UISpriteCollection::SetActive(System.Object,System.Boolean)
extern void UISpriteCollection_SetActive_mEA74E57638B719B814EA7D13134AF4512A86A839 ();
// 0x0000081B System.Boolean UISpriteCollection::SetPosition(System.Object,UnityEngine.Vector2,System.Boolean)
extern void UISpriteCollection_SetPosition_m3CABF3849277B11FD253D1E1608B84133F331B8E ();
// 0x0000081C UnityEngine.Vector2 UISpriteCollection::Rotate(UnityEngine.Vector2,System.Single)
extern void UISpriteCollection_Rotate_m6A49EF4C8992146E3173FD91CC513BA1B25EFB70 ();
// 0x0000081D System.Object UISpriteCollection::GetCurrentSpriteID()
extern void UISpriteCollection_GetCurrentSpriteID_mFA79298B0088215BEB86AB146990C99723AA5D19 ();
// 0x0000081E System.Nullable`1<UISpriteCollection_Sprite> UISpriteCollection::GetCurrentSprite()
extern void UISpriteCollection_GetCurrentSprite_mA722FC8F08972C50B35B2D5ABE07203F441DF0AA ();
// 0x0000081F System.Object UISpriteCollection::GetCurrentSpriteID(UnityEngine.Vector3)
extern void UISpriteCollection_GetCurrentSpriteID_mD218D330254F2D865A3A555EE0168B8D397AC224 ();
// 0x00000820 System.Nullable`1<UISpriteCollection_Sprite> UISpriteCollection::GetCurrentSprite(UnityEngine.Vector3)
extern void UISpriteCollection_GetCurrentSprite_m287BB7326FE11A90A05E2BFC0B1D149E95E646A3 ();
// 0x00000821 System.Void UISpriteCollection::OnClick()
extern void UISpriteCollection_OnClick_mE1110F983A1474F350152FA1771B8DE076AF021C ();
// 0x00000822 System.Void UISpriteCollection::OnPress(System.Boolean)
extern void UISpriteCollection_OnPress_m3DE40024FFD4EE669272E99B071A492D5907B205 ();
// 0x00000823 System.Void UISpriteCollection::OnHover(System.Boolean)
extern void UISpriteCollection_OnHover_m040198DB3E428B88E87E996E0AA38D7B72FDD223 ();
// 0x00000824 System.Void UISpriteCollection::OnMove(UnityEngine.Vector2)
extern void UISpriteCollection_OnMove_m1AE2745AD9CD0A6C2C2B3559818009A46BB6A670 ();
// 0x00000825 System.Void UISpriteCollection::OnDrag(UnityEngine.Vector2)
extern void UISpriteCollection_OnDrag_mB7E13344261C7FE90D3166554BEB0700252EAA62 ();
// 0x00000826 System.Void UISpriteCollection::OnTooltip(System.Boolean)
extern void UISpriteCollection_OnTooltip_m52C432516649D8FF33936349D066993499FF755B ();
// 0x00000827 System.Void UISpriteCollection::.ctor()
extern void UISpriteCollection__ctor_mFD54BD64FEB209E57C97A8893E58C7BFEE3153FC ();
// 0x00000828 System.Boolean UISpriteData::get_hasBorder()
extern void UISpriteData_get_hasBorder_m51F33C69F9410C85FA68389DD21F252DEF6723F0 ();
// 0x00000829 System.Boolean UISpriteData::get_hasPadding()
extern void UISpriteData_get_hasPadding_m2DDF8609C0B953D985BD1C58CDC398B73C84E532 ();
// 0x0000082A System.Void UISpriteData::SetRect(System.Int32,System.Int32,System.Int32,System.Int32)
extern void UISpriteData_SetRect_m7ED08A06179B5C3D7397DC4137F78F5CF8F78B18 ();
// 0x0000082B System.Void UISpriteData::SetPadding(System.Int32,System.Int32,System.Int32,System.Int32)
extern void UISpriteData_SetPadding_mD7468EE6B39EE752073E4D827D6A980D10F0D39F ();
// 0x0000082C System.Void UISpriteData::SetBorder(System.Int32,System.Int32,System.Int32,System.Int32)
extern void UISpriteData_SetBorder_mEB9ECAE9901AC9A7C0099D95A74DE39652559EB3 ();
// 0x0000082D System.Void UISpriteData::CopyFrom(UISpriteData)
extern void UISpriteData_CopyFrom_mE9316CE56583F7B324773D098B9C39F6565EC633 ();
// 0x0000082E System.Void UISpriteData::CopyBorderFrom(UISpriteData)
extern void UISpriteData_CopyBorderFrom_m762D33C3EAB441610C92E0A6742FF1040222A77F ();
// 0x0000082F System.Void UISpriteData::.ctor()
extern void UISpriteData__ctor_m3FDAC1BD995A3330C8241CD9F08CAAB8D22A143A ();
// 0x00000830 System.Void UIStretch::Awake()
extern void UIStretch_Awake_m09B8904BC1AC6170AE08D1343446942B3EEBBF2C ();
// 0x00000831 System.Void UIStretch::OnDestroy()
extern void UIStretch_OnDestroy_m7C12480DECE7830BD8E05749F0947A03BC1BA143 ();
// 0x00000832 System.Void UIStretch::ScreenSizeChanged()
extern void UIStretch_ScreenSizeChanged_mBB2D2B9693996D80A66F36B98671CB4250680048 ();
// 0x00000833 System.Void UIStretch::Start()
extern void UIStretch_Start_m70DC603B8C39E0145AB11828BF30ADF29EA7CCBD ();
// 0x00000834 System.Void UIStretch::Update()
extern void UIStretch_Update_m878DC723C6E0CD3BD70393CD83C21B812017E126 ();
// 0x00000835 System.Void UIStretch::.ctor()
extern void UIStretch__ctor_m6351DCF7999C024FA8E50F7010093A0C87BB247A ();
// 0x00000836 BetterList`1<UITextList_Paragraph> UITextList::get_paragraphs()
extern void UITextList_get_paragraphs_m0F8B69C9D3B0B8A416E65DC97F08107056D11A46 ();
// 0x00000837 System.Int32 UITextList::get_paragraphCount()
extern void UITextList_get_paragraphCount_m80D1677A6F84EF4F4128DE650FA67DF56A7D731E ();
// 0x00000838 System.Boolean UITextList::get_isValid()
extern void UITextList_get_isValid_m81FD9AAD25F9686541B1C6268912D87950784950 ();
// 0x00000839 System.Single UITextList::get_scrollValue()
extern void UITextList_get_scrollValue_m771846E452FAE634C489EF1FE8560D30C29EE771 ();
// 0x0000083A System.Void UITextList::set_scrollValue(System.Single)
extern void UITextList_set_scrollValue_m68B35880AA9C66296A1B02163B62A3A489D0544A ();
// 0x0000083B System.Single UITextList::get_lineHeight()
extern void UITextList_get_lineHeight_m8FEF995939E6D669A8F29CE55022B9728F0D170C ();
// 0x0000083C System.Int32 UITextList::get_scrollHeight()
extern void UITextList_get_scrollHeight_m4BE3B56661728ED81D38C1FB418BD516CAF67665 ();
// 0x0000083D System.Void UITextList::Clear()
extern void UITextList_Clear_m9C621BA0AF1E487BD39C4319E9661C301CA0FB16 ();
// 0x0000083E System.Void UITextList::Start()
extern void UITextList_Start_mED2DC84F02F15C7BCBE77BE1301BF9CE71AC3E0B ();
// 0x0000083F System.Void UITextList::Update()
extern void UITextList_Update_mFE6F0CCE27030CA5395B796A5583D825B9AB4CF2 ();
// 0x00000840 System.Void UITextList::OnScroll(System.Single)
extern void UITextList_OnScroll_mBD0565FB942E8E51FAD89FF582A25BF1BCFDBBC8 ();
// 0x00000841 System.Void UITextList::OnDrag(UnityEngine.Vector2)
extern void UITextList_OnDrag_m30C03183BBABFD4D080B293A76CD4995048BE144 ();
// 0x00000842 System.Void UITextList::OnScrollBar()
extern void UITextList_OnScrollBar_m3BBC940FF135F0788FA42E8E9BD641C403CC2F7E ();
// 0x00000843 System.Void UITextList::Add(System.String)
extern void UITextList_Add_mAA1536F8E38B235AA07C517D4A1062F7A74E2C40 ();
// 0x00000844 System.Void UITextList::Add(System.String,System.Boolean)
extern void UITextList_Add_m0C4C17C34E385412BE33960F3791F5ADE5094D00 ();
// 0x00000845 System.Void UITextList::Rebuild()
extern void UITextList_Rebuild_m477E174D68A5163BBE071B25A82CAA9CBB8BA2C8 ();
// 0x00000846 System.Void UITextList::UpdateVisibleText()
extern void UITextList_UpdateVisibleText_m014D8C8010BA87078A8DD11B7B52003F32AF62A8 ();
// 0x00000847 System.Void UITextList::.ctor()
extern void UITextList__ctor_m0C2A90D1FB97C9D8F844DC011F6B3D3E9F635C20 ();
// 0x00000848 System.Void UITextList::.cctor()
extern void UITextList__cctor_m1BDF9CF007BDF802C40AC99786B4DAF9FF7A644B ();
// 0x00000849 UnityEngine.Texture UITexture::get_mainTexture()
extern void UITexture_get_mainTexture_m60A9C473AC4ABC86C7722BC1179A8FF836B0C3A6 ();
// 0x0000084A System.Void UITexture::set_mainTexture(UnityEngine.Texture)
extern void UITexture_set_mainTexture_mD74649557D861510C98E59F2EEF7279B90723139 ();
// 0x0000084B UnityEngine.Material UITexture::get_material()
extern void UITexture_get_material_mBF294F6A34DC4FEF66B02058B6BABAEF750BF4B1 ();
// 0x0000084C System.Void UITexture::set_material(UnityEngine.Material)
extern void UITexture_set_material_m677F44573F4823B5004879711B483436E7981CEC ();
// 0x0000084D UnityEngine.Shader UITexture::get_shader()
extern void UITexture_get_shader_m63E6AB328838C0AFECF0344AD844C0A6CB602B70 ();
// 0x0000084E System.Void UITexture::set_shader(UnityEngine.Shader)
extern void UITexture_set_shader_m36FFD7EB879574EF1B38AF8C9A3D4D5C9AFD319A ();
// 0x0000084F System.Boolean UITexture::get_premultipliedAlpha()
extern void UITexture_get_premultipliedAlpha_mA8E7ED176C6FAC20F3EA0473EACDDC12D9B80937 ();
// 0x00000850 UnityEngine.Vector4 UITexture::get_border()
extern void UITexture_get_border_mF8C72A438DEB7D884ECC59DA9B34473BB447B8C5 ();
// 0x00000851 System.Void UITexture::set_border(UnityEngine.Vector4)
extern void UITexture_set_border_m91F9085D5108D95250032B2E8353B9B6C6402B04 ();
// 0x00000852 UnityEngine.Rect UITexture::get_uvRect()
extern void UITexture_get_uvRect_m7F3BCA169F73A8412B6A001FD2D7F9946002E9A2 ();
// 0x00000853 System.Void UITexture::set_uvRect(UnityEngine.Rect)
extern void UITexture_set_uvRect_mE03E2CAD1293C9A27EBC383680DBA7844DABD398 ();
// 0x00000854 UnityEngine.Vector4 UITexture::get_drawingDimensions()
extern void UITexture_get_drawingDimensions_mD91CAE0C08A7833C5D26B5AC91BC7595F14F753B ();
// 0x00000855 System.Boolean UITexture::get_fixedAspect()
extern void UITexture_get_fixedAspect_mA4FB66241AAB0E4AAC4798371E597C724BBFC04B ();
// 0x00000856 System.Void UITexture::set_fixedAspect(System.Boolean)
extern void UITexture_set_fixedAspect_m16FD75339823A7F418784401FB846405014B0A47 ();
// 0x00000857 System.Void UITexture::MakePixelPerfect()
extern void UITexture_MakePixelPerfect_m2E36AD0DA05AD3DC2A01869C9A95FBE75BB2A25F ();
// 0x00000858 System.Void UITexture::OnUpdate()
extern void UITexture_OnUpdate_m91DDEEA6F412B4BBAF2FAD3F05052E74B785A170 ();
// 0x00000859 System.Void UITexture::OnFill(System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Color>)
extern void UITexture_OnFill_mEAA8DC5A47BA17228DA9F9910DBC881DBD24B907 ();
// 0x0000085A System.Void UITexture::.ctor()
extern void UITexture__ctor_m5EEE814B121EAD004ECD1E1C7428F83679C49CE6 ();
// 0x0000085B System.Boolean UITooltip::get_isVisible()
extern void UITooltip_get_isVisible_m5D13EAC61F8962AC8FE0B1D7CD5FD9D9B44522C0 ();
// 0x0000085C System.Void UITooltip::Awake()
extern void UITooltip_Awake_m71BFFF8780774D5563373CF27A53BAF85D93B790 ();
// 0x0000085D System.Void UITooltip::OnDestroy()
extern void UITooltip_OnDestroy_m2EA963263DDEB3E65803D3D69EBC63657E2D583F ();
// 0x0000085E System.Void UITooltip::Start()
extern void UITooltip_Start_m0A35671F047B575DF1E2B1F227651535456E4A1E ();
// 0x0000085F System.Void UITooltip::Update()
extern void UITooltip_Update_mB97533A61865DA65951BFE6EAE28E2940D770AA5 ();
// 0x00000860 System.Void UITooltip::SetAlpha(System.Single)
extern void UITooltip_SetAlpha_mECC9F41981FD54D7CFFFE9932BCBBF765CCF693E ();
// 0x00000861 System.Void UITooltip::SetText(System.String)
extern void UITooltip_SetText_m9DCD820C626A93D0DF79C1B08D60D0DF635BCF14 ();
// 0x00000862 System.Void UITooltip::ShowText(System.String)
extern void UITooltip_ShowText_mDB2E4E41098D91B42AF02C2511552C9D2EF99610 ();
// 0x00000863 System.Void UITooltip::Show(System.String)
extern void UITooltip_Show_mE65FE0130F61CF85C7B9459516C5AC8395433E9A ();
// 0x00000864 System.Void UITooltip::Hide()
extern void UITooltip_Hide_mB872271E189557243C31C35AF8FE1B31B397F6FA ();
// 0x00000865 System.Void UITooltip::.ctor()
extern void UITooltip__ctor_m41B7B95F99CA96F957CE3410D94700D38811086D ();
// 0x00000866 System.Void UIViewport::Start()
extern void UIViewport_Start_mF1861E94FE19BA89B59954D9AABF9E43AF658423 ();
// 0x00000867 System.Void UIViewport::LateUpdate()
extern void UIViewport_LateUpdate_mB14AC73F4DB39DEE49657B2DCEA20894C30AC715 ();
// 0x00000868 System.Void UIViewport::.ctor()
extern void UIViewport__ctor_mCCF31F5AB8FD8A9AD97BAE5D62FCF00BAAEAAA20 ();
// 0x00000869 System.Void LevelSelectButton::set_Index(System.Int32)
extern void LevelSelectButton_set_Index_mFA433D2E2C5005CEBFA2E98E4FEC198776F7D5A9 ();
// 0x0000086A System.Int32 LevelSelectButton::get_Index()
extern void LevelSelectButton_get_Index_m218D466ADB076E26786F17FF36617F7BF4266655 ();
// 0x0000086B System.Void LevelSelectButton::set_Display(System.Int32)
extern void LevelSelectButton_set_Display_m9FCC9F23F5B5ED58798DF0993195E4B170DF0F1B ();
// 0x0000086C System.Int32 LevelSelectButton::get_Display()
extern void LevelSelectButton_get_Display_m474371F48E087162FCE84C8E8B5A3356AD768A56 ();
// 0x0000086D System.Void LevelSelectButton::OnEnable()
extern void LevelSelectButton_OnEnable_m107FEC7938F2C116384E296A75E175BCBF02650D ();
// 0x0000086E System.Void LevelSelectButton::OnClick()
extern void LevelSelectButton_OnClick_m86242D7664AAFEEAE16C9610CE8599112A5FC898 ();
// 0x0000086F System.Void LevelSelectButton::.ctor()
extern void LevelSelectButton__ctor_m2D58CD5017CA9FB36E5C46974C4A93AC7785C390 ();
// 0x00000870 System.Void LevelSelectController::Start()
extern void LevelSelectController_Start_m54694E6C3BF24EB19172124AD8328CA17B601C44 ();
// 0x00000871 System.Void LevelSelectController::Update()
extern void LevelSelectController_Update_m4C5C9F31352EB949A8DF382788DC3A2F62D72326 ();
// 0x00000872 System.Void LevelSelectController::.ctor()
extern void LevelSelectController__ctor_m01AE3E8BB4D00AC6A03BA3FF8857573C625E6199 ();
// 0x00000873 System.Void LevelSelectSceneButton::OnClick()
extern void LevelSelectSceneButton_OnClick_m618B51B98B1C76C32AB6B417DDAEE7C8C11A4B0B ();
// 0x00000874 System.Void LevelSelectSceneButton::.ctor()
extern void LevelSelectSceneButton__ctor_m10CB7AD7A1E34634A496B4DD1D237A0720E0B35A ();
// 0x00000875 System.Void LocalRigibodyContrains::OnEnable()
extern void LocalRigibodyContrains_OnEnable_mF57EDE2F34DCFC815A680D814A9CD88DE4D2C606 ();
// 0x00000876 System.Void LocalRigibodyContrains::FixedUpdate()
extern void LocalRigibodyContrains_FixedUpdate_m44F2E42D054BA4649C82B2C4CFD6BA6EE33BC976 ();
// 0x00000877 System.Void LocalRigibodyContrains::.ctor()
extern void LocalRigibodyContrains__ctor_m23EFF4280AB545A9DA13AF78C12440ADE5D31744 ();
// 0x00000878 System.Void TweenFill::Cache()
extern void TweenFill_Cache_mA0121CA36E619F148D80325EBDF3953735781EE0 ();
// 0x00000879 System.Single TweenFill::get_value()
extern void TweenFill_get_value_m7504D991A9E99ECA97E0ECE569E337E48405F352 ();
// 0x0000087A System.Void TweenFill::set_value(System.Single)
extern void TweenFill_set_value_m99AE799715661A201E71DF86CCBCF877AA659504 ();
// 0x0000087B System.Void TweenFill::OnUpdate(System.Single,System.Boolean)
extern void TweenFill_OnUpdate_m4427F588542937C65876158A38F5EB4F9ADDDF1D ();
// 0x0000087C TweenFill TweenFill::Begin(UnityEngine.GameObject,System.Single,System.Single)
extern void TweenFill_Begin_mEB043667B943AA6F2A0ACCF510C6A93B43C42815 ();
// 0x0000087D System.Void TweenFill::SetStartToCurrentValue()
extern void TweenFill_SetStartToCurrentValue_m12B6CB42D16CB5EEF4A7FDD1AE72F9F0698F2E1C ();
// 0x0000087E System.Void TweenFill::SetEndToCurrentValue()
extern void TweenFill_SetEndToCurrentValue_mDDAE18D278C90B55ABA76FFBF90F7009E02F28A1 ();
// 0x0000087F System.Void TweenFill::.ctor()
extern void TweenFill__ctor_m5C85653649027D9CCF8EAFCD524519EC5AC7BD99 ();
// 0x00000880 System.Void TweenLetters::OnEnable()
extern void TweenLetters_OnEnable_mE9A7D0B8F715DD5A65B5BEC35A92623194398722 ();
// 0x00000881 System.Void TweenLetters::OnDisable()
extern void TweenLetters_OnDisable_m1B1BDCD34DD7106D5E0B37AC4F6273F19462B1F4 ();
// 0x00000882 System.Void TweenLetters::Awake()
extern void TweenLetters_Awake_mEBC262806D5218EBBDE989195B4067225DDC6A19 ();
// 0x00000883 System.Void TweenLetters::Play(System.Boolean)
extern void TweenLetters_Play_mE3925648F4457636A8300D178097CFB723C2EAF4 ();
// 0x00000884 System.Void TweenLetters::OnPostFill(UIWidget,System.Int32,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Color>)
extern void TweenLetters_OnPostFill_mA0EA809EDBFF62046242640A019706E8F9F636BE ();
// 0x00000885 System.Void TweenLetters::OnUpdate(System.Single,System.Boolean)
extern void TweenLetters_OnUpdate_mB9F6F98362CC36380CBC8444516E5E900D1E32B3 ();
// 0x00000886 System.Void TweenLetters::SetLetterOrder(System.Int32)
extern void TweenLetters_SetLetterOrder_m29D224842F933A9D4C49B626BCD612DE06B3C692 ();
// 0x00000887 System.Void TweenLetters::GetLetterDuration(System.Int32)
extern void TweenLetters_GetLetterDuration_m9CDAAA0F24685A1385F1152B177DBF89AD70F2E6 ();
// 0x00000888 System.Single TweenLetters::ScaleRange(System.Single,System.Single,System.Single)
extern void TweenLetters_ScaleRange_m67BD5AB67B74B7296BEE66F756E4C0A1890104F7 ();
// 0x00000889 UnityEngine.Vector3 TweenLetters::GetCenter(System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Int32,System.Int32)
extern void TweenLetters_GetCenter_mF07CA947D46F80865BE488C320BCB224CCF043E7 ();
// 0x0000088A System.Void TweenLetters::.ctor()
extern void TweenLetters__ctor_m6C4829BF2BAFF9B7458ACE5BACE1CD68830596BB ();
// 0x0000088B System.Void Obstacle::Awake()
extern void Obstacle_Awake_mBB902A148BE45B6A355B48ED19D4C65FDAC26EB1 ();
// 0x0000088C System.Void Obstacle::OnTriggerEnter(UnityEngine.Collider)
extern void Obstacle_OnTriggerEnter_m9322D17FBF4BCF07FFC7B86FB37176062FC7000E ();
// 0x0000088D System.Void Obstacle::.ctor()
extern void Obstacle__ctor_m2A721D4A6D3CB0FB812FC38AFCAE3E86AB2EB0F3 ();
// 0x0000088E System.Void PlayButton::OnClick()
extern void PlayButton_OnClick_m339AE19E1E7CA628D7E1487159251FC346AE0685 ();
// 0x0000088F System.Void PlayButton::.ctor()
extern void PlayButton__ctor_mA04C10665F6FA8B3448F21B9DA51D3D4FD061DCC ();
// 0x00000890 System.Void Player::OnEnable()
extern void Player_OnEnable_m05D71B0DCF7B62A4A26ED38A594A01B0DA24491D ();
// 0x00000891 System.Void Player::OnDisable()
extern void Player_OnDisable_m356D36D557966C8B7CD250CA88C8377E07DFC0F3 ();
// 0x00000892 System.Void Player::Start()
extern void Player_Start_mD6E1D31879EB485356D1C22C8AE12C5DF6392E79 ();
// 0x00000893 System.Void Player::GameOver()
extern void Player_GameOver_m767C12B8C73F0972668FF006B9F5F32F58C48C0C ();
// 0x00000894 System.Void Player::Aim()
extern void Player_Aim_m772171A41B89B00261FE30EAB9FED3823E7B5C29 ();
// 0x00000895 System.Void Player::Throw()
extern void Player_Throw_m99E887F2E93A0B479AD66C6A4904C306D43B4B1A ();
// 0x00000896 System.Void Player::Victory()
extern void Player_Victory_m0CEF76BA0E870DFB1174637268B96578E02B295A ();
// 0x00000897 System.Void Player::.ctor()
extern void Player__ctor_mAEC38956EFD0E61D848D4E5AFB83BABCE2DF1E23 ();
// 0x00000898 System.Void Player::<Throw>b__6_0()
extern void Player_U3CThrowU3Eb__6_0_m25ED3C3DDBCA96E51ECBA78CAD8310437FA0630E ();
// 0x00000899 System.Void ResetButton::OnClick()
extern void ResetButton_OnClick_m56149AE4BAC425E23E5F845E890450D07577C23C ();
// 0x0000089A System.Void ResetButton::.ctor()
extern void ResetButton__ctor_mF261E21FFC39A4F0CD661F00D3065B46000EFC70 ();
// 0x0000089B System.Void NativePlugin::Init(System.Func`2<System.String,System.IntPtr>,System.String)
extern void NativePlugin_Init_m834FC3A2C0628A771747799066A5B6479CB63449 ();
// 0x0000089C System.Void NativePlugin::.ctor()
extern void NativePlugin__ctor_mDADE0D97B8DEF2DC3E5A69BEE12EC617590FB8CD ();
// 0x0000089D T SingletonMonoBehaviour`1::get_Instance()
// 0x0000089E System.Void SingletonMonoBehaviour`1::Awake()
// 0x0000089F System.Void SingletonMonoBehaviour`1::.ctor()
// 0x000008A0 System.Void MonoBehaviourWithInit::InitIfNeeded()
extern void MonoBehaviourWithInit_InitIfNeeded_mD87E806432B0678BC3271AAE2A30A81FB8E02D67 ();
// 0x000008A1 System.Void MonoBehaviourWithInit::Init()
extern void MonoBehaviourWithInit_Init_m2FA54490F8D6CCA66B5BB499A86C8B0FA62F552A ();
// 0x000008A2 System.Void MonoBehaviourWithInit::Awake()
extern void MonoBehaviourWithInit_Awake_mB39B461D8E13B532241B5B2F2891EEE5C7E3DB82 ();
// 0x000008A3 System.Void MonoBehaviourWithInit::.ctor()
extern void MonoBehaviourWithInit__ctor_m23DF29E9AD6316303633B5C5930014E532103752 ();
// 0x000008A4 System.Void BotBanner::Start()
extern void BotBanner_Start_m1B77C6E0B02FF557A4BEED45E86AE38D0B71E8C6 ();
// 0x000008A5 System.Void BotBanner::.ctor()
extern void BotBanner__ctor_m1E8367E5C648927B33740F6780990DD7642FD2B1 ();
// 0x000008A6 System.Void CustomSpriteAnimation::Start()
extern void CustomSpriteAnimation_Start_m9892D2E97C5666FDDE9BFE806FC77D1BDED3D3E7 ();
// 0x000008A7 System.Void CustomSpriteAnimation::UpdateSprite()
extern void CustomSpriteAnimation_UpdateSprite_mAA1BF42B6D961E26E56E01459C93F5FEBE02358F ();
// 0x000008A8 System.Void CustomSpriteAnimation::.ctor()
extern void CustomSpriteAnimation__ctor_m0AAE71E672F2EFCF504F8740CFDF8F6C35EE8981 ();
// 0x000008A9 System.Void CustomVibration::Do(EVibrationType,System.Boolean)
extern void CustomVibration_Do_m614A990483CEFC1E4E177D35B1883991F9D6E227 ();
// 0x000008AA System.Void CustomVibration::Vibrate()
extern void CustomVibration_Vibrate_mCE01100F8E76AEDCE289D1ED5C101F12CFD22E54 ();
// 0x000008AB System.Void CustomVibration::_UnityCustomVibrationDo(System.Int32)
extern void CustomVibration__UnityCustomVibrationDo_m50A46352AEC86B36B9DB4EDBA09A90FD137D0BD3 ();
// 0x000008AC System.Void Invokable::.ctor(System.Object,System.IntPtr)
extern void Invokable__ctor_mB11D0F76001F6FA63CFC37B69A878C7AB064A6AC ();
// 0x000008AD System.Void Invokable::Invoke()
extern void Invokable_Invoke_mC37A55CB0F6EA847BA352ADDE2C05565398F864A ();
// 0x000008AE System.IAsyncResult Invokable::BeginInvoke(System.AsyncCallback,System.Object)
extern void Invokable_BeginInvoke_m5F0AF9EF6933AB311C2DCC1D497AC81E18008DCD ();
// 0x000008AF System.Void Invokable::EndInvoke(System.IAsyncResult)
extern void Invokable_EndInvoke_m31D1509F8F55555FA3E1BB614BB6DB605AE37D00 ();
// 0x000008B0 Invoker Invoker::get_Instance()
extern void Invoker_get_Instance_m065424E02F689B03BB169D71313AA9C1EEEB1474 ();
// 0x000008B1 System.Single Invoker::RealDeltaTime()
extern void Invoker_RealDeltaTime_m2C69D4A6BAE20BB937E07774F392F323EE91F602 ();
// 0x000008B2 System.Void Invoker::InvokeDelayed(Invokable,System.Single)
extern void Invoker_InvokeDelayed_mFEAA28E3F2535E5A5953EE842E94B92303C667CA ();
// 0x000008B3 System.Void Invoker::Update()
extern void Invoker_Update_m07A12E7764521D38773C1EC2F73E25DADF6183CD ();
// 0x000008B4 System.Void Invoker::.ctor()
extern void Invoker__ctor_mA064ED7F000028CED895D501772878D4639A26F3 ();
// 0x000008B5 System.Void Invoker::.cctor()
extern void Invoker__cctor_m4DD36502AD42A199BD95FCD668243A4F01E0FA8D ();
// 0x000008B6 System.Void LocalizeLabel::Awake()
extern void LocalizeLabel_Awake_m8B051B760F46EA4100F335E515F089AB2AC7B4C8 ();
// 0x000008B7 System.Void LocalizeLabel::.ctor()
extern void LocalizeLabel__ctor_m44CD38EEE5E760683E8B45C847FF47BF563729BC ();
// 0x000008B8 System.Void LocalizeObject::Start()
extern void LocalizeObject_Start_m602D56650B85E7C06BCEC9CB64336A0D16F42975 ();
// 0x000008B9 System.Void LocalizeObject::.ctor()
extern void LocalizeObject__ctor_m474FB769AE7D5388BEC984C9CFD7CB036CFFC107 ();
// 0x000008BA System.Void TopBanner::OnEnable()
extern void TopBanner_OnEnable_mAD75C378377957CA35B5F9302D811B25E3526435 ();
// 0x000008BB System.Void TopBanner::.ctor()
extern void TopBanner__ctor_m927056BF8E4236F57177102B98D0250B0C46AF81 ();
// 0x000008BC System.Void UserData::Save()
extern void UserData_Save_mF50D1054306E1217881B58AD10E3D1BC4F919244 ();
// 0x000008BD System.Int32 UserData::get_PlayCount()
extern void UserData_get_PlayCount_m9084A7560BAA8ED01CBF22B84AC91305809F7E3F ();
// 0x000008BE System.Void UserData::set_PlayCount(System.Int32)
extern void UserData_set_PlayCount_m12825D6588392035F195C4C7B8DE4823B9A4B6FC ();
// 0x000008BF System.Void UserData::add_OnChangeMuteSetting(System.Action`1<System.Boolean>)
extern void UserData_add_OnChangeMuteSetting_m6CCB103D1AD411A532F89153C9E5EFD7DAA8723F ();
// 0x000008C0 System.Void UserData::remove_OnChangeMuteSetting(System.Action`1<System.Boolean>)
extern void UserData_remove_OnChangeMuteSetting_m59B3112368ECF437FC4B4F12AC74C6330C4CF962 ();
// 0x000008C1 System.Boolean UserData::get_IsMute()
extern void UserData_get_IsMute_m4E8D9B381B6D3FBD4159D768B6026D2BBA41ADC5 ();
// 0x000008C2 System.Void UserData::set_IsMute(System.Boolean)
extern void UserData_set_IsMute_m69DCC3A7B603DA633F2B5002838AF1F7C37F0F5A ();
// 0x000008C3 System.Boolean UserData::get_IsVibrate()
extern void UserData_get_IsVibrate_mC51FBFCF62ED184AC7891AB50987C630748ECAB9 ();
// 0x000008C4 System.Void UserData::set_IsVibrate(System.Boolean)
extern void UserData_set_IsVibrate_mF754D5DA9C3C113368CEB55B7963D5BA6D575975 ();
// 0x000008C5 System.Single UserData::get_Delay()
extern void UserData_get_Delay_m7985D9B99E44ED14E8170FE63683FCDCF80051E7 ();
// 0x000008C6 System.Void UserData::set_Delay(System.Single)
extern void UserData_set_Delay_m2FEC62B1DC36B0643DC290D911DA2F2C1CDE1AEB ();
// 0x000008C7 System.Int32 UserData::get_CurrentStage()
extern void UserData_get_CurrentStage_m6162B02896A16936283ACE2B253AA6C6BC46DB36 ();
// 0x000008C8 System.Void UserData::set_CurrentStage(System.Int32)
extern void UserData_set_CurrentStage_mEBB67746EFE7E9DC6811B5583305253BA59DAFE3 ();
// 0x000008C9 System.Int32 UserData::get_MaxStage()
extern void UserData_get_MaxStage_m0D450A2ED7DDE630E05999C77C4D23A9A50F2C48 ();
// 0x000008CA System.Int32 UserData::get_RemainHint()
extern void UserData_get_RemainHint_m28A58C0BB67BB3169877C48D2EAE3410493CFE74 ();
// 0x000008CB System.Void UserData::set_RemainHint(System.Int32)
extern void UserData_set_RemainHint_m8A2D9D05B96CBA824EBF207893899F605AF816BE ();
// 0x000008CC System.Int32 UserData::get_ShowHint()
extern void UserData_get_ShowHint_mBDABB7383A5954607C398F80F8541118DB90967F ();
// 0x000008CD System.Void UserData::set_ShowHint(System.Int32)
extern void UserData_set_ShowHint_m66BC3A4425A8CFFDB5D8AF5CB86B810341803DC7 ();
// 0x000008CE System.Int32 UserData::get_LevelCount()
extern void UserData_get_LevelCount_m87C9051790F33251C2FACD49BDA12A884A7C2FF9 ();
// 0x000008CF System.Void UserData::set_LevelCount(System.Int32)
extern void UserData_set_LevelCount_m899E07D2FE5F6C29963536BEAC8C245C442BE917 ();
// 0x000008D0 System.Void UserData::.cctor()
extern void UserData__cctor_m93E2E4C822B112EBC31139D378E3BAA6CE02CE34 ();
// 0x000008D1 System.Void FPSUI::Awake()
extern void FPSUI_Awake_m34DDDC1B6737732D273EA5FD712D7A9F999080C0 ();
// 0x000008D2 System.Void FPSUI::CalcFPS()
extern void FPSUI_CalcFPS_m647D3FF58D52B996EB8D7358EE84FAEE88B0CECE ();
// 0x000008D3 System.Void FPSUI::UpdateGUI()
extern void FPSUI_UpdateGUI_mDE3561AEC546CE9D04C7F09B2ED8C284696B9C8E ();
// 0x000008D4 System.Void FPSUI::.ctor()
extern void FPSUI__ctor_m2AFD8FB94289459F304CBAD3ED644EED88AC33C2 ();
// 0x000008D5 System.Void TestUI::Awake()
extern void TestUI_Awake_mBCD7230D8EAFDA0CA959B73B8827332FD9FF1404 ();
// 0x000008D6 System.Void TestUI::OnGUI()
extern void TestUI_OnGUI_m88DB80442579D119BF20028EBDA93E4E25027E61 ();
// 0x000008D7 System.Void TestUI::UpdateGUI()
extern void TestUI_UpdateGUI_mE172E2486CE50A20CEC0D24DE400661BC6A91ADE ();
// 0x000008D8 System.Void TestUI::.ctor()
extern void TestUI__ctor_m91C91753026C4246F92C2935EA2FE8C84FB6D269 ();
// 0x000008D9 System.Void TestUI::.cctor()
extern void TestUI__cctor_m1182FDEED2E8FD135202AC0F246E2DAD005D6914 ();
// 0x000008DA TValue DictionaryExtensions::GetValueOrDefault(System.Collections.Generic.IDictionary`2<TKey,TValue>,TKey,TValue)
// 0x000008DB System.Boolean EnumExtensions::HasFlag(System.Enum,System.Enum)
extern void EnumExtensions_HasFlag_m136EA7634B6872FA1A825881D9C2F166899FD1A2 ();
// 0x000008DC UnityEngine.GameObject GameObjectExtension::CreateChild(UnityEngine.GameObject,System.String)
extern void GameObjectExtension_CreateChild_m6D7AE667FE2133030DECDDFE820D43163AAAFA7F ();
// 0x000008DD System.Void GameObjectExtension::SetLayerInChildren(UnityEngine.GameObject,System.Int32)
extern void GameObjectExtension_SetLayerInChildren_mEF3C52E3136945E5416691902378CF35042C41A0 ();
// 0x000008DE System.Void GameObjectExtension::SetTagInChildren(UnityEngine.GameObject,System.String)
extern void GameObjectExtension_SetTagInChildren_m4E2369594AF1067D4733ADDCE702008D6D2A1443 ();
// 0x000008DF System.Void GameObjectExtension::SetMaterialInChildren(UnityEngine.GameObject,UnityEngine.Material)
extern void GameObjectExtension_SetMaterialInChildren_m0BA98CA1415F985D4F4C6675F1170DF3D2D3FAD7 ();
// 0x000008E0 System.Collections.IEnumerator MonoBehaviorExtentsion::DelayMethod(UnityEngine.MonoBehaviour,System.Single,System.Action`3<T1,T2,T3>,T1,T2,T3)
// 0x000008E1 System.Collections.IEnumerator MonoBehaviorExtentsion::DelayMethod(UnityEngine.MonoBehaviour,System.Single,System.Action`2<T1,T2>,T1,T2)
// 0x000008E2 System.Collections.IEnumerator MonoBehaviorExtentsion::DelayMethod(UnityEngine.MonoBehaviour,System.Single,System.Action`1<T>,T)
// 0x000008E3 System.Collections.IEnumerator MonoBehaviorExtentsion::DelayMethod(UnityEngine.MonoBehaviour,System.Single,System.Action)
extern void MonoBehaviorExtentsion_DelayMethod_mDB71FB3D7EECE09D6EC20C39B5CD9DC952B9A013 ();
// 0x000008E4 UnityEngine.RaycastHit PhysicsExtentsion::RaycastAndDraw(UnityEngine.Ray,System.Single,System.Int32)
extern void PhysicsExtentsion_RaycastAndDraw_m0AEE6566D56B05F2A0CB53CD79DBBEFCCB81B5F3 ();
// 0x000008E5 UnityEngine.RaycastHit PhysicsExtentsion::RaycastAndDraw(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32)
extern void PhysicsExtentsion_RaycastAndDraw_mE7DD10DF99D4B5F1E7058B201B7517FA2021DEBB ();
// 0x000008E6 System.Single Velocity2DTmp::get_AngularVelocity()
extern void Velocity2DTmp_get_AngularVelocity_m78E803D51D4A71BCB4CCC405E6B640E6FE6E60FF ();
// 0x000008E7 UnityEngine.Vector2 Velocity2DTmp::get_Velocity()
extern void Velocity2DTmp_get_Velocity_m180DBC13C01CFA72E1EAB1C3130E5BAD7FC048F4 ();
// 0x000008E8 System.Void Velocity2DTmp::Set(UnityEngine.Rigidbody2D)
extern void Velocity2DTmp_Set_mA61B08CF34857359EBD6D160DE7C907940C0F867 ();
// 0x000008E9 System.Void Velocity2DTmp::.ctor()
extern void Velocity2DTmp__ctor_m6D3084B8CDF26BFAA9459089FFACF6553565F726 ();
// 0x000008EA System.Void Rigidbody2DExtension::Pause(UnityEngine.Rigidbody2D,UnityEngine.GameObject)
extern void Rigidbody2DExtension_Pause_m8532097402E3947F35D42A6DB616B3C58771EDB3 ();
// 0x000008EB System.Void Rigidbody2DExtension::Resume(UnityEngine.Rigidbody2D,UnityEngine.GameObject)
extern void Rigidbody2DExtension_Resume_mE5DBAFC62C586B9A4BB98F092CD454AB6482A847 ();
// 0x000008EC UnityEngine.Vector3 VelocityTmp::get_AngularVelocity()
extern void VelocityTmp_get_AngularVelocity_m714D2E8D69B5854B8244E86FD201E6530540DB81 ();
// 0x000008ED UnityEngine.Vector3 VelocityTmp::get_Velocity()
extern void VelocityTmp_get_Velocity_mF014723AB722CE3C31A252C028A110A7A0AD411A ();
// 0x000008EE System.Void VelocityTmp::Set(UnityEngine.Rigidbody)
extern void VelocityTmp_Set_m4786208DAD58492E62817E57938FB2E7923852FE ();
// 0x000008EF System.Void VelocityTmp::.ctor()
extern void VelocityTmp__ctor_m1861706A0901AC2EC5B88E031D64E734C3DF52D5 ();
// 0x000008F0 System.Void RigidbodyExtension::Pause(UnityEngine.Rigidbody,UnityEngine.GameObject)
extern void RigidbodyExtension_Pause_m0592A89689C72A66DC87997EF396C39EEA6B8180 ();
// 0x000008F1 System.Void RigidbodyExtension::Resume(UnityEngine.Rigidbody,UnityEngine.GameObject)
extern void RigidbodyExtension_Resume_m34307BB055FEBA75FC9D36FCB629CAF4B2044E09 ();
// 0x000008F2 System.Boolean StringExtensions::AnyEqual(System.String,System.String[])
extern void StringExtensions_AnyEqual_mD5D42774908AB662084CE3F2BB13DCF0D07E203F ();
// 0x000008F3 System.Boolean StringExtensions::ContainsFormatInFront(System.String,System.String)
extern void StringExtensions_ContainsFormatInFront_m76BF26EA2081A70C0B35C15597D191D61DC466DB ();
// 0x000008F4 System.Boolean StringExtensions::IsNullOrEmpty(System.String)
extern void StringExtensions_IsNullOrEmpty_mDFAF484A60D1D39BFA2C5C34F1F5194DA72AAB7A ();
// 0x000008F5 System.Collections.Generic.List`1<System.String> StringExtensions::Split(System.String,System.String)
extern void StringExtensions_Split_m13358513CE16BF6531B60CF301B4A5596079A2E0 ();
// 0x000008F6 System.Int32 StringExtensions::CountOf(System.String,System.String[])
extern void StringExtensions_CountOf_mA610DB28E762851519E1A6119878F14ABA46FC76 ();
// 0x000008F7 System.String StringExtensions::Coloring(System.String,System.String)
extern void StringExtensions_Coloring_mC0F0E14F3FC4159755F758E0C312428E87C26251 ();
// 0x000008F8 System.String StringExtensions::Red(System.String)
extern void StringExtensions_Red_mA54228D5A10997D9703BA2F223B5546615223CE5 ();
// 0x000008F9 System.String StringExtensions::Green(System.String)
extern void StringExtensions_Green_m61B7076135933BB375A3707264EF222849ACF234 ();
// 0x000008FA System.String StringExtensions::Blue(System.String)
extern void StringExtensions_Blue_mA9816EC7052181765E4047CAC30DB422CDE6B4F2 ();
// 0x000008FB System.String StringExtensions::Resize(System.String,System.Int32)
extern void StringExtensions_Resize_m850BA57A4B1CD7F8E225501A6ECEBE600F929205 ();
// 0x000008FC System.String StringExtensions::Medium(System.String)
extern void StringExtensions_Medium_m04CBA4A1EC1135ECC4B28E9308BEC0B39C9750F5 ();
// 0x000008FD System.String StringExtensions::Small(System.String)
extern void StringExtensions_Small_m93AF9F92D74F3B5D85A540F6B714568195C99E3F ();
// 0x000008FE System.String StringExtensions::Large(System.String)
extern void StringExtensions_Large_mA544BA64C71954B9851733BA1DA9656AD6BC1B88 ();
// 0x000008FF System.String StringExtensions::Bold(System.String)
extern void StringExtensions_Bold_m5693C8A4689DB91E22B5F3D7B8E0C821047F6197 ();
// 0x00000900 System.String StringExtensions::Italic(System.String)
extern void StringExtensions_Italic_m9FAC329D7FD0FAD6F2E048268AB6EB780BAB3341 ();
// 0x00000901 System.Void TransformExtension::SetPosition(UnityEngine.Transform,System.Single,System.Single,System.Single)
extern void TransformExtension_SetPosition_m83FB37CEF983A9BE36AD2CE05FCF3CE85F541FBE ();
// 0x00000902 System.Void TransformExtension::SetPositionX(UnityEngine.Transform,System.Single)
extern void TransformExtension_SetPositionX_mE31A1DE6E28D50748BC243DAD958AC2A8116805D ();
// 0x00000903 System.Void TransformExtension::SetPositionY(UnityEngine.Transform,System.Single)
extern void TransformExtension_SetPositionY_m1222F0103804C4AB55BD7EFD07CABE78C2D99E24 ();
// 0x00000904 System.Void TransformExtension::SetPositionZ(UnityEngine.Transform,System.Single)
extern void TransformExtension_SetPositionZ_m4B1BCE13FD5558427A34CEB5EB0354818B7E6DBB ();
// 0x00000905 System.Void TransformExtension::SetLocalPosition(UnityEngine.Transform,System.Single,System.Single,System.Single)
extern void TransformExtension_SetLocalPosition_m6A12F31B32BC38FC6BB5CAA8610C6FE67B9FC753 ();
// 0x00000906 System.Void TransformExtension::SetLocalPositionX(UnityEngine.Transform,System.Single)
extern void TransformExtension_SetLocalPositionX_m66C8CD40A500F95E71E70B5C48A1615FD89CB040 ();
// 0x00000907 System.Void TransformExtension::SetLocalPositionY(UnityEngine.Transform,System.Single)
extern void TransformExtension_SetLocalPositionY_m038FABF6B0EBBF1F5474B1D3F242726CCD4E56D9 ();
// 0x00000908 System.Void TransformExtension::SetLocalPositionZ(UnityEngine.Transform,System.Single)
extern void TransformExtension_SetLocalPositionZ_mA0307684C139A2C97CACE2850899FB51DEB6845A ();
// 0x00000909 System.Void TransformExtension::AddPosition(UnityEngine.Transform,System.Single,System.Single,System.Single)
extern void TransformExtension_AddPosition_mA39EC21BFCEA995BA62E02E97DAA10AF47C3599C ();
// 0x0000090A System.Void TransformExtension::AddPositionX(UnityEngine.Transform,System.Single)
extern void TransformExtension_AddPositionX_m5D56ED4885ECC4CBC9931E69BBEE8BFA6B00E857 ();
// 0x0000090B System.Void TransformExtension::AddPositionY(UnityEngine.Transform,System.Single)
extern void TransformExtension_AddPositionY_m79E44B898E568297060C7C864F98B485A38BA830 ();
// 0x0000090C System.Void TransformExtension::AddPositionZ(UnityEngine.Transform,System.Single)
extern void TransformExtension_AddPositionZ_m3030520D768B6937F6479BF8F34F6C2A73D19D0E ();
// 0x0000090D System.Void TransformExtension::AddLocalPosition(UnityEngine.Transform,System.Single,System.Single,System.Single)
extern void TransformExtension_AddLocalPosition_m1B948BC531728E3D16A1C6E7CA0790C55DD935BF ();
// 0x0000090E System.Void TransformExtension::AddLocalPositionX(UnityEngine.Transform,System.Single)
extern void TransformExtension_AddLocalPositionX_m893D1A4769967889605F30C2FA6381811F379981 ();
// 0x0000090F System.Void TransformExtension::AddLocalPositionY(UnityEngine.Transform,System.Single)
extern void TransformExtension_AddLocalPositionY_m6D19F5214742BFD844B8E090169B465DD1A5C5E1 ();
// 0x00000910 System.Void TransformExtension::AddLocalPositionZ(UnityEngine.Transform,System.Single)
extern void TransformExtension_AddLocalPositionZ_m0F4A6B7078597090B18897E3977B5943731711EC ();
// 0x00000911 System.Void TransformExtension::SetLocalScale(UnityEngine.Transform,System.Single,System.Single,System.Single)
extern void TransformExtension_SetLocalScale_mD1C487C6049133B846EBC872C5A2ABA785471B58 ();
// 0x00000912 System.Void TransformExtension::SetLocalScaleX(UnityEngine.Transform,System.Single)
extern void TransformExtension_SetLocalScaleX_m87DF307719F53E046967307B90C84AFBC43D6AAE ();
// 0x00000913 System.Void TransformExtension::SetLocalScaleY(UnityEngine.Transform,System.Single)
extern void TransformExtension_SetLocalScaleY_m32567A004ADAC2FA5B860EAD650F3C0E651AB8A7 ();
// 0x00000914 System.Void TransformExtension::SetLocalScaleZ(UnityEngine.Transform,System.Single)
extern void TransformExtension_SetLocalScaleZ_mAB963B72364938063C4C95CF34679593AE5B938F ();
// 0x00000915 System.Void TransformExtension::AddLocalScale(UnityEngine.Transform,System.Single,System.Single,System.Single)
extern void TransformExtension_AddLocalScale_mAEAE8E7424D241D74CF83ADF1C0BA0642226521E ();
// 0x00000916 System.Void TransformExtension::AddLocalScaleX(UnityEngine.Transform,System.Single)
extern void TransformExtension_AddLocalScaleX_m180D504D3B3BDE6E535D9789007F9ACA90DCEFDE ();
// 0x00000917 System.Void TransformExtension::AddLocalScaleY(UnityEngine.Transform,System.Single)
extern void TransformExtension_AddLocalScaleY_m62B49C473DABC1473B0CC656C94268DF42A2289C ();
// 0x00000918 System.Void TransformExtension::AddLocalScaleZ(UnityEngine.Transform,System.Single)
extern void TransformExtension_AddLocalScaleZ_m1FA89ACC81E54F1FA65A560E8CB51FBFC49F47F1 ();
// 0x00000919 System.Void TransformExtension::SetEulerAngles(UnityEngine.Transform,System.Single,System.Single,System.Single)
extern void TransformExtension_SetEulerAngles_m2D9EC0AC53E2A39056B165E0EDBC2CA82F3CF50B ();
// 0x0000091A System.Void TransformExtension::SetEulerAngleX(UnityEngine.Transform,System.Single)
extern void TransformExtension_SetEulerAngleX_m35DA4B51A3DCBE51BA047576FD7465845A21E2B0 ();
// 0x0000091B System.Void TransformExtension::SetEulerAngleY(UnityEngine.Transform,System.Single)
extern void TransformExtension_SetEulerAngleY_mC3DA80139BCB2D25880035D0D13C6FF869AC3165 ();
// 0x0000091C System.Void TransformExtension::SetEulerAngleZ(UnityEngine.Transform,System.Single)
extern void TransformExtension_SetEulerAngleZ_mC746B33B6C7EFFFEBD7C8865A3704F4F9966140C ();
// 0x0000091D System.Void TransformExtension::SetLocalEulerAngles(UnityEngine.Transform,System.Single,System.Single,System.Single)
extern void TransformExtension_SetLocalEulerAngles_mDA4792113BB86902B19DB2366E241D557D807A88 ();
// 0x0000091E System.Void TransformExtension::SetLocalEulerAngleX(UnityEngine.Transform,System.Single)
extern void TransformExtension_SetLocalEulerAngleX_m662E09406C6261E7DC9A0AE1AFB8A183DC414698 ();
// 0x0000091F System.Void TransformExtension::SetLocalEulerAngleY(UnityEngine.Transform,System.Single)
extern void TransformExtension_SetLocalEulerAngleY_m21AF0AC8EDEC340BD4E727E25310E0EE1A2AA111 ();
// 0x00000920 System.Void TransformExtension::SetLocalEulerAngleZ(UnityEngine.Transform,System.Single)
extern void TransformExtension_SetLocalEulerAngleZ_mFEC7420B2CBAA54F212D648E3775DAFB577D8C2C ();
// 0x00000921 System.Void TransformExtension::AddEulerAngles(UnityEngine.Transform,System.Single,System.Single,System.Single)
extern void TransformExtension_AddEulerAngles_mBC93089A9F1B5FBD27E73AF1FE52200F8D36BD22 ();
// 0x00000922 System.Void TransformExtension::AddEulerAngleX(UnityEngine.Transform,System.Single)
extern void TransformExtension_AddEulerAngleX_mB11F61D067A44D5D9FD6BFBFAC17B98453EF720A ();
// 0x00000923 System.Void TransformExtension::AddEulerAngleY(UnityEngine.Transform,System.Single)
extern void TransformExtension_AddEulerAngleY_m68C2B03A2BB7AD67CBBFCD643C83B45875016917 ();
// 0x00000924 System.Void TransformExtension::AddEulerAngleZ(UnityEngine.Transform,System.Single)
extern void TransformExtension_AddEulerAngleZ_mE217FE50ED32F76A7439AA2AD27C5561EB923481 ();
// 0x00000925 System.Void TransformExtension::AddLocalEulerAngles(UnityEngine.Transform,System.Single,System.Single,System.Single)
extern void TransformExtension_AddLocalEulerAngles_m50717DCCF3774FD895D441310970490FE981370D ();
// 0x00000926 System.Void TransformExtension::AddLocalEulerAngleX(UnityEngine.Transform,System.Single)
extern void TransformExtension_AddLocalEulerAngleX_m08313B3D9DDDB611FBAE76018DE5EE64D93571EA ();
// 0x00000927 System.Void TransformExtension::AddLocalEulerAngleY(UnityEngine.Transform,System.Single)
extern void TransformExtension_AddLocalEulerAngleY_mFE99719E478A410AFE7E9E665B1E70EDDB9AEF0F ();
// 0x00000928 System.Void TransformExtension::AddLocalEulerAngleZ(UnityEngine.Transform,System.Single)
extern void TransformExtension_AddLocalEulerAngleZ_m53C27E4F6D1F01082D077E12B02C664A47DD85BF ();
// 0x00000929 System.Void TransformExtension::LookAt2D(UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.Vector2)
extern void TransformExtension_LookAt2D_m087AE0C7AB8FE38744811C020EEC0B6FF59EBA66 ();
// 0x0000092A UnityEngine.Quaternion TransformExtension::GetForwardRotation2D(UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.Vector2)
extern void TransformExtension_GetForwardRotation2D_m101850FA38DFCF6B06CB75F7528D515703278C1B ();
// 0x0000092B System.Single TransformExtension::GetForwardDiffPoint(UnityEngine.Vector2)
extern void TransformExtension_GetForwardDiffPoint_mEFB8F5A7654F9A72F78ACA645F64BC21DA4360AA ();
// 0x0000092C System.Collections.Generic.List`1<UnityEngine.GameObject> TransformExtension::GetChildren(UnityEngine.GameObject)
extern void TransformExtension_GetChildren_m7C87B4DF09731F62919DF948B417AC13654E13D1 ();
// 0x0000092D System.Void AutoBGMSwitcher::Start()
extern void AutoBGMSwitcher_Start_mC5A4C0E0D930855D59501C119D88B23D0E97AEAE ();
// 0x0000092E System.Void AutoBGMSwitcher::ChangeBGM(System.Int32)
extern void AutoBGMSwitcher_ChangeBGM_mE21E512DB1118D135981CB1F50FEFBB19AAB9786 ();
// 0x0000092F System.Void AutoBGMSwitcher::.ctor()
extern void AutoBGMSwitcher__ctor_m89462078CE9F75C7898225737381B397C8FF4E5C ();
// 0x00000930 System.Void AutoBGMSwitcher::<Start>b__0_0()
extern void AutoBGMSwitcher_U3CStartU3Eb__0_0_mB91DFA3426537EF400F61E6F9A38888B11A4B728 ();
// 0x00000931 System.Void RecyclingObject::Init()
extern void RecyclingObject_Init_m1E945321E9C0C019951450C3C6FF9F61E90E945F ();
// 0x00000932 System.Void RecyclingObject::Release()
extern void RecyclingObject_Release_m9611130D3A8CC119BD37F4D3FBD5AFBC6ABD6812 ();
// 0x00000933 System.Void RecyclingObject::.ctor()
extern void RecyclingObject__ctor_m72427734E3C0BA7E476EC4BFA686ED86D00AA459 ();
// 0x00000934 System.Void RecyclingObjectPool::Awake()
extern void RecyclingObjectPool_Awake_mAC704289CFC2633A642D3F1811F33F58412CD418 ();
// 0x00000935 UnityEngine.GameObject RecyclingObjectPool::Get(System.String)
extern void RecyclingObjectPool_Get_mA95041130609E08F805B1072E9832DEB77736C05 ();
// 0x00000936 System.Void RecyclingObjectPool::Release(UnityEngine.GameObject)
extern void RecyclingObjectPool_Release_mE8572A01712AA141618816A425C76ADA8BB9D958 ();
// 0x00000937 System.Void RecyclingObjectPool::.ctor()
extern void RecyclingObjectPool__ctor_mCDAF33192E18D8D1611D885571CFC74313A670E1 ();
// 0x00000938 System.Void AudioManager`1::Init()
// 0x00000939 System.Void AudioManager`1::Start()
// 0x0000093A System.Void AudioManager`1::SetIsMute(System.Boolean)
// 0x0000093B System.Void AudioManager`1::ChangePitch(System.Single)
// 0x0000093C System.Boolean AudioManager`1::IsBGM()
// 0x0000093D System.Void AudioManager`1::.ctor()
// 0x0000093E System.Void BGMManager::Init()
extern void BGMManager_Init_m450FCFFFE68A0A81E06E522078353B79C6149944 ();
// 0x0000093F System.Void BGMManager::Update()
extern void BGMManager_Update_mD053DA45781E1A77AB55D0B109B2B910CEEAFAC8 ();
// 0x00000940 System.Void BGMManager::ChangeLoop(System.Boolean)
extern void BGMManager_ChangeLoop_m3D0D6F50069120219F4567AF1112AEA148D289A8 ();
// 0x00000941 System.Void BGMManager::PlayBGM(System.String)
extern void BGMManager_PlayBGM_m928386725AA12754DD23AF811231D61F30F41368 ();
// 0x00000942 System.Void BGMManager::StopBGM()
extern void BGMManager_StopBGM_mB6CB85931DA79658B2F2FDC4E358FB008C26584B ();
// 0x00000943 System.Void BGMManager::FadeOutBGM(System.String)
extern void BGMManager_FadeOutBGM_mB4E46509624C3F7B6069DB02F055DB4002801B27 ();
// 0x00000944 System.Void BGMManager::Pause()
extern void BGMManager_Pause_m7881455F72B01A98BB06F8719A312E21C6A0D02A ();
// 0x00000945 System.Void BGMManager::Resume()
extern void BGMManager_Resume_m5617B5846D6EB18248A7C99C631CCAB42E07F62E ();
// 0x00000946 System.Void BGMManager::SetIsMute(System.Boolean)
extern void BGMManager_SetIsMute_mF032470896D9B8A65C3C698F998C47C09ECB85A9 ();
// 0x00000947 System.Boolean BGMManager::IsBGM()
extern void BGMManager_IsBGM_mFA6476938BA7507BE0ED9750382842887A86E7C3 ();
// 0x00000948 System.Void BGMManager::.ctor()
extern void BGMManager__ctor_m55484986188DF7C12CB4CEA0022127DDAE2EF718 ();
// 0x00000949 System.Void SEManager::Init()
extern void SEManager_Init_mB6A359CA0A5551B552064055D236A71A73BFE10A ();
// 0x0000094A System.Void SEManager::PlaySE(System.String,System.Single,System.Single,System.Single)
extern void SEManager_PlaySE_mC6022D57E874B786730DBED62173D8D9070F5AAC ();
// 0x0000094B System.Void SEManager::PlayDelaySE(System.String,System.Single,System.Single)
extern void SEManager_PlayDelaySE_m8CD15D69BF32C7C6FFA758918C01A8EF6FE51377 ();
// 0x0000094C System.Void SEManager::Play(UnityEngine.AudioSource,System.Single,System.Single,System.String)
extern void SEManager_Play_m7405E293806F637C9BB01AF01D984B3DF608AA26 ();
// 0x0000094D System.Void SEManager::StopSE(System.String)
extern void SEManager_StopSE_m51B10B9FAE939551A67B1AA5A492AC8D2DE0DAA8 ();
// 0x0000094E System.Boolean SEManager::IsBGM()
extern void SEManager_IsBGM_m89B8B8D61B94CC299777B5F5C63163CC2BFB936C ();
// 0x0000094F System.Void SEManager::.ctor()
extern void SEManager__ctor_m0B0B3BC62B20F730F107D8077DE8E4D599C2D05C ();
// 0x00000950 System.Void MainManager::CreateSelf()
extern void MainManager_CreateSelf_m51193E44C71285D3FAEB1BA5805266F4CE1CA134 ();
// 0x00000951 System.Void MainManager::Init()
extern void MainManager_Init_m6E5EEBFA52A1B1047E4334CA6309466989978A21 ();
// 0x00000952 System.Void MainManager::OnApplicationPause(System.Boolean)
extern void MainManager_OnApplicationPause_m00338ED42A79459BDD5451E1616938FABD6E4DDE ();
// 0x00000953 System.Void MainManager::OnApplicationQuit()
extern void MainManager_OnApplicationQuit_m49D5C2DFAE25C720454FBE9B356DC2F4C0928388 ();
// 0x00000954 System.Void MainManager::.ctor()
extern void MainManager__ctor_m41EEBE8ED32EF698400725FD0344F93D3FC70117 ();
// 0x00000955 System.String SceneNavigator::get_BeforeSceneName()
extern void SceneNavigator_get_BeforeSceneName_mD4E83345A2DCFAC65C9DF62B726C12F1D3E850A6 ();
// 0x00000956 System.String SceneNavigator::get_CurrentSceneName()
extern void SceneNavigator_get_CurrentSceneName_mDBCFC17B3D4277AEAA573994DFADC19283F43A81 ();
// 0x00000957 System.Int32 SceneNavigator::get_CurrentSceneNo()
extern void SceneNavigator_get_CurrentSceneNo_m38D993D52DB3CE8F86AB7783C61BECDBB7B849A1 ();
// 0x00000958 System.Void SceneNavigator::add_OnLoadedScene(System.Action`1<System.Int32>)
extern void SceneNavigator_add_OnLoadedScene_m5643662BB51F97DF2BF927497D9908E265EDDFC2 ();
// 0x00000959 System.Void SceneNavigator::remove_OnLoadedScene(System.Action`1<System.Int32>)
extern void SceneNavigator_remove_OnLoadedScene_m60DC1657F4EB9813CDE4557ED533761292288EB6 ();
// 0x0000095A System.Void SceneNavigator::Init()
extern void SceneNavigator_Init_mFB97107DE30611B7228A836DF56AB344798BCCE9 ();
// 0x0000095B System.Void SceneNavigator::AddScene(System.String)
extern void SceneNavigator_AddScene_mAD3157F3EE53EB4404B4FBD0892E7AA9996C3BED ();
// 0x0000095C System.Void SceneNavigator::LoadScene(System.String)
extern void SceneNavigator_LoadScene_m5DE57772F6E2DCC337495BAF1A014F9B6F6C92EA ();
// 0x0000095D System.Void SceneNavigator::OnSceneLoaded(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode)
extern void SceneNavigator_OnSceneLoaded_m6099A84DE5E26C1CE563FF58F1A1109D450EA516 ();
// 0x0000095E System.Void SceneNavigator::.ctor()
extern void SceneNavigator__ctor_mBCD4DF454720F3510C9C327E5A5F34FED30D4020 ();
// 0x0000095F System.Void AdManager::add_RewardVideoComplete(System.Action`1<System.Boolean>)
extern void AdManager_add_RewardVideoComplete_m05A39C9D8E6709A71BAFBEC1A9A20DE4B1362F7D ();
// 0x00000960 System.Void AdManager::remove_RewardVideoComplete(System.Action`1<System.Boolean>)
extern void AdManager_remove_RewardVideoComplete_mC4E26DCBBA9BC267486015E804A31B3D700C1EB2 ();
// 0x00000961 System.Void AdManager::add_InterAdClose(System.Action)
extern void AdManager_add_InterAdClose_m5CAE519E5750565E5171EA1B3D836DFF16850CDD ();
// 0x00000962 System.Void AdManager::remove_InterAdClose(System.Action)
extern void AdManager_remove_InterAdClose_m85260138B22E1DB1D3B396C89B9521C8F3BF3CC9 ();
// 0x00000963 System.Void AdManager::add_RewardVideoLoaded(System.Action)
extern void AdManager_add_RewardVideoLoaded_m199CB8109649EC2576E9C8A6FDE14E09E233DC58 ();
// 0x00000964 System.Void AdManager::remove_RewardVideoLoaded(System.Action)
extern void AdManager_remove_RewardVideoLoaded_m4ECAA93BABD4160631DC8765D2E865590DCC37C6 ();
// 0x00000965 System.Void AdManager::Init()
extern void AdManager_Init_mF961FAE54D806D83B6B4320DAAA450B614CD91DD ();
// 0x00000966 System.Void AdManager::Start()
extern void AdManager_Start_m7441282F4D11035BC4FA1D580553465DD9A85700 ();
// 0x00000967 System.Void AdManager::OnInitNative(System.String)
extern void AdManager_OnInitNative_m0D10503429E68D076BB383AD826DC4CF84A02580 ();
// 0x00000968 System.Void AdManager::ShowSplashAd()
extern void AdManager_ShowSplashAd_m861478C3F4328F75937186C97DE1C2D03128E05C ();
// 0x00000969 System.Void AdManager::ShowRewardVideo()
extern void AdManager_ShowRewardVideo_m51A61E538B1F4390C274337F8F64B90E9DFC72CC ();
// 0x0000096A System.Boolean AdManager::CheckCanShowVideo()
extern void AdManager_CheckCanShowVideo_m5D29A69F04802F93D845539869D2D867597D1F4C ();
// 0x0000096B System.Void AdManager::HideAllAds()
extern void AdManager_HideAllAds_m152C3B7FF45FC3001EFC020788ADEAC3B0AFB053 ();
// 0x0000096C System.Void AdManager::PauseSound()
extern void AdManager_PauseSound_m9B04A4BA822470BC6F88CED03FB72BDAD843FEC9 ();
// 0x0000096D System.Void AdManager::ResumeSound()
extern void AdManager_ResumeSound_mF4CFB17DCFE56AE5055C4B753851B4B19E1EAF69 ();
// 0x0000096E System.Void AdManager::FinishedPlayingMovieAd(System.String)
extern void AdManager_FinishedPlayingMovieAd_m14BD97831D2851AED3262B2BFFBE6D57F7B0B0C8 ();
// 0x0000096F System.Void AdManager::FinishInterstial()
extern void AdManager_FinishInterstial_m9652413A768F30AA66FE525E82611310C1A2F30D ();
// 0x00000970 System.Void AdManager::FinishLoadRewardVideo()
extern void AdManager_FinishLoadRewardVideo_m071431FD7FB35AE6E79F72B73A80A9ED9AB60D49 ();
// 0x00000971 System.Void AdManager::CheatReward()
extern void AdManager_CheatReward_m406D37B4A02FAAA85267F07CABA98ADF2E823947 ();
// 0x00000972 System.Void AdManager::CheatInter()
extern void AdManager_CheatInter_mE75DD3A3EC8554F9FEEE48DBA8855060A8C65113 ();
// 0x00000973 System.Void AdManager::.ctor()
extern void AdManager__ctor_m90E4F3ECE5164648C36A623707AF2B9F67A9C6E6 ();
// 0x00000974 System.Void AdManager::.cctor()
extern void AdManager__cctor_m4DFC56B7E5F3E5582B1DAE04280A04606712C903 ();
// 0x00000975 System.Void AdPlugin::Init()
extern void AdPlugin_Init_m752585F910DEA9849D1421C38C50D60861FBDF8E ();
// 0x00000976 System.IntPtr AdPlugin::_AdPlugin_Init(System.String)
extern void AdPlugin__AdPlugin_Init_m51DB5B47350C98AF44ED3D4EFB38276C495A39FC ();
// 0x00000977 System.Void AdPlugin::HideAllAds()
extern void AdPlugin_HideAllAds_mAE43C52316E5939E9A8D8C588494D123A280B9A4 ();
// 0x00000978 System.Void AdPlugin::_HideAllAds(System.IntPtr)
extern void AdPlugin__HideAllAds_m5AA7AAF9F9BBF32A4AF22290AB1205A630C6710C ();
// 0x00000979 System.Void AdPlugin::ShowSplashAd(System.Int32)
extern void AdPlugin_ShowSplashAd_m5E41A401B0730386C62378F7F2C4CCBD2E3FA23F ();
// 0x0000097A System.Void AdPlugin::_ShowSplashAd(System.IntPtr,System.Int32)
extern void AdPlugin__ShowSplashAd_mF9C6034F9B9EB0AD4C88FAFDD6AA5C42356AFD7A ();
// 0x0000097B System.Void AdPlugin::ShowRewardVideo()
extern void AdPlugin_ShowRewardVideo_m8293703092631CAA565E8FFDF9951CDE329B9A55 ();
// 0x0000097C System.Void AdPlugin::_ShowRewardVideo(System.IntPtr)
extern void AdPlugin__ShowRewardVideo_m7C79A35A2C01CD0C2C85D080226D1741DCD215E0 ();
// 0x0000097D System.Boolean AdPlugin::CanShowRewardVideo()
extern void AdPlugin_CanShowRewardVideo_mC32B73A1E4E1C171F3BA6D0951463E2B34BD1996 ();
// 0x0000097E System.Boolean AdPlugin::_CanShowRewardVideo(System.IntPtr)
extern void AdPlugin__CanShowRewardVideo_m930EC48A4E3C8820C3F67816CF8F303FAF769166 ();
// 0x0000097F System.Void AdPlugin::.ctor()
extern void AdPlugin__ctor_m8BF0200AD18087C2F772D7A4C095B4392359F8F4 ();
// 0x00000980 System.Void RewardButtonAssistant::Start()
extern void RewardButtonAssistant_Start_m6B52E7E49230C197E6F167A239673AA0C53A5B28 ();
// 0x00000981 System.Void RewardButtonAssistant::OnEnable()
extern void RewardButtonAssistant_OnEnable_mE786CC629D73E6F082E5D8DC4CF17F019B26C95E ();
// 0x00000982 System.Void RewardButtonAssistant::OnDisable()
extern void RewardButtonAssistant_OnDisable_m6907A329E6BECA37A96AD02B934D2AB25B5F7765 ();
// 0x00000983 System.Void RewardButtonAssistant::OnVideoLoaded()
extern void RewardButtonAssistant_OnVideoLoaded_m406B548F6BC2DD5DF0ED1ECC1EDA0649D9CCBFD2 ();
// 0x00000984 System.Void RewardButtonAssistant::EnableMe(System.Boolean)
extern void RewardButtonAssistant_EnableMe_mDE30B9A29CD6D00C00472A0C97E66307DCE43914 ();
// 0x00000985 System.Void RewardButtonAssistant::ShowAdVideo()
extern void RewardButtonAssistant_ShowAdVideo_m7846FF95961363D10A9937515C645CC340FD46CD ();
// 0x00000986 System.Void RewardButtonAssistant::AdManager_RewardVideoComplete(System.Boolean)
extern void RewardButtonAssistant_AdManager_RewardVideoComplete_m66A9D01BA20E1C34441B56E814856422908B896B ();
// 0x00000987 System.Void RewardButtonAssistant::OnVideoSuccess(System.Boolean)
extern void RewardButtonAssistant_OnVideoSuccess_mBD6D849463CD64900B4BC6A5A43DDB69D9EAFA86 ();
// 0x00000988 System.Void RewardButtonAssistant::.ctor()
extern void RewardButtonAssistant__ctor_mCB845C4185E8175C819020669491DD6BE40C4D14 ();
// 0x00000989 System.Void RoulletRewardVideoButton::OnVideoSuccess(System.Boolean)
extern void RoulletRewardVideoButton_OnVideoSuccess_m21DD6274EEC9828A7B860ABC66140EBC41AAA4DD ();
// 0x0000098A System.Void RoulletRewardVideoButton::OnClick()
extern void RoulletRewardVideoButton_OnClick_m31D7356A9F5CB3E4AFF615FD62891354E6AD94A7 ();
// 0x0000098B System.Void RoulletRewardVideoButton::.ctor()
extern void RoulletRewardVideoButton__ctor_mC956F3E0D67F84A9D9BEA9187299C82E27961E59 ();
// 0x0000098C System.Void CommonNativeManager::Init()
extern void CommonNativeManager_Init_m4F1720A646FA6254529A0700C3503506B636F19A ();
// 0x0000098D System.Void CommonNativeManager::ShowAppStoreInApp(System.Int32)
extern void CommonNativeManager_ShowAppStoreInApp_m32E357CF07D9F9D26EADF6DE2147B733A2BF7ACA ();
// 0x0000098E System.Void CommonNativeManager::ShowReviewPopUp()
extern void CommonNativeManager_ShowReviewPopUp_m0A1D3FE957632FDDDB6432D33D7CD3A51645811A ();
// 0x0000098F System.Void CommonNativeManager::ReportAnalytic(System.String)
extern void CommonNativeManager_ReportAnalytic_mB8DE382F75D8A141BD6FF10F08F6835AADBCD83E ();
// 0x00000990 System.Void CommonNativeManager::.ctor()
extern void CommonNativeManager__ctor_m3CAFCAA4893552FA8247A901353F8BD4E3B72B46 ();
// 0x00000991 System.Void CommonNativePlugin::Init()
extern void CommonNativePlugin_Init_m75DC34CC89116BBE5F25313382B610F2119992C1 ();
// 0x00000992 System.IntPtr CommonNativePlugin::_CommonNativePlugin_Init(System.String)
extern void CommonNativePlugin__CommonNativePlugin_Init_m84A7F37CF254F180F61EC109AB3862EE93A557A1 ();
// 0x00000993 System.Void CommonNativePlugin::ShowAppStoreInApp(System.Int32)
extern void CommonNativePlugin_ShowAppStoreInApp_m04D03ABF4209743369680874FEA6CF6D54ADE597 ();
// 0x00000994 System.Void CommonNativePlugin::_ShowAppStoreInApp(System.IntPtr,System.Int32)
extern void CommonNativePlugin__ShowAppStoreInApp_mE58B88797EA171279FDEF890C17A523012594F22 ();
// 0x00000995 System.Void CommonNativePlugin::ReportToFlurry(System.String)
extern void CommonNativePlugin_ReportToFlurry_mBC7F88974E3C5CECAA8394BBCCC3688DF6F0E4D1 ();
// 0x00000996 System.Void CommonNativePlugin::_ReportToFlurry(System.IntPtr,System.String)
extern void CommonNativePlugin__ReportToFlurry_m7C34123BDD614B7E5A505107F5041B9B9080E89B ();
// 0x00000997 System.Void CommonNativePlugin::ShowReviewPopUp()
extern void CommonNativePlugin_ShowReviewPopUp_m1B1020527635BCED7DD1069578A75D5734281783 ();
// 0x00000998 System.Void CommonNativePlugin::_ShowReviewPopUp(System.IntPtr)
extern void CommonNativePlugin__ShowReviewPopUp_m7B78FF8E439D281303749EF2497E3CC0BBA757F8 ();
// 0x00000999 System.Void CommonNativePlugin::ReportToFacebookAnalytic(System.String)
extern void CommonNativePlugin_ReportToFacebookAnalytic_mBEBF2E0E00C7DFCC229D0FDB59AB0A7F703A3995 ();
// 0x0000099A System.Void CommonNativePlugin::_ReportToFacebookAnalytic(System.IntPtr,System.String)
extern void CommonNativePlugin__ReportToFacebookAnalytic_m317C9D05D0CF41259C4C954D977634844A742CE5 ();
// 0x0000099B System.Void CommonNativePlugin::.ctor()
extern void CommonNativePlugin__ctor_m47C212DC119A8569ACBD531526CA8C364C20F793 ();
// 0x0000099C System.Void RankingManager::Init()
extern void RankingManager_Init_m5EC7449E02E2FD668EC94CF0BED922A9B3AA1C1F ();
// 0x0000099D System.Void RankingManager::ShowLeaderBoard()
extern void RankingManager_ShowLeaderBoard_m01CBD294823CFC4B705EB70484867C38C17EE1CC ();
// 0x0000099E System.Void RankingManager::ReportScore(System.Int32,System.Int32)
extern void RankingManager_ReportScore_m6DE982F4E24A242E94A9F9E1D4FFE11793484713 ();
// 0x0000099F System.Void RankingManager::ReportScore(System.Int32,System.Single)
extern void RankingManager_ReportScore_mBA6A65A1C3675A2792DD155F2C7CD0D29187F841 ();
// 0x000009A0 System.Void RankingManager::ReportScore(System.Int32)
extern void RankingManager_ReportScore_m7CB52E0F8809B1DBBCEFDAFD6C2B34EF417ACFEF ();
// 0x000009A1 System.Void RankingManager::ReportScore(System.Single)
extern void RankingManager_ReportScore_m878612867B2CBEECC9001D478BCC062B1255520F ();
// 0x000009A2 System.Void RankingManager::ReportScore(System.Decimal)
extern void RankingManager_ReportScore_m68718056D6B52FC542389FAAF23FD4B01E3E981C ();
// 0x000009A3 System.Void RankingManager::.ctor()
extern void RankingManager__ctor_mAA4BC3F32513E377363347CF0D86ED281D2B7D6D ();
// 0x000009A4 System.Void RankingPlugin::Init()
extern void RankingPlugin_Init_m71B3B18FCE6FF1BF2F444DCDE4563FA66265C522 ();
// 0x000009A5 System.IntPtr RankingPlugin::_RankingPlugin_Init(System.String)
extern void RankingPlugin__RankingPlugin_Init_m05DE1302F06520CF85C476ECB94904331CC89750 ();
// 0x000009A6 System.Void RankingPlugin::ShowLeaderBoard()
extern void RankingPlugin_ShowLeaderBoard_mC9EC717598E6031E5B8A5492E3228C7761073C7A ();
// 0x000009A7 System.Void RankingPlugin::_ShowLeaderBoard(System.IntPtr)
extern void RankingPlugin__ShowLeaderBoard_mF61F05FF2039421326C1FBAEE289B2286440E775 ();
// 0x000009A8 System.Void RankingPlugin::ReportScore(System.Int32,System.Int32)
extern void RankingPlugin_ReportScore_mAA3DD2B822D74A07D28810C44E28F0508BAB55CF ();
// 0x000009A9 System.Void RankingPlugin::_RankingReportIntScore(System.IntPtr,System.Int32,System.Int32)
extern void RankingPlugin__RankingReportIntScore_m0C0A66D870B44ED9501F64507BB3F8DC99CA449F ();
// 0x000009AA System.Void RankingPlugin::ReportScore(System.Int32,System.Single)
extern void RankingPlugin_ReportScore_mF707F7E3C7C1B1B3654631BBF7D8C65A48F57366 ();
// 0x000009AB System.Void RankingPlugin::_RankingReportFloatScore(System.IntPtr,System.Int32,System.Single)
extern void RankingPlugin__RankingReportFloatScore_mE2F6825DE6D2E86185DFAC8CF7185268E0097B35 ();
// 0x000009AC System.Void RankingPlugin::.ctor()
extern void RankingPlugin__ctor_m0BD1FEF6C6E52B84860F01CFA24D9A08CA21D98C ();
// 0x000009AD System.Void ShareManager::Init()
extern void ShareManager_Init_m88FB9B04E9C2C71B20FB00F6097EC315DC36553E ();
// 0x000009AE System.Void ShareManager::Tweet(System.String)
extern void ShareManager_Tweet_m4C90723F79CAB083B4DF93F90410873EF6C28577 ();
// 0x000009AF System.Void ShareManager::.ctor()
extern void ShareManager__ctor_m4E0B6763E02CF8395BBD771B583B6E4E629B9A84 ();
// 0x000009B0 System.Void SharePlugin::Init()
extern void SharePlugin_Init_m82F2975B9F2826BA30249E6856BB7677798CFC95 ();
// 0x000009B1 System.IntPtr SharePlugin::_SharePlugin_Init(System.String)
extern void SharePlugin__SharePlugin_Init_m3C0E3F1DE350C41B431D836E4B35D3DF94D3EF9F ();
// 0x000009B2 System.Void SharePlugin::Tweet(System.String,System.String)
extern void SharePlugin_Tweet_m8683523EBD3F90847945287803C8B5088A9C4C34 ();
// 0x000009B3 System.Void SharePlugin::DoTweet(System.String,System.String)
extern void SharePlugin_DoTweet_m3FBEA0D29070E7CDB3748553BCE68AC9EF8F35BD ();
// 0x000009B4 System.Void SharePlugin::_Tweet(System.IntPtr,System.String,System.String)
extern void SharePlugin__Tweet_m8EC2C79A2BF72AE8E45DE87857B7CF22F6C4D4BF ();
// 0x000009B5 System.Void SharePlugin::.ctor()
extern void SharePlugin__ctor_m2E5CCF92E0F66658C93AA51A6E08D9C1D97A9E67 ();
// 0x000009B6 System.Void ButtonAssistant::add_onClick(System.Action)
extern void ButtonAssistant_add_onClick_m40CF358E70883C77DFD994919C04DA08C0F47512 ();
// 0x000009B7 System.Void ButtonAssistant::remove_onClick(System.Action)
extern void ButtonAssistant_remove_onClick_m121E4E4909472103470168F90F27603445D03B02 ();
// 0x000009B8 System.Void ButtonAssistant::Awake()
extern void ButtonAssistant_Awake_mB14F3471BE5F98CBC8A5D2E59E70D5CA21E46664 ();
// 0x000009B9 System.Void ButtonAssistant::OnClick()
extern void ButtonAssistant_OnClick_m0D587DBDAE914A810035623EAD70CCF7E4CA4A4D ();
// 0x000009BA System.Void ButtonAssistant::UpdateButtonSprite(System.String)
extern void ButtonAssistant_UpdateButtonSprite_m24A4C995BB242F5B968F2D3B50F1EDC3400C74AB ();
// 0x000009BB System.Void ButtonAssistant::.ctor()
extern void ButtonAssistant__ctor_mA7060CEBAE249832E13D59CC8AFC2209F18E3DA9 ();
// 0x000009BC System.Void TweetButtonAssistant::OnClick()
extern void TweetButtonAssistant_OnClick_m8BA84509E43A9384F51CF8C242976A55F3169C37 ();
// 0x000009BD System.Void TweetButtonAssistant::.ctor()
extern void TweetButtonAssistant__ctor_m61B87B3BCC260B0869B70AA8925659A4A60F0472 ();
// 0x000009BE System.Void PauseButtonAssistant::OnApplicationPause(System.Boolean)
extern void PauseButtonAssistant_OnApplicationPause_mE498C2AC669ED9C590CA5E0DE62ECE452C769CE3 ();
// 0x000009BF System.Void PauseButtonAssistant::OnClick()
extern void PauseButtonAssistant_OnClick_mC21A4BC9B02C729DBFD3F6A0793F156DDFD50B86 ();
// 0x000009C0 System.Void PauseButtonAssistant::ChangeState(System.Boolean)
extern void PauseButtonAssistant_ChangeState_mB77734BDAC44524ECC3E8A36FB69273EA1675158 ();
// 0x000009C1 System.Void PauseButtonAssistant::.ctor()
extern void PauseButtonAssistant__ctor_m575F6CCBAB5C254579EFA52BC4A94E4A0F3DE7DF ();
// 0x000009C2 System.Void ResumeButtonAssistant::OnClick()
extern void ResumeButtonAssistant_OnClick_m69BAE0599B3E7138C0F37ECADF6F1464BCEF52B7 ();
// 0x000009C3 System.Void ResumeButtonAssistant::ChangeState(System.Boolean)
extern void ResumeButtonAssistant_ChangeState_mA8CEC3EFFA8FFA86814182440AB28027868E615F ();
// 0x000009C4 System.Void ResumeButtonAssistant::.ctor()
extern void ResumeButtonAssistant__ctor_mF303399F7EC407A917A920325C94FA040B4FC6B7 ();
// 0x000009C5 System.Void RankingButtonAssistant::OnClick()
extern void RankingButtonAssistant_OnClick_m4375D04033BBC9FBE81CA7E4E69984EFB331BEBA ();
// 0x000009C6 System.Void RankingButtonAssistant::.ctor()
extern void RankingButtonAssistant__ctor_mC3B5AE1679C102A3C14BA89DB6704A78420E88E2 ();
// 0x000009C7 System.Void EnumUtility::ExcuteActionInAllValue(System.Action`1<T>)
// 0x000009C8 System.Boolean EnumUtility::ContainsKey(System.String)
// 0x000009C9 T EnumUtility::KeyToType(System.String)
// 0x000009CA T EnumUtility::NoToType(System.Int32)
// 0x000009CB System.Int32 EnumUtility::GetTypeNum()
// 0x000009CC System.Void PlayerPrefsUtility::Save(System.String,System.Int32)
extern void PlayerPrefsUtility_Save_mCC0F4EB58D72ABD71ED3124E85320709ACD2DC51 ();
// 0x000009CD System.Void PlayerPrefsUtility::Save(System.String,System.Single)
extern void PlayerPrefsUtility_Save_mD435BE54E2B75AA200F1C3071BFD51B8E0E43EC0 ();
// 0x000009CE System.Void PlayerPrefsUtility::Save(System.String,System.String)
extern void PlayerPrefsUtility_Save_m2EAF032F789A9F05A01B8ED0026DE93505C75836 ();
// 0x000009CF System.Void PlayerPrefsUtility::Save(System.String,System.Decimal)
extern void PlayerPrefsUtility_Save_mE65DA94E6B06E81229BBF1F38124463F145CA6C1 ();
// 0x000009D0 System.Void PlayerPrefsUtility::Save(System.String,System.DateTime)
extern void PlayerPrefsUtility_Save_mFE85F068E41FD93A0E75BD1C78C9DDFCD49325AE ();
// 0x000009D1 System.Void PlayerPrefsUtility::Save(System.String,System.Boolean)
extern void PlayerPrefsUtility_Save_mC9977D97D73804DA695272755C3349DFFD2ED287 ();
// 0x000009D2 System.Void PlayerPrefsUtility::SaveList(System.String,System.Collections.Generic.List`1<Value>)
// 0x000009D3 System.Void PlayerPrefsUtility::SaveDict(System.String,System.Collections.Generic.Dictionary`2<Key,Value>)
// 0x000009D4 System.Int32 PlayerPrefsUtility::Load(System.String,System.Int32)
extern void PlayerPrefsUtility_Load_mDB08B958BCC8BD3CE0625625AF75CC5B12E696B6 ();
// 0x000009D5 System.Single PlayerPrefsUtility::Load(System.String,System.Single)
extern void PlayerPrefsUtility_Load_m26A0CB1973CBA9973DF290AF5576BA646E4CDAD6 ();
// 0x000009D6 System.String PlayerPrefsUtility::Load(System.String,System.String)
extern void PlayerPrefsUtility_Load_m0950B0A040DEA9883581F29FFA914E4E882550D1 ();
// 0x000009D7 System.Decimal PlayerPrefsUtility::Load(System.String,System.Decimal)
extern void PlayerPrefsUtility_Load_m4B5374CF26650422842F925AC37DFBF0DDD54F3A ();
// 0x000009D8 System.DateTime PlayerPrefsUtility::Load(System.String,System.DateTime)
extern void PlayerPrefsUtility_Load_m02514577227259CDB7912131390159CB07B23C25 ();
// 0x000009D9 System.Boolean PlayerPrefsUtility::Load(System.String,System.Boolean)
extern void PlayerPrefsUtility_Load_mB5409CA68E682FF1DD1C1C2302C7A2A6DE6A8B25 ();
// 0x000009DA System.Collections.Generic.List`1<Value> PlayerPrefsUtility::LoadList(System.String)
// 0x000009DB System.Collections.Generic.Dictionary`2<Key,Value> PlayerPrefsUtility::LoadDict(System.String)
// 0x000009DC System.String PlayerPrefsUtility::Serialize(ObjectType)
// 0x000009DD ObjectType PlayerPrefsUtility::Deserialize(System.String)
// 0x000009DE System.Boolean ProbabilityCalclator::DetectFromPercent(System.Int32)
extern void ProbabilityCalclator_DetectFromPercent_mA2655B6ACA216B916B91E48F150CE8BD46C4F88F ();
// 0x000009DF System.Boolean ProbabilityCalclator::DetectFromPercent(System.Single)
extern void ProbabilityCalclator_DetectFromPercent_mFE89B9DEA2F4D3BCC0165B04FDC3F7FEC998C4FA ();
// 0x000009E0 T ProbabilityCalclator::DetermineFromDict(System.Collections.Generic.Dictionary`2<T,System.Int32>)
// 0x000009E1 T ProbabilityCalclator::DetermineFromDict(System.Collections.Generic.Dictionary`2<T,System.Single>)
// 0x000009E2 System.String StringUtility::GenerateRandomString(System.Int32)
extern void StringUtility_GenerateRandomString_m353019E51CE6D26BBAF1EF250847C96CE03759EF ();
// 0x000009E3 System.Single VectorCalculator::GetAim(UnityEngine.Vector2,UnityEngine.Vector2)
extern void VectorCalculator_GetAim_m6FDA6754D9110834AF6457A9A02BF51EC0F45B8C ();
// 0x000009E4 UnityEngine.Vector3 VectorCalculator::GetDirection(UnityEngine.Vector3,UnityEngine.Vector3)
extern void VectorCalculator_GetDirection_m6F98E62F7CBADC50BBB4AC0B4933B2E83AAAA7D8 ();
// 0x000009E5 System.Void SettingButton::OnClick()
extern void SettingButton_OnClick_m360D5192348E45D50012C8113E70DD53C8B8AA7A ();
// 0x000009E6 System.Void SettingButton::.ctor()
extern void SettingButton__ctor_m617BAB41C2DD80C41049556A7E1F2F0200F59CD3 ();
// 0x000009E7 System.Void SoundButton::OnEnable()
extern void SoundButton_OnEnable_m83EC81C2B67F80EB276590EAD4FBCDC1F880AFC5 ();
// 0x000009E8 System.Void SoundButton::OnClick()
extern void SoundButton_OnClick_m345BA9DC10C775DE0DDCFE2F2924015FE44C9BD2 ();
// 0x000009E9 System.Void SoundButton::UpdateInfo()
extern void SoundButton_UpdateInfo_m441B83D172CA5B3757AC3EC12F0DAE7ECEAF6896 ();
// 0x000009EA System.Void SoundButton::.ctor()
extern void SoundButton__ctor_m2CAD2082286F7CDA27208642B996B689FC1C4D10 ();
// 0x000009EB System.Void Stage_6::OnEnable()
extern void Stage_6_OnEnable_m4ADE4BAE059A229E3633678B3EB496B70AB14CE1 ();
// 0x000009EC System.Void Stage_6::Start()
extern void Stage_6_Start_m7AEAFB0F490DF49AE6E4F350D5AC639DC1979882 ();
// 0x000009ED System.Void Stage_6::OnDisable()
extern void Stage_6_OnDisable_mBB91AB738038C7C9E7625391A72872C108642DC8 ();
// 0x000009EE System.Void Stage_6::OnDead()
extern void Stage_6_OnDead_m44156106909541F6E841444674550EEB52E9051A ();
// 0x000009EF System.Void Stage_6::.ctor()
extern void Stage_6__ctor_m644C1B73571989265B59EF6D4C23C54583631DEA ();
// 0x000009F0 System.Void Stage_6::<Start>b__5_0()
extern void Stage_6_U3CStartU3Eb__5_0_mDC1F889EB9636C94E065E01A8F7B9754ECFC9BDF ();
// 0x000009F1 System.Void Stage_6::<Start>b__5_1()
extern void Stage_6_U3CStartU3Eb__5_1_mD65ACB18D198A8446F33FBAADCA06F66327C56CE ();
// 0x000009F2 System.Void Stage_6::<Start>b__5_2()
extern void Stage_6_U3CStartU3Eb__5_2_m4E3A9159F65D3815E69D3E5498E8012038883BB3 ();
// 0x000009F3 System.Void StopGrenadeSurface::OnCollisionEnter(UnityEngine.Collision)
extern void StopGrenadeSurface_OnCollisionEnter_mF1B8AD819A895F1C6B2BA2E3A4E5AF242F15434D ();
// 0x000009F4 System.Void StopGrenadeSurface::.ctor()
extern void StopGrenadeSurface__ctor_mD9549A2BD48A7B62CDD8669C4D8780EF2ED3AB7B ();
// 0x000009F5 System.Void TouchController::OnEnable()
extern void TouchController_OnEnable_m0B31690352B996A4D7F7D60E9FF9FCF228A44174 ();
// 0x000009F6 System.Void TouchController::Update()
extern void TouchController_Update_mBB75A99965FF13B43A29007142FBBD550EC55990 ();
// 0x000009F7 UnityEngine.Vector3 TouchController::VelocityByTime(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void TouchController_VelocityByTime_mD0F5962545DF34D0E55C88177E1658820C3CADEC ();
// 0x000009F8 System.Void TouchController::Fire()
extern void TouchController_Fire_m428BF15D21666D0DBEB72A963916A2568A59D1C2 ();
// 0x000009F9 System.Void TouchController::Visualize(UnityEngine.Vector3)
extern void TouchController_Visualize_mDC1612FC7358DD030CD4781FACC6277EE66D4D7A ();
// 0x000009FA UnityEngine.Vector3 TouchController::PositionByTime(UnityEngine.Vector3,System.Single)
extern void TouchController_PositionByTime_mEE55B92FCFB2C558889CEF66DA411FCAA292A1CF ();
// 0x000009FB System.Void TouchController::.ctor()
extern void TouchController__ctor_m4FB7729B61339E9534D4D3AA5465226613198DE2 ();
// 0x000009FC System.Void TrajectoryController::set_MainPlayer(Player)
extern void TrajectoryController_set_MainPlayer_m7B412AE702CEA4E39548262B710E2DB4CF1F4D2F ();
// 0x000009FD Player TrajectoryController::get_MainPlayer()
extern void TrajectoryController_get_MainPlayer_m09A12046B5EE7C46592DB72569208A3EEDBBE3D7 ();
// 0x000009FE System.Void TrajectoryController::Start()
extern void TrajectoryController_Start_m0716D4EEBE0AE1C79E26F127FD26099AE51CFBBC ();
// 0x000009FF System.Void TrajectoryController::Update()
extern void TrajectoryController_Update_m2DEB584A824C9B5DA19448AC712FC857D33A06D2 ();
// 0x00000A00 System.Void TrajectoryController::OnDrawGizmos()
extern void TrajectoryController_OnDrawGizmos_mEDEACE692FAA06E14B283CC677BB28E14E85CDEF ();
// 0x00000A01 UnityEngine.Vector3 TrajectoryController::CalculatePosition(System.Single,UnityEngine.Vector3)
extern void TrajectoryController_CalculatePosition_mC036B0675163DF03F2941A2CF3E4EC0A8399AFBF ();
// 0x00000A02 System.Void TrajectoryController::HideAll()
extern void TrajectoryController_HideAll_mF29D6A279581E317FBE3B41561078AF2A14C4EB0 ();
// 0x00000A03 System.Void TrajectoryController::OnDestroy()
extern void TrajectoryController_OnDestroy_m8C0060BC6F2B545ED5F06BD034B0B51EF09A3ED6 ();
// 0x00000A04 System.Void TrajectoryController::.ctor()
extern void TrajectoryController__ctor_m626C3CCA0CCFE4633CB804D1283547F56255E16F ();
// 0x00000A05 System.Void TrajectoryController::<Update>b__19_0()
extern void TrajectoryController_U3CUpdateU3Eb__19_0_mFBDB4B19F7026B0339E5C3C77C4CD1351B273D80 ();
// 0x00000A06 System.Void UIController::OnEnable()
extern void UIController_OnEnable_mEF53A9A85AC08648D9F21E06D4C1AF2B258CAB43 ();
// 0x00000A07 System.Void UIController::GameClear()
extern void UIController_GameClear_m87512BC0B3FCD16B7A6838CF0A90C10F0068D969 ();
// 0x00000A08 System.Void UIController::GameOver()
extern void UIController_GameOver_m4F35441A3CC06BAEA4CAA9B614BA0B99D14B80D9 ();
// 0x00000A09 System.Void UIController::HideHelp()
extern void UIController_HideHelp_m7FCE2186067E8F3DD336D042243E2F404BF342CE ();
// 0x00000A0A System.Void UIController::Setting(System.Boolean)
extern void UIController_Setting_m4F2FAF331D55C0C12C65B86106D1B002762F858F ();
// 0x00000A0B System.Void UIController::StartGame()
extern void UIController_StartGame_m057AFC20C0578F33E5186141A21ABC2BBE9735CE ();
// 0x00000A0C System.Void UIController::SuggestHint(System.Boolean)
extern void UIController_SuggestHint_mF0D74CD2EE48CACF2BB0ED0F7C78EE742506E784 ();
// 0x00000A0D System.Void UIController::.ctor()
extern void UIController__ctor_m2957FB6041970C1305D4DED3E11D4D800E932486 ();
// 0x00000A0E System.Void VibrateButton::OnEnable()
extern void VibrateButton_OnEnable_m10576CB0C3058E578CAA0B7DE9FAF598AF5F16D6 ();
// 0x00000A0F System.Void VibrateButton::OnClick()
extern void VibrateButton_OnClick_m441B5540B433E875A4DDEDDBD6F6682997E25836 ();
// 0x00000A10 System.Void VibrateButton::UpdateInfo()
extern void VibrateButton_UpdateInfo_m6D2F4DE2E70B9D8F6BA41BC2D4A2BFAF06857A05 ();
// 0x00000A11 System.Void VibrateButton::.ctor()
extern void VibrateButton__ctor_m04B7E701E9F25DB7AA112DD6ED3121A863CB5999 ();
// 0x00000A12 System.Void PathCreation.Examples.GeneratePathExample::Start()
extern void GeneratePathExample_Start_m33E81F38570A4476EA56587B91E27E52010F6240 ();
// 0x00000A13 System.Void PathCreation.Examples.GeneratePathExample::.ctor()
extern void GeneratePathExample__ctor_mD301C9244761369007D9A7CC88168DA0EC470FDB ();
// 0x00000A14 System.Void PathCreation.Examples.PathFollower::Start()
extern void PathFollower_Start_mF5C8D3A86E2DF1B5AF058BAD3BA109436ECBB681 ();
// 0x00000A15 System.Void PathCreation.Examples.PathFollower::Update()
extern void PathFollower_Update_mDDBD15F8E2A7B55909B7540B7A3309EBA1102DE7 ();
// 0x00000A16 System.Void PathCreation.Examples.PathFollower::OnPathChanged()
extern void PathFollower_OnPathChanged_m3E49C7A1539AC8E88899A326BE2975E358ED6B81 ();
// 0x00000A17 System.Void PathCreation.Examples.PathFollower::.ctor()
extern void PathFollower__ctor_m92B719D410345E2E4368798F8B926561FB03424F ();
// 0x00000A18 System.Void PathCreation.Examples.PathPlacer::Generate()
extern void PathPlacer_Generate_m1163867C1C742B7FEB8F377B5B22E7FFDDAA505D ();
// 0x00000A19 System.Void PathCreation.Examples.PathPlacer::DestroyObjects()
extern void PathPlacer_DestroyObjects_m9C583B7ABEF0D665FCD90D051E15303DA52889C5 ();
// 0x00000A1A System.Void PathCreation.Examples.PathPlacer::PathUpdated()
extern void PathPlacer_PathUpdated_mDA41722E8489289DCFF4A0E75CD3F3534342BAE1 ();
// 0x00000A1B System.Void PathCreation.Examples.PathPlacer::.ctor()
extern void PathPlacer__ctor_m671F8C1373871C8336B655668BFFBB6C043814D8 ();
// 0x00000A1C System.Void PathCreation.Examples.PathSceneTool::add_onDestroyed(System.Action)
extern void PathSceneTool_add_onDestroyed_m96232BE24CD7BF011934247D2A70773909D80FE2 ();
// 0x00000A1D System.Void PathCreation.Examples.PathSceneTool::remove_onDestroyed(System.Action)
extern void PathSceneTool_remove_onDestroyed_m05DD0890CFDCCB829C08E9954E6764F5CCAA7222 ();
// 0x00000A1E PathCreation.VertexPath PathCreation.Examples.PathSceneTool::get_path()
extern void PathSceneTool_get_path_m654C337CF364E561B69A6DDF02E75BCF15F647FC ();
// 0x00000A1F System.Void PathCreation.Examples.PathSceneTool::TriggerUpdate()
extern void PathSceneTool_TriggerUpdate_mF6F19868131FAC4A94E872A3EC34EE15FFE91370 ();
// 0x00000A20 System.Void PathCreation.Examples.PathSceneTool::OnDestroy()
extern void PathSceneTool_OnDestroy_mC955A014DAE41434CFCC3DD7054181CF9E84D893 ();
// 0x00000A21 System.Void PathCreation.Examples.PathSceneTool::PathUpdated()
// 0x00000A22 System.Void PathCreation.Examples.PathSceneTool::.ctor()
extern void PathSceneTool__ctor_mD66847B179189CE0B41F75B937E8737C3ED16078 ();
// 0x00000A23 System.Void PathCreation.Examples.PathSpawner::Start()
extern void PathSpawner_Start_mE2711EFC506EF8EFE60DD3060D4145A76773C728 ();
// 0x00000A24 System.Void PathCreation.Examples.PathSpawner::.ctor()
extern void PathSpawner__ctor_m271409087DB690262AC03C6DFFF75C1CF7394DD4 ();
// 0x00000A25 System.Void PathCreation.Examples.RoadMeshCreator::PathUpdated()
extern void RoadMeshCreator_PathUpdated_m6BA373E2024F3C480E697865D52920E26B5E720D ();
// 0x00000A26 System.Void PathCreation.Examples.RoadMeshCreator::CreateRoadMesh()
extern void RoadMeshCreator_CreateRoadMesh_m9588792AE7B57BB9351F5E95201CB9748092FEA2 ();
// 0x00000A27 System.Void PathCreation.Examples.RoadMeshCreator::AssignMeshComponents()
extern void RoadMeshCreator_AssignMeshComponents_mA1D24634810691912B694BCD0FB3FAE12AB17961 ();
// 0x00000A28 System.Void PathCreation.Examples.RoadMeshCreator::AssignMaterials()
extern void RoadMeshCreator_AssignMaterials_m6CA20AAE8C5770D7EC4C1E30F2E82C5F0BC09A22 ();
// 0x00000A29 System.Void PathCreation.Examples.RoadMeshCreator::.ctor()
extern void RoadMeshCreator__ctor_m3C0BAF7BA1CA92CF453EA0D32239A55F1F09F16E ();
// 0x00000A2A System.Void Exploder.Array`1::.ctor(System.Int32)
// 0x00000A2B System.Void Exploder.Array`1::Initialize(System.Int32)
// 0x00000A2C System.Int32 Exploder.Array`1::get_Count()
// 0x00000A2D T Exploder.Array`1::get_Item(System.Int32)
// 0x00000A2E System.Void Exploder.Array`1::Clear()
// 0x00000A2F System.Void Exploder.Array`1::Add(T)
// 0x00000A30 System.Void Exploder.Array`1::Reverse()
// 0x00000A31 System.Void Exploder.ArrayDictionary`1::.ctor(System.Int32)
// 0x00000A32 System.Boolean Exploder.ArrayDictionary`1::ContainsKey(System.Int32)
// 0x00000A33 T Exploder.ArrayDictionary`1::get_Item(System.Int32)
// 0x00000A34 System.Void Exploder.ArrayDictionary`1::set_Item(System.Int32,T)
// 0x00000A35 System.Void Exploder.ArrayDictionary`1::Clear()
// 0x00000A36 System.Void Exploder.ArrayDictionary`1::Add(System.Int32,T)
// 0x00000A37 System.Void Exploder.ArrayDictionary`1::Remove(System.Int32)
// 0x00000A38 T[] Exploder.ArrayDictionary`1::ToArray()
// 0x00000A39 System.Boolean Exploder.ArrayDictionary`1::TryGetValue(System.Int32,T&)
// 0x00000A3A T Exploder.ArrayDictionary`1::GetFirstValue()
// 0x00000A3B System.Void Exploder.BakeSkinManager::.ctor(Exploder.Core)
extern void BakeSkinManager__ctor_m0FF21ED366527648B2811404A5CEE18808D7C389 ();
// 0x00000A3C UnityEngine.GameObject Exploder.BakeSkinManager::CreateBakeObject(System.String)
extern void BakeSkinManager_CreateBakeObject_m2A689D5BC3B68E1B1BF91DC856B9E0ECF32042E0 ();
// 0x00000A3D System.Void Exploder.BakeSkinManager::Clear()
extern void BakeSkinManager_Clear_m16B9F2F23407D889E8D1EB4FB58ABBE3C2C75988 ();
// 0x00000A3E System.Void Exploder.Contour::.ctor(System.Int32)
extern void Contour__ctor_mB54F69A1133A23F122DA1ED8D73E14FCD60C509E ();
// 0x00000A3F System.Void Exploder.Contour::AllocateBuffers(System.Int32)
extern void Contour_AllocateBuffers_mE772213E6C7DD98226F8C8049FD037C1CCB87BF4 ();
// 0x00000A40 System.Int32 Exploder.Contour::get_MidPointsCount()
extern void Contour_get_MidPointsCount_m0181117FD61E2F23F9E27E5170500D466A1605BE ();
// 0x00000A41 System.Void Exploder.Contour::set_MidPointsCount(System.Int32)
extern void Contour_set_MidPointsCount_mFBE0E72FE4ADB59BF7D491AFE2E97F7507C9794B ();
// 0x00000A42 System.Void Exploder.Contour::AddTriangle(System.Int32,System.Int32,System.Int32,UnityEngine.Vector3,UnityEngine.Vector3)
extern void Contour_AddTriangle_m2458C4C0FE6D9882694B1A6056F0FE789022DAFC ();
// 0x00000A43 System.Boolean Exploder.Contour::FindContours()
extern void Contour_FindContours_m323DBD951E1B266B8154AC87279E4D3B0D5C8DFF ();
// 0x00000A44 System.Void Exploder.Core::Initialize(Exploder.ExploderObject)
extern void Core_Initialize_m1DAE4B9229DAB8C268565D9AF1099A2BF676E6EF ();
// 0x00000A45 System.Void Exploder.Core::Enqueue(Exploder.ExploderObject,Exploder.ExploderObject_OnExplosion,System.Boolean,UnityEngine.GameObject[])
extern void Core_Enqueue_m38D974EB4852168CB1C9A21A157507994E460D21 ();
// 0x00000A46 System.Void Exploder.Core::ExplodeCracked(UnityEngine.GameObject,Exploder.ExploderObject_OnExplosion)
extern void Core_ExplodeCracked_m55AC9CA4A83510D25316DDFE67CB8AA0EC0C5222 ();
// 0x00000A47 System.Void Exploder.Core::ExplodePartial(UnityEngine.GameObject,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,Exploder.ExploderObject_OnExplosion)
extern void Core_ExplodePartial_mCB444F1740283C4E5F97B2BCEA480C82EDAED992 ();
// 0x00000A48 System.Boolean Exploder.Core::IsCracked(UnityEngine.GameObject)
extern void Core_IsCracked_mEE10CD0BCCEDCACB03EF07FF853CCCCB92F2A840 ();
// 0x00000A49 System.Void Exploder.Core::StartExplosionFromQueue(Exploder.ExploderParams)
extern void Core_StartExplosionFromQueue_mA4F86A547177B6B8897F33D4F6D6BFAF9E26FFFF ();
// 0x00000A4A System.Void Exploder.Core::Update()
extern void Core_Update_m7B4B031EE9FE0331B89EC974D8853CA3C6FBCC81 ();
// 0x00000A4B System.Void Exploder.Core::CancelAllTask()
extern void Core_CancelAllTask_mDDB5FFB6BCBA6B23A479A92C8C357080B69DEC37 ();
// 0x00000A4C System.Void Exploder.Core::OnDestroy()
extern void Core_OnDestroy_mF74B5C4D4B2B586B26451B0743490E8C8D7F2529 ();
// 0x00000A4D System.Void Exploder.Core::PreAllocateBuffers()
extern void Core_PreAllocateBuffers_m264622206FE5D389AD14691D23173E124832407B ();
// 0x00000A4E System.Boolean Exploder.Core::RunTask(Exploder.TaskType,System.Single)
extern void Core_RunTask_mBD50AD18A5E4F05089D2B58EF51DAC3CDE6AF472 ();
// 0x00000A4F System.Void Exploder.Core::InitTask(Exploder.TaskType)
extern void Core_InitTask_m794E0640B6D0A6A5D7F2AB53A9C6460D33574B80 ();
// 0x00000A50 Exploder.TaskType Exploder.Core::NextTask(Exploder.TaskType)
extern void Core_NextTask_m569E1F528D2E8DFEEA3C3057017A28650B5542F9 ();
// 0x00000A51 System.Void Exploder.Core::.ctor()
extern void Core__ctor_m22E711443292DB500F0B1FE7B1F4DCACCB99D4A6 ();
// 0x00000A52 System.Void Exploder.CrackManager::.ctor(Exploder.Core)
extern void CrackManager__ctor_m684876B2999AA885334097A429AF781233CD4446 ();
// 0x00000A53 Exploder.CrackedObject Exploder.CrackManager::Create(UnityEngine.GameObject,Exploder.ExploderParams)
extern void CrackManager_Create_mC2E6054EDD767DE4B03CA218A4F2C4937EEDB189 ();
// 0x00000A54 System.Int64 Exploder.CrackManager::Explode(UnityEngine.GameObject)
extern void CrackManager_Explode_m28B62F13F8A563C236388047714D67E0A1333140 ();
// 0x00000A55 System.Int64 Exploder.CrackManager::ExplodePartial(UnityEngine.GameObject,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void CrackManager_ExplodePartial_mFC91E031E2967CF123A24D4DC8DE8DDBB3562830 ();
// 0x00000A56 System.Int64 Exploder.CrackManager::ExplodeAll()
extern void CrackManager_ExplodeAll_m635BA81BFE0ED1445451BC1ADFF90929F2115E90 ();
// 0x00000A57 System.Boolean Exploder.CrackManager::IsCracked(UnityEngine.GameObject)
extern void CrackManager_IsCracked_m5E11D1CB94A763E0EC119823361A39D70450A011 ();
// 0x00000A58 System.Void Exploder.CrackedObject::.ctor(UnityEngine.GameObject,Exploder.ExploderParams)
extern void CrackedObject__ctor_m5AEDBAC223501FD240B841DBEAE0DDA5E1704A98 ();
// 0x00000A59 System.Void Exploder.CrackedObject::CalculateFractureGrid()
extern void CrackedObject_CalculateFractureGrid_m08D4CB61BAC3D9DD810F107076CDA5DA5714331C ();
// 0x00000A5A System.Int64 Exploder.CrackedObject::Explode()
extern void CrackedObject_Explode_m057B1BC7B6CC4EAAD48A6AA3D9C7F022DEF2A452 ();
// 0x00000A5B System.Int64 Exploder.CrackedObject::ExplodePartial(UnityEngine.GameObject,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void CrackedObject_ExplodePartial_m5985B8A3B66270C071CD144E1C1AA0A12AEE7B83 ();
// 0x00000A5C System.Void Exploder.CuttingPlane::.ctor(Exploder.Core)
extern void CuttingPlane__ctor_mED401832EE1B6BA1075FCB5C6064105A6882CBEA ();
// 0x00000A5D Exploder.Plane Exploder.CuttingPlane::GetRandomPlane(Exploder.ExploderMesh)
extern void CuttingPlane_GetRandomPlane_m67D5161DA8F561E0EDD06A8EFA7EAF1F0DB0A336 ();
// 0x00000A5E Exploder.Plane Exploder.CuttingPlane::GetRectangularRegularPlane(Exploder.ExploderMesh,System.Int32)
extern void CuttingPlane_GetRectangularRegularPlane_m2A86A9FFEB578A8923C01E7EB9E0F96D562E4F71 ();
// 0x00000A5F Exploder.Plane Exploder.CuttingPlane::GetRectangularRandom(Exploder.ExploderMesh,System.Int32)
extern void CuttingPlane_GetRectangularRandom_mCCD394D782B6A243FDFFA0C13DBB813462BCBA62 ();
// 0x00000A60 Exploder.Plane Exploder.CuttingPlane::GetPlane(Exploder.ExploderMesh,System.Int32)
extern void CuttingPlane_GetPlane_mC312E9FF398030419C90A744863EB6E78D4809AF ();
// 0x00000A61 System.Void Exploder.CuttingPlane::.cctor()
extern void CuttingPlane__cctor_m213F4606C0706B0F31DBBE9D8F12434C78464193 ();
// 0x00000A62 System.Void Exploder.ExploderParams::.ctor(Exploder.ExploderObject)
extern void ExploderParams__ctor_m2E22653C5FE6777F863E53EEC15177ABCC73B852 ();
// 0x00000A63 System.Void Exploder.ExploderQueue::.ctor(Exploder.Core)
extern void ExploderQueue__ctor_m17C8D0DD5A5D1735936E81CAE7E48CADE461912F ();
// 0x00000A64 System.Void Exploder.ExploderQueue::Enqueue(Exploder.ExploderObject,Exploder.ExploderObject_OnExplosion,System.Boolean,UnityEngine.GameObject[])
extern void ExploderQueue_Enqueue_m24B29B587A1854D22EDBB12FE91297E9015AB7C7 ();
// 0x00000A65 System.Void Exploder.ExploderQueue::ProcessQueue()
extern void ExploderQueue_ProcessQueue_m7B3FC2973801603C5147619128A13E8AC793103C ();
// 0x00000A66 System.Void Exploder.ExploderQueue::OnExplosionFinished(System.Int32,System.Int64)
extern void ExploderQueue_OnExplosionFinished_m90CBD6A147DBBCCECF8E5780EE6E495A1926C409 ();
// 0x00000A67 System.Void Exploder.ExploderTransform::.ctor(UnityEngine.Transform)
extern void ExploderTransform__ctor_mE70677404E77066F287D912C619AD3DF39B5A962_AdjustorThunk ();
// 0x00000A68 UnityEngine.Vector3 Exploder.ExploderTransform::InverseTransformDirection(UnityEngine.Vector3)
extern void ExploderTransform_InverseTransformDirection_m37F38A988869BD7FA675EA2168E1F6605AA10F2C_AdjustorThunk ();
// 0x00000A69 UnityEngine.Vector3 Exploder.ExploderTransform::InverseTransformPoint(UnityEngine.Vector3)
extern void ExploderTransform_InverseTransformPoint_m1CE5B70D6FD357645A7857BB55CC36AAAFBE0DAC_AdjustorThunk ();
// 0x00000A6A UnityEngine.Vector3 Exploder.ExploderTransform::TransformPoint(UnityEngine.Vector3)
extern void ExploderTransform_TransformPoint_mABAF7F90A9BF3FB58C35C31055BC9D4C12A8F1BB_AdjustorThunk ();
// 0x00000A6B System.Void Exploder.FractureGrid::.ctor(Exploder.CrackedObject)
extern void FractureGrid__ctor_m56CDF579CF3A380D2925F2DD3C6E66D0093C14BC ();
// 0x00000A6C System.Void Exploder.FractureGrid::CreateGrid()
extern void FractureGrid_CreateGrid_mD775F49E1138C4F45B11C426E6F999B9EDB1A06C ();
// 0x00000A6D System.Void Exploder.LSHash::.ctor(System.Single,System.Int32)
extern void LSHash__ctor_mB54462B78C6B42CF88DC2E46EB76014859A7BAAC ();
// 0x00000A6E System.Int32 Exploder.LSHash::Capacity()
extern void LSHash_Capacity_mA118C5596EA6BBC22B9C0B05046AD96053D99372 ();
// 0x00000A6F System.Void Exploder.LSHash::Clear()
extern void LSHash_Clear_mD40787DFB6AB6DD8207C3147462676B8A5F9F725 ();
// 0x00000A70 System.Int32 Exploder.LSHash::Hash(UnityEngine.Vector3)
extern void LSHash_Hash_mE9B89C27B4B576A3ACF0B644E72AB1E5E8F27E10 ();
// 0x00000A71 System.Void Exploder.LSHash::Hash(UnityEngine.Vector3,UnityEngine.Vector3,System.Int32&,System.Int32&)
extern void LSHash_Hash_m2C361D2405D5E097ADD784AB58B615DF4BBF4D67 ();
// 0x00000A72 System.Void Exploder.ExploderMesh::.ctor()
extern void ExploderMesh__ctor_m1E2D794BC31768ABCE73E709F2C38A45956F562A ();
// 0x00000A73 System.Void Exploder.ExploderMesh::.ctor(UnityEngine.Mesh)
extern void ExploderMesh__ctor_mCF3939BC197F1FA87CD78AD1A517C7709B8BD0AD ();
// 0x00000A74 System.Void Exploder.ExploderMesh::CalculateCentroid(System.Collections.Generic.List`1<UnityEngine.Vector3>,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void ExploderMesh_CalculateCentroid_m38C69F41D9A7DD9D61B3060FB50D87C85C710878 ();
// 0x00000A75 UnityEngine.Mesh Exploder.ExploderMesh::ToUnityMesh()
extern void ExploderMesh_ToUnityMesh_m839E57AB3377A97140E3A39BD217C79012725F93 ();
// 0x00000A76 System.Void Exploder.MeshCutter::Init(System.Int32,System.Int32)
extern void MeshCutter_Init_m96905CF2C45ECF40EAB57270046DE1B09ADEF405 ();
// 0x00000A77 System.Void Exploder.MeshCutter::AllocateBuffers(System.Int32,System.Int32,System.Boolean,System.Boolean)
extern void MeshCutter_AllocateBuffers_m5944F2D0A8E700FCEE0FB19F79879FD7C0DA7E3C ();
// 0x00000A78 System.Void Exploder.MeshCutter::AllocateContours(System.Int32)
extern void MeshCutter_AllocateContours_mD684BF50A9B724DB0CFBF30BC686F068C4427E75 ();
// 0x00000A79 System.Single Exploder.MeshCutter::Cut(Exploder.ExploderMesh,Exploder.ExploderTransform,Exploder.Plane,System.Boolean,System.Boolean,System.Collections.Generic.List`1<Exploder.ExploderMesh>&,UnityEngine.Color,UnityEngine.Vector4)
extern void MeshCutter_Cut_mDCCC5E2A934D2FB3EAD5D041CBDBAE63E27E2681 ();
// 0x00000A7A System.Single Exploder.MeshCutter::Cut(Exploder.ExploderMesh,Exploder.ExploderTransform,Exploder.Plane,System.Boolean,System.Boolean,System.Collections.Generic.List`1<Exploder.ExploderMesh>&)
extern void MeshCutter_Cut_m88A7DFB745A547FAE698132D8FC67343F2E522F4 ();
// 0x00000A7B System.Int32 Exploder.MeshCutter::AddIntersectionPoint(UnityEngine.Vector3,Exploder.MeshCutter_Triangle,System.Int32,System.Int32,System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Vector4>,System.Collections.Generic.List`1<UnityEngine.Color32>,System.Boolean,System.Boolean,System.Boolean)
extern void MeshCutter_AddIntersectionPoint_m4472E919F5B05A8A3C8B30B177CCD5A9CE4F8314 ();
// 0x00000A7C System.Int32 Exploder.MeshCutter::AddTrianglePoint(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector2,UnityEngine.Vector4,UnityEngine.Color32,System.Int32,System.Int32[],System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Vector4>,System.Collections.Generic.List`1<UnityEngine.Color32>,System.Boolean,System.Boolean,System.Boolean)
extern void MeshCutter_AddTrianglePoint_m47228BF3465CCEDC769A20C069783ECB669AFF94 ();
// 0x00000A7D System.Void Exploder.MeshCutter::Triangulate(System.Collections.Generic.List`1<System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>>,Exploder.Plane,System.Collections.Generic.List`1<UnityEngine.Vector3>[],System.Collections.Generic.List`1<UnityEngine.Vector3>[],System.Collections.Generic.List`1<UnityEngine.Vector2>[],System.Collections.Generic.List`1<UnityEngine.Vector4>[],System.Collections.Generic.List`1<UnityEngine.Color32>[],System.Collections.Generic.List`1<System.Int32>[],System.Boolean,System.Boolean,System.Boolean,System.Boolean)
extern void MeshCutter_Triangulate_mC9A087AD7166FC265EFC492C553A9A0007834729 ();
// 0x00000A7E System.Void Exploder.MeshCutter::.ctor()
extern void MeshCutter__ctor_m078D65F8D03587274C773640BAEE10711B1F2571 ();
// 0x00000A7F UnityEngine.Vector3 Exploder.MeshUtils::ComputeBarycentricCoordinates(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void MeshUtils_ComputeBarycentricCoordinates_m52C97158C55BA1D3D8351DEF57D0B7302E3CA2E8 ();
// 0x00000A80 System.Void Exploder.MeshUtils::Swap(T&,T&)
// 0x00000A81 System.Void Exploder.MeshUtils::CenterPivot(UnityEngine.Vector3[],UnityEngine.Vector3)
extern void MeshUtils_CenterPivot_mD223D3CDBD77F6FED2C29A126E7FC96F2904E7F7 ();
// 0x00000A82 System.Collections.Generic.List`1<Exploder.ExploderMesh> Exploder.MeshUtils::IsolateMeshIslands(Exploder.ExploderMesh)
extern void MeshUtils_IsolateMeshIslands_m382BF4550E5B08E26662E786E18107F0EB0E9729 ();
// 0x00000A83 System.Void Exploder.MeshUtils::GeneratePolygonCollider(UnityEngine.PolygonCollider2D,UnityEngine.Mesh)
extern void MeshUtils_GeneratePolygonCollider_mDC0CDD5097CEEC4D279FC15909934C5E6AC8AFB8 ();
// 0x00000A84 UnityEngine.Vector3 Exploder.Plane::get_Pnt()
extern void Plane_get_Pnt_mB60B5F3DC896CE74D9A45A177B3F852E2178AB61 ();
// 0x00000A85 System.Void Exploder.Plane::set_Pnt(UnityEngine.Vector3)
extern void Plane_set_Pnt_m8CE1D78982BEB3B3EFBE613D75132FBFEC012DAA ();
// 0x00000A86 System.Void Exploder.Plane::.ctor(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void Plane__ctor_m625C227EAD5B55A774442EF428838B43F68227C3 ();
// 0x00000A87 System.Void Exploder.Plane::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Plane__ctor_m9A92F4BC9197AAF04A6DEA2ECF489BECFF2C3B7F ();
// 0x00000A88 System.Void Exploder.Plane::Set(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Plane_Set_m248258201152ED0D73542B1D8F1DF09622DE6ED2 ();
// 0x00000A89 System.Void Exploder.Plane::.ctor(Exploder.Plane)
extern void Plane__ctor_m7175825CDFDC81EA7173DB851AA3ADD81E1FA14D ();
// 0x00000A8A Exploder.Plane_PointClass Exploder.Plane::ClassifyPoint(UnityEngine.Vector3)
extern void Plane_ClassifyPoint_mADA0108859B4F02A53997998E8CADDB7BCA9607A ();
// 0x00000A8B System.Boolean Exploder.Plane::GetSide(UnityEngine.Vector3)
extern void Plane_GetSide_m6328009A0CB757629E0F17C72363B72D6D17DD81 ();
// 0x00000A8C System.Void Exploder.Plane::Flip()
extern void Plane_Flip_m629F94A064024762433C6B9A5BD2D7CA5066F64F ();
// 0x00000A8D System.Boolean Exploder.Plane::GetSideFix(UnityEngine.Vector3&)
extern void Plane_GetSideFix_mC0B9AFAEE6544EC2E4A943B4A5D65526266A73CE ();
// 0x00000A8E System.Boolean Exploder.Plane::SameSide(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Plane_SameSide_mF1A583C15918F23255E4BE82D5EB6873236897D2 ();
// 0x00000A8F System.Boolean Exploder.Plane::IntersectSegment(UnityEngine.Vector3,UnityEngine.Vector3,System.Single&,UnityEngine.Vector3&)
extern void Plane_IntersectSegment_m702F634D06CEC840354318783BC2FEE0C5A146DE ();
// 0x00000A90 System.Void Exploder.Plane::InverseTransform(Exploder.ExploderTransform)
extern void Plane_InverseTransform_m04B38EE7A991CB85A0596B9E67CA439E0FD190BE ();
// 0x00000A91 UnityEngine.Matrix4x4 Exploder.Plane::GetPlaneMatrix()
extern void Plane_GetPlaneMatrix_m3484CD98D35E28CA9060DE868853FAE66734A5E7 ();
// 0x00000A92 System.Void Exploder.Polygon::.ctor(UnityEngine.Vector2[])
extern void Polygon__ctor_m9A02B61C0E83A1CE4123D739C76ECF66B3C3B114 ();
// 0x00000A93 System.Single Exploder.Polygon::GetArea()
extern void Polygon_GetArea_mF677989AA3C4FB8EA62E0EAF2C096992B3F9FF7A ();
// 0x00000A94 System.Boolean Exploder.Polygon::IsPointInside(UnityEngine.Vector3)
extern void Polygon_IsPointInside_mB3562288D01C8724D3BF05C09D2D5A69342E24C1 ();
// 0x00000A95 System.Boolean Exploder.Polygon::IsPolygonInside(Exploder.Polygon)
extern void Polygon_IsPolygonInside_m4B303AAA43DA79442561BCD2E4A6038BEBB68ABF ();
// 0x00000A96 System.Void Exploder.Polygon::AddHole(Exploder.Polygon)
extern void Polygon_AddHole_m9ACFDA7612CAD668C6C9C127D4E8C7C8C67DA6DA ();
// 0x00000A97 System.Boolean Exploder.Polygon::Triangulate(Exploder.Array`1<System.Int32>)
extern void Polygon_Triangulate_m6230258DAF7F491F1EC76AD87CAD28F42CE9E641 ();
// 0x00000A98 System.Boolean Exploder.Polygon::Snip(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32[])
extern void Polygon_Snip_mC7A12C1CDC71CA5E288207D1289E005F2ACD1A99 ();
// 0x00000A99 System.Boolean Exploder.Polygon::InsideTriangle(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2)
extern void Polygon_InsideTriangle_m409A9EEA4EFA701E75988166CE89BB8589F7BAAF ();
// 0x00000A9A System.Void Exploder.CutterMT::.ctor(Exploder.Core)
extern void CutterMT__ctor_m963A98AE01EA4417B075BDFAC16F72810B1531FF ();
// 0x00000A9B Exploder.TaskType Exploder.CutterMT::get_Type()
extern void CutterMT_get_Type_m421E85B0F08A857335C64E2FE378A767FF74AE6F ();
// 0x00000A9C System.Void Exploder.CutterMT::Init()
extern void CutterMT_Init_mEBD78F6413381AC3A132B73D0D5856C0ECC27A5F ();
// 0x00000A9D System.Void Exploder.CutterMT::OnDestroy()
extern void CutterMT_OnDestroy_m49D8F35E5F9D6C949E71ED069393B0A790F33787 ();
// 0x00000A9E System.Boolean Exploder.CutterMT::Run(System.Single)
extern void CutterMT_Run_mA3E0B33089469BBD3786EC4A24952F5DD04FCC5F ();
// 0x00000A9F System.Boolean Exploder.CutterMT::Cut(System.Single)
extern void CutterMT_Cut_mC8197BEB1C04FBB3EDADBAD1B240B13F73D89DA0 ();
// 0x00000AA0 System.Int32[] Exploder.CutterMT::SplitMeshTargetFragments(System.Int32)
extern void CutterMT_SplitMeshTargetFragments_m3244B35297BB775F37BE7E8FB9D464BE0BD6F70C ();
// 0x00000AA1 System.Void Exploder.CutterST::.ctor(Exploder.Core)
extern void CutterST__ctor_m0D9327BDEA7AC62B8C551EE624774A2A16C0F4BA ();
// 0x00000AA2 Exploder.TaskType Exploder.CutterST::get_Type()
extern void CutterST_get_Type_m9753F45DF212E545436C84E902B350D579B22FC0 ();
// 0x00000AA3 System.Void Exploder.CutterST::Init()
extern void CutterST_Init_m96BEB95D92AAD64BF98C78CAA020D8A4CFD26400 ();
// 0x00000AA4 System.Boolean Exploder.CutterST::Run(System.Single)
extern void CutterST_Run_mAE220084394D85E22732B308C7AFC07CF94051C4 ();
// 0x00000AA5 System.Boolean Exploder.CutterST::Cut(System.Single)
extern void CutterST_Cut_m878CE177718BEE6B91946CF8757206E475F31CE7 ();
// 0x00000AA6 System.Collections.Generic.List`1<Exploder.ExploderMesh> Exploder.CutterST::CutSingleMesh(Exploder.MeshObject)
extern void CutterST_CutSingleMesh_m6C11344CF399847B2D12C9E64A45A0786AED6759 ();
// 0x00000AA7 System.Void Exploder.CutterWorker::.ctor(Exploder.Core,Exploder.CuttingPlane)
extern void CutterWorker__ctor_m7AB996453D078CE3C2FE94E67D249058E29D5F1C ();
// 0x00000AA8 System.Void Exploder.CutterWorker::Init()
extern void CutterWorker_Init_m1105A78935E37053D0646159C46B38858670AD12 ();
// 0x00000AA9 System.Void Exploder.CutterWorker::AddMesh(Exploder.MeshObject)
extern void CutterWorker_AddMesh_mBFB7D391060DCA20245167E88BA50C8FED7326A8 ();
// 0x00000AAA System.Void Exploder.CutterWorker::Run()
extern void CutterWorker_Run_m8DB48906DE953A636251CD7B75FD60118801D6C5 ();
// 0x00000AAB System.Void Exploder.CutterWorker::ThreadRun()
extern void CutterWorker_ThreadRun_mF197658B28AF9D32A12DC474789FFDA276BC612E ();
// 0x00000AAC System.Boolean Exploder.CutterWorker::IsFinished()
extern void CutterWorker_IsFinished_mDA81D254387BCC29F4ACAA8D48A58AC4C8824350 ();
// 0x00000AAD System.Collections.Generic.HashSet`1<Exploder.MeshObject> Exploder.CutterWorker::GetResults()
extern void CutterWorker_GetResults_m483CAA3EDE2FD5D9E7A033E838924633D6CDDCAD ();
// 0x00000AAE System.Void Exploder.CutterWorker::Terminate()
extern void CutterWorker_Terminate_mAE9663CE4A0AC87B4F5B1AF8679494AC57CBA990 ();
// 0x00000AAF System.Void Exploder.CutterWorker::Cut()
extern void CutterWorker_Cut_m5C325E45427F1300436B614172F2F94662963B6C ();
// 0x00000AB0 Exploder.TaskType Exploder.ExploderTask::get_Type()
// 0x00000AB1 System.Diagnostics.Stopwatch Exploder.ExploderTask::get_Watch()
extern void ExploderTask_get_Watch_m0B3FB3B18D66793F1A1D48D73AB3A06A83D09E22 ();
// 0x00000AB2 System.Void Exploder.ExploderTask::set_Watch(System.Diagnostics.Stopwatch)
extern void ExploderTask_set_Watch_m774B069B2B228C32FCF1AA96706911612AFE2D25 ();
// 0x00000AB3 System.Void Exploder.ExploderTask::.ctor(Exploder.Core)
extern void ExploderTask__ctor_mFBB7E6D1F263E5815CE12B67C0FDD9CFDCBDB0DA ();
// 0x00000AB4 System.Void Exploder.ExploderTask::OnDestroy()
extern void ExploderTask_OnDestroy_mAA767EC1F6FCBDC818AECB8745DDE09A8259B13F ();
// 0x00000AB5 System.Void Exploder.ExploderTask::Init()
extern void ExploderTask_Init_m0DA4C90EE76FBA81EE9EC2C4C7DDA8CED1C95CD3 ();
// 0x00000AB6 System.Boolean Exploder.ExploderTask::Run(System.Single)
// 0x00000AB7 System.Void Exploder.IsolateMeshIslands::.ctor(Exploder.Core)
extern void IsolateMeshIslands__ctor_m7D5538928C87F76D341EBB9F41F2BA9159A7ED77 ();
// 0x00000AB8 Exploder.TaskType Exploder.IsolateMeshIslands::get_Type()
extern void IsolateMeshIslands_get_Type_m627DAF038079F3BA73634BE217C80DE125DD23D2 ();
// 0x00000AB9 System.Void Exploder.IsolateMeshIslands::Init()
extern void IsolateMeshIslands_Init_m8E9F7D12205BFD8EFCCD81A6CA61CAC62C1AED93 ();
// 0x00000ABA System.Boolean Exploder.IsolateMeshIslands::Run(System.Single)
extern void IsolateMeshIslands_Run_m13B29B51E320D1A8B01BB549EC934F5F8593D19C ();
// 0x00000ABB System.Void Exploder.Postprocess::.ctor(Exploder.Core)
extern void Postprocess__ctor_m17DFEE2CF55119AE7A48DFC16DCD2DF31E882FE8 ();
// 0x00000ABC System.Void Exploder.Postprocess::Init()
extern void Postprocess_Init_mE2DC4C6ADFB926FA3269EF64D6FA32F3E4EA0073 ();
// 0x00000ABD System.Void Exploder.PostprocessCrack::.ctor(Exploder.Core)
extern void PostprocessCrack__ctor_m15D40761606245F19C05D8B0D6989931082D80CA ();
// 0x00000ABE Exploder.TaskType Exploder.PostprocessCrack::get_Type()
extern void PostprocessCrack_get_Type_m5E349A40669C58F902DFD2A43CFE3FB6546C93EE ();
// 0x00000ABF System.Void Exploder.PostprocessCrack::Init()
extern void PostprocessCrack_Init_mDB86180298E66FA145BDCCE1EBC1E7A48D7C9802 ();
// 0x00000AC0 System.Boolean Exploder.PostprocessCrack::Run(System.Single)
extern void PostprocessCrack_Run_mE99C82AC1CA2A7076BEFDED26B1A3365476CBEC4 ();
// 0x00000AC1 System.Void Exploder.PostprocessExplode::.ctor(Exploder.Core)
extern void PostprocessExplode__ctor_m815EECBE5AE9C3DA343B62D1B82F74FB2A1E86A6 ();
// 0x00000AC2 Exploder.TaskType Exploder.PostprocessExplode::get_Type()
extern void PostprocessExplode_get_Type_m5AC9CE1717C57406B641C0A6CD387D321177679E ();
// 0x00000AC3 System.Void Exploder.PostprocessExplode::Init()
extern void PostprocessExplode_Init_m0CFCA543BFA3A8E513A48FDE297E9AFB7E23F0AF ();
// 0x00000AC4 System.Boolean Exploder.PostprocessExplode::Run(System.Single)
extern void PostprocessExplode_Run_m60EDB519CF9850DD44A07A59F233A753C57A166F ();
// 0x00000AC5 System.Void Exploder.Preprocess::.ctor(Exploder.Core)
extern void Preprocess__ctor_m2393F22580300D8961C50E83A616A79F3A1CEE36 ();
// 0x00000AC6 Exploder.TaskType Exploder.Preprocess::get_Type()
extern void Preprocess_get_Type_m87173F799F513932D16D7AFDDD27974647F56902 ();
// 0x00000AC7 System.Void Exploder.Preprocess::Init()
extern void Preprocess_Init_mD74091CE5904094E62CF68124588E53C4E5AA52F ();
// 0x00000AC8 System.Boolean Exploder.Preprocess::Run(System.Single)
extern void Preprocess_Run_mE934322675F36278C0D00A311628D74787419216 ();
// 0x00000AC9 System.Collections.Generic.List`1<Exploder.MeshObject> Exploder.Preprocess::GetMeshList()
extern void Preprocess_GetMeshList_m1EB0F01EB94D3B9978AA9E16F27D8CF7DA0D49C2 ();
// 0x00000ACA System.Collections.Generic.List`1<Exploder.Preprocess_MeshData> Exploder.Preprocess::GetMeshData(UnityEngine.GameObject)
extern void Preprocess_GetMeshData_mFDEC6971A32C976116122811A1F02804001DCCE8 ();
// 0x00000ACB System.Single Exploder.Preprocess::GetDistanceRatio(System.Single,System.Single)
extern void Preprocess_GetDistanceRatio_mAD2AB165B27C564F9C164B7DABA192BE2C22E0C2 ();
// 0x00000ACC System.Boolean Exploder.Preprocess::IsInRadius(UnityEngine.GameObject)
extern void Preprocess_IsInRadius_mCBE4A74FBFFB9AD0408BFD5E7669DBFCDD270A55 ();
// 0x00000ACD System.Void Exploder.Example::ExplodeObject(UnityEngine.GameObject)
extern void Example_ExplodeObject_mCDCC83F218BA728716D35DDABD4323E520962609 ();
// 0x00000ACE System.Void Exploder.Example::OnExplosion(System.Single,Exploder.ExploderObject_ExplosionState)
extern void Example_OnExplosion_mD8108136A3B47725BE36F26541F1E1D8B482BE8F ();
// 0x00000ACF System.Void Exploder.Example::CrackAndExplodeObject(UnityEngine.GameObject)
extern void Example_CrackAndExplodeObject_m10B56B43BFF93D54FC8372E87F86F3726E68EC4C ();
// 0x00000AD0 System.Void Exploder.Example::OnCracked(System.Single,Exploder.ExploderObject_ExplosionState)
extern void Example_OnCracked_m83D0C297B76C893C0AC1112E104EE7EC8B190A0E ();
// 0x00000AD1 System.Void Exploder.Example::.ctor()
extern void Example__ctor_mC50CE02978AB3B380FD62EA80B5CD0C0D422E08D ();
// 0x00000AD2 System.Void Exploder.Explodable::.ctor()
extern void Explodable__ctor_m4EEF0EF87126911DEF42EE993490C077E10393CE ();
// 0x00000AD3 System.Void Exploder.ExploderObject::ExplodeRadius()
extern void ExploderObject_ExplodeRadius_m8377614BE3E83E6834F85955836612FEDA76DBFE ();
// 0x00000AD4 System.Void Exploder.ExploderObject::ExplodeRadius(Exploder.ExploderObject_OnExplosion)
extern void ExploderObject_ExplodeRadius_mA9D19359E28E75C6D69AD48F8BFD86CCD86C32E5 ();
// 0x00000AD5 System.Void Exploder.ExploderObject::ExplodeObject(UnityEngine.GameObject)
extern void ExploderObject_ExplodeObject_m54A678079E30F09445489770A4691B2463874674 ();
// 0x00000AD6 System.Void Exploder.ExploderObject::ExplodeObject(UnityEngine.GameObject,Exploder.ExploderObject_OnExplosion)
extern void ExploderObject_ExplodeObject_mE224D2619D90181CA1092A5814C41CBF75A73CEA ();
// 0x00000AD7 System.Void Exploder.ExploderObject::ExplodeObjects(UnityEngine.GameObject[])
extern void ExploderObject_ExplodeObjects_m3BFADF6724906BB10304FAA6B9AFD0B757E50E5B ();
// 0x00000AD8 System.Void Exploder.ExploderObject::ExplodeObjects(Exploder.ExploderObject_OnExplosion,UnityEngine.GameObject[])
extern void ExploderObject_ExplodeObjects_m55DCF314EAFD1C2650A9642D4201E73C85F6F329 ();
// 0x00000AD9 System.Void Exploder.ExploderObject::CrackRadius()
extern void ExploderObject_CrackRadius_m0A5300E8AC970DCD7DCE438222E97E4ED561E2A1 ();
// 0x00000ADA System.Void Exploder.ExploderObject::CrackRadius(Exploder.ExploderObject_OnExplosion)
extern void ExploderObject_CrackRadius_m8E04AC1FAFACFCA83F81C3DB23C79AC5491F02FC ();
// 0x00000ADB System.Void Exploder.ExploderObject::CrackObject(UnityEngine.GameObject)
extern void ExploderObject_CrackObject_mC27C49FAD2E7B530E5BCF34B42E27C01978C02A2 ();
// 0x00000ADC System.Void Exploder.ExploderObject::CancelAllTask()
extern void ExploderObject_CancelAllTask_m65D0009D3050EA519DE2F412943BEA23CA21B9D6 ();
// 0x00000ADD System.Void Exploder.ExploderObject::CrackObject(UnityEngine.GameObject,Exploder.ExploderObject_OnExplosion)
extern void ExploderObject_CrackObject_mF25670967E76BCFC1FE23D7BA6C74E349E5DEB8E ();
// 0x00000ADE System.Boolean Exploder.ExploderObject::CanCrack()
extern void ExploderObject_CanCrack_mD965912C2AA71CFE7659AF9A26BD1CE921AADA70 ();
// 0x00000ADF System.Boolean Exploder.ExploderObject::IsCracked(UnityEngine.GameObject)
extern void ExploderObject_IsCracked_m2329CAF68BB0C4F8EC5B8486BBB23EBE9D3A210A ();
// 0x00000AE0 System.Void Exploder.ExploderObject::ExplodeCracked(UnityEngine.GameObject,Exploder.ExploderObject_OnExplosion)
extern void ExploderObject_ExplodeCracked_mB1C821C3EBA584342E91E6A6ED57AB06DF155419 ();
// 0x00000AE1 System.Void Exploder.ExploderObject::ExplodeCracked(UnityEngine.GameObject)
extern void ExploderObject_ExplodeCracked_m067857D3FDEE7546570EAA77B55A3D0F907C42E4 ();
// 0x00000AE2 System.Void Exploder.ExploderObject::ExplodeCracked(Exploder.ExploderObject_OnExplosion)
extern void ExploderObject_ExplodeCracked_mB8911FCE5676C75E87237D685146842DFC7C32F3 ();
// 0x00000AE3 System.Void Exploder.ExploderObject::ExplodeCracked()
extern void ExploderObject_ExplodeCracked_m0408F1E1E88BAD6552221CF797B03CEC6F4A63F6 ();
// 0x00000AE4 System.Int32 Exploder.ExploderObject::get_ProcessingFrames()
extern void ExploderObject_get_ProcessingFrames_mF004BD476E892CC60BC7DF4A4A289CBD09197BED ();
// 0x00000AE5 System.Void Exploder.ExploderObject::Awake()
extern void ExploderObject_Awake_m0495B68A4E8112D44807AD33B2AF9026F7949021 ();
// 0x00000AE6 System.Void Exploder.ExploderObject::OnDrawGizmos()
extern void ExploderObject_OnDrawGizmos_m45CF06CE6A632332F7DEF9483FB15BC6B917398D ();
// 0x00000AE7 System.Boolean Exploder.ExploderObject::IsExplodable(UnityEngine.GameObject)
extern void ExploderObject_IsExplodable_m34C584D4963224DB0A2801E13390429179B565AC ();
// 0x00000AE8 System.Void Exploder.ExploderObject::.ctor()
extern void ExploderObject__ctor_m275ED4751077A435EBA08B881910625DBD83B673 ();
// 0x00000AE9 System.Void Exploder.ExploderObject::.cctor()
extern void ExploderObject__cctor_mC5C613468A3D7BB9B7FEA542E2DB7EC19C349F95 ();
// 0x00000AEA System.Void Exploder.ExploderOption::DuplicateSettings(Exploder.ExploderOption)
extern void ExploderOption_DuplicateSettings_mD27D5745057597B8F10CA7C25F11AEC9160C7204 ();
// 0x00000AEB System.Void Exploder.ExploderOption::.ctor()
extern void ExploderOption__ctor_m444B77E8F5A1FBC486F4358181057A5E4162B1EF ();
// 0x00000AEC System.Boolean Exploder.Fragment::IsSleeping()
extern void Fragment_IsSleeping_m84B9BD16F0BB76792279B5609000A801B725908F ();
// 0x00000AED System.Void Exploder.Fragment::Sleep()
extern void Fragment_Sleep_m65401A0704C7DB2A28A874487522AF3C39123243 ();
// 0x00000AEE System.Void Exploder.Fragment::WakeUp()
extern void Fragment_WakeUp_mB1563B48D347DDE3CBA7F90069B74A8733E35E69 ();
// 0x00000AEF System.Void Exploder.Fragment::SetConstraints(UnityEngine.RigidbodyConstraints)
extern void Fragment_SetConstraints_m57E541573901D3412A6AA39EC106661AAD03593C ();
// 0x00000AF0 System.Void Exploder.Fragment::InitSFX(Exploder.FragmentSFX)
extern void Fragment_InitSFX_mF12ADCC4771CB2E3AF156CC9651DF4D8E4B0281F ();
// 0x00000AF1 System.Void Exploder.Fragment::OnCollisionEnter()
extern void Fragment_OnCollisionEnter_mBB2E12C45A726E3B75BE6A2334BC6B6088B50F53 ();
// 0x00000AF2 System.Void Exploder.Fragment::DisableColliders(System.Boolean,System.Boolean,System.Boolean)
extern void Fragment_DisableColliders_m1821C54FA4F2F2B7DF92675005C8896681880EC3 ();
// 0x00000AF3 System.Void Exploder.Fragment::ApplyExplosion(Exploder.ExploderTransform,UnityEngine.Vector3,System.Single,UnityEngine.GameObject,Exploder.ExploderParams)
extern void Fragment_ApplyExplosion_m897E95DD9520AABC3BBB0CAC8129CC633E303ED8 ();
// 0x00000AF4 System.Void Exploder.Fragment::ApplyExplosion2D(Exploder.ExploderTransform,UnityEngine.Vector3,System.Single,UnityEngine.GameObject)
extern void Fragment_ApplyExplosion2D_m360276A020D25C31C4A299B5CD7FFCD6B9FBF915 ();
// 0x00000AF5 System.Void Exploder.Fragment::RefreshComponentsCache()
extern void Fragment_RefreshComponentsCache_m18EA177FDB3D68FA53F540A1298E9ECB4B505CD3 ();
// 0x00000AF6 System.Void Exploder.Fragment::Explode(Exploder.ExploderParams)
extern void Fragment_Explode_m1ABE086C4928A809C0FDEDADA4DC30AAC7BAE130 ();
// 0x00000AF7 System.Void Exploder.Fragment::Emit(System.Boolean)
extern void Fragment_Emit_mB9327EAB3B07BC9BC4373B293ACB381E35AC60DA ();
// 0x00000AF8 System.Void Exploder.Fragment::Deactivate()
extern void Fragment_Deactivate_mDEB018EA2E4B2280D9C3E38F912EA996BD602264 ();
// 0x00000AF9 System.Void Exploder.Fragment::AssignMesh(UnityEngine.Mesh)
extern void Fragment_AssignMesh_mF60C7DDB26BDCACE4F89B37838306E3C33EDB5F1 ();
// 0x00000AFA System.Void Exploder.Fragment::Start()
extern void Fragment_Start_mE9C3443F60F5E381927992740B511CBDAC9FE68F ();
// 0x00000AFB System.Void Exploder.Fragment::Update()
extern void Fragment_Update_m72D9AB10EAC7D6BBB66614FEEC8C1EEE3604C756 ();
// 0x00000AFC System.Void Exploder.Fragment::.ctor()
extern void Fragment__ctor_mC05EFB167CDA2AD5B52F6937AB5B4202B5CBF20D ();
// 0x00000AFD Exploder.FragmentDeactivation Exploder.FragmentDeactivation::Clone()
extern void FragmentDeactivation_Clone_mC7B7E902AE7ABD588E65643F0E99FEF5FF6F2E58 ();
// 0x00000AFE System.Void Exploder.FragmentDeactivation::.ctor()
extern void FragmentDeactivation__ctor_mCA95CCED4097B517D26A7FA7D2AFC437010E7705 ();
// 0x00000AFF Exploder.FragmentOption Exploder.FragmentOption::Clone()
extern void FragmentOption_Clone_mD27F4C44D05099F4868BA73EDDFE5A4AC9A67637 ();
// 0x00000B00 System.Void Exploder.FragmentOption::.ctor()
extern void FragmentOption__ctor_m1E9DAE5E719A7EA03E64768CD0FBC25568EB1855 ();
// 0x00000B01 Exploder.FragmentPool Exploder.FragmentPool::get_Instance()
extern void FragmentPool_get_Instance_m871D98BC9F81FB9A3C2654B870A5C195549A3013 ();
// 0x00000B02 System.Void Exploder.FragmentPool::Awake()
extern void FragmentPool_Awake_m64B4671E6FDA95E5EDB180796FCDC7B5336782C2 ();
// 0x00000B03 System.Void Exploder.FragmentPool::OnDestroy()
extern void FragmentPool_OnDestroy_mBF7ACD2D2125762DD3568D549B99D3453B8B2A68 ();
// 0x00000B04 System.Int32 Exploder.FragmentPool::get_PoolSize()
extern void FragmentPool_get_PoolSize_m552AA5CFCC63B2CB48AE648288875B3FD247B8D4 ();
// 0x00000B05 Exploder.Fragment[] Exploder.FragmentPool::get_Pool()
extern void FragmentPool_get_Pool_mD0FE15C837EC6198393F82CE344A5779A70B90A5 ();
// 0x00000B06 System.Collections.Generic.List`1<Exploder.Fragment> Exploder.FragmentPool::GetAvailableFragments(System.Int32)
extern void FragmentPool_GetAvailableFragments_mAB3DC746694CD18A6353FE4EDCDD3B6F9EE4B483 ();
// 0x00000B07 System.Int32 Exploder.FragmentPool::GetAvailableCrackFragmentsCount()
extern void FragmentPool_GetAvailableCrackFragmentsCount_m31B21359BFA14F4B6BD38A9B6612AA9B5CF3A5E2 ();
// 0x00000B08 System.Void Exploder.FragmentPool::Reset(Exploder.ExploderParams)
extern void FragmentPool_Reset_m84A3451852DA7A76627D751136A6EE268D4A73C0 ();
// 0x00000B09 System.Void Exploder.FragmentPool::Allocate(System.Int32,System.Boolean,System.Boolean)
extern void FragmentPool_Allocate_m5978FD666CFE30A93D0C58F4E92E8378BA3E7BFD ();
// 0x00000B0A System.Void Exploder.FragmentPool::ResetTransform()
extern void FragmentPool_ResetTransform_mA051CBD46E5680CEB2C3BCB6637409D21526062B ();
// 0x00000B0B System.Void Exploder.FragmentPool::WakeUp()
extern void FragmentPool_WakeUp_m5CABB8C62BA3D11E43179D47887A01BB198BC74F ();
// 0x00000B0C System.Void Exploder.FragmentPool::Sleep()
extern void FragmentPool_Sleep_mF02DF6F82ACB581CC745A4EEF46F1F0632B3DE5F ();
// 0x00000B0D System.Void Exploder.FragmentPool::DestroyFragments()
extern void FragmentPool_DestroyFragments_mD6C79FDE74DF5B5D414BD66AE3B606522F33E51B ();
// 0x00000B0E System.Void Exploder.FragmentPool::DeactivateFragments()
extern void FragmentPool_DeactivateFragments_mD2361F34200B601DD2F74A108481A73EE1325030 ();
// 0x00000B0F System.Void Exploder.FragmentPool::SetExplodableFragments(System.Boolean,System.Boolean)
extern void FragmentPool_SetExplodableFragments_mEEF4B9BC1A3E22B54868DFC933F483BD27797CD5 ();
// 0x00000B10 System.Void Exploder.FragmentPool::SetFragmentPhysicsOptions(Exploder.FragmentOption,System.Boolean)
extern void FragmentPool_SetFragmentPhysicsOptions_m8FF53EE76F32CC0DE62467A8B9E42E1D5FBCCDC6 ();
// 0x00000B11 System.Void Exploder.FragmentPool::SetSFXOptions(Exploder.FragmentSFX)
extern void FragmentPool_SetSFXOptions_m7078F342FE560FBEDB8A97124C543F10D84A4ACC ();
// 0x00000B12 System.Collections.Generic.List`1<Exploder.Fragment> Exploder.FragmentPool::GetActiveFragments()
extern void FragmentPool_GetActiveFragments_m0040CF9F36EAD54711D8FBEFF3AFCEF89B5F16DA ();
// 0x00000B13 System.Void Exploder.FragmentPool::.ctor()
extern void FragmentPool__ctor_mFD8F087142683429A1534FFF38F9DEF3AE23B72F ();
// 0x00000B14 Exploder.FragmentSFX Exploder.FragmentSFX::Clone()
extern void FragmentSFX_Clone_mDB3AC3056D0EC385159148609720B36914A068F4 ();
// 0x00000B15 System.Void Exploder.FragmentSFX::.ctor()
extern void FragmentSFX__ctor_m7A2193DD3E5DD1D4BB65CF47E1EAAE8D432E660C ();
// 0x00000B16 System.Void Exploder.Profiler::Start(System.String)
extern void Profiler_Start_mB3BC2A788C005E584BEB6864901872861B4C37CC ();
// 0x00000B17 System.Void Exploder.Profiler::End(System.String)
extern void Profiler_End_m41AC035DD1BAFE7035074653B9348041B492AF1B ();
// 0x00000B18 System.String[] Exploder.Profiler::PrintResults()
extern void Profiler_PrintResults_m7B468DE3DC58AF1FCEB01313EF4FD320CFC88674 ();
// 0x00000B19 System.Void Exploder.Profiler::.cctor()
extern void Profiler__cctor_m2322DBC43D80F22B646972AF623680E3FEEBA25D ();
// 0x00000B1A System.Void Exploder.ExploderSlowMotion::Start()
extern void ExploderSlowMotion_Start_m217503D43C9ED7934E30E19AFB1CDAD7C5A57BA9 ();
// 0x00000B1B System.Void Exploder.ExploderSlowMotion::EnableSlowMotion(System.Boolean)
extern void ExploderSlowMotion_EnableSlowMotion_m4ABE874E9B93EB25BFCACC0C67A49FB55B37D575 ();
// 0x00000B1C System.Void Exploder.ExploderSlowMotion::Update()
extern void ExploderSlowMotion_Update_mDFE2C72F08982165AA8BB4C4A63556F70DEA8F83 ();
// 0x00000B1D System.Void Exploder.ExploderSlowMotion::.ctor()
extern void ExploderSlowMotion__ctor_mF441190BE9AAC1F9B8A6D47083A6029A20685D6D ();
// 0x00000B1E System.Void Exploder.ExploderUtils::Assert(System.Boolean,System.String)
extern void ExploderUtils_Assert_m8A483F9CB0E41BC0961CB40412B6FA857FD2BEF4 ();
// 0x00000B1F System.Void Exploder.ExploderUtils::Warning(System.Boolean,System.String)
extern void ExploderUtils_Warning_mF093B7437424240190315BFA8F754392B3B9C0C1 ();
// 0x00000B20 System.Void Exploder.ExploderUtils::Log(System.String)
extern void ExploderUtils_Log_m98B8FA99E4B5DA6FE4C6F95A0468C87ACACE6558 ();
// 0x00000B21 UnityEngine.Vector3 Exploder.ExploderUtils::GetCentroid(UnityEngine.GameObject)
extern void ExploderUtils_GetCentroid_m4525C6AD9A2BB6FD5AB389115070F19688D83AA2 ();
// 0x00000B22 System.Void Exploder.ExploderUtils::SetVisible(UnityEngine.GameObject,System.Boolean)
extern void ExploderUtils_SetVisible_m26C6BE5DECF6F5ED7DCA09D61CBD857536B6CC20 ();
// 0x00000B23 System.Void Exploder.ExploderUtils::ClearLog()
extern void ExploderUtils_ClearLog_m418A1E5A0F949CEDC5B85CBABFDDD6BFAE5327A8 ();
// 0x00000B24 System.Boolean Exploder.ExploderUtils::IsActive(UnityEngine.GameObject)
extern void ExploderUtils_IsActive_m09E14F26545A2599C741DC9BA2C07C427EA59F1D ();
// 0x00000B25 System.Void Exploder.ExploderUtils::SetActive(UnityEngine.GameObject,System.Boolean)
extern void ExploderUtils_SetActive_mCFB931F57899BA4A9F293072DC67D1F2126C5172 ();
// 0x00000B26 System.Void Exploder.ExploderUtils::SetActiveRecursively(UnityEngine.GameObject,System.Boolean)
extern void ExploderUtils_SetActiveRecursively_m0E5C699028464B5507956E35770E1278AA0593EF ();
// 0x00000B27 System.Void Exploder.ExploderUtils::EnableCollider(UnityEngine.GameObject,System.Boolean)
extern void ExploderUtils_EnableCollider_m9ADCBF691DED339B9113C5C8A65EDF9A136F3C4E ();
// 0x00000B28 System.Boolean Exploder.ExploderUtils::IsExplodable(UnityEngine.GameObject)
extern void ExploderUtils_IsExplodable_m9B258F32595B53914CE5A2C6E3078519D0CA8FF0 ();
// 0x00000B29 System.Void Exploder.ExploderUtils::CopyAudioSource(UnityEngine.AudioSource,UnityEngine.AudioSource)
extern void ExploderUtils_CopyAudioSource_m6691A9B56CB28D01BBC23CDF6AD74A25CABBCE8A ();
// 0x00000B2A System.Void Exploder.HUDFPS::Start()
extern void HUDFPS_Start_mF45685E1E1AC1A9483B947CA093A4D125260863A ();
// 0x00000B2B System.Void Exploder.HUDFPS::Update()
extern void HUDFPS_Update_m2DC0F5B021EDF57961D475CCDE156A8A2D1B4F07 ();
// 0x00000B2C System.Void Exploder.HUDFPS::.ctor()
extern void HUDFPS__ctor_m957962CC48BDAB08BDDBBDF20CF6D8665DC2427A ();
// 0x00000B2D T Exploder.Singleton`1::get_Instance()
// 0x00000B2E System.Void Exploder.Singleton`1::OnDestroy()
// 0x00000B2F System.Void Exploder.Singleton`1::.ctor()
// 0x00000B30 System.Void Exploder.Singleton`1::.cctor()
// 0x00000B31 System.Void Exploder.Utils.Compatibility::SetVisible(UnityEngine.GameObject,System.Boolean,System.Boolean)
extern void Compatibility_SetVisible_m72D79136EA6471352CD703E7848311C6C4F114DD ();
// 0x00000B32 System.Boolean Exploder.Utils.Compatibility::IsActive(UnityEngine.GameObject)
extern void Compatibility_IsActive_m6782908777C963562C314E9834F67DE473D135F8 ();
// 0x00000B33 System.Void Exploder.Utils.Compatibility::SetActive(UnityEngine.GameObject,System.Boolean)
extern void Compatibility_SetActive_m2EFE07A69DEA51243F9BEAE05E43E2402EDDD8CB ();
// 0x00000B34 System.Void Exploder.Utils.Compatibility::SetActiveRecursively(UnityEngine.GameObject,System.Boolean)
extern void Compatibility_SetActiveRecursively_mE0496CD67DAEA29ADAB45A359E15830ECA745D75 ();
// 0x00000B35 System.Void Exploder.Utils.Compatibility::EnableCollider(UnityEngine.GameObject,System.Boolean)
extern void Compatibility_EnableCollider_mFE4E17EA67D69DD1B25B6AF5527D279D9CEC459A ();
// 0x00000B36 System.Void Exploder.Utils.Compatibility::Destroy(UnityEngine.Object,System.Boolean)
extern void Compatibility_Destroy_mC7CF6F34038A05E159D914DB8E90B13454E1D1BD ();
// 0x00000B37 System.Void Exploder.Utils.Compatibility::SetCursorVisible(System.Boolean)
extern void Compatibility_SetCursorVisible_mD25E90D4B4359A3D39EB1481FD5912677CCD79EF ();
// 0x00000B38 System.Void Exploder.Utils.Compatibility::LockCursor(System.Boolean)
extern void Compatibility_LockCursor_mDE290ADB68B43F8D86688B437451971CB6687E46 ();
// 0x00000B39 System.Boolean Exploder.Utils.Compatibility::IsCursorLocked()
extern void Compatibility_IsCursorLocked_m96B2E64FA41AB7005375CBAD2976CCFC95F61B96 ();
// 0x00000B3A System.Void Exploder.Utils.ExploderSingleton::Awake()
extern void ExploderSingleton_Awake_mB806511E73E9AEBC9A8A74AA42B135CED81FECC6 ();
// 0x00000B3B System.Void Exploder.Utils.ExploderSingleton::OnDestroy()
extern void ExploderSingleton_OnDestroy_mFFBC592DA2B72AA24386E4A2D15A18BF4426BD0F ();
// 0x00000B3C System.Void Exploder.Utils.ExploderSingleton::.ctor()
extern void ExploderSingleton__ctor_mB7A7BACD3FF71D8813DD28F2F8C52F22555513DD ();
// 0x00000B3D System.Void Exploder.Utils.Hull2D::Sort(UnityEngine.Vector2[])
extern void Hull2D_Sort_m18D98D9C7EB2EBB97F87B2AD5981C528270FE0F1 ();
// 0x00000B3E System.Void Exploder.Utils.Hull2D::DumpArray(UnityEngine.Vector2[])
extern void Hull2D_DumpArray_mEBAAB3FC2319B752C9EF69EA38B07A780BBE6A50 ();
// 0x00000B3F UnityEngine.Vector2[] Exploder.Utils.Hull2D::ChainHull2D(UnityEngine.Vector2[])
extern void Hull2D_ChainHull2D_m902184602E491F769D3C69D05C558A30534A7AF3 ();
// 0x00000B40 System.Single Exploder.Utils.Hull2D::Hull2DCross(UnityEngine.Vector2&,UnityEngine.Vector2&,UnityEngine.Vector2&)
extern void Hull2D_Hull2DCross_mA8C821334677A4CA0EF67057260A1E7242D0F5D5 ();
// 0x00000B41 System.Void Exploder.Utils.Hull2D::.ctor()
extern void Hull2D__ctor_m41958C53347B028DF3A73AA861BCC34884D08820 ();
// 0x00000B42 System.Void Exploder.Examples.ExplodeAllObjects::Start()
extern void ExplodeAllObjects_Start_m0CE76A1C538589FCB14E86CC499DBADDBBE4C893 ();
// 0x00000B43 System.Void Exploder.Examples.ExplodeAllObjects::Update()
extern void ExplodeAllObjects_Update_mB3F1B21EB42D39F74A4FDD93561B4B6965326A95 ();
// 0x00000B44 System.Void Exploder.Examples.ExplodeAllObjects::ExplodeObject(UnityEngine.GameObject)
extern void ExplodeAllObjects_ExplodeObject_mB9F2B8FC3218B6B6333E8C8A25C15D90D513E35A ();
// 0x00000B45 System.Void Exploder.Examples.ExplodeAllObjects::OnGUI()
extern void ExplodeAllObjects_OnGUI_mBD4DEF693CD36647CD7B0F2D2092C196884D7553 ();
// 0x00000B46 System.Void Exploder.Examples.ExplodeAllObjects::.ctor()
extern void ExplodeAllObjects__ctor_mE7F0D290A0FA8859B48FE275E0B7BCFC1D65F667 ();
// 0x00000B47 System.Void Exploder.Demo.DemoClickExplode::Start()
extern void DemoClickExplode_Start_mD1A5DF5BC7BC1D17A1084BC25D9FD675CEECF1A9 ();
// 0x00000B48 System.Boolean Exploder.Demo.DemoClickExplode::IsExplodable(UnityEngine.GameObject)
extern void DemoClickExplode_IsExplodable_mCCBE387D974BAC7F02E94C9511C62738971C0A1E ();
// 0x00000B49 System.Void Exploder.Demo.DemoClickExplode::Update()
extern void DemoClickExplode_Update_m5D2121F3DACEB04CC5B792ADE5EE33D3E405DA01 ();
// 0x00000B4A System.Void Exploder.Demo.DemoClickExplode::ExplodeObject(UnityEngine.GameObject)
extern void DemoClickExplode_ExplodeObject_m01374A55580974FB4EA27AC357FC6B673663BE47 ();
// 0x00000B4B System.Void Exploder.Demo.DemoClickExplode::OnExplosion(System.Single,Exploder.ExploderObject_ExplosionState)
extern void DemoClickExplode_OnExplosion_m08F2AE7FD25340B598B9F9A3CA93B01C001BF1A1 ();
// 0x00000B4C System.Void Exploder.Demo.DemoClickExplode::OnCracked(System.Single,Exploder.ExploderObject_ExplosionState)
extern void DemoClickExplode_OnCracked_mDB5118326BE95BE2154FF567261C7D0BF3CA7D95 ();
// 0x00000B4D System.Void Exploder.Demo.DemoClickExplode::ExplodeAfterCrack(UnityEngine.GameObject)
extern void DemoClickExplode_ExplodeAfterCrack_mB300315C2811B43BD347C000D4C8583A7B7001ED ();
// 0x00000B4E System.Void Exploder.Demo.DemoClickExplode::OnGUI()
extern void DemoClickExplode_OnGUI_mFD5DEFCF3BAC5A2FA3A3000B3D09E466666A4065 ();
// 0x00000B4F System.Void Exploder.Demo.DemoClickExplode::.ctor()
extern void DemoClickExplode__ctor_m371B50B4A24FA4D8FBE40FE656327D7177F37DB1 ();
// 0x00000B50 System.Void Exploder.Demo.QuickStartDemo::Start()
extern void QuickStartDemo_Start_m4C798C3B41FDE31C5083F332E1970EFF7188F2C8 ();
// 0x00000B51 System.Void Exploder.Demo.QuickStartDemo::OnGUI()
extern void QuickStartDemo_OnGUI_m40638D5AD7F9E58FB3B95390911DB849172D9CB1 ();
// 0x00000B52 System.Void Exploder.Demo.QuickStartDemo::.ctor()
extern void QuickStartDemo__ctor_mEB9EF5069600257892792D4741BC441BBFDEC8E8 ();
// 0x00000B53 System.Void Exploder.Demo.CursorLocking::Awake()
extern void CursorLocking_Awake_m10C5F85B9D127684595E592CC96C31D33182E368 ();
// 0x00000B54 System.Void Exploder.Demo.CursorLocking::Update()
extern void CursorLocking_Update_m0DF67061FF2AB6CA78D78020C055863A4CA885CC ();
// 0x00000B55 System.Void Exploder.Demo.CursorLocking::Lock()
extern void CursorLocking_Lock_mA94D788BE6FC8238DB0FE3BE611A939FE474B7BF ();
// 0x00000B56 System.Void Exploder.Demo.CursorLocking::Unlock()
extern void CursorLocking_Unlock_m49C671D632E0922D07E56D2E7E3233FA613709D3 ();
// 0x00000B57 System.Void Exploder.Demo.CursorLocking::.ctor()
extern void CursorLocking__ctor_m8D5B839F8892A6131D21E7F1973B7155717F5A68 ();
// 0x00000B58 System.Void Exploder.Demo.ExploderMouseLook::Update()
extern void ExploderMouseLook_Update_mFBE281C514E4D17E52C9971547821D796CC6FC43 ();
// 0x00000B59 System.Void Exploder.Demo.ExploderMouseLook::Start()
extern void ExploderMouseLook_Start_mAF91BA5736819892F1E8341C6F7B51DEF460AC8E ();
// 0x00000B5A System.Void Exploder.Demo.ExploderMouseLook::Kick()
extern void ExploderMouseLook_Kick_m552F042726AA72FEAD11A5ACA2A1B23E6A601941 ();
// 0x00000B5B System.Void Exploder.Demo.ExploderMouseLook::.ctor()
extern void ExploderMouseLook__ctor_m25A52A377C0FBD674947884889CDA3BC0C78CD73 ();
// 0x00000B5C System.Void Exploder.Demo.GrenadeController::Start()
extern void GrenadeController_Start_m90A2D0222065D669BFAADEE9B97F190F0BA9FA5D ();
// 0x00000B5D System.Void Exploder.Demo.GrenadeController::Update()
extern void GrenadeController_Update_mF5974D7867ED1AFEF0E2EC225A8FB7D750878ABA ();
// 0x00000B5E System.Void Exploder.Demo.GrenadeController::.ctor()
extern void GrenadeController__ctor_m3CDCBC3641BED6D9BA0FB61C9F4B5F328C3D3E9E ();
// 0x00000B5F System.Void Exploder.Demo.GrenadeObject::Throw()
extern void GrenadeObject_Throw_m044E7301F0E84A4F8E24FE4CFE74D310D4ABE5BB ();
// 0x00000B60 System.Void Exploder.Demo.GrenadeObject::Explode()
extern void GrenadeObject_Explode_mC4080121FC895AE9A7B7D8531FC054AFEA867915 ();
// 0x00000B61 System.Void Exploder.Demo.GrenadeObject::OnExplode(System.Single,Exploder.ExploderObject_ExplosionState)
extern void GrenadeObject_OnExplode_m78CC3F411D5AEFA5BD7871DA669830A33666C03E ();
// 0x00000B62 System.Void Exploder.Demo.GrenadeObject::OnCollisionEnter(UnityEngine.Collision)
extern void GrenadeObject_OnCollisionEnter_m0CECE17E3B6795C01D32634F9439AB68BBBA1D7C ();
// 0x00000B63 System.Void Exploder.Demo.GrenadeObject::Update()
extern void GrenadeObject_Update_m2D533282BFFE980B48628261C1BC25048ECF3C9A ();
// 0x00000B64 System.Void Exploder.Demo.GrenadeObject::.ctor()
extern void GrenadeObject__ctor_m6A085FE2DD79922104B75970B39727E3D9751D98 ();
// 0x00000B65 System.Void Exploder.Demo.MoveController::Start()
extern void MoveController_Start_m156E1705F1146E2FD2A900111D895446A318AA08 ();
// 0x00000B66 System.Void Exploder.Demo.MoveController::Update()
extern void MoveController_Update_m9275B01F85911FBC7B8F7640FBE32D1A4D68F8EB ();
// 0x00000B67 System.Void Exploder.Demo.MoveController::.ctor()
extern void MoveController__ctor_mEE62C0DE7E6A2EB006FE7F22407D2892BFB1790F ();
// 0x00000B68 System.Void Exploder.Demo.PanelChairBomb::Use()
extern void PanelChairBomb_Use_m607C9A8BFBCDEDCD601C55E91F2B64437CF611EA ();
// 0x00000B69 System.Void Exploder.Demo.PanelChairBomb::OnExplode(System.Single,Exploder.ExploderObject_ExplosionState)
extern void PanelChairBomb_OnExplode_mFAB47304A96F1D85EF2987D85A3A7AC13AD45A15 ();
// 0x00000B6A System.Void Exploder.Demo.PanelChairBomb::Update()
extern void PanelChairBomb_Update_m4B3F3281AEA8E80045397A3C4AFD08791AB6C9F1 ();
// 0x00000B6B System.Void Exploder.Demo.PanelChairBomb::.ctor()
extern void PanelChairBomb__ctor_mCA0A2D2974EBFCBDFADEF2297BAE310F2A074094 ();
// 0x00000B6C System.Void Exploder.Demo.PanelResetScene::Start()
extern void PanelResetScene_Start_m147F9E0BB949C24A5FF07EAAB6D994ACE00AEB5C ();
// 0x00000B6D System.Void Exploder.Demo.PanelResetScene::Use()
extern void PanelResetScene_Use_m12BE94CBD1F4977ED43D863D0507344FDF52E87D ();
// 0x00000B6E System.Void Exploder.Demo.PanelResetScene::Update()
extern void PanelResetScene_Update_m996A084F342D27F3E3FB130829A824988F4D0825 ();
// 0x00000B6F System.Void Exploder.Demo.PanelResetScene::.ctor()
extern void PanelResetScene__ctor_m0C87A7D3B9534EDE4F6BEC0690A933CE19FC48BF ();
// 0x00000B70 System.Void Exploder.Demo.PanelThrowObject::Start()
extern void PanelThrowObject_Start_m502B935CDFCB0A2BCAF2EA166E5F12DC7DFFAA9A ();
// 0x00000B71 System.Void Exploder.Demo.PanelThrowObject::Use()
extern void PanelThrowObject_Use_m746CEE2D1E0605C0B2AF5E4E6C215EA156F642E3 ();
// 0x00000B72 System.Void Exploder.Demo.PanelThrowObject::.ctor()
extern void PanelThrowObject__ctor_m2357FDE29086085E93BF559E7A3349F01EB2D5C7 ();
// 0x00000B73 System.Void Exploder.Demo.RPGController::Start()
extern void RPGController_Start_mC22579F4815E27852AADDE1FA87D42DBA857F6B2 ();
// 0x00000B74 System.Void Exploder.Demo.RPGController::OnActivate()
extern void RPGController_OnActivate_m3E50B5EB9CC003A318E8F6C8FC42194E1976017F ();
// 0x00000B75 System.Void Exploder.Demo.RPGController::OnRocketHit(UnityEngine.Vector3)
extern void RPGController_OnRocketHit_m06C5C3D73CD8F137013F910FA56E29154E2B1208 ();
// 0x00000B76 System.Void Exploder.Demo.RPGController::Update()
extern void RPGController_Update_m09E8A6350A21EBED3891937BFD1618244F1DC021 ();
// 0x00000B77 System.Void Exploder.Demo.RPGController::.ctor()
extern void RPGController__ctor_m7F94AD23A8BD946666FAF576AA097472F05326CB ();
// 0x00000B78 System.Void Exploder.Demo.RobotScript::Start()
extern void RobotScript_Start_m61AF31A9F5D63A71CEF0E293683ABC0B12525C0E ();
// 0x00000B79 System.Void Exploder.Demo.RobotScript::Update()
extern void RobotScript_Update_mE6FA045BAEB555BD11E1FEB8177610EEFD148B60 ();
// 0x00000B7A System.Void Exploder.Demo.RobotScript::FixedUpdate()
extern void RobotScript_FixedUpdate_m2B53B1924FA505FB3A81B06A19AF177C00EE137A ();
// 0x00000B7B System.Void Exploder.Demo.RobotScript::.ctor()
extern void RobotScript__ctor_m7815DBB60B89AEA8437C535502A3064027277D13 ();
// 0x00000B7C System.Void Exploder.Demo.Rocket::Start()
extern void Rocket_Start_m15880B95E91E9AE11D39F00336D0F6D16B90B6C3 ();
// 0x00000B7D System.Void Exploder.Demo.Rocket::OnActivate()
extern void Rocket_OnActivate_mDB10875EE1B0E234B573104401DE6A7BAF960DC3 ();
// 0x00000B7E System.Void Exploder.Demo.Rocket::Reset()
extern void Rocket_Reset_m66B2B461A61975964BE3F8765D449F0DDC655B9E ();
// 0x00000B7F System.Void Exploder.Demo.Rocket::Launch(UnityEngine.Ray)
extern void Rocket_Launch_mF2DE288A855CBDA73D6EAF5BBA432DFC4442EE53 ();
// 0x00000B80 System.Void Exploder.Demo.Rocket::Update()
extern void Rocket_Update_m263D1B7B104D96B6ACFB9DFC6DBFA392CBC32955 ();
// 0x00000B81 System.Void Exploder.Demo.Rocket::.ctor()
extern void Rocket__ctor_mC96A95AE036DA1B503DFAB2899386963A3B32F6B ();
// 0x00000B82 System.Void Exploder.Demo.ShotgunController::OnActivate()
extern void ShotgunController_OnActivate_mBC4643A0F3C195949A9DD43241C0F0A974FB06E3 ();
// 0x00000B83 System.Void Exploder.Demo.ShotgunController::Update()
extern void ShotgunController_Update_m33CF3675D687D06813985A022D26E3B51E1C86BC ();
// 0x00000B84 System.Void Exploder.Demo.ShotgunController::.ctor()
extern void ShotgunController__ctor_m49D5288DA3C2EC4EDABE42034EE8F10FED04ED04 ();
// 0x00000B85 Exploder.Demo.TargetManager Exploder.Demo.TargetManager::get_Instance()
extern void TargetManager_get_Instance_mB256826199FF95DD62E579601B9E703D4C8DB674 ();
// 0x00000B86 System.Void Exploder.Demo.TargetManager::Awake()
extern void TargetManager_Awake_m0CCF1698B5F0BFBDDDD4E6E6AB02A48D181E4016 ();
// 0x00000B87 System.Void Exploder.Demo.TargetManager::Start()
extern void TargetManager_Start_m84672F318469F1F948DE38F816D6676E18212988 ();
// 0x00000B88 System.Void Exploder.Demo.TargetManager::Update()
extern void TargetManager_Update_mBE35DAC34D59FD616A77DD3B4F874EB34617BCFC ();
// 0x00000B89 System.Boolean Exploder.Demo.TargetManager::IsDestroyableObject(UnityEngine.GameObject)
extern void TargetManager_IsDestroyableObject_m47C263CDB6AD74AB1FD2602194268E8429009C90 ();
// 0x00000B8A System.Boolean Exploder.Demo.TargetManager::IsUseObject(UnityEngine.GameObject)
extern void TargetManager_IsUseObject_mD7D0B2ED3EEF96637132DE5C46E1A75D62385BDB ();
// 0x00000B8B System.Void Exploder.Demo.TargetManager::.ctor()
extern void TargetManager__ctor_m1FA55A041C705454D72C267DBA615F08F88E573D ();
// 0x00000B8C System.Int32 Exploder.Demo.TargetManager::<Update>b__12_0(UnityEngine.RaycastHit,UnityEngine.RaycastHit)
extern void TargetManager_U3CUpdateU3Eb__12_0_m31E8BA3302341057A2F4309A2E17DBDFF5FD3198 ();
// 0x00000B8D System.Void Exploder.Demo.ThrowObject::Start()
extern void ThrowObject_Start_m2EBFB58803865D9269D5903865ECDFBBA032BDF6 ();
// 0x00000B8E System.Void Exploder.Demo.ThrowObject::Update()
extern void ThrowObject_Update_mD1F4CAB12DFFC3378BDFB5E66CA7382E4660D0C7 ();
// 0x00000B8F System.Void Exploder.Demo.ThrowObject::.ctor()
extern void ThrowObject__ctor_m5E4EEDE5472EE582E4FC8FB874493B4A42337CC2 ();
// 0x00000B90 System.Void Exploder.Demo.UseObject::Use()
extern void UseObject_Use_m9D102C12987CA0E0DF2424B06BEBFB49498AB689 ();
// 0x00000B91 System.Void Exploder.Demo.UseObject::OnDrawGizmos()
extern void UseObject_OnDrawGizmos_m659784E1E29FD022E705A830ABD728977A03FD6F ();
// 0x00000B92 System.Void Exploder.Demo.UseObject::.ctor()
extern void UseObject__ctor_m7D040F280026FB01DF75A1D6529DDB91FBCE0D3E ();
// 0x00000B93 System.Void Exploder.Demo.WeaponManager::Update()
extern void WeaponManager_Update_m32FB280D8D2BAB807B5475557AE89D0C9658BF99 ();
// 0x00000B94 System.Void Exploder.Demo.WeaponManager::.ctor()
extern void WeaponManager__ctor_m1317845BB799BA42EFD42256132F67FE62C1EFBA ();
// 0x00000B95 System.Void EpicToonFX.ETFXButtonScript::Start()
extern void ETFXButtonScript_Start_m33A4FB0CC86F7136C59CA734D8DF511BC800DF16 ();
// 0x00000B96 System.Void EpicToonFX.ETFXButtonScript::Update()
extern void ETFXButtonScript_Update_m59B34AACCF491A85CC5FD3556204E5CAE1132F95 ();
// 0x00000B97 System.Void EpicToonFX.ETFXButtonScript::getProjectileNames()
extern void ETFXButtonScript_getProjectileNames_m125213EE305F066B86FD30975FB1F5530C775921 ();
// 0x00000B98 System.Boolean EpicToonFX.ETFXButtonScript::overButton()
extern void ETFXButtonScript_overButton_m3340E797EA0E2E0F1D9F63E848A0C9B87C99BB45 ();
// 0x00000B99 System.Void EpicToonFX.ETFXButtonScript::.ctor()
extern void ETFXButtonScript__ctor_m699F4899D8FC6BF816893B3885422E554837AF53 ();
// 0x00000B9A System.Void EpicToonFX.ETFXFireProjectile::Start()
extern void ETFXFireProjectile_Start_m3FEB80C442260ACBFEA4531790418F0DDEB74805 ();
// 0x00000B9B System.Void EpicToonFX.ETFXFireProjectile::Update()
extern void ETFXFireProjectile_Update_mA1BEB2D98E67448597D24B536BE8D86BE55E439E ();
// 0x00000B9C System.Void EpicToonFX.ETFXFireProjectile::nextEffect()
extern void ETFXFireProjectile_nextEffect_mDAAB6C1C6788A94AFFCB516063B782DA5EEB073A ();
// 0x00000B9D System.Void EpicToonFX.ETFXFireProjectile::previousEffect()
extern void ETFXFireProjectile_previousEffect_m5B3208B6BBFCEA6B7AEF6A2028CC81CE3A323E5F ();
// 0x00000B9E System.Void EpicToonFX.ETFXFireProjectile::AdjustSpeed(System.Single)
extern void ETFXFireProjectile_AdjustSpeed_m13129DA829729D1EDB48CE88D0F9557AF39E6ED1 ();
// 0x00000B9F System.Void EpicToonFX.ETFXFireProjectile::.ctor()
extern void ETFXFireProjectile__ctor_mB28C3568BDC94B61A99CFB864CB9B4EA1C0E3BFA ();
// 0x00000BA0 System.Void EpicToonFX.ETFXLoopScript::Start()
extern void ETFXLoopScript_Start_m9A424571F963ED20944D1CA011B364653B53261E ();
// 0x00000BA1 System.Void EpicToonFX.ETFXLoopScript::PlayEffect()
extern void ETFXLoopScript_PlayEffect_m18E7B31868B9EAC272E59782BA74D5E765C36073 ();
// 0x00000BA2 System.Collections.IEnumerator EpicToonFX.ETFXLoopScript::EffectLoop()
extern void ETFXLoopScript_EffectLoop_m7FBB740F68C31AE47D215875553E3C9096164005 ();
// 0x00000BA3 System.Void EpicToonFX.ETFXLoopScript::.ctor()
extern void ETFXLoopScript__ctor_m0DC2C1F2F2CCDC1F8D788A9A5C7D665FB686231B ();
// 0x00000BA4 System.Void EpicToonFX.ETFXMouseOrbit::Start()
extern void ETFXMouseOrbit_Start_mD455ECAD74152F902258F3C955454F5C88AF3524 ();
// 0x00000BA5 System.Void EpicToonFX.ETFXMouseOrbit::LateUpdate()
extern void ETFXMouseOrbit_LateUpdate_mBEAC3D0CA1272A71C77FB618BBB93D7E0FAF5288 ();
// 0x00000BA6 System.Single EpicToonFX.ETFXMouseOrbit::ClampAngle(System.Single,System.Single,System.Single)
extern void ETFXMouseOrbit_ClampAngle_m0BFE0428C5E1083B89C8F3FB799D5DCAAA71F8C9 ();
// 0x00000BA7 System.Void EpicToonFX.ETFXMouseOrbit::.ctor()
extern void ETFXMouseOrbit__ctor_mAEC1F8A3E26D1976B3038B19598C97744D7C4A06 ();
// 0x00000BA8 System.Void EpicToonFX.ETFXTarget::Start()
extern void ETFXTarget_Start_m14929F3ADEFA5C21C8A7283C80203D1A1847F27F ();
// 0x00000BA9 System.Void EpicToonFX.ETFXTarget::SpawnTarget()
extern void ETFXTarget_SpawnTarget_m62C35F4021F983CA1C7C92E3CF089264493388B5 ();
// 0x00000BAA System.Void EpicToonFX.ETFXTarget::OnTriggerEnter(UnityEngine.Collider)
extern void ETFXTarget_OnTriggerEnter_m162C8D756CE74ECDCF84195C3CDCAD912B5460C8 ();
// 0x00000BAB System.Collections.IEnumerator EpicToonFX.ETFXTarget::Respawn()
extern void ETFXTarget_Respawn_m8F66D3EF6939232B79CC832877653CC525EB3400 ();
// 0x00000BAC System.Void EpicToonFX.ETFXTarget::.ctor()
extern void ETFXTarget__ctor_m15F39EFD8564F3EDBE45ED128398859CA5B49912 ();
// 0x00000BAD System.Void EpicToonFX.ETFXLightFade::Start()
extern void ETFXLightFade_Start_mA462AF495E3DACF7F4E85A658CB6128A7E28EC64 ();
// 0x00000BAE System.Void EpicToonFX.ETFXLightFade::Update()
extern void ETFXLightFade_Update_m4BAB6873FAEAE8041C48007C57DD026BAA1A3ABC ();
// 0x00000BAF System.Void EpicToonFX.ETFXLightFade::.ctor()
extern void ETFXLightFade__ctor_m2EBD208A70788C5E441CA5B8F1FDC5A47A607643 ();
// 0x00000BB0 System.Void EpicToonFX.ETFXPitchRandomizer::Start()
extern void ETFXPitchRandomizer_Start_mAF7B1D827B3F63CAEB42B452B6973CB1159C5440 ();
// 0x00000BB1 System.Void EpicToonFX.ETFXPitchRandomizer::.ctor()
extern void ETFXPitchRandomizer__ctor_m76FECEF1C05DEDE11037FCD96BB901CBF48CFE76 ();
// 0x00000BB2 System.Void EpicToonFX.ETFXRotation::Start()
extern void ETFXRotation_Start_mD30953FFDD78FDB7A0E9A87031EE0161F34545ED ();
// 0x00000BB3 System.Void EpicToonFX.ETFXRotation::Update()
extern void ETFXRotation_Update_m041B694ADF0177DEF52FE9D553B5B48FB2D00225 ();
// 0x00000BB4 System.Void EpicToonFX.ETFXRotation::.ctor()
extern void ETFXRotation__ctor_m757B4E57FE637FF776015DAC0E9DCFF349A61F05 ();
// 0x00000BB5 System.UInt32 <PrivateImplementationDetails>::ComputeStringHash(System.String)
extern void U3CPrivateImplementationDetailsU3E_ComputeStringHash_m94E4EDA2F909FACDA1591F7242109AFB650640AA ();
// 0x00000BB6 System.Void Ally_<>c::.cctor()
extern void U3CU3Ec__cctor_m0476EAFFEC1E78B57D5FB75E908BEF46282FF75E ();
// 0x00000BB7 System.Void Ally_<>c::.ctor()
extern void U3CU3Ec__ctor_m75C62406488B8C0BE95B1C13758F84D4498E57F8 ();
// 0x00000BB8 System.Void Ally_<>c::<GameOver>b__2_0()
extern void U3CU3Ec_U3CGameOverU3Eb__2_0_m814322A84405DD399FC1B608B87F9A4B3D1F6392 ();
// 0x00000BB9 System.Void Character_<>c::.cctor()
extern void U3CU3Ec__cctor_m0DAA011AF413A7518BC813E75E5CC223DF93D06F ();
// 0x00000BBA System.Void Character_<>c::.ctor()
extern void U3CU3Ec__ctor_m4571F1547BE5CA1AE21B9B86A6209E6B23DAA87B ();
// 0x00000BBB System.Void Character_<>c::<.ctor>b__8_0()
extern void U3CU3Ec_U3C_ctorU3Eb__8_0_m93D764F72258D39CB90A8EE0EFC391D5CC357650 ();
// 0x00000BBC System.Void Enemy_<>c::.cctor()
extern void U3CU3Ec__cctor_m95FC62473891AA37FFCC8372B40E103D4575C457 ();
// 0x00000BBD System.Void Enemy_<>c::.ctor()
extern void U3CU3Ec__ctor_m840D892C6E61613DBB2DE45958F6EF5D7D7FB3E0 ();
// 0x00000BBE System.Void Enemy_<>c::<GameOver>b__12_0()
extern void U3CU3Ec_U3CGameOverU3Eb__12_0_m959B53DCA5A405F1F7B5C3A3F3A2774D740144AA ();
// 0x00000BBF System.Void LanguageSelection_<>c::.cctor()
extern void U3CU3Ec__cctor_mD6FA8C29EDA0043A7459A18F7C178A925EE95FBF ();
// 0x00000BC0 System.Void LanguageSelection_<>c::.ctor()
extern void U3CU3Ec__ctor_mE41BEFA8FC5911B4B4D2921D09410B21ADB4C36C ();
// 0x00000BC1 System.Void LanguageSelection_<>c::<Start>b__3_0()
extern void U3CU3Ec_U3CStartU3Eb__3_0_m1EEC307F6879EB5ABF0C6E8E5724AFF25703A38A ();
// 0x00000BC2 System.Void UICenterOnChild_OnCenterCallback::.ctor(System.Object,System.IntPtr)
extern void OnCenterCallback__ctor_mBF612D78CBC3C0DAC4E676A3B742C52B8817C8E3 ();
// 0x00000BC3 System.Void UICenterOnChild_OnCenterCallback::Invoke(UnityEngine.GameObject)
extern void OnCenterCallback_Invoke_m66C62792674B9B5B0588BF318DAC08A130FE537E ();
// 0x00000BC4 System.IAsyncResult UICenterOnChild_OnCenterCallback::BeginInvoke(UnityEngine.GameObject,System.AsyncCallback,System.Object)
extern void OnCenterCallback_BeginInvoke_m728F59F26005BC2EB7E985702FFB92E38B98B2FB ();
// 0x00000BC5 System.Void UICenterOnChild_OnCenterCallback::EndInvoke(System.IAsyncResult)
extern void OnCenterCallback_EndInvoke_m2E03D4B4DE600962B815A6FF65188F0DC055E475 ();
// 0x00000BC6 System.Void UIGrid_OnReposition::.ctor(System.Object,System.IntPtr)
extern void OnReposition__ctor_mBDB60A3EF9BD37E8B02AB98A9CE3DF58A08479F1 ();
// 0x00000BC7 System.Void UIGrid_OnReposition::Invoke()
extern void OnReposition_Invoke_m005DE04E6AA774E3228E3F20A27D1D5C20CBE6C8 ();
// 0x00000BC8 System.IAsyncResult UIGrid_OnReposition::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnReposition_BeginInvoke_m74E39E15739FF210002F82E74F69D197E189CA8C ();
// 0x00000BC9 System.Void UIGrid_OnReposition::EndInvoke(System.IAsyncResult)
extern void OnReposition_EndInvoke_m1639D27F5B83064DD2586D38241C4CB98B6A9DF4 ();
// 0x00000BCA System.Void UIPopupList_LegacyEvent::.ctor(System.Object,System.IntPtr)
extern void LegacyEvent__ctor_mCB864A0E976261DC4911455BA9A9BD1BABEF6C8F ();
// 0x00000BCB System.Void UIPopupList_LegacyEvent::Invoke(System.String)
extern void LegacyEvent_Invoke_m9983F95F876B12F4B176783B48F79D449709D872 ();
// 0x00000BCC System.IAsyncResult UIPopupList_LegacyEvent::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void LegacyEvent_BeginInvoke_m336AD54FCD203C67B3A56BEA52CB1317B0F9E870 ();
// 0x00000BCD System.Void UIPopupList_LegacyEvent::EndInvoke(System.IAsyncResult)
extern void LegacyEvent_EndInvoke_m9CB4A7A87254D4812D6EF5CAE54048BF4CF8D6EC ();
// 0x00000BCE System.Void UIPopupList_<UpdateTweenPosition>d__95::.ctor(System.Int32)
extern void U3CUpdateTweenPositionU3Ed__95__ctor_mF6F85F4707A0F220B836D7201B0BAD1C8737423F ();
// 0x00000BCF System.Void UIPopupList_<UpdateTweenPosition>d__95::System.IDisposable.Dispose()
extern void U3CUpdateTweenPositionU3Ed__95_System_IDisposable_Dispose_mC281BD96ECFBD0C56E769A6B535F2ECD8F804ACE ();
// 0x00000BD0 System.Boolean UIPopupList_<UpdateTweenPosition>d__95::MoveNext()
extern void U3CUpdateTweenPositionU3Ed__95_MoveNext_mA82B590343BEA6EA069B0E7D9F4E21913E78BD4D ();
// 0x00000BD1 System.Object UIPopupList_<UpdateTweenPosition>d__95::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CUpdateTweenPositionU3Ed__95_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5FE092A5E051E69C04A79C20FE942AEDCB0D48D0 ();
// 0x00000BD2 System.Void UIPopupList_<UpdateTweenPosition>d__95::System.Collections.IEnumerator.Reset()
extern void U3CUpdateTweenPositionU3Ed__95_System_Collections_IEnumerator_Reset_mFCFD16651F321D28A6DD8D432411DF23481320C1 ();
// 0x00000BD3 System.Object UIPopupList_<UpdateTweenPosition>d__95::System.Collections.IEnumerator.get_Current()
extern void U3CUpdateTweenPositionU3Ed__95_System_Collections_IEnumerator_get_Current_mB99713ECB4B57DB74CB86079D532165C5FC27396 ();
// 0x00000BD4 System.Void UIPopupList_<CloseIfUnselected>d__112::.ctor(System.Int32)
extern void U3CCloseIfUnselectedU3Ed__112__ctor_mADE91FD63EE442895A20F952D3575839B607EA4E ();
// 0x00000BD5 System.Void UIPopupList_<CloseIfUnselected>d__112::System.IDisposable.Dispose()
extern void U3CCloseIfUnselectedU3Ed__112_System_IDisposable_Dispose_m21CD366C42FFC38D2C8EED6CDD47E083EE94138E ();
// 0x00000BD6 System.Boolean UIPopupList_<CloseIfUnselected>d__112::MoveNext()
extern void U3CCloseIfUnselectedU3Ed__112_MoveNext_m70B6B69BD55399F6BFABB0DCD6606EB4B34C34C6 ();
// 0x00000BD7 System.Object UIPopupList_<CloseIfUnselected>d__112::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCloseIfUnselectedU3Ed__112_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB1D952CE98AA9793CF6DAA33365DEF1B166316AB ();
// 0x00000BD8 System.Void UIPopupList_<CloseIfUnselected>d__112::System.Collections.IEnumerator.Reset()
extern void U3CCloseIfUnselectedU3Ed__112_System_Collections_IEnumerator_Reset_m7D83353CB59FBEB1C6B29AFA4E75BF1DDBF30D67 ();
// 0x00000BD9 System.Object UIPopupList_<CloseIfUnselected>d__112::System.Collections.IEnumerator.get_Current()
extern void U3CCloseIfUnselectedU3Ed__112_System_Collections_IEnumerator_get_Current_m070642FB23267F4EB48E62AD3D7D867EC5CAB9EE ();
// 0x00000BDA System.Void UIProgressBar_OnDragFinished::.ctor(System.Object,System.IntPtr)
extern void OnDragFinished__ctor_m98B2272AD70E848F71F61970D7325C8F4E6E64C7 ();
// 0x00000BDB System.Void UIProgressBar_OnDragFinished::Invoke()
extern void OnDragFinished_Invoke_m1F6654FF4A1B269DC11A55DDB7BF6689598C84F3 ();
// 0x00000BDC System.IAsyncResult UIProgressBar_OnDragFinished::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnDragFinished_BeginInvoke_mA0F597F8C42787A2D2E2ECB408A080823BFCB7F6 ();
// 0x00000BDD System.Void UIProgressBar_OnDragFinished::EndInvoke(System.IAsyncResult)
extern void OnDragFinished_EndInvoke_m60299AA3C158D3868BC32232F2E62FA50054821B ();
// 0x00000BDE System.Void UIScrollView_OnDragNotification::.ctor(System.Object,System.IntPtr)
extern void OnDragNotification__ctor_m87B2B385BE26BDE08131F0C45FC996F42871F418 ();
// 0x00000BDF System.Void UIScrollView_OnDragNotification::Invoke()
extern void OnDragNotification_Invoke_m6DDFE33E5A6CAA9ABBB96F7E20957B60FC7C909B ();
// 0x00000BE0 System.IAsyncResult UIScrollView_OnDragNotification::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnDragNotification_BeginInvoke_m4BD977A7F78F01163259AF420351D4D8F8D2855E ();
// 0x00000BE1 System.Void UIScrollView_OnDragNotification::EndInvoke(System.IAsyncResult)
extern void OnDragNotification_EndInvoke_m60A493670F41C10CA419452BB2CCB80905695C20 ();
// 0x00000BE2 System.Void UITable_OnReposition::.ctor(System.Object,System.IntPtr)
extern void OnReposition__ctor_mE58EB28E0ACF9860CF32C6DE75F511446F5EE21F ();
// 0x00000BE3 System.Void UITable_OnReposition::Invoke()
extern void OnReposition_Invoke_m4AB6FEBE04BA7B5BC2489A4297984AB2A19FF95B ();
// 0x00000BE4 System.IAsyncResult UITable_OnReposition::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnReposition_BeginInvoke_mCCB0C9578A5CE760CFD673638F2362740AB37965 ();
// 0x00000BE5 System.Void UITable_OnReposition::EndInvoke(System.IAsyncResult)
extern void OnReposition_EndInvoke_m2614EA0AE22167FF25AAEC13B0C609F7F70668E3 ();
// 0x00000BE6 System.Void UIToggle_Validate::.ctor(System.Object,System.IntPtr)
extern void Validate__ctor_m980443F8C543354F97027865867D8C62524DF404 ();
// 0x00000BE7 System.Boolean UIToggle_Validate::Invoke(System.Boolean)
extern void Validate_Invoke_mA3A22ACC1D122ECAE1FA0CEE067517125E1CC9B8 ();
// 0x00000BE8 System.IAsyncResult UIToggle_Validate::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern void Validate_BeginInvoke_m0F7A10D0E0D3B13A61AFDC645C74A032E71C9A77 ();
// 0x00000BE9 System.Boolean UIToggle_Validate::EndInvoke(System.IAsyncResult)
extern void Validate_EndInvoke_m1D261E4E7BC51A2E6A684292B0E157A504DDC601 ();
// 0x00000BEA System.Void UIWrapContent_OnInitializeItem::.ctor(System.Object,System.IntPtr)
extern void OnInitializeItem__ctor_mDEE8E1B5AB1F426CF82C8EAEAA1D4F7A2579D208 ();
// 0x00000BEB System.Void UIWrapContent_OnInitializeItem::Invoke(UnityEngine.GameObject,System.Int32,System.Int32)
extern void OnInitializeItem_Invoke_mCF74A0F4A3B754D9ABD20B19B7B5FBE2F1B903DA ();
// 0x00000BEC System.IAsyncResult UIWrapContent_OnInitializeItem::BeginInvoke(UnityEngine.GameObject,System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern void OnInitializeItem_BeginInvoke_mE09B62232113AFC5FB420501A0DD44B3E1CD41DF ();
// 0x00000BED System.Void UIWrapContent_OnInitializeItem::EndInvoke(System.IAsyncResult)
extern void OnInitializeItem_EndInvoke_m2A35CBDC775BD84552119A1CAC4B4DDD069DD609 ();
// 0x00000BEE System.Void BetterList`1_CompareFunc::.ctor(System.Object,System.IntPtr)
// 0x00000BEF System.Int32 BetterList`1_CompareFunc::Invoke(T,T)
// 0x00000BF0 System.IAsyncResult BetterList`1_CompareFunc::BeginInvoke(T,T,System.AsyncCallback,System.Object)
// 0x00000BF1 System.Int32 BetterList`1_CompareFunc::EndInvoke(System.IAsyncResult)
// 0x00000BF2 System.Void BetterList`1_<GetEnumerator>d__2::.ctor(System.Int32)
// 0x00000BF3 System.Void BetterList`1_<GetEnumerator>d__2::System.IDisposable.Dispose()
// 0x00000BF4 System.Boolean BetterList`1_<GetEnumerator>d__2::MoveNext()
// 0x00000BF5 T BetterList`1_<GetEnumerator>d__2::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x00000BF6 System.Void BetterList`1_<GetEnumerator>d__2::System.Collections.IEnumerator.Reset()
// 0x00000BF7 System.Object BetterList`1_<GetEnumerator>d__2::System.Collections.IEnumerator.get_Current()
// 0x00000BF8 System.Void EventDelegate_Parameter::.ctor()
extern void Parameter__ctor_m88A15A738BD3CA6EF6523333C2A1E204694DC234 ();
// 0x00000BF9 System.Void EventDelegate_Parameter::.ctor(UnityEngine.Object,System.String)
extern void Parameter__ctor_m6EE680C01C38FF8F04022196C064E9FF578B0FC7 ();
// 0x00000BFA System.Void EventDelegate_Parameter::.ctor(System.Object)
extern void Parameter__ctor_m99B5162EAEAF52137F3A9EC05AB1A9B60852491A ();
// 0x00000BFB System.Object EventDelegate_Parameter::get_value()
extern void Parameter_get_value_mA79E459D497635A298157F7FACD13BACC8D7FEFD ();
// 0x00000BFC System.Void EventDelegate_Parameter::set_value(System.Object)
extern void Parameter_set_value_mE09EA27482BFCB50B9C76104737510C8599D75B2 ();
// 0x00000BFD System.Type EventDelegate_Parameter::get_type()
extern void Parameter_get_type_mEDBAA9C76FEEF89E30FD7BC3E451117B786BDB6F ();
// 0x00000BFE System.Void EventDelegate_Callback::.ctor(System.Object,System.IntPtr)
extern void Callback__ctor_mF72858DCD8EA9E40CB7DD23564B260E735F557AA ();
// 0x00000BFF System.Void EventDelegate_Callback::Invoke()
extern void Callback_Invoke_m56147085BDC8BD8D70084FDD09E4434D741BA19D ();
// 0x00000C00 System.IAsyncResult EventDelegate_Callback::BeginInvoke(System.AsyncCallback,System.Object)
extern void Callback_BeginInvoke_m015229F63AAC972D4635EB14D51B65DBFF92734A ();
// 0x00000C01 System.Void EventDelegate_Callback::EndInvoke(System.IAsyncResult)
extern void Callback_EndInvoke_m817FB4A68988F32584F6CDC50EB3572BDD77BE67 ();
// 0x00000C02 System.Void Localization_LoadFunction::.ctor(System.Object,System.IntPtr)
extern void LoadFunction__ctor_mC60ABE821C56042C2477249F7F21849DFBB4EBC6 ();
// 0x00000C03 System.Byte[] Localization_LoadFunction::Invoke(System.String)
extern void LoadFunction_Invoke_mFE3A02D386915B3E6EFD9C41BDD0D3171DAE1141 ();
// 0x00000C04 System.IAsyncResult Localization_LoadFunction::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void LoadFunction_BeginInvoke_mF0B7277566999A1EB09B30747E9EA6705F5B3CD7 ();
// 0x00000C05 System.Byte[] Localization_LoadFunction::EndInvoke(System.IAsyncResult)
extern void LoadFunction_EndInvoke_mF44BF8CA7BD5B6C267B94F493D7F32A9B97B9DAC ();
// 0x00000C06 System.Void Localization_OnLocalizeNotification::.ctor(System.Object,System.IntPtr)
extern void OnLocalizeNotification__ctor_mCE9D2335B6D636DA743AA4CA88B24FA9C48D5BFD ();
// 0x00000C07 System.Void Localization_OnLocalizeNotification::Invoke()
extern void OnLocalizeNotification_Invoke_m1D019485A85BBE1AFCAEB8260E1C16150A601810 ();
// 0x00000C08 System.IAsyncResult Localization_OnLocalizeNotification::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnLocalizeNotification_BeginInvoke_mC72C240C5B205E5C23FD7DAFED4675DE65CA6BEE ();
// 0x00000C09 System.Void Localization_OnLocalizeNotification::EndInvoke(System.IAsyncResult)
extern void OnLocalizeNotification_EndInvoke_m0CCB59F0ADA19AB445033499D38986B47FC17B65 ();
// 0x00000C0A System.Void NGUIText_GlyphInfo::.ctor()
extern void GlyphInfo__ctor_m3635C9640998839EBE8F00634471F593726EF651 ();
// 0x00000C0B System.Void NGUITools_OnInitFunc`1::.ctor(System.Object,System.IntPtr)
// 0x00000C0C System.Void NGUITools_OnInitFunc`1::Invoke(T)
// 0x00000C0D System.IAsyncResult NGUITools_OnInitFunc`1::BeginInvoke(T,System.AsyncCallback,System.Object)
// 0x00000C0E System.Void NGUITools_OnInitFunc`1::EndInvoke(System.IAsyncResult)
// 0x00000C0F System.Void SpringPanel_OnFinished::.ctor(System.Object,System.IntPtr)
extern void OnFinished__ctor_m864708B3576F231EB8385331A3DD9CA56CA4B8EB ();
// 0x00000C10 System.Void SpringPanel_OnFinished::Invoke()
extern void OnFinished_Invoke_m30357841ED6CC5D88E41D28EF0AFEB64AF0F7B18 ();
// 0x00000C11 System.IAsyncResult SpringPanel_OnFinished::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnFinished_BeginInvoke_m5894A288E4C5675C13141587A59A0997847DBE22 ();
// 0x00000C12 System.Void SpringPanel_OnFinished::EndInvoke(System.IAsyncResult)
extern void OnFinished_EndInvoke_m25AF02AB1C599145508221BB5505DF67215BF42E ();
// 0x00000C13 System.Void UIDrawCall_OnRenderCallback::.ctor(System.Object,System.IntPtr)
extern void OnRenderCallback__ctor_m98D1DF40846630BB8189D761B6FF468194CE615F ();
// 0x00000C14 System.Void UIDrawCall_OnRenderCallback::Invoke(UnityEngine.Material)
extern void OnRenderCallback_Invoke_m179262E5F72E71B0CA5057099D2F450CD012B46F ();
// 0x00000C15 System.IAsyncResult UIDrawCall_OnRenderCallback::BeginInvoke(UnityEngine.Material,System.AsyncCallback,System.Object)
extern void OnRenderCallback_BeginInvoke_m79CA23D666496ADC162FDC5FBCCF609C02D9DFBA ();
// 0x00000C16 System.Void UIDrawCall_OnRenderCallback::EndInvoke(System.IAsyncResult)
extern void OnRenderCallback_EndInvoke_mF63DD136D23E567238BC5D9E1AE2136B8DA7288C ();
// 0x00000C17 System.Void UIDrawCall_OnCreateDrawCall::.ctor(System.Object,System.IntPtr)
extern void OnCreateDrawCall__ctor_m69E25DA9AF36D1A764D008E67A8500966F6876E4 ();
// 0x00000C18 System.Void UIDrawCall_OnCreateDrawCall::Invoke(UIDrawCall,UnityEngine.MeshFilter,UnityEngine.MeshRenderer)
extern void OnCreateDrawCall_Invoke_mEA1A091DC895CD8FCAE1A7ADB61D69F2FBAEC22D ();
// 0x00000C19 System.IAsyncResult UIDrawCall_OnCreateDrawCall::BeginInvoke(UIDrawCall,UnityEngine.MeshFilter,UnityEngine.MeshRenderer,System.AsyncCallback,System.Object)
extern void OnCreateDrawCall_BeginInvoke_m09B9008C452D714DDC9CD1AB484102EC178E9FE6 ();
// 0x00000C1A System.Void UIDrawCall_OnCreateDrawCall::EndInvoke(System.IAsyncResult)
extern void OnCreateDrawCall_EndInvoke_mDF385DDCFA67C985C189990F9884501337D2C38B ();
// 0x00000C1B System.Void UIEventListener_VoidDelegate::.ctor(System.Object,System.IntPtr)
extern void VoidDelegate__ctor_m0BD74AEEC171523CAF4C457AAFA2A6006C41818F ();
// 0x00000C1C System.Void UIEventListener_VoidDelegate::Invoke(UnityEngine.GameObject)
extern void VoidDelegate_Invoke_m20F76283CB9A815F1B98C4D10CC4A12EF037DAAB ();
// 0x00000C1D System.IAsyncResult UIEventListener_VoidDelegate::BeginInvoke(UnityEngine.GameObject,System.AsyncCallback,System.Object)
extern void VoidDelegate_BeginInvoke_mAB15AE42F5C236A675FC525535D42166E489C50E ();
// 0x00000C1E System.Void UIEventListener_VoidDelegate::EndInvoke(System.IAsyncResult)
extern void VoidDelegate_EndInvoke_m081E2FFB87C588444AFBFA2CF7A9D5B425BCF6BB ();
// 0x00000C1F System.Void UIEventListener_BoolDelegate::.ctor(System.Object,System.IntPtr)
extern void BoolDelegate__ctor_m3A5C9DE3AC0781AE00FCCDCBF9365107F4663750 ();
// 0x00000C20 System.Void UIEventListener_BoolDelegate::Invoke(UnityEngine.GameObject,System.Boolean)
extern void BoolDelegate_Invoke_mF5EF3D494D1F4FFF5DCDD1258CFC5C6754BD7BB2 ();
// 0x00000C21 System.IAsyncResult UIEventListener_BoolDelegate::BeginInvoke(UnityEngine.GameObject,System.Boolean,System.AsyncCallback,System.Object)
extern void BoolDelegate_BeginInvoke_m2F9CCD27873E5C2C9F09E3CB20750BBAFF8BDD68 ();
// 0x00000C22 System.Void UIEventListener_BoolDelegate::EndInvoke(System.IAsyncResult)
extern void BoolDelegate_EndInvoke_m829CA47ECB42BD692F410DA835DFA645D4CFFBB2 ();
// 0x00000C23 System.Void UIEventListener_FloatDelegate::.ctor(System.Object,System.IntPtr)
extern void FloatDelegate__ctor_m1345BC7305F929D709154FE857A97158376106D3 ();
// 0x00000C24 System.Void UIEventListener_FloatDelegate::Invoke(UnityEngine.GameObject,System.Single)
extern void FloatDelegate_Invoke_m5163C356F4D7835D0DC110F938D8E74654942DA5 ();
// 0x00000C25 System.IAsyncResult UIEventListener_FloatDelegate::BeginInvoke(UnityEngine.GameObject,System.Single,System.AsyncCallback,System.Object)
extern void FloatDelegate_BeginInvoke_m9B6B884587800515ABC5C654DFE8D24F13E550C2 ();
// 0x00000C26 System.Void UIEventListener_FloatDelegate::EndInvoke(System.IAsyncResult)
extern void FloatDelegate_EndInvoke_m208D74B0CBEB2C4C74ED8BC4015573402C687388 ();
// 0x00000C27 System.Void UIEventListener_VectorDelegate::.ctor(System.Object,System.IntPtr)
extern void VectorDelegate__ctor_m1145066CF4743709B64685BD5A9BEEF846B0E8A3 ();
// 0x00000C28 System.Void UIEventListener_VectorDelegate::Invoke(UnityEngine.GameObject,UnityEngine.Vector2)
extern void VectorDelegate_Invoke_m9B9B1EC2E08FE0E4D104D722230A35C76713561D ();
// 0x00000C29 System.IAsyncResult UIEventListener_VectorDelegate::BeginInvoke(UnityEngine.GameObject,UnityEngine.Vector2,System.AsyncCallback,System.Object)
extern void VectorDelegate_BeginInvoke_m1FB6369E942436A0DB2854284BE007E3D05DC688 ();
// 0x00000C2A System.Void UIEventListener_VectorDelegate::EndInvoke(System.IAsyncResult)
extern void VectorDelegate_EndInvoke_m251EEB4BBA4389C6467E84F0BBC67B942168B590 ();
// 0x00000C2B System.Void UIEventListener_ObjectDelegate::.ctor(System.Object,System.IntPtr)
extern void ObjectDelegate__ctor_m1F69F6E7E56749FF7F6542613B0355A87975E958 ();
// 0x00000C2C System.Void UIEventListener_ObjectDelegate::Invoke(UnityEngine.GameObject,UnityEngine.GameObject)
extern void ObjectDelegate_Invoke_mC5D9522470E7B8B56E311EA865A79E738BFB3976 ();
// 0x00000C2D System.IAsyncResult UIEventListener_ObjectDelegate::BeginInvoke(UnityEngine.GameObject,UnityEngine.GameObject,System.AsyncCallback,System.Object)
extern void ObjectDelegate_BeginInvoke_m4810A5A7AD8495394A7EE6DDA24B34C9DFBD3DC9 ();
// 0x00000C2E System.Void UIEventListener_ObjectDelegate::EndInvoke(System.IAsyncResult)
extern void ObjectDelegate_EndInvoke_mF7FD8E99F05208B3E44296FE72D6C6B9DB6E431F ();
// 0x00000C2F System.Void UIEventListener_KeyCodeDelegate::.ctor(System.Object,System.IntPtr)
extern void KeyCodeDelegate__ctor_mFE3D673253C02C9D526FA9DCFCD4EEAD15DC7A3C ();
// 0x00000C30 System.Void UIEventListener_KeyCodeDelegate::Invoke(UnityEngine.GameObject,UnityEngine.KeyCode)
extern void KeyCodeDelegate_Invoke_m1DA6729D2575C393E2A9ACF8EF38BB206FCEEE6B ();
// 0x00000C31 System.IAsyncResult UIEventListener_KeyCodeDelegate::BeginInvoke(UnityEngine.GameObject,UnityEngine.KeyCode,System.AsyncCallback,System.Object)
extern void KeyCodeDelegate_BeginInvoke_mFEF6B8098466A2F4FFB330A3C6BA767F1D627AB1 ();
// 0x00000C32 System.Void UIEventListener_KeyCodeDelegate::EndInvoke(System.IAsyncResult)
extern void KeyCodeDelegate_EndInvoke_mA871947CA6849101224B7F917A624C661E08EEF5 ();
// 0x00000C33 System.Void UIGeometry_OnCustomWrite::.ctor(System.Object,System.IntPtr)
extern void OnCustomWrite__ctor_m58B1DF46104BAAECE5463266181F0A7E37697548 ();
// 0x00000C34 System.Void UIGeometry_OnCustomWrite::Invoke(System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Color>,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector4>,System.Collections.Generic.List`1<UnityEngine.Vector4>)
extern void OnCustomWrite_Invoke_mB289805ADD15A2C04742C43A23E9D2E078517617 ();
// 0x00000C35 System.IAsyncResult UIGeometry_OnCustomWrite::BeginInvoke(System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Color>,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector4>,System.Collections.Generic.List`1<UnityEngine.Vector4>,System.AsyncCallback,System.Object)
extern void OnCustomWrite_BeginInvoke_mA101B90462E30CAD9320643719B70BD98E64DC14 ();
// 0x00000C36 System.Void UIGeometry_OnCustomWrite::EndInvoke(System.IAsyncResult)
extern void OnCustomWrite_EndInvoke_m9E054DA43564BF302A0EDD2AF039D076D6A09A6C ();
// 0x00000C37 System.Void UIRect_AnchorPoint::.ctor()
extern void AnchorPoint__ctor_m418EA0B6A22EF969625837BDD56B451B151C9729 ();
// 0x00000C38 System.Void UIRect_AnchorPoint::.ctor(System.Single)
extern void AnchorPoint__ctor_m40BBE485D38C9C22CC6458B7A1FB2DB9E641EA17 ();
// 0x00000C39 System.Void UIRect_AnchorPoint::Set(System.Single,System.Single)
extern void AnchorPoint_Set_m594521F81974BD610B958A41AF7E21E5DDB7B95D ();
// 0x00000C3A System.Void UIRect_AnchorPoint::Set(UnityEngine.Transform,System.Single,System.Single)
extern void AnchorPoint_Set_mA772B011DE2BBB0E63CB5842C9ADEA4AD9003D48 ();
// 0x00000C3B System.Void UIRect_AnchorPoint::SetToNearest(System.Single,System.Single,System.Single)
extern void AnchorPoint_SetToNearest_mD9929C17E8C44D23971C2542BBA572CCEB52F1C5 ();
// 0x00000C3C System.Void UIRect_AnchorPoint::SetToNearest(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void AnchorPoint_SetToNearest_m2D84C27C37908C0C3D67E0FDB83AFE5FE800371C ();
// 0x00000C3D System.Void UIRect_AnchorPoint::SetHorizontal(UnityEngine.Transform,System.Single)
extern void AnchorPoint_SetHorizontal_m4BD6D6B8D8EEAFB80B016C93173E0AC9D242CC43 ();
// 0x00000C3E System.Void UIRect_AnchorPoint::SetVertical(UnityEngine.Transform,System.Single)
extern void AnchorPoint_SetVertical_m96259F14398A794B1F7E18ADE848A0D8677EF635 ();
// 0x00000C3F UnityEngine.Vector3[] UIRect_AnchorPoint::GetSides(UnityEngine.Transform)
extern void AnchorPoint_GetSides_m76661F2AEC2A0A35D80AB33CB3D53F8D53BDD0F2 ();
// 0x00000C40 System.Void UIWidget_OnDimensionsChanged::.ctor(System.Object,System.IntPtr)
extern void OnDimensionsChanged__ctor_mC9FF65197698E29B257317CE43427E1B1D4D6ED0 ();
// 0x00000C41 System.Void UIWidget_OnDimensionsChanged::Invoke()
extern void OnDimensionsChanged_Invoke_m7D06982F06C93AD0D419C4F6A4196FEAE3598FB0 ();
// 0x00000C42 System.IAsyncResult UIWidget_OnDimensionsChanged::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnDimensionsChanged_BeginInvoke_mF5E796F0066C64F80C85A6B4196DEAF4D425DF41 ();
// 0x00000C43 System.Void UIWidget_OnDimensionsChanged::EndInvoke(System.IAsyncResult)
extern void OnDimensionsChanged_EndInvoke_m5D1AEE87E54B8FF4E685410A611C910C648D1854 ();
// 0x00000C44 System.Void UIWidget_OnPostFillCallback::.ctor(System.Object,System.IntPtr)
extern void OnPostFillCallback__ctor_mBFD3A46DF1368B7C78AFF06D210DC5CB87D14AF1 ();
// 0x00000C45 System.Void UIWidget_OnPostFillCallback::Invoke(UIWidget,System.Int32,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Color>)
extern void OnPostFillCallback_Invoke_m85560B84A08BB10B28BB3EEC6FD33F194B3451D7 ();
// 0x00000C46 System.IAsyncResult UIWidget_OnPostFillCallback::BeginInvoke(UIWidget,System.Int32,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Color>,System.AsyncCallback,System.Object)
extern void OnPostFillCallback_BeginInvoke_m2F8A89A57343BDBC3982C0C336764C4A8EDFCC54 ();
// 0x00000C47 System.Void UIWidget_OnPostFillCallback::EndInvoke(System.IAsyncResult)
extern void OnPostFillCallback_EndInvoke_mFD2D2824BD07AEE88BC0AC8CAC257509E38427ED ();
// 0x00000C48 System.Void UIWidget_HitCheck::.ctor(System.Object,System.IntPtr)
extern void HitCheck__ctor_m4E11157331C0C0DD9C3CFE9227E24C8D38482C9E ();
// 0x00000C49 System.Boolean UIWidget_HitCheck::Invoke(UnityEngine.Vector3)
extern void HitCheck_Invoke_m1CD630F27E7F0D9012BECA40F84969B20AF52F32 ();
// 0x00000C4A System.IAsyncResult UIWidget_HitCheck::BeginInvoke(UnityEngine.Vector3,System.AsyncCallback,System.Object)
extern void HitCheck_BeginInvoke_mCC172A07CF1404929B039A7BB0F49293524CFF1F ();
// 0x00000C4B System.Boolean UIWidget_HitCheck::EndInvoke(System.IAsyncResult)
extern void HitCheck_EndInvoke_m9CD0A2AAA5C75121A575C4A5CE72FFE8A8EC2ADE ();
// 0x00000C4C System.Void SpringPosition_OnFinished::.ctor(System.Object,System.IntPtr)
extern void OnFinished__ctor_mD285DD6CE3F515BD4D80406DC8E4FF29947D33E1 ();
// 0x00000C4D System.Void SpringPosition_OnFinished::Invoke()
extern void OnFinished_Invoke_mD54C2497223475006CA65FAB1210D8512E7E68A1 ();
// 0x00000C4E System.IAsyncResult SpringPosition_OnFinished::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnFinished_BeginInvoke_m881D6CF6D7D47F803A262779BD8A9812D30F8782 ();
// 0x00000C4F System.Void SpringPosition_OnFinished::EndInvoke(System.IAsyncResult)
extern void OnFinished_EndInvoke_mCCE36654DC997F6767D3188B240772BE97F0E3C6 ();
// 0x00000C50 System.Void NGUIAtlas_<>c::.cctor()
extern void U3CU3Ec__cctor_m2581C46CB4460452A5DF41ECFE93AFF22D87451E ();
// 0x00000C51 System.Void NGUIAtlas_<>c::.ctor()
extern void U3CU3Ec__ctor_mAEF85074E8CCDA308E60FF8632486F71B115684A ();
// 0x00000C52 System.Int32 NGUIAtlas_<>c::<SortAlphabetically>b__25_0(UISpriteData,UISpriteData)
extern void U3CU3Ec_U3CSortAlphabeticallyU3Eb__25_0_m08140D7E115D4C5C0413C0F6E8306A063655F6B3 ();
// 0x00000C53 System.Boolean UIAtlas_Sprite::get_hasPadding()
extern void Sprite_get_hasPadding_m4915522716DE65C14FACCC99D42C0C9CD678BC59 ();
// 0x00000C54 System.Void UIAtlas_Sprite::.ctor()
extern void Sprite__ctor_m12BE409981A75397D17DC860F5B24614E86CA4D5 ();
// 0x00000C55 System.Void UIAtlas_<>c::.cctor()
extern void U3CU3Ec__cctor_m1C9A69CC15AC41C77835EDC387E4633FC53C20A6 ();
// 0x00000C56 System.Void UIAtlas_<>c::.ctor()
extern void U3CU3Ec__ctor_m3F11CDE543FC5464E77DA56D0690B1FE826376C2 ();
// 0x00000C57 System.Int32 UIAtlas_<>c::<SortAlphabetically>b__28_0(UISpriteData,UISpriteData)
extern void U3CU3Ec_U3CSortAlphabeticallyU3Eb__28_0_m0D9BB3123551623DE9DC5136EBD4A5C8F6DB35B2 ();
// 0x00000C58 System.Single UICamera_MouseOrTouch::get_deltaTime()
extern void MouseOrTouch_get_deltaTime_m61642D293BE0AB6C7BFE4F97CB2956E476A97823 ();
// 0x00000C59 System.Boolean UICamera_MouseOrTouch::get_isOverUI()
extern void MouseOrTouch_get_isOverUI_m6C98E84E4FD59CEA007CE5A87E847623A7CB7520 ();
// 0x00000C5A System.Void UICamera_MouseOrTouch::.ctor()
extern void MouseOrTouch__ctor_m7C8494D4386F8F36612AB4DFED0D839244D6E6B4 ();
// 0x00000C5B System.Void UICamera_GetKeyStateFunc::.ctor(System.Object,System.IntPtr)
extern void GetKeyStateFunc__ctor_m902EBDB0C9147A64A6E756B80ADD5131ABCECB3F ();
// 0x00000C5C System.Boolean UICamera_GetKeyStateFunc::Invoke(UnityEngine.KeyCode)
extern void GetKeyStateFunc_Invoke_m8887CBDC5A66CF48D877B8F185490EADC35F7B06 ();
// 0x00000C5D System.IAsyncResult UICamera_GetKeyStateFunc::BeginInvoke(UnityEngine.KeyCode,System.AsyncCallback,System.Object)
extern void GetKeyStateFunc_BeginInvoke_m607EFAE71539E2B076BBC5C104A132C17E4081E3 ();
// 0x00000C5E System.Boolean UICamera_GetKeyStateFunc::EndInvoke(System.IAsyncResult)
extern void GetKeyStateFunc_EndInvoke_mEF7E36B05AB97A413D5619CD66AAD92D436AADA0 ();
// 0x00000C5F System.Void UICamera_GetAxisFunc::.ctor(System.Object,System.IntPtr)
extern void GetAxisFunc__ctor_m3507562A17C64A2B462833593B17786A530F70CB ();
// 0x00000C60 System.Single UICamera_GetAxisFunc::Invoke(System.String)
extern void GetAxisFunc_Invoke_m9292371C72D0FD4B63B36E1A899E76D6B0B308AB ();
// 0x00000C61 System.IAsyncResult UICamera_GetAxisFunc::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void GetAxisFunc_BeginInvoke_m3930C8B9D567BC0434716E50764053A5381C96E8 ();
// 0x00000C62 System.Single UICamera_GetAxisFunc::EndInvoke(System.IAsyncResult)
extern void GetAxisFunc_EndInvoke_mFBBFFA36546EB1127E120320CCE2975E3F14DABF ();
// 0x00000C63 System.Void UICamera_GetAnyKeyFunc::.ctor(System.Object,System.IntPtr)
extern void GetAnyKeyFunc__ctor_mAC645003F496DF5DD8CD50CB6C4CDF3AADA8BA38 ();
// 0x00000C64 System.Boolean UICamera_GetAnyKeyFunc::Invoke()
extern void GetAnyKeyFunc_Invoke_m48C991DFC5BC69AD63C9BD2A2FECD1D49670E9B1 ();
// 0x00000C65 System.IAsyncResult UICamera_GetAnyKeyFunc::BeginInvoke(System.AsyncCallback,System.Object)
extern void GetAnyKeyFunc_BeginInvoke_mFBCEC86DDCA5B9CAFC2516ED37131AA52B002E50 ();
// 0x00000C66 System.Boolean UICamera_GetAnyKeyFunc::EndInvoke(System.IAsyncResult)
extern void GetAnyKeyFunc_EndInvoke_mAEE55F57AC59E6D6180CF0279A6004D8B1FEC2D0 ();
// 0x00000C67 System.Void UICamera_GetMouseDelegate::.ctor(System.Object,System.IntPtr)
extern void GetMouseDelegate__ctor_m4F2A5A110B13B87DF96076A854677ED761CAA6A4 ();
// 0x00000C68 UICamera_MouseOrTouch UICamera_GetMouseDelegate::Invoke(System.Int32)
extern void GetMouseDelegate_Invoke_m57A3FD1A256C09224664172037A5EBB6B5F7EC3F ();
// 0x00000C69 System.IAsyncResult UICamera_GetMouseDelegate::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern void GetMouseDelegate_BeginInvoke_mD9FBEC43663FC02C811672754B3BAA448081791C ();
// 0x00000C6A UICamera_MouseOrTouch UICamera_GetMouseDelegate::EndInvoke(System.IAsyncResult)
extern void GetMouseDelegate_EndInvoke_mA8380AAEE17E1FB9D639F8ACBA96B3AAA944740F ();
// 0x00000C6B System.Void UICamera_GetTouchDelegate::.ctor(System.Object,System.IntPtr)
extern void GetTouchDelegate__ctor_m234AEAE070A7550ABEF74C93C96E45A0406C96E1 ();
// 0x00000C6C UICamera_MouseOrTouch UICamera_GetTouchDelegate::Invoke(System.Int32,System.Boolean)
extern void GetTouchDelegate_Invoke_mFE2519A8303A604B0207A63D0A95100FA15D8704 ();
// 0x00000C6D System.IAsyncResult UICamera_GetTouchDelegate::BeginInvoke(System.Int32,System.Boolean,System.AsyncCallback,System.Object)
extern void GetTouchDelegate_BeginInvoke_m5B73F516E477ACC3373C571362B972702B29677F ();
// 0x00000C6E UICamera_MouseOrTouch UICamera_GetTouchDelegate::EndInvoke(System.IAsyncResult)
extern void GetTouchDelegate_EndInvoke_mCCBF86FA03C853D03D3A957A332B7E9B81107C42 ();
// 0x00000C6F System.Void UICamera_RemoveTouchDelegate::.ctor(System.Object,System.IntPtr)
extern void RemoveTouchDelegate__ctor_m8B7326BACB4B88B5FB5074308F29F81B788E9103 ();
// 0x00000C70 System.Void UICamera_RemoveTouchDelegate::Invoke(System.Int32)
extern void RemoveTouchDelegate_Invoke_m9A7522640F843009DE48CD1F3AADD311193E9C0F ();
// 0x00000C71 System.IAsyncResult UICamera_RemoveTouchDelegate::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern void RemoveTouchDelegate_BeginInvoke_m835A39096B784ADFD230527B6583B6472C2D6269 ();
// 0x00000C72 System.Void UICamera_RemoveTouchDelegate::EndInvoke(System.IAsyncResult)
extern void RemoveTouchDelegate_EndInvoke_m1F597B0C173486C414607800BC8B8BD765EAA112 ();
// 0x00000C73 System.Void UICamera_OnScreenResize::.ctor(System.Object,System.IntPtr)
extern void OnScreenResize__ctor_m41EAB9625A6AA728BB91060CFEEE2FF16BBDC12C ();
// 0x00000C74 System.Void UICamera_OnScreenResize::Invoke()
extern void OnScreenResize_Invoke_m44606D87EFE690FEA51485532AC2F972B862D28D ();
// 0x00000C75 System.IAsyncResult UICamera_OnScreenResize::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnScreenResize_BeginInvoke_m2B29DF67A65E1561A88B62B6A99E4831C247F98B ();
// 0x00000C76 System.Void UICamera_OnScreenResize::EndInvoke(System.IAsyncResult)
extern void OnScreenResize_EndInvoke_m3A59F41499F4BB970375EEAA487187651F7171CF ();
// 0x00000C77 System.Void UICamera_OnCustomInput::.ctor(System.Object,System.IntPtr)
extern void OnCustomInput__ctor_m553736C2A976FDBFE48CD8FEDB31B4408C7EA9B4 ();
// 0x00000C78 System.Void UICamera_OnCustomInput::Invoke()
extern void OnCustomInput_Invoke_m2C432208C697A319D89D8C77BE5BFB572FAF48C1 ();
// 0x00000C79 System.IAsyncResult UICamera_OnCustomInput::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnCustomInput_BeginInvoke_m7563B9A9989E513E4EA0A12979D5FE3C3A98B7BE ();
// 0x00000C7A System.Void UICamera_OnCustomInput::EndInvoke(System.IAsyncResult)
extern void OnCustomInput_EndInvoke_m0AC15F714E4B546AC5547840FD80DC655C578350 ();
// 0x00000C7B System.Void UICamera_OnSchemeChange::.ctor(System.Object,System.IntPtr)
extern void OnSchemeChange__ctor_m6F01271151A14842B7FFE26304988391F1C36D58 ();
// 0x00000C7C System.Void UICamera_OnSchemeChange::Invoke()
extern void OnSchemeChange_Invoke_m4FE60DACBE712EAFFAA79910BFB9EFE8ED9BE04F ();
// 0x00000C7D System.IAsyncResult UICamera_OnSchemeChange::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnSchemeChange_BeginInvoke_m3B014E86F9373F61EA6A6DCAB5B1E69F19DE3214 ();
// 0x00000C7E System.Void UICamera_OnSchemeChange::EndInvoke(System.IAsyncResult)
extern void OnSchemeChange_EndInvoke_mCC667EC1714F54FDB9B66B5B04C0E1EEAAF33A17 ();
// 0x00000C7F System.Void UICamera_MoveDelegate::.ctor(System.Object,System.IntPtr)
extern void MoveDelegate__ctor_m90F4B762E8BA4A5E9ADF868CFBC220CDAB35B921 ();
// 0x00000C80 System.Void UICamera_MoveDelegate::Invoke(UnityEngine.Vector2)
extern void MoveDelegate_Invoke_mF95AA440CD1119361BE94BE1821F7055B435B18A ();
// 0x00000C81 System.IAsyncResult UICamera_MoveDelegate::BeginInvoke(UnityEngine.Vector2,System.AsyncCallback,System.Object)
extern void MoveDelegate_BeginInvoke_m7E6C64DE3161988587D800077B363634D0AEC0DE ();
// 0x00000C82 System.Void UICamera_MoveDelegate::EndInvoke(System.IAsyncResult)
extern void MoveDelegate_EndInvoke_m330E805AF0E20BE279317A79BD522A55C33E5266 ();
// 0x00000C83 System.Void UICamera_VoidDelegate::.ctor(System.Object,System.IntPtr)
extern void VoidDelegate__ctor_mAB06CF2953E1ECE70E59A9A8B2D5AB4DE72AF093 ();
// 0x00000C84 System.Void UICamera_VoidDelegate::Invoke(UnityEngine.GameObject)
extern void VoidDelegate_Invoke_m833F6B7FC8E21D70CC02AB80D420120D76687C8A ();
// 0x00000C85 System.IAsyncResult UICamera_VoidDelegate::BeginInvoke(UnityEngine.GameObject,System.AsyncCallback,System.Object)
extern void VoidDelegate_BeginInvoke_m45786749894962B4BBF6E75A94A8F97A71215291 ();
// 0x00000C86 System.Void UICamera_VoidDelegate::EndInvoke(System.IAsyncResult)
extern void VoidDelegate_EndInvoke_mD64315ED52C709799E754F7A9D229B3405FDA114 ();
// 0x00000C87 System.Void UICamera_BoolDelegate::.ctor(System.Object,System.IntPtr)
extern void BoolDelegate__ctor_mE69C983DBF33C662C70C79867C4A6488D8D928DF ();
// 0x00000C88 System.Void UICamera_BoolDelegate::Invoke(UnityEngine.GameObject,System.Boolean)
extern void BoolDelegate_Invoke_m7FD4D3CBFA4243FF519C5F673DE5BD842166F548 ();
// 0x00000C89 System.IAsyncResult UICamera_BoolDelegate::BeginInvoke(UnityEngine.GameObject,System.Boolean,System.AsyncCallback,System.Object)
extern void BoolDelegate_BeginInvoke_m07C98BD35B81353B0F0B657D8301C063B0F3F888 ();
// 0x00000C8A System.Void UICamera_BoolDelegate::EndInvoke(System.IAsyncResult)
extern void BoolDelegate_EndInvoke_m97B03B2C33BDFBE510FCE47A9ABAC31A10CA559F ();
// 0x00000C8B System.Void UICamera_FloatDelegate::.ctor(System.Object,System.IntPtr)
extern void FloatDelegate__ctor_m22F709AB6E7496ED8106DD3A42839ECF72AB64DF ();
// 0x00000C8C System.Void UICamera_FloatDelegate::Invoke(UnityEngine.GameObject,System.Single)
extern void FloatDelegate_Invoke_m7A47FC0CCEAE6FF089A92B3D7DF75378E67C6CC1 ();
// 0x00000C8D System.IAsyncResult UICamera_FloatDelegate::BeginInvoke(UnityEngine.GameObject,System.Single,System.AsyncCallback,System.Object)
extern void FloatDelegate_BeginInvoke_mEC7B52B00353EE3A27D2B198276755CE0E13AB31 ();
// 0x00000C8E System.Void UICamera_FloatDelegate::EndInvoke(System.IAsyncResult)
extern void FloatDelegate_EndInvoke_mEDBC241554B63A0D70AB3D1AA2DA499D7D753D5B ();
// 0x00000C8F System.Void UICamera_VectorDelegate::.ctor(System.Object,System.IntPtr)
extern void VectorDelegate__ctor_mF5784C25BCF2757FA330FE7A7E97B0E510C1EE32 ();
// 0x00000C90 System.Void UICamera_VectorDelegate::Invoke(UnityEngine.GameObject,UnityEngine.Vector2)
extern void VectorDelegate_Invoke_mD055326A7AA3F9DF9584010B519B33DF4F8F9095 ();
// 0x00000C91 System.IAsyncResult UICamera_VectorDelegate::BeginInvoke(UnityEngine.GameObject,UnityEngine.Vector2,System.AsyncCallback,System.Object)
extern void VectorDelegate_BeginInvoke_m087C26F4CD76031766ADA967AC924F0F781326AE ();
// 0x00000C92 System.Void UICamera_VectorDelegate::EndInvoke(System.IAsyncResult)
extern void VectorDelegate_EndInvoke_m1F8EDE156B005C7A7F4B706D66DA85B3AEB9F0B4 ();
// 0x00000C93 System.Void UICamera_ObjectDelegate::.ctor(System.Object,System.IntPtr)
extern void ObjectDelegate__ctor_m705C0ED35498E475A22CA33D553027329BC11670 ();
// 0x00000C94 System.Void UICamera_ObjectDelegate::Invoke(UnityEngine.GameObject,UnityEngine.GameObject)
extern void ObjectDelegate_Invoke_mEC73F6302EB8D11E66323705EFF4825C5D49D7AE ();
// 0x00000C95 System.IAsyncResult UICamera_ObjectDelegate::BeginInvoke(UnityEngine.GameObject,UnityEngine.GameObject,System.AsyncCallback,System.Object)
extern void ObjectDelegate_BeginInvoke_m1A3CC3C1DA4675B9CF1BA2686387EA3C7F67DBC2 ();
// 0x00000C96 System.Void UICamera_ObjectDelegate::EndInvoke(System.IAsyncResult)
extern void ObjectDelegate_EndInvoke_m5E7C456CD495E4D4972AD2C7B03DA9E5E40034B8 ();
// 0x00000C97 System.Void UICamera_KeyCodeDelegate::.ctor(System.Object,System.IntPtr)
extern void KeyCodeDelegate__ctor_mEA1BC191B11E9AD8C2657292D04E2F031BACB719 ();
// 0x00000C98 System.Void UICamera_KeyCodeDelegate::Invoke(UnityEngine.GameObject,UnityEngine.KeyCode)
extern void KeyCodeDelegate_Invoke_m276224B225A66BF9BA5CD6658DCA91B26C6E8712 ();
// 0x00000C99 System.IAsyncResult UICamera_KeyCodeDelegate::BeginInvoke(UnityEngine.GameObject,UnityEngine.KeyCode,System.AsyncCallback,System.Object)
extern void KeyCodeDelegate_BeginInvoke_mC65DA293F8FB8396A2F7CBF2170D2365232A2344 ();
// 0x00000C9A System.Void UICamera_KeyCodeDelegate::EndInvoke(System.IAsyncResult)
extern void KeyCodeDelegate_EndInvoke_m09A7E4BDEE8F9B8B35D81928FB395C4FE3D54CF9 ();
// 0x00000C9B System.Void UICamera_Touch::.ctor()
extern void Touch__ctor_m9D6453DC218BCD5C244816C0DA41E79B288C9072 ();
// 0x00000C9C System.Void UICamera_GetTouchCountCallback::.ctor(System.Object,System.IntPtr)
extern void GetTouchCountCallback__ctor_m90C1FFE3AAA313202BF417834D579B476038D81F ();
// 0x00000C9D System.Int32 UICamera_GetTouchCountCallback::Invoke()
extern void GetTouchCountCallback_Invoke_m0799C7AD4AC8CAE82DC069300E0C27DC3A4576BA ();
// 0x00000C9E System.IAsyncResult UICamera_GetTouchCountCallback::BeginInvoke(System.AsyncCallback,System.Object)
extern void GetTouchCountCallback_BeginInvoke_m8BB5B76B1A9C563B6458C3F6AB65E7CD160A5136 ();
// 0x00000C9F System.Int32 UICamera_GetTouchCountCallback::EndInvoke(System.IAsyncResult)
extern void GetTouchCountCallback_EndInvoke_mEA583C81FE97ECA375F0A833191E3CA8B2FF2C51 ();
// 0x00000CA0 System.Void UICamera_GetTouchCallback::.ctor(System.Object,System.IntPtr)
extern void GetTouchCallback__ctor_mC0DC65F68C2A762265B583FFBEC419B54FDC857F ();
// 0x00000CA1 UICamera_Touch UICamera_GetTouchCallback::Invoke(System.Int32)
extern void GetTouchCallback_Invoke_m9D16F3CA7778DEBE429581C3FDE7033DFAF9AB5A ();
// 0x00000CA2 System.IAsyncResult UICamera_GetTouchCallback::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern void GetTouchCallback_BeginInvoke_mA4C9623291B1E543148F6EFF3BD455BF84BEC505 ();
// 0x00000CA3 UICamera_Touch UICamera_GetTouchCallback::EndInvoke(System.IAsyncResult)
extern void GetTouchCallback_EndInvoke_m74A2B63F43A5DDDB25B5BBB77C8D6CAF19DC5F07 ();
// 0x00000CA4 System.Void UICamera_<>c::.cctor()
extern void U3CU3Ec__cctor_mAB4058B6C5EF0060A05AEB4C5753E7B8E221B917 ();
// 0x00000CA5 System.Void UICamera_<>c::.ctor()
extern void U3CU3Ec__ctor_mC72E56A0D1E321C16789A382ADB4FA0EE9BB0410 ();
// 0x00000CA6 System.Int32 UICamera_<>c::<Raycast>b__190_0(UICamera_DepthEntry,UICamera_DepthEntry)
extern void U3CU3Ec_U3CRaycastU3Eb__190_0_mE440F0FB2283EDD5EE82836F75A94D4A3ED21D2D ();
// 0x00000CA7 System.Int32 UICamera_<>c::<Raycast>b__190_1(UICamera_DepthEntry,UICamera_DepthEntry)
extern void U3CU3Ec_U3CRaycastU3Eb__190_1_m976B9B9B329EE383D39FD0D07C8774D94EC38343 ();
// 0x00000CA8 System.Boolean UICamera_<>c::<.cctor>b__230_0(UnityEngine.KeyCode)
extern void U3CU3Ec_U3C_cctorU3Eb__230_0_m407281D3677327A31C1A68A75B13921414DB0564 ();
// 0x00000CA9 System.Boolean UICamera_<>c::<.cctor>b__230_1(UnityEngine.KeyCode)
extern void U3CU3Ec_U3C_cctorU3Eb__230_1_m847D54BE962B0D02113BFFB35908F3ED2BC4E3D0 ();
// 0x00000CAA System.Boolean UICamera_<>c::<.cctor>b__230_2(UnityEngine.KeyCode)
extern void U3CU3Ec_U3C_cctorU3Eb__230_2_m5351F16D0071D1C4FB9D3A12982EB1A797B2576E ();
// 0x00000CAB System.Single UICamera_<>c::<.cctor>b__230_3(System.String)
extern void U3CU3Ec_U3C_cctorU3Eb__230_3_m2ACD047F4C9DE94BD9BF9E3079C6B39E1CC7886F ();
// 0x00000CAC UICamera_MouseOrTouch UICamera_<>c::<.cctor>b__230_4(System.Int32)
extern void U3CU3Ec_U3C_cctorU3Eb__230_4_mD268B7FD64B029CD6670210601DBDD96F53507D0 ();
// 0x00000CAD UICamera_MouseOrTouch UICamera_<>c::<.cctor>b__230_5(System.Int32,System.Boolean)
extern void U3CU3Ec_U3C_cctorU3Eb__230_5_mF22C2DA366DC77718C9A5ADD7F919B2E73E0822E ();
// 0x00000CAE System.Void UICamera_<>c::<.cctor>b__230_6(System.Int32)
extern void U3CU3Ec_U3C_cctorU3Eb__230_6_m23DD6B839E82B7DD2673C1347C1B040AEF2111C2 ();
// 0x00000CAF System.Void UIInput_OnValidate::.ctor(System.Object,System.IntPtr)
extern void OnValidate__ctor_m734BBC6B8796E7333FF21471EE266E2815F20605 ();
// 0x00000CB0 System.Char UIInput_OnValidate::Invoke(System.String,System.Int32,System.Char)
extern void OnValidate_Invoke_mA5DCFD2213708341CA4F78F969B4674CCCC05189 ();
// 0x00000CB1 System.IAsyncResult UIInput_OnValidate::BeginInvoke(System.String,System.Int32,System.Char,System.AsyncCallback,System.Object)
extern void OnValidate_BeginInvoke_mAF6077DA7F8DA0AA207A17A6D746C3E0EBCEF0BB ();
// 0x00000CB2 System.Char UIInput_OnValidate::EndInvoke(System.IAsyncResult)
extern void OnValidate_EndInvoke_m41993DBE7BD6FAD03EBEB7F52BF40BA1513FF323 ();
// 0x00000CB3 System.Void UILabel_ModifierFunc::.ctor(System.Object,System.IntPtr)
extern void ModifierFunc__ctor_m573818B286775C56FA715AD6FEEE0E6914AC2BAC ();
// 0x00000CB4 System.String UILabel_ModifierFunc::Invoke(System.String)
extern void ModifierFunc_Invoke_m9CAB74B008DACAF06F4B60C94EF0AECE2747B63C ();
// 0x00000CB5 System.IAsyncResult UILabel_ModifierFunc::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void ModifierFunc_BeginInvoke_m5EA5983D4DEEA75050F770ED3A01D2935E435BB3 ();
// 0x00000CB6 System.String UILabel_ModifierFunc::EndInvoke(System.IAsyncResult)
extern void ModifierFunc_EndInvoke_mC79C64B6018FFCD2310B2C0B858F892A9A90EF3B ();
// 0x00000CB7 System.Void UIPanel_OnGeometryUpdated::.ctor(System.Object,System.IntPtr)
extern void OnGeometryUpdated__ctor_m5B889BC8F535DB17F0DA302A2EC2B064087DBA0F ();
// 0x00000CB8 System.Void UIPanel_OnGeometryUpdated::Invoke()
extern void OnGeometryUpdated_Invoke_mAAC351E53D362D2CC4C429CF791BDC7592ECC66D ();
// 0x00000CB9 System.IAsyncResult UIPanel_OnGeometryUpdated::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnGeometryUpdated_BeginInvoke_mC243DEEA1D28185837FF0CF2BFF9B3AD9210D64E ();
// 0x00000CBA System.Void UIPanel_OnGeometryUpdated::EndInvoke(System.IAsyncResult)
extern void OnGeometryUpdated_EndInvoke_m963956A2BDBD0A2616F801125EE19353385DCFCA ();
// 0x00000CBB System.Void UIPanel_OnClippingMoved::.ctor(System.Object,System.IntPtr)
extern void OnClippingMoved__ctor_m4F91986AD2EF514A6C740BF5274D7465437AC7A2 ();
// 0x00000CBC System.Void UIPanel_OnClippingMoved::Invoke(UIPanel)
extern void OnClippingMoved_Invoke_m6695F8F6BC6584A6C7C7A96E032AB719603F249F ();
// 0x00000CBD System.IAsyncResult UIPanel_OnClippingMoved::BeginInvoke(UIPanel,System.AsyncCallback,System.Object)
extern void OnClippingMoved_BeginInvoke_mF428511832B59599F483C2AB19F43D2D460D713C ();
// 0x00000CBE System.Void UIPanel_OnClippingMoved::EndInvoke(System.IAsyncResult)
extern void OnClippingMoved_EndInvoke_m089A64DBA217E7245936207AD9B3F61ED4A08BE6 ();
// 0x00000CBF System.Void UIPanel_OnCreateMaterial::.ctor(System.Object,System.IntPtr)
extern void OnCreateMaterial__ctor_m3C3D2C39B7F9FF1874B4974346E5E01B29FF9553 ();
// 0x00000CC0 UnityEngine.Material UIPanel_OnCreateMaterial::Invoke(UIWidget,UnityEngine.Material)
extern void OnCreateMaterial_Invoke_mC5981C4D545519C8DB5C52764FF3C5A66AB857C9 ();
// 0x00000CC1 System.IAsyncResult UIPanel_OnCreateMaterial::BeginInvoke(UIWidget,UnityEngine.Material,System.AsyncCallback,System.Object)
extern void OnCreateMaterial_BeginInvoke_m359A374B2DD4830532B2DF8FAAFCA4D11D8857B2 ();
// 0x00000CC2 UnityEngine.Material UIPanel_OnCreateMaterial::EndInvoke(System.IAsyncResult)
extern void OnCreateMaterial_EndInvoke_mAC24DA9128FE07B3C57EAAAF19A73B2C2E9E634A ();
// 0x00000CC3 UnityEngine.Vector4 UISpriteCollection_Sprite::GetDrawingDimensions(System.Single)
extern void Sprite_GetDrawingDimensions_mA4478A253FE5F31C6E725E9F7E3CA70FD2C60DE7_AdjustorThunk ();
// 0x00000CC4 System.Void UISpriteCollection_OnHoverCB::.ctor(System.Object,System.IntPtr)
extern void OnHoverCB__ctor_m6B9911B1D0ED8EEF3319D5FD245D4C1BDC924150 ();
// 0x00000CC5 System.Void UISpriteCollection_OnHoverCB::Invoke(System.Object,System.Boolean)
extern void OnHoverCB_Invoke_mCCC904FE0D729A0F3FEE2AB37E668E9A14BAD22E ();
// 0x00000CC6 System.IAsyncResult UISpriteCollection_OnHoverCB::BeginInvoke(System.Object,System.Boolean,System.AsyncCallback,System.Object)
extern void OnHoverCB_BeginInvoke_m0177489BD3A6B0F8D50B9FB8E433B61B23589973 ();
// 0x00000CC7 System.Void UISpriteCollection_OnHoverCB::EndInvoke(System.IAsyncResult)
extern void OnHoverCB_EndInvoke_m9DDE489390D5F8B507ACB25A45A0776AFB31741C ();
// 0x00000CC8 System.Void UISpriteCollection_OnPressCB::.ctor(System.Object,System.IntPtr)
extern void OnPressCB__ctor_mE2B8993DF0FCC2607115CFB2BD38DDA358E60378 ();
// 0x00000CC9 System.Void UISpriteCollection_OnPressCB::Invoke(System.Object,System.Boolean)
extern void OnPressCB_Invoke_m7791C776FE0EFDD99006D2354E2CFEB27378A4C5 ();
// 0x00000CCA System.IAsyncResult UISpriteCollection_OnPressCB::BeginInvoke(System.Object,System.Boolean,System.AsyncCallback,System.Object)
extern void OnPressCB_BeginInvoke_mA6CC9890EE888DDA5A8B7FC900EA4A3DD0E3D08D ();
// 0x00000CCB System.Void UISpriteCollection_OnPressCB::EndInvoke(System.IAsyncResult)
extern void OnPressCB_EndInvoke_mF0DE7B675F67A6D2C52A3BEADE28A5741F9E9485 ();
// 0x00000CCC System.Void UISpriteCollection_OnClickCB::.ctor(System.Object,System.IntPtr)
extern void OnClickCB__ctor_m418E6D30469B5EA6C6E1924AA09880127FB386F4 ();
// 0x00000CCD System.Void UISpriteCollection_OnClickCB::Invoke(System.Object)
extern void OnClickCB_Invoke_m8F452C3ABCFA316621AE3CBBDB253C7E28135696 ();
// 0x00000CCE System.IAsyncResult UISpriteCollection_OnClickCB::BeginInvoke(System.Object,System.AsyncCallback,System.Object)
extern void OnClickCB_BeginInvoke_m54AF682DA4E932E2C41B24223755AE4F3364CE4F ();
// 0x00000CCF System.Void UISpriteCollection_OnClickCB::EndInvoke(System.IAsyncResult)
extern void OnClickCB_EndInvoke_mB5078F3BCED73628EEFDBF26CA52D4FA6385CB7F ();
// 0x00000CD0 System.Void UISpriteCollection_OnDragCB::.ctor(System.Object,System.IntPtr)
extern void OnDragCB__ctor_m9CD0633F7A5CF8208E3900A49AB6734667553EB7 ();
// 0x00000CD1 System.Void UISpriteCollection_OnDragCB::Invoke(System.Object,UnityEngine.Vector2)
extern void OnDragCB_Invoke_m838BA12FA8EAB7ED99FC5984FCA5F4CCF1477F9D ();
// 0x00000CD2 System.IAsyncResult UISpriteCollection_OnDragCB::BeginInvoke(System.Object,UnityEngine.Vector2,System.AsyncCallback,System.Object)
extern void OnDragCB_BeginInvoke_m8443C3C5BE7A8E5F9FBF536D531EBD3562FF2041 ();
// 0x00000CD3 System.Void UISpriteCollection_OnDragCB::EndInvoke(System.IAsyncResult)
extern void OnDragCB_EndInvoke_m0FE336F544DB2A95B7ECD18B64DB8B5FE70BA83C ();
// 0x00000CD4 System.Void UISpriteCollection_OnTooltipCB::.ctor(System.Object,System.IntPtr)
extern void OnTooltipCB__ctor_m4AD67E30B5C7C64AFEAA2B988D9EDA06A671B337 ();
// 0x00000CD5 System.Void UISpriteCollection_OnTooltipCB::Invoke(System.Object,System.Boolean)
extern void OnTooltipCB_Invoke_m3104772CFBA3EB85D561D2301D3D4FA90A75C748 ();
// 0x00000CD6 System.IAsyncResult UISpriteCollection_OnTooltipCB::BeginInvoke(System.Object,System.Boolean,System.AsyncCallback,System.Object)
extern void OnTooltipCB_BeginInvoke_mFE9C074DDC340B61042F28678EAD36F0A192C358 ();
// 0x00000CD7 System.Void UISpriteCollection_OnTooltipCB::EndInvoke(System.IAsyncResult)
extern void OnTooltipCB_EndInvoke_mB268E2C4A726461EA379E7B9115798E87B5E8FC4 ();
// 0x00000CD8 System.Void UITextList_Paragraph::.ctor()
extern void Paragraph__ctor_m47FCA7CEA13E85DB03BD3D03A28E980245AFA746 ();
// 0x00000CD9 System.Void TweenLetters_LetterProperties::.ctor()
extern void LetterProperties__ctor_mDBC4FDA051A98037A8EA95C81F709F646E2D218C ();
// 0x00000CDA System.Void TweenLetters_AnimationProperties::.ctor()
extern void AnimationProperties__ctor_m576AFE8E2BF356048A23A2C9CA95F51F9C6B8EF5 ();
// 0x00000CDB System.Void Player_<>c::.cctor()
extern void U3CU3Ec__cctor_mF172830ADB11D405A5DEB07964231D0A104C6E32 ();
// 0x00000CDC System.Void Player_<>c::.ctor()
extern void U3CU3Ec__ctor_m23679B08194E6E2397F91CC5962C14B8DDFB7C28 ();
// 0x00000CDD System.Void Player_<>c::<GameOver>b__4_0()
extern void U3CU3Ec_U3CGameOverU3Eb__4_0_m2E0701DA5DC8B743C70DA42C56290D03083EB1CD ();
// 0x00000CDE System.Void Invoker_InvokableItem::.ctor(Invokable,System.Single)
extern void InvokableItem__ctor_mBD1BC7C79F6386C13B55A7225ED67BE60E58E5B1_AdjustorThunk ();
// 0x00000CDF System.Void Invoker_InvokableItem::.cctor()
extern void InvokableItem__cctor_m3B62F51652D6EEFBEA2D11E28D175379479A7561 ();
// 0x00000CE0 System.Void UserData_<>c::.cctor()
extern void U3CU3Ec__cctor_m6C791A37955F7852ED54DE100AB3FE0C7CA32F6B ();
// 0x00000CE1 System.Void UserData_<>c::.ctor()
extern void U3CU3Ec__ctor_m631CA5D86F20199C408287BDE830A6355AF544A8 ();
// 0x00000CE2 System.Void UserData_<>c::<.cctor>b__42_0(System.Boolean)
extern void U3CU3Ec_U3C_cctorU3Eb__42_0_m415A7E2AE7178E3C2743A8149935CA1E89EB949F ();
// 0x00000CE3 System.Void MonoBehaviorExtentsion_<DelayMethod>d__0`3::.ctor(System.Int32)
// 0x00000CE4 System.Void MonoBehaviorExtentsion_<DelayMethod>d__0`3::System.IDisposable.Dispose()
// 0x00000CE5 System.Boolean MonoBehaviorExtentsion_<DelayMethod>d__0`3::MoveNext()
// 0x00000CE6 System.Object MonoBehaviorExtentsion_<DelayMethod>d__0`3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
// 0x00000CE7 System.Void MonoBehaviorExtentsion_<DelayMethod>d__0`3::System.Collections.IEnumerator.Reset()
// 0x00000CE8 System.Object MonoBehaviorExtentsion_<DelayMethod>d__0`3::System.Collections.IEnumerator.get_Current()
// 0x00000CE9 System.Void MonoBehaviorExtentsion_<DelayMethod>d__1`2::.ctor(System.Int32)
// 0x00000CEA System.Void MonoBehaviorExtentsion_<DelayMethod>d__1`2::System.IDisposable.Dispose()
// 0x00000CEB System.Boolean MonoBehaviorExtentsion_<DelayMethod>d__1`2::MoveNext()
// 0x00000CEC System.Object MonoBehaviorExtentsion_<DelayMethod>d__1`2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
// 0x00000CED System.Void MonoBehaviorExtentsion_<DelayMethod>d__1`2::System.Collections.IEnumerator.Reset()
// 0x00000CEE System.Object MonoBehaviorExtentsion_<DelayMethod>d__1`2::System.Collections.IEnumerator.get_Current()
// 0x00000CEF System.Void MonoBehaviorExtentsion_<DelayMethod>d__2`1::.ctor(System.Int32)
// 0x00000CF0 System.Void MonoBehaviorExtentsion_<DelayMethod>d__2`1::System.IDisposable.Dispose()
// 0x00000CF1 System.Boolean MonoBehaviorExtentsion_<DelayMethod>d__2`1::MoveNext()
// 0x00000CF2 System.Object MonoBehaviorExtentsion_<DelayMethod>d__2`1::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
// 0x00000CF3 System.Void MonoBehaviorExtentsion_<DelayMethod>d__2`1::System.Collections.IEnumerator.Reset()
// 0x00000CF4 System.Object MonoBehaviorExtentsion_<DelayMethod>d__2`1::System.Collections.IEnumerator.get_Current()
// 0x00000CF5 System.Void MonoBehaviorExtentsion_<DelayMethod>d__3::.ctor(System.Int32)
extern void U3CDelayMethodU3Ed__3__ctor_mFD2C170FED71FA8F623B7A22A542E2F7265E2265 ();
// 0x00000CF6 System.Void MonoBehaviorExtentsion_<DelayMethod>d__3::System.IDisposable.Dispose()
extern void U3CDelayMethodU3Ed__3_System_IDisposable_Dispose_mFD692FF85D95F6D3009BB6DEE220425BF3A2E958 ();
// 0x00000CF7 System.Boolean MonoBehaviorExtentsion_<DelayMethod>d__3::MoveNext()
extern void U3CDelayMethodU3Ed__3_MoveNext_m7C65BC2319244D84E3E73B869948560AEB054402 ();
// 0x00000CF8 System.Object MonoBehaviorExtentsion_<DelayMethod>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDelayMethodU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m120349318CB903B695ACD972C35B6EE222EC5D45 ();
// 0x00000CF9 System.Void MonoBehaviorExtentsion_<DelayMethod>d__3::System.Collections.IEnumerator.Reset()
extern void U3CDelayMethodU3Ed__3_System_Collections_IEnumerator_Reset_m2E26450A80621BC63BA4F410CCAFA9F00C87FFA8 ();
// 0x00000CFA System.Object MonoBehaviorExtentsion_<DelayMethod>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CDelayMethodU3Ed__3_System_Collections_IEnumerator_get_Current_m8207E574682CB41DFC12DBCA5F577D6E500C29D0 ();
// 0x00000CFB System.Void SceneNavigator_<>c::.cctor()
extern void U3CU3Ec__cctor_mEEB446D263068A72423B8908C226646CAD19C825 ();
// 0x00000CFC System.Void SceneNavigator_<>c::.ctor()
extern void U3CU3Ec__ctor_m568C68F85EF0CE2808829BEBE38CA4A093FDCB18 ();
// 0x00000CFD System.Void SceneNavigator_<>c::<.ctor>b__15_0(System.Int32)
extern void U3CU3Ec_U3C_ctorU3Eb__15_0_m1637C8AD486DEFD353D7B521D65ADF6E54DC7C4E ();
// 0x00000CFE System.Void AdManager_<>c::.cctor()
extern void U3CU3Ec__cctor_m49C8BAC7B2B783DE08DAF777F7843C1C4F483AD2 ();
// 0x00000CFF System.Void AdManager_<>c::.ctor()
extern void U3CU3Ec__ctor_m157A06FB4329325AAF7DE56DFC5249803F138032 ();
// 0x00000D00 System.Void AdManager_<>c::<.cctor>b__25_0(System.Boolean)
extern void U3CU3Ec_U3C_cctorU3Eb__25_0_mF1D41F1CBB3501226B403355867B1464E0FB7501 ();
// 0x00000D01 System.Void AdManager_<>c::<.cctor>b__25_1()
extern void U3CU3Ec_U3C_cctorU3Eb__25_1_m9927755A165639E5135C2967E159DC01C873B915 ();
// 0x00000D02 System.Void AdManager_<>c::<.cctor>b__25_2()
extern void U3CU3Ec_U3C_cctorU3Eb__25_2_m0700845756F9BC84FE3A1FB702B98578453DF73A ();
// 0x00000D03 System.Void SharePlugin_<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_m8A14D74193A64DFDA56705C83191B16AD7F832BC ();
// 0x00000D04 System.Void SharePlugin_<>c__DisplayClass2_0::<Tweet>b__0()
extern void U3CU3Ec__DisplayClass2_0_U3CTweetU3Eb__0_m83DC7DB77E7E7B9BA243D1327C6D9B90730FFACE ();
// 0x00000D05 System.Void ButtonAssistant_<>c::.cctor()
extern void U3CU3Ec__cctor_m536220CFABBE9A93CCADEEC9B8FA930F2192B311 ();
// 0x00000D06 System.Void ButtonAssistant_<>c::.ctor()
extern void U3CU3Ec__ctor_mDB3AC550A53049ABF5E9C66557C11CCD0BADA8ED ();
// 0x00000D07 System.Void ButtonAssistant_<>c::<.ctor>b__8_0()
extern void U3CU3Ec_U3C_ctorU3Eb__8_0_m7C43611373E90ADD36ED6859529A894179D989A9 ();
// 0x00000D08 System.Void Exploder.ExploderObject_OnExplosion::.ctor(System.Object,System.IntPtr)
extern void OnExplosion__ctor_m5785D8E3F7E730BEB622B85F29B0223E56FDD8BA ();
// 0x00000D09 System.Void Exploder.ExploderObject_OnExplosion::Invoke(System.Single,Exploder.ExploderObject_ExplosionState)
extern void OnExplosion_Invoke_m10A7A2ABE292E9439F091E31B0BDAD923C1B2DD1 ();
// 0x00000D0A System.IAsyncResult Exploder.ExploderObject_OnExplosion::BeginInvoke(System.Single,Exploder.ExploderObject_ExplosionState,System.AsyncCallback,System.Object)
extern void OnExplosion_BeginInvoke_m63C90C058889B47E93FEC8B002A09D22EB52D468 ();
// 0x00000D0B System.Void Exploder.ExploderObject_OnExplosion::EndInvoke(System.IAsyncResult)
extern void OnExplosion_EndInvoke_m13AE98EF9EC9AE58C1E682A757E4141362CA94A4 ();
// 0x00000D0C System.Void Exploder.Utils.Hull2D_<>c::.cctor()
extern void U3CU3Ec__cctor_m1D86183FBBB6919940B6778552BAAC46955A2439 ();
// 0x00000D0D System.Void Exploder.Utils.Hull2D_<>c::.ctor()
extern void U3CU3Ec__ctor_m3F0C7BEC8BAB7E33FD71C86A95752375FB734681 ();
// 0x00000D0E System.Int32 Exploder.Utils.Hull2D_<>c::<Sort>b__0_0(UnityEngine.Vector2,UnityEngine.Vector2)
extern void U3CU3Ec_U3CSortU3Eb__0_0_m5ACB7AB065A74072A4DEB12F86B0CF5008CFF87F ();
// 0x00000D0F System.Void Exploder.Demo.DemoClickExplode_<>c::.cctor()
extern void U3CU3Ec__cctor_m75458A6B30EB2456FB23F0EF126DB7D00958ABB6 ();
// 0x00000D10 System.Void Exploder.Demo.DemoClickExplode_<>c::.ctor()
extern void U3CU3Ec__ctor_m927F9ECBCBF816B080F31BCC3F05E1BA374DC7BE ();
// 0x00000D11 System.Boolean Exploder.Demo.DemoClickExplode_<>c::<Start>b__3_0(Exploder.Explodable)
extern void U3CU3Ec_U3CStartU3Eb__3_0_mAD5BBF6B9905C8C1617C1CA95B53A5518DE7A3E7 ();
// 0x00000D12 UnityEngine.GameObject Exploder.Demo.DemoClickExplode_<>c::<Start>b__3_1(Exploder.Explodable)
extern void U3CU3Ec_U3CStartU3Eb__3_1_mE8B28ACC38369D2BA1067EEEE0015DE7622AB4E5 ();
// 0x00000D13 System.Void Exploder.Demo.Rocket_OnHit::.ctor(System.Object,System.IntPtr)
extern void OnHit__ctor_m1DDB7A1C8F596672D6531E98B2FABD8084653DFA ();
// 0x00000D14 System.Void Exploder.Demo.Rocket_OnHit::Invoke(UnityEngine.Vector3)
extern void OnHit_Invoke_m3C32CDF26AFDB5F190616E79A1A4EEB4316D3863 ();
// 0x00000D15 System.IAsyncResult Exploder.Demo.Rocket_OnHit::BeginInvoke(UnityEngine.Vector3,System.AsyncCallback,System.Object)
extern void OnHit_BeginInvoke_mF1DE1EC3D1449EA28158869EB198CEF3DC9B9E31 ();
// 0x00000D16 System.Void Exploder.Demo.Rocket_OnHit::EndInvoke(System.IAsyncResult)
extern void OnHit_EndInvoke_mC58A8A2B30B1AC24B3FA22BEBF2264DB87E1EB1B ();
// 0x00000D17 System.Void EpicToonFX.ETFXLoopScript_<EffectLoop>d__6::.ctor(System.Int32)
extern void U3CEffectLoopU3Ed__6__ctor_m19CC1932A7DA8A6618D298CFF1BBA53618830753 ();
// 0x00000D18 System.Void EpicToonFX.ETFXLoopScript_<EffectLoop>d__6::System.IDisposable.Dispose()
extern void U3CEffectLoopU3Ed__6_System_IDisposable_Dispose_m1D2581BCDA04A67D92D3CB0890C8376670256854 ();
// 0x00000D19 System.Boolean EpicToonFX.ETFXLoopScript_<EffectLoop>d__6::MoveNext()
extern void U3CEffectLoopU3Ed__6_MoveNext_m7A01CBF6EC76A56EB3446517E38FD1D9FB798EC7 ();
// 0x00000D1A System.Object EpicToonFX.ETFXLoopScript_<EffectLoop>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CEffectLoopU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCF17BD0CB66F98139B79E9EB07A691281CF8CD59 ();
// 0x00000D1B System.Void EpicToonFX.ETFXLoopScript_<EffectLoop>d__6::System.Collections.IEnumerator.Reset()
extern void U3CEffectLoopU3Ed__6_System_Collections_IEnumerator_Reset_m4DE482E578C91F13D0208F7A6B035146D6E4E6B7 ();
// 0x00000D1C System.Object EpicToonFX.ETFXLoopScript_<EffectLoop>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CEffectLoopU3Ed__6_System_Collections_IEnumerator_get_Current_m12F1418D67FCE2BC30B2F1114789F336A2A42D98 ();
// 0x00000D1D System.Void EpicToonFX.ETFXTarget_<Respawn>d__7::.ctor(System.Int32)
extern void U3CRespawnU3Ed__7__ctor_m450E28D1F0615476C4B949E64B9E1D7E43DEABA5 ();
// 0x00000D1E System.Void EpicToonFX.ETFXTarget_<Respawn>d__7::System.IDisposable.Dispose()
extern void U3CRespawnU3Ed__7_System_IDisposable_Dispose_mA34A2978F752691D1448A49E9BF89C92CB5A970E ();
// 0x00000D1F System.Boolean EpicToonFX.ETFXTarget_<Respawn>d__7::MoveNext()
extern void U3CRespawnU3Ed__7_MoveNext_m396EB1C3C667DBBF960F402DDC9B3F164BE36F75 ();
// 0x00000D20 System.Object EpicToonFX.ETFXTarget_<Respawn>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRespawnU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD1F4E558CF6D12D0ED2A2CF1159BB026E3498CB5 ();
// 0x00000D21 System.Void EpicToonFX.ETFXTarget_<Respawn>d__7::System.Collections.IEnumerator.Reset()
extern void U3CRespawnU3Ed__7_System_Collections_IEnumerator_Reset_m8E4983D69BFF682E250120798368D2119DAFC045 ();
// 0x00000D22 System.Object EpicToonFX.ETFXTarget_<Respawn>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CRespawnU3Ed__7_System_Collections_IEnumerator_get_Current_mF8D216ECEA311410014AEEEA55F06C55EF8EE812 ();
static Il2CppMethodPointer s_methodPointers[3362] = 
{
	Ally_OnEnable_m6A41660CF284185A92A6C25E074DF390E67A869C,
	Ally_OnDisable_m113302690F880359CE150C2544CE2F934055EDDB,
	Ally_GameOver_mE9899D77788537C2703DAC76B432E9361CB72059,
	Ally__ctor_m03330EBA955FEF5C1FB9A6F5B68C37CC9AEB596F,
	ETFXProjectileScript_Start_m6E1A7FAF7955CF5D05D492FE2639FAF49C29F3A7,
	ETFXProjectileScript_FixedUpdate_mBA7580BD5AA2429A3884E902E0D954B9A07AF0BB,
	ETFXProjectileScript__ctor_m83347094E99EC927D58FE6F3A983665C1CFCF035,
	ETFXSceneManager_LoadScene2DDemo_mE781B655A5B96B0F7C79E6DA112287A1ACE21492,
	ETFXSceneManager_LoadSceneCards_mE6392B1EF78F3BD32D1808BAD0FDE93796CEA772,
	ETFXSceneManager_LoadSceneCombat_m29FB4A70E76F7EB604F8EA2DA1891B378EF037E8,
	ETFXSceneManager_LoadSceneDecals_m8F33C93462F032A6079FB6B31154638284190D6E,
	ETFXSceneManager_LoadSceneDecals2_m50EDDC42545A5F7905C4C98CA72C8632C1F8E40D,
	ETFXSceneManager_LoadSceneEmojis_m17E51BD74D6E346ED9A8D006E7C0BB10777F643A,
	ETFXSceneManager_LoadSceneEmojis2_m51651D4A6E9E6C7A1E5D268EFF5126F76B1759C7,
	ETFXSceneManager_LoadSceneExplosions_m908FEB716367413D6035609F6ED56D84A2B7BB1D,
	ETFXSceneManager_LoadSceneExplosions2_mD9DB30FBF938961AF70EC84549C958458CD4EA1F,
	ETFXSceneManager_LoadSceneFire_m7FA403188E1BF5FBAC751B17506B33274420073A,
	ETFXSceneManager_LoadSceneFire2_m7D2466C93F199DC33C0A9E0C7A116F7DD9CD14F5,
	ETFXSceneManager_LoadSceneFire3_mC270519E872C1871A22256FB0E9F129663E31035,
	ETFXSceneManager_LoadSceneFireworks_mBEBEE0D3702F454CF8A194804459FD975E10842B,
	ETFXSceneManager_LoadSceneFlares_m19F7F5565F02CB3093E28C777922D493EE36F909,
	ETFXSceneManager_LoadSceneMagic_m61ED7F575FC3D2D8AA3D4C2575F10749C79EF60D,
	ETFXSceneManager_LoadSceneMagic2_m5D07B15B6636CF65DC46915564C68B8210525668,
	ETFXSceneManager_LoadSceneMagic3_mBA147539B53E56AAEAD46361E3B26C9098D0FE4E,
	ETFXSceneManager_LoadSceneMainDemo_m288F240F4D782FDE5F2F13071F4F08E0D47B5FA2,
	ETFXSceneManager_LoadSceneMissiles_mC2146CC83899D75C87D58F26D94497417758C5B8,
	ETFXSceneManager_LoadScenePortals_mB05FB9FE782E3F284670F26DCF35F2870D12DBE5,
	ETFXSceneManager_LoadScenePortals2_m6C0D1846A8F3B6E2F01937623BA6DDCF84F670B3,
	ETFXSceneManager_LoadScenePowerups_mA4DDF427C8031375E2AD9A5A1B05EEF039088C86,
	ETFXSceneManager_LoadScenePowerups2_mA18C9E8AA8BB70F6F42B15355A475D29B9938A31,
	ETFXSceneManager_LoadSceneSparkles_m58BEC471FC1EED46A4B4C526F2D4AF67302D87C3,
	ETFXSceneManager_LoadSceneSwordCombat_m15F504EFC2BC5E16DD780555A63BEC9D9C67C0C8,
	ETFXSceneManager_LoadSceneSwordCombat2_mFE31465497F70DC3237F68A820EFB39D1BB93994,
	ETFXSceneManager_LoadSceneMoney_m287946AD46201A0CBA5D5F0C99762B7F060BCAA6,
	ETFXSceneManager_LoadSceneHealing_m614159E20FA8FB9C1A49CA6173B1CCC59F56B555,
	ETFXSceneManager_LoadSceneWind_m3109735CDEBFC386685D2A27817B645D5FBF6AE1,
	ETFXSceneManager_Update_mB6494C8700F23FBEDC192E56A3EAB0E548F2EB80,
	ETFXSceneManager__ctor_m36D42D80C5078EC28655FD9179179556AE6DEAB5,
	PEButtonScript_Start_m8A547470C61210076A8406D0F018A040BF5B37A7,
	PEButtonScript_OnPointerEnter_mADE18B90955FA7ADB1F15403FF64E97E1EF0A9D1,
	PEButtonScript_OnPointerExit_m796601A33B6D76535AB02237627FA38A64522E36,
	PEButtonScript_OnButtonClicked_mD7B14EEB96DF66A13B83DDB5F60357CD617C06B5,
	PEButtonScript__ctor_mCEA81838780EC86A88EA1FEFF96809B92C3CF413,
	ParticleEffectsLibrary_Awake_mCE4F7DE403B8812636A82F733F174E78CEA892DD,
	ParticleEffectsLibrary_Start_mDB7425580D9DD052C79F59A4E0D4F49117CE8B64,
	ParticleEffectsLibrary_GetCurrentPENameString_m1691E2A36A3A021A52107351116FC6788DB26366,
	ParticleEffectsLibrary_PreviousParticleEffect_m461C6A9FE69983C9D115A6FC794E0A27701691B6,
	ParticleEffectsLibrary_NextParticleEffect_m7D6F09E81DA16D6B1FF5221D2258CBD17B6474B4,
	ParticleEffectsLibrary_SpawnParticleEffect_mA27B745B4BB8FBEDBF02F2BDA7E8A3A6F169EB1A,
	ParticleEffectsLibrary__ctor_m6DDE8AB1042AFB41851A2F29B003E73087C217CA,
	UICanvasManager_Awake_m5E00062167821A732D364DDE4EF0D486F2AB15F1,
	UICanvasManager_Start_m52A5A4A6201A8851DAA745961E000F3129E7A470,
	UICanvasManager_Update_mE2DD2A793B2AAEC6417901769CA7429AAE9DF3D1,
	UICanvasManager_UpdateToolTip_m6F278FBA74B3F998D30C84CB45C58824A6DACEF5,
	UICanvasManager_ClearToolTip_mCDCEC725E6C8F802D5EBBEA35475E4D0AD1030E3,
	UICanvasManager_SelectPreviousPE_mE464573E1B2E606EC42AB776DDE7E80941E7C6AB,
	UICanvasManager_SelectNextPE_mA733B8357E23242CEFF99C340C29F6C18B815BA4,
	UICanvasManager_SpawnCurrentParticleEffect_mF0B7AB6F80B2C49D277774DEC620610309621F62,
	UICanvasManager_UIButtonClick_m8B5AB9450814A38B39DF17657F45B1149CC0C765,
	UICanvasManager__ctor_m37771377D8BAC0CD9C48FAB83A4960EE3DBC04FF,
	QuickStart_Start_mCC0C83C2D4568D036FEB485FD604535A330D1706,
	QuickStart__ctor_mB059290F71394E20F766C5B0B4433850D7D5E4F3,
	TweenFragment_Start_mADFEF8A9F351FB03BFA2373DE816D80A807AC857,
	TweenFragment_Update_mDD2E2C21F34CC6EFC25AD3CC257314B3B1B2E59A,
	TweenFragment__ctor_m2D985ED567FDBC6719F9C89E53E6F9FC26AE7442,
	BreakableObstacle_OnTriggerEnter_mE0E398C367FAA6D9E584619E507538CE979B98A9,
	BreakableObstacle__ctor_mC44EC03CDCC5634D7B55860767207D9D3743152F,
	CameraManager_Awake_m4F295CC2C61D64BAC928889D2E54C86FFC7DEC35,
	CameraManager__ctor_m8B63DCA86FB61304B4F9DE1A4323F653D350F4A1,
	Character_Awake_mD2FA19540897E4C0B7C046CDF27511AA687421B0,
	Character_OnTriggerEnter_mF31790DC11B982E42DE90E2F34388370CA7807B7,
	Character__ctor_mD5A6FF1938E86A2995DD8F4DBA0F9BFEF2965966,
	CheatButton_OnEnable_m54C745E81DDDDAB74B4595393F8DE37F949C7AD2,
	CheatButton_OnClick_mCE8015B88A7F7284129916BBFD7DB63C018BDC09,
	CheatButton__ctor_mF07A675E295B77750986EF1C4AD655994AAC1688,
	CheatStageButton_OnClick_mBB77C149BC42F5273D8A357851D8071656122A4A,
	CheatStageButton__ctor_m8549B66A977461D7DDF73691D3DDF9A3A9379C22,
	DelayProgressBar_set_CurrentGrenade_mC479322DE98D4CDB75163B43645462888689175E,
	DelayProgressBar_get_CurrentGrenade_mF1F28FF168441D61D3EC7FC0D4C61E8307870A81,
	DelayProgressBar_set_CanThrow_m2E86ABF312904C7693E29826AE0550046799E251,
	DelayProgressBar_get_CanThrow_m2BFA4858F9A8E97BE3CA7C9D5736E6FE2F13B1FE,
	DelayProgressBar_set_ShouldExplode_m5C5E8A42760DAEE0E4A428E523B2E501F83E2307,
	DelayProgressBar_get_ShouldExplode_m9632C5B9397D9DEBD5D78DA0B73D41AB6DD7A4ED,
	DelayProgressBar_OnEnable_m032431F7A91EBA54B56B80BC6A5486BB68085896,
	DelayProgressBar_StartCountDown_m8F0A3F9F58FF779FF8FE36C41449D6AAC523A27A,
	DelayProgressBar_StopCountDown_m7B31D278964ACAFC9FC23AF08D31911AC01594BE,
	DelayProgressBar__ctor_mD0CEFD4F2AD21416DACADCC5CA4251C9D18605E8,
	DelayProgressBar_U3CStartCountDownU3Eb__18_0_mE2C76B265F85A9329A96D28E840C39E30E2BD7CF,
	DelayProgressBar_U3CStartCountDownU3Eb__18_1_mEAF455BD39A0ABF3AA24A4C75184406887734791,
	DelayProgressBar_U3CStartCountDownU3Eb__18_2_m79521654240E345DC67C7248BBA61701DBAAD394,
	EditDelayCheat_OnEnable_m00C0C75D7808C162FD2EF3A2B0DEC2F660F1072D,
	EditDelayCheat_ChangeDelayTime_mAD54BCFC6FA8D28DE73CBC141359CA68F3887FDE,
	EditDelayCheat__ctor_mF1BF0CC3B6D4278E07B1D55E64E076FCCCC3FC82,
	Enemy_OnEnable_m01D2775026413EC6DF615D76DD967B7C94216FFC,
	Enemy_OnDisable_m5576451CF694305A6D15D450ABD8D05591263373,
	Enemy_Update_m4B43E4ED31FD86486FE3A8B0F65B9D69A46217E6,
	Enemy_GameOver_mEC12CE3DE1FF19DD6831BBBF2EA75251AC102062,
	Enemy_Idle_mAD59C3DA5E59AB234CF02DB2A0B621C164CCB866,
	Enemy_Walk_m9A829016AE89992912F574B5F2B71A333ADD6A61,
	Enemy_ThrowLeft_mB7B1CD7BF802885A766637BFFB5888B3C929EE76,
	Enemy__ctor_mA9911783A212E3706D7D51DC82C19D14CBDE5BE3,
	Enemy_U3CUpdateU3Eb__11_0_m99E25CBE40B95593EE60DA3C05B3C0A6733AFDFC,
	ExplostionFx_OnEnable_m6B57E0A01FBC5A0D87C77D95911D5FAAE58D08B5,
	ExplostionFx_SelfDestroy_m8FD60E94C5340D0C69E6A13AF600EC462DCF2460,
	ExplostionFx__ctor_m022EA2C700692574BAAE3905C20B8F5B1FADFD88,
	FerrisChair_Update_m0BD16FB497FB219AA0D7C7D8EC884B2153E27E6B,
	FerrisChair__ctor_mF166A1422B702A70D06377160EABAC111DDEA1E2,
	GameController_set_GameStart_mE59E339E0213C61C1351D6CD16BA8F8A7F9F4B54,
	GameController_get_GameStart_m53BCF263D4C3722C9190CB5857AFBCD619DE36D1,
	GameController_set_GameEnd_m35EF4F5ECF459A73DCB5B0F23FB11079F52A7DB0,
	GameController_get_GameEnd_m45E1DEA980A154C40333DB335F06CD10F46835BC,
	GameController_set_TargetCount_mFE2376019A69085F47BC0B6A66FEAF9609419273,
	GameController_get_TargetCount_m8C77854AF4BC4A3776B9F6054D433DCF7529D6D1,
	GameController_set_HintIndicator_m10F046FFC927CA948D866A996131AC0DAC5FAC58,
	GameController_get_HintIndicator_mF95C357B724D6AE8A3A6B072FE985579AF02264F,
	GameController_set_ThrowCount_m7FA38B82CE0F0CE3176AE6E78AC160FF5DF88A1B,
	GameController_get_ThrowCount_mA7670F2E17C7A073374FF1F2BD88F59CB4B07190,
	GameController_OnEnable_m0AECD2ED9B514027A58EC0BC732C80AB901E17F4,
	GameController_ShowHint_m8DE7C9ECACBCD2FD329D631E6E32D9FD698174CF,
	GameController_Explose_m90AB64A631F178DDD5F8890247C54FBD78514574,
	GameController_ForceGameOver_m4A97F3D29F43E4E196C65FF31F9684AA1628BEAD,
	GameController__ctor_m6C90AFC15A0F46BF2901D879C39FE7E50AE7CDB3,
	Grenade_set_ActiveBlastZone_mBD2A19909BF1E94DDB05BB5BE1DFC73BDA9C063E,
	Grenade_get_ActiveBlastZone_mBE3283B53B2F6FB12AAD9205D641398C205C584F,
	Grenade_Explode_m8697ACE2B00F4F41565957A9557A2CDBF0D36E5E,
	Grenade_OnTriggerEnter_mE73AF1BC0C410DE23A915A3F1C98F384CD05A137,
	Grenade_OnCollisionEnter_m3DFE013A1F3360E744952388E563F639620B5227,
	Grenade__ctor_mF6563EEA4F8B30F92D150495AE3703A76742AE6C,
	HelpFinger_Start_m2363DA6BAE13D4B7DD00B4487E8970E709EF5449,
	HelpFinger_Animation_m4AB5C0439E3F8734466C97C2F562782175111DCC,
	HelpFinger_OnDisable_mA9DB38D127A274937517D2E486513BD0FBD77377,
	HelpFinger__ctor_mE6053A467D059683B7234C9B6A833D61A99C411D,
	HelpFinger_U3CAnimationU3Eb__5_0_m7966801C81B7383570A124399F1FC7B34F278D70,
	HelpFinger_U3CAnimationU3Eb__5_1_mEAB22B915FE8C633F97B194E579DB4653611C3B4,
	HelpFinger_U3CAnimationU3Eb__5_2_m4A4F7D5E4EDDD8DC06788DDFBF3B8CDA736AB3BF,
	HelpFinger_U3CAnimationU3Eb__5_3_m90B29EFE63D31BEC88FD59F16B309885F7D93AD3,
	HelpFinger_U3CAnimationU3Eb__5_4_mE1034B2780EDA827D07A650C9EC8BCA5259B2F7E,
	HelpFinger_U3CAnimationU3Eb__5_5_m0DAACD364CA9636D3D0016E98D07DBC24BB282FC,
	Hint_Awake_m5A912001BB4DE311DBF4E9D4B357CC94A3EB6608,
	Hint__ctor_m36E4D8E419E289FEE404EB8B51470CDAF2F1A810,
	HintButton_OnClick_m5FD3BDAF2EDACC9F60C2B61C7E175DA361F378C5,
	HintButton__ctor_mF0B74E033CD5ACC2904A77229C733E13B864EC91,
	HintSuggestion_Awake_mFE3596DD8C6C81FA3C1656C4636F99369A2C3F2D,
	HintSuggestion_OnEnable_m97A37B4B6E3EB8D48DFEB55D9CBC60A4ED026CA1,
	HintSuggestion_Move_mB1730A0FB040AC560A7773F9FB24593F11B6AF14,
	HintSuggestion_OnDisable_mFE72A8A53197023CB2F442908A008119DBBAA6CE,
	HintSuggestion__ctor_mCF14DFE0FA051C30892DEF4C7BAE740A1895D697,
	HintSuggestion_U3CMoveU3Eb__7_0_m3F091493D90598D3AE0222BE15229CB41F1C8878,
	HintSuggestion_U3CMoveU3Eb__7_1_m8F9C586207E3C7AECE29FBFE888DD21626C14FE2,
	HintSuggestion_U3CMoveU3Eb__7_2_m4AEC6BC293588FD6BE3514AA095EF1F82ED11033,
	HintSuggestion_U3CMoveU3Eb__7_3_m3CD4BAFF03198B940845F1390E707CA1CFABA314,
	HintSuggestion_U3CMoveU3Eb__7_4_mDB5A42A4058B85437122DF626B5588A198DBAFAD,
	EnvelopContent_Start_mA8CF0807E269B510ED4A0E1FCA6B554BB78ABF0E,
	EnvelopContent_OnEnable_m3FF58C03E1CC5C6AD93428CA886AD4766EC1764A,
	EnvelopContent_Execute_mBCF0EFE00A3030FF3DFBFBC3AE6474BCC36362DE,
	EnvelopContent__ctor_m73FAC025B320878F40783C918E4FF9C150DBDE23,
	IgnoreTweenEffected__ctor_m47A2087DECE858369787CA24ABBF8DD3219B876E,
	LanguageSelection_Awake_mCA6261683F546AFC4EDA7CD2F075D914E53C995F,
	LanguageSelection_Start_mCE78C383E54769AD9A827EAFDF791EC73B584E95,
	LanguageSelection_OnEnable_mE542564DF175F4F5F2C83EEAA134318A0F5D71CC,
	LanguageSelection_Refresh_m5E08F1E09D1450F2A2939EC5F234D1222F8BCFAD,
	LanguageSelection_OnLocalize_mEE0511C277F910C0A1CC1C5A35A153848B1FCA41,
	LanguageSelection__ctor_m3F3565EE45E63D6B61CF77BBC5C8541DD7508281,
	TypewriterEffect_get_isActive_mDF744890861759BFACD85BDD0D9EF5F6B7A3D2A3,
	TypewriterEffect_ResetToBeginning_mEB2D990202B941ABC8F3F6DA4FA6926A5044AE05,
	TypewriterEffect_Finish_m445F6F2DA6F1FDCDFE1E4C1F862C3115C970AFDE,
	TypewriterEffect_OnEnable_m4D0D919F0B7D1FBFBD767B9BABEADFDA17C83A8B,
	TypewriterEffect_OnDisable_mBFBB51E629E4FAE306FCE63009EFB2DDFD59D18C,
	TypewriterEffect_Update_m6084CDED8728BD6FF5B899D62ED9AA16C4DA4E6D,
	TypewriterEffect__ctor_m089AEF3649474C1323B991956EA28A76A48A38D1,
	UIButton_get_isEnabled_m2FD4683F7A2D9B3C32F16788ABDA873A9EFEF7D4,
	UIButton_set_isEnabled_mE662DB68D88B5E53E9B34000190C7DAC760444C4,
	UIButton_get_normalSprite_m3C45AFB9B0997AB1451EFE23F68908BD8639E0A5,
	UIButton_set_normalSprite_m273701A19035BAFCB2C96FC37E1072D079C073B9,
	UIButton_get_normalSprite2D_m535472D38151E04B292629B04E2954D7814D99F7,
	UIButton_set_normalSprite2D_m35BB2077977AC84EE2B605778AA0959F1B00A40C,
	UIButton_OnInit_m6E5F2C43371BA6A7A38CFA01927FAA8ACAD7F35F,
	UIButton_OnEnable_mE00AD702E2526B9F40365EF0085F58BFAD02117E,
	UIButton_OnDragOver_m618DC0180DD5E34585384469B0C044DB4CCB6DE5,
	UIButton_OnDragOut_mD31DB94A7777AED7E5AE6BBF766D640CB918627A,
	UIButton_OnClick_m746B0FBF08CD2A1766CBF99E4B9C938A204334B2,
	UIButton_SetState_m1CEE62E7179C5F5CACB4FCFCCBF95025590EBA58,
	UIButton_SetSprite_m36E8BBE78C879C75B3E8A2998EBD5AA1D4D89D2E,
	UIButton_SetSprite_mDA23A834E7F63528027A1003682AC554F17C68CD,
	UIButton__ctor_m4A0F43C37DC2224AB2BC2762ABA286C62BC9D153,
	UIButtonActivate_OnClick_mE3187B7834A657827F7208994138D1FD602D2E01,
	UIButtonActivate__ctor_m2431A96B4B37B5FE049A74CF7DAD6BC12DA502D5,
	UIButtonColor_get_state_mB3651F93248BA070FA1821134AE823B0883D7619,
	UIButtonColor_set_state_m3828E9F4B0866A761C8A501FD82D3B3B071C23D4,
	UIButtonColor_get_defaultColor_mEA5A7C42A0838BDD46BD6DCA6F15F29277B29C07,
	UIButtonColor_set_defaultColor_m7088FFC1DD176E70B501BAB7E9ECEE39DE37EFA1,
	UIButtonColor_get_isEnabled_m16C957509CE78C76C19F666529EB5636E16F88D2,
	UIButtonColor_set_isEnabled_m878A12A0297E4D440BB8B3169DE5A843249D362F,
	UIButtonColor_ResetDefaultColor_mAC5E5E1A3014392857EF6707CDAC257843484DC8,
	UIButtonColor_CacheDefaultColor_mAFD95B3C8997A8AD6FD538727EAB1D1BF7241C9B,
	UIButtonColor_Start_m20D055773BB3381859B9B7482EDFDDB65EFD5A29,
	UIButtonColor_OnInit_m83DDB1F906298BBD5881E4AFE40A72A78B2175A7,
	UIButtonColor_OnEnable_m8E7643BA61DA89F819DDD84A44E9A7FDADA129CC,
	UIButtonColor_OnDisable_mD3ED869565E278ACE08DF48D0BEC3E7CCD0737EF,
	UIButtonColor_OnHover_m0DD8436B10509F870371B55B675705E8F0DE9F9B,
	UIButtonColor_OnPress_mABF30CB6E52447D4AC0D70CDFD2A7672D0C9627F,
	UIButtonColor_OnDragOver_m1965D57809CF7E7289152497C19C328C0DDB7548,
	UIButtonColor_OnDragOut_m9B567FA50C7750C94BAD1B2F00D9E3C3CD6FBF92,
	UIButtonColor_SetState_mF83DE6B610A0256276DEF3B5BFCF7EFF186061F3,
	UIButtonColor_UpdateColor_m535110FC72BADD526DB3A331D807873D7879A310,
	UIButtonColor__ctor_m6E3A936A8B53EDA2A0D6BFD284F0199606BA4908,
	UIButtonKeys_OnEnable_m17C90D674890A41BBF6E83C3032BBAFC1CE539F9,
	UIButtonKeys_Upgrade_m33AB6F795B993DF91DF32BEDCF6E272E36E9417D,
	UIButtonKeys__ctor_m685CF371629C9012C2B021E28F6D57916247C075,
	UIButtonMessage_Start_mDF22F5BD1742161BE235AC386FDBB69CE43B20FD,
	UIButtonMessage_OnEnable_m1DDCB7253D6E6079474F1457A4645BD05049440B,
	UIButtonMessage_OnHover_m81033B505E5848850764D01E10CD7CB3AE42EA05,
	UIButtonMessage_OnPress_mEEB6F5D016E2DDC8CEBC9597548C29F100AF08FE,
	UIButtonMessage_OnSelect_mA36D45554ADF2B3FF34C1C93D45AD1405A65855A,
	UIButtonMessage_OnClick_mD58CEDF275E0F3DDC7BC26B189647B8950AD951A,
	UIButtonMessage_OnDoubleClick_m096113586EB46039FBFA09C70CC67EB455A843CD,
	UIButtonMessage_Send_mF7FE4BEA344C5D44DDB4BBD6EB09D42A019C1A88,
	UIButtonMessage__ctor_mF10D96603C7FBB1D663903AD1B20609E0D3BFFC1,
	UIButtonOffset_Start_mDFBE32D11820643E0B82524383C02F53AEAD8970,
	UIButtonOffset_OnEnable_m3092801FE1B4F76042C9675F03396F041B123C93,
	UIButtonOffset_OnDisable_m7D9253A01CAFC9546CDA5D020ECE5F9F4CD92048,
	UIButtonOffset_OnPress_m91DD6DE47B3434E870C3945F24CBBA313E6AB870,
	UIButtonOffset_OnHover_mDBBE95D597887E6A8D25554957CC64EAE8273C03,
	UIButtonOffset_OnDragOver_m7A617E029D09FB0897A326BF8877BED9C226E30D,
	UIButtonOffset_OnDragOut_mA5913462E172BA63DE6B91458FF99D29DF08D38B,
	UIButtonOffset_OnSelect_mA469F17DEF1120EACABD7334D5D02CFDFEAAF660,
	UIButtonOffset__ctor_mE752B40BD3A400AB9AFFF7EE2116D1DA8745547A,
	UIButtonRotation_Start_m6F29B40D5D1725CBE08138D04DAD47E369088802,
	UIButtonRotation_OnEnable_mBF8EE246CDA2FF6DB3A7870C159EBA972C893862,
	UIButtonRotation_OnDisable_m1E9418C958E284454674025BD4EB881FBAA197E3,
	UIButtonRotation_OnPress_m803095BFD4365B62AA559A18B882A14FD6765EEE,
	UIButtonRotation_OnHover_mF7CF948826EFA7F857CE7D399149A4B104F7A963,
	UIButtonRotation_OnSelect_mF85D4B056F10529A6A33F6E7030C143EF0A1B87F,
	UIButtonRotation__ctor_mB962AC4EB462AD10C56531EFB77816A1212672F6,
	UIButtonScale_Start_m36D879E62C1FE5C6DE04E3EF2220AA633A47CD3D,
	UIButtonScale_OnEnable_m33E03B7D9F4E1EF8665F6874AC3FA76D5337651B,
	UIButtonScale_OnDisable_mF6D957FB5764B99A819FAA9C1414CC429C249143,
	UIButtonScale_OnPress_m00B00D9CC5347A11681A0CFC59A6AEA3A7A3F7F9,
	UIButtonScale_OnHover_mA915CE55994AF1D8A3590BE45B7A01F5B3E72983,
	UIButtonScale_OnSelect_m9AC99C68F49BB6BFB7007CEAAA101554639DFA43,
	UIButtonScale__ctor_m6CAEC36DE2F3FD0126712E5995B30B19C3674928,
	UICenterOnChild_get_centeredObject_m92EF5A688C6DFF9D5D2A74E2B429FF45FDF34C8A,
	UICenterOnChild_Start_mE8706D1144B2BF50DF9422F8C2C9CC8C78260FCE,
	UICenterOnChild_OnEnable_m1D49EE117F49FEDCEB61DB0026D8F5653F34EA63,
	UICenterOnChild_OnDisable_m199DF6AF8CD243CB25EEFE6DE55187F21E9F3637,
	UICenterOnChild_OnDragFinished_m8D041F88EDE902CF5D9EE5610CDE6231237428F1,
	UICenterOnChild_OnValidate_m124C1647A3B6FE7D023A93FBF1E10C7E9892DB8E,
	UICenterOnChild_Recenter_m45576A58178C385C2FAA4DC834B6772DFFBB83EC,
	UICenterOnChild_CenterOn_mC44AE09332F4A7F84D6859E4E74BBE4E8E7EE115,
	UICenterOnChild_CenterOn_m177289AF868A04AA055AC40B6E29D1E2ECB07D7D,
	UICenterOnChild__ctor_m810084F508F62E3CF933B72CE2AFB508749CD742,
	UICenterOnClick_OnClick_m51CC1D8EF3C4BE8E018A622E2ED7B21356783FC8,
	UICenterOnClick__ctor_m697B670D4BE91E2987C372EEC4BBE57EB297A02D,
	UIDragCamera_Awake_m95ADD3B90A53A782F613A44A2656AF30A3DCB42A,
	UIDragCamera_OnPress_mF402CBCBCBD736FD68A81B3540CB7006A58CFF87,
	UIDragCamera_OnDrag_m4BAD847B3118436329C543C4969D60A053B94345,
	UIDragCamera_OnScroll_mDF2D5C560FFD4DF6AA5A8FA29C0C137D7538FE6B,
	UIDragCamera__ctor_m36303D8AD1BF425A1A7BE58620F3206336EA6326,
	UIDragDropContainer_Start_m5453BC269F8E4FF6682E272EA9838BFD5A117AC9,
	UIDragDropContainer__ctor_m2E8A0DD94AE9D7F9505DC463B9CA439BBDCACE4E,
	UIDragDropItem_IsDragged_mE1E78A0B2A397AF142EF1373083FB6831CE7B225,
	UIDragDropItem_Awake_mCDAEF910EBF0A243F6E813948E2A4E63895FD440,
	UIDragDropItem_OnEnable_mAFB3E46F47A04E77E1554926C764BCABE9F3398D,
	UIDragDropItem_OnDisable_mF20A65223E8B8137292EF81AAD27ED862DA5475D,
	UIDragDropItem_Start_m33D099E2C7A4527AE8178F1148CDDDE828508165,
	UIDragDropItem_OnPress_mDEA5BFE97C7BAFB93A488A096737DC1E2B91368E,
	UIDragDropItem_OnClick_m9040BCABD2AD9B39D28D05F7DE0126DDD3E76DB7,
	UIDragDropItem_OnGlobalPress_m0720D23935D08E98EAD778051B6E9A55E702DA57,
	UIDragDropItem_OnGlobalClick_mEAFD5B7A71E29059589AE76F24E4845720F580F9,
	UIDragDropItem_Update_m53789A239B178DE89DBF4332A6BAA8A77DCF5F72,
	UIDragDropItem_OnDragStart_mD37DB37FB57E96BA5001EC8DB1ACE2BC982A0D6D,
	UIDragDropItem_StartDragging_m42BB2CE0982A32CD7283C5D4BF56487030DA577F,
	UIDragDropItem_OnClone_m85C6B32F66A39D8FF6F4F16BE2B17A69E2E711AF,
	UIDragDropItem_OnDrag_m4AF72B38E3A548B52DAC86E787FFB9B3C5D017DF,
	UIDragDropItem_OnDragEnd_m6FB99544DA12345D0983356D028857833B8ED487,
	UIDragDropItem_StopDragging_m6A46A851FF35A4FF187FA15FB9A815A2F4134795,
	UIDragDropItem_OnDragDropStart_m514076B707C1A000E511C1DB8A3AFDA04CC3A2C5,
	UIDragDropItem_OnDragDropMove_mDD2EDA276D367D7D2AF2DF6E85BD54BD61032601,
	UIDragDropItem_OnDragDropRelease_m1A8A2EB71DA9BCA028E3C17E54A5F524ACC3F0F3,
	UIDragDropItem_DestroySelf_m42EBE971183894A7EF733EC61B9789BD0DBACCD2,
	UIDragDropItem_OnDragDropEnd_m60B8E30891F91817DAFCDC224F17C168ED44771E,
	UIDragDropItem_EnableDragScrollView_m7A8710D5021A4C47A1DDE247B2BF602B82812FEA,
	UIDragDropItem_OnApplicationFocus_mC8E8D35B42C2427B220B1F169CE741FBF5717CE7,
	UIDragDropItem__ctor_mCD97905B117E6D8FEC9D6D64C067F0E777D6F56C,
	UIDragDropItem__cctor_m1BD9A105F3EC2EDAA34EBBCA86BBE2A924CEA80A,
	UIDragDropRoot_OnEnable_mCB2E357733A98E0723ED04AC9ACFDBF82C5A5060,
	UIDragDropRoot_OnDisable_mF81CC1DAAE2FF0D46FF209AAD17AB170341C30E5,
	UIDragDropRoot__ctor_m0C290E6E3C6F2BA3F0501B8EC20520F1080D5929,
	UIDragObject_get_dragMovement_mCA1FCD436283E9972AD6104DCB4233AF1CA59C6F,
	UIDragObject_set_dragMovement_m1E4F6ADEDE33B18E0E6227B6DA8D735106C52E06,
	UIDragObject_OnEnable_m8C491A36D8D8D61AC2375D014C4D56843FD7DDDF,
	UIDragObject_OnDisable_m693588068171A0E5AFD5E8F9ED1DB82E993A6661,
	UIDragObject_FindPanel_m19635BCEE5E51CF6BEE9DD060CCDABE94F7D56B4,
	UIDragObject_UpdateBounds_m907D191F4938DCD09BD58489BA63BE8B073146B4,
	UIDragObject_OnPress_m045D6CDC167D7A6AACFFDD2625E73B92A74A2BA2,
	UIDragObject_OnDrag_m69D848FA076E42F5EB81B522164AF94605FC6BFA,
	UIDragObject_Move_m8E7037F042BFBB1660AE551DEB59734FEA350827,
	UIDragObject_LateUpdate_mA1AA5422382AF68FFA6FC7F14EA37682C81C4A23,
	UIDragObject_CancelMovement_mBA59993B6A2DDCDFF562772E8DF963A8DD8182C9,
	UIDragObject_CancelSpring_mF00B5F50C39C4A081AA4D3FAEC8AAFE8A9C92C0C,
	UIDragObject_OnScroll_m92C12F1EBEFCB447815DDB97A9BC9C3145558943,
	UIDragObject__ctor_m193EC1DDAA69B9C5E3013FFAEAE10AC5B568268E,
	UIDragResize_OnDragStart_mF818E2D19872230AED998A1B718DABAE4E6D2AD9,
	UIDragResize_OnDrag_m2EA378CDD9A209CE12E884061A98FEB0C9197D18,
	UIDragResize_OnDragEnd_mFF975FCF3C136FC43E8F9F49609814F7D7C32507,
	UIDragResize__ctor_mFE27213B79114D72224661F75FA435425F94527C,
	UIDragScrollView_OnEnable_m6C3469996BF75B08C0FC21A35635D2F71581F2DE,
	UIDragScrollView_Start_m51F1E990719813E0E24F6A672AA29F7FBC9516DF,
	UIDragScrollView_FindScrollView_m749EF4217ADDBEA6A92494F49EC2E1013DDD33B4,
	UIDragScrollView_OnDisable_mD0AABC43FC6B20F5D092D6D21364EF2217B71335,
	UIDragScrollView_OnPress_mAAF609E5262143CD168D6A097863EBABB539C1A2,
	UIDragScrollView_OnDrag_m4D1ABF2963D4C0A2854D38D6E8828665DAA98FAB,
	UIDragScrollView_OnScroll_m8E6A33985153C6A43BB2706170E395FCFA192617,
	UIDragScrollView_OnPan_mCB6964497DB8037216F02254A7AC372765DDDA5D,
	UIDragScrollView__ctor_m9F05CE583A0D9F656CFA79B81833E4230E88C4A8,
	UIDraggableCamera_get_currentMomentum_mF30D9D816EB25DA296B660FAAEBFEE958ADDBA8E,
	UIDraggableCamera_set_currentMomentum_m5FDF2C53A844B9A699B1660D8E60919CFADB8E9E,
	UIDraggableCamera_Start_m6F68B8F67C8C7586905962A5EFDC7AC4225F7E47,
	UIDraggableCamera_CalculateConstrainOffset_m4C828D5602CC2066922C5E3A2B60914BB2F507A8,
	UIDraggableCamera_ConstrainToBounds_m8525F6CAD58BF5AE230B20DFCB2953A7A665011A,
	UIDraggableCamera_Press_mA1AFD4D1C0235A2E795741D64F56A896BBE5A426,
	UIDraggableCamera_Drag_mF3798CDE126AFBDFC2A6CFD75AC48A9193B65657,
	UIDraggableCamera_Scroll_mE5C69C7C65BC569901A3ED876054E409BB0378C2,
	UIDraggableCamera_Update_mBF387AFB3953557D0EADFD615019C84C96B4AEEF,
	UIDraggableCamera__ctor_m0E19665CE642E764FC54144E0A6A2EE2FF84F8BD,
	UIEventTrigger_get_isColliderEnabled_m23DB8A580D131F0451E62F309E9870E51D9663D5,
	UIEventTrigger_OnHover_m463B3B1DA12F79C7F094E68C136C385EB408D1BA,
	UIEventTrigger_OnPress_mCC225ADA0D55161ED99B1B313324DC7DDFA80F54,
	UIEventTrigger_OnSelect_mE5CFF980D84B7BFF090A78D5D6608D3F7CD35A0C,
	UIEventTrigger_OnClick_mA5CCEE27F2C20989CDE8AD83ADCDB5EE9FCE2FF2,
	UIEventTrigger_OnDoubleClick_m526DACAE338AAF593A7FE0C70A94291304FFE9DD,
	UIEventTrigger_OnDragStart_m8666A2F6982DA59A0098D3DE2D8BD32AADFA5E91,
	UIEventTrigger_OnDragEnd_m59DF96E13CF3D00EE54D9065D921636C5D5AC1B4,
	UIEventTrigger_OnDragOver_mE6FAA2E310292B6EDDA183D2E4DB2CEC462127D8,
	UIEventTrigger_OnDragOut_m77AFA2AB1F5CC006F95FEAEB287EDC4AF490A56B,
	UIEventTrigger_OnDrag_m74D73947A3517E880ED7578D97F750131CC8E8AE,
	UIEventTrigger__ctor_m5DF2C3EF6CE4BC07880FAE6837C12A358EB23A38,
	UIForwardEvents_OnHover_m1C909C7194F715A9B1BA7076F937F2649204FDAA,
	UIForwardEvents_OnPress_m87E7A0353B6DB1CC8005CA068F59E8DAF41E8E3B,
	UIForwardEvents_OnClick_m8CFC1DAF5B53F896662812DB1F675423856DE20C,
	UIForwardEvents_OnDoubleClick_m388D025345368BFDFA2EBD33926328D7BA36B3FF,
	UIForwardEvents_OnSelect_m5B5EDB7D032F1F78CEDA02AED8DA1737CF92DDE8,
	UIForwardEvents_OnDrag_mB2AF4038A62C555675E76C39112F2319B149BAEA,
	UIForwardEvents_OnDrop_m0751CBC285C2C68C93AA8FC65FC4CE98ED09A91B,
	UIForwardEvents_OnSubmit_mD4ACF667A9D3A7F4022B9D1E1F2785AFBE593CC3,
	UIForwardEvents_OnScroll_m3A4DEEEDF7794BB86BDA70D67A3607D5899FDD47,
	UIForwardEvents__ctor_mA981BE30A04DAAF48FC05B3497E5249365112952,
	UIGrid_set_repositionNow_m1603D14191A19572B6A3E67B4B36136191B3EB5A,
	UIGrid_GetChildList_mBF55B31A144A258BFCE0A41D4DC2FB8A6D9E41B3,
	UIGrid_GetChild_mDB3728401ACA24DD26F2E05470C856A98A5A0FB8,
	UIGrid_GetIndex_m304D1E3DACBE9D093902C5E80242CDA9AF836416,
	UIGrid_AddChild_m97F2941FDA82984D2DF2FBACDB687A2B4FC7C8F5,
	UIGrid_AddChild_mC323E94BBFDE2C9D13D3B41573D4D4F7C2AFB1EE,
	UIGrid_RemoveChild_m3E986D2F0ADAF45C4EBEA0A703D566F327F2EED9,
	UIGrid_Init_mA3ACD66F71796BFA10DC122BA6028291EC8D2A15,
	UIGrid_Start_mCC4298198BC3B8C8DD837EE50F9132F6BFC7AFDD,
	UIGrid_Update_m3BE0E55A2DDF97B6DF247EA0C8188901AA31138F,
	UIGrid_OnValidate_m415C190A86A6C78162050FCF51E2B4373E8CD9C1,
	UIGrid_SortByName_mBD267987C8F95E72644759FD9DE0620EC114A80A,
	UIGrid_SortHorizontal_mA4BF98B85E30F2254DBC3D3CB2B643D7BBC22A30,
	UIGrid_SortVertical_m255D05FB979DE2367E3DF2F23ABDCB287C90800C,
	UIGrid_Sort_m1BBBAC6C5D2E9FDF6DBE2D48D322AA38B80856A6,
	UIGrid_Reposition_m1C13C5769255924877A5296C1DAC3B4AC4AD6D7C,
	UIGrid_ConstrainWithinPanel_m0038539A9EDCE670A8749B239E554326E490A66B,
	UIGrid_ResetPosition_m32DFAA4A1BF7056C029013816B4281C36651D86F,
	UIGrid__ctor_mDED7BFAAE41A54D80267902DCD4398D849E2E6CA,
	UIImageButton_get_isEnabled_mBBD878BA79DE396D0DA9BC0E32D5E9D13268E1C6,
	UIImageButton_set_isEnabled_m0C7FBFBEDD222E03211092784D1466E1A0CBFED8,
	UIImageButton_OnEnable_mF111BD167EF1033B4A03B17F51A1F100DD0C4413,
	UIImageButton_OnValidate_mEDE63BD019577A82276BFDCB1414AFB461113C02,
	UIImageButton_UpdateImage_m3A472A2A805AF619671F5CB3FCEC3F8FB629FC7F,
	UIImageButton_OnHover_m6CF694AA9B9175236C90F094F02A5908D00C03C7,
	UIImageButton_OnPress_m211D6BFE8734ADBA5BE0AF2F3C90A569CD11834C,
	UIImageButton_SetSprite_m526B94C262EE51C1D9A34788788E6578666EF60C,
	UIImageButton__ctor_mEF98213CF7C8FD0FBA3C6484CAB4470967E51B8A,
	UIKeyBinding_get_captionText_m56B9C2FACA7424C4FD409F25C74E8E70D3144E0D,
	UIKeyBinding_IsBound_mABC0B5C68F107C310412EBF2A77C69DF663917B6,
	UIKeyBinding_OnEnable_m5312CA135C64596E02E33022834D07CB414D1B30,
	UIKeyBinding_OnDisable_mAC878B7DE84BF7AA15A25AA456CF122EE2A21083,
	UIKeyBinding_Start_mB0A66CA38CD9769B28BF081515B19295418A561B,
	UIKeyBinding_OnSubmit_mDE45D3D1F751CF98D9056E3D44460FD841C0675C,
	UIKeyBinding_IsModifierActive_m13AE9B598C78B5CAA418367F91428438117D7AC0,
	UIKeyBinding_IsModifierActive_m154E18402BF0DBFB099BBB6C25F9BABEAD85426B,
	UIKeyBinding_Update_m2EC70818B5A3A0214E08AD5589A9010CAFD6E280,
	UIKeyBinding_OnBindingPress_m64538DB62A864D1FD240CD6368A5C84CD15FDB93,
	UIKeyBinding_OnBindingClick_m283E30A69811B905EE00E8C50061936157A6B11C,
	UIKeyBinding_ToString_mA9FA9DD4425B9D3414D1C11AEE89D88895BBCE5F,
	UIKeyBinding_GetString_m8FC3EA682C6E4ACFED2BF6827E9D1F2DA5E9F9E7,
	UIKeyBinding_GetKeyCode_m2A2400ABE3FB5AF4B0953515272E5641C438E6BB,
	UIKeyBinding_GetActiveModifier_mDFABC18138E6120AA25771C4B5130BDE96E13497,
	UIKeyBinding__ctor_m1199987FEE9D64A3F1D0835F67BC826FFC1A78EF,
	UIKeyBinding__cctor_m9A833F0FF39867E9D7C248AB69FEC9747600C2CB,
	UIKeyNavigation_get_current_m65C7A3877CABAA1870E386E8FBAF6812EC8232D9,
	UIKeyNavigation_get_isColliderEnabled_m108E320475DDE432668DD1FD2CE715F5DD9DD92E,
	UIKeyNavigation_OnEnable_m1172E73D7617D4576F6926810DBF81A2FFED976D,
	UIKeyNavigation_Start_m0D774F0CB0D8C8206B978D1A901F69C480B02C31,
	UIKeyNavigation_OnDisable_m07813944DF55C4897672BEBBC4E352EF0146C7FB,
	UIKeyNavigation_IsActive_m2AD4400A6E9E5B6D1E7CCEF27C73A6CC66A0D0D0,
	UIKeyNavigation_GetLeft_mF88E89F8202975E34B29C955F4C202660739D307,
	UIKeyNavigation_GetRight_mE2CA0A37F544EB81D82E67CDBDB91573C9FF9F53,
	UIKeyNavigation_GetUp_mD86582EEC1D594339B22B02FA8DF282A71700023,
	UIKeyNavigation_GetDown_m0435F8557C9DFBB6BE5B4E720F3E345FCD835440,
	UIKeyNavigation_Get_m0D8A264AED2B206D279F396BDBA0F05505DEA03F,
	UIKeyNavigation_GetCenter_m536C911EB7FD8D0A1D106337F4DD4978450D7869,
	UIKeyNavigation_OnNavigate_m72C501354CC0C5F15ADE87F01E93A9E178EA5CE8,
	UIKeyNavigation_OnKey_mAEC0DED4792AA8974C8AB4FBC48CDADA23755417,
	UIKeyNavigation_OnClick_m1DFEFA51E2912B60ABE698416E309449E265E654,
	UIKeyNavigation__ctor_mCA7ED4685797DE931EC550E459A9F239C4D4C9BE,
	UIKeyNavigation__cctor_mB4571D5DB13FE2BC4AD60983D893834773F5D43E,
	UIPlayAnimation_get_dualState_m83DDC90751A08E92E1486BC34EF0343D9AFAA187,
	UIPlayAnimation_Awake_m9564666A1A68750944C6512D0B3E28085D819EC3,
	UIPlayAnimation_Start_mED36B8D375A1072918BC5AF46CBA7F54CC2B0FAB,
	UIPlayAnimation_OnEnable_m01ADAD12440E02FCDC144DE6C2F37C4F989F0EBA,
	UIPlayAnimation_OnDisable_m307B66FA29FEFDC0E81C8A5EC4604E3C01DA7751,
	UIPlayAnimation_OnHover_m9919217BF7107602DCB58803E3B510F75B2A68CA,
	UIPlayAnimation_OnPress_m0F6FB02D193B2C1A9EFE3B2D348828690105F04A,
	UIPlayAnimation_OnClick_m31DA948FE943384BACC7B5AE95E8D5AE75941828,
	UIPlayAnimation_OnDoubleClick_mC94A00C839EFD501D9755BFE13DC68694AB2FA9F,
	UIPlayAnimation_OnSelect_m93BA74A034306C3F1F7A294B27174C3A67C7E563,
	UIPlayAnimation_OnToggle_mB8D4D03A1E25DC269A279019F6B789033CF60C29,
	UIPlayAnimation_OnDragOver_m14F14F8403F548DFEED277F35349D45A2C6543ED,
	UIPlayAnimation_OnDragOut_m89FB9389A0FC68A82537486E8DE8915C09EE400A,
	UIPlayAnimation_OnDrop_m196A91AB7278E64F3E2E6102D4A50E051F209F5D,
	UIPlayAnimation_Play_m07F867F76B1986FD50243CBE7460F508B180FA19,
	UIPlayAnimation_Play_m61A0B09B3033385A0AF976040DDF43462783CD4F,
	UIPlayAnimation_PlayForward_mB8C590FCFA9911968C2490BD9B316A69906C5706,
	UIPlayAnimation_PlayReverse_m5FA91CB92118F00CEC9D6A1B51A01C289D736374,
	UIPlayAnimation_OnFinished_m94EC659F1D7E9F21844785ABC4903B7B36EF0E74,
	UIPlayAnimation__ctor_m2146F6096F3DAFB2F8AFE128549652D800F6FA76,
	UIPlayAnimation__cctor_m1DE4207BE5018BCEA2A5B8EB8A0E7DB40567FDB7,
	UIPlaySound_get_canPlay_mBDB2CF6B1ADA75D33674CEEAD709E061F58F46D9,
	UIPlaySound_OnEnable_m5A56FAC10F2754D0282EC5E311DF0944DD41E3C7,
	UIPlaySound_OnDisable_m11F7C7C1B0DA6EF6DA7B184D34B17B4FAADAB01A,
	UIPlaySound_OnHover_m8CEA76E2B0EA1206EEB9CA38EBD279C229F69301,
	UIPlaySound_OnPress_m4117AE3A33D6339313587E5F8994E4093B0004E8,
	UIPlaySound_OnClick_mC93B0B25E001F5AD9E7FA0CF94A65741D7CC14C7,
	UIPlaySound_OnSelect_m4415DEA521A271BDA8DF0DDC9910C1B487DEBAE1,
	UIPlaySound_Play_m0061729E81BD3C86A8E316B6CBD045E0190E5024,
	UIPlaySound__ctor_m10048EF46A70C46BBAE439F081D1656BD4E1BB84,
	UIPlayTween_Awake_mB4024D84F8D908016270AF5663F02BC388E31420,
	UIPlayTween_Start_m3C1C9594B2FAB886CE09A1A9E0473490AA3107FC,
	UIPlayTween_OnEnable_m9F967299AAC0E593925086890D3DD5497EEB401D,
	UIPlayTween_OnDisable_m79251DCB9A38E1C87E527683DBBE85A0FC608DC6,
	UIPlayTween_OnDragOver_mB51A3EE1985165060047280089F22F88FDCA0207,
	UIPlayTween_OnHover_mFEAA07AEC61E31BA01E2DFD7B6F07F7DF022D98A,
	UIPlayTween_CustomHoverListener_mE8D16EF27FBE16D0F779A6D17B21EEED77A5DD6F,
	UIPlayTween_OnDragOut_mEF4C68CAD90C11C0898F5D311F9B0A39FDAA5ABF,
	UIPlayTween_OnPress_mCD60ED4013F3D41281C39933E8981311C58206FD,
	UIPlayTween_OnClick_m4724BC7614AB4B5568C556BBD8C65C21F429F249,
	UIPlayTween_OnDoubleClick_m0DCAAACF00E76F3DB35E00954C20433A3159BAD3,
	UIPlayTween_OnSelect_mCBCDC9965D0B4820EC0AF64FB9A101F3DB8398FA,
	UIPlayTween_OnToggle_m6586ED60804F4F7F7316D18AA675C8D8B2767D10,
	UIPlayTween_Update_mC7162C3A2C6C3603BDE98FBF1A91BADB6D0764DC,
	UIPlayTween_Play_mF79D3A65ED63A957518677123613E54E0892696F,
	UIPlayTween_Play_m888612B3E1748B69815F4EB39C54CF32DF874569,
	UIPlayTween_OnFinished_m3059ED2205994D0527D91F5708EEBB43B6FF332D,
	UIPlayTween__ctor_m92C7977B6139F6027BD56752B30B365C28786A10,
	UIPopupList_get_font_m755720AB763E79DE52E6FC79C8E6A1949C43BD6F,
	UIPopupList_set_font_m978876D116A04B98B7A29893355D0C26B25B6CC3,
	UIPopupList_get_ambigiousFont_m48C069DF661E2A6C56022EDD7AF73964622AD346,
	UIPopupList_set_ambigiousFont_m881EC17717EAFFCA75F4C303FAEBB426F55601E0,
	UIPopupList_get_onSelectionChange_m6C84E18D2517E443A7B379AFB7443F47F39CD046,
	UIPopupList_set_onSelectionChange_mF27E47B196F197A221A2C2CC514C2FA393B35538,
	UIPopupList_get_isOpen_mB51CDDAEC4E0D6470E74DD451092686C1D1AB302,
	UIPopupList_get_value_m1C19DDCB845052B3EFC6DDBD7588FC515B97D2C0,
	UIPopupList_set_value_m403169A41570EA13671458E80A1B0B5BDC9C2B16,
	UIPopupList_get_data_mA99BD1BE26DDCD95FC9B9AE0F1EC2ABB35F5E73C,
	UIPopupList_get_callback_mCF9BC77A1209AAA6F984E87648C2A4C9ABC87A04,
	UIPopupList_get_isColliderEnabled_m00D0B325D3D364599C7533871D194F84299620F3,
	UIPopupList_set_isColliderEnabled_m222979567114E4CE149F034E39CC56C1341C4BE0,
	UIPopupList_get_isValid_mD489AAB9CDA735A923FDA5159E31516138129D70,
	UIPopupList_get_activeFontSize_m7E73F86FB67DFB514A7618AD8017B9EF8937B497,
	UIPopupList_get_activeFontScale_m71154DD0A5DC1CA0F374AE5C75F1435670118C08,
	UIPopupList_get_fitScale_m1155731EE721FF1AC95FF963DC656645E12E34F8,
	UIPopupList_Set_mBE6012BD9249FEFDCCCF4B943D740FFF6675BD6D,
	UIPopupList_Clear_m3D4E7F208FCF79B1426BFD9AAFB8E5E7DB261714,
	UIPopupList_AddItem_mB5C3FEBB2714BBC7ED9804B780EB287BEF9BF2C8,
	UIPopupList_AddItem_mA55C04144AC64E3718067FF5A03C78B4D30FD176,
	UIPopupList_AddItem_m73B4F5B3195E6B01B3FA3E59E755EBF4BD5F47F2,
	UIPopupList_RemoveItem_mBE5C047AB372A3D24127376473557B966A5277CA,
	UIPopupList_RemoveItemByData_mA6029D6BBA6C7A8D5A7F93FEEF02B97D4C75DE49,
	UIPopupList_TriggerCallbacks_m87E94A5DEFA3C0F3E765DA5241084883652B40E6,
	UIPopupList_OnEnable_m6C8B586098867B2DA4DBCEB212B8C5C63225CB88,
	UIPopupList_Start_mAD3DED4EB90FC055E0C3C57E7267567B00E6E4E8,
	UIPopupList_OnLocalize_mF5A594CF60C14F42CBC6C3E5E7831B0D2D657CC1,
	UIPopupList_Highlight_mA7BE703328D8EB549E75BC128D4D56926366B321,
	UIPopupList_GetHighlightPosition_m73C3CADC407B047873CD1D34655581CCFF961E12,
	UIPopupList_UpdateTweenPosition_mC24F423BA8DA83DC85E71C50C36FB6E170F3BD12,
	UIPopupList_OnItemHover_mC4AC80D54BAB749E3302510EDD4F8286EEC0844E,
	UIPopupList_OnItemPress_m36425671A749880B1FC68C94BF19162080679028,
	UIPopupList_OnItemClick_m98D5368EB2D40A31A7E5C167DF07774063BECC75,
	UIPopupList_Select_mED1D3BC731A577FC06E44168AA634F4ABF103317,
	UIPopupList_OnNavigate_m7599F3271C6478C282C1910254E85CE093D086A9,
	UIPopupList_OnKey_m4614738622514015966205385C0E24A2E9C76D88,
	UIPopupList_OnDisable_m83D67149CBF39DC86A8705BD6824FCF33C0374F5,
	UIPopupList_OnSelect_mAF448DE1B33D14613B5F6BD1386C507B6DAC7C31,
	UIPopupList_Close_m35F5771BDA0C53348DA9AD3CBBBB52FACA59601D,
	UIPopupList_CloseSelf_m83F0042AA52C04D24FF32C2BE8404C5C4CF8035F,
	UIPopupList_AnimateColor_m4ED86B9114C57B4C8975ED62624063321056C9AC,
	UIPopupList_AnimatePosition_m11B23948D451B788118062868D041533F47D36EA,
	UIPopupList_AnimateScale_m2E4ECC3ABB390E842990A177C75A3CC5601FB3C5,
	UIPopupList_Animate_m79B7576CC5ECCD8351481F69E2346FBB05F48B4A,
	UIPopupList_OnClick_m3384F644A33C286AA8D23CE05ADA0B52467D4AD0,
	UIPopupList_OnDoubleClick_mF585EFFF9617A9917063EA96ED60BBB7AE001C5F,
	UIPopupList_CloseIfUnselected_m4B9BD54464443307CA04BC39707770CEF7075B25,
	UIPopupList_Show_mAC249A84A4C98CBE4C0CC58A535AA6F281E4E347,
	UIPopupList__ctor_mBD0934F3E3ED9F35F63552D69C2582E1A334CFAD,
	UIPopupList__cctor_m987BDCA54E76C94A9A07E4D5EE6B09C74CE0D34E,
	UIProgressBar_get_cachedTransform_mD7DE982C606F78FCDF337E59371622A29C787067,
	UIProgressBar_get_cachedCamera_m5A10D1B37EC075D77ACB558690B254F64409D290,
	UIProgressBar_get_foregroundWidget_m882A8610FCA789B8AB1246C71412B442C94CFAE3,
	UIProgressBar_set_foregroundWidget_m8DBFBB429307818EB08B53690CA3C3923B10C6B3,
	UIProgressBar_get_backgroundWidget_mBA7EF2D274318625A0F4E87D9F368DF248F6AE42,
	UIProgressBar_set_backgroundWidget_m126F7CC67F0B611484675F15C2A2831002ADF519,
	UIProgressBar_get_fillDirection_mCEDB865FF0FFD5E06A784E1F268752881F1ED97D,
	UIProgressBar_set_fillDirection_m4F2A2C8AA75C919769F33F0F7AE17E73C4A61668,
	UIProgressBar_get_value_m699C5C061AFE565F8095732DD062CA322D765DF0,
	UIProgressBar_set_value_mD1D63D223C225E444CC8DC15D7EEC393DDDC32A8,
	UIProgressBar_get_alpha_m4A869A292B27D99F6876609A17EC6C0EE9DE1F89,
	UIProgressBar_set_alpha_mC812C7BA892D114EDCFA2077B5EAB426265DAB36,
	UIProgressBar_get_isHorizontal_mE293BEEE3B0258914EF7D5FF06AE918C64977E2E,
	UIProgressBar_get_isInverted_mE8605106AFDEFC114520CFC482741B19527B9F6A,
	UIProgressBar_Set_m9A5919514AF78A9A2F404F04BA0F70C29C77BDCD,
	UIProgressBar_Start_mF51EEB55D7E42E0ACE23E7C3D26DB9E76EFE82B5,
	UIProgressBar_Upgrade_mCE09BA75A233DB56656DE8A1936424AAA0E87973,
	UIProgressBar_OnStart_mA5D54E6E0D8186F000117DE9F833BBFA33F0E20F,
	UIProgressBar_Update_m793451FD6DEAFDEB1A72582F9269C48DACFA5158,
	UIProgressBar_OnValidate_m55BA7D9E9A9409142C62E88FCE7493C9202583E9,
	UIProgressBar_ScreenToValue_mD7B29ABF2509EEBD04B08510BC0B13B7E14A8E66,
	UIProgressBar_LocalToValue_mC27A9C225DAEF60BD4F6BE105211DAF4B287147F,
	UIProgressBar_ForceUpdate_mBF2C86E17B05B059F74F1070C0EFF51296E7A26C,
	UIProgressBar_SetThumbPosition_m74D1684C22D95CA4E91B3EB5ED93DFD6E85B4403,
	UIProgressBar_OnPan_m3FF3967608E363680E540F267D912CB3F9B3D640,
	UIProgressBar__ctor_m5095406C309F7394B74EA09DC60AE32226DC05D4,
	UISavedOption_get_key_m86E41D01D9DA6FDFFDD8A716BD9B7AA1117D8A8E,
	UISavedOption_Awake_m42E1758B75900FA1739C6CDE9101C39CB299909B,
	UISavedOption_OnEnable_m291C21B78688457C2F2167FFADFCC0A71B309BAA,
	UISavedOption_OnDisable_mC5C2E938995AF2B255A6B14E3EF4389B8833C80C,
	UISavedOption_SaveSelection_mC3811EBC90DD5F5BC495957FBE392CF6A8EBB254,
	UISavedOption_SaveState_mE6E14BAF5DDA5A6B2F702AD3282603DD43A87404,
	UISavedOption_SaveProgress_m458AF27605D934EF021ED68113FF0983AFDD0EC0,
	UISavedOption__ctor_mC20F94CB762AC3F25CF51939E8993CF344B89988,
	UIScrollBar_get_scrollValue_m314FD042F4A15212FE0F2764CB66118CA8858F42,
	UIScrollBar_set_scrollValue_mD0F3AA7F365EEC998F54DB0686155F540D48CC49,
	UIScrollBar_get_barSize_mA1108445228E3A20DB15C71CD893949A2C60681C,
	UIScrollBar_set_barSize_m81BF2B4DA3555EE652DC863FDB3D9565419C7160,
	UIScrollBar_Upgrade_m78A2C6461A6E06C66C745EDF2602CA3FFBE0D8BC,
	UIScrollBar_OnStart_m165344171BA9FB54C6A365B73724BE9CA335D49A,
	UIScrollBar_LocalToValue_m6F66172F63A5E014E022563EDCEC1CFB4C8C9A0E,
	UIScrollBar_ForceUpdate_m4C911A795F125F47004238B77FD2B62D52CF6F76,
	UIScrollBar__ctor_m0B9BFB5F18CF2A635C77A75B8E04409A14193311,
	UIScrollView_get_panel_mBEBFAC8C6485164F313407970A380BF988F6AF71,
	UIScrollView_get_isDragging_mA53417F352E0A0C81BD88E00CC12C89B3BD9E62C,
	UIScrollView_get_bounds_mBF6A9B72999F6F8755A1E70F84685AACD1A91A4B,
	UIScrollView_get_canMoveHorizontally_m2C9058155249173D5981DB2113C81E543D0EAF00,
	UIScrollView_get_canMoveVertically_mE5B7092487BBF99C0BDAEB88289CB25D6850F20F,
	UIScrollView_get_shouldMoveHorizontally_m47390D0E106B7DF080E957098C721C28AF0727F2,
	UIScrollView_get_shouldMoveVertically_m340321A638704FC5EAACD5D01AD589A2ED55E78E,
	UIScrollView_get_shouldMove_m6B19565989A0BD3CE3AB013E7B4683B02F09209A,
	UIScrollView_get_currentMomentum_m408526CA7F71FD0A1175BABBAE7A2D027C726814,
	UIScrollView_set_currentMomentum_mFC18218DB6BDCF4605E6933878478E061FC7AEF9,
	UIScrollView_Awake_m7295B914C25B194A0A4F81B60459D8F5C4885195,
	UIScrollView_OnEnable_mD20CE2B33343B4DD3D718ED106FE3C3CB8C9701E,
	UIScrollView_Start_m6EA7138BB60DB53D10664D744DAAEFC4DED0AFBC,
	UIScrollView_CheckScrollbars_m2A2816D362EFED9F8304B06011839B1E29E310F3,
	UIScrollView_OnDisable_m55F4556E05DDCA4CD85FB925F70AFC132E451437,
	UIScrollView_RestrictWithinBounds_m0B820840FDB7F16339785002BF3FE3293CAB1EA1,
	UIScrollView_RestrictWithinBounds_m5F2F17DDA262BBBAE4551CBF4E291A8CD2AC0D74,
	UIScrollView_DisableSpring_m6C872CDD237F74E822BD205DE738D527D665C4C1,
	UIScrollView_UpdateScrollbars_m29528D9AA53BBA21460CA1E5A00608578A2C6D7E,
	UIScrollView_UpdateScrollbars_m76A528016BC440FD86EBA9B691CEF7EB614229EB,
	UIScrollView_UpdateScrollbars_m04AEC8B3A773ACF98C99F692F6C4692A56436678,
	UIScrollView_SetDragAmount_m863417E515472947D7FDACD185F240776DF3B60B,
	UIScrollView_InvalidateBounds_m0C3454AD82F27FCA7EA897517783E18B29E85592,
	UIScrollView_ResetPosition_mAA9839171EAD3D4073F9EE00001A954B26EFAB45,
	UIScrollView_UpdatePosition_mBE3EBBC23F8BBED583A2B7AD6A0517BD0324C9C2,
	UIScrollView_OnScrollBar_m4165B96DF32D2787E7C2CEA6D63EE1356E86E205,
	UIScrollView_MoveRelative_mF335FACD5434833D15DD4157DAA0D9ABF011AB1A,
	UIScrollView_MoveAbsolute_m318FB6944EA70202DBE292CAD35CFC267789A152,
	UIScrollView_Press_m8FB3F2E26551E9BF55C04D853B4ACDFAACD2C420,
	UIScrollView_Drag_m4349CD8FAA0965D4E8619C24EED191E5C4D507F5,
	UIScrollView_Scroll_m9B842E6ACFD14B49CE867825367E617E59CB039B,
	UIScrollView_LateUpdate_m705488392BB76FEA2FFF325EBD955A13E37F4488,
	UIScrollView_OnPan_m04B62FC0B5D78764CE663AE26A5AF653D9362633,
	UIScrollView__ctor_m36E58295828F705ABAA790E4365944DEC3381769,
	UIScrollView__cctor_mF0B30A0767415B93986ED7F16E4119728C8C9DE9,
	UIShowControlScheme_OnEnable_m3ED41636221A84FDF524EBECD46ECFDD4106140B,
	UIShowControlScheme_OnDisable_m247356D75EE328CF8D921ADCD8655DAF5EF5E7DA,
	UIShowControlScheme_OnScheme_m0AB8F0C1C07977B9C58E6A4943F30B2DF6BF4626,
	UIShowControlScheme__ctor_m19EE086C803B61E3D1D757976EC40B7AD3ACC33C,
	UISlider_get_isColliderEnabled_mD8731E3D4F98178438824C8820C3019C951A6FA9,
	UISlider_get_sliderValue_mB3365FB79A545CF25FD1F8B33955E30079A31E89,
	UISlider_set_sliderValue_m0C87B150722DCD0E07234BE9694A87D0D630A16A,
	UISlider_get_inverted_mB32E4232541CF31CDDDA1B33F1ADED9675381D51,
	UISlider_set_inverted_mCE4E0EEEA1AE0A0E16B2D0005A6FA4EB295C8C93,
	UISlider_Upgrade_mC6E1E2E9EA43620412270889D8A3916B4D5D3B07,
	UISlider_OnStart_m0359CE5BDBE6E9C8A7CD00AE3029AC02D9D4B0B7,
	UISlider_OnPressBackground_mBBCE0234A1703305BAAA2E5CB01677BAA17E1E11,
	UISlider_OnDragBackground_m563C0088C6569CD409895E23EA3D51A1B501E163,
	UISlider_OnPressForeground_m22FA341E6DBB260B2D6B411E5125A03695AC189A,
	UISlider_OnDragForeground_m8E6BB58DF5DCA83DAF73D5FE7C7AB9CE8E6F46DF,
	UISlider_OnPan_mFD2A89310F736DA871B10F84A094BF9EEE81762A,
	UISlider__ctor_m75EB0A507F3981D2FC96B967180F5E65C3B9AEFC,
	UISoundVolume_Awake_m29698F4342445E9CE8E60AAF834EEB86698A6560,
	UISoundVolume_OnChange_mFCCD9F9801C3211995D7793CFF9097846F02D079,
	UISoundVolume__ctor_m9FCBCEA790666168C55B8F9CB076096476BDB15D,
	UITable_set_repositionNow_mD243457E53C1801B27B6E4B93118AD1BD5B0BA3B,
	UITable_GetChildList_m85DC97B2F1E3151DC052BEE30D5F886740507CA1,
	UITable_Sort_m8616A1E456FF09FA9D7BE78B31CB68629D91FA6B,
	UITable_Start_m09E81216A8E277230EC07AC9D9867FAADDBB3745,
	UITable_Init_m199C860085804BC483B4D4EE6865A1EA53152B7F,
	UITable_LateUpdate_mF505A9867E42BA7870A83B638D52E3A6A19CF22C,
	UITable_OnValidate_m34108C4BAE9EDF3FA70080C80B2000B46B8DB7F5,
	UITable_RepositionVariableSize_mDC876B3F4A8A34862EE6C9E8CF28EA1B0A1D9B75,
	UITable_Reposition_m2076F5DC73CDE16433A37E421E2ACE0901D5C9EF,
	UITable__ctor_mAA178AEC3F50F43FD42C6C02A0E709603CE60ABB,
	UIToggle_get_value_mCBCD4247DE67C2C61E1E75D7150D637D10CAB873,
	UIToggle_set_value_mBDCDF8D43B37CB5B05C26DC54E7A91FA96C7A18D,
	UIToggle_get_isColliderEnabled_mFE8F96770DB7C6118C81802CAA541ABB8DA7C9D9,
	UIToggle_get_isChecked_m45EC1A78C22706AA014CFE0C8CBA805BB5CC4889,
	UIToggle_set_isChecked_m938534C39FFD0BA8A19D7F78E77189E87AB514DE,
	UIToggle_GetActiveToggle_m9DF963C68A5D9DC21AA0A9381ED184F986148FEC,
	UIToggle_OnEnable_m413F9ABA8FB12D64A4718E1888B2878ED4BD228F,
	UIToggle_OnDisable_m089E1063FAFA02649309FCBF3D6660108526EB90,
	UIToggle_Start_mB1AD437D182C3709E0BBAEDAD89E05A2FFD71C9B,
	UIToggle_OnClick_m7840D881C17D9F90D4331AADCFD23ACE8B168C85,
	UIToggle_Set_mBDD54986DE8114C86E07C2F969F1EA272C1369D7,
	UIToggle__ctor_mFA6C18C6C693D7626E3D9411613DFA438990CD72,
	UIToggle__cctor_mD563A9197DA664347C7FE2F323891B308F9C7A45,
	UIToggledComponents_Awake_m5ADE134BEA8D7C1CCCDA5D09B5661E8FDBFC082B,
	UIToggledComponents_Toggle_m0D515DC5CF4E74528862199EBC10317EB43BF6AE,
	UIToggledComponents__ctor_m1ECF5B565594EC3369F95DF8D80D80C6C57029AB,
	UIToggledObjects_Awake_m21EF4F75E646C7ECD81CEC091058D7F5347C2279,
	UIToggledObjects_Toggle_m201BF3998B25E7D07CB8206002C63F4356903B63,
	UIToggledObjects_Set_mC721A1CD5FE868EC21E8C66293BDB27EE20A9C4F,
	UIToggledObjects__ctor_m1FFD830F1D0BBEF3899B86565999C8080AF9A584,
	UIWidgetContainer__ctor_mFF8C191910B00DCBBFA8127FE6F73E1290D3BB1F,
	UIWrapContent_Start_mFB10931CDAB8A64E3E2FCD275FDDA6A47CD98B56,
	UIWrapContent_OnMove_m005C3049D8133096F1C3E0DC5ABA597F5355FC31,
	UIWrapContent_SortBasedOnScrollMovement_m6C5ABDD7B3DC0B08FA051EC2EFA819D00F97A305,
	UIWrapContent_SortAlphabetically_m647C29FF33BBCF7108C0952D01D1F7159BB776E2,
	UIWrapContent_CacheScrollView_m01B46417F30D6B1563D532BA2A932291864D0F80,
	UIWrapContent_ResetChildPositions_m2C37A68BD98BCF688146FF4D4EFCA282D6D70E00,
	UIWrapContent_WrapContent_m87249CBB87BCC28A254D028A3B17C27D6C4DCD68,
	UIWrapContent_OnValidate_mA6F75556072FAE490A3865186CD5F949671D812D,
	UIWrapContent_UpdateItem_m248467386580B4928EAACB1F98882A626E5ADE00,
	UIWrapContent__ctor_mFEF5B56E9EA20E46D362AA341CCA578389381DA0,
	ActiveAnimation_get_playbackTime_mB4231908ABE26152EA9D648F1313DBCB53C13E96,
	ActiveAnimation_get_isPlaying_mA3D22F5B957AA018A280581357F596D32F8DD506,
	ActiveAnimation_Finish_mB13A2535FEED77E472B4D95678C6B47ECDE09BAF,
	ActiveAnimation_Reset_mDC610FE95826B0775C0A255BF549E2E1F91FFDFE,
	ActiveAnimation_Start_m3493D2687CCBE1F9231CF9A55FDB6EB4F337FBA3,
	ActiveAnimation_Update_m947AFA115B90BB5DB3AF6C8F197066654BD15AE1,
	ActiveAnimation_Play_m9BE80E95A87BA8B324B6C3CA7E7DB2EBBCE43A41,
	ActiveAnimation_Play_m00DF9A80DC52AA343ABE75DBF9B2C8F401A039DD,
	ActiveAnimation_Play_m6A32F3485CE5F30F1492952073D0E3361195661E,
	ActiveAnimation_Play_m800182CE5C1880480446B73EDA4F3E5A725EEE2F,
	ActiveAnimation_Play_m5C0D794AC0768BBD7E8095980F9C644A1F3021EB,
	ActiveAnimation__ctor_mD1D441A7E0F6609A51D3E77B10EB69040D339551,
	BMFont_get_isValid_m9B125D4290CAC19C7A6E64C98B0A8480637EB6FC,
	BMFont_get_charSize_mF94B61A5B60AE675C337C9CCF41C6D4AD1A72456,
	BMFont_set_charSize_m0ED5B6DF647142B61FD36FF6B72F37C7FD4D0629,
	BMFont_get_baseOffset_mDF77D32CEFC81A50C0672A980FB78B01AAD2990F,
	BMFont_set_baseOffset_mB2314B2EFB01BEE62B67F8B148C51A7670204648,
	BMFont_get_texWidth_mD603000A1F55EB7766122485C2BC111A383CFC78,
	BMFont_set_texWidth_mFB3A87272B5F4E911F5449393D1B5A3FEB4CA52A,
	BMFont_get_texHeight_mCA5D3F525A071793A5E0BFA94E0660590954C097,
	BMFont_set_texHeight_m210344B5B2CD973F6E5ECF2B45A1DE775213256C,
	BMFont_get_glyphCount_mD07E143B2F77735D3148E531704D08F047C9031C,
	BMFont_get_spriteName_m8CDC1C0D10819E65D1DB199F5CFE01D99C0BE84F,
	BMFont_set_spriteName_mE6749A3966F3370D11C8848D70C96B530D26EADA,
	BMFont_get_glyphs_m47ABD209BC853FB1D3426B9D99A35C4D50DC23BC,
	BMFont_GetGlyph_mA73323B40435EF19882139F4ED97117DA416810A,
	BMFont_GetGlyph_mB1937D47E81474AEEBF021A3029D38B8E2EF6899,
	BMFont_Clear_mB387DEA2628E59E201745D472541703840BB5DC8,
	BMFont_Trim_m3C5A6B7B92DA838E88584C0A978C01D9165DCBE6,
	BMFont__ctor_m928AD21F5469E72F562A908EA6A317AB0609F9D5,
	BMGlyph_GetKerning_mF08433129A11E73C01FD36B6645EEA69689390FF,
	BMGlyph_SetKerning_mEDC70ACF28AEFEBE627C759BF6C2DE951E81449E,
	BMGlyph_Trim_m0660D92C8F7128EA2039359C616C22CE6B73C3A6,
	BMGlyph__ctor_m05B7C263C9DA5E93FBCF30200A0155407471932D,
	BMSymbol_get_length_mF7E671E546D5ADDEF7BD1BAA3FEC7777927D77A0,
	BMSymbol_get_offsetX_m1EAC99E62D751D406391493BF91F103687E12ABA,
	BMSymbol_get_offsetY_mFFD914698FCEDAB5EFDF9932A5279ADE96E43F7E,
	BMSymbol_get_width_m2E6B707652F8A9C8BA488CC1C5C60BACC8F449BF,
	BMSymbol_get_height_m8FB6159133574894FC9D07A6BECC2F0AA2505E2E,
	BMSymbol_get_advance_m70CCC18605558A24565A705C275271F44729B665,
	BMSymbol_get_uvRect_mE008BD67DCFF3DC4FFB39958ABE71BFD185491CF,
	BMSymbol_MarkAsChanged_m904B4E50E1AE8651B0A23D04C09FB72FDFC14D14,
	BMSymbol_Validate_m3EAC3E8EF4077C4527F713F91297CB4FCF943DB9,
	BMSymbol__ctor_mEA6BAE427D60F27B404FBD1B4BB06680F49A5F06,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ByteReader__ctor_mC57C833BDDA43C8A3237E4B8551272512BCD52DE,
	ByteReader__ctor_mE7A78D898DD013A16CB2F7AAB5C483CF4814CDAD,
	ByteReader_Open_m09A1AB6FABAE5CC92F2C8C89F6C539B6A7BB5CEA,
	ByteReader_get_canRead_m7E9FDBCFA90FC4DECCEEFF44428AC5A3073F190F,
	ByteReader_ReadLine_m59CA3836BC437C38F2369F8E3A91BF7DF823ECAA,
	ByteReader_ReadLine_mEE4F1184E75A4B8CD5188F904ECB99BB539F84A0,
	ByteReader_ReadLine_m304B98DF8293705C51851056704CEF8777707897,
	ByteReader_ReadDictionary_m5BE7FEEB9A92EF7B6C1A29225BF62861AF56AA16,
	ByteReader_ReadCSV_mB5DE9807684878995C27E44EB45BC12AC11252E5,
	ByteReader__cctor_m56913D7334EECA5D144E16A177BC3F583E5E9230,
	EventDelegate_get_target_m64100D73B6BAA1EC13A76681D4654854A103A458,
	EventDelegate_set_target_m60EE1DC693FF5308E4EF29A809ABBE9A1AA6F02C,
	EventDelegate_get_methodName_mEC14CB3A6099BA9C66536F1CA47FB5F00C3D35FD,
	EventDelegate_set_methodName_m7A456BB24021946A5579BF57CC92D75AFDE879B2,
	EventDelegate_get_parameters_mA66E82D047579F2EC9E83FCAD9C40524F2A97D85,
	EventDelegate_get_isValid_m635C54235E25C5F2F5F74B0B141AD3F980697511,
	EventDelegate_get_isEnabled_mBDE1841EB2E68865A46A2FC90B83C71C6566078B,
	EventDelegate__ctor_m5ACD5C32F319DB0A1774A43ADD07793B06FF48D9,
	EventDelegate__ctor_m3C62112679619038F9B7358C8405E3DFA521A9CE,
	EventDelegate__ctor_m1741C4C5A485A545E665FEECB04CE9B4A90B1971,
	EventDelegate_GetMethodName_m0BB18FA0518A4F10622DE49BE30171EC319D3CF6,
	EventDelegate_IsValid_mFCD0EF3F90EC174DE6EE4D660DB7F7938FA4DDFC,
	EventDelegate_Equals_m40396381EDE962BDD8A1F50C35E06DADF0E79000,
	EventDelegate_GetHashCode_mD59E2B2A9A8997ED2112D5FAC2B6D6B628DB883C,
	EventDelegate_Set_mE7A976C05D1CA3D3DF1CE1F2176BDAB3BE37BF7E,
	EventDelegate_Set_mA3314D0C08AA946539C6A539C7F1EA027D080488,
	EventDelegate_Cache_m471C84DF5FEE39FAB8B235EAD46B1E7E59EA56BD,
	EventDelegate_Execute_m9E230204342FD3A857AF2E7B0266061E75FBB1A5,
	EventDelegate_Clear_mC23B80E3B455A8616D3A14ECE442409F4F16E5A9,
	EventDelegate_ToString_m8F383BE25754ED3769B71A0D0D0CCB1811C5F5CC,
	EventDelegate_Execute_m7B555B745DAEB6500E03634B1093007BD36776D8,
	EventDelegate_IsValid_m48BDBF4FD26762BFD48E11EC74E7E11CB036ED0B,
	EventDelegate_Set_m03EF9C4BE928E0423BD51A39EFBBF1B2D2BB0F72,
	EventDelegate_Set_mF1D2C876A89CAF69516BF8B77FFB1AAB8A0EE675,
	EventDelegate_Add_m05C3EC1B00F45DB2403A862632409B4963A4CE00,
	EventDelegate_Add_m1FBEFBED683C8C23190887D275D8C95859D75971,
	EventDelegate_Add_m1C5FC53DCB2B17970E30FDF0228DD0200557385E,
	EventDelegate_Add_mA778BB95D83429A65992CBBAD670EBB4B8E3674F,
	EventDelegate_Remove_mC6D065C13DDEE21813B986611B80ED58D9EE4E90,
	EventDelegate_Remove_m65EE562FAC029B4000B555EA359C7572E0F9A8A6,
	EventDelegate__cctor_mB40F0710B88A4BC1E033281AAC31A75660125D55,
	Localization_get_dictionary_mFF8C19565326BB07E2107AAA44BE57382626396F,
	Localization_set_dictionary_mE8F643C11684DB5EF671DB78E84131189050005B,
	Localization_get_knownLanguages_mE4590B5169C400B20AA8F0FC9A9C3149D9537595,
	Localization_get_language_mADBA19E8969CFECCF39053DCEDB525A11A525707,
	Localization_set_language_m1F5F1680D27449B5D01494CF6F8A14DFCE6C87B5,
	Localization_Reload_mB33B2338EC994F7D9D833D3A2AB74A61146662C0,
	Localization_LoadDictionary_mCF9CC76FF322B699B00029989AD865C01E4BBBD1,
	Localization_LoadAndSelect_m3EEC72108629346072BE2C71E8185BE528DA9FC9,
	Localization_Load_m03451F8EC3824FB903581DD4A2FC532267540DCF,
	Localization_Set_m8F52E1F48A73F9B1A6F29FC0CF102CFC0DADB3B7,
	Localization_ReplaceKey_m34296577488CF411A46BFC15809C8630DD1223A0,
	Localization_ClearReplacements_mDA3A10B66B078FAA00069C604F16301AD3BD2A3D,
	Localization_LoadCSV_m43DCD17C97D7895FFE4E6071E03E1518439A5F48,
	Localization_LoadCSV_mB3ACDFDABB0FBD2D29B3E3199D85B137F5A2CFFF,
	Localization_HasLanguage_m5F50B7C458ECCBC9DCFF17208A911B7A7E3D433F,
	Localization_LoadCSV_m4251DE7B38764BE812BD3DC6AC542DBFDFD05EC7,
	Localization_AddCSV_m977CC734F5FB3CB0DB69EE03E1504A66EC71DE69,
	Localization_ExtractStrings_mB11F2DA6BF373EAF434B425290032049299084A0,
	Localization_SelectLanguage_mE648D7AB86D49836218CCD747E01CFF17B650273,
	Localization_Set_mCC12AF0DA8490FD29D53D3DABB884C108425BCEA,
	Localization_Set_m17FB27B9C1CD4C77C7AAFB389E68C1DCA3C97F09,
	Localization_Has_m460661310031466BB615F2F992D5007AB753835E,
	Localization_Get_m71CC6E0E507E872895945D6B388F7FAC5024913E,
	Localization_Format_m49B9F233C30ED48145A93CA9A19099336AE8C3DE,
	Localization_Format_mC66AC7E9CB915F8ED574AC4F82F444B93A79D5B1,
	Localization_Format_mF6FB2474A569C8FB1BBCB6153C66DEF4177AFD08,
	Localization_Format_m5A368A8E2B17350ADB073BFC4D0B9B89A920EFD6,
	Localization_get_isActive_mD584542029D9FD1B0E79B606EF26F56DDADEC07A,
	Localization_Localize_mD3DCDD1267C199CFFB61426D19ADEBF3C8DE6198,
	Localization_Exists_m7B989D009B9976E6B461709265B68DD11CD64EA3,
	Localization_Set_mBE0687CFE601D8635670BCC21FE3D87693BCCDA2,
	Localization__cctor_m7C61BF1A06EAFAEC19C732AA4AAEC1A24EC10F4E,
	MinMaxRangeAttribute__ctor_m4822F2C1A15E7655CA6CAA98274FEFEA0AEF6BB1,
	NGUIDebug_get_debugRaycast_m7B6339838BA680A6F8FAE912CF8520FC0576B7F1,
	NGUIDebug_set_debugRaycast_m91F3BF5053C67912F320B253370858F65D29CA73,
	NGUIDebug_CreateInstance_m0763327AC4776C0A2A129B194FDA044968ADBF2E,
	NGUIDebug_LogString_m2B9C97B9C80339CE18357E45CCAE5C88C757407B,
	NGUIDebug_Log_m764C362ECAD787205A481F1F579F4636B4932C8F,
	NGUIDebug_Log_m2CFC6A766618564F72A605D59DE3F35D7F9B1EF8,
	NGUIDebug_Clear_m468D4B1B97B70BC83E2D61D1529D53681B6DF3E4,
	NGUIDebug_DrawBounds_m350A38CAEF75B00AB9B49DBF6FD064913A598B11,
	NGUIDebug_OnGUI_mCC04476B04DBE3B5E2CB51384F787D296B741338,
	NGUIDebug__ctor_m76E094AD4979720C242DC5430F8D1ACE55AED195,
	NGUIDebug__cctor_mA2137581359544F4A3EC5C04263B5B49A577D880,
	NGUIMath_Lerp_m2EED4A1074799416C2CFA8FDB87A237DDEAC467E,
	NGUIMath_ClampIndex_m7EE0BC19807698B1E5CBDC1F7B0EC3A19352499D,
	NGUIMath_RepeatIndex_m368D29F79815222AD1DFFEF08F0CD29AECB58B71,
	NGUIMath_WrapAngle_mBDB1BE35C813A501B2CF741A76DA6EC5F65EADCA,
	NGUIMath_Wrap01_mDFB9E75D7AC3EF27636A73B03F7520F6677DA5D2,
	NGUIMath_HexToDecimal_mBBC3AEEBF2A228355D182DC89ABFFB0907712979,
	NGUIMath_DecimalToHexChar_mE11A73B7E013C4C7DEAAA1B2CFF1F6FF173AB430,
	NGUIMath_DecimalToHex8_m75865AF6898DB774B134AC1F0A1F760E3F5E41D7,
	NGUIMath_DecimalToHex24_m0B7CF1ACE3A9EEDB133B8FDA7574ECE3D0247DA6,
	NGUIMath_DecimalToHex32_m7C2CCC1305527FA9F37B0D810AF70342324B46E6,
	NGUIMath_ColorToInt_m3CBDFF71F0BF42CF6F69D44632359EFD6A570F88,
	NGUIMath_IntToColor_mD7622360E472860D708B0C76F4BE05B7DA2FCEBC,
	NGUIMath_IntToBinary_m33DB0591E46D47B4FD2423489FB0D119061F467A,
	NGUIMath_HexToColor_mFEA0304A100B6077081374CB207FEAA0337F16D9,
	NGUIMath_ConvertToTexCoords_mAB40AFD9262379C4240BDBEC43330C012D0EBA51,
	NGUIMath_ConvertToPixels_m1EBD6AC96038346B0979E2D43947E0744E7F89A9,
	NGUIMath_MakePixelPerfect_m1E5D10A2F3EA9A4FB7E1A4774053D1C2EC5F3607,
	NGUIMath_MakePixelPerfect_m4CE089AFAFDF677309B131EB3824130479E43133,
	NGUIMath_ConstrainRect_mE4D2859AE2B58FA1CD60A59FEADB26D4FE7093F7,
	NGUIMath_CalculateAbsoluteWidgetBounds_m20840230FC0377284C55A966B1398F6396BC4036,
	NGUIMath_CalculateRelativeWidgetBounds_m6765D9DB4BA7AE48402A5599A9EB1C5BBCD88A2D,
	NGUIMath_CalculateRelativeWidgetBounds_mD1628188670A35267678BA4C7F7A9712C6D21229,
	NGUIMath_CalculateRelativeWidgetBounds_mECD250EF93DF225D203CB3CB915AFBDBDB65837B,
	NGUIMath_CalculateRelativeWidgetBounds_m3EFA53D7118C36252444564139163860A69D6BD2,
	NGUIMath_CalculateRelativeWidgetBounds_m208D60E5212CAD43A40A44A0A92A56D6CBFC1D84,
	NGUIMath_SpringDampen_m16C4F9D01E9FC95A74DFD4B09FED5BDDCFC42D5F,
	NGUIMath_SpringDampen_m0ADC6C40DE14969744C4BDD81677B5CE801787E8,
	NGUIMath_SpringLerp_mE6EA4341572FB93DBF7DC36000F5D36F44F34715,
	NGUIMath_SpringLerp_m437999C602267292344F929141FAC1DE756ABD57,
	NGUIMath_SpringLerp_mA5E775C9C8E24C83580FF72DC92B60CB6E3EBEA0,
	NGUIMath_SpringLerp_m129F1E552965EDFA853295EDB6D9DFB585CBFCF5,
	NGUIMath_SpringLerp_mCBD6397870CC8132AAE1ABA05A5429F639ABEF54,
	NGUIMath_RotateTowards_m4481031E35F471F0B7767C9643060CB22A4E46BA,
	NGUIMath_DistancePointToLineSegment_m625089FF3C4B943FADE141E7CAC417F1180966CB,
	NGUIMath_DistanceToRectangle_m030D10A1651544F2F132EFCEC60668EC786E2D5C,
	NGUIMath_DistanceToRectangle_m3F4215071D4CE8DCA1A33722E84233F8A5E24E88,
	NGUIMath_GetPivotOffset_m40AF14CF6F049D898A91B2656F815B7057C4C0CF,
	NGUIMath_GetPivot_m3668F84D0DA993DE33E3F5CB7D9C6B8D0CABE25C,
	NGUIMath_MoveWidget_m9EBC47ECB2BF2C5FA47377000B4134AFCA48E95A,
	NGUIMath_MoveRect_mBC4206AC00AE46EF8316D8946449255C19A00806,
	NGUIMath_ResizeWidget_m973F23A481B05C9AE3C8FD59989853463ACDAF28,
	NGUIMath_ResizeWidget_mE33235768C91781D6180C15F137387D4321F75C9,
	NGUIMath_AdjustWidget_mA32AB12D8F097F18115D1201C9024DD7F88CD40A,
	NGUIMath_AdjustWidget_mDDEB41C0D8A85130B54490D1D5D0C56D067052B4,
	NGUIMath_AdjustWidget_m4C912A48D2C859780C64394F55B2371DAB77F78A,
	NGUIMath_AdjustByDPI_mA03EDDDE3B31DC484054CD507C9D06CBE557E875,
	NGUIMath_ScreenToPixels_m8155245ED90B4FCB19EB0F00C7421501E9010F08,
	NGUIMath_ScreenToParentPixels_mFF790DBD884F1AE1117BBA81EF480D41A9EE2F69,
	NGUIMath_WorldToLocalPoint_m4C0D9B131E366654B1B851B7F72CCB6417756EEC,
	NGUIMath_OverlayPosition_m442BE422B85C6EB870FDC763D8E02FEF2BE1EF4B,
	NGUIMath_OverlayPosition_m70BEAA845E1B4F02F90A2EF30F87BDFC6B3C39D9,
	NGUIMath_OverlayPosition_m2DEBC646044C5B528A9542C9872685C9E00F0044,
	NGUIText_get_isDynamic_m780510753DE4882F7AF1C4DD2C3EC1E29A8494F2,
	NGUIText_Update_mE369A19305ED9342FB6E2A98152BE461452EBDE0,
	NGUIText_Update_m69C14C6289E33186C52365E0A44028CFDC6C21F6,
	NGUIText_Prepare_m9A9984E177BA6C03004ACD6F1E222CA983600C2E,
	NGUIText_GetSymbol_mD98969ECBC92C8B79B9EB310DBEB4FA624040C43,
	NGUIText_GetGlyphWidth_mD4416AEA2814E7329737102D83BE4FB69EC8AA1A,
	NGUIText_GetGlyph_m24C826AFAD5F0F682B4F95938C7298F74D68C0A8,
	NGUIText_ParseAlpha_m9B835C744BFC2DC351249E970DD36FF270F3C366,
	NGUIText_ParseColor_m225EC449AB5C5F1D7B30113B7531FB7AFA40B20A,
	NGUIText_ParseColor24_mF89976BF321124A8A1854565CCD44F00D920B7DE,
	NGUIText_ParseColor32_mB62FDEA11F9E679EA64317C3D9BBCBCC7C73F456,
	NGUIText_EncodeColor_m011A9601C09CC5FA3000CAA842CAEC7379627F20,
	NGUIText_EncodeColor_mF8C970A6835B583AAB973FAD7D6983F428A3BF2A,
	NGUIText_EncodeAlpha_mE25E4FC809E3A5A59B28C37F0A4D0F7A1B7E8D6B,
	NGUIText_EncodeColor24_m235CEBE72CE923B37AFEB5D2DE73AC8FFFEFEC02,
	NGUIText_EncodeColor32_m0FDD28904C23461D2C6B78585BBE0064500642C2,
	NGUIText_ParseSymbol_m0337AE49871B56C70921B9784958CE580DAD8944,
	NGUIText_IsHex_m470ADC792E8979C599D07E20F397A0FFE95D10F2,
	NGUIText_ParseSymbol_mCD0ED77878630C02B324B806DB3F8D44391A82DC,
	NGUIText_StripSymbols_mB13F3FCED52C5FC7C5205783327890DC868A2A67,
	NGUIText_Align_m3413DCE9025E2C1D8D019DA6D42C569F7FA30A51,
	NGUIText_GetExactCharacterIndex_m77BD5AF911D011DFBC83980E1510956A16053EDA,
	NGUIText_GetApproximateCharacterIndex_m0E3F211380066E85EC3E505E7FA902B0B935A242,
	NGUIText_IsSpace_mD2CEA5A154FF53C705FC8461953040FED79DFE31,
	NGUIText_EndLine_m9615733C96E90E4D4E2DBFAEBD9B7D0C8AD5F31D,
	NGUIText_ReplaceSpaceWithNewline_mEAD85D43D2DEEB91C7B1B654B22C896270C1E0D7,
	NGUIText_CalculatePrintedSize_mF99DB023DEA045B412D44A620263FE98BCFA8A1C,
	NGUIText_CalculateOffsetToFit_m62C351293EE347F5554A04614C67B15F3C3A81B5,
	NGUIText_GetEndOfLineThatFits_mFF6FA627178687096C2CABC37F15B205712C9476,
	NGUIText_WrapText_m1A0A9B4777FA039F301810ED5D9EF00211C5E73C,
	NGUIText_WrapText_m4AB7C61367E1EF8FAA4D74B02A5ABA18A7F3F9ED,
	NGUIText_Print_m371BBA726ACEAB5020C39F15B9046C883F0765D5,
	NGUIText_PrintApproximateCharacterPositions_m72278C40BC9DE14F86EA26C90DFA207386EE2FAA,
	NGUIText_PrintExactCharacterPositions_m21527F461DDFA756363BD8A763D71555F1CFBB3A,
	NGUIText_PrintCaretAndSelection_mFE0865718C9A689E7D263E134DA405C1B568A62D,
	NGUIText_ReplaceLink_mF63BC9A5B1A713444BB1572E026DD60CDA57EBA7,
	NGUIText_InsertHyperlink_mB791B0473D15DC9BB3B3D4EF9BA4E952CD811852,
	NGUIText_ReplaceLinks_m745CDD8F505B22369E0E6139A7799BD98EB4C7E7,
	NGUIText__cctor_mBD477BFB0F1905FCD16A32FACD183CD848BD76C8,
	DoNotObfuscateNGUI__ctor_m15746C13EEFFCFB36FDEA5050E96CDB075773B6E,
	NGUITools_get_soundVolume_m8B36EA3014465E4E4E68CEE829186908CF8897B2,
	NGUITools_set_soundVolume_m076BA16321F46BCB67D1A4C0A4E4C7EBAC87C42B,
	NGUITools_get_fileAccess_m6DB81B3644EA842EA31FDB23682FD2F774076A59,
	NGUITools_PlaySound_m13921F71D25334857BD4FC597C9D71FED6987952,
	NGUITools_PlaySound_mE505C2D1EF05FEDA1C0BF2A766B8CA44DF9362B5,
	NGUITools_PlaySound_m627B89CD6021BDC4EB52FACDC2C43193A0B8FF82,
	NGUITools_RandomRange_m8CDD0EF7BF81569C2CD5DA5FDF3C7865E09E62C9,
	NGUITools_GetHierarchy_m83DE7FB3C71FE4688CFB53A75853B5CDF022E1CA,
	NULL,
	NGUITools_FindCameraForLayer_m330D0A272025D6790CCF61B63D30F84768B0C8A2,
	NGUITools_AddWidgetCollider_m8CFBB0F5268602E760059475FD4EE58E566647EC,
	NGUITools_AddWidgetCollider_m4EC12D8F558528F2361C9FD32C3CDE3F34B6A8E4,
	NGUITools_UpdateWidgetCollider_m11B3A31EFF911C0F2AF4A744AD5608673677B6CD,
	NGUITools_UpdateWidgetCollider_m98F16A789D99224D2902A62D958764FC6050090B,
	NGUITools_UpdateWidgetCollider_mC1D53041E75E8A81684B7B1CDEAD63C44D9F5458,
	NGUITools_UpdateWidgetCollider_m5FF73B8CCAC3885C2BF00204537E8662DB4F9D04,
	NGUITools_UpdateWidgetCollider_m59F646FD254F98ED72AC09DF16AF099FB22C9C31,
	NGUITools_UpdateWidgetCollider_m58BD1B135748D9B205DD695BB5350F7C829EAA27,
	NGUITools_UpdateWidgetCollider_m62D63D54DDCDF13D5B909C3EA5D7C36851BB95D5,
	NULL,
	NGUITools_GetTypeName_m27C25D4702E642A45D511F83DF697242E016BA0F,
	NGUITools_RegisterUndo_mC3CBCB74073640D5DED8EBA9C6FF65BA74F910DC,
	NGUITools_SetDirty_m56E3BE49DE56664FC27CF194F3F98D1445DDDAC1,
	NGUITools_CheckForPrefabStage_m52DAAC25CCE8C06D556C58318A55B2225B4FD438,
	NGUITools_AddChild_m26744EB18FD0C1EA3EB556D84BF95EB91F180025,
	NGUITools_AddChild_m6AA072199772F2A8B24D3F144B35EF9EEF0D96B3,
	NGUITools_AddChild_m33281A355D65241E84D9C21B0D3C7319C0E0D8B0,
	NGUITools_AddChild_mEA942D7347A48418B17EEFF287C0E4B97180EA2F,
	NGUITools_AddChild_m855D0A43C7FBB3A2F4773AF69C173908D89993CF,
	NGUITools_AddChild_mED3563EE99363B2C1F37B1D3BB0F97C5AA5D8506,
	NGUITools_AddChild_m02DAED8952ADFD1606A58555E5850FED5CC3FB39,
	NGUITools_CalculateRaycastDepth_mF86576E100A96A60CD282825FF79360AF500175E,
	NGUITools_CalculateNextDepth_m8D47CF3FFE33AC951500D06C76066D7FE3C47D9A,
	NGUITools_CalculateNextDepth_m8ADB7C3AE7CE74C9171119BB7E613DEA2DE21139,
	NGUITools_AdjustDepth_m1C7C44D01816FFD49D27E76D509551EB9A54B428,
	NGUITools_BringForward_mDB83861BC0E52402DCC774B775107776A662C276,
	NGUITools_PushBack_mE9224A3B30F72C6F60562FEB23AB39AE991011CE,
	NGUITools_NormalizeDepths_m38FA489E6B23C5AEB3C0AF6EA2F5F153719D16E2,
	NGUITools_NormalizeWidgetDepths_m9F11D11EDAE57043D00C91DAA9FDE30D8978DDBA,
	NGUITools_NormalizeWidgetDepths_m0C5D5D0DF42673310C7079D727D25D0FE6D8C1F5,
	NGUITools_NormalizeWidgetDepths_m1BF8512C755F24A225EFC67AFFD26BBD858677E7,
	NGUITools_NormalizePanelDepths_mD531F1736A693D4555E7642F67E82136FEC15EFC,
	NGUITools_CreateUI_m557D90666C2859E14000D4E1450885002EF57175,
	NGUITools_CreateUI_m9392D044FA8440EABE8DD0188A9838E5FE9506A9,
	NGUITools_CreateUI_m025B5545FF7F78C94B786048237882158429B504,
	NGUITools_SetChildLayer_mBC9096C1694BB3615E3A65E4EA78B294CFDCB1E4,
	NULL,
	NULL,
	NULL,
	NGUITools_AddSprite_m06A7AFEFD7B4DA8E0A0C9A408FA444E4FC761E9B,
	NGUITools_GetRoot_m16F9BE91AD3851F2BD4BF9455AABA0DF63B9FDE1,
	NULL,
	NULL,
	NGUITools_Destroy_mD9ADA30042B1708E977F9F0B45359CD4943DA9C2,
	NGUITools_DestroyChildren_m61517522CC6B7540469A83EB86896622085AD3E9,
	NGUITools_DestroyImmediate_mD8B1758EBC7454307B05B76887BCFBC68445F111,
	NGUITools_Broadcast_mEC242003D0D28A2EB9C1A65C0494AE72ED0ED0AE,
	NGUITools_Broadcast_mFE37CF943011EDEADED5F9F25867EB8339E48EC3,
	NGUITools_IsChild_m71A1365BB9A0C8BBB3E849ED3A187A6F5337C9BA,
	NGUITools_Activate_m2BBEB6AA413580B3F092989BE5D28CB2F7827E7D,
	NGUITools_Activate_mAE71BEA8E563A110B4433A660B259FFD94498B3D,
	NGUITools_Deactivate_mC619B19A8FEE6D9A26F867C23C513A551DF1FBD3,
	NGUITools_SetActive_mA3F5F2C7B320D7C7A3C6C79E01BA07FD247B5C00,
	NGUITools_SetActive_m7628B941BF60C630DD3CBB324E0C001F836956C6,
	NGUITools_CallCreatePanel_m52EC705DB98219A5642E07F5F4CF5A983CE71FBD,
	NGUITools_SetActiveChildren_m7F43096CF750662B768300E6B14EB6E5542E39BE,
	NGUITools_IsActive_m1BAD84A628550FA6DC419B19DE684F5567E55EA7,
	NGUITools_GetActive_mA792F8D7A43B84E538D372B34D0C571F1DB3CD5A,
	NGUITools_GetActive_m9BDF3B08048AA4374F9090241978364AEADACD65,
	NGUITools_SetActiveSelf_m741DCDD7D59423BFF92BD245F6D716D9F624350B,
	NGUITools_SetLayer_mF54601184A83316756227AB6425A3D3AE50FA464,
	NGUITools_Round_mFEC5681CFF4269FA8B66E411CB4BECA0166D311D,
	NGUITools_MakePixelPerfect_m11A3ADA948631461B38871B31A36BF4729E58D62,
	NGUITools_FitOnScreen_m2FA418A87C8BE7CD77A0168C80819D5E7DF235DA,
	NGUITools_FitOnScreen_mF5B07F2E6B28B5D3EDB276ADA1D9A56DEBFC7FA8,
	NGUITools_FitOnScreen_m6E90A47BE5D9A5ADE89A95C29C69970201DFA830,
	NGUITools_FitOnScreen_m982791D2D71449AF3103DA332C7D45CBF5440BAA,
	NGUITools_Save_m75EBF3CAC8A64AB8796CD89F43535554868DA7D4,
	NGUITools_Load_m7A4FB722B3A07A81D586F37C183FAB625C133903,
	NGUITools_ApplyPMA_m4BCE7D3EF3CD4E3043DE84FF6FC71E0CDF1C030B,
	NGUITools_MarkParentAsChanged_mECD9DF8737E79B0E8D7E537260449C8B1D66BADE,
	NGUITools_get_clipboard_m6B0886D12FF4D2774B8D78BC510FEFC8C3EA146C,
	NGUITools_set_clipboard_mB52C5201B1ECF57BA90D98640C3A871F811F2BEF,
	NGUITools_EncodeColor_m36EFE9140D9FED4F66D48259AFBD6917E1F9ED60,
	NGUITools_ParseColor_m39F32594C7972D9666F2D1709D8FB285A0C0AF94,
	NGUITools_StripSymbols_m4DE0071CCDFC1D4E1B945ACD6E918EAB4E03DC13,
	NULL,
	NGUITools_GetSides_m8508A7DFF97136776B53C046E4BB80BD7E2CEA88,
	NGUITools_GetSides_mC7957AE9AAB137D2615EC5F7B7D966969A667F1D,
	NGUITools_GetSides_m1059978E86338E089E9C9CD8769B6456ADF267CB,
	NGUITools_GetSides_m4B27189ECD06B8CBB53C5F7087DA9BC6865C300D,
	NGUITools_GetWorldCorners_m536DC3D351DE13E1F6CF1D5CD1C89210A547FA64,
	NGUITools_GetWorldCorners_m3AE6A529ED232232219CA0C3CC49E7C5FF9B93DE,
	NGUITools_GetWorldCorners_m6D62820674215DE638728FA89E8024F9EEE5CEB9,
	NGUITools_GetWorldCorners_mA4113611BDF3B6D9D6E2B94856936D837ADA306F,
	NGUITools_GetFuncName_m73534015C7EACB5B30CED8B992A4074ACC860AA2,
	NULL,
	NULL,
	NGUITools_ImmediatelyCreateDrawCalls_mFC2097BDDA3ED4DF432DAA0D633814798F43E334,
	NGUITools_get_screenSize_m26698F4FE12B2DD3E4E9AB0D001AAE4FDC0E4546,
	NGUITools_KeyToCaption_mFD6B276F406358A10EE24A3788D867664BEEC9BF,
	NGUITools_CaptionToKey_mC69B30C9D024DBDC91DE7B3D551C6F397CD6D283,
	NULL,
	NGUITools_GammaToLinearSpace_m47F873E42A757A1859A17FFB9B6BEA240CCA2665,
	NGUITools_LinearToGammaSpace_mC4B2E07A841F7EB74CAD023E14C398361FE7A6E6,
	NGUITools_CheckIfRelated_m270D412CCEE6F414425078202656693CE8220C01,
	NGUITools_Replace_m237FCE492C5F2B818791838AC7A7C9BF7A472F36,
	NGUITools_CheckIfRelated_m103205B040C5B8B1B0087C7CE47A9E037BF381B4,
	NGUITools__cctor_mAE14E665B28058BAB815945C5D3ED76F1F49C273,
	PropertyBinding_Start_mD9CAFC7B705BA1A90F345AA187704520041AB18F,
	PropertyBinding_Update_mDDA363BFD1AF3F91890B6E6C0F7C10E5CC737132,
	PropertyBinding_LateUpdate_mC8953375B82664BCE207383C2C093B93EDC17545,
	PropertyBinding_FixedUpdate_m4952127DC536922ED7B7AC0303684531BB924013,
	PropertyBinding_OnValidate_mA902B30FC154921451C3E5F3E1222861FAF3FDD7,
	PropertyBinding_UpdateTarget_m0528CCC45E246CE2E0FA48679BF34D38FFB0453E,
	PropertyBinding__ctor_m452E88CBCA0CFB1B74555630B6999DBE7C36269F,
	PropertyReference_get_target_m26C1C45FDE1517E75A056198FDA5DC8A337B7498,
	PropertyReference_set_target_mD9CD8E816FA2D2A0350D4585EACCF770CAB8EDA1,
	PropertyReference_get_name_m6983C9F764BCEE796D25A87E266AB45FAF12412D,
	PropertyReference_set_name_m4ECCACF8292D1D2D65959195EEECB6D580456C21,
	PropertyReference_get_isValid_mBA99CC4CFAFD4234F036CE1FC92F1AE45C75C5F6,
	PropertyReference_get_isEnabled_mBDEFB1622052F55C31AC2273979DE95CA7EDEC51,
	PropertyReference__ctor_mA7100BA4D5EAB83995BD81DE418C3D99E4C7C3FE,
	PropertyReference__ctor_m1FC7E50E91AF0F2873D95F6F4FA5BBF5B6A9BB09,
	PropertyReference_GetPropertyType_mDF09D3B305E4F9C534E5452458431AA75F513931,
	PropertyReference_Equals_m31CA1608DF03D4ACA040D2798BC65C4C4B6E9321,
	PropertyReference_GetHashCode_m616DA1A03A461503716871C3C84DB62DC8F0D652,
	PropertyReference_Set_mC00BAA968F4E8265C8704DD4997366DE06DF993F,
	PropertyReference_Clear_m6D321CD3E4D4A94D994C0D93FB84A127C7297107,
	PropertyReference_Reset_m74D4A8794AEE517BF8B1D8ECD873DFAE20728C6E,
	PropertyReference_ToString_m26483E1A4A796010E8AB723164F55A7C44EDFD8E,
	PropertyReference_ToString_m3251091C9B11F87155479E0325E6A654CE0C321B,
	PropertyReference_Get_mD1097CB119895113A378F62B9E954B0105FFFA52,
	PropertyReference_Set_m1B70D84FFDCDE18FDDD8A341CDC3F079F290C9FB,
	PropertyReference_Cache_m52615000874052288578C880582A67E269410C28,
	PropertyReference_Convert_m400EFA3A3B8F92CE77336BC77A401ED6ACB525EB,
	PropertyReference_Convert_m37199A7C52CE10DA9E040CCC666D5DC58139D415,
	PropertyReference_Convert_m5CE6E53BE8A8DDFED08ECAD0C32969CE3FC5894F,
	PropertyReference_Convert_m46315B2118CB3E8D121398E9DA379E5F0563CB6E,
	PropertyReference__cctor_m9596D23D59E4F76F63B84CC19A28D4D6567587FB,
	RealTime_get_time_mEEC08DDB8D7C7059920471D5749243D9F7F5008B,
	RealTime_get_deltaTime_m83F26A5D9973F1E8C0189D36D2C5E7F0754B0B14,
	RealTime__ctor_m92A02D01407864F2F503A8E3FA0A20F1B0941742,
	SpringPanel_Start_m90CDC1BFAA83C929E62A5D0060A427B58232AC58,
	SpringPanel_Update_mBC20B42931484D676163943C799358BCA829A4FA,
	SpringPanel_AdvanceTowardsPosition_mDE7937BE1037B29C9DF0A86BC57C60B844E9617B,
	SpringPanel_Begin_mA1D1CD392CDA4DE2E470EA5BE63771F096500B75,
	SpringPanel_Stop_mB4F476CCCDCACB21E2B3FC16DA507C3846FACB75,
	SpringPanel__ctor_m8224B71A7D7AB90511A98AD3C2E99709C2611CC9,
	UIBasicSprite_get_type_m29FF9568B93BD441C5FC949264A9BFC778DEA70A,
	UIBasicSprite_set_type_mDB4F7080CB30FEAF7342BC95F60E65539FB44CDA,
	UIBasicSprite_get_flip_mE9DE042BB3A55E603713133D04CAC4BB77B39843,
	UIBasicSprite_set_flip_mF98BFB561338B5C8BBE5466663E7C4DF12C3D354,
	UIBasicSprite_get_fillDirection_mF9E1721DC32A7D9D5C5170FFD03FB118181F3723,
	UIBasicSprite_set_fillDirection_m5CC00536CC90D9444DEBF72B8995006653585B30,
	UIBasicSprite_get_fillAmount_m5FB3F2797728A61FF6322BFC603F333EF8818CA8,
	UIBasicSprite_set_fillAmount_m16AB3631F6082CCBC958EF87DF2571B277ED171E,
	UIBasicSprite_get_minWidth_m6E399B2CAB1B3EDBF694FC3F51EDB28B808B7187,
	UIBasicSprite_get_minHeight_mB1553A5B58A580BB6711DF659D4194358B6B7687,
	UIBasicSprite_get_invert_mBDE341041B97AC361C9363AC8899B4B79B595D38,
	UIBasicSprite_set_invert_m4B20059B094D00FD55CE0334D18B8A61BEB1FEFF,
	UIBasicSprite_get_hasBorder_mFEF250B1396D0DDAB3DBE96F58FEF92B1D92104D,
	UIBasicSprite_get_premultipliedAlpha_m51A1BD83502BF223EEA38E5D0169317795B17EA7,
	UIBasicSprite_get_pixelSize_m217448D733B5487985117AB9DE59AA94BBAACBFE,
	UIBasicSprite_get_padding_mE1DEFC28DE35CBC34C9C93ABB89AF9390D10D209,
	UIBasicSprite_get_drawingUVs_m49F26ED67F674D1EF77B8977C4A53CDFE28CCA1D,
	UIBasicSprite_get_drawingColor_m5941D60718207F3B5DFE460B0A3F19FAB7019C88,
	UIBasicSprite_Fill_mC8DEC548E594A6F0D455AED31585DCB715612B11,
	UIBasicSprite_SimpleFill_m9BE49C3B027F4340581BA2B90F6A52C903015507,
	UIBasicSprite_SlicedFill_mA46200B0DB756115D0A21BE55F995E983F20E79D,
	UIBasicSprite_AddVertexColours_m3EBB3AD76F05573B102240F5C6C7713FA997FA89,
	UIBasicSprite_TiledFill_m603771E6CE6A189AA0CFB6DAF7491604F4A79D17,
	UIBasicSprite_FilledFill_m0E56B32B5B1FDC93E5E3F80AFD99E6415A3BD213,
	UIBasicSprite_AdvancedFill_m8901C983EE70ECC99EF7AF15EED06C0CB0BC228E,
	UIBasicSprite_RadialCut_m0FDE6060DC5D747FE0532B5A1916EB9163F047EF,
	UIBasicSprite_RadialCut_mD90282CEDF1D3243D8BB53DE8E3E603529E2FBF7,
	UIBasicSprite_Fill_mC02E2AB202D36EF962CCEF28E60D4D1FB5AAA9C8,
	UIBasicSprite__ctor_m9F4D2EEBC2F7106D7E563855FAB1061822C0040C,
	UIBasicSprite__cctor_mE7682C9273E48A8442D54244E686B7933FB9931B,
	UIDrawCall_get_list_mAA51D26856381B325FFF209F53148B7EDA639A57,
	UIDrawCall_get_activeList_m84068B1AD19D00D0BD5CA87AE5B40D2797030748,
	UIDrawCall_get_inactiveList_mB920CB654104145871090A5FE724C26DD2448B94,
	UIDrawCall_get_renderQueue_m87C110B9DFD0D4DE598B269DBDC1D295EFACF5D8,
	UIDrawCall_set_renderQueue_m618A6DC9DAB0DDEF635D4881C45DB649B66F0118,
	UIDrawCall_get_sortingOrder_m171E0DD0EF311883D70C7B08C2EDE05979801F44,
	UIDrawCall_set_sortingOrder_m665E9519E643571503E54005B5BD06E720CD6132,
	UIDrawCall_get_sortingLayerName_m59938A4F120A40160AAB499FCD02A79E72BAE7DE,
	UIDrawCall_set_sortingLayerName_m31F3738F9F0B96E082C5F1C7FB82F3AA26742756,
	UIDrawCall_get_finalRenderQueue_mA77D4F632EBDD2E0361FF964008767262520BF55,
	UIDrawCall_get_cachedTransform_m1A4BC491540E297E21D2A7A127D9F31EC66AE41F,
	UIDrawCall_get_baseMaterial_mA6F9A0CE878C29E25FE7AE96B5E263B57CCE7448,
	UIDrawCall_set_baseMaterial_m9E6F8EF6355777A5D1EC2E8442A72D6B9E18B13C,
	UIDrawCall_get_dynamicMaterial_m1AA012B8409EEB8FBDCD5328ABCBB71BA8717B24,
	UIDrawCall_get_mainTexture_mDE560C837E90BE4FCA2383CA09191F4F393BD377,
	UIDrawCall_set_mainTexture_m2D33224D302AFD8ABEFE0A7F454BFAB68C135A02,
	UIDrawCall_get_shader_m84C71254C08A93B962BFC1575E5880FEA4142738,
	UIDrawCall_set_shader_m81AF5264D64624CB9C00E866E80BEA4A761CE172,
	UIDrawCall_get_shadowMode_m39B09F5E737ADE903FB8AEB78823BCBB755381F0,
	UIDrawCall_set_shadowMode_mDD7676C2C05532D1B4202049CA4B7A83C3840ED3,
	UIDrawCall_get_triangles_m33965C4D2F6A02615D35C415B913F4D35B49A28E,
	UIDrawCall_get_isClipped_mCFC48B4E6954BBD9488FF5B0B38AAFBCD2DF2368,
	UIDrawCall_CreateMaterial_m8435FC9185576B2B01F9F71128CCDFF4BC147F27,
	UIDrawCall_RebuildMaterial_mD551DFCA8EA4F634C21FD500FCFF4803FD17034C,
	UIDrawCall_UpdateMaterials_mA72BDDC781FD8D97333ABCC8CD3E8DD0281602EA,
	UIDrawCall_UpdateGeometry_m12702B26C210365F07AE3EDE314F4BC7A338CC81,
	UIDrawCall_GenerateCachedIndexBuffer_m211BFF47762288D80F1BD0D0C45F8BBFC2507C6A,
	UIDrawCall_OnWillRenderObject_mD53A340502BFA1C2A8351C1630D6FF65D39C4613,
	UIDrawCall_SetClipping_m8BC425B59C1CC1115FF9365ED9A61460270C8198,
	UIDrawCall_Awake_mEB28B18B6CCA80682ABA33421D69A9D42ACA6A3C,
	UIDrawCall_OnEnable_mDD12737CB69291920FE0D0B9FC42D62DD826ED83,
	UIDrawCall_OnDisable_m514E2D2FA74D43DC2D6651C8D0AF042C350EB7B3,
	UIDrawCall_OnDestroy_mF16581CEFBD428187F0BB38134C539D39268AA5C,
	UIDrawCall_Create_mE4871F283BB145EDC6ACAED444FB57E7061E13AE,
	UIDrawCall_Create_mADFA7B0C975A6D38765179456F4E9A022CDE996B,
	UIDrawCall_Create_mD1AA7F6884E16952448094F6D81E43A3F1CAC031,
	UIDrawCall_ClearAll_mF0DE9E526F92E66EBF483FA0F6CCF8B3B8EAC78E,
	UIDrawCall_ReleaseAll_mDD5E0D328A025E6FB9F56B08DCE4883A3748F198,
	UIDrawCall_ReleaseInactive_mA40CEAAB0610335980ECE349405AAA5A47ABEA0C,
	UIDrawCall_Count_m46C70D0447EA1E62433F5E37E54E6C7E0B0C4B65,
	UIDrawCall_Destroy_mCD892DCFA60C02A6CD743A2187A07FFDBBC5E4A5,
	UIDrawCall_MoveToScene_m29A229ED9AE12CCFD12B3CF834FB0A74D87A99FB,
	UIDrawCall__ctor_m6BC9991506E910F83FB56F3BA6F8CD64F75B086B,
	UIDrawCall__cctor_mDF5CBC8C3BC96B51BBB50CF19705209A817EE218,
	UIEventListener_get_isColliderEnabled_m58C64C6ED90803135E0094A41B4B66B01A680D1B,
	UIEventListener_OnSubmit_m42D15B693FFB67630A32B1198B8BFCCB0C1FC989,
	UIEventListener_OnClick_mCFC2FFDFC520B0A538142A557616C23412AD6A3E,
	UIEventListener_OnDoubleClick_mC68B1193B99A86DC45102DB3FB2065753DAEDF08,
	UIEventListener_OnHover_mB2874768BFA67CF168DAE249A789E5CBD3C52983,
	UIEventListener_OnPress_m666E9E985F4B661E673C54D310F3EA3D6BBDF1F2,
	UIEventListener_OnSelect_mBDBCFFC0704E34FC5675E71AD84B34B547D7DDD7,
	UIEventListener_OnScroll_m51EDED0E28A387501E3F19BF9F382A124777F59E,
	UIEventListener_OnDragStart_mB51C5C3C75BF6D2D81180E28795170F6E0D06638,
	UIEventListener_OnDrag_m9B3B91DB98AB2038B4E8A2E0CD3B86EDB0F9000D,
	UIEventListener_OnDragOver_m3C587416B2D8281A2D0DBAF2200B10252E35398E,
	UIEventListener_OnDragOut_m2CC8C0E96FCAB4AA01BE2FA733EC82909455EF5B,
	UIEventListener_OnDragEnd_m7E6ACACA000DBE210B55ABD2D20AC775F8E6E84A,
	UIEventListener_OnDrop_m59664431C43B6B26C30ACE5C34BFF918A8EC722E,
	UIEventListener_OnKey_m20C14FCCD294FD737FE644058F075160B5E4436B,
	UIEventListener_OnTooltip_mE4787CE6622861C67B3302EFBD045E6A7B0E6802,
	UIEventListener_Clear_m2C1156719ED590E606A60AFC6D7AB1C9C27BBDEC,
	UIEventListener_Get_m71055B6D20CD09BC66FE2E355EB299092998CA41,
	UIEventListener__ctor_m88FE9E7BC2488312D114EB82FE1B52C03C26ACB4,
	UIGeometry_get_hasVertices_m722414C27996059EEA1374D02F12F18A26C5E4E4,
	UIGeometry_get_hasTransformed_m12119A5B276829468B262A3B3A486CB201C2E36D,
	UIGeometry_Clear_mDB258FE13320B70040D95AA4D1B9B466825F227A,
	UIGeometry_ApplyTransform_m71DAA81EA50FB9C3F5276521CA924B61968A0732,
	UIGeometry_WriteToBuffers_m667B28DC786D12C544A9209212A13802529FC3F8,
	UIGeometry__ctor_m3945EB3D2B37C74A5A1FEC4C992EDCC2E4EA37D1,
	UIRect_get_cachedGameObject_m1A7B6A2EC0DE8F70AE5CC11C935AF89EC8E33F6D,
	UIRect_get_cachedTransform_m8A41449F84B2729D818288E33C95E4C145625BD5,
	UIRect_get_anchorCamera_m907B5D2030FF5D3E3075B47CC27D99B027C86F3F,
	UIRect_get_isFullyAnchored_m5A0B7C115AB49AF0E8C297BCCAD39A43B14A24B3,
	UIRect_get_isAnchoredHorizontally_m6B309D3B834CE8F0F0E82151E9E15F70666661EE,
	UIRect_get_isAnchoredVertically_mBF3BB773CED851B99789C9220B8182E991016B88,
	UIRect_get_canBeAnchored_m4AC7995E9246046AFB100FA0195EDC011D4B5CE0,
	UIRect_get_parent_mCEA314C6C528BC8A24338F2BBACCB141A39435B7,
	UIRect_get_root_mF478C1D6F5C7307F88E612355586336FAFB7AE3C,
	UIRect_get_isAnchored_mC1C3AD3C5FCE76CAFB729C22D96EAC8F024E2FF3,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	UIRect_get_cameraRayDistance_m9A48AA8DDF69E9D2D866A6175F3C77B0C7911547,
	UIRect_Invalidate_m02C0FADEAE441B0595154F013CCCC1CDDB08AB12,
	UIRect_GetSides_m27B77E7183E2A4CF39938B7EA3D53E89202FC7BD,
	UIRect_GetLocalPos_mF222005B8D4547B2EFB11A92BE8479BA2D863416,
	UIRect_OnEnable_m399CFA9D0D4BCA82B3ABF836B61E4F0A6A0D9DE2,
	UIRect_OnInit_mC7B43D2B22C2DB6084C04FC1C98C1FD18854A3A1,
	UIRect_OnDisable_m748451148A210DAFC88100CE263932B84AA3AFED,
	UIRect_Awake_mCF2BF0CF81FACEDC52245B9FC4B5A17A9C384D13,
	UIRect_Start_m4AA2087E77AFFD4D5E083F831778C75715DDFE40,
	UIRect_Update_m95C76C26D889B788CD71B50C85072F59F5CA1D93,
	UIRect_UpdateAnchorsInternal_m38944C5493670E57282865776914A972C2840E96,
	UIRect_UpdateAnchors_m3830127DBCC659E78D9BA8E7C86B310ACEB2EB82,
	NULL,
	UIRect_SetAnchor_m991813CC6FB904412E1E025ABD47F77AE822A58E,
	UIRect_SetAnchor_m5B66336950CE93D3B834CED30DDCFD429901D0AE,
	UIRect_SetAnchor_m3C2C4507D2EE7A5E8E844B9968CBA90D1862861F,
	UIRect_SetAnchor_m240D5B0AA7416BAF2983329A1D1DED99067D30F4,
	UIRect_SetAnchor_m10AD9DD59F4A2AAA9ED4C1A39518E7EAB8CF9C43,
	UIRect_SetAnchor_mF494D6010E6C4FF436BBBDFDB423557F68AD48CA,
	UIRect_SetScreenRect_m23FB016991B65C0533AC7BFE69E34947E25F10FD,
	UIRect_ResetAnchors_m1F700DEA18803E60FBDC0B04F63C3BBD3F3D171B,
	UIRect_ResetAndUpdateAnchors_m84C47AEFBC9D8300E87B61CFB5D2EBBF5CAD8253,
	NULL,
	UIRect_FindCameraFor_mC174180C24476849A38A17E80DD2A78A9CB83182,
	UIRect_ParentHasChanged_m763B6930F47DD0636A5BF71B669A573CFA44258A,
	NULL,
	UIRect_OnUpdate_m1BD123C0A9340F341367552436003C2989BF3A06,
	UIRect__ctor_m02F3ECDE7B0D3FA4C2B57C1FCC54DB394E7CA242,
	UIRect__cctor_m02BA4169A62737040D35ADBDF92DBC52A412AEE0,
	UISnapshotPoint_Start_m311469C05ECA65FE7AB2D81655EC8544D8C23A0C,
	UISnapshotPoint__ctor_m93EEB0F30696CFA341AD25C1C73F777C7365DDA2,
	UIWidget_get_onRender_mBDA41A911F499DE240C2DD41C93FC1E3D75D9EA9,
	UIWidget_set_onRender_m852C95058BF0FEC623103E833C730589D0C750E5,
	UIWidget_get_drawRegion_m9BB02BDEFE1D64D18F40D34A003345B7B474A1BD,
	UIWidget_set_drawRegion_m436D5C91934F94B4A97311EC9A6A94CE0BE4F5C6,
	UIWidget_get_pivotOffset_m283BBEBB36F14E121D6ABDD43073FC6D5534DF8E,
	UIWidget_get_width_m501FB5DA7B41280C91F52126ABB5EB74E85C3489,
	UIWidget_set_width_mE85FCDFE759D90433DAF81FC24847DB59E3F1398,
	UIWidget_get_height_mFBC3202F87974968FB377C58614CE88C94B1EBEE,
	UIWidget_set_height_m74F387C207606753DC06FE86860FDF2FE4BCD810,
	UIWidget_get_color_m95AC0009FA1B810799E9CD13059FB81C512E1185,
	UIWidget_set_color_mF4F8AF441D7B036BA06490D6CC2F30B3221D4077,
	UIWidget_SetColorNoAlpha_m51412375A9A5A2F10BD7421081E539DB2CBA9F97,
	UIWidget_get_alpha_mD79751A78C033D10D4A1FAE1C8BC787C3776258E,
	UIWidget_set_alpha_m1D2F49C76228E691436E84F6CC135766F3746C60,
	UIWidget_get_isVisible_m764764ED516E85D271BCB0366F77B11AE63188BD,
	UIWidget_get_hasVertices_m160C255D37C5C5A659DF777E13A8DE1CD98FE88D,
	UIWidget_get_rawPivot_m6AA7D43224C4BBE0659206928389101544C97B76,
	UIWidget_set_rawPivot_m0D416A11B5210671B18175F9AB97A1A714C261A9,
	UIWidget_get_pivot_m81FE06F39AFFB8521ECAA3F510848B809840C916,
	UIWidget_set_pivot_m23B38DCDF14101C72316255F873935DD1812DAA9,
	UIWidget_get_depth_m1B6831E51E344D0C7E199D75F0F588FE1D438AFD,
	UIWidget_set_depth_mA805C0FCA1F9E599A47E8645C4628E409CE432BC,
	UIWidget_get_raycastDepth_m8B56D2845FF838A957BBF816FCE515D69AF8CC7C,
	UIWidget_get_localCorners_mBDB209C1D75558B3F569BA4C69322B5F60A3CF6C,
	UIWidget_get_localSize_m99A52F3D2520E1A0CC77FD56A67457A9C0D9E517,
	UIWidget_get_localCenter_m0CEC08EE307E9B32FB6EC082E8605A98E3EDE9CA,
	UIWidget_get_worldCorners_m63B7184AA06A85263751DF402382B70A0976AB5D,
	UIWidget_get_worldCenter_m657AD490E77213650E2C7A31017DD2DB38FC955C,
	UIWidget_get_drawingDimensions_mD8B72C1D855684425D58542BC30ECC06CEB19FB3,
	UIWidget_get_material_m607983EA24B3AD623F9A110C51F7337A624E16EA,
	UIWidget_set_material_m66AC8B7DA43F78BD1726D742C889C233D2209BE1,
	UIWidget_get_mainTexture_mF6BDF21DE8C1EA1B98A2A5F3B1D22D7201020D71,
	UIWidget_set_mainTexture_m8CEA5C773D19E54E296922FD0C77C44C7598F05C,
	UIWidget_get_shader_mE5DFB5182640DB01B77899B93C6DA1809564718B,
	UIWidget_set_shader_mA86118B1459911418C05000FC471017DE3582C32,
	UIWidget_get_relativeSize_mD3680D627133FB1B33C8944CAE9C411D94839191,
	UIWidget_get_hasBoxCollider_m7C303CAD2047600C2C2DF29FAE561B0954C4C920,
	UIWidget_SetDimensions_m260BF1D7A9BD6B4D7491DD3EDD20083516D7FA8A,
	UIWidget_GetSides_mEDC6F8316A3D231C0284404717BCF05A048371D3,
	UIWidget_CalculateFinalAlpha_mA607C68C3F35DD5D0417A00D23A3C7815A68A853,
	UIWidget_UpdateFinalAlpha_mC1D3449F86ACA063636179BDCDEF61DF669B809B,
	UIWidget_Invalidate_m0CF4AEB0CC2711DEA16F9BCB0AEAFA1D783E8AB8,
	UIWidget_CalculateCumulativeAlpha_m38AB15C3779B1952455C4D93A6ABA5891739C322,
	UIWidget_SetRect_m9C723841936E57B4FECE645A06787B488C8563C4,
	UIWidget_ResizeCollider_m06950674D2A67F789A2AEF65BDCB44AFAF00859D,
	UIWidget_FullCompareFunc_m8A26746E8D923DC005B0CEA3E5397E1920D50FAD,
	UIWidget_PanelCompareFunc_m5243EE27D249C9FF4E4AD40361F0FA38467256A7,
	UIWidget_CalculateBounds_m278A87A3BDC729D6CBDDEF81862B2B38C6046539,
	UIWidget_CalculateBounds_m31352A2014D1D939D91A536B592858F8EAB101C3,
	UIWidget_SetDirty_m614ED3BCF9237CED22AE8F2B1AEB2534038C5C8B,
	UIWidget_RemoveFromPanel_mD0AF4138918814C39C8F5D191FC6F767D6F17B11,
	UIWidget_MarkAsChanged_m6A78B19F42D8F95784ABD03DB72229299F436B1D,
	UIWidget_CreatePanel_m81420CE0B4E84CB13C0FB52D73B5025472AC8BB9,
	UIWidget_CheckLayer_mA2985B207BAA98BC6EB38BB69241A9B6CAA28808,
	UIWidget_ParentHasChanged_m4B96CF7868A9907DBA59FE21F906190B44578DBF,
	UIWidget_Awake_mC5FA326FE922950136C565AFEA1D342C0737DCD2,
	UIWidget_OnInit_mD879CDBC6DE015A4F7EAA024F17EE442ACD5E2A9,
	UIWidget_UpgradeFrom265_m7768FBCFBEAAB25266555DCB45AE0ACABD30B27E,
	UIWidget_OnStart_m90F5F491D3276B1B6E0743294732B19DFEB2197E,
	UIWidget_OnAnchor_mB8FE129D9EF30107EE9F0C048A491C50DCBB889A,
	UIWidget_OnUpdate_m03E4F19607AB2951F0EF8E51AE44DDF0693DDC0B,
	UIWidget_OnApplicationPause_m811E558A524339D324106CAF3BBF7CB2410DFFA8,
	UIWidget_OnDisable_m126EB62FBB31E51C61E7939457D4A6A70D568EA4,
	UIWidget_OnDestroy_m345F7E030B89EF3620773EE2E52E4A3D6649892A,
	UIWidget_UpdateVisibility_m3E95D95A568E0A843964AB4F51123152FD556E62,
	UIWidget_UpdateTransform_m8567B2240B9820C5B368A4F9D779C0C50822A8D7,
	UIWidget_UpdateGeometry_m4ADC52D29C6448BB6E5517AFB9F2001E6520625C,
	UIWidget_WriteToBuffers_m6C94C9A12E5F880E8B5CBDB8765554BF4A16B36E,
	UIWidget_MakePixelPerfect_mB46E1643B60029E008E2F1BA090335552D5894CE,
	UIWidget_get_minWidth_m049D3D3CB7AE4A99563C2A378542F95651B20931,
	UIWidget_get_minHeight_m9D24A8447F54190325A75A8A45DAE237F3E232C0,
	UIWidget_get_border_mDE84BECB08267330656B68F6B4282C525B160D87,
	UIWidget_set_border_m194A7EFB88428D44F5F274C578AF2222A0437C53,
	UIWidget_OnFill_mF01AB6B21201740BE6D8088CF7160FAEA93C9D18,
	UIWidget__ctor_m15381CA62FD8A713D2EB69D28E00736380E754DD,
	AnimatedAlpha_OnEnable_m6C0815A157256CED25AC8313849F7BC7E7708524,
	AnimatedAlpha_LateUpdate_mFECA92DEE54250A6F9C1FE0FED8C386820D56AB9,
	AnimatedAlpha__ctor_m3829C89E80A80D83C080F1AC70897C3A20922921,
	AnimatedColor_OnEnable_mD9AD7365458EA540CFF3C689D5863BD14A5E0378,
	AnimatedColor_LateUpdate_m6EEADB4F286634FD81851493E19A374470F655D0,
	AnimatedColor__ctor_m4E73A4B9D7A696A78704AB5791446599E6718B03,
	AnimatedWidget_OnEnable_m1986C9E721179C012C944759A9697BD2CD5D5496,
	AnimatedWidget_LateUpdate_m747109EA5292C8F2F906E543A1B706AD12008DDB,
	AnimatedWidget__ctor_m763F9820C197B22A513D2C3EACABF8CEC58F2758,
	SpringPosition_Start_m9FDD77240C52F824A049EF39C95EA544DCF9D530,
	SpringPosition_Update_m67158C1AC0CE51326612A9090F812865A6BB7E51,
	SpringPosition_NotifyListeners_m5F6B54D296417CE6037D41B103D215731DA59320,
	SpringPosition_Begin_mF79F82F871D4AAA0859457A0CE2619EDF043B23E,
	SpringPosition__ctor_mF923DF4E721AE5866561DD017F3418CC7CAE0C2B,
	TweenAlpha_get_alpha_m241A3DE3B689A5495C7575E4A162431D6F0E3F49,
	TweenAlpha_set_alpha_mB243AA3AA7C729556964FBCED9CD80FC4679336C,
	TweenAlpha_Cache_m280CDA7B07703BA0A908BD38E925823EDD75EC91,
	TweenAlpha_get_value_m2BBD968CEFEC382D45D693B178C8AC447074E717,
	TweenAlpha_set_value_mD9A5EB274087A83C15FE8E74D6960F72D5ACC805,
	TweenAlpha_OnUpdate_m1B06AD33092E3AF2C592F60843683474D37699D2,
	TweenAlpha_Begin_mE1FF3A63EFD6AEE469BB14FDBE9E351067ED6C01,
	TweenAlpha_SetStartToCurrentValue_m2B2791243929FA8923AB80B71C8295D1FD365E4F,
	TweenAlpha_SetEndToCurrentValue_m49A9948E21A6C1B4E8F5A951F1E3978D138C8154,
	TweenAlpha__ctor_m22F3A2D9EBB8A4DFC9F4EEA615676AD835D20DB3,
	TweenColor_Cache_m5B2C0D509EBE5A61E94A80E94A57E5A22EC940A2,
	TweenColor_get_color_mC4882992104276229F31C2D1414B9B50B33E0D82,
	TweenColor_set_color_m502A9790AD360BEE3BC20A42A46A7E8D24C77DE7,
	TweenColor_get_value_m6469D6798A7E9A9E614FBB8B2E92095061B9C6BC,
	TweenColor_set_value_m6380219EF737E3A407FD3D5588A4FC35DB6B96F5,
	TweenColor_OnUpdate_mCCB17D6395229F4F74F22E07003F450B5BC18EB8,
	TweenColor_Begin_m65D6BCFD276DC396075D9518627E8CEAC679D899,
	TweenColor_SetStartToCurrentValue_mAB515CB1BAD0BF3C01C74E8AFF7BA62F7DC5A162,
	TweenColor_SetEndToCurrentValue_m4B038B025D85A77E1F15B3E8270E9BE8AEC5B3DA,
	TweenColor_SetCurrentValueToStart_mA752953F3F6B5458E080EE5F3EAF3F9B75FE9EF9,
	TweenColor_SetCurrentValueToEnd_m734F7B3B9CEDB9628A2F232396722304FE239C72,
	TweenColor__ctor_m176916136FD9F154B9F38A6E00A141A2DC38FD5C,
	TweenFOV_get_cachedCamera_m3C6E5F5297C5478D6D9549D2D41DCB16016C6A47,
	TweenFOV_get_fov_m985E0BC21ACC9DE2E516D36C1515D84670D3D6C3,
	TweenFOV_set_fov_m517FF589EA6818D7E7BDCD9E0A2AF7F71FB5A9FC,
	TweenFOV_get_value_m909E3DAFBB8370D106059B74A800F52EEB6F35DA,
	TweenFOV_set_value_mB40D3869A0033C22F8F061D6123A815414A70F6A,
	TweenFOV_OnUpdate_m7B02517C94F74A8CA26A3CB245E66DBE65D96AB7,
	TweenFOV_Begin_m0B33120E302DA8F2FFA12CD3D0CCCC604E816E11,
	TweenFOV_SetStartToCurrentValue_m6A50F02A2C8EB082F5DD3634236794B11AB8EE36,
	TweenFOV_SetEndToCurrentValue_m254CFA3A2989551F99B28A7BFBC36D5C85C9D47C,
	TweenFOV_SetCurrentValueToStart_mE1EF1423889BB213BAC664A42232D439E97387BE,
	TweenFOV_SetCurrentValueToEnd_mBD66AC40DC4D88061562402A3085AA7174EF5013,
	TweenFOV__ctor_m9174D5780F0069158ED940CE5E7D26615A654811,
	TweenHeight_get_cachedWidget_m9FF65A8C9656E8A1BEB4AF48AFDEC9C1AC07C516,
	TweenHeight_get_height_m9143FD7EB40879C6E220D691EA9EBBB5E0D621EF,
	TweenHeight_set_height_m76A2903A1579102011242B0288B364AC53B6EDF8,
	TweenHeight_get_value_m3DACA554DA23F36B09747BCA1571D94969F04A30,
	TweenHeight_set_value_mECBCDCC2862182A965CD149C296DDA61D2B88D0D,
	TweenHeight_OnUpdate_m323DB8FAF329D780B06F10CF4DF03641AF7F00F8,
	TweenHeight_Begin_m66080CB04417EB0C583E90F01390ABC148AC1D8C,
	TweenHeight_SetStartToCurrentValue_m0AB942FBFF965357B04FB6595B3D4C57D95988FB,
	TweenHeight_SetEndToCurrentValue_m71159A7422124D2E4E3128AAE2073C850F4D4EEC,
	TweenHeight_SetCurrentValueToStart_mC2600BBAA9979B2DADCC4065D39FABEB6B0895B7,
	TweenHeight_SetCurrentValueToEnd_mAB75F2579AC546DBAF8D2C20684463462273AFA4,
	TweenHeight__ctor_m3C10BC44777615275ECBCB1099B7C9531C5B39DE,
	TweenOrthoSize_get_cachedCamera_m9E6C25FDE7223FC92576584B871FFF4903FFF4EA,
	TweenOrthoSize_get_orthoSize_mB470EB9A10757D1AF9EF03A4D3601489349E0DBB,
	TweenOrthoSize_set_orthoSize_m2AF287C5605DE356B81667EE34070B3CAD4DE8BA,
	TweenOrthoSize_get_value_mECDCAC759AFC9E67C9C11EDF1D6833A15CD3CCA2,
	TweenOrthoSize_set_value_m242135017A5F3E2106A473B6B5E3CBA28CD16F69,
	TweenOrthoSize_OnUpdate_m0D3C5A18BD427D81341F482DED181E7679E8C99B,
	TweenOrthoSize_Begin_m42526FCD991983BB2C57F693FA9EF521144713BF,
	TweenOrthoSize_SetStartToCurrentValue_m6FAC81624BEE1115F12A74A31262A0560A8DA3A7,
	TweenOrthoSize_SetEndToCurrentValue_m93BDD784EFBD63892128E898D7151D023465B0F8,
	TweenOrthoSize__ctor_m5E0059FD7D7D04CA7D83FC2CFCD68CC17EC8B005,
	TweenPosition_get_cachedTransform_mB2FCF9D2A16216C43C7F09EE60A42B832F7E0CBC,
	TweenPosition_get_position_m00DC112C2CE10ED7693369CC2F828F7C81480634,
	TweenPosition_set_position_mBC49D42592B74F08FE88B042801A0871C3954D80,
	TweenPosition_get_value_m296E543A78B604623145F9228E2B0E35EA0B7F7D,
	TweenPosition_set_value_m604E6D107E23EB538924C0D3DC071E75D3893F04,
	TweenPosition_Awake_m26EC5559F86B3B98ECB9474E0519FE6C81732D45,
	TweenPosition_OnUpdate_m8A16A139AB92E4BFC693D6000329DAECE0FE8765,
	TweenPosition_Begin_m6135BC77DE682E8876EC3877E395D2CF2A75FC7D,
	TweenPosition_Begin_m591145BC123A1F1A2665D50BD86F7034D467E619,
	TweenPosition_SetStartToCurrentValue_m1C352FB520CD418931B79962DB8685A875C96E60,
	TweenPosition_SetEndToCurrentValue_m614F820F0F9B4B8E2A39801993851D5CEB78D4B6,
	TweenPosition_SetCurrentValueToStart_mD14B1B78476ABFC0AD1B46292DDEFF631822636A,
	TweenPosition_SetCurrentValueToEnd_mDE034DEEA71D656A066DEC62DFF940BC0CA8C184,
	TweenPosition__ctor_mC4196F443C4C32EECEC4178DE3137F6E9312796D,
	TweenRotation_get_cachedTransform_m87532B2684B73386C3D4DF98CE65317E0AD4B364,
	TweenRotation_get_rotation_mC4729B6D220AC1C541D162C700C1FF7BF4954524,
	TweenRotation_set_rotation_mE8DB816A26B36EEA996D0D5605A2AFC563BBF235,
	TweenRotation_get_value_m665460E9B94843F0DB261C0BA2438752304FBFF6,
	TweenRotation_set_value_mB752EA5C054FE0FA83F0252BA972DBC58E7791EA,
	TweenRotation_OnUpdate_m527FE3D82DA24C23868C604B3E1ED30688BEF552,
	TweenRotation_Begin_mAE089BEC2488CF55B668AEC690D4B542CEBC0939,
	TweenRotation_SetStartToCurrentValue_m5C3EFA9589AF8077E539952BB230273C2E6D5785,
	TweenRotation_SetEndToCurrentValue_m1A7322B155DC25A3D81F13F0C5121F409EC2CDC3,
	TweenRotation_SetCurrentValueToStart_m328908EFF06B3F61731F867D7537375F39498494,
	TweenRotation_SetCurrentValueToEnd_mEE5106A73CF13E2169074BAA9257850173D683B1,
	TweenRotation__ctor_m895A60EBF361716F220AD20AF389709BDD8D7DFD,
	TweenScale_get_cachedTransform_mD061040C5BC8A0AB87C973A2EC095EDEDE57010F,
	TweenScale_get_value_m5A2EFCD0945BEB1FD231CD08455B64A789C812A6,
	TweenScale_set_value_m1E2E524B665E2A0624611E789A8FBC9CE3305DCD,
	TweenScale_get_scale_mBB781570E66E516598C0A54F3BC61161E6BF1097,
	TweenScale_set_scale_m37F3F7D20F94FF56341557000728D3AAF7E7345F,
	TweenScale_OnUpdate_mCD87CF488CC8A7A7CB782A47475CB8D5D26F0BC5,
	TweenScale_Begin_m019929488D58C79F89C5B0D3382320892CC0437B,
	TweenScale_SetStartToCurrentValue_m9BB35FD691576B05165CBCAA0DE57601C92653F1,
	TweenScale_SetEndToCurrentValue_mAE858AE6B3EBB2C2BFA265F1DB516C21C321D5A8,
	TweenScale_SetCurrentValueToStart_mA51B39E40B5529A8FFF35E3F29082B66357C7400,
	TweenScale_SetCurrentValueToEnd_m0C38F938A6C8660215E8D098DCA20650BE087CD9,
	TweenScale__ctor_m7219C6C5E1868123E67F63D8598F0A57139212C0,
	TweenTransform_OnUpdate_m52473C365740E2FA94F845E41F934F0DF19B569E,
	TweenTransform_Begin_mDBDDA6E537B99AA6607DC4230AA4E570A455986F,
	TweenTransform_Begin_mDAE53BFBE8B832C60F778EE2394AA746350947F8,
	TweenTransform__ctor_m52C49BE4FC52B1832C7BECAFE31411E83A22E647,
	TweenVolume_get_audioSource_m1BA5B5577257343FCE4A8A52D4196A66EF6F5DD2,
	TweenVolume_get_volume_mD615AEC4892EAE0046F615D645C81E9C5980DC17,
	TweenVolume_set_volume_m5C4C08541ED5EBC481582A6D42124378EB77C477,
	TweenVolume_get_value_m24832F7ABB8E323E73C9E8BC244E51D1CD526F48,
	TweenVolume_set_value_mFB9AEB0318843005F486FC3FE9E8140C14B82434,
	TweenVolume_OnUpdate_m1588B4A1E4B102A989C7ECA9F794194ECDBAB3CB,
	TweenVolume_Begin_m267765236E46AFDC84367988BCD109E97AA883F0,
	TweenVolume_SetStartToCurrentValue_mD8DC5FDDB9D46DB226D74F80ECC452C63542EE14,
	TweenVolume_SetEndToCurrentValue_m4A4E08C23651D2B2C61C0DB7A5C2C7E0B20A5D67,
	TweenVolume__ctor_m26D151D7586233B1A7EFF58B0D1635055B94FD49,
	TweenWidth_get_cachedWidget_m72509E0558F2E27BB10504878FBD9CC810B0A791,
	TweenWidth_get_width_m67FECA3AC27B21668C5EEBB506AF20F2D07A738F,
	TweenWidth_set_width_mA47685DF8F52E1BBD8823C7347FE6CBDC62E523A,
	TweenWidth_get_value_mC551FDE56EE1702FBE2638BD0ADE6E5B5AE1BFAA,
	TweenWidth_set_value_mFC8876905F6E2B0834A765B208638A6611233EB0,
	TweenWidth_OnUpdate_m58C9902103707FB825567C69CF181A8FDDBA700A,
	TweenWidth_Begin_m905BAC85272A971D9EF2BC39B72FAF7E5AB1594E,
	TweenWidth_SetStartToCurrentValue_mC6F3EA47282F91292A7DD6C2E2E3440FB1FBED71,
	TweenWidth_SetEndToCurrentValue_m8557A0AD508E8BB0F2688241CB1F80E5321EA5CE,
	TweenWidth_SetCurrentValueToStart_m44F1FA22E1258529E9DF7EFE69349C6FA09B4A53,
	TweenWidth_SetCurrentValueToEnd_m90549399D2EF73A921E6AC47F9233ADD1EE44A52,
	TweenWidth__ctor_mBDC99A01803DCD2B693FAF3F0CAC66A89C40E5CD,
	UITweener_get_amountPerDelta_m77B2354E2E70C5FA72168D57B30E6A459DE48060,
	UITweener_get_tweenFactor_m3346C3BB3EE33A6174DA477E6A6E6789E2672895,
	UITweener_set_tweenFactor_m516564702FF65697C5C56C2BD94E830D37EE349B,
	UITweener_get_direction_mD7D9A2722D8D06E3F0E0A7DD84A32C0872347489,
	UITweener_Reset_m2EDAE6A6D123A65701D2532978923E1DFC419E7C,
	UITweener_Start_mE61DC78452BBD1908CC10983BD9CDCFDD5DC41D6,
	UITweener_Update_mD62A939BB0B2EF7532C2AAF373A9EE9CB2DEE413,
	UITweener_FixedUpdate_m4949D2C6B3F02CBB3998CE393F5B60AB8EBFAE55,
	UITweener_DoUpdate_mCBC23E1C682F2433AA66814578DAED98A5742D83,
	UITweener_SetOnFinished_m89D7AF68B4B1BF36551863203D89EED6B43A32E3,
	UITweener_SetOnFinished_m2BB03E9F562235D38B1F4B37B0A2CA43BAB72371,
	UITweener_AddOnFinished_m77E1D38EB82A475F720ECFD1FB04EF8A10DD1CD0,
	UITweener_AddOnFinished_m73730C3868832054C989BC69353B243AEA8BC4D7,
	UITweener_RemoveOnFinished_m8E7FF445CE4B9B6FA7204E45F1B95D6C67CDE821,
	UITweener_OnDisable_m8E3FEAC9A8D2D597400583256B4FAECA89EC4510,
	UITweener_Finish_m4F0B71FB3872B2E27DA2D58C392A9D2BA1221EF3,
	UITweener_Sample_m933EC29AC5E22697706F7D6925570A748844C90C,
	UITweener_BounceLogic_m8B6A27F4B57234ECE1A06C67605544A25F818A4C,
	UITweener_Play_m2502B40C91058DA380DDEB7178784423E31C14D3,
	UITweener_PlayForward_m1A6444E46C0C2DE3EC94A873536267D8FEDC3C5A,
	UITweener_PlayReverse_mCA94F29B817DE004B53865058FEFCC1A2CC492B2,
	UITweener_Play_m247329644D7824009B46D1C455FA25FDE0AD7FD5,
	UITweener_ResetToBeginning_m6A91A7B8A286385D7A4F37D26395626752AA23D8,
	UITweener_Toggle_m55CDC4B5E24C2A0F700BE3809B71548263DC082A,
	NULL,
	NULL,
	UITweener_SetStartToCurrentValue_m52C28B8160DFA7CDB155D2E15157E7CED713D15F,
	UITweener_SetEndToCurrentValue_m75C4AC06978337DCB1D52F1D45EF7A7C48ECC2AB,
	UITweener__ctor_m58E649975D51FCCD968F001173043618F9E190DC,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NGUIAtlas_get_spriteMaterial_mDC91C1E0B3E5CA4178BE2ABC94222DBD30A07683,
	NGUIAtlas_set_spriteMaterial_mC0DB3C2B430BA7011DCEB7EFBE1CA939D3FFB4B2,
	NGUIAtlas_get_premultipliedAlpha_mE842C894EB7DEFA1B0072F1DDC7A7545ECD6854A,
	NGUIAtlas_get_spriteList_mAAC68E66640C86E858C1EE3725F857D8890777EF,
	NGUIAtlas_set_spriteList_mDEB029FCDF8FA751D0DB0DAEC217E9B9390DDEED,
	NGUIAtlas_get_texture_m21FBD3CBF56A4AB5E9632A191CB31BFEA9DA0FC0,
	NGUIAtlas_get_pixelSize_m8EC36284EE10157940943E51C54E6E3AC4407A60,
	NGUIAtlas_set_pixelSize_m36B7B79A352B71684734CB1832177F3FCC5FE411,
	NGUIAtlas_get_replacement_mFBC9665D6D46D096DDEC7571584456CD7464646B,
	NGUIAtlas_set_replacement_m1B0D15C5951B9AEBDA4A45CA21510647DE98590D,
	NGUIAtlas_GetSprite_mD03D8C5763FB4410EE71F4B698722B86074C82F6,
	NGUIAtlas_MarkSpriteListAsChanged_m08CBC5DC15CAC2BE4C08D74FF7DF7A0A3CC1B050,
	NGUIAtlas_SortAlphabetically_m1F8F9ADC4A8EF1168B676BD99768FA3A62FB6314,
	NGUIAtlas_GetListOfSprites_mBF286BE86D2213E398312349279E5BDAB96D4DBE,
	NGUIAtlas_GetListOfSprites_m72887872FA71580D08FB35CAA3FC651DC37CB815,
	NGUIAtlas_References_mA33F20928BAFE8B08E11CA2A52A8B33879F6F0E8,
	NGUIAtlas_MarkAsChanged_mD47E21DFA9AD5C8F6A7988F3C52034EFD2E4913E,
	NGUIAtlas__ctor_mC5C882DEA68246DC8691BF24461D7F70C891EDC2,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NGUIFont_get_bmFont_m7243FD4762DE6DDEF271C71E3475C47401155087,
	NGUIFont_set_bmFont_mB6FEE1B76F32B3C29EB641F547380A9327E1D332,
	NGUIFont_get_texWidth_m3AED6D163B214B4A57E4E72A92447DC8CA015471,
	NGUIFont_set_texWidth_m4C74CF4EA349DE26CA0E97E8A4BB0A743A3090C3,
	NGUIFont_get_texHeight_mEA3A280AA9DFB8792483E6E41452382AF9F69FA5,
	NGUIFont_set_texHeight_m1E58F3C23EF1E30CE426E93C568F368C03DB470B,
	NGUIFont_get_hasSymbols_mE5EBE4E83D3CAE6A77CBFB052D7CEFDF735F5B15,
	NGUIFont_get_symbols_m32FCD752223135FAEFFB6F29CCED681CB7AE652A,
	NGUIFont_set_symbols_m7BBE356A18D55DC6C890A4E90B19B870C8F89B06,
	NGUIFont_get_atlas_mF6359BF502AEFB82E8FA349AA259390EE5E45095,
	NGUIFont_set_atlas_mADE8DFD6BFEC11C965896A19214417427E23448B,
	NGUIFont_GetSprite_m647C0B34A7008A4F76FB0037CD6D88C3382058F1,
	NGUIFont_get_material_mDA3ABB8F8937F16A141E3804BB87217AA16C4887,
	NGUIFont_set_material_mD609E59B02BE8DBE2FFF7609A5AB3CE98A1FC0B2,
	NGUIFont_get_premultipliedAlpha_mB28902FCA401E930BF46C38EF14A3272128EE177,
	NGUIFont_get_premultipliedAlphaShader_m6513460C466A7760DA90F8873AD61E9D473B1B92,
	NGUIFont_get_packedFontShader_m751904214ED2D7500BD15E4B0B9D351F3F1EEC32,
	NGUIFont_get_texture_mB35A71CE9254E38B4E25BD3DA0A0A676882F4502,
	NGUIFont_get_uvRect_mC4E244D5EB6ECA7A2F24C5DAD790D3A7BCB62C2E,
	NGUIFont_set_uvRect_m4298757026765B7CB4B25DCAE9A276DF08875A48,
	NGUIFont_get_spriteName_mF9E0E6C23790A5CFECAE040000BF4FCFEDC9DE53,
	NGUIFont_set_spriteName_m1D809833B35A3B648D922E4FD7551ACA82B0AF12,
	NGUIFont_get_isValid_m67569FEDDBA8CC479728639EFE3A0EE0491F86FF,
	NGUIFont_get_size_mC6C5528804EE4FE48251F47F529C58183ED1B28C,
	NGUIFont_set_size_m77727F6D4B96BE6518A0911FB1ECF2BF415CEE2B,
	NGUIFont_get_defaultSize_m50BF70C4E1E9292357FACF29F834A43994EB394F,
	NGUIFont_set_defaultSize_mEB4287D6086A9988A2901B72AE287E5A03C47C48,
	NGUIFont_get_sprite_m61E43BFD2E3DEEDB7D397DD219A52C8EA21A93FA,
	NGUIFont_get_replacement_m6C72BDEE4BB2FCBB67E7F50E05CA3667F67EB509,
	NGUIFont_set_replacement_m5408CC2234318B687ADE5E9574C41FBF8A577D64,
	NGUIFont_get_finalFont_m0A06FDD03A1538E83B0E209964ACA3D10A95B80D,
	NGUIFont_get_isDynamic_m20C51A821C667A0C696A238FCABC3DCC85B62B76,
	NGUIFont_get_dynamicFont_m636ED1D01EF0DC1DD3913596290F78F3478D8ABB,
	NGUIFont_set_dynamicFont_m3189483B77C2890260C67E74D953100C21062B60,
	NGUIFont_get_dynamicFontStyle_m22268921B171DB4C363FB390B409B58E44CD89C2,
	NGUIFont_set_dynamicFontStyle_mF10FBF57AB22AAF6743EB77297DB3A0E1E339C2C,
	NGUIFont_Trim_m53A0DF44A82E85BC96FEC42425B78677F76B1605,
	NGUIFont_References_m72981A19BF112EA2E80E20B6320F0C725784FEB6,
	NGUIFont_MarkAsChanged_mA3FB55C62DFD4DA5254E83C0BA3C7FE41148E045,
	NGUIFont_UpdateUVRect_m0BAA77B591F68585BB743DD3DBD6A0E9C5474155,
	NGUIFont_GetSymbol_m6A0174EABFC155CCC7DA4458BDA8FCB24FD6087A,
	NGUIFont_MatchSymbol_m2D00EE38464398EA6DEEE99B46A32525275330CA,
	NGUIFont_AddSymbol_mB604A233937C71214B9A9D6A446757D401FD91FE,
	NGUIFont_RemoveSymbol_mA6636E9B6DE56974B60138B06D16253BB084223D,
	NGUIFont_RenameSymbol_mD35210C918828593C8072CAF4B8ECF449C560E61,
	NGUIFont_UsesSprite_mDD3989EFCB3A0091765B7508A00E8AC1167C9FDD,
	NGUIFont__ctor_mA560F53147845E3ECBC7C40689EA2DAB36BC967B,
	UI2DSprite_get_sprite2D_m1E1284E1490F600C822136B18468DD0C4D64DB20,
	UI2DSprite_set_sprite2D_mAC7A95FBE25D09FD85F3D3BD95C0EE75B3DC8396,
	UI2DSprite_get_material_m6266CF7CD1E1FFB64E28F3561CB6D0CFDB0A0652,
	UI2DSprite_set_material_mBB0582819D293C98DDA514C68CBA7688ED34AA99,
	UI2DSprite_get_shader_mBF9C277319B43B6D2A8E00DCE73ABDF2F15229B7,
	UI2DSprite_set_shader_m3BC7A2EED4C05E1EE11615FDE584E6A4AB6FA70B,
	UI2DSprite_get_mainTexture_m42F3FB94C316EF4F62C1E66FBB79FF2CAF78DED0,
	UI2DSprite_get_fixedAspect_m8B6416DD2C5A1361B2F76405D2CB8302873DEB27,
	UI2DSprite_set_fixedAspect_m2920944190915B89F55E87ABF7342CDE8075439A,
	UI2DSprite_get_premultipliedAlpha_mE7FDA2263FF7480B90644BCEFF25A7D802A09A45,
	UI2DSprite_get_pixelSize_m55F2DF2F35BB32ACAAF870F1B93F378C76C33355,
	UI2DSprite_get_drawingDimensions_m4C8B77E29E8F4D6D353A3CA3D196B8B1DE53746B,
	UI2DSprite_get_border_mD46482AA2F62A70CCF4E2C05FED74269CD8B5D3E,
	UI2DSprite_set_border_m8F1A4F8FBC42FBA96DB23D3FCF74299555201ACC,
	UI2DSprite_OnUpdate_m0B277BD47412AA5F8E79D099DEA03EFC20C28C49,
	UI2DSprite_MakePixelPerfect_m7631B7CBC156F5766F2AC96198F56D0B0D301380,
	UI2DSprite_OnFill_m2ECB3D234798945398A91D4C9F93503CC8B09C96,
	UI2DSprite__ctor_m49FEA47A62C512EA3EFDB302B798AF031FC99E3D,
	UI2DSpriteAnimation_get_isPlaying_m6B91DE9C26AC0835676402D223400136469AC2D3,
	UI2DSpriteAnimation_get_framesPerSecond_m35E5264A030E87AC6C36D181A4D60445A2BAD65A,
	UI2DSpriteAnimation_set_framesPerSecond_m7F440608C75F05F53EC34A96BFFF6DB73830BD80,
	UI2DSpriteAnimation_Play_mD4A2041842B0F7262F7C9B8164EDAEF7317967DF,
	UI2DSpriteAnimation_Pause_mA19F460F1AF9E37EB5F9B13A45D32EF0651860C9,
	UI2DSpriteAnimation_ResetToBeginning_m216B696F56ACF81CED283724709AC4D4B3827805,
	UI2DSpriteAnimation_Start_mEE2D459BFCA4CCA1603D1C0F597022449D7AC029,
	UI2DSpriteAnimation_Update_m7B4D97123C0BED9408FAD9CB8E13FFB9AB00E3E2,
	UI2DSpriteAnimation_UpdateSprite_m8400E5E064EF6CE941F8E44FA3DF48F8063202B2,
	UI2DSpriteAnimation__ctor_m553CB9535E79BEE9EB33CE394417572E533B8876,
	UIAnchor_OnEnable_m87C5C94DD470BDC63F2F19CA86BADE170654CF69,
	UIAnchor_OnDisable_mBDF4A12356A7512878C8AEF6F2321E971BF596B4,
	UIAnchor_ScreenSizeChanged_mCD05D6AAACD945B7C6406C2CA61F9F49929D8EFD,
	UIAnchor_Start_m3C2265A24196A042BB15A69B362F3B78E185B8BB,
	UIAnchor_Update_m00FA68CBB09AE49A550EBF2FAA49DD70151E4591,
	UIAnchor__ctor_m7EA683F444FA30CF0158A5EF1A63C760985794CD,
	UIAtlas_get_spriteMaterial_m7371EEDA6F2273E5F939C5ED397446183CBDA923,
	UIAtlas_set_spriteMaterial_m6273EC1924D97E1AD1E2C7B2FF3A6FCDC315868A,
	UIAtlas_get_premultipliedAlpha_m5774627993086F87F282C6EA68E7FC1F012EA219,
	UIAtlas_get_spriteList_m07EA1B091518CAB738F9AEB75AE6B681F777133A,
	UIAtlas_set_spriteList_m2F58785C0632D40772650F12C95A8767893BAF73,
	UIAtlas_get_texture_m9FE3AED0F265F5E46D2C5249D3BBB48107EC5328,
	UIAtlas_get_pixelSize_m76F4624BAC553F89295601A4E986E4E3635EDACC,
	UIAtlas_set_pixelSize_m05161F9ED542CC2D94F4182BD91D2165AA92237F,
	UIAtlas_get_replacement_m1FD1ABA22D29C761064F38E617355813CDBB52E6,
	UIAtlas_set_replacement_mD10C3CAE1EAF8DE25F85468059A148DD4DEE5577,
	UIAtlas_GetSprite_m37CFA130126C3727653AA3F6680AD724A566F9BC,
	UIAtlas_MarkSpriteListAsChanged_mA300FAD6DA9BE967C2DE425755CF6E1B6AA70BB2,
	UIAtlas_SortAlphabetically_mF6D665166E733EB92646196733A22628152EA636,
	UIAtlas_GetListOfSprites_mBBECAEBF4A615D4E9322A542938B8FA985E07CBD,
	UIAtlas_GetListOfSprites_m866707A85E4F30B9E36EB386BF902D5CB4F14509,
	UIAtlas_References_mC91AD50368654CC0785D111991267920F102C004,
	UIAtlas_MarkAsChanged_m53C35D8CCB3EC410E821BDC19F8C2F48FAA33D78,
	UIAtlas_Upgrade_m8E9984FE90DE7C88B9998117FAC1255D869171AB,
	UIAtlas__ctor_m0362C4C0F41843BA4D10D3589ED6A52E0206B2E6,
	UICamera_get_stickyPress_m4289E4BC47B126F40FF3D94ACAE0CDEDE6A96975,
	UICamera_get_disableController_m53D285BB8CC81822F59956C453FB8EC2FDEEA526,
	UICamera_set_disableController_mD0DFCEA1D399DDCAEEC6610992FB416833BFD94A,
	UICamera_get_lastTouchPosition_m55F7C6E5D2C7A851A7AE555A97C83EF1462D9BDF,
	UICamera_set_lastTouchPosition_m6497F86F5EC29DE8D635766474495DF4854C4377,
	UICamera_get_lastEventPosition_mF96ACC6364A504251DC5A35DA40EEA747584F4A7,
	UICamera_set_lastEventPosition_m9691322578126A5FA41B454C2E0F9987E3D6AC36,
	UICamera_get_first_m350ED998D44F98B449B0238F07FD9CA93ADD1D72,
	UICamera_get_currentScheme_mF9535F4CA9EA5AA19806D5C6DEAA0B542B219E0C,
	UICamera_set_currentScheme_m313A1E0951B8E2ED0E60F230B383400AB8BE47EC,
	UICamera_get_currentKey_mC322520CC1398D25DE63B15AEC6809283098CF05,
	UICamera_set_currentKey_mA37CD11C4A5C833FAB5936D36C8D0C8A2F340A26,
	UICamera_get_currentRay_mE89B1AC7039F1E5559C61E37DE5EB4BD8BC872C3,
	UICamera_get_inputHasFocus_mAD29557111E3C2F2B06BFA0E1432E4C015D66CDF,
	UICamera_get_genericEventHandler_mCF068433F16A810812C355743FDD6851673564C3,
	UICamera_set_genericEventHandler_m273F1E600062BE1CB82F5D0F6E81BDE9CB49FDE0,
	UICamera_get_mouse0_m9426DED7833A196B9EDA70357D5DE9955CD8382F,
	UICamera_get_mouse1_m769FD46B12D60B2815043DF679DE29DE7B8A26AB,
	UICamera_get_mouse2_mAECAC667003790D721AC8D7E48DC5B7F90DC7E49,
	UICamera_get_handlesEvents_mF529A27FCA66621F66D40A8DAAB27778682162D2,
	UICamera_get_cachedCamera_m5D9A047CC4DEB7E47277D855AB4618F6B5B39718,
	UICamera_get_tooltipObject_m757350B10B875BDBAD81A89951AC02EC8256C3AA,
	UICamera_set_tooltipObject_mE48BBEA24C26C5135165AB3A230C95A48944A59A,
	UICamera_IsPartOfUI_m3E5DB6029CC731162F8A1076F597CAA65CE89CDB,
	UICamera_get_isOverUI_m3E925F361D4B998199661AE94082C5F89C02CD1B,
	UICamera_get_uiHasFocus_m141895F2E0C394581CF456FCEF6DCC7FB98263D8,
	UICamera_get_interactingWithUI_m8EE9692990230B8F75413AFDF8825E6D59CD00AE,
	UICamera_get_hoveredObject_m6187088315284EB93BF5173A9C949592A70C56BC,
	UICamera_set_hoveredObject_mE36A960DC8BF0955CA25F2EBF977C2C2F09E7C69,
	UICamera_get_controllerNavigationObject_mB4678E1FFBDA02C02876193A1CCC916B04DFD360,
	UICamera_set_controllerNavigationObject_mF3A475C9842B26EC3A0DF5EA018A782EDBF3FC80,
	UICamera_get_selectedObject_mF2C7E24A50234C79FE6ADC9FD163286B468B7312,
	UICamera_set_selectedObject_m684E76F4F6FC35B63D9969E494FFD7EDAFF7553E,
	UICamera_IsPressed_m451DF6B0635A5B247D8DF707DA5277247D75E9B5,
	UICamera_get_touchCount_m5ABC8F8CD89D38D495D38C09BFC5151FF6449BF5,
	UICamera_CountInputSources_mACC286DD14458DA213056B1C4FDEBA477652EF27,
	UICamera_get_dragCount_mD4E72C1C288F18C9899A331D4E6103F5B8F34C26,
	UICamera_get_mainCamera_m608FDDCB5F90D887B187F9415C6CE379704DD96D,
	UICamera_get_eventHandler_mE028D2D3F68A17FAC0B0501C16CD1AFE65BA6246,
	UICamera_CompareFunc_m1768E988EDD3D9E68C0EC119FA01EA989FA16DBC,
	UICamera_FindRootRigidbody_mBCB9EFDA741DA78F111FF29D4D1C976CDC9C88CB,
	UICamera_FindRootRigidbody2D_mEF6B37400FE4FF381C2EA4907D63AF2720C6DB82,
	UICamera_Raycast_m99FD036EF140E8B716C6AE4AB42145653AA065F1,
	UICamera_Raycast_m9A633A8F7041E28AA90E07B2D74AC8EBFB2F1848,
	UICamera_IsVisible_mFF8DF92B236B0F365E684A1AA235F031EE21273A,
	UICamera_IsVisible_mFA2D887599BF69450F4EE5967157CE7AADB8E9EA,
	UICamera_IsHighlighted_m780968D78E50807FFAAD96FD75AFB2424D4AAF74,
	UICamera_FindCameraForLayer_m697DF5ADBF9DE45F2AC644CC2E54BE66E137B2E0,
	UICamera_GetDirection_mE127CF716DC7F15886CB351696C3C997FECE6018,
	UICamera_GetDirection_mF9D6D9D93B850A86BFA3D59B4A551BCBFC092B41,
	UICamera_GetDirection_mD76C8CE5D965D1BAE36373360DADE0C63BBADB67,
	UICamera_Notify_m332E4C0410D4FFF5BE5D37AA14AF49D304790690,
	UICamera_Awake_mFB78DA7BFE9F4FF67A16C805F4E27E1596A7E54B,
	UICamera_OnEnable_m3CDBB9AF0B2BB140FF2C92FBA7E8CDF9D0845A26,
	UICamera_OnDisable_mB5EBF7DCA72ECC1AC9D7F6C876843A561A220DBC,
	UICamera_Start_m5232F49E693490097B1F56A5AF8BE4E5D24E3F82,
	UICamera_StartIgnoring_m22CE508E12581B0D83A132179D8CC4B3768B39B4,
	UICamera_StopIgnoring_m83B4332C8763E8F899FEB81342E5BC872D6CC853,
	UICamera_Update_m362960F50925D887D20B13BF23D6262423BA41B5,
	UICamera_LateUpdate_mDBFB6BCDF00A1BABFD19880860815CE2B0C50B75,
	UICamera_ProcessEvents_mC44E0C86E7ED295D1EC6561526525F608A5D5BDE,
	UICamera_ProcessMouse_m36664D44B02381C1C09D0327BC4C0DADF01FE932,
	UICamera_ProcessTouches_m3103656DD27C522902BC5132CACA50B6F6C1576D,
	UICamera_ProcessFakeTouches_m0ADEFB69FC9D4906D09D2F9268C642584ADF1A6C,
	UICamera_ProcessOthers_mA438599E5E33F3186C08740A90A3F53E691E23D0,
	UICamera_ProcessPress_mA6292ABA2A1EC20CD7E6192690724F7E8F411B3E,
	UICamera_ProcessRelease_m8B5D117C3E8FB1CE9ADAE3664BFF13E621F68E9B,
	UICamera_HasCollider_mF5E5787E537DE8D0F5DA2CFED3A21B49814DC4F7,
	UICamera_ProcessTouch_mE0567F856F14E5BA7E82DBDB4C294ED6B409C5D6,
	UICamera_CancelNextTooltip_m9EAD7EF6FF656B04FD5EF27B3C944E0B94DE8C35,
	UICamera_ShowTooltip_mBBA6E4250B0311FA799E12A0395D102CEBE4B2B8,
	UICamera_HideTooltip_m685D7DC6784E7C44CB9C918D1F9505B4EE5D1836,
	UICamera_ResetTooltip_mD702FB5126BA49F3E2B33998CE92A23257CAA518,
	UICamera__ctor_mE7572544B4650B5DE19D7E3A09A23F704517AB0B,
	UICamera__cctor_m043C3AE3CE3F46EF088D788BB4A0044FEC23C9D6,
	UIColorPicker_Start_m68B322FC9D63307BF5DE74DE713161882BC1BF25,
	UIColorPicker_OnDestroy_mFBC918AC4A99F2DE546735699D4EED5BACF51A31,
	UIColorPicker_OnPress_m151965D6BFD35D6369AA68CAEBAC54FBA9D2924B,
	UIColorPicker_OnDrag_m8E9B5228C31485E6AE67CD39EA9EAE67F409A88D,
	UIColorPicker_OnPan_m39EF615BA031305C91EC68A4D498F44796D34DEE,
	UIColorPicker_Sample_m32A569A7282A96708168619C7E1459F69E2D0757,
	UIColorPicker_Select_m9D1918E9E9E621102580F2B386B9BED8765D7A84,
	UIColorPicker_Select_m42E458C8FC3533B5B0672A2E959DA1964F27F3A4,
	UIColorPicker_Sample_mFE2340C551B0657CCCF4B44869DAF04716168ED0,
	UIColorPicker__ctor_m42FB46A9DEE608917930B107779CEB578B4A2045,
	UIFont_get_bmFont_mC28A1C77FB375BB603767DD8FBCD38321F294190,
	UIFont_set_bmFont_m649C8858576E44B19E38026266BEB89F56B0D807,
	UIFont_get_texWidth_m8171B683D5E10948F56150A74E04D5B8373B14FE,
	UIFont_set_texWidth_m96FD34E962B2631F3FB8876905A4D3625031356B,
	UIFont_get_texHeight_m3D7529E1503D3FF9645F1E51DF589D66246602FC,
	UIFont_set_texHeight_m86957EA62E7BB14860753CA9001703C0942DC7B1,
	UIFont_get_hasSymbols_mD6DC06D619AAE2A67FF7076F4F0724ACD67B4B89,
	UIFont_get_symbols_mEAF54452A3B1FB3B713D1069AAD988AA2A589F0C,
	UIFont_set_symbols_mAEE3169A0F3B35F495871A7AFBDE58862E6168BE,
	UIFont_get_atlas_mDD89B2869A9D95B763B576C6A5B1BEBCBE4C06AB,
	UIFont_set_atlas_mFC022BAF28AC1FBC3FABE5B1244440BDC1D9C808,
	UIFont_GetSprite_mD3A6303F857F0E99FD0E75B1A1A733E47D154AEE,
	UIFont_get_material_mF4DCD06F529651B25C7D23E0EF5F555261BE8186,
	UIFont_set_material_m28450BA18EB15FDAEA9C7803F58060F087ECA75E,
	UIFont_get_premultipliedAlpha_m051130ACAF345DD391144C7739EF3BB3DC4A69A8,
	UIFont_get_premultipliedAlphaShader_mDFC54BF4554A8E70426C8037CF4FBB8AE2897FA3,
	UIFont_get_packedFontShader_m0F406BCED91B77429389A87172D3E9042E9DC3B0,
	UIFont_get_texture_m253AAA33E8525FF29C25C200B0FC611E257F70A5,
	UIFont_get_uvRect_m8814C374D7B3AE121D694F3B304DA94AA68B5ABF,
	UIFont_set_uvRect_mF7BDBA704CBB413BC4C8F7E9C15F5BB116204390,
	UIFont_get_spriteName_m45C8F68EC9DC8BC4F0295A6FEAB4FF531AE98CEF,
	UIFont_set_spriteName_mED22D5D38B739741538D545FC37831E23ECA1777,
	UIFont_get_isValid_m67D1FC72810346A47A9F8C482E715C46302C0137,
	UIFont_get_size_m4090157802E2D48AB95694386DB661E73F1B50DC,
	UIFont_set_size_m9AD684715540EA94C211227E2BFD5FBEAAE2150E,
	UIFont_get_defaultSize_m19D708D60FD82A82CE6A3438F26A96EDE7C61442,
	UIFont_set_defaultSize_mD67475EF3DCA22D69EA34EA520CBAB0047C42B9B,
	UIFont_get_sprite_m416D73C6CA299A01525211CA9AA003972534943C,
	UIFont_get_replacement_m20F942AD6CB79776A2FBA6A84C0F905FD4B34BCD,
	UIFont_set_replacement_mEE0DFE7CAA5622405708974A09757BB5DE25614D,
	UIFont_get_finalFont_mD85A4FC36E28745F3215FF1A621DB0CD129C99B8,
	UIFont_get_isDynamic_mCE3444BEFA124649D682C71A2F360A0C24782166,
	UIFont_get_dynamicFont_m4BCB8A0FD57D0E01071B611B44FFF99D4844FE72,
	UIFont_set_dynamicFont_m3412D992F4387FBFB84DCAB7080D21216EBAB1F0,
	UIFont_get_dynamicFontStyle_m6DAAB335734B6C579D3C4023231BBDFB81D8EFE9,
	UIFont_set_dynamicFontStyle_mFDFDC51D35A9E047541920A2982DCA8C63261544,
	UIFont_Trim_m36A261EC90F7BDF9B5180F875533286614311148,
	UIFont_References_m719B1E019DBB1C36123BC847D3AD71E706E55D52,
	UIFont_MarkAsChanged_mDCF5F46D989B1F3DF6CCB2BB5A86DCC831B68D76,
	UIFont_UpdateUVRect_m803C299339722F39162A863B2C91D13C4053531C,
	UIFont_GetSymbol_m3F84B8F520FC0A5424679E1295D248C4919EE2FA,
	UIFont_MatchSymbol_m2841B326B52B200AF7602D4292C0C0AB5703B3E5,
	UIFont_AddSymbol_mDA2B8876C3F2B135ECD5B1F298A2F0FFC4570351,
	UIFont_RemoveSymbol_m10FCAF527E1899BC7FD9D659A724418A262F1C38,
	UIFont_RenameSymbol_m213E4B1A36A75DE8BB96B4998A3C39595A4DF85B,
	UIFont_UsesSprite_m368DDE708F7733FE6A9099FC31EED0C8AE1A37E3,
	UIFont__ctor_m1031C618915F5981EC733CF8AB86E761C08A4BB6,
	UIInput_get_defaultText_mC4CFCE41D7070571CD81D3C11FCCE935A829E4FE,
	UIInput_set_defaultText_mDBFC354722CE3FD6051FF5E0D95DEA85D03415BF,
	UIInput_get_defaultColor_mCA3071A424BB59184F419AA5BFEAE8DFC6AC2F7C,
	UIInput_set_defaultColor_mA0F435DCC00644E9BF26E4BBA11EF1B67C2500EC,
	UIInput_get_inputShouldBeHidden_m6BADEE0A29B13D2BC74CF5D13C651B1DDD96959A,
	UIInput_get_text_m0B29D843E5E408FA5DC4F6D800B81F8ACBEC1DAF,
	UIInput_set_text_m9FC1CEDD041D2F390CDD508686DDD738148EC4B6,
	UIInput_get_value_m9CC94B340302716271E787645BA343BB7DD30822,
	UIInput_set_value_m22AEF2CA93D0D2EF636639C3C54369AA6FF6865A,
	UIInput_Set_m674B5853322FC95305072B79FF4CA2B6693C4E16,
	UIInput_get_selected_m98C81B59877EDC8AC5A9B0265DC7A846BFBBA197,
	UIInput_set_selected_mA59C56E0AAE479AF5559945E449AF8623EDD9CD9,
	UIInput_get_isSelected_m65688CF6B2FB314F5EB9683AD4E751148B3E7E3E,
	UIInput_set_isSelected_m6B7CA7FB02CB028754E998EF6938155874629658,
	UIInput_get_cursorPosition_mEFB1BC5B0AFAA18ED2064A25065CC8BCC7274977,
	UIInput_set_cursorPosition_mB11DFB23912B68B76397A54A1614A6199E24515A,
	UIInput_get_selectionStart_mFA8DBC990E80A7B4362CCA75B526B1A351FFCE92,
	UIInput_set_selectionStart_mC0CDAE4ECDE7AEE00291525E4DA32EC451B5275F,
	UIInput_get_selectionEnd_m5BEFA5A7F3E9E77959A58F6BD91F34237BBEAC58,
	UIInput_set_selectionEnd_m9A01C1EB5CC1370635D1431AFF4A6329DF1DB607,
	UIInput_get_caret_m3FF87B4360152100654340DFD779C7BF38999098,
	UIInput_Validate_m1D4347A872FE65DC9C30AA343E2838A39E90C9BD,
	UIInput_Start_m3A3189CB3B77EE464A87B58FFEC96CF5BF877695,
	UIInput_Init_m1DFF06A31927382F25BF529B72B972FCE566FAE8,
	UIInput_SaveToPlayerPrefs_m01151DE065D19ECE5EDA77F2436A6036A35C2B93,
	UIInput_OnSelect_m7C57C9C3D6503E5BC77D005A85D91D4A371A1A6E,
	UIInput_OnSelectEvent_m64384473BC16227D1CFD95643E62D2B394EB1DCE,
	UIInput_OnDeselectEvent_mB30D2D6200FEB86023134E569687002D8F464618,
	UIInput_Update_m104AA1C2C1312057BFD5658926E9838826BFA505,
	UIInput_OnKey_m281CCB262AFB8A1927D25B2D903CB690A6B01CED,
	UIInput_DoBackspace_m0FD7585FDDA2E1E125E476D25F93247B9DF63D78,
	UIInput_Insert_mEC3E4E9AFA08E098B313B966D878365C6E6CEA42,
	UIInput_GetLeftText_m2A8CA69FC45D63E7792E5C869EAC4370C7A926F7,
	UIInput_GetRightText_m68C9EC25592809A9DF9B18A90692BB6EDCF8ADF8,
	UIInput_GetSelection_m52082544051670074501EB3530743C9EF76B3B58,
	UIInput_GetCharUnderMouse_mA1CEA29FE6B148C00D4BC639B979C007B6F45BD8,
	UIInput_OnPress_m629B8D7D32D568CA83A6F41D6C0BEEDCE4A8353D,
	UIInput_OnDrag_m3A8192AE8CE107A68B7F2AECB706AF63254B0F2B,
	UIInput_OnDisable_m18581657A834C4B1222633DFBCB74A08D361EB4C,
	UIInput_Cleanup_mFA5EC55D1AB273CA320970EF6971439E88FD94C7,
	UIInput_Submit_mA1C05CA28B7987013C701326ABFB334619659DD5,
	UIInput_UpdateLabel_mB11D23F3CD774EFB2A074D5AE1051548BCB77C4F,
	UIInput_Validate_m6C7AEADDCEBDFB34DDBDF0064E88829F376968C5,
	UIInput_ExecuteOnChange_m33FD9EECE3D7273FA3369DF986D48C91BF65A585,
	UIInput_RemoveFocus_m1F30C0FAB4C00CB90ADD0022CEB608DA7E44AF20,
	UIInput_SaveValue_m554783D1C941704F5B1F116488FF824026385BCD,
	UIInput_LoadValue_mCE1A321E6FD24681D47682121E5986AACC14AD73,
	UIInput__ctor_m3CDC658FEB019232D87CBBE33B1C149217B193D8,
	UIInput__cctor_mF543F47D7E1AC8A886FC24A11093CBA7E962D6DA,
	UIInputOnGUI__ctor_mCFE0D3FB40D24D4F331B5B8169E73FBF2983D1AB,
	UILabel_get_finalFontSize_mAF390ABBDDE8BB18741F9AF14335AA2A80A502E9,
	UILabel_get_shouldBeProcessed_m33C1F4726DDBB8BC43D470478878AEC6030AA7B2,
	UILabel_set_shouldBeProcessed_mF848DAE2C439A028441FF0A81305B2128C4F21C0,
	UILabel_get_isAnchoredHorizontally_m0D122A26474BBEA20960ACB929B0E4A92907DCBC,
	UILabel_get_isAnchoredVertically_m79815EE93669C2DFFC451EA2A8CE050B7EFD56FD,
	UILabel_get_material_mF2A0909977FA3E11070DC5A44083A445C0DF9378,
	UILabel_set_material_m93F8E638A30CD42581F70B0EACD4E584145431EC,
	UILabel_get_mainTexture_mB1BAADF9F8E5538BCE8FA346519512D2C7FD11FC,
	UILabel_set_mainTexture_m915AB05BE02CB8742D3CCD4E8FA3CF4DB7021B31,
	UILabel_get_font_m8C1C7EBE46424B46AE0576E133DFF27E37AD0791,
	UILabel_set_font_mA8BF3F820A14C36DD4F37F876514079675EC6226,
	UILabel_get_bitmapFont_mA29FACDD7C70E457B7BE3484DDBD2569387D69F7,
	UILabel_set_bitmapFont_mCD3850E87E521E5F50B0E0F81807200661415A2F,
	UILabel_get_atlas_mD37F618C7D05CE1806640D50323F86DE2E206E5E,
	UILabel_set_atlas_m306A4AF6EF74D768200B3363EC7E477365D278F8,
	UILabel_get_trueTypeFont_m892287B19700E96C4556CFB992B072D28FF2E12E,
	UILabel_set_trueTypeFont_mE22949BACA92A29B3A4070FD892F27842FEDEBFC,
	UILabel_get_ambigiousFont_m9F2ABC34155E9B33D7D5F09633127D6B683A9B1E,
	UILabel_set_ambigiousFont_mFC4CFF650E6384FCB2F0099F4368DC07C50ED97A,
	UILabel_get_text_m0EB04C8F82985701EF0FBB2F86B9063C687E5ED7,
	UILabel_set_text_m4B5BE8EAD657BEEA4CC32A0272AECA3ACB8C35CE,
	UILabel_get_defaultFontSize_m52C50D86BE44627A7D2E49A44946D99C810AC9FF,
	UILabel_get_fontSize_mAFE1540C0E7FF7CA1B3F5824B723352B94C065C6,
	UILabel_set_fontSize_m1FDCFC9AB8B93443B49C5B2F13A5DDDDCCD4EE8A,
	UILabel_get_fontStyle_m0B943B3852820F157ED474E153E8603C79EB845E,
	UILabel_set_fontStyle_mBB60ACF5E137DE2792DDBF728669F8021AAB4E36,
	UILabel_get_alignment_m7770D32D5AB8EE1DB49F4D9B32C3A38BEB699C78,
	UILabel_set_alignment_m97721ECA2AE471EEFCBD9025BE767F730AADC6EB,
	UILabel_get_applyGradient_m3F01DF13EA8F80E50D994606E4F2C34BDC5F25D2,
	UILabel_set_applyGradient_m0DFA68C382126B67FF91D9BDEA108D4C3C823DD8,
	UILabel_get_gradientTop_mC15B476DA2E352D32BFE380396D376303A0F144F,
	UILabel_set_gradientTop_m815A5CEB2B89B0E4CA23713573D442A5C28C3903,
	UILabel_get_gradientBottom_mC0A2E338763967C5D3A9DA18AAB44C6509783C45,
	UILabel_set_gradientBottom_mC7B0A53A69F93210FB6E355D482818864B0F3C0C,
	UILabel_get_spacingX_m609D8B4986FA798B1ACABCE7BC352D4ED81B3FBF,
	UILabel_set_spacingX_mC1CBF8632EAB40D17815EBA40C7F5FA501BBE5CC,
	UILabel_get_spacingY_mF57CBFA7E9F0CB0C6C1212E35268A01B50DBEE38,
	UILabel_set_spacingY_m10C8032A82E11F87BECC258BC851E6A7D4849806,
	UILabel_get_useFloatSpacing_m2243942162004C708515D07AC83AADA8A18478F4,
	UILabel_set_useFloatSpacing_mB3E81185115B4096CE597AE7B08BD169D0C1C2B5,
	UILabel_get_floatSpacingX_mDBDE3F3B8BC329BDE893F3BC0D773DD582047D0A,
	UILabel_set_floatSpacingX_mD51501C33AAD498146B8B9C93D50E9A1B850B616,
	UILabel_get_floatSpacingY_m3CC8271888CC0AE018B5B88438FA89E81EFD20CD,
	UILabel_set_floatSpacingY_mF5F1637FCC5DC038BFBC9E856E48C014F10AF072,
	UILabel_get_effectiveSpacingY_m7DADF35B3837C245CAD56AB2AD5D622550A5E941,
	UILabel_get_effectiveSpacingX_m474A2765705892FACE4834E0A630BFA1BFE4005D,
	UILabel_get_overflowEllipsis_m7FE9D42C8BA7087E6D0385342E2CD2DC1D73976B,
	UILabel_set_overflowEllipsis_mF0BBAD32DB161BCC4A0203B8225D5E8B7547ABC5,
	UILabel_get_overflowWidth_mC8333C28DEAD3371320DD42B797AC30A1A91FE02,
	UILabel_set_overflowWidth_m423D40C345B0AEA3835ABFBD39E08E0024F154E1,
	UILabel_get_overflowHeight_m2068162AAC1EBED03473F06230734374FDBD2FC1,
	UILabel_set_overflowHeight_m4B42C34948AC594F5250EB39A68E5EE33254976C,
	UILabel_get_keepCrisp_m4D81A25254F5CCC6404CE7F60263E76F48907DFA,
	UILabel_get_supportEncoding_mC69D57B55567FAEA0C72659882C197AE88216227,
	UILabel_set_supportEncoding_mCE8A5BE4F0E6349D2DEF47A4B432B77EAE889C7C,
	UILabel_get_symbolStyle_m56E0FAEC4981028324306A9889DB8B58424BED55,
	UILabel_set_symbolStyle_mCF23BA041D2FAFF79E9C2CC1B2D607B49863E3D8,
	UILabel_get_overflowMethod_m1E54E5A775CDFE66ECE147CA27119FCF962744AA,
	UILabel_set_overflowMethod_m76DEBA8BADD21ECFA0E1AC6BC83B28FD32806B5F,
	UILabel_get_lineWidth_m7BCFDD110ED48402D67E802212C1E57E93F97454,
	UILabel_set_lineWidth_mF676B776531B9342C7D84411060AE9698DE47525,
	UILabel_get_lineHeight_mD10D5DF298FFB886CAABBF70FE218EB81E291F98,
	UILabel_set_lineHeight_m9CEBA80B805D30F7DB5155C26421EC0DE3B47E9A,
	UILabel_get_multiLine_m68209F29D6A94F7D612DACA1EC26BDACBD454E66,
	UILabel_set_multiLine_mDFDBBF8F9E4F5D6DD41587C3A002B12650D63FDF,
	UILabel_get_localCorners_mE8FF29B0289DE24A3431C493C4643DD0AE0D3FE9,
	UILabel_get_worldCorners_mE6DD227FEF24C44B64C2FC696E33D557FB0ED31C,
	UILabel_get_drawingDimensions_mB1B1B20B64B1C4E5B54381AADA80D08E6261A56D,
	UILabel_get_maxLineCount_m0442B08F374BC98D9A5421F95B561A0EE33B491A,
	UILabel_set_maxLineCount_m3EA762A5FC20417DCA8DFC158CF9CC8C6EFB211C,
	UILabel_get_effectStyle_m05B562546779D566345AA6F5CCA50936CC950C21,
	UILabel_set_effectStyle_m23DD614F34C1A279974639D448AD6B4FCC85562E,
	UILabel_get_effectColor_m9F3837FF21E48B0F6C0B4E9BCC29C4B2BD0555BF,
	UILabel_set_effectColor_m981FF1AB2ED11C1963791E98C4FD47FD80A2C7D3,
	UILabel_get_effectDistance_mDD6FA8522835D3FD3DDE79E9C5A61842FE8541AB,
	UILabel_set_effectDistance_mE3C1259F88612CE8D5E0164DA567E029D26A374D,
	UILabel_get_quadsPerCharacter_m458402E82E7957A321AEE0E86E0845B150311F5F,
	UILabel_get_shrinkToFit_mDE4EE110C1E0E25538F660A4F4C9E5C72C2A60B3,
	UILabel_set_shrinkToFit_m6DA253D0F72B4B2E5745E21DA0C274AE11566387,
	UILabel_get_processedText_mFDCDD3B1998962C592DD3FA6CEBE5E739F6CCECD,
	UILabel_get_printedSize_m53D88DF4EC0DBEC9F9DB364D0973C037DDD6C980,
	UILabel_get_localSize_mC499FA67CADDD2394C880BB0A63F15CD7F52CD0C,
	UILabel_get_isValid_mADF18ED9792FEE19DADAFDCB5C312630A527D3A3,
	UILabel_get_modifier_mE073C4C51C8D28B50C70CA0C7CD25C25DD6C30AC,
	UILabel_set_modifier_m31232967F05F7EEC6955C5DA8E15D63AA25B71FB,
	UILabel_OnInit_m10702A8FBD0AD3CC40D70CFB4325CFAD0677ADDB,
	UILabel_OnDisable_m9080321EF3A6753C2E6E0D1E0D35E4ED400074A4,
	UILabel_SetActiveFont_m48B2C2CB902EBF9A41FB0AB08D7EE1499B1823C5,
	UILabel_get_printedText_m1686A6ADD71E8B6AB98C6F1F31CCE14B1A43E34D,
	UILabel_OnFontChanged_mDDCC30B359E77AE600BF4CACD7395FFE4C439EE8,
	UILabel_GetSides_m8000A6BD42C770208288B9CB3EC0266476764672,
	UILabel_UpgradeFrom265_mDFD3CC8010B20A35A02813CA1F25ED4707D7121F,
	UILabel_OnAnchor_m63724935E7257F159C1BEFE875B33EA5AB220445,
	UILabel_ProcessAndRequest_m3B789AE5AC7D1FF051AFD9DCC64670B6C7D8EDB5,
	UILabel_OnEnable_m0E0A34FC734BED1B7A41C5FADA3D8921CC34E4A0,
	UILabel_OnStart_m441D9E57A34B524DB87B3BC877CD570E33581B2E,
	UILabel_MarkAsChanged_m5B64C88BADA2BEC46E7EE07CCE9B58EB23BFA125,
	UILabel_ProcessText_m82330948205E17A92811E9383C8EBC1391952F78,
	UILabel_MakePixelPerfect_m82A15F103B04464A2E76F4A9C0480DBE1329F4BD,
	UILabel_AssumeNaturalSize_mF1324FCD4B32348F66817766956FABD247BB4A2C,
	UILabel_GetCharacterIndex_mE7125B8911407156D42CF41544C66903727C2094,
	UILabel_GetCharacterIndex_m2865253F6EE6016BD1C784BFC1A73F13CD92849E,
	UILabel_GetCharacterIndexAtPosition_m32E03E672CB0C233CB3D0FFDCB9917259E347D7C,
	UILabel_GetCharacterIndexAtPosition_mCA1DC0D44FF43CC8E9A594E089AADBC381428E9F,
	UILabel_GetWordAtPosition_m4CEC10A264CAAD906BDE6E583B65E4AE799499ED,
	UILabel_GetWordAtPosition_m926FDEAFB751D695F2E67CD1716868705D38CED7,
	UILabel_GetWordAtCharacterIndex_m5600856D0F5E779F8F500B6132DF9A97D408D5D4,
	UILabel_GetUrlAtPosition_mAFE9227F267EDE243F5232A9277ADFEC327389D8,
	UILabel_GetUrlAtPosition_m7BB1DED8A33347D1664934BAA6B4757C392047D2,
	UILabel_GetUrlAtCharacterIndex_m8539B8DFAEA172C1A803CA32FC3FDF927FB5E6EC,
	UILabel_GetCharacterIndex_m5FF236BB03945FC9B37177D7745B76B056DDF8ED,
	UILabel_PrintOverlay_m836BCD2E57F9732957961FAAE0E1638C3068526A,
	UILabel_get_premultipliedAlphaShader_m5590D5A7ECC42ABE7F994E160779D3286F3C9D55,
	UILabel_get_packedFontShader_mFC17F1718AECD33B807321E866DE6DF7DA3E283A,
	UILabel_OnFill_m23EC55001D8447A7BEE6C39929AC289F9D42FAF3,
	UILabel_ApplyOffset_mD5795B9F6279BA5CFE2C3D8E172561F098511F1F,
	UILabel_ApplyShadow_mD4549F661AC02336ADD0D9168486D2CC318330FA,
	UILabel_CalculateOffsetToFit_mD5672BBEF542E0B468D34753606603D27B85A4CF,
	UILabel_SetCurrentProgress_m0290C82DE70EF6F8740001BA8F7C04838244EE56,
	UILabel_SetCurrentPercent_m200E3B12EB531978AA3804B07F779128C56F9018,
	UILabel_SetCurrentSelection_m7744138C63FF8E90AFFC02CC096A87EE41E5337B,
	UILabel_Wrap_m2B4F318D2634E38FB37BF54B979B22DFAECCA622,
	UILabel_Wrap_m69EABF139770D4897AE427794E439DE1E28B7796,
	UILabel_UpdateNGUIText_mA40E569AA3EDF6839F53A943ADA8E3F3F1E1E2CC,
	UILabel_OnApplicationPause_mF629177B1B9749A9F23944A040A562CDA24556DB,
	UILabel__ctor_m0FD77A6187149DCF9D74E727D91E04A2B7435091,
	UILabel__cctor_mBB14C2E315EDE3FE9A56E10702AB513E88DE125F,
	UILocalize_set_value_mCF4380A57C322974EB8E0D034778014600748863,
	UILocalize_OnEnable_m7EFE7B9DDCC0098830C2C2CE23ACF30AD2BB6249,
	UILocalize_Start_m1E49B9AB04E898FA798ADFE9315D03CD5ADCB9DD,
	UILocalize_OnLocalize_m8101CF98F513D77BD48546ED246036D182C5F740,
	UILocalize__ctor_mFA32627FA59A7E2FDED902CFF2EDD4A430697A3A,
	UIOrthoCamera_Start_m70DDFBD29B19561B0C8413843A7E5A8808013CA6,
	UIOrthoCamera_Update_mE09BB1BE105D76633BD46FE7D6557E55C873CFB6,
	UIOrthoCamera__ctor_mAF564418B2FD201C4D83317CFEDAE3A31F3D08F4,
	UIPanel_get_sortingLayerName_m8B0E3515CC9E31C35250D3FC256ADE8DD036F998,
	UIPanel_set_sortingLayerName_mA3D08DEDBCF5F89AD147123DD1BB0FAB49367619,
	UIPanel_get_nextUnusedDepth_m4228488B7CF6C6ABA52878FB2EA28F0E8096ACA2,
	UIPanel_get_canBeAnchored_m553923A05FF0606586FA5F2975208660FCD43BC6,
	UIPanel_get_alpha_mFA80442F426A556D87A9784CAA47E16FDCD2C5E2,
	UIPanel_set_alpha_m984DFAEB0F4BB7C23CB03397928B8A272718732A,
	UIPanel_get_depth_m88BC62787C7185453A1C286561040BE1C73E3484,
	UIPanel_set_depth_m79CAD98E1D9EB0C866884DC135175DEF5719FBFC,
	UIPanel_get_sortingOrder_mF60C35883A649F39CF0C6BA4AEF0053F2786202D,
	UIPanel_set_sortingOrder_m96AF7D91D0A41287F190279F1AD4A237C81ABD74,
	UIPanel_CompareFunc_m3C08EC37535CDB9CBF2AB38846E60E3A538A2637,
	UIPanel_get_width_m2C4C0BCB71C09CB23A73211AC0752F7DAEA972E8,
	UIPanel_get_height_m84B34BF13710F4356A8EF4DBACC176D9ABC7935F,
	UIPanel_get_halfPixelOffset_mF37839F26FE6AB9075A225579EAC9EDB4C8551F7,
	UIPanel_get_usedForUI_mF26D72FF5EDB90B83351FB46F6A6C4DC5F766B2B,
	UIPanel_get_drawCallOffset_m498BA3A16182DC147E3854F951E05CEFF539DD4A,
	UIPanel_get_clipping_mC43FFA658D783CC5DE9D670489685E2036F1AEE3,
	UIPanel_set_clipping_mBA3ED0F32C491F5848D5415E4B9260D7150B1CB9,
	UIPanel_get_parentPanel_m4283512358000B37774FFC21C25D3EEE88DD3FB8,
	UIPanel_get_clipCount_m482891B77676C1B48882E345E66C640DCE9798BF,
	UIPanel_get_hasClipping_m4FDD3EA5877D6F03761EA68800C7E7E8A024F18E,
	UIPanel_get_hasCumulativeClipping_m25F0B03722CCBB12D56CEBCE3F8048D09C84CC8E,
	UIPanel_get_clipsChildren_m8F8217EE112B3EFA28A76080A527C8CB8A101021,
	UIPanel_get_clipOffset_m4742A2DC94BBCE038453F9DCEA7EC65A9A86C336,
	UIPanel_set_clipOffset_mBD95718836342A55C6F3F7C512B10F9A2126C04D,
	UIPanel_InvalidateClipping_m5BBACD88E6E0502ECAABC49652870DCCE3E90DD2,
	UIPanel_get_clipTexture_m495A41C7760FA01113BD52B717506B6A6D59880B,
	UIPanel_set_clipTexture_mD1C16C42DCF33B9362597D1B12BA6B7211E449FB,
	UIPanel_get_clipRange_m23D7DDEAD0BB61337073F2BC739BE75431646368,
	UIPanel_set_clipRange_m50A1BC3CCC1FF3E77B7D2BC7F2197FE39CF78717,
	UIPanel_get_baseClipRegion_mEAEF9F90FB2DEAD0566C3EE09381AA4D29D4BA41,
	UIPanel_set_baseClipRegion_m5FB0796888890EAEE58EF88C795B366636C41E79,
	UIPanel_get_finalClipRegion_m4DBF5E39EFBC20B0209695636A2F3E430E29FA11,
	UIPanel_get_clipSoftness_m3108AC74F24FD406592CCDC3723C2B460CBF94F9,
	UIPanel_set_clipSoftness_mA25DF6A704E4FC98977B6AA8C7F07C15F6858988,
	UIPanel_get_localCorners_mE1EFA3EEA7997B9220B80144307FCF761131623F,
	UIPanel_get_worldCorners_m083821095477F607162A3974BE64B5C242F34F00,
	UIPanel_GetSides_mA1D0BEA4283D746C74E3BC01EF61E9EE4EDEFC8E,
	UIPanel_Invalidate_mF105304D3FB743D14403A94F3C5872AC91DDD651,
	UIPanel_CalculateFinalAlpha_mC772E36375B8D9BFC2F67498FA4D8B9CBA763441,
	UIPanel_SetRect_m0EB4AE4A724641F3E8D89982875465CF0B1BC07D,
	UIPanel_IsVisible_mBDD72A90BF87CE5A634815F21D82526CB23C7F5E,
	UIPanel_IsVisible_m6B9769623337CCB0307FBE83E034580B83C920B8,
	UIPanel_IsVisible_mD145F68E37F9214F882E98523F0A65953270C354,
	UIPanel_Affects_m73BA14034B7F33D499BDC38610F79C8856436F63,
	UIPanel_RebuildAllDrawCalls_m0A792521A131A1216228539581D9738CCB12F0E4,
	UIPanel_SetDirty_m5911794DE0E914B8E8634196644CE227ACC7A296,
	UIPanel_Awake_m2B5CCDAF5CBB25F396B1E61994F2EE20079AFD3C,
	UIPanel_FindParent_mC2A07AE1503FA975CC541FEEDECE10FED0D99F7F,
	UIPanel_ParentHasChanged_m94569D8424190D11BA9EE0D68019D1DA9626017C,
	UIPanel_OnStart_m4D72A0AEA70F86409861419382BC60B7322AC0F9,
	UIPanel_OnEnable_m551698B999EF662526E3AE549E30CE9F8981EA21,
	UIPanel_OnInit_mEC4A5A7BE5BCC782B689D513A52EAEEB8DEE66CA,
	UIPanel_OnDisable_m7D5F1A3952F2F11D27D4415846697BC77B727433,
	UIPanel_UpdateTransformMatrix_m44F5D5A9F7F688E2398D7701141CB6BB40DD86D2,
	UIPanel_OnAnchor_mA68366862152A7D98563B3490622D87CB89A66CF,
	UIPanel_LateUpdate_m4CB3805CC27F78BCBD646E1055753E0D5A78305B,
	UIPanel_UpdateSelf_m8DE5C71719B72C73F3B3A40E39684AFAFBF87976,
	UIPanel_SortWidgets_mA57FBDB50143F8D0BFC1CF08961DB96F15BEF892,
	UIPanel_FillAllDrawCalls_mADEAE89AF1D8989A6A5462569BDDF4B0A8C42B79,
	UIPanel_FillDrawCall_m6FA14A8A2EF4501D7DED1F26BD8FCB8178F6F4F0,
	UIPanel_FillDrawCall_m46968901118FFA9DB30E6E12CA6DC7FEC76FB9B0,
	UIPanel_UpdateDrawCalls_m20088FC7413030FA856B5952C21BFAF982347A1C,
	UIPanel_UpdateLayers_m57F19D6A36E4D29FE28BC573133D936399CEFE92,
	UIPanel_UpdateWidgets_m2471993CD370BA7C8696C57972571BD4C2B2DFB4,
	UIPanel_FindDrawCall_m4DA51D74681E513B85972FDBB95E857996CC4003,
	UIPanel_AddWidget_mCF202233DE7CEE30DBC4872B84041A37F896B78E,
	UIPanel_RemoveWidget_m40CB5A093558B799DF7986A3D9A7608307BAB24C,
	UIPanel_Refresh_mD2181152F37B7388E417D9AF3ABD652CC809D709,
	UIPanel_CalculateConstrainOffset_m124E27D6AE24E8D65AA8F0BC44AB75270E1D62D4,
	UIPanel_ConstrainTargetToBounds_m90265D32706FC4701E36DCE7E186DB92E0E2DBBC,
	UIPanel_ConstrainTargetToBounds_m9CB610609953CE96EB2F809D379A283B88C706AE,
	UIPanel_Find_m10E2FDD197287FC1639148980B27449105617179,
	UIPanel_Find_m8BB3DBA1209E71BAD27605B8379308EAE86A17AC,
	UIPanel_Find_mB7445A2AA23832506BCB1563B0ECF01C9AC45A23,
	UIPanel_GetWindowSize_m1EA9BC985171E34ACD89AF9E944AB21EC71067C3,
	UIPanel_GetViewSize_m824189FA54F8A5DB2B0D7F8CC6180C4975DBED23,
	UIPanel__ctor_m3BAC36854F0F03ED0344C22C2E018C450E4E9A97,
	UIPanel__cctor_mE999C4979E412E9F52DA893E5A92A4DB7E16C17D,
	UIRoot_get_constraint_mE04720CF42D6169961FC90454F14BA20C1862E8C,
	UIRoot_get_activeScaling_m5BED634E4655B6D5573E996B3760FFF62B85670F,
	UIRoot_get_activeHeight_m2C5F916BE4A224C08084F81491BB783AEB1D1B45,
	UIRoot_get_pixelSizeAdjustment_mDF35D2B941B25A2EC91A211A5D78C4E04DADECC3,
	UIRoot_GetPixelSizeAdjustment_m8C744D98FACB42F815DB458F8725B83CCFD3FB92,
	UIRoot_GetPixelSizeAdjustment_m4395C9A983CDE15D1186A9870D5D2D1C1212E0CD,
	UIRoot_Awake_m71C5FC9FE8D0F33923C0F04D4B4E2DD27737489E,
	UIRoot_OnEnable_mD80982A107124EA829909032E2F03EE345D7E7D7,
	UIRoot_OnDisable_m9ADC555C5B69FEDE4A66FE53856DA4C94FDF48A8,
	UIRoot_Start_m77FBEBFB0B0F6FF24D50173CB67C6489835C8CAA,
	UIRoot_Update_m7A26DDC2AB2DEDC22458119D5CFDC8F4E318DC07,
	UIRoot_UpdateScale_m81966BAAF56ECFDB0B53974085EBBCF301D313C5,
	UIRoot_Broadcast_mBBD4CDA3176E4B6945018AA12C5D62564C54EB99,
	UIRoot_Broadcast_mD7DDAEF575314F73624997017D2E8735AE0EBF77,
	UIRoot__ctor_m9CB2488CEDA6AAB980E9536281470B441708EEB8,
	UIRoot__cctor_m8368600793C8792CB53DD2915CB547DB34B56C24,
	UISprite_get_mainTexture_m3D041319076D28C2EB775CA5270AEC2FF55BC813,
	UISprite_set_mainTexture_m70096D175CA97F5D5C2D7D69765C10760441904F,
	UISprite_get_material_mEBEA1101E0231B68F6EFB56730B85D4EF66554FF,
	UISprite_set_material_m737821A629675F73CE8CF2A452F478E69C1905D1,
	UISprite_get_atlas_m184550E0493869A2B1DA7001318A029ABF4731A6,
	UISprite_set_atlas_m2E4D75239AFE7A57D161DF690F74CFD5A32DC8D1,
	UISprite_get_fixedAspect_mDE1608D6CACD31ECA9D2BF519CCFB0814537CB83,
	UISprite_set_fixedAspect_mAB937B623E7DAEC4C49C2003404D3CC660801E04,
	UISprite_GetSprite_mD603D0467F40E6D5A707925BDAB69E43FE44C9C8,
	UISprite_MarkAsChanged_mA9A52798F45DDF4BDED5423CC1FF25420BE67CDB,
	UISprite_get_spriteName_mE93043CE57A6A6E741A09A224133F17D647CF2BF,
	UISprite_set_spriteName_mFC4DAE1E73BAEE79EA2B8E91A7207F700C82DC49,
	UISprite_get_isValid_mB6FBF9E8408AA63D5B0FBDEB222791BB28DC9A3B,
	UISprite_get_fillCenter_m63E0868E2C649CA60005287E165B5F6DDC14B658,
	UISprite_set_fillCenter_m9369872265E934761ACE12AED6471D5568A00413,
	UISprite_get_applyGradient_mF1C3811235040880426FCADFDD1CB5A11C4541E9,
	UISprite_set_applyGradient_m4C151ED8EF160829A6AD5877B166857EFBC97C1F,
	UISprite_get_gradientTop_mE8DAEBFA68DAA3D1F33333961774FD2C08C03057,
	UISprite_set_gradientTop_m37B8A855993DA88F590478BDE369C69655E3C0DB,
	UISprite_get_gradientBottom_mE82D76F259E77C8D3713E2E71552713C3FDA9C7B,
	UISprite_set_gradientBottom_mB03786E52C807BCA04987BB00B473F8F45E3A7C5,
	UISprite_get_border_m147BD8A84448866E904BBEBEE58D925F3EBD65C4,
	UISprite_get_padding_m57EFE3DCCC4E818CC2451183BE4277EED46DB112,
	UISprite_get_pixelSize_m07298F7ED10135F3774EF6C8906F8B20A810FEF4,
	UISprite_get_minWidth_m07D45C7BDFD697D3DB689F09CFEF7DB03B3B1302,
	UISprite_get_minHeight_mBE846FF9FCE59AAA3B0A1F9F477154A4EC45583F,
	UISprite_get_drawingDimensions_m89BA1E4DF4969B785737D227B82C406BAD1CC64D,
	UISprite_get_premultipliedAlpha_mDE94D1C68BF997DA020001E053D91496F7561FF1,
	UISprite_GetAtlasSprite_mB292BF9A63EF7818987C7A720EED7C1E20135A84,
	UISprite_SetAtlasSprite_mA069E8AD2BAC97FEC8F355FFDDD741B927488232,
	UISprite_MakePixelPerfect_mB50E6CD8DFC3E31C970F808DA79894746F31CD29,
	UISprite_OnInit_m033D118E6906F024EDC10AEAF003BCFEB077DE47,
	UISprite_OnUpdate_m3A7AEB5EFAEC51D5D25680905EA6C372B7BFBE5A,
	UISprite_OnFill_m14C57CCD491F01BDB4F2EA7CD671F91366898147,
	UISprite__ctor_mA7126752BE57D55ABCA80281522ED1D5394718CA,
	UISpriteAnimation_get_frames_mDA0940CA772AA0C3F0A787A250ABC5F5C74497DA,
	UISpriteAnimation_get_framesPerSecond_m19B5091FC686501D1B8CE377367B4CDF5E57A945,
	UISpriteAnimation_set_framesPerSecond_mAF9DFC76130001D15FDC75E5CC458E51816D20CA,
	UISpriteAnimation_get_namePrefix_m16DEA930951E2F028881CA0D58E26B53D80EC95D,
	UISpriteAnimation_set_namePrefix_m446197DE95AF730F9350566C0C2EFB4292599643,
	UISpriteAnimation_get_loop_mEF8AF1ED52B3AD9152924BBEF96C291BF383B89B,
	UISpriteAnimation_set_loop_m24D8DD84396A3A7D7006E1C806B54A26B2578445,
	UISpriteAnimation_get_isPlaying_mDE4468AE08B65ED9D56EA8BD69E579C32B37C157,
	UISpriteAnimation_Start_m598603BDF435243A5FCCAC35CDC3E8FEB012C262,
	UISpriteAnimation_Update_m813E2428DD4A7A28F17AECB44C1155CEE8B7FFE4,
	UISpriteAnimation_RebuildSpriteList_mFAC99E1E01E10D583D217E72A6868DF72B6AD097,
	UISpriteAnimation_Play_m0505EDE6FE6CF5BF1974043CD66AED1CDAEF21F9,
	UISpriteAnimation_Pause_m32D1BD303DDFC3BA55F02828AF2A24EAD26B320A,
	UISpriteAnimation_ResetToBeginning_m8E3E5657A5B97E7175F73E8B32A662B94D73B112,
	UISpriteAnimation__ctor_m4BBC2AB1E88BAA1003303521D9EBC3DD9635446A,
	UISpriteCollection_get_mainTexture_m5F5B307A574A973FB82D67041676972FF9EEC3D6,
	UISpriteCollection_set_mainTexture_m1035AFDC8D4EB9CF9ABF0C978BDF64F9F45B4B4B,
	UISpriteCollection_get_material_mE22B7B6CB0AFD96A880A6C7C9F56C41820B624D4,
	UISpriteCollection_set_material_mF91756475404288BCC8D94733DB18153A6B9B02F,
	UISpriteCollection_get_atlas_m10F04A942D79B881722E1B9CA7E60E11C280000A,
	UISpriteCollection_set_atlas_mC0E89DDE8F278D71D2CF43D93826680C7B261BFC,
	UISpriteCollection_get_pixelSize_m6598AF8AE5BEDF8FEC9B9BBBFAFCF88EF20480C9,
	UISpriteCollection_get_premultipliedAlpha_m0662857555FD016B863E77952C1D4A4D18EED1CE,
	UISpriteCollection_get_border_m185E35067CFD5DE21E838EB086693115D931D100,
	UISpriteCollection_get_padding_m04F321702CA6B16DF4B05007BB3F9CEA2F126253,
	UISpriteCollection_OnFill_m9073ECA04973233C7042649680174B9D8081B03E,
	UISpriteCollection_Add_m1C02581B58AFA2106D87197F6A0B16F334A2E640,
	UISpriteCollection_Add_m04598866B1131788EAB5475D9AF9FDE21DC7FE5B,
	UISpriteCollection_AddSprite_m284615DFC84BCD672B16A32BF3ACBC276CA11D86,
	UISpriteCollection_GetSprite_mFAC7CA782F746A6BDBE36C8EC027A205FB08756E,
	UISpriteCollection_RemoveSprite_mFB776652CCA0ED5BA7BBDB948CC5AF57FF88EBDB,
	UISpriteCollection_SetSprite_m341A5C6AE9B8AEE35C63A06F06F43440C9DCBC56,
	UISpriteCollection_Clear_m87E2C53349B91BF73C4C85A9F87BEEF051FD5EEA,
	UISpriteCollection_IsActive_m721A1758AFDE9B634CAE46954B89AA0342C5E880,
	UISpriteCollection_SetActive_mEA74E57638B719B814EA7D13134AF4512A86A839,
	UISpriteCollection_SetPosition_m3CABF3849277B11FD253D1E1608B84133F331B8E,
	UISpriteCollection_Rotate_m6A49EF4C8992146E3173FD91CC513BA1B25EFB70,
	UISpriteCollection_GetCurrentSpriteID_mFA79298B0088215BEB86AB146990C99723AA5D19,
	UISpriteCollection_GetCurrentSprite_mA722FC8F08972C50B35B2D5ABE07203F441DF0AA,
	UISpriteCollection_GetCurrentSpriteID_mD218D330254F2D865A3A555EE0168B8D397AC224,
	UISpriteCollection_GetCurrentSprite_m287BB7326FE11A90A05E2BFC0B1D149E95E646A3,
	UISpriteCollection_OnClick_mE1110F983A1474F350152FA1771B8DE076AF021C,
	UISpriteCollection_OnPress_m3DE40024FFD4EE669272E99B071A492D5907B205,
	UISpriteCollection_OnHover_m040198DB3E428B88E87E996E0AA38D7B72FDD223,
	UISpriteCollection_OnMove_m1AE2745AD9CD0A6C2C2B3559818009A46BB6A670,
	UISpriteCollection_OnDrag_mB7E13344261C7FE90D3166554BEB0700252EAA62,
	UISpriteCollection_OnTooltip_m52C432516649D8FF33936349D066993499FF755B,
	UISpriteCollection__ctor_mFD54BD64FEB209E57C97A8893E58C7BFEE3153FC,
	UISpriteData_get_hasBorder_m51F33C69F9410C85FA68389DD21F252DEF6723F0,
	UISpriteData_get_hasPadding_m2DDF8609C0B953D985BD1C58CDC398B73C84E532,
	UISpriteData_SetRect_m7ED08A06179B5C3D7397DC4137F78F5CF8F78B18,
	UISpriteData_SetPadding_mD7468EE6B39EE752073E4D827D6A980D10F0D39F,
	UISpriteData_SetBorder_mEB9ECAE9901AC9A7C0099D95A74DE39652559EB3,
	UISpriteData_CopyFrom_mE9316CE56583F7B324773D098B9C39F6565EC633,
	UISpriteData_CopyBorderFrom_m762D33C3EAB441610C92E0A6742FF1040222A77F,
	UISpriteData__ctor_m3FDAC1BD995A3330C8241CD9F08CAAB8D22A143A,
	UIStretch_Awake_m09B8904BC1AC6170AE08D1343446942B3EEBBF2C,
	UIStretch_OnDestroy_m7C12480DECE7830BD8E05749F0947A03BC1BA143,
	UIStretch_ScreenSizeChanged_mBB2D2B9693996D80A66F36B98671CB4250680048,
	UIStretch_Start_m70DC603B8C39E0145AB11828BF30ADF29EA7CCBD,
	UIStretch_Update_m878DC723C6E0CD3BD70393CD83C21B812017E126,
	UIStretch__ctor_m6351DCF7999C024FA8E50F7010093A0C87BB247A,
	UITextList_get_paragraphs_m0F8B69C9D3B0B8A416E65DC97F08107056D11A46,
	UITextList_get_paragraphCount_m80D1677A6F84EF4F4128DE650FA67DF56A7D731E,
	UITextList_get_isValid_m81FD9AAD25F9686541B1C6268912D87950784950,
	UITextList_get_scrollValue_m771846E452FAE634C489EF1FE8560D30C29EE771,
	UITextList_set_scrollValue_m68B35880AA9C66296A1B02163B62A3A489D0544A,
	UITextList_get_lineHeight_m8FEF995939E6D669A8F29CE55022B9728F0D170C,
	UITextList_get_scrollHeight_m4BE3B56661728ED81D38C1FB418BD516CAF67665,
	UITextList_Clear_m9C621BA0AF1E487BD39C4319E9661C301CA0FB16,
	UITextList_Start_mED2DC84F02F15C7BCBE77BE1301BF9CE71AC3E0B,
	UITextList_Update_mFE6F0CCE27030CA5395B796A5583D825B9AB4CF2,
	UITextList_OnScroll_mBD0565FB942E8E51FAD89FF582A25BF1BCFDBBC8,
	UITextList_OnDrag_m30C03183BBABFD4D080B293A76CD4995048BE144,
	UITextList_OnScrollBar_m3BBC940FF135F0788FA42E8E9BD641C403CC2F7E,
	UITextList_Add_mAA1536F8E38B235AA07C517D4A1062F7A74E2C40,
	UITextList_Add_m0C4C17C34E385412BE33960F3791F5ADE5094D00,
	UITextList_Rebuild_m477E174D68A5163BBE071B25A82CAA9CBB8BA2C8,
	UITextList_UpdateVisibleText_m014D8C8010BA87078A8DD11B7B52003F32AF62A8,
	UITextList__ctor_m0C2A90D1FB97C9D8F844DC011F6B3D3E9F635C20,
	UITextList__cctor_m1BDF9CF007BDF802C40AC99786B4DAF9FF7A644B,
	UITexture_get_mainTexture_m60A9C473AC4ABC86C7722BC1179A8FF836B0C3A6,
	UITexture_set_mainTexture_mD74649557D861510C98E59F2EEF7279B90723139,
	UITexture_get_material_mBF294F6A34DC4FEF66B02058B6BABAEF750BF4B1,
	UITexture_set_material_m677F44573F4823B5004879711B483436E7981CEC,
	UITexture_get_shader_m63E6AB328838C0AFECF0344AD844C0A6CB602B70,
	UITexture_set_shader_m36FFD7EB879574EF1B38AF8C9A3D4D5C9AFD319A,
	UITexture_get_premultipliedAlpha_mA8E7ED176C6FAC20F3EA0473EACDDC12D9B80937,
	UITexture_get_border_mF8C72A438DEB7D884ECC59DA9B34473BB447B8C5,
	UITexture_set_border_m91F9085D5108D95250032B2E8353B9B6C6402B04,
	UITexture_get_uvRect_m7F3BCA169F73A8412B6A001FD2D7F9946002E9A2,
	UITexture_set_uvRect_mE03E2CAD1293C9A27EBC383680DBA7844DABD398,
	UITexture_get_drawingDimensions_mD91CAE0C08A7833C5D26B5AC91BC7595F14F753B,
	UITexture_get_fixedAspect_mA4FB66241AAB0E4AAC4798371E597C724BBFC04B,
	UITexture_set_fixedAspect_m16FD75339823A7F418784401FB846405014B0A47,
	UITexture_MakePixelPerfect_m2E36AD0DA05AD3DC2A01869C9A95FBE75BB2A25F,
	UITexture_OnUpdate_m91DDEEA6F412B4BBAF2FAD3F05052E74B785A170,
	UITexture_OnFill_mEAA8DC5A47BA17228DA9F9910DBC881DBD24B907,
	UITexture__ctor_m5EEE814B121EAD004ECD1E1C7428F83679C49CE6,
	UITooltip_get_isVisible_m5D13EAC61F8962AC8FE0B1D7CD5FD9D9B44522C0,
	UITooltip_Awake_m71BFFF8780774D5563373CF27A53BAF85D93B790,
	UITooltip_OnDestroy_m2EA963263DDEB3E65803D3D69EBC63657E2D583F,
	UITooltip_Start_m0A35671F047B575DF1E2B1F227651535456E4A1E,
	UITooltip_Update_mB97533A61865DA65951BFE6EAE28E2940D770AA5,
	UITooltip_SetAlpha_mECC9F41981FD54D7CFFFE9932BCBBF765CCF693E,
	UITooltip_SetText_m9DCD820C626A93D0DF79C1B08D60D0DF635BCF14,
	UITooltip_ShowText_mDB2E4E41098D91B42AF02C2511552C9D2EF99610,
	UITooltip_Show_mE65FE0130F61CF85C7B9459516C5AC8395433E9A,
	UITooltip_Hide_mB872271E189557243C31C35AF8FE1B31B397F6FA,
	UITooltip__ctor_m41B7B95F99CA96F957CE3410D94700D38811086D,
	UIViewport_Start_mF1861E94FE19BA89B59954D9AABF9E43AF658423,
	UIViewport_LateUpdate_mB14AC73F4DB39DEE49657B2DCEA20894C30AC715,
	UIViewport__ctor_mCCF31F5AB8FD8A9AD97BAE5D62FCF00BAAEAAA20,
	LevelSelectButton_set_Index_mFA433D2E2C5005CEBFA2E98E4FEC198776F7D5A9,
	LevelSelectButton_get_Index_m218D466ADB076E26786F17FF36617F7BF4266655,
	LevelSelectButton_set_Display_m9FCC9F23F5B5ED58798DF0993195E4B170DF0F1B,
	LevelSelectButton_get_Display_m474371F48E087162FCE84C8E8B5A3356AD768A56,
	LevelSelectButton_OnEnable_m107FEC7938F2C116384E296A75E175BCBF02650D,
	LevelSelectButton_OnClick_m86242D7664AAFEEAE16C9610CE8599112A5FC898,
	LevelSelectButton__ctor_m2D58CD5017CA9FB36E5C46974C4A93AC7785C390,
	LevelSelectController_Start_m54694E6C3BF24EB19172124AD8328CA17B601C44,
	LevelSelectController_Update_m4C5C9F31352EB949A8DF382788DC3A2F62D72326,
	LevelSelectController__ctor_m01AE3E8BB4D00AC6A03BA3FF8857573C625E6199,
	LevelSelectSceneButton_OnClick_m618B51B98B1C76C32AB6B417DDAEE7C8C11A4B0B,
	LevelSelectSceneButton__ctor_m10CB7AD7A1E34634A496B4DD1D237A0720E0B35A,
	LocalRigibodyContrains_OnEnable_mF57EDE2F34DCFC815A680D814A9CD88DE4D2C606,
	LocalRigibodyContrains_FixedUpdate_m44F2E42D054BA4649C82B2C4CFD6BA6EE33BC976,
	LocalRigibodyContrains__ctor_m23EFF4280AB545A9DA13AF78C12440ADE5D31744,
	TweenFill_Cache_mA0121CA36E619F148D80325EBDF3953735781EE0,
	TweenFill_get_value_m7504D991A9E99ECA97E0ECE569E337E48405F352,
	TweenFill_set_value_m99AE799715661A201E71DF86CCBCF877AA659504,
	TweenFill_OnUpdate_m4427F588542937C65876158A38F5EB4F9ADDDF1D,
	TweenFill_Begin_mEB043667B943AA6F2A0ACCF510C6A93B43C42815,
	TweenFill_SetStartToCurrentValue_m12B6CB42D16CB5EEF4A7FDD1AE72F9F0698F2E1C,
	TweenFill_SetEndToCurrentValue_mDDAE18D278C90B55ABA76FFBF90F7009E02F28A1,
	TweenFill__ctor_m5C85653649027D9CCF8EAFCD524519EC5AC7BD99,
	TweenLetters_OnEnable_mE9A7D0B8F715DD5A65B5BEC35A92623194398722,
	TweenLetters_OnDisable_m1B1BDCD34DD7106D5E0B37AC4F6273F19462B1F4,
	TweenLetters_Awake_mEBC262806D5218EBBDE989195B4067225DDC6A19,
	TweenLetters_Play_mE3925648F4457636A8300D178097CFB723C2EAF4,
	TweenLetters_OnPostFill_mA0EA809EDBFF62046242640A019706E8F9F636BE,
	TweenLetters_OnUpdate_mB9F6F98362CC36380CBC8444516E5E900D1E32B3,
	TweenLetters_SetLetterOrder_m29D224842F933A9D4C49B626BCD612DE06B3C692,
	TweenLetters_GetLetterDuration_m9CDAAA0F24685A1385F1152B177DBF89AD70F2E6,
	TweenLetters_ScaleRange_m67BD5AB67B74B7296BEE66F756E4C0A1890104F7,
	TweenLetters_GetCenter_mF07CA947D46F80865BE488C320BCB224CCF043E7,
	TweenLetters__ctor_m6C4829BF2BAFF9B7458ACE5BACE1CD68830596BB,
	Obstacle_Awake_mBB902A148BE45B6A355B48ED19D4C65FDAC26EB1,
	Obstacle_OnTriggerEnter_m9322D17FBF4BCF07FFC7B86FB37176062FC7000E,
	Obstacle__ctor_m2A721D4A6D3CB0FB812FC38AFCAE3E86AB2EB0F3,
	PlayButton_OnClick_m339AE19E1E7CA628D7E1487159251FC346AE0685,
	PlayButton__ctor_mA04C10665F6FA8B3448F21B9DA51D3D4FD061DCC,
	Player_OnEnable_m05D71B0DCF7B62A4A26ED38A594A01B0DA24491D,
	Player_OnDisable_m356D36D557966C8B7CD250CA88C8377E07DFC0F3,
	Player_Start_mD6E1D31879EB485356D1C22C8AE12C5DF6392E79,
	Player_GameOver_m767C12B8C73F0972668FF006B9F5F32F58C48C0C,
	Player_Aim_m772171A41B89B00261FE30EAB9FED3823E7B5C29,
	Player_Throw_m99E887F2E93A0B479AD66C6A4904C306D43B4B1A,
	Player_Victory_m0CEF76BA0E870DFB1174637268B96578E02B295A,
	Player__ctor_mAEC38956EFD0E61D848D4E5AFB83BABCE2DF1E23,
	Player_U3CThrowU3Eb__6_0_m25ED3C3DDBCA96E51ECBA78CAD8310437FA0630E,
	ResetButton_OnClick_m56149AE4BAC425E23E5F845E890450D07577C23C,
	ResetButton__ctor_mF261E21FFC39A4F0CD661F00D3065B46000EFC70,
	NativePlugin_Init_m834FC3A2C0628A771747799066A5B6479CB63449,
	NativePlugin__ctor_mDADE0D97B8DEF2DC3E5A69BEE12EC617590FB8CD,
	NULL,
	NULL,
	NULL,
	MonoBehaviourWithInit_InitIfNeeded_mD87E806432B0678BC3271AAE2A30A81FB8E02D67,
	MonoBehaviourWithInit_Init_m2FA54490F8D6CCA66B5BB499A86C8B0FA62F552A,
	MonoBehaviourWithInit_Awake_mB39B461D8E13B532241B5B2F2891EEE5C7E3DB82,
	MonoBehaviourWithInit__ctor_m23DF29E9AD6316303633B5C5930014E532103752,
	BotBanner_Start_m1B77C6E0B02FF557A4BEED45E86AE38D0B71E8C6,
	BotBanner__ctor_m1E8367E5C648927B33740F6780990DD7642FD2B1,
	CustomSpriteAnimation_Start_m9892D2E97C5666FDDE9BFE806FC77D1BDED3D3E7,
	CustomSpriteAnimation_UpdateSprite_mAA1BF42B6D961E26E56E01459C93F5FEBE02358F,
	CustomSpriteAnimation__ctor_m0AAE71E672F2EFCF504F8740CFDF8F6C35EE8981,
	CustomVibration_Do_m614A990483CEFC1E4E177D35B1883991F9D6E227,
	CustomVibration_Vibrate_mCE01100F8E76AEDCE289D1ED5C101F12CFD22E54,
	CustomVibration__UnityCustomVibrationDo_m50A46352AEC86B36B9DB4EDBA09A90FD137D0BD3,
	Invokable__ctor_mB11D0F76001F6FA63CFC37B69A878C7AB064A6AC,
	Invokable_Invoke_mC37A55CB0F6EA847BA352ADDE2C05565398F864A,
	Invokable_BeginInvoke_m5F0AF9EF6933AB311C2DCC1D497AC81E18008DCD,
	Invokable_EndInvoke_m31D1509F8F55555FA3E1BB614BB6DB605AE37D00,
	Invoker_get_Instance_m065424E02F689B03BB169D71313AA9C1EEEB1474,
	Invoker_RealDeltaTime_m2C69D4A6BAE20BB937E07774F392F323EE91F602,
	Invoker_InvokeDelayed_mFEAA28E3F2535E5A5953EE842E94B92303C667CA,
	Invoker_Update_m07A12E7764521D38773C1EC2F73E25DADF6183CD,
	Invoker__ctor_mA064ED7F000028CED895D501772878D4639A26F3,
	Invoker__cctor_m4DD36502AD42A199BD95FCD668243A4F01E0FA8D,
	LocalizeLabel_Awake_m8B051B760F46EA4100F335E515F089AB2AC7B4C8,
	LocalizeLabel__ctor_m44CD38EEE5E760683E8B45C847FF47BF563729BC,
	LocalizeObject_Start_m602D56650B85E7C06BCEC9CB64336A0D16F42975,
	LocalizeObject__ctor_m474FB769AE7D5388BEC984C9CFD7CB036CFFC107,
	TopBanner_OnEnable_mAD75C378377957CA35B5F9302D811B25E3526435,
	TopBanner__ctor_m927056BF8E4236F57177102B98D0250B0C46AF81,
	UserData_Save_mF50D1054306E1217881B58AD10E3D1BC4F919244,
	UserData_get_PlayCount_m9084A7560BAA8ED01CBF22B84AC91305809F7E3F,
	UserData_set_PlayCount_m12825D6588392035F195C4C7B8DE4823B9A4B6FC,
	UserData_add_OnChangeMuteSetting_m6CCB103D1AD411A532F89153C9E5EFD7DAA8723F,
	UserData_remove_OnChangeMuteSetting_m59B3112368ECF437FC4B4F12AC74C6330C4CF962,
	UserData_get_IsMute_m4E8D9B381B6D3FBD4159D768B6026D2BBA41ADC5,
	UserData_set_IsMute_m69DCC3A7B603DA633F2B5002838AF1F7C37F0F5A,
	UserData_get_IsVibrate_mC51FBFCF62ED184AC7891AB50987C630748ECAB9,
	UserData_set_IsVibrate_mF754D5DA9C3C113368CEB55B7963D5BA6D575975,
	UserData_get_Delay_m7985D9B99E44ED14E8170FE63683FCDCF80051E7,
	UserData_set_Delay_m2FEC62B1DC36B0643DC290D911DA2F2C1CDE1AEB,
	UserData_get_CurrentStage_m6162B02896A16936283ACE2B253AA6C6BC46DB36,
	UserData_set_CurrentStage_mEBB67746EFE7E9DC6811B5583305253BA59DAFE3,
	UserData_get_MaxStage_m0D450A2ED7DDE630E05999C77C4D23A9A50F2C48,
	UserData_get_RemainHint_m28A58C0BB67BB3169877C48D2EAE3410493CFE74,
	UserData_set_RemainHint_m8A2D9D05B96CBA824EBF207893899F605AF816BE,
	UserData_get_ShowHint_mBDABB7383A5954607C398F80F8541118DB90967F,
	UserData_set_ShowHint_m66BC3A4425A8CFFDB5D8AF5CB86B810341803DC7,
	UserData_get_LevelCount_m87C9051790F33251C2FACD49BDA12A884A7C2FF9,
	UserData_set_LevelCount_m899E07D2FE5F6C29963536BEAC8C245C442BE917,
	UserData__cctor_m93E2E4C822B112EBC31139D378E3BAA6CE02CE34,
	FPSUI_Awake_m34DDDC1B6737732D273EA5FD712D7A9F999080C0,
	FPSUI_CalcFPS_m647D3FF58D52B996EB8D7358EE84FAEE88B0CECE,
	FPSUI_UpdateGUI_mDE3561AEC546CE9D04C7F09B2ED8C284696B9C8E,
	FPSUI__ctor_m2AFD8FB94289459F304CBAD3ED644EED88AC33C2,
	TestUI_Awake_mBCD7230D8EAFDA0CA959B73B8827332FD9FF1404,
	TestUI_OnGUI_m88DB80442579D119BF20028EBDA93E4E25027E61,
	TestUI_UpdateGUI_mE172E2486CE50A20CEC0D24DE400661BC6A91ADE,
	TestUI__ctor_m91C91753026C4246F92C2935EA2FE8C84FB6D269,
	TestUI__cctor_m1182FDEED2E8FD135202AC0F246E2DAD005D6914,
	NULL,
	EnumExtensions_HasFlag_m136EA7634B6872FA1A825881D9C2F166899FD1A2,
	GameObjectExtension_CreateChild_m6D7AE667FE2133030DECDDFE820D43163AAAFA7F,
	GameObjectExtension_SetLayerInChildren_mEF3C52E3136945E5416691902378CF35042C41A0,
	GameObjectExtension_SetTagInChildren_m4E2369594AF1067D4733ADDCE702008D6D2A1443,
	GameObjectExtension_SetMaterialInChildren_m0BA98CA1415F985D4F4C6675F1170DF3D2D3FAD7,
	NULL,
	NULL,
	NULL,
	MonoBehaviorExtentsion_DelayMethod_mDB71FB3D7EECE09D6EC20C39B5CD9DC952B9A013,
	PhysicsExtentsion_RaycastAndDraw_m0AEE6566D56B05F2A0CB53CD79DBBEFCCB81B5F3,
	PhysicsExtentsion_RaycastAndDraw_mE7DD10DF99D4B5F1E7058B201B7517FA2021DEBB,
	Velocity2DTmp_get_AngularVelocity_m78E803D51D4A71BCB4CCC405E6B640E6FE6E60FF,
	Velocity2DTmp_get_Velocity_m180DBC13C01CFA72E1EAB1C3130E5BAD7FC048F4,
	Velocity2DTmp_Set_mA61B08CF34857359EBD6D160DE7C907940C0F867,
	Velocity2DTmp__ctor_m6D3084B8CDF26BFAA9459089FFACF6553565F726,
	Rigidbody2DExtension_Pause_m8532097402E3947F35D42A6DB616B3C58771EDB3,
	Rigidbody2DExtension_Resume_mE5DBAFC62C586B9A4BB98F092CD454AB6482A847,
	VelocityTmp_get_AngularVelocity_m714D2E8D69B5854B8244E86FD201E6530540DB81,
	VelocityTmp_get_Velocity_mF014723AB722CE3C31A252C028A110A7A0AD411A,
	VelocityTmp_Set_m4786208DAD58492E62817E57938FB2E7923852FE,
	VelocityTmp__ctor_m1861706A0901AC2EC5B88E031D64E734C3DF52D5,
	RigidbodyExtension_Pause_m0592A89689C72A66DC87997EF396C39EEA6B8180,
	RigidbodyExtension_Resume_m34307BB055FEBA75FC9D36FCB629CAF4B2044E09,
	StringExtensions_AnyEqual_mD5D42774908AB662084CE3F2BB13DCF0D07E203F,
	StringExtensions_ContainsFormatInFront_m76BF26EA2081A70C0B35C15597D191D61DC466DB,
	StringExtensions_IsNullOrEmpty_mDFAF484A60D1D39BFA2C5C34F1F5194DA72AAB7A,
	StringExtensions_Split_m13358513CE16BF6531B60CF301B4A5596079A2E0,
	StringExtensions_CountOf_mA610DB28E762851519E1A6119878F14ABA46FC76,
	StringExtensions_Coloring_mC0F0E14F3FC4159755F758E0C312428E87C26251,
	StringExtensions_Red_mA54228D5A10997D9703BA2F223B5546615223CE5,
	StringExtensions_Green_m61B7076135933BB375A3707264EF222849ACF234,
	StringExtensions_Blue_mA9816EC7052181765E4047CAC30DB422CDE6B4F2,
	StringExtensions_Resize_m850BA57A4B1CD7F8E225501A6ECEBE600F929205,
	StringExtensions_Medium_m04CBA4A1EC1135ECC4B28E9308BEC0B39C9750F5,
	StringExtensions_Small_m93AF9F92D74F3B5D85A540F6B714568195C99E3F,
	StringExtensions_Large_mA544BA64C71954B9851733BA1DA9656AD6BC1B88,
	StringExtensions_Bold_m5693C8A4689DB91E22B5F3D7B8E0C821047F6197,
	StringExtensions_Italic_m9FAC329D7FD0FAD6F2E048268AB6EB780BAB3341,
	TransformExtension_SetPosition_m83FB37CEF983A9BE36AD2CE05FCF3CE85F541FBE,
	TransformExtension_SetPositionX_mE31A1DE6E28D50748BC243DAD958AC2A8116805D,
	TransformExtension_SetPositionY_m1222F0103804C4AB55BD7EFD07CABE78C2D99E24,
	TransformExtension_SetPositionZ_m4B1BCE13FD5558427A34CEB5EB0354818B7E6DBB,
	TransformExtension_SetLocalPosition_m6A12F31B32BC38FC6BB5CAA8610C6FE67B9FC753,
	TransformExtension_SetLocalPositionX_m66C8CD40A500F95E71E70B5C48A1615FD89CB040,
	TransformExtension_SetLocalPositionY_m038FABF6B0EBBF1F5474B1D3F242726CCD4E56D9,
	TransformExtension_SetLocalPositionZ_mA0307684C139A2C97CACE2850899FB51DEB6845A,
	TransformExtension_AddPosition_mA39EC21BFCEA995BA62E02E97DAA10AF47C3599C,
	TransformExtension_AddPositionX_m5D56ED4885ECC4CBC9931E69BBEE8BFA6B00E857,
	TransformExtension_AddPositionY_m79E44B898E568297060C7C864F98B485A38BA830,
	TransformExtension_AddPositionZ_m3030520D768B6937F6479BF8F34F6C2A73D19D0E,
	TransformExtension_AddLocalPosition_m1B948BC531728E3D16A1C6E7CA0790C55DD935BF,
	TransformExtension_AddLocalPositionX_m893D1A4769967889605F30C2FA6381811F379981,
	TransformExtension_AddLocalPositionY_m6D19F5214742BFD844B8E090169B465DD1A5C5E1,
	TransformExtension_AddLocalPositionZ_m0F4A6B7078597090B18897E3977B5943731711EC,
	TransformExtension_SetLocalScale_mD1C487C6049133B846EBC872C5A2ABA785471B58,
	TransformExtension_SetLocalScaleX_m87DF307719F53E046967307B90C84AFBC43D6AAE,
	TransformExtension_SetLocalScaleY_m32567A004ADAC2FA5B860EAD650F3C0E651AB8A7,
	TransformExtension_SetLocalScaleZ_mAB963B72364938063C4C95CF34679593AE5B938F,
	TransformExtension_AddLocalScale_mAEAE8E7424D241D74CF83ADF1C0BA0642226521E,
	TransformExtension_AddLocalScaleX_m180D504D3B3BDE6E535D9789007F9ACA90DCEFDE,
	TransformExtension_AddLocalScaleY_m62B49C473DABC1473B0CC656C94268DF42A2289C,
	TransformExtension_AddLocalScaleZ_m1FA89ACC81E54F1FA65A560E8CB51FBFC49F47F1,
	TransformExtension_SetEulerAngles_m2D9EC0AC53E2A39056B165E0EDBC2CA82F3CF50B,
	TransformExtension_SetEulerAngleX_m35DA4B51A3DCBE51BA047576FD7465845A21E2B0,
	TransformExtension_SetEulerAngleY_mC3DA80139BCB2D25880035D0D13C6FF869AC3165,
	TransformExtension_SetEulerAngleZ_mC746B33B6C7EFFFEBD7C8865A3704F4F9966140C,
	TransformExtension_SetLocalEulerAngles_mDA4792113BB86902B19DB2366E241D557D807A88,
	TransformExtension_SetLocalEulerAngleX_m662E09406C6261E7DC9A0AE1AFB8A183DC414698,
	TransformExtension_SetLocalEulerAngleY_m21AF0AC8EDEC340BD4E727E25310E0EE1A2AA111,
	TransformExtension_SetLocalEulerAngleZ_mFEC7420B2CBAA54F212D648E3775DAFB577D8C2C,
	TransformExtension_AddEulerAngles_mBC93089A9F1B5FBD27E73AF1FE52200F8D36BD22,
	TransformExtension_AddEulerAngleX_mB11F61D067A44D5D9FD6BFBFAC17B98453EF720A,
	TransformExtension_AddEulerAngleY_m68C2B03A2BB7AD67CBBFCD643C83B45875016917,
	TransformExtension_AddEulerAngleZ_mE217FE50ED32F76A7439AA2AD27C5561EB923481,
	TransformExtension_AddLocalEulerAngles_m50717DCCF3774FD895D441310970490FE981370D,
	TransformExtension_AddLocalEulerAngleX_m08313B3D9DDDB611FBAE76018DE5EE64D93571EA,
	TransformExtension_AddLocalEulerAngleY_mFE99719E478A410AFE7E9E665B1E70EDDB9AEF0F,
	TransformExtension_AddLocalEulerAngleZ_m53C27E4F6D1F01082D077E12B02C664A47DD85BF,
	TransformExtension_LookAt2D_m087AE0C7AB8FE38744811C020EEC0B6FF59EBA66,
	TransformExtension_GetForwardRotation2D_m101850FA38DFCF6B06CB75F7528D515703278C1B,
	TransformExtension_GetForwardDiffPoint_mEFB8F5A7654F9A72F78ACA645F64BC21DA4360AA,
	TransformExtension_GetChildren_m7C87B4DF09731F62919DF948B417AC13654E13D1,
	AutoBGMSwitcher_Start_mC5A4C0E0D930855D59501C119D88B23D0E97AEAE,
	AutoBGMSwitcher_ChangeBGM_mE21E512DB1118D135981CB1F50FEFBB19AAB9786,
	AutoBGMSwitcher__ctor_m89462078CE9F75C7898225737381B397C8FF4E5C,
	AutoBGMSwitcher_U3CStartU3Eb__0_0_mB91DFA3426537EF400F61E6F9A38888B11A4B728,
	RecyclingObject_Init_m1E945321E9C0C019951450C3C6FF9F61E90E945F,
	RecyclingObject_Release_m9611130D3A8CC119BD37F4D3FBD5AFBC6ABD6812,
	RecyclingObject__ctor_m72427734E3C0BA7E476EC4BFA686ED86D00AA459,
	RecyclingObjectPool_Awake_mAC704289CFC2633A642D3F1811F33F58412CD418,
	RecyclingObjectPool_Get_mA95041130609E08F805B1072E9832DEB77736C05,
	RecyclingObjectPool_Release_mE8572A01712AA141618816A425C76ADA8BB9D958,
	RecyclingObjectPool__ctor_mCDAF33192E18D8D1611D885571CFC74313A670E1,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	BGMManager_Init_m450FCFFFE68A0A81E06E522078353B79C6149944,
	BGMManager_Update_mD053DA45781E1A77AB55D0B109B2B910CEEAFAC8,
	BGMManager_ChangeLoop_m3D0D6F50069120219F4567AF1112AEA148D289A8,
	BGMManager_PlayBGM_m928386725AA12754DD23AF811231D61F30F41368,
	BGMManager_StopBGM_mB6CB85931DA79658B2F2FDC4E358FB008C26584B,
	BGMManager_FadeOutBGM_mB4E46509624C3F7B6069DB02F055DB4002801B27,
	BGMManager_Pause_m7881455F72B01A98BB06F8719A312E21C6A0D02A,
	BGMManager_Resume_m5617B5846D6EB18248A7C99C631CCAB42E07F62E,
	BGMManager_SetIsMute_mF032470896D9B8A65C3C698F998C47C09ECB85A9,
	BGMManager_IsBGM_mFA6476938BA7507BE0ED9750382842887A86E7C3,
	BGMManager__ctor_m55484986188DF7C12CB4CEA0022127DDAE2EF718,
	SEManager_Init_mB6A359CA0A5551B552064055D236A71A73BFE10A,
	SEManager_PlaySE_mC6022D57E874B786730DBED62173D8D9070F5AAC,
	SEManager_PlayDelaySE_m8CD15D69BF32C7C6FFA758918C01A8EF6FE51377,
	SEManager_Play_m7405E293806F637C9BB01AF01D984B3DF608AA26,
	SEManager_StopSE_m51B10B9FAE939551A67B1AA5A492AC8D2DE0DAA8,
	SEManager_IsBGM_m89B8B8D61B94CC299777B5F5C63163CC2BFB936C,
	SEManager__ctor_m0B0B3BC62B20F730F107D8077DE8E4D599C2D05C,
	MainManager_CreateSelf_m51193E44C71285D3FAEB1BA5805266F4CE1CA134,
	MainManager_Init_m6E5EEBFA52A1B1047E4334CA6309466989978A21,
	MainManager_OnApplicationPause_m00338ED42A79459BDD5451E1616938FABD6E4DDE,
	MainManager_OnApplicationQuit_m49D5C2DFAE25C720454FBE9B356DC2F4C0928388,
	MainManager__ctor_m41EEBE8ED32EF698400725FD0344F93D3FC70117,
	SceneNavigator_get_BeforeSceneName_mD4E83345A2DCFAC65C9DF62B726C12F1D3E850A6,
	SceneNavigator_get_CurrentSceneName_mDBCFC17B3D4277AEAA573994DFADC19283F43A81,
	SceneNavigator_get_CurrentSceneNo_m38D993D52DB3CE8F86AB7783C61BECDBB7B849A1,
	SceneNavigator_add_OnLoadedScene_m5643662BB51F97DF2BF927497D9908E265EDDFC2,
	SceneNavigator_remove_OnLoadedScene_m60DC1657F4EB9813CDE4557ED533761292288EB6,
	SceneNavigator_Init_mFB97107DE30611B7228A836DF56AB344798BCCE9,
	SceneNavigator_AddScene_mAD3157F3EE53EB4404B4FBD0892E7AA9996C3BED,
	SceneNavigator_LoadScene_m5DE57772F6E2DCC337495BAF1A014F9B6F6C92EA,
	SceneNavigator_OnSceneLoaded_m6099A84DE5E26C1CE563FF58F1A1109D450EA516,
	SceneNavigator__ctor_mBCD4DF454720F3510C9C327E5A5F34FED30D4020,
	AdManager_add_RewardVideoComplete_m05A39C9D8E6709A71BAFBEC1A9A20DE4B1362F7D,
	AdManager_remove_RewardVideoComplete_mC4E26DCBBA9BC267486015E804A31B3D700C1EB2,
	AdManager_add_InterAdClose_m5CAE519E5750565E5171EA1B3D836DFF16850CDD,
	AdManager_remove_InterAdClose_m85260138B22E1DB1D3B396C89B9521C8F3BF3CC9,
	AdManager_add_RewardVideoLoaded_m199CB8109649EC2576E9C8A6FDE14E09E233DC58,
	AdManager_remove_RewardVideoLoaded_m4ECAA93BABD4160631DC8765D2E865590DCC37C6,
	AdManager_Init_mF961FAE54D806D83B6B4320DAAA450B614CD91DD,
	AdManager_Start_m7441282F4D11035BC4FA1D580553465DD9A85700,
	AdManager_OnInitNative_m0D10503429E68D076BB383AD826DC4CF84A02580,
	AdManager_ShowSplashAd_m861478C3F4328F75937186C97DE1C2D03128E05C,
	AdManager_ShowRewardVideo_m51A61E538B1F4390C274337F8F64B90E9DFC72CC,
	AdManager_CheckCanShowVideo_m5D29A69F04802F93D845539869D2D867597D1F4C,
	AdManager_HideAllAds_m152C3B7FF45FC3001EFC020788ADEAC3B0AFB053,
	AdManager_PauseSound_m9B04A4BA822470BC6F88CED03FB72BDAD843FEC9,
	AdManager_ResumeSound_mF4CFB17DCFE56AE5055C4B753851B4B19E1EAF69,
	AdManager_FinishedPlayingMovieAd_m14BD97831D2851AED3262B2BFFBE6D57F7B0B0C8,
	AdManager_FinishInterstial_m9652413A768F30AA66FE525E82611310C1A2F30D,
	AdManager_FinishLoadRewardVideo_m071431FD7FB35AE6E79F72B73A80A9ED9AB60D49,
	AdManager_CheatReward_m406D37B4A02FAAA85267F07CABA98ADF2E823947,
	AdManager_CheatInter_mE75DD3A3EC8554F9FEEE48DBA8855060A8C65113,
	AdManager__ctor_m90E4F3ECE5164648C36A623707AF2B9F67A9C6E6,
	AdManager__cctor_m4DFC56B7E5F3E5582B1DAE04280A04606712C903,
	AdPlugin_Init_m752585F910DEA9849D1421C38C50D60861FBDF8E,
	AdPlugin__AdPlugin_Init_m51DB5B47350C98AF44ED3D4EFB38276C495A39FC,
	AdPlugin_HideAllAds_mAE43C52316E5939E9A8D8C588494D123A280B9A4,
	AdPlugin__HideAllAds_m5AA7AAF9F9BBF32A4AF22290AB1205A630C6710C,
	AdPlugin_ShowSplashAd_m5E41A401B0730386C62378F7F2C4CCBD2E3FA23F,
	AdPlugin__ShowSplashAd_mF9C6034F9B9EB0AD4C88FAFDD6AA5C42356AFD7A,
	AdPlugin_ShowRewardVideo_m8293703092631CAA565E8FFDF9951CDE329B9A55,
	AdPlugin__ShowRewardVideo_m7C79A35A2C01CD0C2C85D080226D1741DCD215E0,
	AdPlugin_CanShowRewardVideo_mC32B73A1E4E1C171F3BA6D0951463E2B34BD1996,
	AdPlugin__CanShowRewardVideo_m930EC48A4E3C8820C3F67816CF8F303FAF769166,
	AdPlugin__ctor_m8BF0200AD18087C2F772D7A4C095B4392359F8F4,
	RewardButtonAssistant_Start_m6B52E7E49230C197E6F167A239673AA0C53A5B28,
	RewardButtonAssistant_OnEnable_mE786CC629D73E6F082E5D8DC4CF17F019B26C95E,
	RewardButtonAssistant_OnDisable_m6907A329E6BECA37A96AD02B934D2AB25B5F7765,
	RewardButtonAssistant_OnVideoLoaded_m406B548F6BC2DD5DF0ED1ECC1EDA0649D9CCBFD2,
	RewardButtonAssistant_EnableMe_mDE30B9A29CD6D00C00472A0C97E66307DCE43914,
	RewardButtonAssistant_ShowAdVideo_m7846FF95961363D10A9937515C645CC340FD46CD,
	RewardButtonAssistant_AdManager_RewardVideoComplete_m66A9D01BA20E1C34441B56E814856422908B896B,
	RewardButtonAssistant_OnVideoSuccess_mBD6D849463CD64900B4BC6A5A43DDB69D9EAFA86,
	RewardButtonAssistant__ctor_mCB845C4185E8175C819020669491DD6BE40C4D14,
	RoulletRewardVideoButton_OnVideoSuccess_m21DD6274EEC9828A7B860ABC66140EBC41AAA4DD,
	RoulletRewardVideoButton_OnClick_m31D7356A9F5CB3E4AFF615FD62891354E6AD94A7,
	RoulletRewardVideoButton__ctor_mC956F3E0D67F84A9D9BEA9187299C82E27961E59,
	CommonNativeManager_Init_m4F1720A646FA6254529A0700C3503506B636F19A,
	CommonNativeManager_ShowAppStoreInApp_m32E357CF07D9F9D26EADF6DE2147B733A2BF7ACA,
	CommonNativeManager_ShowReviewPopUp_m0A1D3FE957632FDDDB6432D33D7CD3A51645811A,
	CommonNativeManager_ReportAnalytic_mB8DE382F75D8A141BD6FF10F08F6835AADBCD83E,
	CommonNativeManager__ctor_m3CAFCAA4893552FA8247A901353F8BD4E3B72B46,
	CommonNativePlugin_Init_m75DC34CC89116BBE5F25313382B610F2119992C1,
	CommonNativePlugin__CommonNativePlugin_Init_m84A7F37CF254F180F61EC109AB3862EE93A557A1,
	CommonNativePlugin_ShowAppStoreInApp_m04D03ABF4209743369680874FEA6CF6D54ADE597,
	CommonNativePlugin__ShowAppStoreInApp_mE58B88797EA171279FDEF890C17A523012594F22,
	CommonNativePlugin_ReportToFlurry_mBC7F88974E3C5CECAA8394BBCCC3688DF6F0E4D1,
	CommonNativePlugin__ReportToFlurry_m7C34123BDD614B7E5A505107F5041B9B9080E89B,
	CommonNativePlugin_ShowReviewPopUp_m1B1020527635BCED7DD1069578A75D5734281783,
	CommonNativePlugin__ShowReviewPopUp_m7B78FF8E439D281303749EF2497E3CC0BBA757F8,
	CommonNativePlugin_ReportToFacebookAnalytic_mBEBF2E0E00C7DFCC229D0FDB59AB0A7F703A3995,
	CommonNativePlugin__ReportToFacebookAnalytic_m317C9D05D0CF41259C4C954D977634844A742CE5,
	CommonNativePlugin__ctor_m47C212DC119A8569ACBD531526CA8C364C20F793,
	RankingManager_Init_m5EC7449E02E2FD668EC94CF0BED922A9B3AA1C1F,
	RankingManager_ShowLeaderBoard_m01CBD294823CFC4B705EB70484867C38C17EE1CC,
	RankingManager_ReportScore_m6DE982F4E24A242E94A9F9E1D4FFE11793484713,
	RankingManager_ReportScore_mBA6A65A1C3675A2792DD155F2C7CD0D29187F841,
	RankingManager_ReportScore_m7CB52E0F8809B1DBBCEFDAFD6C2B34EF417ACFEF,
	RankingManager_ReportScore_m878612867B2CBEECC9001D478BCC062B1255520F,
	RankingManager_ReportScore_m68718056D6B52FC542389FAAF23FD4B01E3E981C,
	RankingManager__ctor_mAA4BC3F32513E377363347CF0D86ED281D2B7D6D,
	RankingPlugin_Init_m71B3B18FCE6FF1BF2F444DCDE4563FA66265C522,
	RankingPlugin__RankingPlugin_Init_m05DE1302F06520CF85C476ECB94904331CC89750,
	RankingPlugin_ShowLeaderBoard_mC9EC717598E6031E5B8A5492E3228C7761073C7A,
	RankingPlugin__ShowLeaderBoard_mF61F05FF2039421326C1FBAEE289B2286440E775,
	RankingPlugin_ReportScore_mAA3DD2B822D74A07D28810C44E28F0508BAB55CF,
	RankingPlugin__RankingReportIntScore_m0C0A66D870B44ED9501F64507BB3F8DC99CA449F,
	RankingPlugin_ReportScore_mF707F7E3C7C1B1B3654631BBF7D8C65A48F57366,
	RankingPlugin__RankingReportFloatScore_mE2F6825DE6D2E86185DFAC8CF7185268E0097B35,
	RankingPlugin__ctor_m0BD1FEF6C6E52B84860F01CFA24D9A08CA21D98C,
	ShareManager_Init_m88FB9B04E9C2C71B20FB00F6097EC315DC36553E,
	ShareManager_Tweet_m4C90723F79CAB083B4DF93F90410873EF6C28577,
	ShareManager__ctor_m4E0B6763E02CF8395BBD771B583B6E4E629B9A84,
	SharePlugin_Init_m82F2975B9F2826BA30249E6856BB7677798CFC95,
	SharePlugin__SharePlugin_Init_m3C0E3F1DE350C41B431D836E4B35D3DF94D3EF9F,
	SharePlugin_Tweet_m8683523EBD3F90847945287803C8B5088A9C4C34,
	SharePlugin_DoTweet_m3FBEA0D29070E7CDB3748553BCE68AC9EF8F35BD,
	SharePlugin__Tweet_m8EC2C79A2BF72AE8E45DE87857B7CF22F6C4D4BF,
	SharePlugin__ctor_m2E5CCF92E0F66658C93AA51A6E08D9C1D97A9E67,
	ButtonAssistant_add_onClick_m40CF358E70883C77DFD994919C04DA08C0F47512,
	ButtonAssistant_remove_onClick_m121E4E4909472103470168F90F27603445D03B02,
	ButtonAssistant_Awake_mB14F3471BE5F98CBC8A5D2E59E70D5CA21E46664,
	ButtonAssistant_OnClick_m0D587DBDAE914A810035623EAD70CCF7E4CA4A4D,
	ButtonAssistant_UpdateButtonSprite_m24A4C995BB242F5B968F2D3B50F1EDC3400C74AB,
	ButtonAssistant__ctor_mA7060CEBAE249832E13D59CC8AFC2209F18E3DA9,
	TweetButtonAssistant_OnClick_m8BA84509E43A9384F51CF8C242976A55F3169C37,
	TweetButtonAssistant__ctor_m61B87B3BCC260B0869B70AA8925659A4A60F0472,
	PauseButtonAssistant_OnApplicationPause_mE498C2AC669ED9C590CA5E0DE62ECE452C769CE3,
	PauseButtonAssistant_OnClick_mC21A4BC9B02C729DBFD3F6A0793F156DDFD50B86,
	PauseButtonAssistant_ChangeState_mB77734BDAC44524ECC3E8A36FB69273EA1675158,
	PauseButtonAssistant__ctor_m575F6CCBAB5C254579EFA52BC4A94E4A0F3DE7DF,
	ResumeButtonAssistant_OnClick_m69BAE0599B3E7138C0F37ECADF6F1464BCEF52B7,
	ResumeButtonAssistant_ChangeState_mA8CEC3EFFA8FFA86814182440AB28027868E615F,
	ResumeButtonAssistant__ctor_mF303399F7EC407A917A920325C94FA040B4FC6B7,
	RankingButtonAssistant_OnClick_m4375D04033BBC9FBE81CA7E4E69984EFB331BEBA,
	RankingButtonAssistant__ctor_mC3B5AE1679C102A3C14BA89DB6704A78420E88E2,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PlayerPrefsUtility_Save_mCC0F4EB58D72ABD71ED3124E85320709ACD2DC51,
	PlayerPrefsUtility_Save_mD435BE54E2B75AA200F1C3071BFD51B8E0E43EC0,
	PlayerPrefsUtility_Save_m2EAF032F789A9F05A01B8ED0026DE93505C75836,
	PlayerPrefsUtility_Save_mE65DA94E6B06E81229BBF1F38124463F145CA6C1,
	PlayerPrefsUtility_Save_mFE85F068E41FD93A0E75BD1C78C9DDFCD49325AE,
	PlayerPrefsUtility_Save_mC9977D97D73804DA695272755C3349DFFD2ED287,
	NULL,
	NULL,
	PlayerPrefsUtility_Load_mDB08B958BCC8BD3CE0625625AF75CC5B12E696B6,
	PlayerPrefsUtility_Load_m26A0CB1973CBA9973DF290AF5576BA646E4CDAD6,
	PlayerPrefsUtility_Load_m0950B0A040DEA9883581F29FFA914E4E882550D1,
	PlayerPrefsUtility_Load_m4B5374CF26650422842F925AC37DFBF0DDD54F3A,
	PlayerPrefsUtility_Load_m02514577227259CDB7912131390159CB07B23C25,
	PlayerPrefsUtility_Load_mB5409CA68E682FF1DD1C1C2302C7A2A6DE6A8B25,
	NULL,
	NULL,
	NULL,
	NULL,
	ProbabilityCalclator_DetectFromPercent_mA2655B6ACA216B916B91E48F150CE8BD46C4F88F,
	ProbabilityCalclator_DetectFromPercent_mFE89B9DEA2F4D3BCC0165B04FDC3F7FEC998C4FA,
	NULL,
	NULL,
	StringUtility_GenerateRandomString_m353019E51CE6D26BBAF1EF250847C96CE03759EF,
	VectorCalculator_GetAim_m6FDA6754D9110834AF6457A9A02BF51EC0F45B8C,
	VectorCalculator_GetDirection_m6F98E62F7CBADC50BBB4AC0B4933B2E83AAAA7D8,
	SettingButton_OnClick_m360D5192348E45D50012C8113E70DD53C8B8AA7A,
	SettingButton__ctor_m617BAB41C2DD80C41049556A7E1F2F0200F59CD3,
	SoundButton_OnEnable_m83EC81C2B67F80EB276590EAD4FBCDC1F880AFC5,
	SoundButton_OnClick_m345BA9DC10C775DE0DDCFE2F2924015FE44C9BD2,
	SoundButton_UpdateInfo_m441B83D172CA5B3757AC3EC12F0DAE7ECEAF6896,
	SoundButton__ctor_m2CAD2082286F7CDA27208642B996B689FC1C4D10,
	Stage_6_OnEnable_m4ADE4BAE059A229E3633678B3EB496B70AB14CE1,
	Stage_6_Start_m7AEAFB0F490DF49AE6E4F350D5AC639DC1979882,
	Stage_6_OnDisable_mBB91AB738038C7C9E7625391A72872C108642DC8,
	Stage_6_OnDead_m44156106909541F6E841444674550EEB52E9051A,
	Stage_6__ctor_m644C1B73571989265B59EF6D4C23C54583631DEA,
	Stage_6_U3CStartU3Eb__5_0_mDC1F889EB9636C94E065E01A8F7B9754ECFC9BDF,
	Stage_6_U3CStartU3Eb__5_1_mD65ACB18D198A8446F33FBAADCA06F66327C56CE,
	Stage_6_U3CStartU3Eb__5_2_m4E3A9159F65D3815E69D3E5498E8012038883BB3,
	StopGrenadeSurface_OnCollisionEnter_mF1B8AD819A895F1C6B2BA2E3A4E5AF242F15434D,
	StopGrenadeSurface__ctor_mD9549A2BD48A7B62CDD8669C4D8780EF2ED3AB7B,
	TouchController_OnEnable_m0B31690352B996A4D7F7D60E9FF9FCF228A44174,
	TouchController_Update_mBB75A99965FF13B43A29007142FBBD550EC55990,
	TouchController_VelocityByTime_mD0F5962545DF34D0E55C88177E1658820C3CADEC,
	TouchController_Fire_m428BF15D21666D0DBEB72A963916A2568A59D1C2,
	TouchController_Visualize_mDC1612FC7358DD030CD4781FACC6277EE66D4D7A,
	TouchController_PositionByTime_mEE55B92FCFB2C558889CEF66DA411FCAA292A1CF,
	TouchController__ctor_m4FB7729B61339E9534D4D3AA5465226613198DE2,
	TrajectoryController_set_MainPlayer_m7B412AE702CEA4E39548262B710E2DB4CF1F4D2F,
	TrajectoryController_get_MainPlayer_m09A12046B5EE7C46592DB72569208A3EEDBBE3D7,
	TrajectoryController_Start_m0716D4EEBE0AE1C79E26F127FD26099AE51CFBBC,
	TrajectoryController_Update_m2DEB584A824C9B5DA19448AC712FC857D33A06D2,
	TrajectoryController_OnDrawGizmos_mEDEACE692FAA06E14B283CC677BB28E14E85CDEF,
	TrajectoryController_CalculatePosition_mC036B0675163DF03F2941A2CF3E4EC0A8399AFBF,
	TrajectoryController_HideAll_mF29D6A279581E317FBE3B41561078AF2A14C4EB0,
	TrajectoryController_OnDestroy_m8C0060BC6F2B545ED5F06BD034B0B51EF09A3ED6,
	TrajectoryController__ctor_m626C3CCA0CCFE4633CB804D1283547F56255E16F,
	TrajectoryController_U3CUpdateU3Eb__19_0_mFBDB4B19F7026B0339E5C3C77C4CD1351B273D80,
	UIController_OnEnable_mEF53A9A85AC08648D9F21E06D4C1AF2B258CAB43,
	UIController_GameClear_m87512BC0B3FCD16B7A6838CF0A90C10F0068D969,
	UIController_GameOver_m4F35441A3CC06BAEA4CAA9B614BA0B99D14B80D9,
	UIController_HideHelp_m7FCE2186067E8F3DD336D042243E2F404BF342CE,
	UIController_Setting_m4F2FAF331D55C0C12C65B86106D1B002762F858F,
	UIController_StartGame_m057AFC20C0578F33E5186141A21ABC2BBE9735CE,
	UIController_SuggestHint_mF0D74CD2EE48CACF2BB0ED0F7C78EE742506E784,
	UIController__ctor_m2957FB6041970C1305D4DED3E11D4D800E932486,
	VibrateButton_OnEnable_m10576CB0C3058E578CAA0B7DE9FAF598AF5F16D6,
	VibrateButton_OnClick_m441B5540B433E875A4DDEDDBD6F6682997E25836,
	VibrateButton_UpdateInfo_m6D2F4DE2E70B9D8F6BA41BC2D4A2BFAF06857A05,
	VibrateButton__ctor_m04B7E701E9F25DB7AA112DD6ED3121A863CB5999,
	GeneratePathExample_Start_m33E81F38570A4476EA56587B91E27E52010F6240,
	GeneratePathExample__ctor_mD301C9244761369007D9A7CC88168DA0EC470FDB,
	PathFollower_Start_mF5C8D3A86E2DF1B5AF058BAD3BA109436ECBB681,
	PathFollower_Update_mDDBD15F8E2A7B55909B7540B7A3309EBA1102DE7,
	PathFollower_OnPathChanged_m3E49C7A1539AC8E88899A326BE2975E358ED6B81,
	PathFollower__ctor_m92B719D410345E2E4368798F8B926561FB03424F,
	PathPlacer_Generate_m1163867C1C742B7FEB8F377B5B22E7FFDDAA505D,
	PathPlacer_DestroyObjects_m9C583B7ABEF0D665FCD90D051E15303DA52889C5,
	PathPlacer_PathUpdated_mDA41722E8489289DCFF4A0E75CD3F3534342BAE1,
	PathPlacer__ctor_m671F8C1373871C8336B655668BFFBB6C043814D8,
	PathSceneTool_add_onDestroyed_m96232BE24CD7BF011934247D2A70773909D80FE2,
	PathSceneTool_remove_onDestroyed_m05DD0890CFDCCB829C08E9954E6764F5CCAA7222,
	PathSceneTool_get_path_m654C337CF364E561B69A6DDF02E75BCF15F647FC,
	PathSceneTool_TriggerUpdate_mF6F19868131FAC4A94E872A3EC34EE15FFE91370,
	PathSceneTool_OnDestroy_mC955A014DAE41434CFCC3DD7054181CF9E84D893,
	NULL,
	PathSceneTool__ctor_mD66847B179189CE0B41F75B937E8737C3ED16078,
	PathSpawner_Start_mE2711EFC506EF8EFE60DD3060D4145A76773C728,
	PathSpawner__ctor_m271409087DB690262AC03C6DFFF75C1CF7394DD4,
	RoadMeshCreator_PathUpdated_m6BA373E2024F3C480E697865D52920E26B5E720D,
	RoadMeshCreator_CreateRoadMesh_m9588792AE7B57BB9351F5E95201CB9748092FEA2,
	RoadMeshCreator_AssignMeshComponents_mA1D24634810691912B694BCD0FB3FAE12AB17961,
	RoadMeshCreator_AssignMaterials_m6CA20AAE8C5770D7EC4C1E30F2E82C5F0BC09A22,
	RoadMeshCreator__ctor_m3C0BAF7BA1CA92CF453EA0D32239A55F1F09F16E,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	BakeSkinManager__ctor_m0FF21ED366527648B2811404A5CEE18808D7C389,
	BakeSkinManager_CreateBakeObject_m2A689D5BC3B68E1B1BF91DC856B9E0ECF32042E0,
	BakeSkinManager_Clear_m16B9F2F23407D889E8D1EB4FB58ABBE3C2C75988,
	Contour__ctor_mB54F69A1133A23F122DA1ED8D73E14FCD60C509E,
	Contour_AllocateBuffers_mE772213E6C7DD98226F8C8049FD037C1CCB87BF4,
	Contour_get_MidPointsCount_m0181117FD61E2F23F9E27E5170500D466A1605BE,
	Contour_set_MidPointsCount_mFBE0E72FE4ADB59BF7D491AFE2E97F7507C9794B,
	Contour_AddTriangle_m2458C4C0FE6D9882694B1A6056F0FE789022DAFC,
	Contour_FindContours_m323DBD951E1B266B8154AC87279E4D3B0D5C8DFF,
	Core_Initialize_m1DAE4B9229DAB8C268565D9AF1099A2BF676E6EF,
	Core_Enqueue_m38D974EB4852168CB1C9A21A157507994E460D21,
	Core_ExplodeCracked_m55AC9CA4A83510D25316DDFE67CB8AA0EC0C5222,
	Core_ExplodePartial_mCB444F1740283C4E5F97B2BCEA480C82EDAED992,
	Core_IsCracked_mEE10CD0BCCEDCACB03EF07FF853CCCCB92F2A840,
	Core_StartExplosionFromQueue_mA4F86A547177B6B8897F33D4F6D6BFAF9E26FFFF,
	Core_Update_m7B4B031EE9FE0331B89EC974D8853CA3C6FBCC81,
	Core_CancelAllTask_mDDB5FFB6BCBA6B23A479A92C8C357080B69DEC37,
	Core_OnDestroy_mF74B5C4D4B2B586B26451B0743490E8C8D7F2529,
	Core_PreAllocateBuffers_m264622206FE5D389AD14691D23173E124832407B,
	Core_RunTask_mBD50AD18A5E4F05089D2B58EF51DAC3CDE6AF472,
	Core_InitTask_m794E0640B6D0A6A5D7F2AB53A9C6460D33574B80,
	Core_NextTask_m569E1F528D2E8DFEEA3C3057017A28650B5542F9,
	Core__ctor_m22E711443292DB500F0B1FE7B1F4DCACCB99D4A6,
	CrackManager__ctor_m684876B2999AA885334097A429AF781233CD4446,
	CrackManager_Create_mC2E6054EDD767DE4B03CA218A4F2C4937EEDB189,
	CrackManager_Explode_m28B62F13F8A563C236388047714D67E0A1333140,
	CrackManager_ExplodePartial_mFC91E031E2967CF123A24D4DC8DE8DDBB3562830,
	CrackManager_ExplodeAll_m635BA81BFE0ED1445451BC1ADFF90929F2115E90,
	CrackManager_IsCracked_m5E11D1CB94A763E0EC119823361A39D70450A011,
	CrackedObject__ctor_m5AEDBAC223501FD240B841DBEAE0DDA5E1704A98,
	CrackedObject_CalculateFractureGrid_m08D4CB61BAC3D9DD810F107076CDA5DA5714331C,
	CrackedObject_Explode_m057B1BC7B6CC4EAAD48A6AA3D9C7F022DEF2A452,
	CrackedObject_ExplodePartial_m5985B8A3B66270C071CD144E1C1AA0A12AEE7B83,
	CuttingPlane__ctor_mED401832EE1B6BA1075FCB5C6064105A6882CBEA,
	CuttingPlane_GetRandomPlane_m67D5161DA8F561E0EDD06A8EFA7EAF1F0DB0A336,
	CuttingPlane_GetRectangularRegularPlane_m2A86A9FFEB578A8923C01E7EB9E0F96D562E4F71,
	CuttingPlane_GetRectangularRandom_mCCD394D782B6A243FDFFA0C13DBB813462BCBA62,
	CuttingPlane_GetPlane_mC312E9FF398030419C90A744863EB6E78D4809AF,
	CuttingPlane__cctor_m213F4606C0706B0F31DBBE9D8F12434C78464193,
	ExploderParams__ctor_m2E22653C5FE6777F863E53EEC15177ABCC73B852,
	ExploderQueue__ctor_m17C8D0DD5A5D1735936E81CAE7E48CADE461912F,
	ExploderQueue_Enqueue_m24B29B587A1854D22EDBB12FE91297E9015AB7C7,
	ExploderQueue_ProcessQueue_m7B3FC2973801603C5147619128A13E8AC793103C,
	ExploderQueue_OnExplosionFinished_m90CBD6A147DBBCCECF8E5780EE6E495A1926C409,
	ExploderTransform__ctor_mE70677404E77066F287D912C619AD3DF39B5A962_AdjustorThunk,
	ExploderTransform_InverseTransformDirection_m37F38A988869BD7FA675EA2168E1F6605AA10F2C_AdjustorThunk,
	ExploderTransform_InverseTransformPoint_m1CE5B70D6FD357645A7857BB55CC36AAAFBE0DAC_AdjustorThunk,
	ExploderTransform_TransformPoint_mABAF7F90A9BF3FB58C35C31055BC9D4C12A8F1BB_AdjustorThunk,
	FractureGrid__ctor_m56CDF579CF3A380D2925F2DD3C6E66D0093C14BC,
	FractureGrid_CreateGrid_mD775F49E1138C4F45B11C426E6F999B9EDB1A06C,
	LSHash__ctor_mB54462B78C6B42CF88DC2E46EB76014859A7BAAC,
	LSHash_Capacity_mA118C5596EA6BBC22B9C0B05046AD96053D99372,
	LSHash_Clear_mD40787DFB6AB6DD8207C3147462676B8A5F9F725,
	LSHash_Hash_mE9B89C27B4B576A3ACF0B644E72AB1E5E8F27E10,
	LSHash_Hash_m2C361D2405D5E097ADD784AB58B615DF4BBF4D67,
	ExploderMesh__ctor_m1E2D794BC31768ABCE73E709F2C38A45956F562A,
	ExploderMesh__ctor_mCF3939BC197F1FA87CD78AD1A517C7709B8BD0AD,
	ExploderMesh_CalculateCentroid_m38C69F41D9A7DD9D61B3060FB50D87C85C710878,
	ExploderMesh_ToUnityMesh_m839E57AB3377A97140E3A39BD217C79012725F93,
	MeshCutter_Init_m96905CF2C45ECF40EAB57270046DE1B09ADEF405,
	MeshCutter_AllocateBuffers_m5944F2D0A8E700FCEE0FB19F79879FD7C0DA7E3C,
	MeshCutter_AllocateContours_mD684BF50A9B724DB0CFBF30BC686F068C4427E75,
	MeshCutter_Cut_mDCCC5E2A934D2FB3EAD5D041CBDBAE63E27E2681,
	MeshCutter_Cut_m88A7DFB745A547FAE698132D8FC67343F2E522F4,
	MeshCutter_AddIntersectionPoint_m4472E919F5B05A8A3C8B30B177CCD5A9CE4F8314,
	MeshCutter_AddTrianglePoint_m47228BF3465CCEDC769A20C069783ECB669AFF94,
	MeshCutter_Triangulate_mC9A087AD7166FC265EFC492C553A9A0007834729,
	MeshCutter__ctor_m078D65F8D03587274C773640BAEE10711B1F2571,
	MeshUtils_ComputeBarycentricCoordinates_m52C97158C55BA1D3D8351DEF57D0B7302E3CA2E8,
	NULL,
	MeshUtils_CenterPivot_mD223D3CDBD77F6FED2C29A126E7FC96F2904E7F7,
	MeshUtils_IsolateMeshIslands_m382BF4550E5B08E26662E786E18107F0EB0E9729,
	MeshUtils_GeneratePolygonCollider_mDC0CDD5097CEEC4D279FC15909934C5E6AC8AFB8,
	Plane_get_Pnt_mB60B5F3DC896CE74D9A45A177B3F852E2178AB61,
	Plane_set_Pnt_m8CE1D78982BEB3B3EFBE613D75132FBFEC012DAA,
	Plane__ctor_m625C227EAD5B55A774442EF428838B43F68227C3,
	Plane__ctor_m9A92F4BC9197AAF04A6DEA2ECF489BECFF2C3B7F,
	Plane_Set_m248258201152ED0D73542B1D8F1DF09622DE6ED2,
	Plane__ctor_m7175825CDFDC81EA7173DB851AA3ADD81E1FA14D,
	Plane_ClassifyPoint_mADA0108859B4F02A53997998E8CADDB7BCA9607A,
	Plane_GetSide_m6328009A0CB757629E0F17C72363B72D6D17DD81,
	Plane_Flip_m629F94A064024762433C6B9A5BD2D7CA5066F64F,
	Plane_GetSideFix_mC0B9AFAEE6544EC2E4A943B4A5D65526266A73CE,
	Plane_SameSide_mF1A583C15918F23255E4BE82D5EB6873236897D2,
	Plane_IntersectSegment_m702F634D06CEC840354318783BC2FEE0C5A146DE,
	Plane_InverseTransform_m04B38EE7A991CB85A0596B9E67CA439E0FD190BE,
	Plane_GetPlaneMatrix_m3484CD98D35E28CA9060DE868853FAE66734A5E7,
	Polygon__ctor_m9A02B61C0E83A1CE4123D739C76ECF66B3C3B114,
	Polygon_GetArea_mF677989AA3C4FB8EA62E0EAF2C096992B3F9FF7A,
	Polygon_IsPointInside_mB3562288D01C8724D3BF05C09D2D5A69342E24C1,
	Polygon_IsPolygonInside_m4B303AAA43DA79442561BCD2E4A6038BEBB68ABF,
	Polygon_AddHole_m9ACFDA7612CAD668C6C9C127D4E8C7C8C67DA6DA,
	Polygon_Triangulate_m6230258DAF7F491F1EC76AD87CAD28F42CE9E641,
	Polygon_Snip_mC7A12C1CDC71CA5E288207D1289E005F2ACD1A99,
	Polygon_InsideTriangle_m409A9EEA4EFA701E75988166CE89BB8589F7BAAF,
	CutterMT__ctor_m963A98AE01EA4417B075BDFAC16F72810B1531FF,
	CutterMT_get_Type_m421E85B0F08A857335C64E2FE378A767FF74AE6F,
	CutterMT_Init_mEBD78F6413381AC3A132B73D0D5856C0ECC27A5F,
	CutterMT_OnDestroy_m49D8F35E5F9D6C949E71ED069393B0A790F33787,
	CutterMT_Run_mA3E0B33089469BBD3786EC4A24952F5DD04FCC5F,
	CutterMT_Cut_mC8197BEB1C04FBB3EDADBAD1B240B13F73D89DA0,
	CutterMT_SplitMeshTargetFragments_m3244B35297BB775F37BE7E8FB9D464BE0BD6F70C,
	CutterST__ctor_m0D9327BDEA7AC62B8C551EE624774A2A16C0F4BA,
	CutterST_get_Type_m9753F45DF212E545436C84E902B350D579B22FC0,
	CutterST_Init_m96BEB95D92AAD64BF98C78CAA020D8A4CFD26400,
	CutterST_Run_mAE220084394D85E22732B308C7AFC07CF94051C4,
	CutterST_Cut_m878CE177718BEE6B91946CF8757206E475F31CE7,
	CutterST_CutSingleMesh_m6C11344CF399847B2D12C9E64A45A0786AED6759,
	CutterWorker__ctor_m7AB996453D078CE3C2FE94E67D249058E29D5F1C,
	CutterWorker_Init_m1105A78935E37053D0646159C46B38858670AD12,
	CutterWorker_AddMesh_mBFB7D391060DCA20245167E88BA50C8FED7326A8,
	CutterWorker_Run_m8DB48906DE953A636251CD7B75FD60118801D6C5,
	CutterWorker_ThreadRun_mF197658B28AF9D32A12DC474789FFDA276BC612E,
	CutterWorker_IsFinished_mDA81D254387BCC29F4ACAA8D48A58AC4C8824350,
	CutterWorker_GetResults_m483CAA3EDE2FD5D9E7A033E838924633D6CDDCAD,
	CutterWorker_Terminate_mAE9663CE4A0AC87B4F5B1AF8679494AC57CBA990,
	CutterWorker_Cut_m5C325E45427F1300436B614172F2F94662963B6C,
	NULL,
	ExploderTask_get_Watch_m0B3FB3B18D66793F1A1D48D73AB3A06A83D09E22,
	ExploderTask_set_Watch_m774B069B2B228C32FCF1AA96706911612AFE2D25,
	ExploderTask__ctor_mFBB7E6D1F263E5815CE12B67C0FDD9CFDCBDB0DA,
	ExploderTask_OnDestroy_mAA767EC1F6FCBDC818AECB8745DDE09A8259B13F,
	ExploderTask_Init_m0DA4C90EE76FBA81EE9EC2C4C7DDA8CED1C95CD3,
	NULL,
	IsolateMeshIslands__ctor_m7D5538928C87F76D341EBB9F41F2BA9159A7ED77,
	IsolateMeshIslands_get_Type_m627DAF038079F3BA73634BE217C80DE125DD23D2,
	IsolateMeshIslands_Init_m8E9F7D12205BFD8EFCCD81A6CA61CAC62C1AED93,
	IsolateMeshIslands_Run_m13B29B51E320D1A8B01BB549EC934F5F8593D19C,
	Postprocess__ctor_m17DFEE2CF55119AE7A48DFC16DCD2DF31E882FE8,
	Postprocess_Init_mE2DC4C6ADFB926FA3269EF64D6FA32F3E4EA0073,
	PostprocessCrack__ctor_m15D40761606245F19C05D8B0D6989931082D80CA,
	PostprocessCrack_get_Type_m5E349A40669C58F902DFD2A43CFE3FB6546C93EE,
	PostprocessCrack_Init_mDB86180298E66FA145BDCCE1EBC1E7A48D7C9802,
	PostprocessCrack_Run_mE99C82AC1CA2A7076BEFDED26B1A3365476CBEC4,
	PostprocessExplode__ctor_m815EECBE5AE9C3DA343B62D1B82F74FB2A1E86A6,
	PostprocessExplode_get_Type_m5AC9CE1717C57406B641C0A6CD387D321177679E,
	PostprocessExplode_Init_m0CFCA543BFA3A8E513A48FDE297E9AFB7E23F0AF,
	PostprocessExplode_Run_m60EDB519CF9850DD44A07A59F233A753C57A166F,
	Preprocess__ctor_m2393F22580300D8961C50E83A616A79F3A1CEE36,
	Preprocess_get_Type_m87173F799F513932D16D7AFDDD27974647F56902,
	Preprocess_Init_mD74091CE5904094E62CF68124588E53C4E5AA52F,
	Preprocess_Run_mE934322675F36278C0D00A311628D74787419216,
	Preprocess_GetMeshList_m1EB0F01EB94D3B9978AA9E16F27D8CF7DA0D49C2,
	Preprocess_GetMeshData_mFDEC6971A32C976116122811A1F02804001DCCE8,
	Preprocess_GetDistanceRatio_mAD2AB165B27C564F9C164B7DABA192BE2C22E0C2,
	Preprocess_IsInRadius_mCBE4A74FBFFB9AD0408BFD5E7669DBFCDD270A55,
	Example_ExplodeObject_mCDCC83F218BA728716D35DDABD4323E520962609,
	Example_OnExplosion_mD8108136A3B47725BE36F26541F1E1D8B482BE8F,
	Example_CrackAndExplodeObject_m10B56B43BFF93D54FC8372E87F86F3726E68EC4C,
	Example_OnCracked_m83D0C297B76C893C0AC1112E104EE7EC8B190A0E,
	Example__ctor_mC50CE02978AB3B380FD62EA80B5CD0C0D422E08D,
	Explodable__ctor_m4EEF0EF87126911DEF42EE993490C077E10393CE,
	ExploderObject_ExplodeRadius_m8377614BE3E83E6834F85955836612FEDA76DBFE,
	ExploderObject_ExplodeRadius_mA9D19359E28E75C6D69AD48F8BFD86CCD86C32E5,
	ExploderObject_ExplodeObject_m54A678079E30F09445489770A4691B2463874674,
	ExploderObject_ExplodeObject_mE224D2619D90181CA1092A5814C41CBF75A73CEA,
	ExploderObject_ExplodeObjects_m3BFADF6724906BB10304FAA6B9AFD0B757E50E5B,
	ExploderObject_ExplodeObjects_m55DCF314EAFD1C2650A9642D4201E73C85F6F329,
	ExploderObject_CrackRadius_m0A5300E8AC970DCD7DCE438222E97E4ED561E2A1,
	ExploderObject_CrackRadius_m8E04AC1FAFACFCA83F81C3DB23C79AC5491F02FC,
	ExploderObject_CrackObject_mC27C49FAD2E7B530E5BCF34B42E27C01978C02A2,
	ExploderObject_CancelAllTask_m65D0009D3050EA519DE2F412943BEA23CA21B9D6,
	ExploderObject_CrackObject_mF25670967E76BCFC1FE23D7BA6C74E349E5DEB8E,
	ExploderObject_CanCrack_mD965912C2AA71CFE7659AF9A26BD1CE921AADA70,
	ExploderObject_IsCracked_m2329CAF68BB0C4F8EC5B8486BBB23EBE9D3A210A,
	ExploderObject_ExplodeCracked_mB1C821C3EBA584342E91E6A6ED57AB06DF155419,
	ExploderObject_ExplodeCracked_m067857D3FDEE7546570EAA77B55A3D0F907C42E4,
	ExploderObject_ExplodeCracked_mB8911FCE5676C75E87237D685146842DFC7C32F3,
	ExploderObject_ExplodeCracked_m0408F1E1E88BAD6552221CF797B03CEC6F4A63F6,
	ExploderObject_get_ProcessingFrames_mF004BD476E892CC60BC7DF4A4A289CBD09197BED,
	ExploderObject_Awake_m0495B68A4E8112D44807AD33B2AF9026F7949021,
	ExploderObject_OnDrawGizmos_m45CF06CE6A632332F7DEF9483FB15BC6B917398D,
	ExploderObject_IsExplodable_m34C584D4963224DB0A2801E13390429179B565AC,
	ExploderObject__ctor_m275ED4751077A435EBA08B881910625DBD83B673,
	ExploderObject__cctor_mC5C613468A3D7BB9B7FEA542E2DB7EC19C349F95,
	ExploderOption_DuplicateSettings_mD27D5745057597B8F10CA7C25F11AEC9160C7204,
	ExploderOption__ctor_m444B77E8F5A1FBC486F4358181057A5E4162B1EF,
	Fragment_IsSleeping_m84B9BD16F0BB76792279B5609000A801B725908F,
	Fragment_Sleep_m65401A0704C7DB2A28A874487522AF3C39123243,
	Fragment_WakeUp_mB1563B48D347DDE3CBA7F90069B74A8733E35E69,
	Fragment_SetConstraints_m57E541573901D3412A6AA39EC106661AAD03593C,
	Fragment_InitSFX_mF12ADCC4771CB2E3AF156CC9651DF4D8E4B0281F,
	Fragment_OnCollisionEnter_mBB2E12C45A726E3B75BE6A2334BC6B6088B50F53,
	Fragment_DisableColliders_m1821C54FA4F2F2B7DF92675005C8896681880EC3,
	Fragment_ApplyExplosion_m897E95DD9520AABC3BBB0CAC8129CC633E303ED8,
	Fragment_ApplyExplosion2D_m360276A020D25C31C4A299B5CD7FFCD6B9FBF915,
	Fragment_RefreshComponentsCache_m18EA177FDB3D68FA53F540A1298E9ECB4B505CD3,
	Fragment_Explode_m1ABE086C4928A809C0FDEDADA4DC30AAC7BAE130,
	Fragment_Emit_mB9327EAB3B07BC9BC4373B293ACB381E35AC60DA,
	Fragment_Deactivate_mDEB018EA2E4B2280D9C3E38F912EA996BD602264,
	Fragment_AssignMesh_mF60C7DDB26BDCACE4F89B37838306E3C33EDB5F1,
	Fragment_Start_mE9C3443F60F5E381927992740B511CBDAC9FE68F,
	Fragment_Update_m72D9AB10EAC7D6BBB66614FEEC8C1EEE3604C756,
	Fragment__ctor_mC05EFB167CDA2AD5B52F6937AB5B4202B5CBF20D,
	FragmentDeactivation_Clone_mC7B7E902AE7ABD588E65643F0E99FEF5FF6F2E58,
	FragmentDeactivation__ctor_mCA95CCED4097B517D26A7FA7D2AFC437010E7705,
	FragmentOption_Clone_mD27F4C44D05099F4868BA73EDDFE5A4AC9A67637,
	FragmentOption__ctor_m1E9DAE5E719A7EA03E64768CD0FBC25568EB1855,
	FragmentPool_get_Instance_m871D98BC9F81FB9A3C2654B870A5C195549A3013,
	FragmentPool_Awake_m64B4671E6FDA95E5EDB180796FCDC7B5336782C2,
	FragmentPool_OnDestroy_mBF7ACD2D2125762DD3568D549B99D3453B8B2A68,
	FragmentPool_get_PoolSize_m552AA5CFCC63B2CB48AE648288875B3FD247B8D4,
	FragmentPool_get_Pool_mD0FE15C837EC6198393F82CE344A5779A70B90A5,
	FragmentPool_GetAvailableFragments_mAB3DC746694CD18A6353FE4EDCDD3B6F9EE4B483,
	FragmentPool_GetAvailableCrackFragmentsCount_m31B21359BFA14F4B6BD38A9B6612AA9B5CF3A5E2,
	FragmentPool_Reset_m84A3451852DA7A76627D751136A6EE268D4A73C0,
	FragmentPool_Allocate_m5978FD666CFE30A93D0C58F4E92E8378BA3E7BFD,
	FragmentPool_ResetTransform_mA051CBD46E5680CEB2C3BCB6637409D21526062B,
	FragmentPool_WakeUp_m5CABB8C62BA3D11E43179D47887A01BB198BC74F,
	FragmentPool_Sleep_mF02DF6F82ACB581CC745A4EEF46F1F0632B3DE5F,
	FragmentPool_DestroyFragments_mD6C79FDE74DF5B5D414BD66AE3B606522F33E51B,
	FragmentPool_DeactivateFragments_mD2361F34200B601DD2F74A108481A73EE1325030,
	FragmentPool_SetExplodableFragments_mEEF4B9BC1A3E22B54868DFC933F483BD27797CD5,
	FragmentPool_SetFragmentPhysicsOptions_m8FF53EE76F32CC0DE62467A8B9E42E1D5FBCCDC6,
	FragmentPool_SetSFXOptions_m7078F342FE560FBEDB8A97124C543F10D84A4ACC,
	FragmentPool_GetActiveFragments_m0040CF9F36EAD54711D8FBEFF3AFCEF89B5F16DA,
	FragmentPool__ctor_mFD8F087142683429A1534FFF38F9DEF3AE23B72F,
	FragmentSFX_Clone_mDB3AC3056D0EC385159148609720B36914A068F4,
	FragmentSFX__ctor_m7A2193DD3E5DD1D4BB65CF47E1EAAE8D432E660C,
	Profiler_Start_mB3BC2A788C005E584BEB6864901872861B4C37CC,
	Profiler_End_m41AC035DD1BAFE7035074653B9348041B492AF1B,
	Profiler_PrintResults_m7B468DE3DC58AF1FCEB01313EF4FD320CFC88674,
	Profiler__cctor_m2322DBC43D80F22B646972AF623680E3FEEBA25D,
	ExploderSlowMotion_Start_m217503D43C9ED7934E30E19AFB1CDAD7C5A57BA9,
	ExploderSlowMotion_EnableSlowMotion_m4ABE874E9B93EB25BFCACC0C67A49FB55B37D575,
	ExploderSlowMotion_Update_mDFE2C72F08982165AA8BB4C4A63556F70DEA8F83,
	ExploderSlowMotion__ctor_mF441190BE9AAC1F9B8A6D47083A6029A20685D6D,
	ExploderUtils_Assert_m8A483F9CB0E41BC0961CB40412B6FA857FD2BEF4,
	ExploderUtils_Warning_mF093B7437424240190315BFA8F754392B3B9C0C1,
	ExploderUtils_Log_m98B8FA99E4B5DA6FE4C6F95A0468C87ACACE6558,
	ExploderUtils_GetCentroid_m4525C6AD9A2BB6FD5AB389115070F19688D83AA2,
	ExploderUtils_SetVisible_m26C6BE5DECF6F5ED7DCA09D61CBD857536B6CC20,
	ExploderUtils_ClearLog_m418A1E5A0F949CEDC5B85CBABFDDD6BFAE5327A8,
	ExploderUtils_IsActive_m09E14F26545A2599C741DC9BA2C07C427EA59F1D,
	ExploderUtils_SetActive_mCFB931F57899BA4A9F293072DC67D1F2126C5172,
	ExploderUtils_SetActiveRecursively_m0E5C699028464B5507956E35770E1278AA0593EF,
	ExploderUtils_EnableCollider_m9ADCBF691DED339B9113C5C8A65EDF9A136F3C4E,
	ExploderUtils_IsExplodable_m9B258F32595B53914CE5A2C6E3078519D0CA8FF0,
	ExploderUtils_CopyAudioSource_m6691A9B56CB28D01BBC23CDF6AD74A25CABBCE8A,
	HUDFPS_Start_mF45685E1E1AC1A9483B947CA093A4D125260863A,
	HUDFPS_Update_m2DC0F5B021EDF57961D475CCDE156A8A2D1B4F07,
	HUDFPS__ctor_m957962CC48BDAB08BDDBBDF20CF6D8665DC2427A,
	NULL,
	NULL,
	NULL,
	NULL,
	Compatibility_SetVisible_m72D79136EA6471352CD703E7848311C6C4F114DD,
	Compatibility_IsActive_m6782908777C963562C314E9834F67DE473D135F8,
	Compatibility_SetActive_m2EFE07A69DEA51243F9BEAE05E43E2402EDDD8CB,
	Compatibility_SetActiveRecursively_mE0496CD67DAEA29ADAB45A359E15830ECA745D75,
	Compatibility_EnableCollider_mFE4E17EA67D69DD1B25B6AF5527D279D9CEC459A,
	Compatibility_Destroy_mC7CF6F34038A05E159D914DB8E90B13454E1D1BD,
	Compatibility_SetCursorVisible_mD25E90D4B4359A3D39EB1481FD5912677CCD79EF,
	Compatibility_LockCursor_mDE290ADB68B43F8D86688B437451971CB6687E46,
	Compatibility_IsCursorLocked_m96B2E64FA41AB7005375CBAD2976CCFC95F61B96,
	ExploderSingleton_Awake_mB806511E73E9AEBC9A8A74AA42B135CED81FECC6,
	ExploderSingleton_OnDestroy_mFFBC592DA2B72AA24386E4A2D15A18BF4426BD0F,
	ExploderSingleton__ctor_mB7A7BACD3FF71D8813DD28F2F8C52F22555513DD,
	Hull2D_Sort_m18D98D9C7EB2EBB97F87B2AD5981C528270FE0F1,
	Hull2D_DumpArray_mEBAAB3FC2319B752C9EF69EA38B07A780BBE6A50,
	Hull2D_ChainHull2D_m902184602E491F769D3C69D05C558A30534A7AF3,
	Hull2D_Hull2DCross_mA8C821334677A4CA0EF67057260A1E7242D0F5D5,
	Hull2D__ctor_m41958C53347B028DF3A73AA861BCC34884D08820,
	ExplodeAllObjects_Start_m0CE76A1C538589FCB14E86CC499DBADDBBE4C893,
	ExplodeAllObjects_Update_mB3F1B21EB42D39F74A4FDD93561B4B6965326A95,
	ExplodeAllObjects_ExplodeObject_mB9F2B8FC3218B6B6333E8C8A25C15D90D513E35A,
	ExplodeAllObjects_OnGUI_mBD4DEF693CD36647CD7B0F2D2092C196884D7553,
	ExplodeAllObjects__ctor_mE7F0D290A0FA8859B48FE275E0B7BCFC1D65F667,
	DemoClickExplode_Start_mD1A5DF5BC7BC1D17A1084BC25D9FD675CEECF1A9,
	DemoClickExplode_IsExplodable_mCCBE387D974BAC7F02E94C9511C62738971C0A1E,
	DemoClickExplode_Update_m5D2121F3DACEB04CC5B792ADE5EE33D3E405DA01,
	DemoClickExplode_ExplodeObject_m01374A55580974FB4EA27AC357FC6B673663BE47,
	DemoClickExplode_OnExplosion_m08F2AE7FD25340B598B9F9A3CA93B01C001BF1A1,
	DemoClickExplode_OnCracked_mDB5118326BE95BE2154FF567261C7D0BF3CA7D95,
	DemoClickExplode_ExplodeAfterCrack_mB300315C2811B43BD347C000D4C8583A7B7001ED,
	DemoClickExplode_OnGUI_mFD5DEFCF3BAC5A2FA3A3000B3D09E466666A4065,
	DemoClickExplode__ctor_m371B50B4A24FA4D8FBE40FE656327D7177F37DB1,
	QuickStartDemo_Start_m4C798C3B41FDE31C5083F332E1970EFF7188F2C8,
	QuickStartDemo_OnGUI_m40638D5AD7F9E58FB3B95390911DB849172D9CB1,
	QuickStartDemo__ctor_mEB9EF5069600257892792D4741BC441BBFDEC8E8,
	CursorLocking_Awake_m10C5F85B9D127684595E592CC96C31D33182E368,
	CursorLocking_Update_m0DF67061FF2AB6CA78D78020C055863A4CA885CC,
	CursorLocking_Lock_mA94D788BE6FC8238DB0FE3BE611A939FE474B7BF,
	CursorLocking_Unlock_m49C671D632E0922D07E56D2E7E3233FA613709D3,
	CursorLocking__ctor_m8D5B839F8892A6131D21E7F1973B7155717F5A68,
	ExploderMouseLook_Update_mFBE281C514E4D17E52C9971547821D796CC6FC43,
	ExploderMouseLook_Start_mAF91BA5736819892F1E8341C6F7B51DEF460AC8E,
	ExploderMouseLook_Kick_m552F042726AA72FEAD11A5ACA2A1B23E6A601941,
	ExploderMouseLook__ctor_m25A52A377C0FBD674947884889CDA3BC0C78CD73,
	GrenadeController_Start_m90A2D0222065D669BFAADEE9B97F190F0BA9FA5D,
	GrenadeController_Update_mF5974D7867ED1AFEF0E2EC225A8FB7D750878ABA,
	GrenadeController__ctor_m3CDCBC3641BED6D9BA0FB61C9F4B5F328C3D3E9E,
	GrenadeObject_Throw_m044E7301F0E84A4F8E24FE4CFE74D310D4ABE5BB,
	GrenadeObject_Explode_mC4080121FC895AE9A7B7D8531FC054AFEA867915,
	GrenadeObject_OnExplode_m78CC3F411D5AEFA5BD7871DA669830A33666C03E,
	GrenadeObject_OnCollisionEnter_m0CECE17E3B6795C01D32634F9439AB68BBBA1D7C,
	GrenadeObject_Update_m2D533282BFFE980B48628261C1BC25048ECF3C9A,
	GrenadeObject__ctor_m6A085FE2DD79922104B75970B39727E3D9751D98,
	MoveController_Start_m156E1705F1146E2FD2A900111D895446A318AA08,
	MoveController_Update_m9275B01F85911FBC7B8F7640FBE32D1A4D68F8EB,
	MoveController__ctor_mEE62C0DE7E6A2EB006FE7F22407D2892BFB1790F,
	PanelChairBomb_Use_m607C9A8BFBCDEDCD601C55E91F2B64437CF611EA,
	PanelChairBomb_OnExplode_mFAB47304A96F1D85EF2987D85A3A7AC13AD45A15,
	PanelChairBomb_Update_m4B3F3281AEA8E80045397A3C4AFD08791AB6C9F1,
	PanelChairBomb__ctor_mCA0A2D2974EBFCBDFADEF2297BAE310F2A074094,
	PanelResetScene_Start_m147F9E0BB949C24A5FF07EAAB6D994ACE00AEB5C,
	PanelResetScene_Use_m12BE94CBD1F4977ED43D863D0507344FDF52E87D,
	PanelResetScene_Update_m996A084F342D27F3E3FB130829A824988F4D0825,
	PanelResetScene__ctor_m0C87A7D3B9534EDE4F6BEC0690A933CE19FC48BF,
	PanelThrowObject_Start_m502B935CDFCB0A2BCAF2EA166E5F12DC7DFFAA9A,
	PanelThrowObject_Use_m746CEE2D1E0605C0B2AF5E4E6C215EA156F642E3,
	PanelThrowObject__ctor_m2357FDE29086085E93BF559E7A3349F01EB2D5C7,
	RPGController_Start_mC22579F4815E27852AADDE1FA87D42DBA857F6B2,
	RPGController_OnActivate_m3E50B5EB9CC003A318E8F6C8FC42194E1976017F,
	RPGController_OnRocketHit_m06C5C3D73CD8F137013F910FA56E29154E2B1208,
	RPGController_Update_m09E8A6350A21EBED3891937BFD1618244F1DC021,
	RPGController__ctor_m7F94AD23A8BD946666FAF576AA097472F05326CB,
	RobotScript_Start_m61AF31A9F5D63A71CEF0E293683ABC0B12525C0E,
	RobotScript_Update_mE6FA045BAEB555BD11E1FEB8177610EEFD148B60,
	RobotScript_FixedUpdate_m2B53B1924FA505FB3A81B06A19AF177C00EE137A,
	RobotScript__ctor_m7815DBB60B89AEA8437C535502A3064027277D13,
	Rocket_Start_m15880B95E91E9AE11D39F00336D0F6D16B90B6C3,
	Rocket_OnActivate_mDB10875EE1B0E234B573104401DE6A7BAF960DC3,
	Rocket_Reset_m66B2B461A61975964BE3F8765D449F0DDC655B9E,
	Rocket_Launch_mF2DE288A855CBDA73D6EAF5BBA432DFC4442EE53,
	Rocket_Update_m263D1B7B104D96B6ACFB9DFC6DBFA392CBC32955,
	Rocket__ctor_mC96A95AE036DA1B503DFAB2899386963A3B32F6B,
	ShotgunController_OnActivate_mBC4643A0F3C195949A9DD43241C0F0A974FB06E3,
	ShotgunController_Update_m33CF3675D687D06813985A022D26E3B51E1C86BC,
	ShotgunController__ctor_m49D5288DA3C2EC4EDABE42034EE8F10FED04ED04,
	TargetManager_get_Instance_mB256826199FF95DD62E579601B9E703D4C8DB674,
	TargetManager_Awake_m0CCF1698B5F0BFBDDDD4E6E6AB02A48D181E4016,
	TargetManager_Start_m84672F318469F1F948DE38F816D6676E18212988,
	TargetManager_Update_mBE35DAC34D59FD616A77DD3B4F874EB34617BCFC,
	TargetManager_IsDestroyableObject_m47C263CDB6AD74AB1FD2602194268E8429009C90,
	TargetManager_IsUseObject_mD7D0B2ED3EEF96637132DE5C46E1A75D62385BDB,
	TargetManager__ctor_m1FA55A041C705454D72C267DBA615F08F88E573D,
	TargetManager_U3CUpdateU3Eb__12_0_m31E8BA3302341057A2F4309A2E17DBDFF5FD3198,
	ThrowObject_Start_m2EBFB58803865D9269D5903865ECDFBBA032BDF6,
	ThrowObject_Update_mD1F4CAB12DFFC3378BDFB5E66CA7382E4660D0C7,
	ThrowObject__ctor_m5E4EEDE5472EE582E4FC8FB874493B4A42337CC2,
	UseObject_Use_m9D102C12987CA0E0DF2424B06BEBFB49498AB689,
	UseObject_OnDrawGizmos_m659784E1E29FD022E705A830ABD728977A03FD6F,
	UseObject__ctor_m7D040F280026FB01DF75A1D6529DDB91FBCE0D3E,
	WeaponManager_Update_m32FB280D8D2BAB807B5475557AE89D0C9658BF99,
	WeaponManager__ctor_m1317845BB799BA42EFD42256132F67FE62C1EFBA,
	ETFXButtonScript_Start_m33A4FB0CC86F7136C59CA734D8DF511BC800DF16,
	ETFXButtonScript_Update_m59B34AACCF491A85CC5FD3556204E5CAE1132F95,
	ETFXButtonScript_getProjectileNames_m125213EE305F066B86FD30975FB1F5530C775921,
	ETFXButtonScript_overButton_m3340E797EA0E2E0F1D9F63E848A0C9B87C99BB45,
	ETFXButtonScript__ctor_m699F4899D8FC6BF816893B3885422E554837AF53,
	ETFXFireProjectile_Start_m3FEB80C442260ACBFEA4531790418F0DDEB74805,
	ETFXFireProjectile_Update_mA1BEB2D98E67448597D24B536BE8D86BE55E439E,
	ETFXFireProjectile_nextEffect_mDAAB6C1C6788A94AFFCB516063B782DA5EEB073A,
	ETFXFireProjectile_previousEffect_m5B3208B6BBFCEA6B7AEF6A2028CC81CE3A323E5F,
	ETFXFireProjectile_AdjustSpeed_m13129DA829729D1EDB48CE88D0F9557AF39E6ED1,
	ETFXFireProjectile__ctor_mB28C3568BDC94B61A99CFB864CB9B4EA1C0E3BFA,
	ETFXLoopScript_Start_m9A424571F963ED20944D1CA011B364653B53261E,
	ETFXLoopScript_PlayEffect_m18E7B31868B9EAC272E59782BA74D5E765C36073,
	ETFXLoopScript_EffectLoop_m7FBB740F68C31AE47D215875553E3C9096164005,
	ETFXLoopScript__ctor_m0DC2C1F2F2CCDC1F8D788A9A5C7D665FB686231B,
	ETFXMouseOrbit_Start_mD455ECAD74152F902258F3C955454F5C88AF3524,
	ETFXMouseOrbit_LateUpdate_mBEAC3D0CA1272A71C77FB618BBB93D7E0FAF5288,
	ETFXMouseOrbit_ClampAngle_m0BFE0428C5E1083B89C8F3FB799D5DCAAA71F8C9,
	ETFXMouseOrbit__ctor_mAEC1F8A3E26D1976B3038B19598C97744D7C4A06,
	ETFXTarget_Start_m14929F3ADEFA5C21C8A7283C80203D1A1847F27F,
	ETFXTarget_SpawnTarget_m62C35F4021F983CA1C7C92E3CF089264493388B5,
	ETFXTarget_OnTriggerEnter_m162C8D756CE74ECDCF84195C3CDCAD912B5460C8,
	ETFXTarget_Respawn_m8F66D3EF6939232B79CC832877653CC525EB3400,
	ETFXTarget__ctor_m15F39EFD8564F3EDBE45ED128398859CA5B49912,
	ETFXLightFade_Start_mA462AF495E3DACF7F4E85A658CB6128A7E28EC64,
	ETFXLightFade_Update_m4BAB6873FAEAE8041C48007C57DD026BAA1A3ABC,
	ETFXLightFade__ctor_m2EBD208A70788C5E441CA5B8F1FDC5A47A607643,
	ETFXPitchRandomizer_Start_mAF7B1D827B3F63CAEB42B452B6973CB1159C5440,
	ETFXPitchRandomizer__ctor_m76FECEF1C05DEDE11037FCD96BB901CBF48CFE76,
	ETFXRotation_Start_mD30953FFDD78FDB7A0E9A87031EE0161F34545ED,
	ETFXRotation_Update_m041B694ADF0177DEF52FE9D553B5B48FB2D00225,
	ETFXRotation__ctor_m757B4E57FE637FF776015DAC0E9DCFF349A61F05,
	U3CPrivateImplementationDetailsU3E_ComputeStringHash_m94E4EDA2F909FACDA1591F7242109AFB650640AA,
	U3CU3Ec__cctor_m0476EAFFEC1E78B57D5FB75E908BEF46282FF75E,
	U3CU3Ec__ctor_m75C62406488B8C0BE95B1C13758F84D4498E57F8,
	U3CU3Ec_U3CGameOverU3Eb__2_0_m814322A84405DD399FC1B608B87F9A4B3D1F6392,
	U3CU3Ec__cctor_m0DAA011AF413A7518BC813E75E5CC223DF93D06F,
	U3CU3Ec__ctor_m4571F1547BE5CA1AE21B9B86A6209E6B23DAA87B,
	U3CU3Ec_U3C_ctorU3Eb__8_0_m93D764F72258D39CB90A8EE0EFC391D5CC357650,
	U3CU3Ec__cctor_m95FC62473891AA37FFCC8372B40E103D4575C457,
	U3CU3Ec__ctor_m840D892C6E61613DBB2DE45958F6EF5D7D7FB3E0,
	U3CU3Ec_U3CGameOverU3Eb__12_0_m959B53DCA5A405F1F7B5C3A3F3A2774D740144AA,
	U3CU3Ec__cctor_mD6FA8C29EDA0043A7459A18F7C178A925EE95FBF,
	U3CU3Ec__ctor_mE41BEFA8FC5911B4B4D2921D09410B21ADB4C36C,
	U3CU3Ec_U3CStartU3Eb__3_0_m1EEC307F6879EB5ABF0C6E8E5724AFF25703A38A,
	OnCenterCallback__ctor_mBF612D78CBC3C0DAC4E676A3B742C52B8817C8E3,
	OnCenterCallback_Invoke_m66C62792674B9B5B0588BF318DAC08A130FE537E,
	OnCenterCallback_BeginInvoke_m728F59F26005BC2EB7E985702FFB92E38B98B2FB,
	OnCenterCallback_EndInvoke_m2E03D4B4DE600962B815A6FF65188F0DC055E475,
	OnReposition__ctor_mBDB60A3EF9BD37E8B02AB98A9CE3DF58A08479F1,
	OnReposition_Invoke_m005DE04E6AA774E3228E3F20A27D1D5C20CBE6C8,
	OnReposition_BeginInvoke_m74E39E15739FF210002F82E74F69D197E189CA8C,
	OnReposition_EndInvoke_m1639D27F5B83064DD2586D38241C4CB98B6A9DF4,
	LegacyEvent__ctor_mCB864A0E976261DC4911455BA9A9BD1BABEF6C8F,
	LegacyEvent_Invoke_m9983F95F876B12F4B176783B48F79D449709D872,
	LegacyEvent_BeginInvoke_m336AD54FCD203C67B3A56BEA52CB1317B0F9E870,
	LegacyEvent_EndInvoke_m9CB4A7A87254D4812D6EF5CAE54048BF4CF8D6EC,
	U3CUpdateTweenPositionU3Ed__95__ctor_mF6F85F4707A0F220B836D7201B0BAD1C8737423F,
	U3CUpdateTweenPositionU3Ed__95_System_IDisposable_Dispose_mC281BD96ECFBD0C56E769A6B535F2ECD8F804ACE,
	U3CUpdateTweenPositionU3Ed__95_MoveNext_mA82B590343BEA6EA069B0E7D9F4E21913E78BD4D,
	U3CUpdateTweenPositionU3Ed__95_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5FE092A5E051E69C04A79C20FE942AEDCB0D48D0,
	U3CUpdateTweenPositionU3Ed__95_System_Collections_IEnumerator_Reset_mFCFD16651F321D28A6DD8D432411DF23481320C1,
	U3CUpdateTweenPositionU3Ed__95_System_Collections_IEnumerator_get_Current_mB99713ECB4B57DB74CB86079D532165C5FC27396,
	U3CCloseIfUnselectedU3Ed__112__ctor_mADE91FD63EE442895A20F952D3575839B607EA4E,
	U3CCloseIfUnselectedU3Ed__112_System_IDisposable_Dispose_m21CD366C42FFC38D2C8EED6CDD47E083EE94138E,
	U3CCloseIfUnselectedU3Ed__112_MoveNext_m70B6B69BD55399F6BFABB0DCD6606EB4B34C34C6,
	U3CCloseIfUnselectedU3Ed__112_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB1D952CE98AA9793CF6DAA33365DEF1B166316AB,
	U3CCloseIfUnselectedU3Ed__112_System_Collections_IEnumerator_Reset_m7D83353CB59FBEB1C6B29AFA4E75BF1DDBF30D67,
	U3CCloseIfUnselectedU3Ed__112_System_Collections_IEnumerator_get_Current_m070642FB23267F4EB48E62AD3D7D867EC5CAB9EE,
	OnDragFinished__ctor_m98B2272AD70E848F71F61970D7325C8F4E6E64C7,
	OnDragFinished_Invoke_m1F6654FF4A1B269DC11A55DDB7BF6689598C84F3,
	OnDragFinished_BeginInvoke_mA0F597F8C42787A2D2E2ECB408A080823BFCB7F6,
	OnDragFinished_EndInvoke_m60299AA3C158D3868BC32232F2E62FA50054821B,
	OnDragNotification__ctor_m87B2B385BE26BDE08131F0C45FC996F42871F418,
	OnDragNotification_Invoke_m6DDFE33E5A6CAA9ABBB96F7E20957B60FC7C909B,
	OnDragNotification_BeginInvoke_m4BD977A7F78F01163259AF420351D4D8F8D2855E,
	OnDragNotification_EndInvoke_m60A493670F41C10CA419452BB2CCB80905695C20,
	OnReposition__ctor_mE58EB28E0ACF9860CF32C6DE75F511446F5EE21F,
	OnReposition_Invoke_m4AB6FEBE04BA7B5BC2489A4297984AB2A19FF95B,
	OnReposition_BeginInvoke_mCCB0C9578A5CE760CFD673638F2362740AB37965,
	OnReposition_EndInvoke_m2614EA0AE22167FF25AAEC13B0C609F7F70668E3,
	Validate__ctor_m980443F8C543354F97027865867D8C62524DF404,
	Validate_Invoke_mA3A22ACC1D122ECAE1FA0CEE067517125E1CC9B8,
	Validate_BeginInvoke_m0F7A10D0E0D3B13A61AFDC645C74A032E71C9A77,
	Validate_EndInvoke_m1D261E4E7BC51A2E6A684292B0E157A504DDC601,
	OnInitializeItem__ctor_mDEE8E1B5AB1F426CF82C8EAEAA1D4F7A2579D208,
	OnInitializeItem_Invoke_mCF74A0F4A3B754D9ABD20B19B7B5FBE2F1B903DA,
	OnInitializeItem_BeginInvoke_mE09B62232113AFC5FB420501A0DD44B3E1CD41DF,
	OnInitializeItem_EndInvoke_m2A35CBDC775BD84552119A1CAC4B4DDD069DD609,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Parameter__ctor_m88A15A738BD3CA6EF6523333C2A1E204694DC234,
	Parameter__ctor_m6EE680C01C38FF8F04022196C064E9FF578B0FC7,
	Parameter__ctor_m99B5162EAEAF52137F3A9EC05AB1A9B60852491A,
	Parameter_get_value_mA79E459D497635A298157F7FACD13BACC8D7FEFD,
	Parameter_set_value_mE09EA27482BFCB50B9C76104737510C8599D75B2,
	Parameter_get_type_mEDBAA9C76FEEF89E30FD7BC3E451117B786BDB6F,
	Callback__ctor_mF72858DCD8EA9E40CB7DD23564B260E735F557AA,
	Callback_Invoke_m56147085BDC8BD8D70084FDD09E4434D741BA19D,
	Callback_BeginInvoke_m015229F63AAC972D4635EB14D51B65DBFF92734A,
	Callback_EndInvoke_m817FB4A68988F32584F6CDC50EB3572BDD77BE67,
	LoadFunction__ctor_mC60ABE821C56042C2477249F7F21849DFBB4EBC6,
	LoadFunction_Invoke_mFE3A02D386915B3E6EFD9C41BDD0D3171DAE1141,
	LoadFunction_BeginInvoke_mF0B7277566999A1EB09B30747E9EA6705F5B3CD7,
	LoadFunction_EndInvoke_mF44BF8CA7BD5B6C267B94F493D7F32A9B97B9DAC,
	OnLocalizeNotification__ctor_mCE9D2335B6D636DA743AA4CA88B24FA9C48D5BFD,
	OnLocalizeNotification_Invoke_m1D019485A85BBE1AFCAEB8260E1C16150A601810,
	OnLocalizeNotification_BeginInvoke_mC72C240C5B205E5C23FD7DAFED4675DE65CA6BEE,
	OnLocalizeNotification_EndInvoke_m0CCB59F0ADA19AB445033499D38986B47FC17B65,
	GlyphInfo__ctor_m3635C9640998839EBE8F00634471F593726EF651,
	NULL,
	NULL,
	NULL,
	NULL,
	OnFinished__ctor_m864708B3576F231EB8385331A3DD9CA56CA4B8EB,
	OnFinished_Invoke_m30357841ED6CC5D88E41D28EF0AFEB64AF0F7B18,
	OnFinished_BeginInvoke_m5894A288E4C5675C13141587A59A0997847DBE22,
	OnFinished_EndInvoke_m25AF02AB1C599145508221BB5505DF67215BF42E,
	OnRenderCallback__ctor_m98D1DF40846630BB8189D761B6FF468194CE615F,
	OnRenderCallback_Invoke_m179262E5F72E71B0CA5057099D2F450CD012B46F,
	OnRenderCallback_BeginInvoke_m79CA23D666496ADC162FDC5FBCCF609C02D9DFBA,
	OnRenderCallback_EndInvoke_mF63DD136D23E567238BC5D9E1AE2136B8DA7288C,
	OnCreateDrawCall__ctor_m69E25DA9AF36D1A764D008E67A8500966F6876E4,
	OnCreateDrawCall_Invoke_mEA1A091DC895CD8FCAE1A7ADB61D69F2FBAEC22D,
	OnCreateDrawCall_BeginInvoke_m09B9008C452D714DDC9CD1AB484102EC178E9FE6,
	OnCreateDrawCall_EndInvoke_mDF385DDCFA67C985C189990F9884501337D2C38B,
	VoidDelegate__ctor_m0BD74AEEC171523CAF4C457AAFA2A6006C41818F,
	VoidDelegate_Invoke_m20F76283CB9A815F1B98C4D10CC4A12EF037DAAB,
	VoidDelegate_BeginInvoke_mAB15AE42F5C236A675FC525535D42166E489C50E,
	VoidDelegate_EndInvoke_m081E2FFB87C588444AFBFA2CF7A9D5B425BCF6BB,
	BoolDelegate__ctor_m3A5C9DE3AC0781AE00FCCDCBF9365107F4663750,
	BoolDelegate_Invoke_mF5EF3D494D1F4FFF5DCDD1258CFC5C6754BD7BB2,
	BoolDelegate_BeginInvoke_m2F9CCD27873E5C2C9F09E3CB20750BBAFF8BDD68,
	BoolDelegate_EndInvoke_m829CA47ECB42BD692F410DA835DFA645D4CFFBB2,
	FloatDelegate__ctor_m1345BC7305F929D709154FE857A97158376106D3,
	FloatDelegate_Invoke_m5163C356F4D7835D0DC110F938D8E74654942DA5,
	FloatDelegate_BeginInvoke_m9B6B884587800515ABC5C654DFE8D24F13E550C2,
	FloatDelegate_EndInvoke_m208D74B0CBEB2C4C74ED8BC4015573402C687388,
	VectorDelegate__ctor_m1145066CF4743709B64685BD5A9BEEF846B0E8A3,
	VectorDelegate_Invoke_m9B9B1EC2E08FE0E4D104D722230A35C76713561D,
	VectorDelegate_BeginInvoke_m1FB6369E942436A0DB2854284BE007E3D05DC688,
	VectorDelegate_EndInvoke_m251EEB4BBA4389C6467E84F0BBC67B942168B590,
	ObjectDelegate__ctor_m1F69F6E7E56749FF7F6542613B0355A87975E958,
	ObjectDelegate_Invoke_mC5D9522470E7B8B56E311EA865A79E738BFB3976,
	ObjectDelegate_BeginInvoke_m4810A5A7AD8495394A7EE6DDA24B34C9DFBD3DC9,
	ObjectDelegate_EndInvoke_mF7FD8E99F05208B3E44296FE72D6C6B9DB6E431F,
	KeyCodeDelegate__ctor_mFE3D673253C02C9D526FA9DCFCD4EEAD15DC7A3C,
	KeyCodeDelegate_Invoke_m1DA6729D2575C393E2A9ACF8EF38BB206FCEEE6B,
	KeyCodeDelegate_BeginInvoke_mFEF6B8098466A2F4FFB330A3C6BA767F1D627AB1,
	KeyCodeDelegate_EndInvoke_mA871947CA6849101224B7F917A624C661E08EEF5,
	OnCustomWrite__ctor_m58B1DF46104BAAECE5463266181F0A7E37697548,
	OnCustomWrite_Invoke_mB289805ADD15A2C04742C43A23E9D2E078517617,
	OnCustomWrite_BeginInvoke_mA101B90462E30CAD9320643719B70BD98E64DC14,
	OnCustomWrite_EndInvoke_m9E054DA43564BF302A0EDD2AF039D076D6A09A6C,
	AnchorPoint__ctor_m418EA0B6A22EF969625837BDD56B451B151C9729,
	AnchorPoint__ctor_m40BBE485D38C9C22CC6458B7A1FB2DB9E641EA17,
	AnchorPoint_Set_m594521F81974BD610B958A41AF7E21E5DDB7B95D,
	AnchorPoint_Set_mA772B011DE2BBB0E63CB5842C9ADEA4AD9003D48,
	AnchorPoint_SetToNearest_mD9929C17E8C44D23971C2542BBA572CCEB52F1C5,
	AnchorPoint_SetToNearest_m2D84C27C37908C0C3D67E0FDB83AFE5FE800371C,
	AnchorPoint_SetHorizontal_m4BD6D6B8D8EEAFB80B016C93173E0AC9D242CC43,
	AnchorPoint_SetVertical_m96259F14398A794B1F7E18ADE848A0D8677EF635,
	AnchorPoint_GetSides_m76661F2AEC2A0A35D80AB33CB3D53F8D53BDD0F2,
	OnDimensionsChanged__ctor_mC9FF65197698E29B257317CE43427E1B1D4D6ED0,
	OnDimensionsChanged_Invoke_m7D06982F06C93AD0D419C4F6A4196FEAE3598FB0,
	OnDimensionsChanged_BeginInvoke_mF5E796F0066C64F80C85A6B4196DEAF4D425DF41,
	OnDimensionsChanged_EndInvoke_m5D1AEE87E54B8FF4E685410A611C910C648D1854,
	OnPostFillCallback__ctor_mBFD3A46DF1368B7C78AFF06D210DC5CB87D14AF1,
	OnPostFillCallback_Invoke_m85560B84A08BB10B28BB3EEC6FD33F194B3451D7,
	OnPostFillCallback_BeginInvoke_m2F8A89A57343BDBC3982C0C336764C4A8EDFCC54,
	OnPostFillCallback_EndInvoke_mFD2D2824BD07AEE88BC0AC8CAC257509E38427ED,
	HitCheck__ctor_m4E11157331C0C0DD9C3CFE9227E24C8D38482C9E,
	HitCheck_Invoke_m1CD630F27E7F0D9012BECA40F84969B20AF52F32,
	HitCheck_BeginInvoke_mCC172A07CF1404929B039A7BB0F49293524CFF1F,
	HitCheck_EndInvoke_m9CD0A2AAA5C75121A575C4A5CE72FFE8A8EC2ADE,
	OnFinished__ctor_mD285DD6CE3F515BD4D80406DC8E4FF29947D33E1,
	OnFinished_Invoke_mD54C2497223475006CA65FAB1210D8512E7E68A1,
	OnFinished_BeginInvoke_m881D6CF6D7D47F803A262779BD8A9812D30F8782,
	OnFinished_EndInvoke_mCCE36654DC997F6767D3188B240772BE97F0E3C6,
	U3CU3Ec__cctor_m2581C46CB4460452A5DF41ECFE93AFF22D87451E,
	U3CU3Ec__ctor_mAEF85074E8CCDA308E60FF8632486F71B115684A,
	U3CU3Ec_U3CSortAlphabeticallyU3Eb__25_0_m08140D7E115D4C5C0413C0F6E8306A063655F6B3,
	Sprite_get_hasPadding_m4915522716DE65C14FACCC99D42C0C9CD678BC59,
	Sprite__ctor_m12BE409981A75397D17DC860F5B24614E86CA4D5,
	U3CU3Ec__cctor_m1C9A69CC15AC41C77835EDC387E4633FC53C20A6,
	U3CU3Ec__ctor_m3F11CDE543FC5464E77DA56D0690B1FE826376C2,
	U3CU3Ec_U3CSortAlphabeticallyU3Eb__28_0_m0D9BB3123551623DE9DC5136EBD4A5C8F6DB35B2,
	MouseOrTouch_get_deltaTime_m61642D293BE0AB6C7BFE4F97CB2956E476A97823,
	MouseOrTouch_get_isOverUI_m6C98E84E4FD59CEA007CE5A87E847623A7CB7520,
	MouseOrTouch__ctor_m7C8494D4386F8F36612AB4DFED0D839244D6E6B4,
	GetKeyStateFunc__ctor_m902EBDB0C9147A64A6E756B80ADD5131ABCECB3F,
	GetKeyStateFunc_Invoke_m8887CBDC5A66CF48D877B8F185490EADC35F7B06,
	GetKeyStateFunc_BeginInvoke_m607EFAE71539E2B076BBC5C104A132C17E4081E3,
	GetKeyStateFunc_EndInvoke_mEF7E36B05AB97A413D5619CD66AAD92D436AADA0,
	GetAxisFunc__ctor_m3507562A17C64A2B462833593B17786A530F70CB,
	GetAxisFunc_Invoke_m9292371C72D0FD4B63B36E1A899E76D6B0B308AB,
	GetAxisFunc_BeginInvoke_m3930C8B9D567BC0434716E50764053A5381C96E8,
	GetAxisFunc_EndInvoke_mFBBFFA36546EB1127E120320CCE2975E3F14DABF,
	GetAnyKeyFunc__ctor_mAC645003F496DF5DD8CD50CB6C4CDF3AADA8BA38,
	GetAnyKeyFunc_Invoke_m48C991DFC5BC69AD63C9BD2A2FECD1D49670E9B1,
	GetAnyKeyFunc_BeginInvoke_mFBCEC86DDCA5B9CAFC2516ED37131AA52B002E50,
	GetAnyKeyFunc_EndInvoke_mAEE55F57AC59E6D6180CF0279A6004D8B1FEC2D0,
	GetMouseDelegate__ctor_m4F2A5A110B13B87DF96076A854677ED761CAA6A4,
	GetMouseDelegate_Invoke_m57A3FD1A256C09224664172037A5EBB6B5F7EC3F,
	GetMouseDelegate_BeginInvoke_mD9FBEC43663FC02C811672754B3BAA448081791C,
	GetMouseDelegate_EndInvoke_mA8380AAEE17E1FB9D639F8ACBA96B3AAA944740F,
	GetTouchDelegate__ctor_m234AEAE070A7550ABEF74C93C96E45A0406C96E1,
	GetTouchDelegate_Invoke_mFE2519A8303A604B0207A63D0A95100FA15D8704,
	GetTouchDelegate_BeginInvoke_m5B73F516E477ACC3373C571362B972702B29677F,
	GetTouchDelegate_EndInvoke_mCCBF86FA03C853D03D3A957A332B7E9B81107C42,
	RemoveTouchDelegate__ctor_m8B7326BACB4B88B5FB5074308F29F81B788E9103,
	RemoveTouchDelegate_Invoke_m9A7522640F843009DE48CD1F3AADD311193E9C0F,
	RemoveTouchDelegate_BeginInvoke_m835A39096B784ADFD230527B6583B6472C2D6269,
	RemoveTouchDelegate_EndInvoke_m1F597B0C173486C414607800BC8B8BD765EAA112,
	OnScreenResize__ctor_m41EAB9625A6AA728BB91060CFEEE2FF16BBDC12C,
	OnScreenResize_Invoke_m44606D87EFE690FEA51485532AC2F972B862D28D,
	OnScreenResize_BeginInvoke_m2B29DF67A65E1561A88B62B6A99E4831C247F98B,
	OnScreenResize_EndInvoke_m3A59F41499F4BB970375EEAA487187651F7171CF,
	OnCustomInput__ctor_m553736C2A976FDBFE48CD8FEDB31B4408C7EA9B4,
	OnCustomInput_Invoke_m2C432208C697A319D89D8C77BE5BFB572FAF48C1,
	OnCustomInput_BeginInvoke_m7563B9A9989E513E4EA0A12979D5FE3C3A98B7BE,
	OnCustomInput_EndInvoke_m0AC15F714E4B546AC5547840FD80DC655C578350,
	OnSchemeChange__ctor_m6F01271151A14842B7FFE26304988391F1C36D58,
	OnSchemeChange_Invoke_m4FE60DACBE712EAFFAA79910BFB9EFE8ED9BE04F,
	OnSchemeChange_BeginInvoke_m3B014E86F9373F61EA6A6DCAB5B1E69F19DE3214,
	OnSchemeChange_EndInvoke_mCC667EC1714F54FDB9B66B5B04C0E1EEAAF33A17,
	MoveDelegate__ctor_m90F4B762E8BA4A5E9ADF868CFBC220CDAB35B921,
	MoveDelegate_Invoke_mF95AA440CD1119361BE94BE1821F7055B435B18A,
	MoveDelegate_BeginInvoke_m7E6C64DE3161988587D800077B363634D0AEC0DE,
	MoveDelegate_EndInvoke_m330E805AF0E20BE279317A79BD522A55C33E5266,
	VoidDelegate__ctor_mAB06CF2953E1ECE70E59A9A8B2D5AB4DE72AF093,
	VoidDelegate_Invoke_m833F6B7FC8E21D70CC02AB80D420120D76687C8A,
	VoidDelegate_BeginInvoke_m45786749894962B4BBF6E75A94A8F97A71215291,
	VoidDelegate_EndInvoke_mD64315ED52C709799E754F7A9D229B3405FDA114,
	BoolDelegate__ctor_mE69C983DBF33C662C70C79867C4A6488D8D928DF,
	BoolDelegate_Invoke_m7FD4D3CBFA4243FF519C5F673DE5BD842166F548,
	BoolDelegate_BeginInvoke_m07C98BD35B81353B0F0B657D8301C063B0F3F888,
	BoolDelegate_EndInvoke_m97B03B2C33BDFBE510FCE47A9ABAC31A10CA559F,
	FloatDelegate__ctor_m22F709AB6E7496ED8106DD3A42839ECF72AB64DF,
	FloatDelegate_Invoke_m7A47FC0CCEAE6FF089A92B3D7DF75378E67C6CC1,
	FloatDelegate_BeginInvoke_mEC7B52B00353EE3A27D2B198276755CE0E13AB31,
	FloatDelegate_EndInvoke_mEDBC241554B63A0D70AB3D1AA2DA499D7D753D5B,
	VectorDelegate__ctor_mF5784C25BCF2757FA330FE7A7E97B0E510C1EE32,
	VectorDelegate_Invoke_mD055326A7AA3F9DF9584010B519B33DF4F8F9095,
	VectorDelegate_BeginInvoke_m087C26F4CD76031766ADA967AC924F0F781326AE,
	VectorDelegate_EndInvoke_m1F8EDE156B005C7A7F4B706D66DA85B3AEB9F0B4,
	ObjectDelegate__ctor_m705C0ED35498E475A22CA33D553027329BC11670,
	ObjectDelegate_Invoke_mEC73F6302EB8D11E66323705EFF4825C5D49D7AE,
	ObjectDelegate_BeginInvoke_m1A3CC3C1DA4675B9CF1BA2686387EA3C7F67DBC2,
	ObjectDelegate_EndInvoke_m5E7C456CD495E4D4972AD2C7B03DA9E5E40034B8,
	KeyCodeDelegate__ctor_mEA1BC191B11E9AD8C2657292D04E2F031BACB719,
	KeyCodeDelegate_Invoke_m276224B225A66BF9BA5CD6658DCA91B26C6E8712,
	KeyCodeDelegate_BeginInvoke_mC65DA293F8FB8396A2F7CBF2170D2365232A2344,
	KeyCodeDelegate_EndInvoke_m09A7E4BDEE8F9B8B35D81928FB395C4FE3D54CF9,
	Touch__ctor_m9D6453DC218BCD5C244816C0DA41E79B288C9072,
	GetTouchCountCallback__ctor_m90C1FFE3AAA313202BF417834D579B476038D81F,
	GetTouchCountCallback_Invoke_m0799C7AD4AC8CAE82DC069300E0C27DC3A4576BA,
	GetTouchCountCallback_BeginInvoke_m8BB5B76B1A9C563B6458C3F6AB65E7CD160A5136,
	GetTouchCountCallback_EndInvoke_mEA583C81FE97ECA375F0A833191E3CA8B2FF2C51,
	GetTouchCallback__ctor_mC0DC65F68C2A762265B583FFBEC419B54FDC857F,
	GetTouchCallback_Invoke_m9D16F3CA7778DEBE429581C3FDE7033DFAF9AB5A,
	GetTouchCallback_BeginInvoke_mA4C9623291B1E543148F6EFF3BD455BF84BEC505,
	GetTouchCallback_EndInvoke_m74A2B63F43A5DDDB25B5BBB77C8D6CAF19DC5F07,
	U3CU3Ec__cctor_mAB4058B6C5EF0060A05AEB4C5753E7B8E221B917,
	U3CU3Ec__ctor_mC72E56A0D1E321C16789A382ADB4FA0EE9BB0410,
	U3CU3Ec_U3CRaycastU3Eb__190_0_mE440F0FB2283EDD5EE82836F75A94D4A3ED21D2D,
	U3CU3Ec_U3CRaycastU3Eb__190_1_m976B9B9B329EE383D39FD0D07C8774D94EC38343,
	U3CU3Ec_U3C_cctorU3Eb__230_0_m407281D3677327A31C1A68A75B13921414DB0564,
	U3CU3Ec_U3C_cctorU3Eb__230_1_m847D54BE962B0D02113BFFB35908F3ED2BC4E3D0,
	U3CU3Ec_U3C_cctorU3Eb__230_2_m5351F16D0071D1C4FB9D3A12982EB1A797B2576E,
	U3CU3Ec_U3C_cctorU3Eb__230_3_m2ACD047F4C9DE94BD9BF9E3079C6B39E1CC7886F,
	U3CU3Ec_U3C_cctorU3Eb__230_4_mD268B7FD64B029CD6670210601DBDD96F53507D0,
	U3CU3Ec_U3C_cctorU3Eb__230_5_mF22C2DA366DC77718C9A5ADD7F919B2E73E0822E,
	U3CU3Ec_U3C_cctorU3Eb__230_6_m23DD6B839E82B7DD2673C1347C1B040AEF2111C2,
	OnValidate__ctor_m734BBC6B8796E7333FF21471EE266E2815F20605,
	OnValidate_Invoke_mA5DCFD2213708341CA4F78F969B4674CCCC05189,
	OnValidate_BeginInvoke_mAF6077DA7F8DA0AA207A17A6D746C3E0EBCEF0BB,
	OnValidate_EndInvoke_m41993DBE7BD6FAD03EBEB7F52BF40BA1513FF323,
	ModifierFunc__ctor_m573818B286775C56FA715AD6FEEE0E6914AC2BAC,
	ModifierFunc_Invoke_m9CAB74B008DACAF06F4B60C94EF0AECE2747B63C,
	ModifierFunc_BeginInvoke_m5EA5983D4DEEA75050F770ED3A01D2935E435BB3,
	ModifierFunc_EndInvoke_mC79C64B6018FFCD2310B2C0B858F892A9A90EF3B,
	OnGeometryUpdated__ctor_m5B889BC8F535DB17F0DA302A2EC2B064087DBA0F,
	OnGeometryUpdated_Invoke_mAAC351E53D362D2CC4C429CF791BDC7592ECC66D,
	OnGeometryUpdated_BeginInvoke_mC243DEEA1D28185837FF0CF2BFF9B3AD9210D64E,
	OnGeometryUpdated_EndInvoke_m963956A2BDBD0A2616F801125EE19353385DCFCA,
	OnClippingMoved__ctor_m4F91986AD2EF514A6C740BF5274D7465437AC7A2,
	OnClippingMoved_Invoke_m6695F8F6BC6584A6C7C7A96E032AB719603F249F,
	OnClippingMoved_BeginInvoke_mF428511832B59599F483C2AB19F43D2D460D713C,
	OnClippingMoved_EndInvoke_m089A64DBA217E7245936207AD9B3F61ED4A08BE6,
	OnCreateMaterial__ctor_m3C3D2C39B7F9FF1874B4974346E5E01B29FF9553,
	OnCreateMaterial_Invoke_mC5981C4D545519C8DB5C52764FF3C5A66AB857C9,
	OnCreateMaterial_BeginInvoke_m359A374B2DD4830532B2DF8FAAFCA4D11D8857B2,
	OnCreateMaterial_EndInvoke_mAC24DA9128FE07B3C57EAAAF19A73B2C2E9E634A,
	Sprite_GetDrawingDimensions_mA4478A253FE5F31C6E725E9F7E3CA70FD2C60DE7_AdjustorThunk,
	OnHoverCB__ctor_m6B9911B1D0ED8EEF3319D5FD245D4C1BDC924150,
	OnHoverCB_Invoke_mCCC904FE0D729A0F3FEE2AB37E668E9A14BAD22E,
	OnHoverCB_BeginInvoke_m0177489BD3A6B0F8D50B9FB8E433B61B23589973,
	OnHoverCB_EndInvoke_m9DDE489390D5F8B507ACB25A45A0776AFB31741C,
	OnPressCB__ctor_mE2B8993DF0FCC2607115CFB2BD38DDA358E60378,
	OnPressCB_Invoke_m7791C776FE0EFDD99006D2354E2CFEB27378A4C5,
	OnPressCB_BeginInvoke_mA6CC9890EE888DDA5A8B7FC900EA4A3DD0E3D08D,
	OnPressCB_EndInvoke_mF0DE7B675F67A6D2C52A3BEADE28A5741F9E9485,
	OnClickCB__ctor_m418E6D30469B5EA6C6E1924AA09880127FB386F4,
	OnClickCB_Invoke_m8F452C3ABCFA316621AE3CBBDB253C7E28135696,
	OnClickCB_BeginInvoke_m54AF682DA4E932E2C41B24223755AE4F3364CE4F,
	OnClickCB_EndInvoke_mB5078F3BCED73628EEFDBF26CA52D4FA6385CB7F,
	OnDragCB__ctor_m9CD0633F7A5CF8208E3900A49AB6734667553EB7,
	OnDragCB_Invoke_m838BA12FA8EAB7ED99FC5984FCA5F4CCF1477F9D,
	OnDragCB_BeginInvoke_m8443C3C5BE7A8E5F9FBF536D531EBD3562FF2041,
	OnDragCB_EndInvoke_m0FE336F544DB2A95B7ECD18B64DB8B5FE70BA83C,
	OnTooltipCB__ctor_m4AD67E30B5C7C64AFEAA2B988D9EDA06A671B337,
	OnTooltipCB_Invoke_m3104772CFBA3EB85D561D2301D3D4FA90A75C748,
	OnTooltipCB_BeginInvoke_mFE9C074DDC340B61042F28678EAD36F0A192C358,
	OnTooltipCB_EndInvoke_mB268E2C4A726461EA379E7B9115798E87B5E8FC4,
	Paragraph__ctor_m47FCA7CEA13E85DB03BD3D03A28E980245AFA746,
	LetterProperties__ctor_mDBC4FDA051A98037A8EA95C81F709F646E2D218C,
	AnimationProperties__ctor_m576AFE8E2BF356048A23A2C9CA95F51F9C6B8EF5,
	U3CU3Ec__cctor_mF172830ADB11D405A5DEB07964231D0A104C6E32,
	U3CU3Ec__ctor_m23679B08194E6E2397F91CC5962C14B8DDFB7C28,
	U3CU3Ec_U3CGameOverU3Eb__4_0_m2E0701DA5DC8B743C70DA42C56290D03083EB1CD,
	InvokableItem__ctor_mBD1BC7C79F6386C13B55A7225ED67BE60E58E5B1_AdjustorThunk,
	InvokableItem__cctor_m3B62F51652D6EEFBEA2D11E28D175379479A7561,
	U3CU3Ec__cctor_m6C791A37955F7852ED54DE100AB3FE0C7CA32F6B,
	U3CU3Ec__ctor_m631CA5D86F20199C408287BDE830A6355AF544A8,
	U3CU3Ec_U3C_cctorU3Eb__42_0_m415A7E2AE7178E3C2743A8149935CA1E89EB949F,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CDelayMethodU3Ed__3__ctor_mFD2C170FED71FA8F623B7A22A542E2F7265E2265,
	U3CDelayMethodU3Ed__3_System_IDisposable_Dispose_mFD692FF85D95F6D3009BB6DEE220425BF3A2E958,
	U3CDelayMethodU3Ed__3_MoveNext_m7C65BC2319244D84E3E73B869948560AEB054402,
	U3CDelayMethodU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m120349318CB903B695ACD972C35B6EE222EC5D45,
	U3CDelayMethodU3Ed__3_System_Collections_IEnumerator_Reset_m2E26450A80621BC63BA4F410CCAFA9F00C87FFA8,
	U3CDelayMethodU3Ed__3_System_Collections_IEnumerator_get_Current_m8207E574682CB41DFC12DBCA5F577D6E500C29D0,
	U3CU3Ec__cctor_mEEB446D263068A72423B8908C226646CAD19C825,
	U3CU3Ec__ctor_m568C68F85EF0CE2808829BEBE38CA4A093FDCB18,
	U3CU3Ec_U3C_ctorU3Eb__15_0_m1637C8AD486DEFD353D7B521D65ADF6E54DC7C4E,
	U3CU3Ec__cctor_m49C8BAC7B2B783DE08DAF777F7843C1C4F483AD2,
	U3CU3Ec__ctor_m157A06FB4329325AAF7DE56DFC5249803F138032,
	U3CU3Ec_U3C_cctorU3Eb__25_0_mF1D41F1CBB3501226B403355867B1464E0FB7501,
	U3CU3Ec_U3C_cctorU3Eb__25_1_m9927755A165639E5135C2967E159DC01C873B915,
	U3CU3Ec_U3C_cctorU3Eb__25_2_m0700845756F9BC84FE3A1FB702B98578453DF73A,
	U3CU3Ec__DisplayClass2_0__ctor_m8A14D74193A64DFDA56705C83191B16AD7F832BC,
	U3CU3Ec__DisplayClass2_0_U3CTweetU3Eb__0_m83DC7DB77E7E7B9BA243D1327C6D9B90730FFACE,
	U3CU3Ec__cctor_m536220CFABBE9A93CCADEEC9B8FA930F2192B311,
	U3CU3Ec__ctor_mDB3AC550A53049ABF5E9C66557C11CCD0BADA8ED,
	U3CU3Ec_U3C_ctorU3Eb__8_0_m7C43611373E90ADD36ED6859529A894179D989A9,
	OnExplosion__ctor_m5785D8E3F7E730BEB622B85F29B0223E56FDD8BA,
	OnExplosion_Invoke_m10A7A2ABE292E9439F091E31B0BDAD923C1B2DD1,
	OnExplosion_BeginInvoke_m63C90C058889B47E93FEC8B002A09D22EB52D468,
	OnExplosion_EndInvoke_m13AE98EF9EC9AE58C1E682A757E4141362CA94A4,
	U3CU3Ec__cctor_m1D86183FBBB6919940B6778552BAAC46955A2439,
	U3CU3Ec__ctor_m3F0C7BEC8BAB7E33FD71C86A95752375FB734681,
	U3CU3Ec_U3CSortU3Eb__0_0_m5ACB7AB065A74072A4DEB12F86B0CF5008CFF87F,
	U3CU3Ec__cctor_m75458A6B30EB2456FB23F0EF126DB7D00958ABB6,
	U3CU3Ec__ctor_m927F9ECBCBF816B080F31BCC3F05E1BA374DC7BE,
	U3CU3Ec_U3CStartU3Eb__3_0_mAD5BBF6B9905C8C1617C1CA95B53A5518DE7A3E7,
	U3CU3Ec_U3CStartU3Eb__3_1_mE8B28ACC38369D2BA1067EEEE0015DE7622AB4E5,
	OnHit__ctor_m1DDB7A1C8F596672D6531E98B2FABD8084653DFA,
	OnHit_Invoke_m3C32CDF26AFDB5F190616E79A1A4EEB4316D3863,
	OnHit_BeginInvoke_mF1DE1EC3D1449EA28158869EB198CEF3DC9B9E31,
	OnHit_EndInvoke_mC58A8A2B30B1AC24B3FA22BEBF2264DB87E1EB1B,
	U3CEffectLoopU3Ed__6__ctor_m19CC1932A7DA8A6618D298CFF1BBA53618830753,
	U3CEffectLoopU3Ed__6_System_IDisposable_Dispose_m1D2581BCDA04A67D92D3CB0890C8376670256854,
	U3CEffectLoopU3Ed__6_MoveNext_m7A01CBF6EC76A56EB3446517E38FD1D9FB798EC7,
	U3CEffectLoopU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCF17BD0CB66F98139B79E9EB07A691281CF8CD59,
	U3CEffectLoopU3Ed__6_System_Collections_IEnumerator_Reset_m4DE482E578C91F13D0208F7A6B035146D6E4E6B7,
	U3CEffectLoopU3Ed__6_System_Collections_IEnumerator_get_Current_m12F1418D67FCE2BC30B2F1114789F336A2A42D98,
	U3CRespawnU3Ed__7__ctor_m450E28D1F0615476C4B949E64B9E1D7E43DEABA5,
	U3CRespawnU3Ed__7_System_IDisposable_Dispose_mA34A2978F752691D1448A49E9BF89C92CB5A970E,
	U3CRespawnU3Ed__7_MoveNext_m396EB1C3C667DBBF960F402DDC9B3F164BE36F75,
	U3CRespawnU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD1F4E558CF6D12D0ED2A2CF1159BB026E3498CB5,
	U3CRespawnU3Ed__7_System_Collections_IEnumerator_Reset_m8E4983D69BFF682E250120798368D2119DAFC045,
	U3CRespawnU3Ed__7_System_Collections_IEnumerator_get_Current_mF8D216ECEA311410014AEEEA55F06C55EF8EE812,
};
static const int32_t s_InvokerIndices[3362] = 
{
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	23,
	23,
	23,
	23,
	14,
	23,
	23,
	1051,
	23,
	23,
	23,
	23,
	32,
	23,
	23,
	23,
	23,
	32,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	14,
	31,
	102,
	31,
	102,
	23,
	23,
	23,
	23,
	664,
	275,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	31,
	102,
	31,
	102,
	32,
	10,
	26,
	14,
	32,
	10,
	23,
	23,
	390,
	23,
	23,
	31,
	102,
	23,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	102,
	23,
	23,
	23,
	23,
	23,
	23,
	102,
	31,
	14,
	26,
	14,
	26,
	23,
	23,
	23,
	23,
	23,
	577,
	26,
	26,
	23,
	23,
	23,
	10,
	32,
	1030,
	1031,
	102,
	31,
	23,
	23,
	23,
	23,
	23,
	23,
	31,
	31,
	23,
	23,
	577,
	31,
	23,
	23,
	23,
	23,
	23,
	23,
	31,
	31,
	31,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	31,
	31,
	23,
	23,
	31,
	23,
	23,
	23,
	23,
	31,
	31,
	31,
	23,
	23,
	23,
	23,
	31,
	31,
	31,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	2039,
	26,
	23,
	23,
	23,
	23,
	31,
	1083,
	275,
	23,
	23,
	23,
	109,
	23,
	23,
	23,
	23,
	31,
	23,
	390,
	26,
	23,
	23,
	14,
	26,
	1083,
	23,
	26,
	23,
	1083,
	26,
	23,
	26,
	23,
	31,
	23,
	3,
	23,
	23,
	23,
	1050,
	1051,
	23,
	23,
	23,
	23,
	31,
	1083,
	1051,
	23,
	23,
	23,
	275,
	23,
	23,
	1083,
	23,
	23,
	23,
	23,
	23,
	23,
	31,
	1083,
	275,
	1083,
	23,
	1060,
	1083,
	23,
	1050,
	176,
	31,
	1083,
	275,
	23,
	23,
	102,
	31,
	31,
	31,
	23,
	23,
	23,
	23,
	26,
	26,
	1083,
	23,
	31,
	31,
	23,
	23,
	31,
	1083,
	26,
	23,
	275,
	23,
	31,
	14,
	34,
	104,
	26,
	390,
	9,
	23,
	23,
	23,
	23,
	126,
	126,
	126,
	26,
	23,
	23,
	26,
	23,
	102,
	31,
	23,
	23,
	23,
	31,
	31,
	26,
	23,
	14,
	46,
	23,
	23,
	23,
	23,
	102,
	46,
	23,
	31,
	23,
	14,
	523,
	305,
	522,
	23,
	3,
	4,
	102,
	23,
	23,
	23,
	109,
	14,
	14,
	14,
	14,
	2040,
	1860,
	32,
	32,
	23,
	23,
	3,
	102,
	23,
	23,
	23,
	23,
	31,
	31,
	23,
	23,
	31,
	23,
	23,
	23,
	26,
	31,
	42,
	23,
	23,
	23,
	23,
	3,
	102,
	23,
	23,
	31,
	31,
	23,
	31,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	31,
	390,
	23,
	31,
	23,
	23,
	31,
	23,
	23,
	23,
	31,
	23,
	23,
	14,
	26,
	14,
	26,
	14,
	26,
	49,
	14,
	26,
	14,
	14,
	102,
	31,
	102,
	10,
	664,
	664,
	390,
	23,
	26,
	27,
	156,
	26,
	26,
	23,
	23,
	23,
	23,
	390,
	1050,
	14,
	390,
	390,
	26,
	390,
	32,
	32,
	23,
	31,
	3,
	23,
	26,
	2041,
	2041,
	2041,
	23,
	23,
	14,
	23,
	23,
	3,
	14,
	14,
	14,
	26,
	14,
	26,
	10,
	32,
	664,
	275,
	664,
	275,
	102,
	102,
	1643,
	23,
	23,
	23,
	23,
	23,
	2042,
	2042,
	23,
	1051,
	1083,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	664,
	275,
	664,
	275,
	23,
	23,
	2042,
	23,
	23,
	14,
	102,
	1082,
	102,
	102,
	102,
	102,
	102,
	1050,
	1051,
	23,
	23,
	23,
	23,
	23,
	176,
	2043,
	23,
	23,
	31,
	2044,
	1613,
	23,
	23,
	23,
	23,
	1051,
	1051,
	31,
	23,
	275,
	23,
	1083,
	23,
	3,
	23,
	23,
	23,
	23,
	102,
	664,
	275,
	102,
	31,
	23,
	23,
	390,
	1094,
	390,
	1094,
	1083,
	23,
	23,
	23,
	23,
	31,
	14,
	26,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	102,
	31,
	102,
	102,
	31,
	43,
	23,
	23,
	23,
	23,
	42,
	23,
	3,
	23,
	23,
	23,
	23,
	23,
	390,
	23,
	23,
	23,
	26,
	23,
	23,
	102,
	23,
	23,
	23,
	124,
	23,
	664,
	102,
	23,
	23,
	23,
	23,
	124,
	2045,
	493,
	152,
	2045,
	23,
	102,
	10,
	32,
	10,
	32,
	10,
	32,
	10,
	32,
	10,
	14,
	26,
	14,
	1097,
	34,
	23,
	279,
	23,
	37,
	157,
	279,
	23,
	10,
	10,
	10,
	10,
	10,
	10,
	1032,
	23,
	9,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	26,
	26,
	0,
	102,
	151,
	14,
	310,
	14,
	14,
	3,
	14,
	26,
	14,
	26,
	14,
	102,
	102,
	23,
	26,
	27,
	0,
	109,
	9,
	10,
	26,
	27,
	23,
	102,
	23,
	14,
	111,
	109,
	1,
	122,
	1,
	167,
	122,
	1343,
	99,
	99,
	3,
	4,
	111,
	4,
	4,
	111,
	49,
	561,
	109,
	111,
	122,
	122,
	3,
	561,
	561,
	109,
	168,
	144,
	2,
	109,
	122,
	122,
	109,
	110,
	1,
	2,
	296,
	1,
	49,
	0,
	109,
	144,
	3,
	1024,
	49,
	775,
	3,
	111,
	111,
	111,
	3,
	2046,
	23,
	23,
	3,
	1187,
	125,
	125,
	375,
	375,
	192,
	173,
	43,
	43,
	43,
	2047,
	2048,
	523,
	2048,
	2049,
	2050,
	1063,
	2049,
	1929,
	2051,
	2051,
	2052,
	2053,
	2054,
	2055,
	2056,
	2057,
	376,
	1834,
	2058,
	1158,
	2059,
	1187,
	2060,
	2061,
	2062,
	1660,
	2063,
	1853,
	1853,
	2064,
	2065,
	2066,
	2067,
	2068,
	218,
	2069,
	2069,
	2070,
	2071,
	2072,
	122,
	49,
	3,
	775,
	111,
	151,
	2073,
	2074,
	92,
	2075,
	2075,
	2075,
	1146,
	1851,
	89,
	1146,
	1146,
	178,
	48,
	2076,
	0,
	2077,
	2078,
	2078,
	46,
	17,
	17,
	1255,
	186,
	0,
	2079,
	2080,
	1905,
	144,
	144,
	2081,
	2082,
	2083,
	2084,
	3,
	23,
	1076,
	1231,
	49,
	0,
	1730,
	1737,
	125,
	0,
	-1,
	43,
	111,
	554,
	111,
	554,
	554,
	111,
	122,
	122,
	554,
	-1,
	0,
	122,
	122,
	111,
	0,
	152,
	110,
	2085,
	1,
	1,
	493,
	186,
	186,
	1714,
	171,
	111,
	111,
	3,
	3,
	111,
	111,
	3,
	741,
	2086,
	2085,
	313,
	-1,
	-1,
	-1,
	2087,
	0,
	-1,
	-1,
	111,
	111,
	111,
	111,
	122,
	99,
	111,
	554,
	111,
	554,
	1570,
	111,
	554,
	109,
	109,
	109,
	554,
	313,
	1073,
	111,
	896,
	2088,
	2089,
	2090,
	99,
	0,
	1840,
	111,
	4,
	111,
	1146,
	2075,
	0,
	-1,
	0,
	1730,
	1,
	548,
	0,
	1730,
	1,
	548,
	1,
	-1,
	-1,
	111,
	1200,
	43,
	186,
	-1,
	1840,
	1840,
	99,
	122,
	99,
	3,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	26,
	14,
	26,
	102,
	102,
	23,
	27,
	14,
	9,
	10,
	27,
	23,
	23,
	14,
	1,
	14,
	9,
	102,
	767,
	99,
	99,
	1572,
	3,
	1076,
	1076,
	23,
	23,
	23,
	23,
	1756,
	0,
	23,
	10,
	32,
	10,
	32,
	10,
	32,
	664,
	275,
	10,
	10,
	102,
	31,
	102,
	102,
	664,
	1245,
	1245,
	1030,
	2091,
	2092,
	2092,
	2093,
	2094,
	2092,
	2092,
	1620,
	1621,
	2095,
	23,
	3,
	4,
	4,
	4,
	10,
	32,
	10,
	32,
	14,
	26,
	10,
	14,
	14,
	26,
	14,
	14,
	26,
	14,
	26,
	10,
	32,
	10,
	102,
	23,
	14,
	23,
	577,
	148,
	23,
	2096,
	23,
	23,
	23,
	23,
	296,
	448,
	0,
	3,
	3,
	3,
	186,
	111,
	1266,
	23,
	3,
	102,
	23,
	23,
	23,
	31,
	31,
	31,
	275,
	23,
	1083,
	23,
	23,
	23,
	26,
	32,
	31,
	23,
	0,
	23,
	102,
	102,
	23,
	2100,
	877,
	23,
	14,
	14,
	14,
	102,
	102,
	102,
	102,
	14,
	14,
	102,
	664,
	275,
	1085,
	14,
	14,
	664,
	31,
	28,
	2102,
	23,
	23,
	23,
	23,
	23,
	23,
	32,
	23,
	23,
	26,
	26,
	681,
	2103,
	2104,
	2105,
	279,
	23,
	23,
	1025,
	26,
	23,
	23,
	23,
	23,
	3,
	23,
	23,
	14,
	26,
	1245,
	1762,
	1060,
	10,
	32,
	10,
	32,
	1030,
	1031,
	1031,
	664,
	275,
	102,
	102,
	10,
	32,
	10,
	32,
	10,
	32,
	10,
	14,
	1060,
	1050,
	14,
	1050,
	1245,
	14,
	26,
	14,
	26,
	14,
	26,
	1060,
	102,
	157,
	28,
	1085,
	32,
	31,
	1085,
	1025,
	23,
	126,
	126,
	1082,
	1911,
	23,
	23,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	31,
	23,
	23,
	602,
	30,
	30,
	877,
	23,
	10,
	10,
	1245,
	1762,
	156,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	1756,
	23,
	664,
	275,
	23,
	664,
	275,
	1643,
	1708,
	23,
	23,
	23,
	23,
	1030,
	1031,
	1030,
	1031,
	1643,
	2110,
	23,
	23,
	23,
	23,
	23,
	14,
	664,
	275,
	664,
	275,
	1643,
	1737,
	23,
	23,
	23,
	23,
	23,
	14,
	10,
	32,
	10,
	32,
	1643,
	2111,
	23,
	23,
	23,
	23,
	23,
	14,
	664,
	275,
	664,
	275,
	1643,
	1737,
	23,
	23,
	23,
	14,
	1050,
	1051,
	1050,
	1051,
	23,
	1643,
	2112,
	2113,
	23,
	23,
	23,
	23,
	23,
	14,
	1239,
	1240,
	1239,
	1240,
	1643,
	2114,
	23,
	23,
	23,
	23,
	23,
	14,
	1050,
	1051,
	1050,
	1051,
	1643,
	2112,
	23,
	23,
	23,
	23,
	23,
	1643,
	548,
	2115,
	23,
	14,
	664,
	275,
	664,
	275,
	1643,
	1737,
	23,
	23,
	23,
	14,
	10,
	32,
	10,
	32,
	1643,
	2111,
	23,
	23,
	23,
	23,
	23,
	664,
	664,
	275,
	10,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	26,
	26,
	26,
	23,
	23,
	1643,
	1027,
	23,
	23,
	23,
	31,
	23,
	23,
	1643,
	-1,
	23,
	23,
	23,
	14,
	26,
	14,
	26,
	14,
	664,
	275,
	102,
	14,
	26,
	28,
	14,
	28,
	9,
	23,
	23,
	14,
	26,
	102,
	14,
	26,
	14,
	664,
	275,
	14,
	26,
	28,
	23,
	23,
	14,
	28,
	9,
	23,
	23,
	14,
	26,
	10,
	32,
	10,
	32,
	102,
	14,
	26,
	14,
	26,
	28,
	14,
	26,
	102,
	102,
	14,
	1032,
	1033,
	14,
	26,
	102,
	10,
	32,
	14,
	14,
	26,
	14,
	102,
	14,
	26,
	10,
	32,
	9,
	23,
	23,
	54,
	27,
	26,
	27,
	9,
	14,
	26,
	10,
	32,
	10,
	32,
	102,
	14,
	26,
	14,
	26,
	28,
	14,
	26,
	102,
	102,
	102,
	14,
	1032,
	1033,
	14,
	26,
	102,
	10,
	32,
	10,
	32,
	14,
	14,
	26,
	14,
	102,
	14,
	26,
	10,
	32,
	23,
	9,
	23,
	23,
	100,
	54,
	27,
	26,
	27,
	9,
	23,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	102,
	31,
	102,
	664,
	1245,
	1245,
	1762,
	23,
	23,
	156,
	23,
	102,
	10,
	32,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	26,
	102,
	14,
	26,
	14,
	664,
	275,
	14,
	26,
	28,
	23,
	23,
	14,
	28,
	9,
	23,
	102,
	23,
	102,
	49,
	775,
	1200,
	1495,
	1200,
	1495,
	4,
	522,
	121,
	522,
	121,
	2116,
	49,
	4,
	111,
	4,
	4,
	4,
	102,
	14,
	4,
	111,
	109,
	49,
	49,
	49,
	4,
	111,
	4,
	111,
	4,
	111,
	109,
	522,
	522,
	522,
	4,
	4,
	126,
	0,
	0,
	111,
	2117,
	2118,
	327,
	109,
	43,
	125,
	2119,
	186,
	144,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	2120,
	2121,
	9,
	42,
	3,
	109,
	49,
	1231,
	23,
	3,
	23,
	23,
	31,
	1083,
	1083,
	23,
	1083,
	2125,
	2126,
	23,
	14,
	26,
	10,
	32,
	10,
	32,
	102,
	14,
	26,
	14,
	26,
	28,
	14,
	26,
	102,
	102,
	102,
	14,
	1032,
	1033,
	14,
	26,
	102,
	10,
	32,
	10,
	32,
	14,
	14,
	26,
	14,
	102,
	14,
	26,
	10,
	32,
	23,
	9,
	23,
	23,
	100,
	54,
	27,
	26,
	27,
	9,
	23,
	14,
	26,
	1030,
	1031,
	102,
	14,
	26,
	14,
	26,
	390,
	102,
	31,
	102,
	31,
	10,
	32,
	10,
	32,
	10,
	32,
	14,
	28,
	23,
	23,
	26,
	31,
	23,
	23,
	23,
	32,
	23,
	26,
	14,
	14,
	14,
	10,
	31,
	1083,
	23,
	23,
	23,
	23,
	1627,
	23,
	23,
	23,
	23,
	23,
	3,
	23,
	10,
	102,
	31,
	102,
	102,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	10,
	10,
	32,
	10,
	32,
	10,
	32,
	102,
	31,
	1030,
	1031,
	1030,
	1031,
	10,
	32,
	10,
	32,
	102,
	31,
	664,
	275,
	664,
	275,
	664,
	664,
	102,
	31,
	10,
	32,
	10,
	32,
	102,
	102,
	31,
	10,
	32,
	10,
	32,
	10,
	32,
	10,
	32,
	102,
	31,
	14,
	14,
	1245,
	10,
	32,
	10,
	32,
	1030,
	1031,
	1060,
	1083,
	10,
	102,
	31,
	14,
	1060,
	1060,
	102,
	10,
	32,
	23,
	23,
	26,
	14,
	111,
	28,
	23,
	23,
	23,
	23,
	23,
	23,
	42,
	23,
	23,
	1319,
	1624,
	2127,
	2128,
	1652,
	2129,
	34,
	1652,
	2129,
	34,
	56,
	2130,
	102,
	102,
	156,
	2131,
	2132,
	104,
	23,
	23,
	23,
	1897,
	2133,
	23,
	31,
	23,
	3,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	26,
	522,
	102,
	664,
	275,
	10,
	32,
	10,
	32,
	126,
	664,
	664,
	102,
	102,
	1050,
	10,
	32,
	14,
	10,
	102,
	102,
	102,
	1060,
	1083,
	23,
	14,
	26,
	1245,
	1762,
	1245,
	1762,
	1245,
	1060,
	1083,
	14,
	14,
	28,
	31,
	1085,
	1025,
	2134,
	1062,
	9,
	9,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	9,
	412,
	32,
	23,
	23,
	28,
	26,
	26,
	23,
	2135,
	2136,
	412,
	0,
	110,
	2085,
	1060,
	1060,
	23,
	3,
	10,
	10,
	10,
	664,
	423,
	1085,
	23,
	23,
	23,
	23,
	23,
	31,
	111,
	122,
	23,
	3,
	14,
	26,
	14,
	26,
	14,
	26,
	102,
	31,
	28,
	23,
	14,
	26,
	102,
	102,
	31,
	102,
	31,
	1030,
	1031,
	1030,
	1031,
	1245,
	1245,
	664,
	10,
	10,
	1245,
	102,
	14,
	26,
	23,
	23,
	23,
	156,
	23,
	10,
	10,
	32,
	14,
	26,
	102,
	31,
	102,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	26,
	14,
	26,
	14,
	26,
	664,
	102,
	1245,
	1245,
	156,
	2137,
	2138,
	2139,
	2140,
	9,
	2141,
	23,
	9,
	412,
	2142,
	1195,
	14,
	2143,
	1652,
	2144,
	23,
	31,
	31,
	1083,
	1083,
	31,
	23,
	102,
	102,
	279,
	279,
	279,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	10,
	102,
	664,
	275,
	664,
	10,
	23,
	23,
	23,
	275,
	1083,
	23,
	26,
	390,
	23,
	23,
	23,
	3,
	14,
	26,
	14,
	26,
	14,
	26,
	102,
	1245,
	1762,
	1032,
	1033,
	1245,
	102,
	31,
	23,
	23,
	156,
	23,
	49,
	23,
	23,
	23,
	23,
	275,
	26,
	111,
	111,
	3,
	23,
	23,
	23,
	23,
	32,
	10,
	32,
	10,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	664,
	275,
	1643,
	1737,
	23,
	23,
	23,
	23,
	23,
	23,
	31,
	2107,
	1643,
	32,
	32,
	2146,
	2030,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	27,
	23,
	-1,
	-1,
	-1,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	2147,
	3,
	121,
	163,
	23,
	101,
	26,
	4,
	664,
	1218,
	23,
	23,
	3,
	23,
	23,
	23,
	23,
	23,
	23,
	3,
	522,
	121,
	111,
	111,
	49,
	775,
	49,
	775,
	1076,
	1231,
	522,
	121,
	522,
	522,
	121,
	522,
	121,
	522,
	121,
	3,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	3,
	-1,
	99,
	1,
	313,
	122,
	122,
	-1,
	-1,
	-1,
	548,
	2148,
	2149,
	664,
	1060,
	26,
	23,
	122,
	122,
	1050,
	1050,
	26,
	23,
	122,
	122,
	99,
	99,
	109,
	1,
	126,
	1,
	0,
	0,
	0,
	152,
	0,
	0,
	0,
	0,
	0,
	2150,
	1218,
	1218,
	1218,
	2150,
	1218,
	1218,
	1218,
	2150,
	1218,
	1218,
	1218,
	2150,
	1218,
	1218,
	1218,
	2150,
	1218,
	1218,
	1218,
	2150,
	1218,
	1218,
	1218,
	2150,
	1218,
	1218,
	1218,
	2150,
	1218,
	1218,
	1218,
	2150,
	1218,
	1218,
	1218,
	2150,
	1218,
	1218,
	1218,
	2151,
	2152,
	2153,
	0,
	23,
	32,
	23,
	23,
	23,
	23,
	23,
	23,
	28,
	26,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	23,
	23,
	31,
	26,
	23,
	26,
	23,
	23,
	31,
	102,
	23,
	23,
	2154,
	1225,
	2155,
	26,
	102,
	23,
	3,
	23,
	31,
	23,
	23,
	14,
	14,
	10,
	26,
	26,
	23,
	26,
	26,
	1990,
	23,
	111,
	111,
	111,
	111,
	111,
	111,
	23,
	23,
	26,
	23,
	23,
	102,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	3,
	23,
	24,
	23,
	25,
	32,
	2156,
	23,
	25,
	102,
	533,
	23,
	23,
	23,
	23,
	23,
	31,
	23,
	31,
	31,
	23,
	31,
	23,
	23,
	23,
	32,
	23,
	26,
	23,
	23,
	24,
	32,
	2156,
	26,
	967,
	23,
	25,
	26,
	967,
	23,
	23,
	23,
	157,
	911,
	32,
	275,
	856,
	23,
	23,
	24,
	23,
	25,
	157,
	2157,
	911,
	2158,
	23,
	23,
	26,
	23,
	23,
	24,
	27,
	27,
	2159,
	23,
	26,
	26,
	23,
	23,
	26,
	23,
	23,
	23,
	31,
	23,
	31,
	23,
	23,
	31,
	23,
	23,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	313,
	1218,
	122,
	2160,
	324,
	554,
	-1,
	-1,
	171,
	1219,
	1,
	2161,
	2162,
	561,
	-1,
	-1,
	-1,
	-1,
	46,
	207,
	-1,
	-1,
	43,
	1193,
	1161,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	2163,
	23,
	1051,
	2164,
	23,
	26,
	14,
	23,
	23,
	23,
	2165,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	31,
	23,
	31,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	26,
	28,
	23,
	32,
	32,
	10,
	32,
	2166,
	102,
	26,
	2167,
	27,
	2168,
	9,
	26,
	23,
	23,
	23,
	23,
	2169,
	32,
	37,
	23,
	26,
	101,
	180,
	2170,
	130,
	9,
	27,
	23,
	130,
	2170,
	26,
	28,
	58,
	58,
	58,
	3,
	26,
	26,
	2167,
	23,
	161,
	26,
	1035,
	1035,
	1035,
	26,
	23,
	1639,
	10,
	23,
	1319,
	2171,
	23,
	26,
	2172,
	14,
	157,
	2173,
	32,
	2174,
	2175,
	2176,
	2177,
	2178,
	23,
	2179,
	-1,
	2180,
	0,
	122,
	1050,
	1051,
	1055,
	1048,
	1048,
	26,
	1319,
	1062,
	23,
	767,
	2181,
	2182,
	2183,
	1150,
	26,
	664,
	1062,
	9,
	26,
	9,
	1871,
	1872,
	26,
	10,
	23,
	23,
	422,
	422,
	34,
	26,
	10,
	23,
	422,
	422,
	2184,
	27,
	23,
	2185,
	23,
	23,
	102,
	14,
	23,
	23,
	10,
	14,
	26,
	26,
	23,
	23,
	422,
	26,
	10,
	23,
	422,
	26,
	23,
	26,
	10,
	23,
	422,
	26,
	10,
	23,
	422,
	26,
	10,
	23,
	422,
	14,
	28,
	2186,
	9,
	26,
	1639,
	26,
	1639,
	23,
	23,
	23,
	26,
	26,
	27,
	26,
	27,
	23,
	26,
	26,
	23,
	27,
	102,
	9,
	27,
	26,
	26,
	23,
	10,
	23,
	23,
	9,
	23,
	3,
	26,
	23,
	102,
	23,
	23,
	32,
	26,
	23,
	617,
	2188,
	2189,
	23,
	26,
	31,
	23,
	26,
	23,
	23,
	23,
	14,
	23,
	14,
	23,
	4,
	23,
	23,
	10,
	14,
	34,
	10,
	26,
	729,
	23,
	23,
	23,
	23,
	23,
	42,
	390,
	26,
	14,
	23,
	14,
	23,
	111,
	111,
	4,
	3,
	23,
	31,
	23,
	23,
	698,
	698,
	111,
	1860,
	554,
	3,
	109,
	554,
	554,
	554,
	109,
	122,
	23,
	23,
	23,
	-1,
	-1,
	-1,
	-1,
	1570,
	109,
	554,
	554,
	554,
	554,
	775,
	775,
	49,
	23,
	23,
	23,
	111,
	111,
	0,
	2190,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	9,
	23,
	26,
	1639,
	1639,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	3,
	3,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	1639,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	1639,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	1051,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	2036,
	23,
	23,
	23,
	23,
	23,
	4,
	23,
	23,
	23,
	9,
	9,
	23,
	1689,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	102,
	23,
	23,
	23,
	23,
	23,
	275,
	23,
	23,
	23,
	14,
	23,
	23,
	23,
	1187,
	23,
	23,
	23,
	26,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	186,
	3,
	23,
	23,
	3,
	23,
	23,
	3,
	23,
	23,
	3,
	23,
	23,
	163,
	26,
	166,
	26,
	163,
	23,
	101,
	26,
	163,
	26,
	166,
	26,
	32,
	23,
	102,
	14,
	23,
	14,
	32,
	23,
	102,
	14,
	23,
	14,
	163,
	23,
	101,
	26,
	163,
	23,
	101,
	26,
	163,
	23,
	101,
	26,
	163,
	176,
	1428,
	9,
	163,
	35,
	669,
	26,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	23,
	27,
	26,
	14,
	26,
	14,
	163,
	23,
	101,
	26,
	163,
	28,
	166,
	28,
	163,
	23,
	101,
	26,
	23,
	-1,
	-1,
	-1,
	-1,
	163,
	23,
	101,
	26,
	163,
	26,
	166,
	26,
	163,
	156,
	2097,
	26,
	163,
	26,
	166,
	26,
	163,
	390,
	765,
	26,
	163,
	839,
	2098,
	26,
	163,
	1094,
	2099,
	26,
	163,
	27,
	202,
	26,
	163,
	124,
	1009,
	26,
	163,
	877,
	2101,
	26,
	23,
	275,
	1024,
	1225,
	1134,
	2106,
	839,
	839,
	28,
	163,
	23,
	101,
	26,
	163,
	2107,
	2108,
	26,
	163,
	1062,
	2109,
	9,
	163,
	23,
	101,
	26,
	3,
	23,
	41,
	102,
	23,
	3,
	23,
	41,
	664,
	102,
	23,
	163,
	30,
	532,
	9,
	163,
	181,
	166,
	181,
	163,
	102,
	101,
	9,
	163,
	34,
	532,
	28,
	163,
	1097,
	2122,
	28,
	163,
	32,
	532,
	26,
	163,
	23,
	101,
	26,
	163,
	23,
	101,
	26,
	163,
	23,
	101,
	26,
	163,
	1083,
	2123,
	26,
	163,
	26,
	166,
	26,
	163,
	390,
	765,
	26,
	163,
	839,
	2098,
	26,
	163,
	1094,
	2099,
	26,
	163,
	27,
	202,
	26,
	163,
	124,
	1009,
	26,
	23,
	163,
	10,
	101,
	104,
	163,
	34,
	532,
	28,
	3,
	23,
	2124,
	2124,
	30,
	30,
	30,
	181,
	34,
	1097,
	32,
	163,
	1627,
	1628,
	179,
	163,
	28,
	166,
	28,
	163,
	23,
	101,
	26,
	163,
	26,
	166,
	26,
	163,
	101,
	202,
	28,
	2145,
	163,
	390,
	765,
	26,
	163,
	390,
	765,
	26,
	163,
	26,
	166,
	26,
	163,
	1094,
	2099,
	26,
	163,
	390,
	765,
	26,
	23,
	23,
	23,
	3,
	23,
	23,
	839,
	3,
	3,
	23,
	31,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	32,
	23,
	102,
	14,
	23,
	14,
	3,
	23,
	32,
	3,
	23,
	31,
	23,
	23,
	23,
	23,
	3,
	23,
	23,
	163,
	1639,
	2187,
	26,
	3,
	23,
	2191,
	3,
	23,
	9,
	28,
	163,
	1051,
	2109,
	26,
	32,
	23,
	102,
	14,
	23,
	14,
	32,
	23,
	102,
	14,
	23,
	14,
};
static const Il2CppTokenRangePair s_rgctxIndices[39] = 
{
	{ 0x0200004D, { 0, 11 } },
	{ 0x02000098, { 40, 4 } },
	{ 0x020000C1, { 54, 3 } },
	{ 0x020000EC, { 102, 2 } },
	{ 0x020000ED, { 104, 2 } },
	{ 0x02000118, { 106, 4 } },
	{ 0x0200015E, { 11, 1 } },
	{ 0x020001C2, { 51, 1 } },
	{ 0x020001C3, { 52, 1 } },
	{ 0x020001C4, { 53, 1 } },
	{ 0x0600037E, { 12, 2 } },
	{ 0x06000389, { 14, 1 } },
	{ 0x060003A4, { 15, 3 } },
	{ 0x060003A5, { 18, 3 } },
	{ 0x060003A6, { 21, 2 } },
	{ 0x060003A9, { 23, 1 } },
	{ 0x060003AA, { 24, 1 } },
	{ 0x060003CC, { 25, 3 } },
	{ 0x060003D6, { 28, 2 } },
	{ 0x060003D7, { 30, 2 } },
	{ 0x060003DC, { 32, 3 } },
	{ 0x06000586, { 35, 5 } },
	{ 0x060008DA, { 44, 1 } },
	{ 0x060008E0, { 45, 2 } },
	{ 0x060008E1, { 47, 2 } },
	{ 0x060008E2, { 49, 2 } },
	{ 0x060009C7, { 57, 3 } },
	{ 0x060009C8, { 60, 2 } },
	{ 0x060009C9, { 62, 2 } },
	{ 0x060009CA, { 64, 2 } },
	{ 0x060009CB, { 66, 1 } },
	{ 0x060009D2, { 67, 1 } },
	{ 0x060009D3, { 68, 1 } },
	{ 0x060009DA, { 69, 3 } },
	{ 0x060009DB, { 72, 3 } },
	{ 0x060009DC, { 75, 1 } },
	{ 0x060009DD, { 76, 1 } },
	{ 0x060009E0, { 77, 10 } },
	{ 0x060009E1, { 87, 15 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[110] = 
{
	{ (Il2CppRGCTXDataType)2, 20480 },
	{ (Il2CppRGCTXDataType)3, 15416 },
	{ (Il2CppRGCTXDataType)2, 18602 },
	{ (Il2CppRGCTXDataType)3, 15417 },
	{ (Il2CppRGCTXDataType)3, 15418 },
	{ (Il2CppRGCTXDataType)2, 18601 },
	{ (Il2CppRGCTXDataType)3, 15419 },
	{ (Il2CppRGCTXDataType)2, 20481 },
	{ (Il2CppRGCTXDataType)3, 15420 },
	{ (Il2CppRGCTXDataType)3, 15421 },
	{ (Il2CppRGCTXDataType)3, 15422 },
	{ (Il2CppRGCTXDataType)2, 18610 },
	{ (Il2CppRGCTXDataType)1, 18674 },
	{ (Il2CppRGCTXDataType)2, 18673 },
	{ (Il2CppRGCTXDataType)1, 20482 },
	{ (Il2CppRGCTXDataType)1, 18676 },
	{ (Il2CppRGCTXDataType)3, 15423 },
	{ (Il2CppRGCTXDataType)3, 15424 },
	{ (Il2CppRGCTXDataType)1, 18677 },
	{ (Il2CppRGCTXDataType)3, 15425 },
	{ (Il2CppRGCTXDataType)3, 15426 },
	{ (Il2CppRGCTXDataType)3, 15427 },
	{ (Il2CppRGCTXDataType)2, 18678 },
	{ (Il2CppRGCTXDataType)3, 15428 },
	{ (Il2CppRGCTXDataType)3, 15429 },
	{ (Il2CppRGCTXDataType)3, 15430 },
	{ (Il2CppRGCTXDataType)2, 18681 },
	{ (Il2CppRGCTXDataType)3, 15431 },
	{ (Il2CppRGCTXDataType)3, 15432 },
	{ (Il2CppRGCTXDataType)2, 20483 },
	{ (Il2CppRGCTXDataType)3, 15433 },
	{ (Il2CppRGCTXDataType)3, 15434 },
	{ (Il2CppRGCTXDataType)2, 18683 },
	{ (Il2CppRGCTXDataType)3, 15435 },
	{ (Il2CppRGCTXDataType)3, 15436 },
	{ (Il2CppRGCTXDataType)3, 15437 },
	{ (Il2CppRGCTXDataType)2, 18868 },
	{ (Il2CppRGCTXDataType)3, 15438 },
	{ (Il2CppRGCTXDataType)3, 15439 },
	{ (Il2CppRGCTXDataType)1, 18868 },
	{ (Il2CppRGCTXDataType)2, 20485 },
	{ (Il2CppRGCTXDataType)2, 19210 },
	{ (Il2CppRGCTXDataType)1, 19210 },
	{ (Il2CppRGCTXDataType)3, 15440 },
	{ (Il2CppRGCTXDataType)2, 19276 },
	{ (Il2CppRGCTXDataType)2, 20486 },
	{ (Il2CppRGCTXDataType)3, 15441 },
	{ (Il2CppRGCTXDataType)2, 20487 },
	{ (Il2CppRGCTXDataType)3, 15442 },
	{ (Il2CppRGCTXDataType)2, 20488 },
	{ (Il2CppRGCTXDataType)3, 15443 },
	{ (Il2CppRGCTXDataType)3, 15444 },
	{ (Il2CppRGCTXDataType)3, 15445 },
	{ (Il2CppRGCTXDataType)3, 15446 },
	{ (Il2CppRGCTXDataType)3, 15447 },
	{ (Il2CppRGCTXDataType)3, 15448 },
	{ (Il2CppRGCTXDataType)2, 19339 },
	{ (Il2CppRGCTXDataType)1, 19389 },
	{ (Il2CppRGCTXDataType)2, 19389 },
	{ (Il2CppRGCTXDataType)3, 15449 },
	{ (Il2CppRGCTXDataType)1, 20489 },
	{ (Il2CppRGCTXDataType)2, 20489 },
	{ (Il2CppRGCTXDataType)1, 19390 },
	{ (Il2CppRGCTXDataType)2, 19390 },
	{ (Il2CppRGCTXDataType)1, 19391 },
	{ (Il2CppRGCTXDataType)2, 19391 },
	{ (Il2CppRGCTXDataType)1, 20490 },
	{ (Il2CppRGCTXDataType)3, 15450 },
	{ (Il2CppRGCTXDataType)3, 15451 },
	{ (Il2CppRGCTXDataType)2, 19399 },
	{ (Il2CppRGCTXDataType)3, 15452 },
	{ (Il2CppRGCTXDataType)3, 15453 },
	{ (Il2CppRGCTXDataType)2, 19401 },
	{ (Il2CppRGCTXDataType)3, 15454 },
	{ (Il2CppRGCTXDataType)3, 15455 },
	{ (Il2CppRGCTXDataType)2, 19404 },
	{ (Il2CppRGCTXDataType)2, 19405 },
	{ (Il2CppRGCTXDataType)2, 20491 },
	{ (Il2CppRGCTXDataType)3, 15456 },
	{ (Il2CppRGCTXDataType)3, 15457 },
	{ (Il2CppRGCTXDataType)3, 15458 },
	{ (Il2CppRGCTXDataType)3, 15459 },
	{ (Il2CppRGCTXDataType)3, 15460 },
	{ (Il2CppRGCTXDataType)3, 15461 },
	{ (Il2CppRGCTXDataType)3, 15462 },
	{ (Il2CppRGCTXDataType)2, 20492 },
	{ (Il2CppRGCTXDataType)3, 15463 },
	{ (Il2CppRGCTXDataType)3, 15464 },
	{ (Il2CppRGCTXDataType)3, 15465 },
	{ (Il2CppRGCTXDataType)3, 15466 },
	{ (Il2CppRGCTXDataType)3, 15467 },
	{ (Il2CppRGCTXDataType)2, 20493 },
	{ (Il2CppRGCTXDataType)3, 15468 },
	{ (Il2CppRGCTXDataType)3, 15469 },
	{ (Il2CppRGCTXDataType)3, 15470 },
	{ (Il2CppRGCTXDataType)3, 15471 },
	{ (Il2CppRGCTXDataType)3, 15472 },
	{ (Il2CppRGCTXDataType)2, 20494 },
	{ (Il2CppRGCTXDataType)3, 15473 },
	{ (Il2CppRGCTXDataType)2, 20495 },
	{ (Il2CppRGCTXDataType)3, 15474 },
	{ (Il2CppRGCTXDataType)3, 15475 },
	{ (Il2CppRGCTXDataType)2, 20496 },
	{ (Il2CppRGCTXDataType)3, 15476 },
	{ (Il2CppRGCTXDataType)2, 20497 },
	{ (Il2CppRGCTXDataType)2, 19470 },
	{ (Il2CppRGCTXDataType)2, 20498 },
	{ (Il2CppRGCTXDataType)2, 19662 },
	{ (Il2CppRGCTXDataType)1, 19662 },
	{ (Il2CppRGCTXDataType)3, 15477 },
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	3362,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	39,
	s_rgctxIndices,
	110,
	s_rgctxValues,
	NULL,
};
