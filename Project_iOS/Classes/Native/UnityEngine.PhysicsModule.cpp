﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.String
struct String_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.BoxCollider
struct BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA;
// UnityEngine.CapsuleCollider
struct CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1;
// UnityEngine.CharacterController
struct CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E;
// UnityEngine.CharacterJoint
struct CharacterJoint_tDA58379B9EB6DAF19FE5FD664FBA59B3843CEFE2;
// UnityEngine.Collider
struct Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF;
// UnityEngine.Collider[]
struct ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252;
// UnityEngine.Collision
struct Collision_t7FF0F4B0E24A2AEB1131DD980F63AB8CBF11FC3C;
// UnityEngine.Component
struct Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621;
// UnityEngine.ConfigurableJoint
struct ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7;
// UnityEngine.ContactPoint[]
struct ContactPointU5BU5D_t10BB5D5BFFFA3C919FD97DFDEDB49D954AFB8EAA;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.HingeJoint
struct HingeJoint_tDA10BC5B1D01F5226ED5B0F83D106E296925036D;
// UnityEngine.Joint
struct Joint_t734F77C82E6372173403FCA61D9E7FC4618595AF;
// UnityEngine.Mesh
struct Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C;
// UnityEngine.MeshCollider
struct MeshCollider_t60EB55ADE92499FE8D1AA206D2BD96E65B2766DE;
// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0;
// UnityEngine.PhysicMaterial
struct PhysicMaterial_tBEBB6F4620A5221A4CBAEDB2E5984CCA70AA07F8;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57;
// UnityEngine.Rigidbody
struct Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5;
// UnityEngine.SphereCollider
struct SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F;
// UnityEngine.SpringJoint
struct SpringJoint_t2D196194480E339F728B463619720385523B09AF;

IL2CPP_EXTERN_C RuntimeClass* Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ContactPointU5BU5D_t10BB5D5BFFFA3C919FD97DFDEDB49D954AFB8EAA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral62C02D6AE723470CE8452EABBE91DD3AB73F31F4;
IL2CPP_EXTERN_C String_t* _stringLiteralC76B3E2116774CE05868F05B32DDBF1C6F86EAB8;
IL2CPP_EXTERN_C const uint32_t Collision_get_contacts_m3807F7784D655257D7153CB615EF1FF7FAEAE0CF_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Collision_get_gameObject_m9A7069ABE50D4BB957A8ED76E5F4A59ACEC57C49_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PhysicMaterial__ctor_m1E6AB8A9A487E70DE44AF221946176BE96584DFC_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PhysicsScene_Equals_m538C6A86CCC4EF0546590BAC51E5D08ACC2CBEF9_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PhysicsScene_Internal_SphereCast_mAD5109B24E5C668DC9A49E434B5139CA8A43DBD0_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PhysicsScene_Raycast_m90D1AE1F641D9344BEBEF32E7AC0BAC883024C0B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PhysicsScene_Raycast_mA90972A8E828722A26EBC754EABB3086BE6E2336_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PhysicsScene_ToString_m192392A2C486E2F76AAC736C519CB117BD187BE9_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Physics_Linecast_m2CAAB76F9786AB83FCF6B9C751B6A754AAEA48FB_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Physics_Linecast_mCAA3A0F713478346F006ADF57D65F64C89A544C6_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Physics_RaycastAll_m6627435AE46B6720066FEEA5A80923420EE8FD2D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t RaycastHit_get_collider_mE70B84C4312B567344F60992A6067855F2C3A7A9_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t RaycastHit_get_rigidbody_m8E28BDE09DC588AAF0C15182AFF3C00EE11EB0FC_MetadataUsageId;
struct ContactPoint_tE0D3A30ED34A1FC8CA3F7391348429F3232CA515 ;

struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
struct ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252;
struct ContactPointU5BU5D_t10BB5D5BFFFA3C919FD97DFDEDB49D954AFB8EAA;
struct RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t1ABA099244C4281E9C7E9402BE77B2165BA664F6 
{
public:

public:
};


// System.Object

struct Il2CppArrayBounds;

// System.Array


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};

// UnityEngine.Physics
struct  Physics_t795152566290B75B831BBCB079E5F82B1BAA93B2  : public RuntimeObject
{
public:

public:
};


// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Single
struct  Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};


// UnityEngine.JointDrive
struct  JointDrive_t2082D48A52244171DA92ED5FC110D2A58BCBB575 
{
public:
	// System.Single UnityEngine.JointDrive::m_PositionSpring
	float ___m_PositionSpring_0;
	// System.Single UnityEngine.JointDrive::m_PositionDamper
	float ___m_PositionDamper_1;
	// System.Single UnityEngine.JointDrive::m_MaximumForce
	float ___m_MaximumForce_2;

public:
	inline static int32_t get_offset_of_m_PositionSpring_0() { return static_cast<int32_t>(offsetof(JointDrive_t2082D48A52244171DA92ED5FC110D2A58BCBB575, ___m_PositionSpring_0)); }
	inline float get_m_PositionSpring_0() const { return ___m_PositionSpring_0; }
	inline float* get_address_of_m_PositionSpring_0() { return &___m_PositionSpring_0; }
	inline void set_m_PositionSpring_0(float value)
	{
		___m_PositionSpring_0 = value;
	}

	inline static int32_t get_offset_of_m_PositionDamper_1() { return static_cast<int32_t>(offsetof(JointDrive_t2082D48A52244171DA92ED5FC110D2A58BCBB575, ___m_PositionDamper_1)); }
	inline float get_m_PositionDamper_1() const { return ___m_PositionDamper_1; }
	inline float* get_address_of_m_PositionDamper_1() { return &___m_PositionDamper_1; }
	inline void set_m_PositionDamper_1(float value)
	{
		___m_PositionDamper_1 = value;
	}

	inline static int32_t get_offset_of_m_MaximumForce_2() { return static_cast<int32_t>(offsetof(JointDrive_t2082D48A52244171DA92ED5FC110D2A58BCBB575, ___m_MaximumForce_2)); }
	inline float get_m_MaximumForce_2() const { return ___m_MaximumForce_2; }
	inline float* get_address_of_m_MaximumForce_2() { return &___m_MaximumForce_2; }
	inline void set_m_MaximumForce_2(float value)
	{
		___m_MaximumForce_2 = value;
	}
};


// UnityEngine.JointLimits
struct  JointLimits_t89D8D0E21FB1E840F8380AFB9FB55E966A3030F3 
{
public:
	// System.Single UnityEngine.JointLimits::m_Min
	float ___m_Min_0;
	// System.Single UnityEngine.JointLimits::m_Max
	float ___m_Max_1;
	// System.Single UnityEngine.JointLimits::m_Bounciness
	float ___m_Bounciness_2;
	// System.Single UnityEngine.JointLimits::m_BounceMinVelocity
	float ___m_BounceMinVelocity_3;
	// System.Single UnityEngine.JointLimits::m_ContactDistance
	float ___m_ContactDistance_4;
	// System.Single UnityEngine.JointLimits::minBounce
	float ___minBounce_5;
	// System.Single UnityEngine.JointLimits::maxBounce
	float ___maxBounce_6;

public:
	inline static int32_t get_offset_of_m_Min_0() { return static_cast<int32_t>(offsetof(JointLimits_t89D8D0E21FB1E840F8380AFB9FB55E966A3030F3, ___m_Min_0)); }
	inline float get_m_Min_0() const { return ___m_Min_0; }
	inline float* get_address_of_m_Min_0() { return &___m_Min_0; }
	inline void set_m_Min_0(float value)
	{
		___m_Min_0 = value;
	}

	inline static int32_t get_offset_of_m_Max_1() { return static_cast<int32_t>(offsetof(JointLimits_t89D8D0E21FB1E840F8380AFB9FB55E966A3030F3, ___m_Max_1)); }
	inline float get_m_Max_1() const { return ___m_Max_1; }
	inline float* get_address_of_m_Max_1() { return &___m_Max_1; }
	inline void set_m_Max_1(float value)
	{
		___m_Max_1 = value;
	}

	inline static int32_t get_offset_of_m_Bounciness_2() { return static_cast<int32_t>(offsetof(JointLimits_t89D8D0E21FB1E840F8380AFB9FB55E966A3030F3, ___m_Bounciness_2)); }
	inline float get_m_Bounciness_2() const { return ___m_Bounciness_2; }
	inline float* get_address_of_m_Bounciness_2() { return &___m_Bounciness_2; }
	inline void set_m_Bounciness_2(float value)
	{
		___m_Bounciness_2 = value;
	}

	inline static int32_t get_offset_of_m_BounceMinVelocity_3() { return static_cast<int32_t>(offsetof(JointLimits_t89D8D0E21FB1E840F8380AFB9FB55E966A3030F3, ___m_BounceMinVelocity_3)); }
	inline float get_m_BounceMinVelocity_3() const { return ___m_BounceMinVelocity_3; }
	inline float* get_address_of_m_BounceMinVelocity_3() { return &___m_BounceMinVelocity_3; }
	inline void set_m_BounceMinVelocity_3(float value)
	{
		___m_BounceMinVelocity_3 = value;
	}

	inline static int32_t get_offset_of_m_ContactDistance_4() { return static_cast<int32_t>(offsetof(JointLimits_t89D8D0E21FB1E840F8380AFB9FB55E966A3030F3, ___m_ContactDistance_4)); }
	inline float get_m_ContactDistance_4() const { return ___m_ContactDistance_4; }
	inline float* get_address_of_m_ContactDistance_4() { return &___m_ContactDistance_4; }
	inline void set_m_ContactDistance_4(float value)
	{
		___m_ContactDistance_4 = value;
	}

	inline static int32_t get_offset_of_minBounce_5() { return static_cast<int32_t>(offsetof(JointLimits_t89D8D0E21FB1E840F8380AFB9FB55E966A3030F3, ___minBounce_5)); }
	inline float get_minBounce_5() const { return ___minBounce_5; }
	inline float* get_address_of_minBounce_5() { return &___minBounce_5; }
	inline void set_minBounce_5(float value)
	{
		___minBounce_5 = value;
	}

	inline static int32_t get_offset_of_maxBounce_6() { return static_cast<int32_t>(offsetof(JointLimits_t89D8D0E21FB1E840F8380AFB9FB55E966A3030F3, ___maxBounce_6)); }
	inline float get_maxBounce_6() const { return ___maxBounce_6; }
	inline float* get_address_of_maxBounce_6() { return &___maxBounce_6; }
	inline void set_maxBounce_6(float value)
	{
		___maxBounce_6 = value;
	}
};


// UnityEngine.JointSpring
struct  JointSpring_t5BA0622E25A91A16BB70A8598BCCD11DD93FB3CD 
{
public:
	// System.Single UnityEngine.JointSpring::spring
	float ___spring_0;
	// System.Single UnityEngine.JointSpring::damper
	float ___damper_1;
	// System.Single UnityEngine.JointSpring::targetPosition
	float ___targetPosition_2;

public:
	inline static int32_t get_offset_of_spring_0() { return static_cast<int32_t>(offsetof(JointSpring_t5BA0622E25A91A16BB70A8598BCCD11DD93FB3CD, ___spring_0)); }
	inline float get_spring_0() const { return ___spring_0; }
	inline float* get_address_of_spring_0() { return &___spring_0; }
	inline void set_spring_0(float value)
	{
		___spring_0 = value;
	}

	inline static int32_t get_offset_of_damper_1() { return static_cast<int32_t>(offsetof(JointSpring_t5BA0622E25A91A16BB70A8598BCCD11DD93FB3CD, ___damper_1)); }
	inline float get_damper_1() const { return ___damper_1; }
	inline float* get_address_of_damper_1() { return &___damper_1; }
	inline void set_damper_1(float value)
	{
		___damper_1 = value;
	}

	inline static int32_t get_offset_of_targetPosition_2() { return static_cast<int32_t>(offsetof(JointSpring_t5BA0622E25A91A16BB70A8598BCCD11DD93FB3CD, ___targetPosition_2)); }
	inline float get_targetPosition_2() const { return ___targetPosition_2; }
	inline float* get_address_of_targetPosition_2() { return &___targetPosition_2; }
	inline void set_targetPosition_2(float value)
	{
		___targetPosition_2 = value;
	}
};


// UnityEngine.PhysicsScene
struct  PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 
{
public:
	// System.Int32 UnityEngine.PhysicsScene::m_Handle
	int32_t ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5, ___m_Handle_0)); }
	inline int32_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline int32_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(int32_t value)
	{
		___m_Handle_0 = value;
	}
};


// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};


// UnityEngine.SoftJointLimit
struct  SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 
{
public:
	// System.Single UnityEngine.SoftJointLimit::m_Limit
	float ___m_Limit_0;
	// System.Single UnityEngine.SoftJointLimit::m_Bounciness
	float ___m_Bounciness_1;
	// System.Single UnityEngine.SoftJointLimit::m_ContactDistance
	float ___m_ContactDistance_2;

public:
	inline static int32_t get_offset_of_m_Limit_0() { return static_cast<int32_t>(offsetof(SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9, ___m_Limit_0)); }
	inline float get_m_Limit_0() const { return ___m_Limit_0; }
	inline float* get_address_of_m_Limit_0() { return &___m_Limit_0; }
	inline void set_m_Limit_0(float value)
	{
		___m_Limit_0 = value;
	}

	inline static int32_t get_offset_of_m_Bounciness_1() { return static_cast<int32_t>(offsetof(SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9, ___m_Bounciness_1)); }
	inline float get_m_Bounciness_1() const { return ___m_Bounciness_1; }
	inline float* get_address_of_m_Bounciness_1() { return &___m_Bounciness_1; }
	inline void set_m_Bounciness_1(float value)
	{
		___m_Bounciness_1 = value;
	}

	inline static int32_t get_offset_of_m_ContactDistance_2() { return static_cast<int32_t>(offsetof(SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9, ___m_ContactDistance_2)); }
	inline float get_m_ContactDistance_2() const { return ___m_ContactDistance_2; }
	inline float* get_address_of_m_ContactDistance_2() { return &___m_ContactDistance_2; }
	inline void set_m_ContactDistance_2(float value)
	{
		___m_ContactDistance_2 = value;
	}
};


// UnityEngine.SoftJointLimitSpring
struct  SoftJointLimitSpring_t8D4D658B29266F9573CB134D0DBC15017488FDC2 
{
public:
	// System.Single UnityEngine.SoftJointLimitSpring::m_Spring
	float ___m_Spring_0;
	// System.Single UnityEngine.SoftJointLimitSpring::m_Damper
	float ___m_Damper_1;

public:
	inline static int32_t get_offset_of_m_Spring_0() { return static_cast<int32_t>(offsetof(SoftJointLimitSpring_t8D4D658B29266F9573CB134D0DBC15017488FDC2, ___m_Spring_0)); }
	inline float get_m_Spring_0() const { return ___m_Spring_0; }
	inline float* get_address_of_m_Spring_0() { return &___m_Spring_0; }
	inline void set_m_Spring_0(float value)
	{
		___m_Spring_0 = value;
	}

	inline static int32_t get_offset_of_m_Damper_1() { return static_cast<int32_t>(offsetof(SoftJointLimitSpring_t8D4D658B29266F9573CB134D0DBC15017488FDC2, ___m_Damper_1)); }
	inline float get_m_Damper_1() const { return ___m_Damper_1; }
	inline float* get_address_of_m_Damper_1() { return &___m_Damper_1; }
	inline void set_m_Damper_1(float value)
	{
		___m_Damper_1 = value;
	}
};


// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// UnityEngine.Bounds
struct  Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890, ___m_Center_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890, ___m_Extents_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Extents_1 = value;
	}
};


// UnityEngine.Collision
struct  Collision_t7FF0F4B0E24A2AEB1131DD980F63AB8CBF11FC3C  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 UnityEngine.Collision::m_Impulse
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Impulse_0;
	// UnityEngine.Vector3 UnityEngine.Collision::m_RelativeVelocity
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_RelativeVelocity_1;
	// UnityEngine.Rigidbody UnityEngine.Collision::m_Rigidbody
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___m_Rigidbody_2;
	// UnityEngine.Collider UnityEngine.Collision::m_Collider
	Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * ___m_Collider_3;
	// System.Int32 UnityEngine.Collision::m_ContactCount
	int32_t ___m_ContactCount_4;
	// UnityEngine.ContactPoint[] UnityEngine.Collision::m_ReusedContacts
	ContactPointU5BU5D_t10BB5D5BFFFA3C919FD97DFDEDB49D954AFB8EAA* ___m_ReusedContacts_5;
	// UnityEngine.ContactPoint[] UnityEngine.Collision::m_LegacyContacts
	ContactPointU5BU5D_t10BB5D5BFFFA3C919FD97DFDEDB49D954AFB8EAA* ___m_LegacyContacts_6;

public:
	inline static int32_t get_offset_of_m_Impulse_0() { return static_cast<int32_t>(offsetof(Collision_t7FF0F4B0E24A2AEB1131DD980F63AB8CBF11FC3C, ___m_Impulse_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Impulse_0() const { return ___m_Impulse_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Impulse_0() { return &___m_Impulse_0; }
	inline void set_m_Impulse_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Impulse_0 = value;
	}

	inline static int32_t get_offset_of_m_RelativeVelocity_1() { return static_cast<int32_t>(offsetof(Collision_t7FF0F4B0E24A2AEB1131DD980F63AB8CBF11FC3C, ___m_RelativeVelocity_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_RelativeVelocity_1() const { return ___m_RelativeVelocity_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_RelativeVelocity_1() { return &___m_RelativeVelocity_1; }
	inline void set_m_RelativeVelocity_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_RelativeVelocity_1 = value;
	}

	inline static int32_t get_offset_of_m_Rigidbody_2() { return static_cast<int32_t>(offsetof(Collision_t7FF0F4B0E24A2AEB1131DD980F63AB8CBF11FC3C, ___m_Rigidbody_2)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get_m_Rigidbody_2() const { return ___m_Rigidbody_2; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of_m_Rigidbody_2() { return &___m_Rigidbody_2; }
	inline void set_m_Rigidbody_2(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		___m_Rigidbody_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Rigidbody_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_Collider_3() { return static_cast<int32_t>(offsetof(Collision_t7FF0F4B0E24A2AEB1131DD980F63AB8CBF11FC3C, ___m_Collider_3)); }
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * get_m_Collider_3() const { return ___m_Collider_3; }
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF ** get_address_of_m_Collider_3() { return &___m_Collider_3; }
	inline void set_m_Collider_3(Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * value)
	{
		___m_Collider_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Collider_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_ContactCount_4() { return static_cast<int32_t>(offsetof(Collision_t7FF0F4B0E24A2AEB1131DD980F63AB8CBF11FC3C, ___m_ContactCount_4)); }
	inline int32_t get_m_ContactCount_4() const { return ___m_ContactCount_4; }
	inline int32_t* get_address_of_m_ContactCount_4() { return &___m_ContactCount_4; }
	inline void set_m_ContactCount_4(int32_t value)
	{
		___m_ContactCount_4 = value;
	}

	inline static int32_t get_offset_of_m_ReusedContacts_5() { return static_cast<int32_t>(offsetof(Collision_t7FF0F4B0E24A2AEB1131DD980F63AB8CBF11FC3C, ___m_ReusedContacts_5)); }
	inline ContactPointU5BU5D_t10BB5D5BFFFA3C919FD97DFDEDB49D954AFB8EAA* get_m_ReusedContacts_5() const { return ___m_ReusedContacts_5; }
	inline ContactPointU5BU5D_t10BB5D5BFFFA3C919FD97DFDEDB49D954AFB8EAA** get_address_of_m_ReusedContacts_5() { return &___m_ReusedContacts_5; }
	inline void set_m_ReusedContacts_5(ContactPointU5BU5D_t10BB5D5BFFFA3C919FD97DFDEDB49D954AFB8EAA* value)
	{
		___m_ReusedContacts_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ReusedContacts_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_LegacyContacts_6() { return static_cast<int32_t>(offsetof(Collision_t7FF0F4B0E24A2AEB1131DD980F63AB8CBF11FC3C, ___m_LegacyContacts_6)); }
	inline ContactPointU5BU5D_t10BB5D5BFFFA3C919FD97DFDEDB49D954AFB8EAA* get_m_LegacyContacts_6() const { return ___m_LegacyContacts_6; }
	inline ContactPointU5BU5D_t10BB5D5BFFFA3C919FD97DFDEDB49D954AFB8EAA** get_address_of_m_LegacyContacts_6() { return &___m_LegacyContacts_6; }
	inline void set_m_LegacyContacts_6(ContactPointU5BU5D_t10BB5D5BFFFA3C919FD97DFDEDB49D954AFB8EAA* value)
	{
		___m_LegacyContacts_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_LegacyContacts_6), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Collision
struct Collision_t7FF0F4B0E24A2AEB1131DD980F63AB8CBF11FC3C_marshaled_pinvoke
{
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Impulse_0;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_RelativeVelocity_1;
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___m_Rigidbody_2;
	Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * ___m_Collider_3;
	int32_t ___m_ContactCount_4;
	ContactPoint_tE0D3A30ED34A1FC8CA3F7391348429F3232CA515 * ___m_ReusedContacts_5;
	ContactPoint_tE0D3A30ED34A1FC8CA3F7391348429F3232CA515 * ___m_LegacyContacts_6;
};
// Native definition for COM marshalling of UnityEngine.Collision
struct Collision_t7FF0F4B0E24A2AEB1131DD980F63AB8CBF11FC3C_marshaled_com
{
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Impulse_0;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_RelativeVelocity_1;
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___m_Rigidbody_2;
	Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * ___m_Collider_3;
	int32_t ___m_ContactCount_4;
	ContactPoint_tE0D3A30ED34A1FC8CA3F7391348429F3232CA515 * ___m_ReusedContacts_5;
	ContactPoint_tE0D3A30ED34A1FC8CA3F7391348429F3232CA515 * ___m_LegacyContacts_6;
};

// UnityEngine.CollisionFlags
struct  CollisionFlags_t5638B0B2D65637D85403CB6FC2279DCBBB8AF63E 
{
public:
	// System.Int32 UnityEngine.CollisionFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CollisionFlags_t5638B0B2D65637D85403CB6FC2279DCBBB8AF63E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.ConfigurableJointMotion
struct  ConfigurableJointMotion_tD72511CF7D21F849BF07DE493FB9E1961A730E0D 
{
public:
	// System.Int32 UnityEngine.ConfigurableJointMotion::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ConfigurableJointMotion_tD72511CF7D21F849BF07DE493FB9E1961A730E0D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.ContactPoint
struct  ContactPoint_tE0D3A30ED34A1FC8CA3F7391348429F3232CA515 
{
public:
	// UnityEngine.Vector3 UnityEngine.ContactPoint::m_Point
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.ContactPoint::m_Normal
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Normal_1;
	// System.Int32 UnityEngine.ContactPoint::m_ThisColliderInstanceID
	int32_t ___m_ThisColliderInstanceID_2;
	// System.Int32 UnityEngine.ContactPoint::m_OtherColliderInstanceID
	int32_t ___m_OtherColliderInstanceID_3;
	// System.Single UnityEngine.ContactPoint::m_Separation
	float ___m_Separation_4;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(ContactPoint_tE0D3A30ED34A1FC8CA3F7391348429F3232CA515, ___m_Point_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(ContactPoint_tE0D3A30ED34A1FC8CA3F7391348429F3232CA515, ___m_Normal_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_ThisColliderInstanceID_2() { return static_cast<int32_t>(offsetof(ContactPoint_tE0D3A30ED34A1FC8CA3F7391348429F3232CA515, ___m_ThisColliderInstanceID_2)); }
	inline int32_t get_m_ThisColliderInstanceID_2() const { return ___m_ThisColliderInstanceID_2; }
	inline int32_t* get_address_of_m_ThisColliderInstanceID_2() { return &___m_ThisColliderInstanceID_2; }
	inline void set_m_ThisColliderInstanceID_2(int32_t value)
	{
		___m_ThisColliderInstanceID_2 = value;
	}

	inline static int32_t get_offset_of_m_OtherColliderInstanceID_3() { return static_cast<int32_t>(offsetof(ContactPoint_tE0D3A30ED34A1FC8CA3F7391348429F3232CA515, ___m_OtherColliderInstanceID_3)); }
	inline int32_t get_m_OtherColliderInstanceID_3() const { return ___m_OtherColliderInstanceID_3; }
	inline int32_t* get_address_of_m_OtherColliderInstanceID_3() { return &___m_OtherColliderInstanceID_3; }
	inline void set_m_OtherColliderInstanceID_3(int32_t value)
	{
		___m_OtherColliderInstanceID_3 = value;
	}

	inline static int32_t get_offset_of_m_Separation_4() { return static_cast<int32_t>(offsetof(ContactPoint_tE0D3A30ED34A1FC8CA3F7391348429F3232CA515, ___m_Separation_4)); }
	inline float get_m_Separation_4() const { return ___m_Separation_4; }
	inline float* get_address_of_m_Separation_4() { return &___m_Separation_4; }
	inline void set_m_Separation_4(float value)
	{
		___m_Separation_4 = value;
	}
};


// UnityEngine.ControllerColliderHit
struct  ControllerColliderHit_tB009AA7F769B4A3E988DEF71F4C5A29AB6A38968  : public RuntimeObject
{
public:
	// UnityEngine.CharacterController UnityEngine.ControllerColliderHit::m_Controller
	CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * ___m_Controller_0;
	// UnityEngine.Collider UnityEngine.ControllerColliderHit::m_Collider
	Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * ___m_Collider_1;
	// UnityEngine.Vector3 UnityEngine.ControllerColliderHit::m_Point
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Point_2;
	// UnityEngine.Vector3 UnityEngine.ControllerColliderHit::m_Normal
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Normal_3;
	// UnityEngine.Vector3 UnityEngine.ControllerColliderHit::m_MoveDirection
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_MoveDirection_4;
	// System.Single UnityEngine.ControllerColliderHit::m_MoveLength
	float ___m_MoveLength_5;
	// System.Int32 UnityEngine.ControllerColliderHit::m_Push
	int32_t ___m_Push_6;

public:
	inline static int32_t get_offset_of_m_Controller_0() { return static_cast<int32_t>(offsetof(ControllerColliderHit_tB009AA7F769B4A3E988DEF71F4C5A29AB6A38968, ___m_Controller_0)); }
	inline CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * get_m_Controller_0() const { return ___m_Controller_0; }
	inline CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E ** get_address_of_m_Controller_0() { return &___m_Controller_0; }
	inline void set_m_Controller_0(CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * value)
	{
		___m_Controller_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Controller_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Collider_1() { return static_cast<int32_t>(offsetof(ControllerColliderHit_tB009AA7F769B4A3E988DEF71F4C5A29AB6A38968, ___m_Collider_1)); }
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * get_m_Collider_1() const { return ___m_Collider_1; }
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF ** get_address_of_m_Collider_1() { return &___m_Collider_1; }
	inline void set_m_Collider_1(Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * value)
	{
		___m_Collider_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Collider_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_Point_2() { return static_cast<int32_t>(offsetof(ControllerColliderHit_tB009AA7F769B4A3E988DEF71F4C5A29AB6A38968, ___m_Point_2)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Point_2() const { return ___m_Point_2; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Point_2() { return &___m_Point_2; }
	inline void set_m_Point_2(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Point_2 = value;
	}

	inline static int32_t get_offset_of_m_Normal_3() { return static_cast<int32_t>(offsetof(ControllerColliderHit_tB009AA7F769B4A3E988DEF71F4C5A29AB6A38968, ___m_Normal_3)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Normal_3() const { return ___m_Normal_3; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Normal_3() { return &___m_Normal_3; }
	inline void set_m_Normal_3(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Normal_3 = value;
	}

	inline static int32_t get_offset_of_m_MoveDirection_4() { return static_cast<int32_t>(offsetof(ControllerColliderHit_tB009AA7F769B4A3E988DEF71F4C5A29AB6A38968, ___m_MoveDirection_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_MoveDirection_4() const { return ___m_MoveDirection_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_MoveDirection_4() { return &___m_MoveDirection_4; }
	inline void set_m_MoveDirection_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_MoveDirection_4 = value;
	}

	inline static int32_t get_offset_of_m_MoveLength_5() { return static_cast<int32_t>(offsetof(ControllerColliderHit_tB009AA7F769B4A3E988DEF71F4C5A29AB6A38968, ___m_MoveLength_5)); }
	inline float get_m_MoveLength_5() const { return ___m_MoveLength_5; }
	inline float* get_address_of_m_MoveLength_5() { return &___m_MoveLength_5; }
	inline void set_m_MoveLength_5(float value)
	{
		___m_MoveLength_5 = value;
	}

	inline static int32_t get_offset_of_m_Push_6() { return static_cast<int32_t>(offsetof(ControllerColliderHit_tB009AA7F769B4A3E988DEF71F4C5A29AB6A38968, ___m_Push_6)); }
	inline int32_t get_m_Push_6() const { return ___m_Push_6; }
	inline int32_t* get_address_of_m_Push_6() { return &___m_Push_6; }
	inline void set_m_Push_6(int32_t value)
	{
		___m_Push_6 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.ControllerColliderHit
struct ControllerColliderHit_tB009AA7F769B4A3E988DEF71F4C5A29AB6A38968_marshaled_pinvoke
{
	CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * ___m_Controller_0;
	Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * ___m_Collider_1;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Point_2;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Normal_3;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_MoveDirection_4;
	float ___m_MoveLength_5;
	int32_t ___m_Push_6;
};
// Native definition for COM marshalling of UnityEngine.ControllerColliderHit
struct ControllerColliderHit_tB009AA7F769B4A3E988DEF71F4C5A29AB6A38968_marshaled_com
{
	CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * ___m_Controller_0;
	Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * ___m_Collider_1;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Point_2;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Normal_3;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_MoveDirection_4;
	float ___m_MoveLength_5;
	int32_t ___m_Push_6;
};

// UnityEngine.ForceMode
struct  ForceMode_t76188FF14D0038E184106555207A81218E97D0A5 
{
public:
	// System.Int32 UnityEngine.ForceMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ForceMode_t76188FF14D0038E184106555207A81218E97D0A5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.JointProjectionMode
struct  JointProjectionMode_tF844FC948B1D15FE4E8EB939DEC9902D5028D8C7 
{
public:
	// System.Int32 UnityEngine.JointProjectionMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(JointProjectionMode_tF844FC948B1D15FE4E8EB939DEC9902D5028D8C7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.PhysicMaterialCombine
struct  PhysicMaterialCombine_tC0ACD5BFB99EA82B9D6C908D767D41E259403482 
{
public:
	// System.Int32 UnityEngine.PhysicMaterialCombine::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PhysicMaterialCombine_tC0ACD5BFB99EA82B9D6C908D767D41E259403482, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.QueryTriggerInteraction
struct  QueryTriggerInteraction_t56ABDF7B81C5D989BE40455CF39647A532791739 
{
public:
	// System.Int32 UnityEngine.QueryTriggerInteraction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(QueryTriggerInteraction_t56ABDF7B81C5D989BE40455CF39647A532791739, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Ray
struct  Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2, ___m_Origin_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2, ___m_Direction_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Direction_1 = value;
	}
};


// UnityEngine.RaycastHit
struct  RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Normal_1;
	// System.UInt32 UnityEngine.RaycastHit::m_FaceID
	uint32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_UV_4;
	// System.Int32 UnityEngine.RaycastHit::m_Collider
	int32_t ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Point_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Normal_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_FaceID_2)); }
	inline uint32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline uint32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(uint32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_UV_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Collider_5)); }
	inline int32_t get_m_Collider_5() const { return ___m_Collider_5; }
	inline int32_t* get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(int32_t value)
	{
		___m_Collider_5 = value;
	}
};


// UnityEngine.RigidbodyConstraints
struct  RigidbodyConstraints_tC57FF2BFB454674CE1FDEF9EEDAF5FCFAB23FFC8 
{
public:
	// System.Int32 UnityEngine.RigidbodyConstraints::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RigidbodyConstraints_tC57FF2BFB454674CE1FDEF9EEDAF5FCFAB23FFC8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.RigidbodyInterpolation
struct  RigidbodyInterpolation_t00D5C0028310C1C0D18B1148F59071F354987F95 
{
public:
	// System.Int32 UnityEngine.RigidbodyInterpolation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RigidbodyInterpolation_t00D5C0028310C1C0D18B1148F59071F354987F95, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.RotationDriveMode
struct  RotationDriveMode_tA709F9631428894E89C36E54EBD1E61C8890B67E 
{
public:
	// System.Int32 UnityEngine.RotationDriveMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RotationDriveMode_tA709F9631428894E89C36E54EBD1E61C8890B67E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.GameObject
struct  GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.Mesh
struct  Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.PhysicMaterial
struct  PhysicMaterial_tBEBB6F4620A5221A4CBAEDB2E5984CCA70AA07F8  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.Collider
struct  Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.Joint
struct  Joint_t734F77C82E6372173403FCA61D9E7FC4618595AF  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.Rigidbody
struct  Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.BoxCollider
struct  BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA  : public Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF
{
public:

public:
};


// UnityEngine.CapsuleCollider
struct  CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1  : public Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF
{
public:

public:
};


// UnityEngine.CharacterController
struct  CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E  : public Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF
{
public:

public:
};


// UnityEngine.CharacterJoint
struct  CharacterJoint_tDA58379B9EB6DAF19FE5FD664FBA59B3843CEFE2  : public Joint_t734F77C82E6372173403FCA61D9E7FC4618595AF
{
public:
	// UnityEngine.Quaternion UnityEngine.CharacterJoint::targetRotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___targetRotation_4;
	// UnityEngine.Vector3 UnityEngine.CharacterJoint::targetAngularVelocity
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___targetAngularVelocity_5;
	// UnityEngine.JointDrive UnityEngine.CharacterJoint::rotationDrive
	JointDrive_t2082D48A52244171DA92ED5FC110D2A58BCBB575  ___rotationDrive_6;

public:
	inline static int32_t get_offset_of_targetRotation_4() { return static_cast<int32_t>(offsetof(CharacterJoint_tDA58379B9EB6DAF19FE5FD664FBA59B3843CEFE2, ___targetRotation_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_targetRotation_4() const { return ___targetRotation_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_targetRotation_4() { return &___targetRotation_4; }
	inline void set_targetRotation_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___targetRotation_4 = value;
	}

	inline static int32_t get_offset_of_targetAngularVelocity_5() { return static_cast<int32_t>(offsetof(CharacterJoint_tDA58379B9EB6DAF19FE5FD664FBA59B3843CEFE2, ___targetAngularVelocity_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_targetAngularVelocity_5() const { return ___targetAngularVelocity_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_targetAngularVelocity_5() { return &___targetAngularVelocity_5; }
	inline void set_targetAngularVelocity_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___targetAngularVelocity_5 = value;
	}

	inline static int32_t get_offset_of_rotationDrive_6() { return static_cast<int32_t>(offsetof(CharacterJoint_tDA58379B9EB6DAF19FE5FD664FBA59B3843CEFE2, ___rotationDrive_6)); }
	inline JointDrive_t2082D48A52244171DA92ED5FC110D2A58BCBB575  get_rotationDrive_6() const { return ___rotationDrive_6; }
	inline JointDrive_t2082D48A52244171DA92ED5FC110D2A58BCBB575 * get_address_of_rotationDrive_6() { return &___rotationDrive_6; }
	inline void set_rotationDrive_6(JointDrive_t2082D48A52244171DA92ED5FC110D2A58BCBB575  value)
	{
		___rotationDrive_6 = value;
	}
};


// UnityEngine.ConfigurableJoint
struct  ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7  : public Joint_t734F77C82E6372173403FCA61D9E7FC4618595AF
{
public:

public:
};


// UnityEngine.FixedJoint
struct  FixedJoint_tE700873B552522792ED1B9A42DA62E511DC4A266  : public Joint_t734F77C82E6372173403FCA61D9E7FC4618595AF
{
public:

public:
};


// UnityEngine.HingeJoint
struct  HingeJoint_tDA10BC5B1D01F5226ED5B0F83D106E296925036D  : public Joint_t734F77C82E6372173403FCA61D9E7FC4618595AF
{
public:

public:
};


// UnityEngine.MeshCollider
struct  MeshCollider_t60EB55ADE92499FE8D1AA206D2BD96E65B2766DE  : public Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF
{
public:

public:
};


// UnityEngine.SphereCollider
struct  SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F  : public Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF
{
public:

public:
};


// UnityEngine.SpringJoint
struct  SpringJoint_t2D196194480E339F728B463619720385523B09AF  : public Joint_t734F77C82E6372173403FCA61D9E7FC4618595AF
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// UnityEngine.ContactPoint[]
struct ContactPointU5BU5D_t10BB5D5BFFFA3C919FD97DFDEDB49D954AFB8EAA  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) ContactPoint_tE0D3A30ED34A1FC8CA3F7391348429F3232CA515  m_Items[1];

public:
	inline ContactPoint_tE0D3A30ED34A1FC8CA3F7391348429F3232CA515  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ContactPoint_tE0D3A30ED34A1FC8CA3F7391348429F3232CA515 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ContactPoint_tE0D3A30ED34A1FC8CA3F7391348429F3232CA515  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline ContactPoint_tE0D3A30ED34A1FC8CA3F7391348429F3232CA515  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ContactPoint_tE0D3A30ED34A1FC8CA3F7391348429F3232CA515 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ContactPoint_tE0D3A30ED34A1FC8CA3F7391348429F3232CA515  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3  m_Items[1];

public:
	inline RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Collider[]
struct ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * m_Items[1];

public:
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};



// System.Void UnityEngine.BoxCollider::get_center_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BoxCollider_get_center_Injected_m04983F947C5E02756596EC89923061041DB7D5D8 (BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___ret0, const RuntimeMethod* method);
// System.Void UnityEngine.BoxCollider::set_center_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BoxCollider_set_center_Injected_m3D018F7FF1507924176F2B8672B1AB6FE945D5E4 (BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.BoxCollider::get_size_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BoxCollider_get_size_Injected_m0102C89526BAADB06950B4BCF339C1B155449AD1 (BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___ret0, const RuntimeMethod* method);
// System.Void UnityEngine.BoxCollider::set_size_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BoxCollider_set_size_Injected_mC0F1AC95BA07EF05E218E1A8EB5F49E211318943 (BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.CapsuleCollider::get_center_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CapsuleCollider_get_center_Injected_mCD228988D6221C1AAB16F9A46FA6D3EBF02D7BB2 (CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___ret0, const RuntimeMethod* method);
// System.Void UnityEngine.CapsuleCollider::set_center_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CapsuleCollider_set_center_Injected_mE6B65C8E5FC795B8C3E1666FB675897F3139AFB3 (CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Collider::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Collider__ctor_m4E6D2F06C3BDB9CB006BD94C294049209D7563ED (Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.CharacterController::SimpleMove_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CharacterController_SimpleMove_Injected_mA94FFCC3D0D3C4D6650237B77BA4F09B43C81AC0 (CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___speed0, const RuntimeMethod* method);
// UnityEngine.CollisionFlags UnityEngine.CharacterController::Move_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t CharacterController_Move_Injected_m729AD52E288CE0BF810D548AF6F9309EEB56A551 (CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___motion0, const RuntimeMethod* method);
// System.Void UnityEngine.CharacterController::get_velocity_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharacterController_get_velocity_Injected_m0E49CB063AAA4E43CBBF4F422E5E4CAABAFCA3A8 (CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___ret0, const RuntimeMethod* method);
// System.Void UnityEngine.CharacterJoint::get_swingAxis_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharacterJoint_get_swingAxis_Injected_mB19190386E2A95FB348A8FC533259C03A518EA3D (CharacterJoint_tDA58379B9EB6DAF19FE5FD664FBA59B3843CEFE2 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___ret0, const RuntimeMethod* method);
// System.Void UnityEngine.CharacterJoint::set_swingAxis_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharacterJoint_set_swingAxis_Injected_mB47A75071514311FB1BB8ED3C18818D94F8A9B6F (CharacterJoint_tDA58379B9EB6DAF19FE5FD664FBA59B3843CEFE2 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.CharacterJoint::get_twistLimitSpring_Injected(UnityEngine.SoftJointLimitSpring&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharacterJoint_get_twistLimitSpring_Injected_m5B4773EFAA80FE677F65BBE10FE24418A8EBDD5D (CharacterJoint_tDA58379B9EB6DAF19FE5FD664FBA59B3843CEFE2 * __this, SoftJointLimitSpring_t8D4D658B29266F9573CB134D0DBC15017488FDC2 * ___ret0, const RuntimeMethod* method);
// System.Void UnityEngine.CharacterJoint::get_swingLimitSpring_Injected(UnityEngine.SoftJointLimitSpring&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharacterJoint_get_swingLimitSpring_Injected_mBE8094BE61445AFF3E08423076C0A91FBF9DED61 (CharacterJoint_tDA58379B9EB6DAF19FE5FD664FBA59B3843CEFE2 * __this, SoftJointLimitSpring_t8D4D658B29266F9573CB134D0DBC15017488FDC2 * ___ret0, const RuntimeMethod* method);
// System.Void UnityEngine.CharacterJoint::get_lowTwistLimit_Injected(UnityEngine.SoftJointLimit&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharacterJoint_get_lowTwistLimit_Injected_m0C15C16C4B5D49128F3117A1DB8DAC4978B170BA (CharacterJoint_tDA58379B9EB6DAF19FE5FD664FBA59B3843CEFE2 * __this, SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 * ___ret0, const RuntimeMethod* method);
// System.Void UnityEngine.CharacterJoint::set_lowTwistLimit_Injected(UnityEngine.SoftJointLimit&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharacterJoint_set_lowTwistLimit_Injected_m99CF3E0755509747DEC5F57DE152DE525B76D3A5 (CharacterJoint_tDA58379B9EB6DAF19FE5FD664FBA59B3843CEFE2 * __this, SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.CharacterJoint::get_highTwistLimit_Injected(UnityEngine.SoftJointLimit&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharacterJoint_get_highTwistLimit_Injected_mAEAFE737AF7A19AA5E52960A3D4FB4045EEA32AB (CharacterJoint_tDA58379B9EB6DAF19FE5FD664FBA59B3843CEFE2 * __this, SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 * ___ret0, const RuntimeMethod* method);
// System.Void UnityEngine.CharacterJoint::set_highTwistLimit_Injected(UnityEngine.SoftJointLimit&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharacterJoint_set_highTwistLimit_Injected_m23184846DE1A86DC30D073767383B10F01AA5FD6 (CharacterJoint_tDA58379B9EB6DAF19FE5FD664FBA59B3843CEFE2 * __this, SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.CharacterJoint::get_swing1Limit_Injected(UnityEngine.SoftJointLimit&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharacterJoint_get_swing1Limit_Injected_m38AEB2810965D21D04B007636ADAA87BA71C24EF (CharacterJoint_tDA58379B9EB6DAF19FE5FD664FBA59B3843CEFE2 * __this, SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 * ___ret0, const RuntimeMethod* method);
// System.Void UnityEngine.CharacterJoint::set_swing1Limit_Injected(UnityEngine.SoftJointLimit&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharacterJoint_set_swing1Limit_Injected_m9E7B47F5A65D8C69FFC29820946652113AB2F8B0 (CharacterJoint_tDA58379B9EB6DAF19FE5FD664FBA59B3843CEFE2 * __this, SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.CharacterJoint::get_swing2Limit_Injected(UnityEngine.SoftJointLimit&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharacterJoint_get_swing2Limit_Injected_m7C50B2E73B1C889969551CF2CF18EA349BD4B4E6 (CharacterJoint_tDA58379B9EB6DAF19FE5FD664FBA59B3843CEFE2 * __this, SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 * ___ret0, const RuntimeMethod* method);
// System.Void UnityEngine.CharacterJoint::set_swing2Limit_Injected(UnityEngine.SoftJointLimit&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharacterJoint_set_swing2Limit_Injected_m9A8090BAAE2846A452B872448EE3179C660AF0F4 (CharacterJoint_tDA58379B9EB6DAF19FE5FD664FBA59B3843CEFE2 * __this, SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Collider::get_bounds_Injected(UnityEngine.Bounds&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Collider_get_bounds_Injected_m90D0FE433CCD44CC83E1089055A5A17286F9B1E6 (Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * __this, Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 * ___ret0, const RuntimeMethod* method);
// System.Void UnityEngine.Component::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Component__ctor_m5E2740C0ACA4B368BC460315FAA2EDBFEAC0B8EF (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___x0, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___y1, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// System.Void System.Array::Copy(System.Array,System.Array,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Array_Copy_m2D96731C600DE8A167348CA8BA796344E64F7434 (RuntimeArray * ___sourceArray0, RuntimeArray * ___destinationArray1, int32_t ___length2, const RuntimeMethod* method);
// System.Void UnityEngine.ConfigurableJoint::get_secondaryAxis_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConfigurableJoint_get_secondaryAxis_Injected_m6D51BC27202AACE1FA1BA20FF59298C66D794540 (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___ret0, const RuntimeMethod* method);
// System.Void UnityEngine.ConfigurableJoint::set_secondaryAxis_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConfigurableJoint_set_secondaryAxis_Injected_m8D630F896751C83CA9B07173DE8A120936733A91 (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.ConfigurableJoint::set_linearLimitSpring_Injected(UnityEngine.SoftJointLimitSpring&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConfigurableJoint_set_linearLimitSpring_Injected_mE90C49B513E7B3EA7ABFA95A3B8ED2A73730C5AB (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 * __this, SoftJointLimitSpring_t8D4D658B29266F9573CB134D0DBC15017488FDC2 * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.ConfigurableJoint::set_angularXLimitSpring_Injected(UnityEngine.SoftJointLimitSpring&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConfigurableJoint_set_angularXLimitSpring_Injected_m01FA0164ABAF03566B4158C6DB7A068DEDA038BE (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 * __this, SoftJointLimitSpring_t8D4D658B29266F9573CB134D0DBC15017488FDC2 * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.ConfigurableJoint::set_angularYZLimitSpring_Injected(UnityEngine.SoftJointLimitSpring&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConfigurableJoint_set_angularYZLimitSpring_Injected_mC5AF9A234E7E867197988D78BFA271C8F0AE0B2C (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 * __this, SoftJointLimitSpring_t8D4D658B29266F9573CB134D0DBC15017488FDC2 * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.ConfigurableJoint::set_linearLimit_Injected(UnityEngine.SoftJointLimit&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConfigurableJoint_set_linearLimit_Injected_m07AFBF1C2812A55724A0D8877E7BC47DE4AD6236 (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 * __this, SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.ConfigurableJoint::set_lowAngularXLimit_Injected(UnityEngine.SoftJointLimit&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConfigurableJoint_set_lowAngularXLimit_Injected_m8363CB59C106D8D98E1CE44974A2935AA2722A88 (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 * __this, SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.ConfigurableJoint::set_highAngularXLimit_Injected(UnityEngine.SoftJointLimit&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConfigurableJoint_set_highAngularXLimit_Injected_m46820CB6DD56738EE2E83A2603EFE8A993332F77 (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 * __this, SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.ConfigurableJoint::set_angularYLimit_Injected(UnityEngine.SoftJointLimit&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConfigurableJoint_set_angularYLimit_Injected_m7FFFFAA6B3057DD3B48DFB0DC557C81046D3292A (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 * __this, SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.ConfigurableJoint::set_angularZLimit_Injected(UnityEngine.SoftJointLimit&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConfigurableJoint_set_angularZLimit_Injected_m1F8CA881EFF4CB5C14F56296F33F55446CE95ECA (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 * __this, SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.ConfigurableJoint::set_targetRotation_Injected(UnityEngine.Quaternion&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConfigurableJoint_set_targetRotation_Injected_m9992253D130FFAB7F74C360A6874A30329799B38 (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 * __this, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.ConfigurableJoint::set_targetAngularVelocity_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConfigurableJoint_set_targetAngularVelocity_Injected_m400680616F59B0EA2BAB6B96DBFA28BF731DBFBF (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.ConfigurableJoint::set_slerpDrive_Injected(UnityEngine.JointDrive&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConfigurableJoint_set_slerpDrive_Injected_mDFDC04EFC76F28B3EEE803CCFCAD620DDC06D1B6 (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 * __this, JointDrive_t2082D48A52244171DA92ED5FC110D2A58BCBB575 * ___value0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.ContactPoint::get_point()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ContactPoint_get_point_mB222EB009D8B3DBE5F248088167F074551B424F7 (ContactPoint_tE0D3A30ED34A1FC8CA3F7391348429F3232CA515 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.ContactPoint::get_normal()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ContactPoint_get_normal_m0C47576D775F12C2BED5B25ACC720180008EDA3E (ContactPoint_tE0D3A30ED34A1FC8CA3F7391348429F3232CA515 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.HingeJoint::get_limits_Injected(UnityEngine.JointLimits&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HingeJoint_get_limits_Injected_mBC5408A9C2B0381572A34180B58D3C8B4405B161 (HingeJoint_tDA10BC5B1D01F5226ED5B0F83D106E296925036D * __this, JointLimits_t89D8D0E21FB1E840F8380AFB9FB55E966A3030F3 * ___ret0, const RuntimeMethod* method);
// System.Void UnityEngine.HingeJoint::get_spring_Injected(UnityEngine.JointSpring&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HingeJoint_get_spring_Injected_mC61A3CC33620B8F1DF1E5F20CF89ED3E5861EB9A (HingeJoint_tDA10BC5B1D01F5226ED5B0F83D106E296925036D * __this, JointSpring_t5BA0622E25A91A16BB70A8598BCCD11DD93FB3CD * ___ret0, const RuntimeMethod* method);
// System.Void UnityEngine.Joint::get_axis_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Joint_get_axis_Injected_m2F5466A10251C5F740DFBFA597FDBAEA02A2AE02 (Joint_t734F77C82E6372173403FCA61D9E7FC4618595AF * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___ret0, const RuntimeMethod* method);
// System.Void UnityEngine.Joint::set_axis_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Joint_set_axis_Injected_m7BFCEEDE9581D1B5F646E31F6AD3FA3990E3A9E4 (Joint_t734F77C82E6372173403FCA61D9E7FC4618595AF * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Joint::get_anchor_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Joint_get_anchor_Injected_mD0B6F04D2157DDD14E48278FAADA44AC5AF0433B (Joint_t734F77C82E6372173403FCA61D9E7FC4618595AF * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___ret0, const RuntimeMethod* method);
// System.Void UnityEngine.Joint::set_anchor_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Joint_set_anchor_Injected_mCDEEE334517B97D137C1AE4ADAAFAD9CD85399EB (Joint_t734F77C82E6372173403FCA61D9E7FC4618595AF * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Joint::get_connectedAnchor_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Joint_get_connectedAnchor_Injected_m8DBF0F41D88D496175BE8F21660DE20653E3CC9C (Joint_t734F77C82E6372173403FCA61D9E7FC4618595AF * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___ret0, const RuntimeMethod* method);
// System.Void UnityEngine.Joint::set_connectedAnchor_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Joint_set_connectedAnchor_Injected_m4CCEF375EFC641A7521AC470B6770282D17DEDED (Joint_t734F77C82E6372173403FCA61D9E7FC4618595AF * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.JointDrive::set_positionSpring(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void JointDrive_set_positionSpring_mB6FF4730ECEB6B03BACB391B34381EB34B70A9F8 (JointDrive_t2082D48A52244171DA92ED5FC110D2A58BCBB575 * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.JointDrive::set_positionDamper(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void JointDrive_set_positionDamper_mA07AE9A9384511BC160C19EF4CCB75260621C2CF (JointDrive_t2082D48A52244171DA92ED5FC110D2A58BCBB575 * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.JointDrive::set_maximumForce(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void JointDrive_set_maximumForce_m1D0AFF4DC2FB20093A1F29922550D88524CAE945 (JointDrive_t2082D48A52244171DA92ED5FC110D2A58BCBB575 * __this, float ___value0, const RuntimeMethod* method);
// System.Single UnityEngine.JointLimits::get_min()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float JointLimits_get_min_m7C30825582F94CECDD5DE097143F91B830103001 (JointLimits_t89D8D0E21FB1E840F8380AFB9FB55E966A3030F3 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.JointLimits::get_max()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float JointLimits_get_max_mFC3FC1E95BCBEA96DDC48D9B94BC8F3AE587CD2F (JointLimits_t89D8D0E21FB1E840F8380AFB9FB55E966A3030F3 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.JointLimits::get_bounciness()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float JointLimits_get_bounciness_m29ABD9EAE433CAA892D823A26EE8C9423E4CDBB1 (JointLimits_t89D8D0E21FB1E840F8380AFB9FB55E966A3030F3 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m091EBAEBC7919B0391ABDAFB7389ADC12206525B (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.PhysicMaterial::Internal_CreateDynamicsMaterial(UnityEngine.PhysicMaterial,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhysicMaterial_Internal_CreateDynamicsMaterial_m477978C8F91BAC9E41E0BE575C0C0730C80AFE1C (PhysicMaterial_tBEBB6F4620A5221A4CBAEDB2E5984CCA70AA07F8 * ___mat0, String_t* ___name1, const RuntimeMethod* method);
// System.Void UnityEngine.Physics::get_gravity_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Physics_get_gravity_Injected_m7B5CE9CF65B245C2C2EA078BE636E393225B0335 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___ret0, const RuntimeMethod* method);
// System.Void UnityEngine.Physics::get_defaultPhysicsScene_Injected(UnityEngine.PhysicsScene&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Physics_get_defaultPhysicsScene_Injected_mEAB07CA5307D0DDB331A31E6A6BA28CF83009262 (PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 * ___ret0, const RuntimeMethod* method);
// System.Void UnityEngine.Physics::IgnoreLayerCollision(System.Int32,System.Int32,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Physics_IgnoreLayerCollision_m0DFDA0BBDFA1F0737524272504E3D85989989C32 (int32_t ___layer10, int32_t ___layer21, bool ___ignore2, const RuntimeMethod* method);
// UnityEngine.PhysicsScene UnityEngine.Physics::get_defaultPhysicsScene()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  Physics_get_defaultPhysicsScene_mEA96CE906577479223C956BFF0B8D0B79BB47B30 (const RuntimeMethod* method);
// System.Boolean UnityEngine.PhysicsScene::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PhysicsScene_Raycast_mA90972A8E828722A26EBC754EABB3086BE6E2336 (PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___origin0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___direction1, float ___maxDistance2, int32_t ___layerMask3, int32_t ___queryTriggerInteraction4, const RuntimeMethod* method);
// System.Boolean UnityEngine.PhysicsScene::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PhysicsScene_Raycast_m90D1AE1F641D9344BEBEF32E7AC0BAC883024C0B (PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___origin0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___direction1, RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * ___hitInfo2, float ___maxDistance3, int32_t ___layerMask4, int32_t ___queryTriggerInteraction5, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Ray::get_origin()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Ray_get_origin_m3773CA7B1E2F26F6F1447652B485D86C0BEC5187 (Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Ray::get_direction()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Ray_get_direction_m9E6468CD87844B437FC4B93491E63D388322F76E (Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Physics_Raycast_m44270C2D7C8CD056B6D78DE60A4C91FCC0A225C1 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___origin0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___direction1, RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * ___hitInfo2, float ___maxDistance3, int32_t ___layerMask4, int32_t ___queryTriggerInteraction5, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_op_Subtraction_mF9846B723A5034F8B9F5F5DCB78E3D67649143D3 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___a0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___b1, const RuntimeMethod* method);
// System.Single UnityEngine.Vector3::get_magnitude()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Vector3_get_magnitude_m9A750659B60C5FE0C30438A7F9681775D5DB1274 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Physics::Linecast(UnityEngine.Vector3,UnityEngine.Vector3,System.Int32,UnityEngine.QueryTriggerInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Physics_Linecast_m2CAAB76F9786AB83FCF6B9C751B6A754AAEA48FB (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___start0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___end1, int32_t ___layerMask2, int32_t ___queryTriggerInteraction3, const RuntimeMethod* method);
// System.Boolean UnityEngine.Physics::Linecast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Int32,UnityEngine.QueryTriggerInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Physics_Linecast_mCAA3A0F713478346F006ADF57D65F64C89A544C6 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___start0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___end1, RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * ___hitInfo2, int32_t ___layerMask3, int32_t ___queryTriggerInteraction4, const RuntimeMethod* method);
// System.Boolean UnityEngine.PhysicsScene::SphereCast(UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PhysicsScene_SphereCast_m99D2D1C789BB817406E7C7FBAA75F2019A13D6C1 (PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___origin0, float ___radius1, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___direction2, RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * ___hitInfo3, float ___maxDistance4, int32_t ___layerMask5, int32_t ___queryTriggerInteraction6, const RuntimeMethod* method);
// System.Boolean UnityEngine.Physics::SphereCast(UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Physics_SphereCast_m2D57A1F4DA048B95DC33939367F2556CEA22B8BD (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___origin0, float ___radius1, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___direction2, RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * ___hitInfo3, float ___maxDistance4, int32_t ___layerMask5, int32_t ___queryTriggerInteraction6, const RuntimeMethod* method);
// System.Boolean UnityEngine.Physics::SphereCast(UnityEngine.Ray,System.Single,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Physics_SphereCast_m6B6F941FB5632998A000262CDC9337E2B46E3871 (Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  ___ray0, float ___radius1, RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * ___hitInfo2, float ___maxDistance3, int32_t ___layerMask4, int32_t ___queryTriggerInteraction5, const RuntimeMethod* method);
// UnityEngine.RaycastHit[] UnityEngine.Physics::Internal_RaycastAll_Injected(UnityEngine.PhysicsScene&,UnityEngine.Ray&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* Physics_Internal_RaycastAll_Injected_m1FD369E67F19BB09F85967481F69EB24B5247913 (PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 * ___physicsScene0, Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 * ___ray1, float ___maxDistance2, int32_t ___mask3, int32_t ___queryTriggerInteraction4, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Division(UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_op_Division_mDF34F1CC445981B4D1137765BC6277419E561624 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___a0, float ___d1, const RuntimeMethod* method);
// System.Void UnityEngine.Ray::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Ray__ctor_m695D219349B8AA4C82F96C55A27D384C07736F6B (Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___origin0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___direction1, const RuntimeMethod* method);
// UnityEngine.RaycastHit[] UnityEngine.Physics::Internal_RaycastAll(UnityEngine.PhysicsScene,UnityEngine.Ray,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* Physics_Internal_RaycastAll_m366801D3E9D3DE8E9A94C5243D1FB2132DFDD851 (PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  ___physicsScene0, Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  ___ray1, float ___maxDistance2, int32_t ___mask3, int32_t ___queryTriggerInteraction4, const RuntimeMethod* method);
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* Physics_RaycastAll_m6627435AE46B6720066FEEA5A80923420EE8FD2D (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___origin0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___direction1, float ___maxDistance2, int32_t ___layerMask3, int32_t ___queryTriggerInteraction4, const RuntimeMethod* method);
// System.Int32 UnityEngine.PhysicsScene::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit[],System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PhysicsScene_Raycast_mFB4A1BB42BB1D950E3854A08DFE3ECCDEA3BCCB1 (PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___origin0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___direction1, RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* ___raycastHits2, float ___maxDistance3, int32_t ___layerMask4, int32_t ___queryTriggerInteraction5, const RuntimeMethod* method);
// UnityEngine.Collider[] UnityEngine.Physics::OverlapSphere_Internal_Injected(UnityEngine.PhysicsScene&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* Physics_OverlapSphere_Internal_Injected_m315C4BB796FD73B41F5710D1DB52A6BCB57C344D (PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 * ___physicsScene0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___position1, float ___radius2, int32_t ___layerMask3, int32_t ___queryTriggerInteraction4, const RuntimeMethod* method);
// UnityEngine.Collider[] UnityEngine.Physics::OverlapSphere_Internal(UnityEngine.PhysicsScene,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* Physics_OverlapSphere_Internal_m10C15A0F86303EF92B205F16591E344E5728B6D0 (PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  ___physicsScene0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___position1, float ___radius2, int32_t ___layerMask3, int32_t ___queryTriggerInteraction4, const RuntimeMethod* method);
// UnityEngine.Collider[] UnityEngine.Physics::OverlapSphere(UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* Physics_OverlapSphere_m73878904F621A28F8DD8855CB113C690CF1E89C0 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___position0, float ___radius1, int32_t ___layerMask2, int32_t ___queryTriggerInteraction3, const RuntimeMethod* method);
// System.String UnityEngine.UnityString::Format(System.String,System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* UnityString_Format_m415056ECF8DA7B3EC6A8456E299D0C2002177387 (String_t* ___fmt0, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___args1, const RuntimeMethod* method);
// System.String UnityEngine.PhysicsScene::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* PhysicsScene_ToString_m192392A2C486E2F76AAC736C519CB117BD187BE9 (PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.PhysicsScene::GetHashCode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PhysicsScene_GetHashCode_m8B6554AA89D7DFD183573EA89E5ACF6B21E08654 (PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.PhysicsScene::Equals(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PhysicsScene_Equals_m538C6A86CCC4EF0546590BAC51E5D08ACC2CBEF9 (PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 * __this, RuntimeObject * ___other0, const RuntimeMethod* method);
// System.Boolean UnityEngine.PhysicsScene::Equals(UnityEngine.PhysicsScene)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PhysicsScene_Equals_m25CC71614BEABB76B6FB618516B8807DAF8CDDF8 (PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 * __this, PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  ___other0, const RuntimeMethod* method);
// System.Boolean UnityEngine.PhysicsScene::Internal_RaycastTest(UnityEngine.PhysicsScene,UnityEngine.Ray,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PhysicsScene_Internal_RaycastTest_m09DD752D166094AC2A6F19302F7DDB2EA88DE67F (PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  ___physicsScene0, Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  ___ray1, float ___maxDistance2, int32_t ___layerMask3, int32_t ___queryTriggerInteraction4, const RuntimeMethod* method);
// System.Boolean UnityEngine.PhysicsScene::Internal_RaycastTest_Injected(UnityEngine.PhysicsScene&,UnityEngine.Ray&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PhysicsScene_Internal_RaycastTest_Injected_m544D0227B2312D7431A9351ADA5082B390B4ADDF (PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 * ___physicsScene0, Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 * ___ray1, float ___maxDistance2, int32_t ___layerMask3, int32_t ___queryTriggerInteraction4, const RuntimeMethod* method);
// System.Boolean UnityEngine.PhysicsScene::Internal_Raycast(UnityEngine.PhysicsScene,UnityEngine.Ray,System.Single,UnityEngine.RaycastHit&,System.Int32,UnityEngine.QueryTriggerInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PhysicsScene_Internal_Raycast_m2769B78FDCAA21C6F609DD709BADFF6F4B07C529 (PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  ___physicsScene0, Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  ___ray1, float ___maxDistance2, RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * ___hit3, int32_t ___layerMask4, int32_t ___queryTriggerInteraction5, const RuntimeMethod* method);
// System.Boolean UnityEngine.PhysicsScene::Internal_Raycast_Injected(UnityEngine.PhysicsScene&,UnityEngine.Ray&,System.Single,UnityEngine.RaycastHit&,System.Int32,UnityEngine.QueryTriggerInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PhysicsScene_Internal_Raycast_Injected_m039B99FEFE39873FF8B0F60F44DE817BF43D48DA (PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 * ___physicsScene0, Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 * ___ray1, float ___maxDistance2, RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * ___hit3, int32_t ___layerMask4, int32_t ___queryTriggerInteraction5, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_normalized()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_get_normalized_mE20796F1D2D36244FACD4D14DADB245BE579849B (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.PhysicsScene::Internal_RaycastNonAlloc(UnityEngine.PhysicsScene,UnityEngine.Ray,UnityEngine.RaycastHit[],System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PhysicsScene_Internal_RaycastNonAlloc_m9E5DAC724FC74D6B618C82BC8F19980E9636D5B1 (PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  ___physicsScene0, Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  ___ray1, RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* ___raycastHits2, float ___maxDistance3, int32_t ___mask4, int32_t ___queryTriggerInteraction5, const RuntimeMethod* method);
// System.Int32 UnityEngine.PhysicsScene::Internal_RaycastNonAlloc_Injected(UnityEngine.PhysicsScene&,UnityEngine.Ray&,UnityEngine.RaycastHit[],System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PhysicsScene_Internal_RaycastNonAlloc_Injected_m4A5BC29FB0D0AAFC1DE827838CE144F882FE632E (PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 * ___physicsScene0, Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 * ___ray1, RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* ___raycastHits2, float ___maxDistance3, int32_t ___mask4, int32_t ___queryTriggerInteraction5, const RuntimeMethod* method);
// System.Boolean UnityEngine.PhysicsScene::Query_SphereCast_Injected(UnityEngine.PhysicsScene&,UnityEngine.Vector3&,System.Single,UnityEngine.Vector3&,System.Single,UnityEngine.RaycastHit&,System.Int32,UnityEngine.QueryTriggerInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PhysicsScene_Query_SphereCast_Injected_mA3953A644D1ED0B7D37A22E44635A413A4A1A12F (PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 * ___physicsScene0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___origin1, float ___radius2, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___direction3, float ___maxDistance4, RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * ___hitInfo5, int32_t ___layerMask6, int32_t ___queryTriggerInteraction7, const RuntimeMethod* method);
// System.Boolean UnityEngine.PhysicsScene::Query_SphereCast(UnityEngine.PhysicsScene,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,System.Single,UnityEngine.RaycastHit&,System.Int32,UnityEngine.QueryTriggerInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PhysicsScene_Query_SphereCast_m6DD7B632CF528C867859E329E2C4807C2CC891D8 (PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  ___physicsScene0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___origin1, float ___radius2, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___direction3, float ___maxDistance4, RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * ___hitInfo5, int32_t ___layerMask6, int32_t ___queryTriggerInteraction7, const RuntimeMethod* method);
// System.Boolean UnityEngine.PhysicsScene::Internal_SphereCast(UnityEngine.PhysicsScene,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PhysicsScene_Internal_SphereCast_mAD5109B24E5C668DC9A49E434B5139CA8A43DBD0 (PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  ___physicsScene0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___origin1, float ___radius2, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___direction3, RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * ___hitInfo4, float ___maxDistance5, int32_t ___layerMask6, int32_t ___queryTriggerInteraction7, const RuntimeMethod* method);
// UnityEngine.Object UnityEngine.Object::FindObjectFromInstanceID(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * Object_FindObjectFromInstanceID_m7594ED98F525AAE38FEC80052729ECAF3E821350 (int32_t ___instanceID0, const RuntimeMethod* method);
// UnityEngine.Collider UnityEngine.RaycastHit::get_collider()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * RaycastHit_get_collider_mE70B84C4312B567344F60992A6067855F2C3A7A9 (RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_point()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  RaycastHit_get_point_m0E564B2A72C7A744B889AE9D596F3EFA55059001 (RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.RaycastHit::set_point(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RaycastHit_set_point_mCB652835DA3A0AED8A8574B4A47FD9BF8F9A4191 (RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_normal()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  RaycastHit_get_normal_mF736A6D09D98D63AB7E5BF10F38AEBFC177A1D94 (RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.RaycastHit::set_normal(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RaycastHit_set_normal_mB21B55B92CE45F93EB140793D31DFE69693F7303 (RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value0, const RuntimeMethod* method);
// System.Single UnityEngine.RaycastHit::get_distance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float RaycastHit_get_distance_m1CBA60855C35F29BBC348D374BBC76386A243543 (RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * __this, const RuntimeMethod* method);
// UnityEngine.Rigidbody UnityEngine.Collider::get_attachedRigidbody()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * Collider_get_attachedRigidbody_m9E3C688EAE2F6A76C9AC14968D96769D9A71B1E8 (Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * __this, const RuntimeMethod* method);
// UnityEngine.Rigidbody UnityEngine.RaycastHit::get_rigidbody()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * RaycastHit_get_rigidbody_m8E28BDE09DC588AAF0C15182AFF3C00EE11EB0FC (RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody::get_velocity_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_get_velocity_Injected_m61C1628D08B48C8971E476FCBB9323CB5EB73DAC (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___ret0, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody::set_velocity_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_set_velocity_Injected_mA0CBA4077CADA05CF30404564E8D6260EF60E07D (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody::get_angularVelocity_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_get_angularVelocity_Injected_mF130145BC134034C2ECB1C4E9BF276F0A63EC0D1 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___ret0, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody::set_angularVelocity_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_set_angularVelocity_Injected_mC5F63763F3701505D5F8E41CCE7F6325F1024783 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody::get_centerOfMass_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_get_centerOfMass_Injected_m15590C0C367EB322CF67A40CFE2A28A814A8766F (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___ret0, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody::set_centerOfMass_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_set_centerOfMass_Injected_m999B2BDCDFCAC0D049AF7A7BE5BEB7A4B75D1D0A (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody::get_worldCenterOfMass_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_get_worldCenterOfMass_Injected_mCF61FE2285D07E76A226249DA7C877F6888C945F (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___ret0, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody::get_inertiaTensor_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_get_inertiaTensor_Injected_m87F89C6379EB9C47DDB03A4F5AE47CBFF9CFD2FF (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___ret0, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody::set_inertiaTensor_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_set_inertiaTensor_Injected_mB43DBD3F1C979D04B0955484E0B14A971C7AE838 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody::get_position_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_get_position_Injected_m8B581634C88AB745341D4ACF1CC12544769EC019 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___ret0, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody::set_position_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_set_position_Injected_m3210674B4144CD8E599226F6343BC541A9975581 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody::get_rotation_Injected(UnityEngine.Quaternion&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_get_rotation_Injected_m5FEC1EC2B3CFCE6585C63CD64ECAE39828B73C91 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * ___ret0, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody::set_rotation_Injected(UnityEngine.Quaternion&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_set_rotation_Injected_m52738493BB4ACEDC29288DA8905907C1918A99C3 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody::MovePosition_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_MovePosition_Injected_mFD8BDE08912A4C3FC240A8834C3394A88349C440 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___position0, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody::MoveRotation_Injected(UnityEngine.Quaternion&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_MoveRotation_Injected_m283E5FB82CB1721DC5220311967ED0E6ED831BF0 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * ___rot0, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody::GetPointVelocity_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_GetPointVelocity_Injected_mFFC7165D5058AA5B62F08545554F78EE25FACC2E (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___worldPoint0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___ret1, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody::AddForce_Injected(UnityEngine.Vector3&,UnityEngine.ForceMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_AddForce_Injected_mEB8DB621D03BE740C5CBD472E873FF8B6EF6C119 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___force0, int32_t ___mode1, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody::AddForce(UnityEngine.Vector3,UnityEngine.ForceMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_AddForce_mD64ACF772614FE36CFD8A477A07A407B35DF1A54 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___force0, int32_t ___mode1, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody::AddTorque_Injected(UnityEngine.Vector3&,UnityEngine.ForceMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_AddTorque_Injected_mBD4EFD1DF707E92D237322E8A48B5827D3E8AD6B (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___torque0, int32_t ___mode1, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody::AddTorque(UnityEngine.Vector3,UnityEngine.ForceMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_AddTorque_m18EA054D294E8A26F6485C1E933E986BD5BE5F60 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___torque0, int32_t ___mode1, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody::AddForceAtPosition_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.ForceMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_AddForceAtPosition_Injected_m5A653201E107E36E0E70F6A9933B8CE5F3F31E1F (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___force0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___position1, int32_t ___mode2, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody::AddForceAtPosition(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.ForceMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_AddForceAtPosition_mF3B282490DFBD57EEA2A67AA44F602368A20D769 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___force0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___position1, int32_t ___mode2, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody::AddExplosionForce_Injected(System.Single,UnityEngine.Vector3&,System.Single,System.Single,UnityEngine.ForceMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_AddExplosionForce_Injected_m52842CCC1B482C3713F35132507C8F9652BC5D07 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, float ___explosionForce0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___explosionPosition1, float ___explosionRadius2, float ___upwardsModifier3, int32_t ___mode4, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody::AddExplosionForce(System.Single,UnityEngine.Vector3,System.Single,System.Single,UnityEngine.ForceMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_AddExplosionForce_m8A2C25CE73AC51E1C255929733A4187089BA689A (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, float ___explosionForce0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___explosionPosition1, float ___explosionRadius2, float ___upwardsModifier3, int32_t ___mode4, const RuntimeMethod* method);
// System.Single UnityEngine.SoftJointLimit::get_limit()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float SoftJointLimit_get_limit_m40164161D58BA46F6F1EDA382FC552607BFC7D03 (SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.SoftJointLimit::set_limit(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SoftJointLimit_set_limit_m65386F3B7DE799354F233666054E36B4E244FE26 (SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 * __this, float ___value0, const RuntimeMethod* method);
// System.Single UnityEngine.SoftJointLimit::get_bounciness()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float SoftJointLimit_get_bounciness_m387D59B7712B1E70CFA337F9E311573C0D928846 (SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.SoftJointLimit::set_bounciness(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SoftJointLimit_set_bounciness_mC19E24774A8CF6C1229AE5D83AFE13F9F6D0938B (SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 * __this, float ___value0, const RuntimeMethod* method);
// System.Single UnityEngine.SoftJointLimitSpring::get_spring()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float SoftJointLimitSpring_get_spring_mC5D04E551048F33C874E4A7022E6FE582094AE21 (SoftJointLimitSpring_t8D4D658B29266F9573CB134D0DBC15017488FDC2 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.SoftJointLimitSpring::set_spring(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SoftJointLimitSpring_set_spring_mCC90918E4E61E1DBCB55653A3357B168B3281232 (SoftJointLimitSpring_t8D4D658B29266F9573CB134D0DBC15017488FDC2 * __this, float ___value0, const RuntimeMethod* method);
// System.Single UnityEngine.SoftJointLimitSpring::get_damper()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float SoftJointLimitSpring_get_damper_mD1AAF428D25AFDF21E1CD5B349ACBE64D7C3A006 (SoftJointLimitSpring_t8D4D658B29266F9573CB134D0DBC15017488FDC2 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.SoftJointLimitSpring::set_damper(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SoftJointLimitSpring_set_damper_m03BF5BE752A1620C6691D7DB663D05F0FFD55534 (SoftJointLimitSpring_t8D4D658B29266F9573CB134D0DBC15017488FDC2 * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.SphereCollider::get_center_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SphereCollider_get_center_Injected_mE8ECA23236D73D0685D76F15CB21BA1C09C9F6DB (SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___ret0, const RuntimeMethod* method);
// System.Void UnityEngine.SphereCollider::set_center_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SphereCollider_set_center_Injected_mB5E101BA19F903AF24B7FAE2D125BBCC781FF82C (SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___value0, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Vector3 UnityEngine.BoxCollider::get_center()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  BoxCollider_get_center_mA9164B9949F419A35CC949685F1DC14588BC6402 (BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA * __this, const RuntimeMethod* method)
{
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		BoxCollider_get_center_Injected_m04983F947C5E02756596EC89923061041DB7D5D8(__this, (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_0), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.BoxCollider::set_center(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BoxCollider_set_center_m8A871056CA383C9932A7694FE396A1EFA247FC69 (BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value0, const RuntimeMethod* method)
{
	{
		BoxCollider_set_center_Injected_m3D018F7FF1507924176F2B8672B1AB6FE945D5E4(__this, (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&___value0), /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.BoxCollider::get_size()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  BoxCollider_get_size_m1C7DA815D3BA9DDB3D92A58BEEFE2FCBA5206FE2 (BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA * __this, const RuntimeMethod* method)
{
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		BoxCollider_get_size_Injected_m0102C89526BAADB06950B4BCF339C1B155449AD1(__this, (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_0), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.BoxCollider::set_size(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BoxCollider_set_size_m65F9B4BD610D3094313EC8D1C5CE58D1D345A176 (BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value0, const RuntimeMethod* method)
{
	{
		BoxCollider_set_size_Injected_mC0F1AC95BA07EF05E218E1A8EB5F49E211318943(__this, (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.BoxCollider::get_center_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BoxCollider_get_center_Injected_m04983F947C5E02756596EC89923061041DB7D5D8 (BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___ret0, const RuntimeMethod* method)
{
	typedef void (*BoxCollider_get_center_Injected_m04983F947C5E02756596EC89923061041DB7D5D8_ftn) (BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA *, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *);
	static BoxCollider_get_center_Injected_m04983F947C5E02756596EC89923061041DB7D5D8_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (BoxCollider_get_center_Injected_m04983F947C5E02756596EC89923061041DB7D5D8_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.BoxCollider::get_center_Injected(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___ret0);
}
// System.Void UnityEngine.BoxCollider::set_center_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BoxCollider_set_center_Injected_m3D018F7FF1507924176F2B8672B1AB6FE945D5E4 (BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___value0, const RuntimeMethod* method)
{
	typedef void (*BoxCollider_set_center_Injected_m3D018F7FF1507924176F2B8672B1AB6FE945D5E4_ftn) (BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA *, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *);
	static BoxCollider_set_center_Injected_m3D018F7FF1507924176F2B8672B1AB6FE945D5E4_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (BoxCollider_set_center_Injected_m3D018F7FF1507924176F2B8672B1AB6FE945D5E4_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.BoxCollider::set_center_Injected(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.BoxCollider::get_size_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BoxCollider_get_size_Injected_m0102C89526BAADB06950B4BCF339C1B155449AD1 (BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___ret0, const RuntimeMethod* method)
{
	typedef void (*BoxCollider_get_size_Injected_m0102C89526BAADB06950B4BCF339C1B155449AD1_ftn) (BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA *, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *);
	static BoxCollider_get_size_Injected_m0102C89526BAADB06950B4BCF339C1B155449AD1_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (BoxCollider_get_size_Injected_m0102C89526BAADB06950B4BCF339C1B155449AD1_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.BoxCollider::get_size_Injected(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___ret0);
}
// System.Void UnityEngine.BoxCollider::set_size_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BoxCollider_set_size_Injected_mC0F1AC95BA07EF05E218E1A8EB5F49E211318943 (BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___value0, const RuntimeMethod* method)
{
	typedef void (*BoxCollider_set_size_Injected_mC0F1AC95BA07EF05E218E1A8EB5F49E211318943_ftn) (BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA *, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *);
	static BoxCollider_set_size_Injected_mC0F1AC95BA07EF05E218E1A8EB5F49E211318943_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (BoxCollider_set_size_Injected_mC0F1AC95BA07EF05E218E1A8EB5F49E211318943_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.BoxCollider::set_size_Injected(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Vector3 UnityEngine.CapsuleCollider::get_center()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  CapsuleCollider_get_center_m415B40B8ADB6B1C29F3EF4C23839D5514BBA18AE (CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1 * __this, const RuntimeMethod* method)
{
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		CapsuleCollider_get_center_Injected_mCD228988D6221C1AAB16F9A46FA6D3EBF02D7BB2(__this, (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_0), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.CapsuleCollider::set_center(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CapsuleCollider_set_center_mD2A261C23EB2DE8B0D9F946596BF64B72F7015C6 (CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value0, const RuntimeMethod* method)
{
	{
		CapsuleCollider_set_center_Injected_mE6B65C8E5FC795B8C3E1666FB675897F3139AFB3(__this, (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.CapsuleCollider::get_radius()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float CapsuleCollider_get_radius_m8E753A625226A5AF557AAFEBF5B6D0720C00802D (CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1 * __this, const RuntimeMethod* method)
{
	typedef float (*CapsuleCollider_get_radius_m8E753A625226A5AF557AAFEBF5B6D0720C00802D_ftn) (CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1 *);
	static CapsuleCollider_get_radius_m8E753A625226A5AF557AAFEBF5B6D0720C00802D_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CapsuleCollider_get_radius_m8E753A625226A5AF557AAFEBF5B6D0720C00802D_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CapsuleCollider::get_radius()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.CapsuleCollider::set_radius(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CapsuleCollider_set_radius_mFE5E4E8C0EC1ECAC76CFDDA3EF9713027872E50D (CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1 * __this, float ___value0, const RuntimeMethod* method)
{
	typedef void (*CapsuleCollider_set_radius_mFE5E4E8C0EC1ECAC76CFDDA3EF9713027872E50D_ftn) (CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1 *, float);
	static CapsuleCollider_set_radius_mFE5E4E8C0EC1ECAC76CFDDA3EF9713027872E50D_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CapsuleCollider_set_radius_mFE5E4E8C0EC1ECAC76CFDDA3EF9713027872E50D_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CapsuleCollider::set_radius(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.CapsuleCollider::get_height()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float CapsuleCollider_get_height_mA0F14683CEDB4F32B59D0262AB7507574228EF75 (CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1 * __this, const RuntimeMethod* method)
{
	typedef float (*CapsuleCollider_get_height_mA0F14683CEDB4F32B59D0262AB7507574228EF75_ftn) (CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1 *);
	static CapsuleCollider_get_height_mA0F14683CEDB4F32B59D0262AB7507574228EF75_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CapsuleCollider_get_height_mA0F14683CEDB4F32B59D0262AB7507574228EF75_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CapsuleCollider::get_height()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.CapsuleCollider::set_height(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CapsuleCollider_set_height_m77D7E2FFC2A2D587B30746E00690A375D3E3D0F6 (CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1 * __this, float ___value0, const RuntimeMethod* method)
{
	typedef void (*CapsuleCollider_set_height_m77D7E2FFC2A2D587B30746E00690A375D3E3D0F6_ftn) (CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1 *, float);
	static CapsuleCollider_set_height_m77D7E2FFC2A2D587B30746E00690A375D3E3D0F6_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CapsuleCollider_set_height_m77D7E2FFC2A2D587B30746E00690A375D3E3D0F6_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CapsuleCollider::set_height(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.CapsuleCollider::get_direction()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t CapsuleCollider_get_direction_m699EB45D6B2D713374B163F59AA4CB61B2BFA3DF (CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*CapsuleCollider_get_direction_m699EB45D6B2D713374B163F59AA4CB61B2BFA3DF_ftn) (CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1 *);
	static CapsuleCollider_get_direction_m699EB45D6B2D713374B163F59AA4CB61B2BFA3DF_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CapsuleCollider_get_direction_m699EB45D6B2D713374B163F59AA4CB61B2BFA3DF_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CapsuleCollider::get_direction()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.CapsuleCollider::set_direction(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CapsuleCollider_set_direction_mBF666730647E4C553A011BC503EEDD4F9117626C (CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*CapsuleCollider_set_direction_mBF666730647E4C553A011BC503EEDD4F9117626C_ftn) (CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1 *, int32_t);
	static CapsuleCollider_set_direction_mBF666730647E4C553A011BC503EEDD4F9117626C_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CapsuleCollider_set_direction_mBF666730647E4C553A011BC503EEDD4F9117626C_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CapsuleCollider::set_direction(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.CapsuleCollider::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CapsuleCollider__ctor_m4334AB0C7D4CA7745D4492117175FB180DF164A8 (CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1 * __this, const RuntimeMethod* method)
{
	{
		Collider__ctor_m4E6D2F06C3BDB9CB006BD94C294049209D7563ED(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.CapsuleCollider::get_center_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CapsuleCollider_get_center_Injected_mCD228988D6221C1AAB16F9A46FA6D3EBF02D7BB2 (CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___ret0, const RuntimeMethod* method)
{
	typedef void (*CapsuleCollider_get_center_Injected_mCD228988D6221C1AAB16F9A46FA6D3EBF02D7BB2_ftn) (CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1 *, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *);
	static CapsuleCollider_get_center_Injected_mCD228988D6221C1AAB16F9A46FA6D3EBF02D7BB2_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CapsuleCollider_get_center_Injected_mCD228988D6221C1AAB16F9A46FA6D3EBF02D7BB2_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CapsuleCollider::get_center_Injected(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___ret0);
}
// System.Void UnityEngine.CapsuleCollider::set_center_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CapsuleCollider_set_center_Injected_mE6B65C8E5FC795B8C3E1666FB675897F3139AFB3 (CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___value0, const RuntimeMethod* method)
{
	typedef void (*CapsuleCollider_set_center_Injected_mE6B65C8E5FC795B8C3E1666FB675897F3139AFB3_ftn) (CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1 *, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *);
	static CapsuleCollider_set_center_Injected_mE6B65C8E5FC795B8C3E1666FB675897F3139AFB3_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CapsuleCollider_set_center_Injected_mE6B65C8E5FC795B8C3E1666FB675897F3139AFB3_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CapsuleCollider::set_center_Injected(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean UnityEngine.CharacterController::SimpleMove(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CharacterController_SimpleMove_mE56BC226845D3BAACAA61BCA150D6723AC897489 (CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___speed0, const RuntimeMethod* method)
{
	{
		bool L_0 = CharacterController_SimpleMove_Injected_mA94FFCC3D0D3C4D6650237B77BA4F09B43C81AC0(__this, (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&___speed0), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.CollisionFlags UnityEngine.CharacterController::Move(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t CharacterController_Move_m31D77B4E934015FE3D6CE04BF8017A1DD0487434 (CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___motion0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = CharacterController_Move_Injected_m729AD52E288CE0BF810D548AF6F9309EEB56A551(__this, (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&___motion0), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.CharacterController::get_velocity()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  CharacterController_get_velocity_m735C4159CF165335A5C51E2B059952601F8CA0CD (CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * __this, const RuntimeMethod* method)
{
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		CharacterController_get_velocity_Injected_m0E49CB063AAA4E43CBBF4F422E5E4CAABAFCA3A8(__this, (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_0), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = V_0;
		return L_0;
	}
}
// System.Boolean UnityEngine.CharacterController::get_isGrounded()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CharacterController_get_isGrounded_m5BB5A50DD5942ED9AEF9F3A97CF52913591E077B (CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * __this, const RuntimeMethod* method)
{
	typedef bool (*CharacterController_get_isGrounded_m5BB5A50DD5942ED9AEF9F3A97CF52913591E077B_ftn) (CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E *);
	static CharacterController_get_isGrounded_m5BB5A50DD5942ED9AEF9F3A97CF52913591E077B_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CharacterController_get_isGrounded_m5BB5A50DD5942ED9AEF9F3A97CF52913591E077B_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CharacterController::get_isGrounded()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Boolean UnityEngine.CharacterController::SimpleMove_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CharacterController_SimpleMove_Injected_mA94FFCC3D0D3C4D6650237B77BA4F09B43C81AC0 (CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___speed0, const RuntimeMethod* method)
{
	typedef bool (*CharacterController_SimpleMove_Injected_mA94FFCC3D0D3C4D6650237B77BA4F09B43C81AC0_ftn) (CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E *, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *);
	static CharacterController_SimpleMove_Injected_mA94FFCC3D0D3C4D6650237B77BA4F09B43C81AC0_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CharacterController_SimpleMove_Injected_mA94FFCC3D0D3C4D6650237B77BA4F09B43C81AC0_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CharacterController::SimpleMove_Injected(UnityEngine.Vector3&)");
	bool retVal = _il2cpp_icall_func(__this, ___speed0);
	return retVal;
}
// UnityEngine.CollisionFlags UnityEngine.CharacterController::Move_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t CharacterController_Move_Injected_m729AD52E288CE0BF810D548AF6F9309EEB56A551 (CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___motion0, const RuntimeMethod* method)
{
	typedef int32_t (*CharacterController_Move_Injected_m729AD52E288CE0BF810D548AF6F9309EEB56A551_ftn) (CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E *, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *);
	static CharacterController_Move_Injected_m729AD52E288CE0BF810D548AF6F9309EEB56A551_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CharacterController_Move_Injected_m729AD52E288CE0BF810D548AF6F9309EEB56A551_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CharacterController::Move_Injected(UnityEngine.Vector3&)");
	int32_t retVal = _il2cpp_icall_func(__this, ___motion0);
	return retVal;
}
// System.Void UnityEngine.CharacterController::get_velocity_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharacterController_get_velocity_Injected_m0E49CB063AAA4E43CBBF4F422E5E4CAABAFCA3A8 (CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___ret0, const RuntimeMethod* method)
{
	typedef void (*CharacterController_get_velocity_Injected_m0E49CB063AAA4E43CBBF4F422E5E4CAABAFCA3A8_ftn) (CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E *, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *);
	static CharacterController_get_velocity_Injected_m0E49CB063AAA4E43CBBF4F422E5E4CAABAFCA3A8_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CharacterController_get_velocity_Injected_m0E49CB063AAA4E43CBBF4F422E5E4CAABAFCA3A8_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CharacterController::get_velocity_Injected(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___ret0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Vector3 UnityEngine.CharacterJoint::get_swingAxis()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  CharacterJoint_get_swingAxis_m3D84F5C03ADEC6D798CD2F7D610CD4BED92B316F (CharacterJoint_tDA58379B9EB6DAF19FE5FD664FBA59B3843CEFE2 * __this, const RuntimeMethod* method)
{
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		CharacterJoint_get_swingAxis_Injected_mB19190386E2A95FB348A8FC533259C03A518EA3D(__this, (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_0), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.CharacterJoint::set_swingAxis(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharacterJoint_set_swingAxis_mE1A37DAAA9E81D2AC53717C3D14188EB60E2C401 (CharacterJoint_tDA58379B9EB6DAF19FE5FD664FBA59B3843CEFE2 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value0, const RuntimeMethod* method)
{
	{
		CharacterJoint_set_swingAxis_Injected_mB47A75071514311FB1BB8ED3C18818D94F8A9B6F(__this, (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&___value0), /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.SoftJointLimitSpring UnityEngine.CharacterJoint::get_twistLimitSpring()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR SoftJointLimitSpring_t8D4D658B29266F9573CB134D0DBC15017488FDC2  CharacterJoint_get_twistLimitSpring_m8D739C967A41819B9BED9E7175113ADDD333FB53 (CharacterJoint_tDA58379B9EB6DAF19FE5FD664FBA59B3843CEFE2 * __this, const RuntimeMethod* method)
{
	SoftJointLimitSpring_t8D4D658B29266F9573CB134D0DBC15017488FDC2  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		CharacterJoint_get_twistLimitSpring_Injected_m5B4773EFAA80FE677F65BBE10FE24418A8EBDD5D(__this, (SoftJointLimitSpring_t8D4D658B29266F9573CB134D0DBC15017488FDC2 *)(&V_0), /*hidden argument*/NULL);
		SoftJointLimitSpring_t8D4D658B29266F9573CB134D0DBC15017488FDC2  L_0 = V_0;
		return L_0;
	}
}
// UnityEngine.SoftJointLimitSpring UnityEngine.CharacterJoint::get_swingLimitSpring()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR SoftJointLimitSpring_t8D4D658B29266F9573CB134D0DBC15017488FDC2  CharacterJoint_get_swingLimitSpring_mDC584B04AFAE8E93EB9978AA4C2ED2F9DEC0F315 (CharacterJoint_tDA58379B9EB6DAF19FE5FD664FBA59B3843CEFE2 * __this, const RuntimeMethod* method)
{
	SoftJointLimitSpring_t8D4D658B29266F9573CB134D0DBC15017488FDC2  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		CharacterJoint_get_swingLimitSpring_Injected_mBE8094BE61445AFF3E08423076C0A91FBF9DED61(__this, (SoftJointLimitSpring_t8D4D658B29266F9573CB134D0DBC15017488FDC2 *)(&V_0), /*hidden argument*/NULL);
		SoftJointLimitSpring_t8D4D658B29266F9573CB134D0DBC15017488FDC2  L_0 = V_0;
		return L_0;
	}
}
// UnityEngine.SoftJointLimit UnityEngine.CharacterJoint::get_lowTwistLimit()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9  CharacterJoint_get_lowTwistLimit_mF14777D3BEE16FD317EA79D674B69686EB600579 (CharacterJoint_tDA58379B9EB6DAF19FE5FD664FBA59B3843CEFE2 * __this, const RuntimeMethod* method)
{
	SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		CharacterJoint_get_lowTwistLimit_Injected_m0C15C16C4B5D49128F3117A1DB8DAC4978B170BA(__this, (SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 *)(&V_0), /*hidden argument*/NULL);
		SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.CharacterJoint::set_lowTwistLimit(UnityEngine.SoftJointLimit)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharacterJoint_set_lowTwistLimit_m3354D75D6F1A2703CDB94A0169D797FD374DA5CD (CharacterJoint_tDA58379B9EB6DAF19FE5FD664FBA59B3843CEFE2 * __this, SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9  ___value0, const RuntimeMethod* method)
{
	{
		CharacterJoint_set_lowTwistLimit_Injected_m99CF3E0755509747DEC5F57DE152DE525B76D3A5(__this, (SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 *)(&___value0), /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.SoftJointLimit UnityEngine.CharacterJoint::get_highTwistLimit()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9  CharacterJoint_get_highTwistLimit_mEAAFB8F58969DA1AE9C5FDB44012A990BB213A53 (CharacterJoint_tDA58379B9EB6DAF19FE5FD664FBA59B3843CEFE2 * __this, const RuntimeMethod* method)
{
	SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		CharacterJoint_get_highTwistLimit_Injected_mAEAFE737AF7A19AA5E52960A3D4FB4045EEA32AB(__this, (SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 *)(&V_0), /*hidden argument*/NULL);
		SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.CharacterJoint::set_highTwistLimit(UnityEngine.SoftJointLimit)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharacterJoint_set_highTwistLimit_mCB094A57AFBD1833966D8CCB3A4555E091A39D02 (CharacterJoint_tDA58379B9EB6DAF19FE5FD664FBA59B3843CEFE2 * __this, SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9  ___value0, const RuntimeMethod* method)
{
	{
		CharacterJoint_set_highTwistLimit_Injected_m23184846DE1A86DC30D073767383B10F01AA5FD6(__this, (SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 *)(&___value0), /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.SoftJointLimit UnityEngine.CharacterJoint::get_swing1Limit()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9  CharacterJoint_get_swing1Limit_m0C3A7166C4AEE153CB663CF62F90E2E545B733AE (CharacterJoint_tDA58379B9EB6DAF19FE5FD664FBA59B3843CEFE2 * __this, const RuntimeMethod* method)
{
	SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		CharacterJoint_get_swing1Limit_Injected_m38AEB2810965D21D04B007636ADAA87BA71C24EF(__this, (SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 *)(&V_0), /*hidden argument*/NULL);
		SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.CharacterJoint::set_swing1Limit(UnityEngine.SoftJointLimit)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharacterJoint_set_swing1Limit_m238C86F635049095559A92BD4F3E32A152D14B71 (CharacterJoint_tDA58379B9EB6DAF19FE5FD664FBA59B3843CEFE2 * __this, SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9  ___value0, const RuntimeMethod* method)
{
	{
		CharacterJoint_set_swing1Limit_Injected_m9E7B47F5A65D8C69FFC29820946652113AB2F8B0(__this, (SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 *)(&___value0), /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.SoftJointLimit UnityEngine.CharacterJoint::get_swing2Limit()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9  CharacterJoint_get_swing2Limit_mE128EB6EA89A3C0791EF032665FF6213CE574397 (CharacterJoint_tDA58379B9EB6DAF19FE5FD664FBA59B3843CEFE2 * __this, const RuntimeMethod* method)
{
	SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		CharacterJoint_get_swing2Limit_Injected_m7C50B2E73B1C889969551CF2CF18EA349BD4B4E6(__this, (SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 *)(&V_0), /*hidden argument*/NULL);
		SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.CharacterJoint::set_swing2Limit(UnityEngine.SoftJointLimit)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharacterJoint_set_swing2Limit_m671879988AE029D2171C6F631B9D35E69C47DF43 (CharacterJoint_tDA58379B9EB6DAF19FE5FD664FBA59B3843CEFE2 * __this, SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9  ___value0, const RuntimeMethod* method)
{
	{
		CharacterJoint_set_swing2Limit_Injected_m9A8090BAAE2846A452B872448EE3179C660AF0F4(__this, (SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 *)(&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.CharacterJoint::get_enableProjection()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CharacterJoint_get_enableProjection_mCFE8DE15EBE057ED6FB50DCA4CBE7D15FB6C713C (CharacterJoint_tDA58379B9EB6DAF19FE5FD664FBA59B3843CEFE2 * __this, const RuntimeMethod* method)
{
	typedef bool (*CharacterJoint_get_enableProjection_mCFE8DE15EBE057ED6FB50DCA4CBE7D15FB6C713C_ftn) (CharacterJoint_tDA58379B9EB6DAF19FE5FD664FBA59B3843CEFE2 *);
	static CharacterJoint_get_enableProjection_mCFE8DE15EBE057ED6FB50DCA4CBE7D15FB6C713C_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CharacterJoint_get_enableProjection_mCFE8DE15EBE057ED6FB50DCA4CBE7D15FB6C713C_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CharacterJoint::get_enableProjection()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Single UnityEngine.CharacterJoint::get_projectionDistance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float CharacterJoint_get_projectionDistance_m7FF0A9BA90DB6968D003A2A43E72E4A9C2319193 (CharacterJoint_tDA58379B9EB6DAF19FE5FD664FBA59B3843CEFE2 * __this, const RuntimeMethod* method)
{
	typedef float (*CharacterJoint_get_projectionDistance_m7FF0A9BA90DB6968D003A2A43E72E4A9C2319193_ftn) (CharacterJoint_tDA58379B9EB6DAF19FE5FD664FBA59B3843CEFE2 *);
	static CharacterJoint_get_projectionDistance_m7FF0A9BA90DB6968D003A2A43E72E4A9C2319193_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CharacterJoint_get_projectionDistance_m7FF0A9BA90DB6968D003A2A43E72E4A9C2319193_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CharacterJoint::get_projectionDistance()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Single UnityEngine.CharacterJoint::get_projectionAngle()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float CharacterJoint_get_projectionAngle_mB959ED98AFFF713E0298C2C543A72EEEE97F25C0 (CharacterJoint_tDA58379B9EB6DAF19FE5FD664FBA59B3843CEFE2 * __this, const RuntimeMethod* method)
{
	typedef float (*CharacterJoint_get_projectionAngle_mB959ED98AFFF713E0298C2C543A72EEEE97F25C0_ftn) (CharacterJoint_tDA58379B9EB6DAF19FE5FD664FBA59B3843CEFE2 *);
	static CharacterJoint_get_projectionAngle_mB959ED98AFFF713E0298C2C543A72EEEE97F25C0_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CharacterJoint_get_projectionAngle_mB959ED98AFFF713E0298C2C543A72EEEE97F25C0_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CharacterJoint::get_projectionAngle()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.CharacterJoint::get_swingAxis_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharacterJoint_get_swingAxis_Injected_mB19190386E2A95FB348A8FC533259C03A518EA3D (CharacterJoint_tDA58379B9EB6DAF19FE5FD664FBA59B3843CEFE2 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___ret0, const RuntimeMethod* method)
{
	typedef void (*CharacterJoint_get_swingAxis_Injected_mB19190386E2A95FB348A8FC533259C03A518EA3D_ftn) (CharacterJoint_tDA58379B9EB6DAF19FE5FD664FBA59B3843CEFE2 *, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *);
	static CharacterJoint_get_swingAxis_Injected_mB19190386E2A95FB348A8FC533259C03A518EA3D_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CharacterJoint_get_swingAxis_Injected_mB19190386E2A95FB348A8FC533259C03A518EA3D_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CharacterJoint::get_swingAxis_Injected(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___ret0);
}
// System.Void UnityEngine.CharacterJoint::set_swingAxis_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharacterJoint_set_swingAxis_Injected_mB47A75071514311FB1BB8ED3C18818D94F8A9B6F (CharacterJoint_tDA58379B9EB6DAF19FE5FD664FBA59B3843CEFE2 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___value0, const RuntimeMethod* method)
{
	typedef void (*CharacterJoint_set_swingAxis_Injected_mB47A75071514311FB1BB8ED3C18818D94F8A9B6F_ftn) (CharacterJoint_tDA58379B9EB6DAF19FE5FD664FBA59B3843CEFE2 *, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *);
	static CharacterJoint_set_swingAxis_Injected_mB47A75071514311FB1BB8ED3C18818D94F8A9B6F_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CharacterJoint_set_swingAxis_Injected_mB47A75071514311FB1BB8ED3C18818D94F8A9B6F_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CharacterJoint::set_swingAxis_Injected(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.CharacterJoint::get_twistLimitSpring_Injected(UnityEngine.SoftJointLimitSpring&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharacterJoint_get_twistLimitSpring_Injected_m5B4773EFAA80FE677F65BBE10FE24418A8EBDD5D (CharacterJoint_tDA58379B9EB6DAF19FE5FD664FBA59B3843CEFE2 * __this, SoftJointLimitSpring_t8D4D658B29266F9573CB134D0DBC15017488FDC2 * ___ret0, const RuntimeMethod* method)
{
	typedef void (*CharacterJoint_get_twistLimitSpring_Injected_m5B4773EFAA80FE677F65BBE10FE24418A8EBDD5D_ftn) (CharacterJoint_tDA58379B9EB6DAF19FE5FD664FBA59B3843CEFE2 *, SoftJointLimitSpring_t8D4D658B29266F9573CB134D0DBC15017488FDC2 *);
	static CharacterJoint_get_twistLimitSpring_Injected_m5B4773EFAA80FE677F65BBE10FE24418A8EBDD5D_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CharacterJoint_get_twistLimitSpring_Injected_m5B4773EFAA80FE677F65BBE10FE24418A8EBDD5D_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CharacterJoint::get_twistLimitSpring_Injected(UnityEngine.SoftJointLimitSpring&)");
	_il2cpp_icall_func(__this, ___ret0);
}
// System.Void UnityEngine.CharacterJoint::get_swingLimitSpring_Injected(UnityEngine.SoftJointLimitSpring&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharacterJoint_get_swingLimitSpring_Injected_mBE8094BE61445AFF3E08423076C0A91FBF9DED61 (CharacterJoint_tDA58379B9EB6DAF19FE5FD664FBA59B3843CEFE2 * __this, SoftJointLimitSpring_t8D4D658B29266F9573CB134D0DBC15017488FDC2 * ___ret0, const RuntimeMethod* method)
{
	typedef void (*CharacterJoint_get_swingLimitSpring_Injected_mBE8094BE61445AFF3E08423076C0A91FBF9DED61_ftn) (CharacterJoint_tDA58379B9EB6DAF19FE5FD664FBA59B3843CEFE2 *, SoftJointLimitSpring_t8D4D658B29266F9573CB134D0DBC15017488FDC2 *);
	static CharacterJoint_get_swingLimitSpring_Injected_mBE8094BE61445AFF3E08423076C0A91FBF9DED61_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CharacterJoint_get_swingLimitSpring_Injected_mBE8094BE61445AFF3E08423076C0A91FBF9DED61_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CharacterJoint::get_swingLimitSpring_Injected(UnityEngine.SoftJointLimitSpring&)");
	_il2cpp_icall_func(__this, ___ret0);
}
// System.Void UnityEngine.CharacterJoint::get_lowTwistLimit_Injected(UnityEngine.SoftJointLimit&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharacterJoint_get_lowTwistLimit_Injected_m0C15C16C4B5D49128F3117A1DB8DAC4978B170BA (CharacterJoint_tDA58379B9EB6DAF19FE5FD664FBA59B3843CEFE2 * __this, SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 * ___ret0, const RuntimeMethod* method)
{
	typedef void (*CharacterJoint_get_lowTwistLimit_Injected_m0C15C16C4B5D49128F3117A1DB8DAC4978B170BA_ftn) (CharacterJoint_tDA58379B9EB6DAF19FE5FD664FBA59B3843CEFE2 *, SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 *);
	static CharacterJoint_get_lowTwistLimit_Injected_m0C15C16C4B5D49128F3117A1DB8DAC4978B170BA_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CharacterJoint_get_lowTwistLimit_Injected_m0C15C16C4B5D49128F3117A1DB8DAC4978B170BA_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CharacterJoint::get_lowTwistLimit_Injected(UnityEngine.SoftJointLimit&)");
	_il2cpp_icall_func(__this, ___ret0);
}
// System.Void UnityEngine.CharacterJoint::set_lowTwistLimit_Injected(UnityEngine.SoftJointLimit&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharacterJoint_set_lowTwistLimit_Injected_m99CF3E0755509747DEC5F57DE152DE525B76D3A5 (CharacterJoint_tDA58379B9EB6DAF19FE5FD664FBA59B3843CEFE2 * __this, SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 * ___value0, const RuntimeMethod* method)
{
	typedef void (*CharacterJoint_set_lowTwistLimit_Injected_m99CF3E0755509747DEC5F57DE152DE525B76D3A5_ftn) (CharacterJoint_tDA58379B9EB6DAF19FE5FD664FBA59B3843CEFE2 *, SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 *);
	static CharacterJoint_set_lowTwistLimit_Injected_m99CF3E0755509747DEC5F57DE152DE525B76D3A5_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CharacterJoint_set_lowTwistLimit_Injected_m99CF3E0755509747DEC5F57DE152DE525B76D3A5_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CharacterJoint::set_lowTwistLimit_Injected(UnityEngine.SoftJointLimit&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.CharacterJoint::get_highTwistLimit_Injected(UnityEngine.SoftJointLimit&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharacterJoint_get_highTwistLimit_Injected_mAEAFE737AF7A19AA5E52960A3D4FB4045EEA32AB (CharacterJoint_tDA58379B9EB6DAF19FE5FD664FBA59B3843CEFE2 * __this, SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 * ___ret0, const RuntimeMethod* method)
{
	typedef void (*CharacterJoint_get_highTwistLimit_Injected_mAEAFE737AF7A19AA5E52960A3D4FB4045EEA32AB_ftn) (CharacterJoint_tDA58379B9EB6DAF19FE5FD664FBA59B3843CEFE2 *, SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 *);
	static CharacterJoint_get_highTwistLimit_Injected_mAEAFE737AF7A19AA5E52960A3D4FB4045EEA32AB_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CharacterJoint_get_highTwistLimit_Injected_mAEAFE737AF7A19AA5E52960A3D4FB4045EEA32AB_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CharacterJoint::get_highTwistLimit_Injected(UnityEngine.SoftJointLimit&)");
	_il2cpp_icall_func(__this, ___ret0);
}
// System.Void UnityEngine.CharacterJoint::set_highTwistLimit_Injected(UnityEngine.SoftJointLimit&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharacterJoint_set_highTwistLimit_Injected_m23184846DE1A86DC30D073767383B10F01AA5FD6 (CharacterJoint_tDA58379B9EB6DAF19FE5FD664FBA59B3843CEFE2 * __this, SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 * ___value0, const RuntimeMethod* method)
{
	typedef void (*CharacterJoint_set_highTwistLimit_Injected_m23184846DE1A86DC30D073767383B10F01AA5FD6_ftn) (CharacterJoint_tDA58379B9EB6DAF19FE5FD664FBA59B3843CEFE2 *, SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 *);
	static CharacterJoint_set_highTwistLimit_Injected_m23184846DE1A86DC30D073767383B10F01AA5FD6_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CharacterJoint_set_highTwistLimit_Injected_m23184846DE1A86DC30D073767383B10F01AA5FD6_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CharacterJoint::set_highTwistLimit_Injected(UnityEngine.SoftJointLimit&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.CharacterJoint::get_swing1Limit_Injected(UnityEngine.SoftJointLimit&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharacterJoint_get_swing1Limit_Injected_m38AEB2810965D21D04B007636ADAA87BA71C24EF (CharacterJoint_tDA58379B9EB6DAF19FE5FD664FBA59B3843CEFE2 * __this, SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 * ___ret0, const RuntimeMethod* method)
{
	typedef void (*CharacterJoint_get_swing1Limit_Injected_m38AEB2810965D21D04B007636ADAA87BA71C24EF_ftn) (CharacterJoint_tDA58379B9EB6DAF19FE5FD664FBA59B3843CEFE2 *, SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 *);
	static CharacterJoint_get_swing1Limit_Injected_m38AEB2810965D21D04B007636ADAA87BA71C24EF_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CharacterJoint_get_swing1Limit_Injected_m38AEB2810965D21D04B007636ADAA87BA71C24EF_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CharacterJoint::get_swing1Limit_Injected(UnityEngine.SoftJointLimit&)");
	_il2cpp_icall_func(__this, ___ret0);
}
// System.Void UnityEngine.CharacterJoint::set_swing1Limit_Injected(UnityEngine.SoftJointLimit&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharacterJoint_set_swing1Limit_Injected_m9E7B47F5A65D8C69FFC29820946652113AB2F8B0 (CharacterJoint_tDA58379B9EB6DAF19FE5FD664FBA59B3843CEFE2 * __this, SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 * ___value0, const RuntimeMethod* method)
{
	typedef void (*CharacterJoint_set_swing1Limit_Injected_m9E7B47F5A65D8C69FFC29820946652113AB2F8B0_ftn) (CharacterJoint_tDA58379B9EB6DAF19FE5FD664FBA59B3843CEFE2 *, SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 *);
	static CharacterJoint_set_swing1Limit_Injected_m9E7B47F5A65D8C69FFC29820946652113AB2F8B0_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CharacterJoint_set_swing1Limit_Injected_m9E7B47F5A65D8C69FFC29820946652113AB2F8B0_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CharacterJoint::set_swing1Limit_Injected(UnityEngine.SoftJointLimit&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.CharacterJoint::get_swing2Limit_Injected(UnityEngine.SoftJointLimit&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharacterJoint_get_swing2Limit_Injected_m7C50B2E73B1C889969551CF2CF18EA349BD4B4E6 (CharacterJoint_tDA58379B9EB6DAF19FE5FD664FBA59B3843CEFE2 * __this, SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 * ___ret0, const RuntimeMethod* method)
{
	typedef void (*CharacterJoint_get_swing2Limit_Injected_m7C50B2E73B1C889969551CF2CF18EA349BD4B4E6_ftn) (CharacterJoint_tDA58379B9EB6DAF19FE5FD664FBA59B3843CEFE2 *, SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 *);
	static CharacterJoint_get_swing2Limit_Injected_m7C50B2E73B1C889969551CF2CF18EA349BD4B4E6_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CharacterJoint_get_swing2Limit_Injected_m7C50B2E73B1C889969551CF2CF18EA349BD4B4E6_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CharacterJoint::get_swing2Limit_Injected(UnityEngine.SoftJointLimit&)");
	_il2cpp_icall_func(__this, ___ret0);
}
// System.Void UnityEngine.CharacterJoint::set_swing2Limit_Injected(UnityEngine.SoftJointLimit&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharacterJoint_set_swing2Limit_Injected_m9A8090BAAE2846A452B872448EE3179C660AF0F4 (CharacterJoint_tDA58379B9EB6DAF19FE5FD664FBA59B3843CEFE2 * __this, SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 * ___value0, const RuntimeMethod* method)
{
	typedef void (*CharacterJoint_set_swing2Limit_Injected_m9A8090BAAE2846A452B872448EE3179C660AF0F4_ftn) (CharacterJoint_tDA58379B9EB6DAF19FE5FD664FBA59B3843CEFE2 *, SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 *);
	static CharacterJoint_set_swing2Limit_Injected_m9A8090BAAE2846A452B872448EE3179C660AF0F4_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CharacterJoint_set_swing2Limit_Injected_m9A8090BAAE2846A452B872448EE3179C660AF0F4_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CharacterJoint::set_swing2Limit_Injected(UnityEngine.SoftJointLimit&)");
	_il2cpp_icall_func(__this, ___value0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean UnityEngine.Collider::get_enabled()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Collider_get_enabled_mED644D98C6AC2DF95BD86145E8D31AD7081C76EB (Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * __this, const RuntimeMethod* method)
{
	typedef bool (*Collider_get_enabled_mED644D98C6AC2DF95BD86145E8D31AD7081C76EB_ftn) (Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF *);
	static Collider_get_enabled_mED644D98C6AC2DF95BD86145E8D31AD7081C76EB_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Collider_get_enabled_mED644D98C6AC2DF95BD86145E8D31AD7081C76EB_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Collider::get_enabled()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Collider::set_enabled(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Collider_set_enabled_mF84DE8B0C8CAF33ACDB7F29BC055D9C8CFACB57B (Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * __this, bool ___value0, const RuntimeMethod* method)
{
	typedef void (*Collider_set_enabled_mF84DE8B0C8CAF33ACDB7F29BC055D9C8CFACB57B_ftn) (Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF *, bool);
	static Collider_set_enabled_mF84DE8B0C8CAF33ACDB7F29BC055D9C8CFACB57B_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Collider_set_enabled_mF84DE8B0C8CAF33ACDB7F29BC055D9C8CFACB57B_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Collider::set_enabled(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Rigidbody UnityEngine.Collider::get_attachedRigidbody()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * Collider_get_attachedRigidbody_m9E3C688EAE2F6A76C9AC14968D96769D9A71B1E8 (Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * __this, const RuntimeMethod* method)
{
	typedef Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * (*Collider_get_attachedRigidbody_m9E3C688EAE2F6A76C9AC14968D96769D9A71B1E8_ftn) (Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF *);
	static Collider_get_attachedRigidbody_m9E3C688EAE2F6A76C9AC14968D96769D9A71B1E8_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Collider_get_attachedRigidbody_m9E3C688EAE2F6A76C9AC14968D96769D9A71B1E8_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Collider::get_attachedRigidbody()");
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Boolean UnityEngine.Collider::get_isTrigger()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Collider_get_isTrigger_m08B7B55C34B99492CE923444B5253A7812BC8D8E (Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * __this, const RuntimeMethod* method)
{
	typedef bool (*Collider_get_isTrigger_m08B7B55C34B99492CE923444B5253A7812BC8D8E_ftn) (Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF *);
	static Collider_get_isTrigger_m08B7B55C34B99492CE923444B5253A7812BC8D8E_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Collider_get_isTrigger_m08B7B55C34B99492CE923444B5253A7812BC8D8E_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Collider::get_isTrigger()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Collider::set_isTrigger(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Collider_set_isTrigger_mD9EB1E99EA96B08398D68188F2DEB2434C1890C5 (Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * __this, bool ___value0, const RuntimeMethod* method)
{
	typedef void (*Collider_set_isTrigger_mD9EB1E99EA96B08398D68188F2DEB2434C1890C5_ftn) (Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF *, bool);
	static Collider_set_isTrigger_mD9EB1E99EA96B08398D68188F2DEB2434C1890C5_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Collider_set_isTrigger_mD9EB1E99EA96B08398D68188F2DEB2434C1890C5_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Collider::set_isTrigger(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Bounds UnityEngine.Collider::get_bounds()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  Collider_get_bounds_mD3CB68E38FB998406193A88D18C01F510272058A (Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * __this, const RuntimeMethod* method)
{
	Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Collider_get_bounds_Injected_m90D0FE433CCD44CC83E1089055A5A17286F9B1E6(__this, (Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 *)(&V_0), /*hidden argument*/NULL);
		Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Collider::set_material(UnityEngine.PhysicMaterial)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Collider_set_material_m2978E803DF20381466E0BD1F41F759DA015C5E74 (Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * __this, PhysicMaterial_tBEBB6F4620A5221A4CBAEDB2E5984CCA70AA07F8 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Collider_set_material_m2978E803DF20381466E0BD1F41F759DA015C5E74_ftn) (Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF *, PhysicMaterial_tBEBB6F4620A5221A4CBAEDB2E5984CCA70AA07F8 *);
	static Collider_set_material_m2978E803DF20381466E0BD1F41F759DA015C5E74_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Collider_set_material_m2978E803DF20381466E0BD1F41F759DA015C5E74_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Collider::set_material(UnityEngine.PhysicMaterial)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Collider::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Collider__ctor_m4E6D2F06C3BDB9CB006BD94C294049209D7563ED (Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * __this, const RuntimeMethod* method)
{
	{
		Component__ctor_m5E2740C0ACA4B368BC460315FAA2EDBFEAC0B8EF(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Collider::get_bounds_Injected(UnityEngine.Bounds&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Collider_get_bounds_Injected_m90D0FE433CCD44CC83E1089055A5A17286F9B1E6 (Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * __this, Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 * ___ret0, const RuntimeMethod* method)
{
	typedef void (*Collider_get_bounds_Injected_m90D0FE433CCD44CC83E1089055A5A17286F9B1E6_ftn) (Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF *, Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 *);
	static Collider_get_bounds_Injected_m90D0FE433CCD44CC83E1089055A5A17286F9B1E6_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Collider_get_bounds_Injected_m90D0FE433CCD44CC83E1089055A5A17286F9B1E6_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Collider::get_bounds_Injected(UnityEngine.Bounds&)");
	_il2cpp_icall_func(__this, ___ret0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UnityEngine.Collision
IL2CPP_EXTERN_C void Collision_t7FF0F4B0E24A2AEB1131DD980F63AB8CBF11FC3C_marshal_pinvoke(const Collision_t7FF0F4B0E24A2AEB1131DD980F63AB8CBF11FC3C& unmarshaled, Collision_t7FF0F4B0E24A2AEB1131DD980F63AB8CBF11FC3C_marshaled_pinvoke& marshaled)
{
	Exception_t* ___m_Rigidbody_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Rigidbody' of type 'Collision': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Rigidbody_2Exception, NULL);
}
IL2CPP_EXTERN_C void Collision_t7FF0F4B0E24A2AEB1131DD980F63AB8CBF11FC3C_marshal_pinvoke_back(const Collision_t7FF0F4B0E24A2AEB1131DD980F63AB8CBF11FC3C_marshaled_pinvoke& marshaled, Collision_t7FF0F4B0E24A2AEB1131DD980F63AB8CBF11FC3C& unmarshaled)
{
	Exception_t* ___m_Rigidbody_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Rigidbody' of type 'Collision': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Rigidbody_2Exception, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.Collision
IL2CPP_EXTERN_C void Collision_t7FF0F4B0E24A2AEB1131DD980F63AB8CBF11FC3C_marshal_pinvoke_cleanup(Collision_t7FF0F4B0E24A2AEB1131DD980F63AB8CBF11FC3C_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Collision
IL2CPP_EXTERN_C void Collision_t7FF0F4B0E24A2AEB1131DD980F63AB8CBF11FC3C_marshal_com(const Collision_t7FF0F4B0E24A2AEB1131DD980F63AB8CBF11FC3C& unmarshaled, Collision_t7FF0F4B0E24A2AEB1131DD980F63AB8CBF11FC3C_marshaled_com& marshaled)
{
	Exception_t* ___m_Rigidbody_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Rigidbody' of type 'Collision': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Rigidbody_2Exception, NULL);
}
IL2CPP_EXTERN_C void Collision_t7FF0F4B0E24A2AEB1131DD980F63AB8CBF11FC3C_marshal_com_back(const Collision_t7FF0F4B0E24A2AEB1131DD980F63AB8CBF11FC3C_marshaled_com& marshaled, Collision_t7FF0F4B0E24A2AEB1131DD980F63AB8CBF11FC3C& unmarshaled)
{
	Exception_t* ___m_Rigidbody_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Rigidbody' of type 'Collision': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Rigidbody_2Exception, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.Collision
IL2CPP_EXTERN_C void Collision_t7FF0F4B0E24A2AEB1131DD980F63AB8CBF11FC3C_marshal_com_cleanup(Collision_t7FF0F4B0E24A2AEB1131DD980F63AB8CBF11FC3C_marshaled_com& marshaled)
{
}
// UnityEngine.Rigidbody UnityEngine.Collision::get_rigidbody()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * Collision_get_rigidbody_m0D6A338F7DA1C6B62711034BBAF2444895D517C3 (Collision_t7FF0F4B0E24A2AEB1131DD980F63AB8CBF11FC3C * __this, const RuntimeMethod* method)
{
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * V_0 = NULL;
	{
		Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * L_0 = __this->get_m_Rigidbody_2();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Collider UnityEngine.Collision::get_collider()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * Collision_get_collider_m52F32CFE0BC1925C72A5B8EB743BBEF628201352 (Collision_t7FF0F4B0E24A2AEB1131DD980F63AB8CBF11FC3C * __this, const RuntimeMethod* method)
{
	Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * V_0 = NULL;
	{
		Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * L_0 = __this->get_m_Collider_3();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.GameObject UnityEngine.Collision::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * Collision_get_gameObject_m9A7069ABE50D4BB957A8ED76E5F4A59ACEC57C49 (Collision_t7FF0F4B0E24A2AEB1131DD980F63AB8CBF11FC3C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Collision_get_gameObject_m9A7069ABE50D4BB957A8ED76E5F4A59ACEC57C49_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * V_0 = NULL;
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * G_B3_0 = NULL;
	{
		Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * L_0 = __this->get_m_Rigidbody_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_0, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * L_2 = __this->get_m_Collider_3();
		NullCheck(L_2);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_3 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_0027;
	}

IL_001c:
	{
		Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * L_4 = __this->get_m_Rigidbody_2();
		NullCheck(L_4);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_5 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(L_4, /*hidden argument*/NULL);
		G_B3_0 = L_5;
	}

IL_0027:
	{
		V_0 = G_B3_0;
		goto IL_002a;
	}

IL_002a:
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_6 = V_0;
		return L_6;
	}
}
// UnityEngine.ContactPoint[] UnityEngine.Collision::get_contacts()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ContactPointU5BU5D_t10BB5D5BFFFA3C919FD97DFDEDB49D954AFB8EAA* Collision_get_contacts_m3807F7784D655257D7153CB615EF1FF7FAEAE0CF (Collision_t7FF0F4B0E24A2AEB1131DD980F63AB8CBF11FC3C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Collision_get_contacts_m3807F7784D655257D7153CB615EF1FF7FAEAE0CF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	ContactPointU5BU5D_t10BB5D5BFFFA3C919FD97DFDEDB49D954AFB8EAA* V_1 = NULL;
	{
		ContactPointU5BU5D_t10BB5D5BFFFA3C919FD97DFDEDB49D954AFB8EAA* L_0 = __this->get_m_LegacyContacts_6();
		V_0 = (bool)((((RuntimeObject*)(ContactPointU5BU5D_t10BB5D5BFFFA3C919FD97DFDEDB49D954AFB8EAA*)L_0) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0039;
		}
	}
	{
		int32_t L_2 = __this->get_m_ContactCount_4();
		ContactPointU5BU5D_t10BB5D5BFFFA3C919FD97DFDEDB49D954AFB8EAA* L_3 = (ContactPointU5BU5D_t10BB5D5BFFFA3C919FD97DFDEDB49D954AFB8EAA*)(ContactPointU5BU5D_t10BB5D5BFFFA3C919FD97DFDEDB49D954AFB8EAA*)SZArrayNew(ContactPointU5BU5D_t10BB5D5BFFFA3C919FD97DFDEDB49D954AFB8EAA_il2cpp_TypeInfo_var, (uint32_t)L_2);
		__this->set_m_LegacyContacts_6(L_3);
		ContactPointU5BU5D_t10BB5D5BFFFA3C919FD97DFDEDB49D954AFB8EAA* L_4 = __this->get_m_ReusedContacts_5();
		ContactPointU5BU5D_t10BB5D5BFFFA3C919FD97DFDEDB49D954AFB8EAA* L_5 = __this->get_m_LegacyContacts_6();
		int32_t L_6 = __this->get_m_ContactCount_4();
		Array_Copy_m2D96731C600DE8A167348CA8BA796344E64F7434((RuntimeArray *)(RuntimeArray *)L_4, (RuntimeArray *)(RuntimeArray *)L_5, L_6, /*hidden argument*/NULL);
	}

IL_0039:
	{
		ContactPointU5BU5D_t10BB5D5BFFFA3C919FD97DFDEDB49D954AFB8EAA* L_7 = __this->get_m_LegacyContacts_6();
		V_1 = L_7;
		goto IL_0042;
	}

IL_0042:
	{
		ContactPointU5BU5D_t10BB5D5BFFFA3C919FD97DFDEDB49D954AFB8EAA* L_8 = V_1;
		return L_8;
	}
}
// UnityEngine.Vector3 UnityEngine.Collision::get_impulse()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Collision_get_impulse_mF9B3802CE7856CCE130B85BC383AE79674AE7432 (Collision_t7FF0F4B0E24A2AEB1131DD980F63AB8CBF11FC3C * __this, const RuntimeMethod* method)
{
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = __this->get_m_Impulse_0();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = V_0;
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Vector3 UnityEngine.ConfigurableJoint::get_secondaryAxis()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ConfigurableJoint_get_secondaryAxis_mE9390C46782477183A7E1542AAEAA16F743C1139 (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 * __this, const RuntimeMethod* method)
{
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		ConfigurableJoint_get_secondaryAxis_Injected_m6D51BC27202AACE1FA1BA20FF59298C66D794540(__this, (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_0), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.ConfigurableJoint::set_secondaryAxis(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConfigurableJoint_set_secondaryAxis_mC342ABE40AB6B5054B06ED4A8B712AAF79AB648A (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value0, const RuntimeMethod* method)
{
	{
		ConfigurableJoint_set_secondaryAxis_Injected_m8D630F896751C83CA9B07173DE8A120936733A91(__this, (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&___value0), /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.ConfigurableJointMotion UnityEngine.ConfigurableJoint::get_xMotion()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ConfigurableJoint_get_xMotion_mA0B0EB46F696B3F7AD21C8FCFE570F9204B843DC (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*ConfigurableJoint_get_xMotion_mA0B0EB46F696B3F7AD21C8FCFE570F9204B843DC_ftn) (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 *);
	static ConfigurableJoint_get_xMotion_mA0B0EB46F696B3F7AD21C8FCFE570F9204B843DC_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ConfigurableJoint_get_xMotion_mA0B0EB46F696B3F7AD21C8FCFE570F9204B843DC_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ConfigurableJoint::get_xMotion()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.ConfigurableJoint::set_xMotion(UnityEngine.ConfigurableJointMotion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConfigurableJoint_set_xMotion_mC5CF8F90391EC7C38B5825FEDF17CA2DF9647310 (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*ConfigurableJoint_set_xMotion_mC5CF8F90391EC7C38B5825FEDF17CA2DF9647310_ftn) (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 *, int32_t);
	static ConfigurableJoint_set_xMotion_mC5CF8F90391EC7C38B5825FEDF17CA2DF9647310_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ConfigurableJoint_set_xMotion_mC5CF8F90391EC7C38B5825FEDF17CA2DF9647310_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ConfigurableJoint::set_xMotion(UnityEngine.ConfigurableJointMotion)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.ConfigurableJointMotion UnityEngine.ConfigurableJoint::get_yMotion()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ConfigurableJoint_get_yMotion_m96FD055764DEC031E4028FEA73392AFC9B742AFD (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*ConfigurableJoint_get_yMotion_m96FD055764DEC031E4028FEA73392AFC9B742AFD_ftn) (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 *);
	static ConfigurableJoint_get_yMotion_m96FD055764DEC031E4028FEA73392AFC9B742AFD_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ConfigurableJoint_get_yMotion_m96FD055764DEC031E4028FEA73392AFC9B742AFD_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ConfigurableJoint::get_yMotion()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.ConfigurableJoint::set_yMotion(UnityEngine.ConfigurableJointMotion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConfigurableJoint_set_yMotion_mF331F91608B4578B02BE457098A5099B1D89D7D5 (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*ConfigurableJoint_set_yMotion_mF331F91608B4578B02BE457098A5099B1D89D7D5_ftn) (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 *, int32_t);
	static ConfigurableJoint_set_yMotion_mF331F91608B4578B02BE457098A5099B1D89D7D5_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ConfigurableJoint_set_yMotion_mF331F91608B4578B02BE457098A5099B1D89D7D5_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ConfigurableJoint::set_yMotion(UnityEngine.ConfigurableJointMotion)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.ConfigurableJointMotion UnityEngine.ConfigurableJoint::get_zMotion()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ConfigurableJoint_get_zMotion_m6107BE4C3522026A26D89936A0E30B4124BCA7A9 (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*ConfigurableJoint_get_zMotion_m6107BE4C3522026A26D89936A0E30B4124BCA7A9_ftn) (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 *);
	static ConfigurableJoint_get_zMotion_m6107BE4C3522026A26D89936A0E30B4124BCA7A9_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ConfigurableJoint_get_zMotion_m6107BE4C3522026A26D89936A0E30B4124BCA7A9_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ConfigurableJoint::get_zMotion()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.ConfigurableJoint::set_zMotion(UnityEngine.ConfigurableJointMotion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConfigurableJoint_set_zMotion_m9D2A9358472F9AC11A2FECDF9603BA79E004127A (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*ConfigurableJoint_set_zMotion_m9D2A9358472F9AC11A2FECDF9603BA79E004127A_ftn) (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 *, int32_t);
	static ConfigurableJoint_set_zMotion_m9D2A9358472F9AC11A2FECDF9603BA79E004127A_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ConfigurableJoint_set_zMotion_m9D2A9358472F9AC11A2FECDF9603BA79E004127A_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ConfigurableJoint::set_zMotion(UnityEngine.ConfigurableJointMotion)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.ConfigurableJointMotion UnityEngine.ConfigurableJoint::get_angularXMotion()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ConfigurableJoint_get_angularXMotion_mCB43695BB1E2D10EB43CAC105C2F1E33418A5CE0 (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*ConfigurableJoint_get_angularXMotion_mCB43695BB1E2D10EB43CAC105C2F1E33418A5CE0_ftn) (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 *);
	static ConfigurableJoint_get_angularXMotion_mCB43695BB1E2D10EB43CAC105C2F1E33418A5CE0_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ConfigurableJoint_get_angularXMotion_mCB43695BB1E2D10EB43CAC105C2F1E33418A5CE0_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ConfigurableJoint::get_angularXMotion()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.ConfigurableJoint::set_angularXMotion(UnityEngine.ConfigurableJointMotion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConfigurableJoint_set_angularXMotion_mCC6D6951DC0D2BC40CE4AEA4F2DE224A39ED630E (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*ConfigurableJoint_set_angularXMotion_mCC6D6951DC0D2BC40CE4AEA4F2DE224A39ED630E_ftn) (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 *, int32_t);
	static ConfigurableJoint_set_angularXMotion_mCC6D6951DC0D2BC40CE4AEA4F2DE224A39ED630E_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ConfigurableJoint_set_angularXMotion_mCC6D6951DC0D2BC40CE4AEA4F2DE224A39ED630E_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ConfigurableJoint::set_angularXMotion(UnityEngine.ConfigurableJointMotion)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.ConfigurableJointMotion UnityEngine.ConfigurableJoint::get_angularYMotion()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ConfigurableJoint_get_angularYMotion_mD22A8F18305F7ADCC9E17524CA8D46B289B127E8 (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*ConfigurableJoint_get_angularYMotion_mD22A8F18305F7ADCC9E17524CA8D46B289B127E8_ftn) (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 *);
	static ConfigurableJoint_get_angularYMotion_mD22A8F18305F7ADCC9E17524CA8D46B289B127E8_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ConfigurableJoint_get_angularYMotion_mD22A8F18305F7ADCC9E17524CA8D46B289B127E8_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ConfigurableJoint::get_angularYMotion()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.ConfigurableJoint::set_angularYMotion(UnityEngine.ConfigurableJointMotion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConfigurableJoint_set_angularYMotion_m38E70B9932443F547BFE8C15F804ADA140BAB360 (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*ConfigurableJoint_set_angularYMotion_m38E70B9932443F547BFE8C15F804ADA140BAB360_ftn) (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 *, int32_t);
	static ConfigurableJoint_set_angularYMotion_m38E70B9932443F547BFE8C15F804ADA140BAB360_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ConfigurableJoint_set_angularYMotion_m38E70B9932443F547BFE8C15F804ADA140BAB360_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ConfigurableJoint::set_angularYMotion(UnityEngine.ConfigurableJointMotion)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.ConfigurableJointMotion UnityEngine.ConfigurableJoint::get_angularZMotion()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ConfigurableJoint_get_angularZMotion_m49D868A9CFE293E6A8D697ABDF952B613123FD28 (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*ConfigurableJoint_get_angularZMotion_m49D868A9CFE293E6A8D697ABDF952B613123FD28_ftn) (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 *);
	static ConfigurableJoint_get_angularZMotion_m49D868A9CFE293E6A8D697ABDF952B613123FD28_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ConfigurableJoint_get_angularZMotion_m49D868A9CFE293E6A8D697ABDF952B613123FD28_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ConfigurableJoint::get_angularZMotion()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.ConfigurableJoint::set_angularZMotion(UnityEngine.ConfigurableJointMotion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConfigurableJoint_set_angularZMotion_mB4CAE9B73204E261174AECB3C45069FF2E0FBBDE (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*ConfigurableJoint_set_angularZMotion_mB4CAE9B73204E261174AECB3C45069FF2E0FBBDE_ftn) (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 *, int32_t);
	static ConfigurableJoint_set_angularZMotion_mB4CAE9B73204E261174AECB3C45069FF2E0FBBDE_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ConfigurableJoint_set_angularZMotion_mB4CAE9B73204E261174AECB3C45069FF2E0FBBDE_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ConfigurableJoint::set_angularZMotion(UnityEngine.ConfigurableJointMotion)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.ConfigurableJoint::set_linearLimitSpring(UnityEngine.SoftJointLimitSpring)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConfigurableJoint_set_linearLimitSpring_m605DDD6622C8FBA039CC9B18C7747DB6F73EF14C (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 * __this, SoftJointLimitSpring_t8D4D658B29266F9573CB134D0DBC15017488FDC2  ___value0, const RuntimeMethod* method)
{
	{
		ConfigurableJoint_set_linearLimitSpring_Injected_mE90C49B513E7B3EA7ABFA95A3B8ED2A73730C5AB(__this, (SoftJointLimitSpring_t8D4D658B29266F9573CB134D0DBC15017488FDC2 *)(&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ConfigurableJoint::set_angularXLimitSpring(UnityEngine.SoftJointLimitSpring)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConfigurableJoint_set_angularXLimitSpring_m1EE955EED38E15A7E899F505A68DF34DFC3FAF60 (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 * __this, SoftJointLimitSpring_t8D4D658B29266F9573CB134D0DBC15017488FDC2  ___value0, const RuntimeMethod* method)
{
	{
		ConfigurableJoint_set_angularXLimitSpring_Injected_m01FA0164ABAF03566B4158C6DB7A068DEDA038BE(__this, (SoftJointLimitSpring_t8D4D658B29266F9573CB134D0DBC15017488FDC2 *)(&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ConfigurableJoint::set_angularYZLimitSpring(UnityEngine.SoftJointLimitSpring)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConfigurableJoint_set_angularYZLimitSpring_m98FA9937FC9D0C05D54BD5A28CA56A324DB049ED (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 * __this, SoftJointLimitSpring_t8D4D658B29266F9573CB134D0DBC15017488FDC2  ___value0, const RuntimeMethod* method)
{
	{
		ConfigurableJoint_set_angularYZLimitSpring_Injected_mC5AF9A234E7E867197988D78BFA271C8F0AE0B2C(__this, (SoftJointLimitSpring_t8D4D658B29266F9573CB134D0DBC15017488FDC2 *)(&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ConfigurableJoint::set_linearLimit(UnityEngine.SoftJointLimit)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConfigurableJoint_set_linearLimit_mF600A726E8D4E5A49DABE0756636AD77FF7F9406 (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 * __this, SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9  ___value0, const RuntimeMethod* method)
{
	{
		ConfigurableJoint_set_linearLimit_Injected_m07AFBF1C2812A55724A0D8877E7BC47DE4AD6236(__this, (SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 *)(&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ConfigurableJoint::set_lowAngularXLimit(UnityEngine.SoftJointLimit)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConfigurableJoint_set_lowAngularXLimit_mF03F0EFDBBE85A29FE3D787E5607118DD08B71EB (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 * __this, SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9  ___value0, const RuntimeMethod* method)
{
	{
		ConfigurableJoint_set_lowAngularXLimit_Injected_m8363CB59C106D8D98E1CE44974A2935AA2722A88(__this, (SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 *)(&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ConfigurableJoint::set_highAngularXLimit(UnityEngine.SoftJointLimit)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConfigurableJoint_set_highAngularXLimit_m9CF8EFEE498A2328943E19E15BF9A7C4539A5132 (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 * __this, SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9  ___value0, const RuntimeMethod* method)
{
	{
		ConfigurableJoint_set_highAngularXLimit_Injected_m46820CB6DD56738EE2E83A2603EFE8A993332F77(__this, (SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 *)(&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ConfigurableJoint::set_angularYLimit(UnityEngine.SoftJointLimit)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConfigurableJoint_set_angularYLimit_m39CD3BD8ACCBB7F685BCF44B973E700AD11A3EAA (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 * __this, SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9  ___value0, const RuntimeMethod* method)
{
	{
		ConfigurableJoint_set_angularYLimit_Injected_m7FFFFAA6B3057DD3B48DFB0DC557C81046D3292A(__this, (SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 *)(&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ConfigurableJoint::set_angularZLimit(UnityEngine.SoftJointLimit)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConfigurableJoint_set_angularZLimit_m3A4C51AD727F48FE28E4886A8EC60508420B1DCC (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 * __this, SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9  ___value0, const RuntimeMethod* method)
{
	{
		ConfigurableJoint_set_angularZLimit_Injected_m1F8CA881EFF4CB5C14F56296F33F55446CE95ECA(__this, (SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 *)(&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ConfigurableJoint::set_targetRotation(UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConfigurableJoint_set_targetRotation_m77F193BD35FBF52926CF40165B6A9D776C9103B6 (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 * __this, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___value0, const RuntimeMethod* method)
{
	{
		ConfigurableJoint_set_targetRotation_Injected_m9992253D130FFAB7F74C360A6874A30329799B38(__this, (Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 *)(&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ConfigurableJoint::set_targetAngularVelocity(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConfigurableJoint_set_targetAngularVelocity_m39DA1316EFFDA5D6B04A90587C488A4099F01A0B (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value0, const RuntimeMethod* method)
{
	{
		ConfigurableJoint_set_targetAngularVelocity_Injected_m400680616F59B0EA2BAB6B96DBFA28BF731DBFBF(__this, (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ConfigurableJoint::set_rotationDriveMode(UnityEngine.RotationDriveMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConfigurableJoint_set_rotationDriveMode_m9815890EA96A93E0BBCB5BD20EC7779D2E2A514A (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*ConfigurableJoint_set_rotationDriveMode_m9815890EA96A93E0BBCB5BD20EC7779D2E2A514A_ftn) (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 *, int32_t);
	static ConfigurableJoint_set_rotationDriveMode_m9815890EA96A93E0BBCB5BD20EC7779D2E2A514A_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ConfigurableJoint_set_rotationDriveMode_m9815890EA96A93E0BBCB5BD20EC7779D2E2A514A_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ConfigurableJoint::set_rotationDriveMode(UnityEngine.RotationDriveMode)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.ConfigurableJoint::set_slerpDrive(UnityEngine.JointDrive)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConfigurableJoint_set_slerpDrive_m341A1BFCB9DCF19F036A678E8864AC62F77B4B55 (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 * __this, JointDrive_t2082D48A52244171DA92ED5FC110D2A58BCBB575  ___value0, const RuntimeMethod* method)
{
	{
		ConfigurableJoint_set_slerpDrive_Injected_mDFDC04EFC76F28B3EEE803CCFCAD620DDC06D1B6(__this, (JointDrive_t2082D48A52244171DA92ED5FC110D2A58BCBB575 *)(&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ConfigurableJoint::set_projectionMode(UnityEngine.JointProjectionMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConfigurableJoint_set_projectionMode_m5A4660083BA0AB076BAE52626CC46FAFC06B8262 (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*ConfigurableJoint_set_projectionMode_m5A4660083BA0AB076BAE52626CC46FAFC06B8262_ftn) (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 *, int32_t);
	static ConfigurableJoint_set_projectionMode_m5A4660083BA0AB076BAE52626CC46FAFC06B8262_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ConfigurableJoint_set_projectionMode_m5A4660083BA0AB076BAE52626CC46FAFC06B8262_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ConfigurableJoint::set_projectionMode(UnityEngine.JointProjectionMode)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.ConfigurableJoint::set_projectionDistance(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConfigurableJoint_set_projectionDistance_m1789C1F4F493FF46507491C06EFD77F516C9E7B1 (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 * __this, float ___value0, const RuntimeMethod* method)
{
	typedef void (*ConfigurableJoint_set_projectionDistance_m1789C1F4F493FF46507491C06EFD77F516C9E7B1_ftn) (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 *, float);
	static ConfigurableJoint_set_projectionDistance_m1789C1F4F493FF46507491C06EFD77F516C9E7B1_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ConfigurableJoint_set_projectionDistance_m1789C1F4F493FF46507491C06EFD77F516C9E7B1_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ConfigurableJoint::set_projectionDistance(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.ConfigurableJoint::set_projectionAngle(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConfigurableJoint_set_projectionAngle_mD6D0C5890F88043862395E3329021B9BBA55DC95 (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 * __this, float ___value0, const RuntimeMethod* method)
{
	typedef void (*ConfigurableJoint_set_projectionAngle_mD6D0C5890F88043862395E3329021B9BBA55DC95_ftn) (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 *, float);
	static ConfigurableJoint_set_projectionAngle_mD6D0C5890F88043862395E3329021B9BBA55DC95_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ConfigurableJoint_set_projectionAngle_mD6D0C5890F88043862395E3329021B9BBA55DC95_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ConfigurableJoint::set_projectionAngle(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.ConfigurableJoint::set_configuredInWorldSpace(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConfigurableJoint_set_configuredInWorldSpace_m23EA574084D35FA87640897F04EAC3236754339E (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 * __this, bool ___value0, const RuntimeMethod* method)
{
	typedef void (*ConfigurableJoint_set_configuredInWorldSpace_m23EA574084D35FA87640897F04EAC3236754339E_ftn) (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 *, bool);
	static ConfigurableJoint_set_configuredInWorldSpace_m23EA574084D35FA87640897F04EAC3236754339E_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ConfigurableJoint_set_configuredInWorldSpace_m23EA574084D35FA87640897F04EAC3236754339E_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ConfigurableJoint::set_configuredInWorldSpace(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.ConfigurableJoint::get_secondaryAxis_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConfigurableJoint_get_secondaryAxis_Injected_m6D51BC27202AACE1FA1BA20FF59298C66D794540 (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___ret0, const RuntimeMethod* method)
{
	typedef void (*ConfigurableJoint_get_secondaryAxis_Injected_m6D51BC27202AACE1FA1BA20FF59298C66D794540_ftn) (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 *, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *);
	static ConfigurableJoint_get_secondaryAxis_Injected_m6D51BC27202AACE1FA1BA20FF59298C66D794540_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ConfigurableJoint_get_secondaryAxis_Injected_m6D51BC27202AACE1FA1BA20FF59298C66D794540_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ConfigurableJoint::get_secondaryAxis_Injected(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___ret0);
}
// System.Void UnityEngine.ConfigurableJoint::set_secondaryAxis_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConfigurableJoint_set_secondaryAxis_Injected_m8D630F896751C83CA9B07173DE8A120936733A91 (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___value0, const RuntimeMethod* method)
{
	typedef void (*ConfigurableJoint_set_secondaryAxis_Injected_m8D630F896751C83CA9B07173DE8A120936733A91_ftn) (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 *, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *);
	static ConfigurableJoint_set_secondaryAxis_Injected_m8D630F896751C83CA9B07173DE8A120936733A91_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ConfigurableJoint_set_secondaryAxis_Injected_m8D630F896751C83CA9B07173DE8A120936733A91_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ConfigurableJoint::set_secondaryAxis_Injected(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.ConfigurableJoint::set_linearLimitSpring_Injected(UnityEngine.SoftJointLimitSpring&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConfigurableJoint_set_linearLimitSpring_Injected_mE90C49B513E7B3EA7ABFA95A3B8ED2A73730C5AB (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 * __this, SoftJointLimitSpring_t8D4D658B29266F9573CB134D0DBC15017488FDC2 * ___value0, const RuntimeMethod* method)
{
	typedef void (*ConfigurableJoint_set_linearLimitSpring_Injected_mE90C49B513E7B3EA7ABFA95A3B8ED2A73730C5AB_ftn) (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 *, SoftJointLimitSpring_t8D4D658B29266F9573CB134D0DBC15017488FDC2 *);
	static ConfigurableJoint_set_linearLimitSpring_Injected_mE90C49B513E7B3EA7ABFA95A3B8ED2A73730C5AB_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ConfigurableJoint_set_linearLimitSpring_Injected_mE90C49B513E7B3EA7ABFA95A3B8ED2A73730C5AB_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ConfigurableJoint::set_linearLimitSpring_Injected(UnityEngine.SoftJointLimitSpring&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.ConfigurableJoint::set_angularXLimitSpring_Injected(UnityEngine.SoftJointLimitSpring&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConfigurableJoint_set_angularXLimitSpring_Injected_m01FA0164ABAF03566B4158C6DB7A068DEDA038BE (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 * __this, SoftJointLimitSpring_t8D4D658B29266F9573CB134D0DBC15017488FDC2 * ___value0, const RuntimeMethod* method)
{
	typedef void (*ConfigurableJoint_set_angularXLimitSpring_Injected_m01FA0164ABAF03566B4158C6DB7A068DEDA038BE_ftn) (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 *, SoftJointLimitSpring_t8D4D658B29266F9573CB134D0DBC15017488FDC2 *);
	static ConfigurableJoint_set_angularXLimitSpring_Injected_m01FA0164ABAF03566B4158C6DB7A068DEDA038BE_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ConfigurableJoint_set_angularXLimitSpring_Injected_m01FA0164ABAF03566B4158C6DB7A068DEDA038BE_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ConfigurableJoint::set_angularXLimitSpring_Injected(UnityEngine.SoftJointLimitSpring&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.ConfigurableJoint::set_angularYZLimitSpring_Injected(UnityEngine.SoftJointLimitSpring&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConfigurableJoint_set_angularYZLimitSpring_Injected_mC5AF9A234E7E867197988D78BFA271C8F0AE0B2C (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 * __this, SoftJointLimitSpring_t8D4D658B29266F9573CB134D0DBC15017488FDC2 * ___value0, const RuntimeMethod* method)
{
	typedef void (*ConfigurableJoint_set_angularYZLimitSpring_Injected_mC5AF9A234E7E867197988D78BFA271C8F0AE0B2C_ftn) (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 *, SoftJointLimitSpring_t8D4D658B29266F9573CB134D0DBC15017488FDC2 *);
	static ConfigurableJoint_set_angularYZLimitSpring_Injected_mC5AF9A234E7E867197988D78BFA271C8F0AE0B2C_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ConfigurableJoint_set_angularYZLimitSpring_Injected_mC5AF9A234E7E867197988D78BFA271C8F0AE0B2C_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ConfigurableJoint::set_angularYZLimitSpring_Injected(UnityEngine.SoftJointLimitSpring&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.ConfigurableJoint::set_linearLimit_Injected(UnityEngine.SoftJointLimit&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConfigurableJoint_set_linearLimit_Injected_m07AFBF1C2812A55724A0D8877E7BC47DE4AD6236 (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 * __this, SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 * ___value0, const RuntimeMethod* method)
{
	typedef void (*ConfigurableJoint_set_linearLimit_Injected_m07AFBF1C2812A55724A0D8877E7BC47DE4AD6236_ftn) (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 *, SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 *);
	static ConfigurableJoint_set_linearLimit_Injected_m07AFBF1C2812A55724A0D8877E7BC47DE4AD6236_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ConfigurableJoint_set_linearLimit_Injected_m07AFBF1C2812A55724A0D8877E7BC47DE4AD6236_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ConfigurableJoint::set_linearLimit_Injected(UnityEngine.SoftJointLimit&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.ConfigurableJoint::set_lowAngularXLimit_Injected(UnityEngine.SoftJointLimit&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConfigurableJoint_set_lowAngularXLimit_Injected_m8363CB59C106D8D98E1CE44974A2935AA2722A88 (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 * __this, SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 * ___value0, const RuntimeMethod* method)
{
	typedef void (*ConfigurableJoint_set_lowAngularXLimit_Injected_m8363CB59C106D8D98E1CE44974A2935AA2722A88_ftn) (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 *, SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 *);
	static ConfigurableJoint_set_lowAngularXLimit_Injected_m8363CB59C106D8D98E1CE44974A2935AA2722A88_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ConfigurableJoint_set_lowAngularXLimit_Injected_m8363CB59C106D8D98E1CE44974A2935AA2722A88_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ConfigurableJoint::set_lowAngularXLimit_Injected(UnityEngine.SoftJointLimit&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.ConfigurableJoint::set_highAngularXLimit_Injected(UnityEngine.SoftJointLimit&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConfigurableJoint_set_highAngularXLimit_Injected_m46820CB6DD56738EE2E83A2603EFE8A993332F77 (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 * __this, SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 * ___value0, const RuntimeMethod* method)
{
	typedef void (*ConfigurableJoint_set_highAngularXLimit_Injected_m46820CB6DD56738EE2E83A2603EFE8A993332F77_ftn) (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 *, SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 *);
	static ConfigurableJoint_set_highAngularXLimit_Injected_m46820CB6DD56738EE2E83A2603EFE8A993332F77_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ConfigurableJoint_set_highAngularXLimit_Injected_m46820CB6DD56738EE2E83A2603EFE8A993332F77_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ConfigurableJoint::set_highAngularXLimit_Injected(UnityEngine.SoftJointLimit&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.ConfigurableJoint::set_angularYLimit_Injected(UnityEngine.SoftJointLimit&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConfigurableJoint_set_angularYLimit_Injected_m7FFFFAA6B3057DD3B48DFB0DC557C81046D3292A (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 * __this, SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 * ___value0, const RuntimeMethod* method)
{
	typedef void (*ConfigurableJoint_set_angularYLimit_Injected_m7FFFFAA6B3057DD3B48DFB0DC557C81046D3292A_ftn) (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 *, SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 *);
	static ConfigurableJoint_set_angularYLimit_Injected_m7FFFFAA6B3057DD3B48DFB0DC557C81046D3292A_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ConfigurableJoint_set_angularYLimit_Injected_m7FFFFAA6B3057DD3B48DFB0DC557C81046D3292A_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ConfigurableJoint::set_angularYLimit_Injected(UnityEngine.SoftJointLimit&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.ConfigurableJoint::set_angularZLimit_Injected(UnityEngine.SoftJointLimit&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConfigurableJoint_set_angularZLimit_Injected_m1F8CA881EFF4CB5C14F56296F33F55446CE95ECA (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 * __this, SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 * ___value0, const RuntimeMethod* method)
{
	typedef void (*ConfigurableJoint_set_angularZLimit_Injected_m1F8CA881EFF4CB5C14F56296F33F55446CE95ECA_ftn) (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 *, SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 *);
	static ConfigurableJoint_set_angularZLimit_Injected_m1F8CA881EFF4CB5C14F56296F33F55446CE95ECA_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ConfigurableJoint_set_angularZLimit_Injected_m1F8CA881EFF4CB5C14F56296F33F55446CE95ECA_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ConfigurableJoint::set_angularZLimit_Injected(UnityEngine.SoftJointLimit&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.ConfigurableJoint::set_targetRotation_Injected(UnityEngine.Quaternion&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConfigurableJoint_set_targetRotation_Injected_m9992253D130FFAB7F74C360A6874A30329799B38 (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 * __this, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * ___value0, const RuntimeMethod* method)
{
	typedef void (*ConfigurableJoint_set_targetRotation_Injected_m9992253D130FFAB7F74C360A6874A30329799B38_ftn) (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 *, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 *);
	static ConfigurableJoint_set_targetRotation_Injected_m9992253D130FFAB7F74C360A6874A30329799B38_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ConfigurableJoint_set_targetRotation_Injected_m9992253D130FFAB7F74C360A6874A30329799B38_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ConfigurableJoint::set_targetRotation_Injected(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.ConfigurableJoint::set_targetAngularVelocity_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConfigurableJoint_set_targetAngularVelocity_Injected_m400680616F59B0EA2BAB6B96DBFA28BF731DBFBF (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___value0, const RuntimeMethod* method)
{
	typedef void (*ConfigurableJoint_set_targetAngularVelocity_Injected_m400680616F59B0EA2BAB6B96DBFA28BF731DBFBF_ftn) (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 *, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *);
	static ConfigurableJoint_set_targetAngularVelocity_Injected_m400680616F59B0EA2BAB6B96DBFA28BF731DBFBF_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ConfigurableJoint_set_targetAngularVelocity_Injected_m400680616F59B0EA2BAB6B96DBFA28BF731DBFBF_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ConfigurableJoint::set_targetAngularVelocity_Injected(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.ConfigurableJoint::set_slerpDrive_Injected(UnityEngine.JointDrive&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConfigurableJoint_set_slerpDrive_Injected_mDFDC04EFC76F28B3EEE803CCFCAD620DDC06D1B6 (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 * __this, JointDrive_t2082D48A52244171DA92ED5FC110D2A58BCBB575 * ___value0, const RuntimeMethod* method)
{
	typedef void (*ConfigurableJoint_set_slerpDrive_Injected_mDFDC04EFC76F28B3EEE803CCFCAD620DDC06D1B6_ftn) (ConfigurableJoint_tBDB5DA1730E94E4E2FA0E4550624D3616B6AF1D7 *, JointDrive_t2082D48A52244171DA92ED5FC110D2A58BCBB575 *);
	static ConfigurableJoint_set_slerpDrive_Injected_mDFDC04EFC76F28B3EEE803CCFCAD620DDC06D1B6_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ConfigurableJoint_set_slerpDrive_Injected_mDFDC04EFC76F28B3EEE803CCFCAD620DDC06D1B6_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ConfigurableJoint::set_slerpDrive_Injected(UnityEngine.JointDrive&)");
	_il2cpp_icall_func(__this, ___value0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Vector3 UnityEngine.ContactPoint::get_point()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ContactPoint_get_point_mB222EB009D8B3DBE5F248088167F074551B424F7 (ContactPoint_tE0D3A30ED34A1FC8CA3F7391348429F3232CA515 * __this, const RuntimeMethod* method)
{
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = __this->get_m_Point_0();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = V_0;
		return L_1;
	}
}
IL2CPP_EXTERN_C  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ContactPoint_get_point_mB222EB009D8B3DBE5F248088167F074551B424F7_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ContactPoint_tE0D3A30ED34A1FC8CA3F7391348429F3232CA515 * _thisAdjusted = reinterpret_cast<ContactPoint_tE0D3A30ED34A1FC8CA3F7391348429F3232CA515 *>(__this + _offset);
	return ContactPoint_get_point_mB222EB009D8B3DBE5F248088167F074551B424F7(_thisAdjusted, method);
}
// UnityEngine.Vector3 UnityEngine.ContactPoint::get_normal()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ContactPoint_get_normal_m0C47576D775F12C2BED5B25ACC720180008EDA3E (ContactPoint_tE0D3A30ED34A1FC8CA3F7391348429F3232CA515 * __this, const RuntimeMethod* method)
{
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = __this->get_m_Normal_1();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = V_0;
		return L_1;
	}
}
IL2CPP_EXTERN_C  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ContactPoint_get_normal_m0C47576D775F12C2BED5B25ACC720180008EDA3E_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ContactPoint_tE0D3A30ED34A1FC8CA3F7391348429F3232CA515 * _thisAdjusted = reinterpret_cast<ContactPoint_tE0D3A30ED34A1FC8CA3F7391348429F3232CA515 *>(__this + _offset);
	return ContactPoint_get_normal_m0C47576D775F12C2BED5B25ACC720180008EDA3E(_thisAdjusted, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UnityEngine.ControllerColliderHit
IL2CPP_EXTERN_C void ControllerColliderHit_tB009AA7F769B4A3E988DEF71F4C5A29AB6A38968_marshal_pinvoke(const ControllerColliderHit_tB009AA7F769B4A3E988DEF71F4C5A29AB6A38968& unmarshaled, ControllerColliderHit_tB009AA7F769B4A3E988DEF71F4C5A29AB6A38968_marshaled_pinvoke& marshaled)
{
	Exception_t* ___m_Controller_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Controller' of type 'ControllerColliderHit': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Controller_0Exception, NULL);
}
IL2CPP_EXTERN_C void ControllerColliderHit_tB009AA7F769B4A3E988DEF71F4C5A29AB6A38968_marshal_pinvoke_back(const ControllerColliderHit_tB009AA7F769B4A3E988DEF71F4C5A29AB6A38968_marshaled_pinvoke& marshaled, ControllerColliderHit_tB009AA7F769B4A3E988DEF71F4C5A29AB6A38968& unmarshaled)
{
	Exception_t* ___m_Controller_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Controller' of type 'ControllerColliderHit': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Controller_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ControllerColliderHit
IL2CPP_EXTERN_C void ControllerColliderHit_tB009AA7F769B4A3E988DEF71F4C5A29AB6A38968_marshal_pinvoke_cleanup(ControllerColliderHit_tB009AA7F769B4A3E988DEF71F4C5A29AB6A38968_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ControllerColliderHit
IL2CPP_EXTERN_C void ControllerColliderHit_tB009AA7F769B4A3E988DEF71F4C5A29AB6A38968_marshal_com(const ControllerColliderHit_tB009AA7F769B4A3E988DEF71F4C5A29AB6A38968& unmarshaled, ControllerColliderHit_tB009AA7F769B4A3E988DEF71F4C5A29AB6A38968_marshaled_com& marshaled)
{
	Exception_t* ___m_Controller_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Controller' of type 'ControllerColliderHit': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Controller_0Exception, NULL);
}
IL2CPP_EXTERN_C void ControllerColliderHit_tB009AA7F769B4A3E988DEF71F4C5A29AB6A38968_marshal_com_back(const ControllerColliderHit_tB009AA7F769B4A3E988DEF71F4C5A29AB6A38968_marshaled_com& marshaled, ControllerColliderHit_tB009AA7F769B4A3E988DEF71F4C5A29AB6A38968& unmarshaled)
{
	Exception_t* ___m_Controller_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Controller' of type 'ControllerColliderHit': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Controller_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ControllerColliderHit
IL2CPP_EXTERN_C void ControllerColliderHit_tB009AA7F769B4A3E988DEF71F4C5A29AB6A38968_marshal_com_cleanup(ControllerColliderHit_tB009AA7F769B4A3E988DEF71F4C5A29AB6A38968_marshaled_com& marshaled)
{
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.JointLimits UnityEngine.HingeJoint::get_limits()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR JointLimits_t89D8D0E21FB1E840F8380AFB9FB55E966A3030F3  HingeJoint_get_limits_m988B1E8056689C22248ABB05272559518C3E6990 (HingeJoint_tDA10BC5B1D01F5226ED5B0F83D106E296925036D * __this, const RuntimeMethod* method)
{
	JointLimits_t89D8D0E21FB1E840F8380AFB9FB55E966A3030F3  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		HingeJoint_get_limits_Injected_mBC5408A9C2B0381572A34180B58D3C8B4405B161(__this, (JointLimits_t89D8D0E21FB1E840F8380AFB9FB55E966A3030F3 *)(&V_0), /*hidden argument*/NULL);
		JointLimits_t89D8D0E21FB1E840F8380AFB9FB55E966A3030F3  L_0 = V_0;
		return L_0;
	}
}
// UnityEngine.JointSpring UnityEngine.HingeJoint::get_spring()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR JointSpring_t5BA0622E25A91A16BB70A8598BCCD11DD93FB3CD  HingeJoint_get_spring_m0BFA4E9ED60DBBDC97F43B4FF44C1F7E59E99789 (HingeJoint_tDA10BC5B1D01F5226ED5B0F83D106E296925036D * __this, const RuntimeMethod* method)
{
	JointSpring_t5BA0622E25A91A16BB70A8598BCCD11DD93FB3CD  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		HingeJoint_get_spring_Injected_mC61A3CC33620B8F1DF1E5F20CF89ED3E5861EB9A(__this, (JointSpring_t5BA0622E25A91A16BB70A8598BCCD11DD93FB3CD *)(&V_0), /*hidden argument*/NULL);
		JointSpring_t5BA0622E25A91A16BB70A8598BCCD11DD93FB3CD  L_0 = V_0;
		return L_0;
	}
}
// System.Boolean UnityEngine.HingeJoint::get_useMotor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool HingeJoint_get_useMotor_m2E14DFC0BA78C44C43B14A4012CF83F28703617B (HingeJoint_tDA10BC5B1D01F5226ED5B0F83D106E296925036D * __this, const RuntimeMethod* method)
{
	typedef bool (*HingeJoint_get_useMotor_m2E14DFC0BA78C44C43B14A4012CF83F28703617B_ftn) (HingeJoint_tDA10BC5B1D01F5226ED5B0F83D106E296925036D *);
	static HingeJoint_get_useMotor_m2E14DFC0BA78C44C43B14A4012CF83F28703617B_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (HingeJoint_get_useMotor_m2E14DFC0BA78C44C43B14A4012CF83F28703617B_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.HingeJoint::get_useMotor()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Boolean UnityEngine.HingeJoint::get_useLimits()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool HingeJoint_get_useLimits_mFF9EB175970941DB444545729AE808CEAD50CB55 (HingeJoint_tDA10BC5B1D01F5226ED5B0F83D106E296925036D * __this, const RuntimeMethod* method)
{
	typedef bool (*HingeJoint_get_useLimits_mFF9EB175970941DB444545729AE808CEAD50CB55_ftn) (HingeJoint_tDA10BC5B1D01F5226ED5B0F83D106E296925036D *);
	static HingeJoint_get_useLimits_mFF9EB175970941DB444545729AE808CEAD50CB55_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (HingeJoint_get_useLimits_mFF9EB175970941DB444545729AE808CEAD50CB55_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.HingeJoint::get_useLimits()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Boolean UnityEngine.HingeJoint::get_useSpring()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool HingeJoint_get_useSpring_mDA9FBD711920418719E71DE98F3CB3CE95BEB776 (HingeJoint_tDA10BC5B1D01F5226ED5B0F83D106E296925036D * __this, const RuntimeMethod* method)
{
	typedef bool (*HingeJoint_get_useSpring_mDA9FBD711920418719E71DE98F3CB3CE95BEB776_ftn) (HingeJoint_tDA10BC5B1D01F5226ED5B0F83D106E296925036D *);
	static HingeJoint_get_useSpring_mDA9FBD711920418719E71DE98F3CB3CE95BEB776_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (HingeJoint_get_useSpring_mDA9FBD711920418719E71DE98F3CB3CE95BEB776_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.HingeJoint::get_useSpring()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.HingeJoint::get_limits_Injected(UnityEngine.JointLimits&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HingeJoint_get_limits_Injected_mBC5408A9C2B0381572A34180B58D3C8B4405B161 (HingeJoint_tDA10BC5B1D01F5226ED5B0F83D106E296925036D * __this, JointLimits_t89D8D0E21FB1E840F8380AFB9FB55E966A3030F3 * ___ret0, const RuntimeMethod* method)
{
	typedef void (*HingeJoint_get_limits_Injected_mBC5408A9C2B0381572A34180B58D3C8B4405B161_ftn) (HingeJoint_tDA10BC5B1D01F5226ED5B0F83D106E296925036D *, JointLimits_t89D8D0E21FB1E840F8380AFB9FB55E966A3030F3 *);
	static HingeJoint_get_limits_Injected_mBC5408A9C2B0381572A34180B58D3C8B4405B161_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (HingeJoint_get_limits_Injected_mBC5408A9C2B0381572A34180B58D3C8B4405B161_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.HingeJoint::get_limits_Injected(UnityEngine.JointLimits&)");
	_il2cpp_icall_func(__this, ___ret0);
}
// System.Void UnityEngine.HingeJoint::get_spring_Injected(UnityEngine.JointSpring&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HingeJoint_get_spring_Injected_mC61A3CC33620B8F1DF1E5F20CF89ED3E5861EB9A (HingeJoint_tDA10BC5B1D01F5226ED5B0F83D106E296925036D * __this, JointSpring_t5BA0622E25A91A16BB70A8598BCCD11DD93FB3CD * ___ret0, const RuntimeMethod* method)
{
	typedef void (*HingeJoint_get_spring_Injected_mC61A3CC33620B8F1DF1E5F20CF89ED3E5861EB9A_ftn) (HingeJoint_tDA10BC5B1D01F5226ED5B0F83D106E296925036D *, JointSpring_t5BA0622E25A91A16BB70A8598BCCD11DD93FB3CD *);
	static HingeJoint_get_spring_Injected_mC61A3CC33620B8F1DF1E5F20CF89ED3E5861EB9A_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (HingeJoint_get_spring_Injected_mC61A3CC33620B8F1DF1E5F20CF89ED3E5861EB9A_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.HingeJoint::get_spring_Injected(UnityEngine.JointSpring&)");
	_il2cpp_icall_func(__this, ___ret0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Rigidbody UnityEngine.Joint::get_connectedBody()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * Joint_get_connectedBody_m6CAB9002D1AAC30C6D5A7B88660C7713D8C34A73 (Joint_t734F77C82E6372173403FCA61D9E7FC4618595AF * __this, const RuntimeMethod* method)
{
	typedef Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * (*Joint_get_connectedBody_m6CAB9002D1AAC30C6D5A7B88660C7713D8C34A73_ftn) (Joint_t734F77C82E6372173403FCA61D9E7FC4618595AF *);
	static Joint_get_connectedBody_m6CAB9002D1AAC30C6D5A7B88660C7713D8C34A73_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Joint_get_connectedBody_m6CAB9002D1AAC30C6D5A7B88660C7713D8C34A73_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Joint::get_connectedBody()");
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Joint::set_connectedBody(UnityEngine.Rigidbody)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Joint_set_connectedBody_mB5843F21494B4468A218DA96238467710126F762 (Joint_t734F77C82E6372173403FCA61D9E7FC4618595AF * __this, Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Joint_set_connectedBody_mB5843F21494B4468A218DA96238467710126F762_ftn) (Joint_t734F77C82E6372173403FCA61D9E7FC4618595AF *, Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 *);
	static Joint_set_connectedBody_mB5843F21494B4468A218DA96238467710126F762_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Joint_set_connectedBody_mB5843F21494B4468A218DA96238467710126F762_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Joint::set_connectedBody(UnityEngine.Rigidbody)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector3 UnityEngine.Joint::get_axis()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Joint_get_axis_m54A1BFEC3A3594CFFCF0C21FBEC15B5ADA7CB702 (Joint_t734F77C82E6372173403FCA61D9E7FC4618595AF * __this, const RuntimeMethod* method)
{
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Joint_get_axis_Injected_m2F5466A10251C5F740DFBFA597FDBAEA02A2AE02(__this, (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_0), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Joint::set_axis(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Joint_set_axis_mD77BFEA4D264E9365979261520BA5B83D86E26BA (Joint_t734F77C82E6372173403FCA61D9E7FC4618595AF * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value0, const RuntimeMethod* method)
{
	{
		Joint_set_axis_Injected_m7BFCEEDE9581D1B5F646E31F6AD3FA3990E3A9E4(__this, (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&___value0), /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Joint::get_anchor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Joint_get_anchor_m50C893690CD5C0E85C9A7F9AB66685BB990EF662 (Joint_t734F77C82E6372173403FCA61D9E7FC4618595AF * __this, const RuntimeMethod* method)
{
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Joint_get_anchor_Injected_mD0B6F04D2157DDD14E48278FAADA44AC5AF0433B(__this, (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_0), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Joint::set_anchor(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Joint_set_anchor_m9584AAAB1BA704C95E4C5C2E4844E45E3ADCA9D3 (Joint_t734F77C82E6372173403FCA61D9E7FC4618595AF * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value0, const RuntimeMethod* method)
{
	{
		Joint_set_anchor_Injected_mCDEEE334517B97D137C1AE4ADAAFAD9CD85399EB(__this, (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&___value0), /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Joint::get_connectedAnchor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Joint_get_connectedAnchor_mA5051D7B6BE0F109E3AE454E05CDE706FCB39D46 (Joint_t734F77C82E6372173403FCA61D9E7FC4618595AF * __this, const RuntimeMethod* method)
{
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Joint_get_connectedAnchor_Injected_m8DBF0F41D88D496175BE8F21660DE20653E3CC9C(__this, (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_0), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Joint::set_connectedAnchor(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Joint_set_connectedAnchor_m17111B0CD83F1F68D2E2323DFD9E5A01A57D8059 (Joint_t734F77C82E6372173403FCA61D9E7FC4618595AF * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value0, const RuntimeMethod* method)
{
	{
		Joint_set_connectedAnchor_Injected_m4CCEF375EFC641A7521AC470B6770282D17DEDED(__this, (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Joint::get_autoConfigureConnectedAnchor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Joint_get_autoConfigureConnectedAnchor_m34DB3B2F73BE4179BD5AE0D08872587564D65177 (Joint_t734F77C82E6372173403FCA61D9E7FC4618595AF * __this, const RuntimeMethod* method)
{
	typedef bool (*Joint_get_autoConfigureConnectedAnchor_m34DB3B2F73BE4179BD5AE0D08872587564D65177_ftn) (Joint_t734F77C82E6372173403FCA61D9E7FC4618595AF *);
	static Joint_get_autoConfigureConnectedAnchor_m34DB3B2F73BE4179BD5AE0D08872587564D65177_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Joint_get_autoConfigureConnectedAnchor_m34DB3B2F73BE4179BD5AE0D08872587564D65177_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Joint::get_autoConfigureConnectedAnchor()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Joint::set_autoConfigureConnectedAnchor(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Joint_set_autoConfigureConnectedAnchor_m1D576D32DC45DD451CD436AF28B30BA7116D4959 (Joint_t734F77C82E6372173403FCA61D9E7FC4618595AF * __this, bool ___value0, const RuntimeMethod* method)
{
	typedef void (*Joint_set_autoConfigureConnectedAnchor_m1D576D32DC45DD451CD436AF28B30BA7116D4959_ftn) (Joint_t734F77C82E6372173403FCA61D9E7FC4618595AF *, bool);
	static Joint_set_autoConfigureConnectedAnchor_m1D576D32DC45DD451CD436AF28B30BA7116D4959_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Joint_set_autoConfigureConnectedAnchor_m1D576D32DC45DD451CD436AF28B30BA7116D4959_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Joint::set_autoConfigureConnectedAnchor(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Joint::get_breakForce()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Joint_get_breakForce_m4421DE1B772CE45DEC21435610EF0E4F09BAC681 (Joint_t734F77C82E6372173403FCA61D9E7FC4618595AF * __this, const RuntimeMethod* method)
{
	typedef float (*Joint_get_breakForce_m4421DE1B772CE45DEC21435610EF0E4F09BAC681_ftn) (Joint_t734F77C82E6372173403FCA61D9E7FC4618595AF *);
	static Joint_get_breakForce_m4421DE1B772CE45DEC21435610EF0E4F09BAC681_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Joint_get_breakForce_m4421DE1B772CE45DEC21435610EF0E4F09BAC681_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Joint::get_breakForce()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Joint::set_breakForce(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Joint_set_breakForce_mDB7D58CE20412259C5717872ADC9A0B780857BF7 (Joint_t734F77C82E6372173403FCA61D9E7FC4618595AF * __this, float ___value0, const RuntimeMethod* method)
{
	typedef void (*Joint_set_breakForce_mDB7D58CE20412259C5717872ADC9A0B780857BF7_ftn) (Joint_t734F77C82E6372173403FCA61D9E7FC4618595AF *, float);
	static Joint_set_breakForce_mDB7D58CE20412259C5717872ADC9A0B780857BF7_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Joint_set_breakForce_mDB7D58CE20412259C5717872ADC9A0B780857BF7_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Joint::set_breakForce(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Joint::get_breakTorque()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Joint_get_breakTorque_mD9D60ADFBBDF2F0671CAAFBA1FBE8C5C4C655FDC (Joint_t734F77C82E6372173403FCA61D9E7FC4618595AF * __this, const RuntimeMethod* method)
{
	typedef float (*Joint_get_breakTorque_mD9D60ADFBBDF2F0671CAAFBA1FBE8C5C4C655FDC_ftn) (Joint_t734F77C82E6372173403FCA61D9E7FC4618595AF *);
	static Joint_get_breakTorque_mD9D60ADFBBDF2F0671CAAFBA1FBE8C5C4C655FDC_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Joint_get_breakTorque_mD9D60ADFBBDF2F0671CAAFBA1FBE8C5C4C655FDC_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Joint::get_breakTorque()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Joint::set_breakTorque(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Joint_set_breakTorque_m586C72D45EC3F17E43D6842118512FA403C72641 (Joint_t734F77C82E6372173403FCA61D9E7FC4618595AF * __this, float ___value0, const RuntimeMethod* method)
{
	typedef void (*Joint_set_breakTorque_m586C72D45EC3F17E43D6842118512FA403C72641_ftn) (Joint_t734F77C82E6372173403FCA61D9E7FC4618595AF *, float);
	static Joint_set_breakTorque_m586C72D45EC3F17E43D6842118512FA403C72641_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Joint_set_breakTorque_m586C72D45EC3F17E43D6842118512FA403C72641_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Joint::set_breakTorque(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.Joint::get_enableCollision()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Joint_get_enableCollision_m1B8589A969D1601AE8C2947885E974160D6338B4 (Joint_t734F77C82E6372173403FCA61D9E7FC4618595AF * __this, const RuntimeMethod* method)
{
	typedef bool (*Joint_get_enableCollision_m1B8589A969D1601AE8C2947885E974160D6338B4_ftn) (Joint_t734F77C82E6372173403FCA61D9E7FC4618595AF *);
	static Joint_get_enableCollision_m1B8589A969D1601AE8C2947885E974160D6338B4_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Joint_get_enableCollision_m1B8589A969D1601AE8C2947885E974160D6338B4_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Joint::get_enableCollision()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Joint::set_enableCollision(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Joint_set_enableCollision_mB91FAA42BBFD9A36AA8B6AA482A8E5AFC9E14CAB (Joint_t734F77C82E6372173403FCA61D9E7FC4618595AF * __this, bool ___value0, const RuntimeMethod* method)
{
	typedef void (*Joint_set_enableCollision_mB91FAA42BBFD9A36AA8B6AA482A8E5AFC9E14CAB_ftn) (Joint_t734F77C82E6372173403FCA61D9E7FC4618595AF *, bool);
	static Joint_set_enableCollision_mB91FAA42BBFD9A36AA8B6AA482A8E5AFC9E14CAB_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Joint_set_enableCollision_mB91FAA42BBFD9A36AA8B6AA482A8E5AFC9E14CAB_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Joint::set_enableCollision(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Joint::get_axis_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Joint_get_axis_Injected_m2F5466A10251C5F740DFBFA597FDBAEA02A2AE02 (Joint_t734F77C82E6372173403FCA61D9E7FC4618595AF * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___ret0, const RuntimeMethod* method)
{
	typedef void (*Joint_get_axis_Injected_m2F5466A10251C5F740DFBFA597FDBAEA02A2AE02_ftn) (Joint_t734F77C82E6372173403FCA61D9E7FC4618595AF *, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *);
	static Joint_get_axis_Injected_m2F5466A10251C5F740DFBFA597FDBAEA02A2AE02_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Joint_get_axis_Injected_m2F5466A10251C5F740DFBFA597FDBAEA02A2AE02_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Joint::get_axis_Injected(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___ret0);
}
// System.Void UnityEngine.Joint::set_axis_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Joint_set_axis_Injected_m7BFCEEDE9581D1B5F646E31F6AD3FA3990E3A9E4 (Joint_t734F77C82E6372173403FCA61D9E7FC4618595AF * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Joint_set_axis_Injected_m7BFCEEDE9581D1B5F646E31F6AD3FA3990E3A9E4_ftn) (Joint_t734F77C82E6372173403FCA61D9E7FC4618595AF *, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *);
	static Joint_set_axis_Injected_m7BFCEEDE9581D1B5F646E31F6AD3FA3990E3A9E4_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Joint_set_axis_Injected_m7BFCEEDE9581D1B5F646E31F6AD3FA3990E3A9E4_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Joint::set_axis_Injected(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Joint::get_anchor_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Joint_get_anchor_Injected_mD0B6F04D2157DDD14E48278FAADA44AC5AF0433B (Joint_t734F77C82E6372173403FCA61D9E7FC4618595AF * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___ret0, const RuntimeMethod* method)
{
	typedef void (*Joint_get_anchor_Injected_mD0B6F04D2157DDD14E48278FAADA44AC5AF0433B_ftn) (Joint_t734F77C82E6372173403FCA61D9E7FC4618595AF *, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *);
	static Joint_get_anchor_Injected_mD0B6F04D2157DDD14E48278FAADA44AC5AF0433B_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Joint_get_anchor_Injected_mD0B6F04D2157DDD14E48278FAADA44AC5AF0433B_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Joint::get_anchor_Injected(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___ret0);
}
// System.Void UnityEngine.Joint::set_anchor_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Joint_set_anchor_Injected_mCDEEE334517B97D137C1AE4ADAAFAD9CD85399EB (Joint_t734F77C82E6372173403FCA61D9E7FC4618595AF * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Joint_set_anchor_Injected_mCDEEE334517B97D137C1AE4ADAAFAD9CD85399EB_ftn) (Joint_t734F77C82E6372173403FCA61D9E7FC4618595AF *, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *);
	static Joint_set_anchor_Injected_mCDEEE334517B97D137C1AE4ADAAFAD9CD85399EB_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Joint_set_anchor_Injected_mCDEEE334517B97D137C1AE4ADAAFAD9CD85399EB_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Joint::set_anchor_Injected(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Joint::get_connectedAnchor_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Joint_get_connectedAnchor_Injected_m8DBF0F41D88D496175BE8F21660DE20653E3CC9C (Joint_t734F77C82E6372173403FCA61D9E7FC4618595AF * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___ret0, const RuntimeMethod* method)
{
	typedef void (*Joint_get_connectedAnchor_Injected_m8DBF0F41D88D496175BE8F21660DE20653E3CC9C_ftn) (Joint_t734F77C82E6372173403FCA61D9E7FC4618595AF *, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *);
	static Joint_get_connectedAnchor_Injected_m8DBF0F41D88D496175BE8F21660DE20653E3CC9C_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Joint_get_connectedAnchor_Injected_m8DBF0F41D88D496175BE8F21660DE20653E3CC9C_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Joint::get_connectedAnchor_Injected(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___ret0);
}
// System.Void UnityEngine.Joint::set_connectedAnchor_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Joint_set_connectedAnchor_Injected_m4CCEF375EFC641A7521AC470B6770282D17DEDED (Joint_t734F77C82E6372173403FCA61D9E7FC4618595AF * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Joint_set_connectedAnchor_Injected_m4CCEF375EFC641A7521AC470B6770282D17DEDED_ftn) (Joint_t734F77C82E6372173403FCA61D9E7FC4618595AF *, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *);
	static Joint_set_connectedAnchor_Injected_m4CCEF375EFC641A7521AC470B6770282D17DEDED_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Joint_set_connectedAnchor_Injected_m4CCEF375EFC641A7521AC470B6770282D17DEDED_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Joint::set_connectedAnchor_Injected(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.JointDrive::set_positionSpring(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void JointDrive_set_positionSpring_mB6FF4730ECEB6B03BACB391B34381EB34B70A9F8 (JointDrive_t2082D48A52244171DA92ED5FC110D2A58BCBB575 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_PositionSpring_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void JointDrive_set_positionSpring_mB6FF4730ECEB6B03BACB391B34381EB34B70A9F8_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	JointDrive_t2082D48A52244171DA92ED5FC110D2A58BCBB575 * _thisAdjusted = reinterpret_cast<JointDrive_t2082D48A52244171DA92ED5FC110D2A58BCBB575 *>(__this + _offset);
	JointDrive_set_positionSpring_mB6FF4730ECEB6B03BACB391B34381EB34B70A9F8(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.JointDrive::set_positionDamper(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void JointDrive_set_positionDamper_mA07AE9A9384511BC160C19EF4CCB75260621C2CF (JointDrive_t2082D48A52244171DA92ED5FC110D2A58BCBB575 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_PositionDamper_1(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void JointDrive_set_positionDamper_mA07AE9A9384511BC160C19EF4CCB75260621C2CF_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	JointDrive_t2082D48A52244171DA92ED5FC110D2A58BCBB575 * _thisAdjusted = reinterpret_cast<JointDrive_t2082D48A52244171DA92ED5FC110D2A58BCBB575 *>(__this + _offset);
	JointDrive_set_positionDamper_mA07AE9A9384511BC160C19EF4CCB75260621C2CF(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.JointDrive::set_maximumForce(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void JointDrive_set_maximumForce_m1D0AFF4DC2FB20093A1F29922550D88524CAE945 (JointDrive_t2082D48A52244171DA92ED5FC110D2A58BCBB575 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_MaximumForce_2(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void JointDrive_set_maximumForce_m1D0AFF4DC2FB20093A1F29922550D88524CAE945_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	JointDrive_t2082D48A52244171DA92ED5FC110D2A58BCBB575 * _thisAdjusted = reinterpret_cast<JointDrive_t2082D48A52244171DA92ED5FC110D2A58BCBB575 *>(__this + _offset);
	JointDrive_set_maximumForce_m1D0AFF4DC2FB20093A1F29922550D88524CAE945(_thisAdjusted, ___value0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Single UnityEngine.JointLimits::get_min()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float JointLimits_get_min_m7C30825582F94CECDD5DE097143F91B830103001 (JointLimits_t89D8D0E21FB1E840F8380AFB9FB55E966A3030F3 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Min_0();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		float L_1 = V_0;
		return L_1;
	}
}
IL2CPP_EXTERN_C  float JointLimits_get_min_m7C30825582F94CECDD5DE097143F91B830103001_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	JointLimits_t89D8D0E21FB1E840F8380AFB9FB55E966A3030F3 * _thisAdjusted = reinterpret_cast<JointLimits_t89D8D0E21FB1E840F8380AFB9FB55E966A3030F3 *>(__this + _offset);
	return JointLimits_get_min_m7C30825582F94CECDD5DE097143F91B830103001(_thisAdjusted, method);
}
// System.Single UnityEngine.JointLimits::get_max()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float JointLimits_get_max_mFC3FC1E95BCBEA96DDC48D9B94BC8F3AE587CD2F (JointLimits_t89D8D0E21FB1E840F8380AFB9FB55E966A3030F3 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Max_1();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		float L_1 = V_0;
		return L_1;
	}
}
IL2CPP_EXTERN_C  float JointLimits_get_max_mFC3FC1E95BCBEA96DDC48D9B94BC8F3AE587CD2F_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	JointLimits_t89D8D0E21FB1E840F8380AFB9FB55E966A3030F3 * _thisAdjusted = reinterpret_cast<JointLimits_t89D8D0E21FB1E840F8380AFB9FB55E966A3030F3 *>(__this + _offset);
	return JointLimits_get_max_mFC3FC1E95BCBEA96DDC48D9B94BC8F3AE587CD2F(_thisAdjusted, method);
}
// System.Single UnityEngine.JointLimits::get_bounciness()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float JointLimits_get_bounciness_m29ABD9EAE433CAA892D823A26EE8C9423E4CDBB1 (JointLimits_t89D8D0E21FB1E840F8380AFB9FB55E966A3030F3 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Bounciness_2();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		float L_1 = V_0;
		return L_1;
	}
}
IL2CPP_EXTERN_C  float JointLimits_get_bounciness_m29ABD9EAE433CAA892D823A26EE8C9423E4CDBB1_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	JointLimits_t89D8D0E21FB1E840F8380AFB9FB55E966A3030F3 * _thisAdjusted = reinterpret_cast<JointLimits_t89D8D0E21FB1E840F8380AFB9FB55E966A3030F3 *>(__this + _offset);
	return JointLimits_get_bounciness_m29ABD9EAE433CAA892D823A26EE8C9423E4CDBB1(_thisAdjusted, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.MeshCollider::set_sharedMesh(UnityEngine.Mesh)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MeshCollider_set_sharedMesh_m88BE136B396DA960BC78F1A563BE0A810AA0FBBA (MeshCollider_t60EB55ADE92499FE8D1AA206D2BD96E65B2766DE * __this, Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___value0, const RuntimeMethod* method)
{
	typedef void (*MeshCollider_set_sharedMesh_m88BE136B396DA960BC78F1A563BE0A810AA0FBBA_ftn) (MeshCollider_t60EB55ADE92499FE8D1AA206D2BD96E65B2766DE *, Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C *);
	static MeshCollider_set_sharedMesh_m88BE136B396DA960BC78F1A563BE0A810AA0FBBA_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MeshCollider_set_sharedMesh_m88BE136B396DA960BC78F1A563BE0A810AA0FBBA_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MeshCollider::set_sharedMesh(UnityEngine.Mesh)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.MeshCollider::set_convex(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MeshCollider_set_convex_m9437496D05ABFBE619F8B2C6E80D6AECB95EAFE0 (MeshCollider_t60EB55ADE92499FE8D1AA206D2BD96E65B2766DE * __this, bool ___value0, const RuntimeMethod* method)
{
	typedef void (*MeshCollider_set_convex_m9437496D05ABFBE619F8B2C6E80D6AECB95EAFE0_ftn) (MeshCollider_t60EB55ADE92499FE8D1AA206D2BD96E65B2766DE *, bool);
	static MeshCollider_set_convex_m9437496D05ABFBE619F8B2C6E80D6AECB95EAFE0_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MeshCollider_set_convex_m9437496D05ABFBE619F8B2C6E80D6AECB95EAFE0_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MeshCollider::set_convex(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.PhysicMaterial::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhysicMaterial__ctor_m1E6AB8A9A487E70DE44AF221946176BE96584DFC (PhysicMaterial_tBEBB6F4620A5221A4CBAEDB2E5984CCA70AA07F8 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhysicMaterial__ctor_m1E6AB8A9A487E70DE44AF221946176BE96584DFC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object__ctor_m091EBAEBC7919B0391ABDAFB7389ADC12206525B(__this, /*hidden argument*/NULL);
		PhysicMaterial_Internal_CreateDynamicsMaterial_m477978C8F91BAC9E41E0BE575C0C0730C80AFE1C(__this, _stringLiteralC76B3E2116774CE05868F05B32DDBF1C6F86EAB8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.PhysicMaterial::Internal_CreateDynamicsMaterial(UnityEngine.PhysicMaterial,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhysicMaterial_Internal_CreateDynamicsMaterial_m477978C8F91BAC9E41E0BE575C0C0730C80AFE1C (PhysicMaterial_tBEBB6F4620A5221A4CBAEDB2E5984CCA70AA07F8 * ___mat0, String_t* ___name1, const RuntimeMethod* method)
{
	typedef void (*PhysicMaterial_Internal_CreateDynamicsMaterial_m477978C8F91BAC9E41E0BE575C0C0730C80AFE1C_ftn) (PhysicMaterial_tBEBB6F4620A5221A4CBAEDB2E5984CCA70AA07F8 *, String_t*);
	static PhysicMaterial_Internal_CreateDynamicsMaterial_m477978C8F91BAC9E41E0BE575C0C0730C80AFE1C_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PhysicMaterial_Internal_CreateDynamicsMaterial_m477978C8F91BAC9E41E0BE575C0C0730C80AFE1C_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PhysicMaterial::Internal_CreateDynamicsMaterial(UnityEngine.PhysicMaterial,System.String)");
	_il2cpp_icall_func(___mat0, ___name1);
}
// System.Void UnityEngine.PhysicMaterial::set_bounciness(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhysicMaterial_set_bounciness_m452DFF64A89EC0F7C1BE978BB99A6FCCEBFCC35B (PhysicMaterial_tBEBB6F4620A5221A4CBAEDB2E5984CCA70AA07F8 * __this, float ___value0, const RuntimeMethod* method)
{
	typedef void (*PhysicMaterial_set_bounciness_m452DFF64A89EC0F7C1BE978BB99A6FCCEBFCC35B_ftn) (PhysicMaterial_tBEBB6F4620A5221A4CBAEDB2E5984CCA70AA07F8 *, float);
	static PhysicMaterial_set_bounciness_m452DFF64A89EC0F7C1BE978BB99A6FCCEBFCC35B_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PhysicMaterial_set_bounciness_m452DFF64A89EC0F7C1BE978BB99A6FCCEBFCC35B_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PhysicMaterial::set_bounciness(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.PhysicMaterial::set_dynamicFriction(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhysicMaterial_set_dynamicFriction_m9FCE6D1715B7385FE3FB427C13C77643BECF635E (PhysicMaterial_tBEBB6F4620A5221A4CBAEDB2E5984CCA70AA07F8 * __this, float ___value0, const RuntimeMethod* method)
{
	typedef void (*PhysicMaterial_set_dynamicFriction_m9FCE6D1715B7385FE3FB427C13C77643BECF635E_ftn) (PhysicMaterial_tBEBB6F4620A5221A4CBAEDB2E5984CCA70AA07F8 *, float);
	static PhysicMaterial_set_dynamicFriction_m9FCE6D1715B7385FE3FB427C13C77643BECF635E_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PhysicMaterial_set_dynamicFriction_m9FCE6D1715B7385FE3FB427C13C77643BECF635E_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PhysicMaterial::set_dynamicFriction(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.PhysicMaterial::set_staticFriction(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhysicMaterial_set_staticFriction_m6DC930416F9C22EBCA4661D58F3E369B4E779D3A (PhysicMaterial_tBEBB6F4620A5221A4CBAEDB2E5984CCA70AA07F8 * __this, float ___value0, const RuntimeMethod* method)
{
	typedef void (*PhysicMaterial_set_staticFriction_m6DC930416F9C22EBCA4661D58F3E369B4E779D3A_ftn) (PhysicMaterial_tBEBB6F4620A5221A4CBAEDB2E5984CCA70AA07F8 *, float);
	static PhysicMaterial_set_staticFriction_m6DC930416F9C22EBCA4661D58F3E369B4E779D3A_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PhysicMaterial_set_staticFriction_m6DC930416F9C22EBCA4661D58F3E369B4E779D3A_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PhysicMaterial::set_staticFriction(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.PhysicMaterial::set_frictionCombine(UnityEngine.PhysicMaterialCombine)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhysicMaterial_set_frictionCombine_mB05C3A5D9A430E1BECADC0530C67C604E2F11696 (PhysicMaterial_tBEBB6F4620A5221A4CBAEDB2E5984CCA70AA07F8 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*PhysicMaterial_set_frictionCombine_mB05C3A5D9A430E1BECADC0530C67C604E2F11696_ftn) (PhysicMaterial_tBEBB6F4620A5221A4CBAEDB2E5984CCA70AA07F8 *, int32_t);
	static PhysicMaterial_set_frictionCombine_mB05C3A5D9A430E1BECADC0530C67C604E2F11696_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PhysicMaterial_set_frictionCombine_mB05C3A5D9A430E1BECADC0530C67C604E2F11696_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PhysicMaterial::set_frictionCombine(UnityEngine.PhysicMaterialCombine)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.PhysicMaterial::set_bounceCombine(UnityEngine.PhysicMaterialCombine)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhysicMaterial_set_bounceCombine_mE4B35BC7940DF0CC1C4D160F4676768914DE032F (PhysicMaterial_tBEBB6F4620A5221A4CBAEDB2E5984CCA70AA07F8 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*PhysicMaterial_set_bounceCombine_mE4B35BC7940DF0CC1C4D160F4676768914DE032F_ftn) (PhysicMaterial_tBEBB6F4620A5221A4CBAEDB2E5984CCA70AA07F8 *, int32_t);
	static PhysicMaterial_set_bounceCombine_mE4B35BC7940DF0CC1C4D160F4676768914DE032F_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PhysicMaterial_set_bounceCombine_mE4B35BC7940DF0CC1C4D160F4676768914DE032F_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PhysicMaterial::set_bounceCombine(UnityEngine.PhysicMaterialCombine)");
	_il2cpp_icall_func(__this, ___value0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Vector3 UnityEngine.Physics::get_gravity()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Physics_get_gravity_mC0F44371C49C1DD1455D6ADEC824BAE35E1E104B (const RuntimeMethod* method)
{
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Physics_get_gravity_Injected_m7B5CE9CF65B245C2C2EA078BE636E393225B0335((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_0), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = V_0;
		return L_0;
	}
}
// UnityEngine.PhysicsScene UnityEngine.Physics::get_defaultPhysicsScene()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  Physics_get_defaultPhysicsScene_mEA96CE906577479223C956BFF0B8D0B79BB47B30 (const RuntimeMethod* method)
{
	PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Physics_get_defaultPhysicsScene_Injected_mEAB07CA5307D0DDB331A31E6A6BA28CF83009262((PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 *)(&V_0), /*hidden argument*/NULL);
		PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Physics::IgnoreCollision(UnityEngine.Collider,UnityEngine.Collider,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Physics_IgnoreCollision_mF7183C0761289A45400F0E2C1B223B980EBE5F8B (Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * ___collider10, Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * ___collider21, bool ___ignore2, const RuntimeMethod* method)
{
	typedef void (*Physics_IgnoreCollision_mF7183C0761289A45400F0E2C1B223B980EBE5F8B_ftn) (Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF *, Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF *, bool);
	static Physics_IgnoreCollision_mF7183C0761289A45400F0E2C1B223B980EBE5F8B_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics_IgnoreCollision_mF7183C0761289A45400F0E2C1B223B980EBE5F8B_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::IgnoreCollision(UnityEngine.Collider,UnityEngine.Collider,System.Boolean)");
	_il2cpp_icall_func(___collider10, ___collider21, ___ignore2);
}
// System.Void UnityEngine.Physics::IgnoreLayerCollision(System.Int32,System.Int32,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Physics_IgnoreLayerCollision_m0DFDA0BBDFA1F0737524272504E3D85989989C32 (int32_t ___layer10, int32_t ___layer21, bool ___ignore2, const RuntimeMethod* method)
{
	typedef void (*Physics_IgnoreLayerCollision_m0DFDA0BBDFA1F0737524272504E3D85989989C32_ftn) (int32_t, int32_t, bool);
	static Physics_IgnoreLayerCollision_m0DFDA0BBDFA1F0737524272504E3D85989989C32_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics_IgnoreLayerCollision_m0DFDA0BBDFA1F0737524272504E3D85989989C32_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::IgnoreLayerCollision(System.Int32,System.Int32,System.Boolean)");
	_il2cpp_icall_func(___layer10, ___layer21, ___ignore2);
}
// System.Void UnityEngine.Physics::IgnoreLayerCollision(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Physics_IgnoreLayerCollision_m21F8A3A611C8DB8CA7D80C3D7E9DB836126C0C2B (int32_t ___layer10, int32_t ___layer21, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___layer10;
		int32_t L_1 = ___layer21;
		Physics_IgnoreLayerCollision_m0DFDA0BBDFA1F0737524272504E3D85989989C32(L_0, L_1, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Physics::GetIgnoreLayerCollision(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Physics_GetIgnoreLayerCollision_mD79111559F93397C8516436A7456086FB5021961 (int32_t ___layer10, int32_t ___layer21, const RuntimeMethod* method)
{
	typedef bool (*Physics_GetIgnoreLayerCollision_mD79111559F93397C8516436A7456086FB5021961_ftn) (int32_t, int32_t);
	static Physics_GetIgnoreLayerCollision_mD79111559F93397C8516436A7456086FB5021961_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics_GetIgnoreLayerCollision_mD79111559F93397C8516436A7456086FB5021961_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::GetIgnoreLayerCollision(System.Int32,System.Int32)");
	bool retVal = _il2cpp_icall_func(___layer10, ___layer21);
	return retVal;
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Physics_Raycast_m511ECD9EF3223645EF104DDA50BB81C07F29717C (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___origin0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___direction1, float ___maxDistance2, int32_t ___layerMask3, int32_t ___queryTriggerInteraction4, const RuntimeMethod* method)
{
	PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  V_0;
	memset((&V_0), 0, sizeof(V_0));
	bool V_1 = false;
	{
		PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  L_0 = Physics_get_defaultPhysicsScene_mEA96CE906577479223C956BFF0B8D0B79BB47B30(/*hidden argument*/NULL);
		V_0 = L_0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = ___origin0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = ___direction1;
		float L_3 = ___maxDistance2;
		int32_t L_4 = ___layerMask3;
		int32_t L_5 = ___queryTriggerInteraction4;
		bool L_6 = PhysicsScene_Raycast_mA90972A8E828722A26EBC754EABB3086BE6E2336((PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 *)(&V_0), L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		goto IL_0017;
	}

IL_0017:
	{
		bool L_7 = V_1;
		return L_7;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Physics_Raycast_mC87F52EDC44BBBAFFED0D9DC92B37B11C6E07BEA (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___origin0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___direction1, float ___maxDistance2, int32_t ___layerMask3, const RuntimeMethod* method)
{
	PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  V_0;
	memset((&V_0), 0, sizeof(V_0));
	bool V_1 = false;
	{
		PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  L_0 = Physics_get_defaultPhysicsScene_mEA96CE906577479223C956BFF0B8D0B79BB47B30(/*hidden argument*/NULL);
		V_0 = L_0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = ___origin0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = ___direction1;
		float L_3 = ___maxDistance2;
		int32_t L_4 = ___layerMask3;
		bool L_5 = PhysicsScene_Raycast_mA90972A8E828722A26EBC754EABB3086BE6E2336((PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 *)(&V_0), L_1, L_2, L_3, L_4, 0, /*hidden argument*/NULL);
		V_1 = L_5;
		goto IL_0016;
	}

IL_0016:
	{
		bool L_6 = V_1;
		return L_6;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Physics_Raycast_m0583CCAA9E2F3BD031F12FA080837E9A48EEC16D (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___origin0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___direction1, float ___maxDistance2, const RuntimeMethod* method)
{
	PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  V_0;
	memset((&V_0), 0, sizeof(V_0));
	bool V_1 = false;
	{
		PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  L_0 = Physics_get_defaultPhysicsScene_mEA96CE906577479223C956BFF0B8D0B79BB47B30(/*hidden argument*/NULL);
		V_0 = L_0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = ___origin0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = ___direction1;
		float L_3 = ___maxDistance2;
		bool L_4 = PhysicsScene_Raycast_mA90972A8E828722A26EBC754EABB3086BE6E2336((PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 *)(&V_0), L_1, L_2, L_3, ((int32_t)-5), 0, /*hidden argument*/NULL);
		V_1 = L_4;
		goto IL_0017;
	}

IL_0017:
	{
		bool L_5 = V_1;
		return L_5;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Physics_Raycast_m6FFBDC7166A2129B70027FC392D06C735F81C73E (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___origin0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___direction1, const RuntimeMethod* method)
{
	PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  V_0;
	memset((&V_0), 0, sizeof(V_0));
	bool V_1 = false;
	{
		PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  L_0 = Physics_get_defaultPhysicsScene_mEA96CE906577479223C956BFF0B8D0B79BB47B30(/*hidden argument*/NULL);
		V_0 = L_0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = ___origin0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = ___direction1;
		bool L_3 = PhysicsScene_Raycast_mA90972A8E828722A26EBC754EABB3086BE6E2336((PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 *)(&V_0), L_1, L_2, (std::numeric_limits<float>::infinity()), ((int32_t)-5), 0, /*hidden argument*/NULL);
		V_1 = L_3;
		goto IL_001b;
	}

IL_001b:
	{
		bool L_4 = V_1;
		return L_4;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Physics_Raycast_m44270C2D7C8CD056B6D78DE60A4C91FCC0A225C1 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___origin0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___direction1, RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * ___hitInfo2, float ___maxDistance3, int32_t ___layerMask4, int32_t ___queryTriggerInteraction5, const RuntimeMethod* method)
{
	PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  V_0;
	memset((&V_0), 0, sizeof(V_0));
	bool V_1 = false;
	{
		PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  L_0 = Physics_get_defaultPhysicsScene_mEA96CE906577479223C956BFF0B8D0B79BB47B30(/*hidden argument*/NULL);
		V_0 = L_0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = ___origin0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = ___direction1;
		RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * L_3 = ___hitInfo2;
		float L_4 = ___maxDistance3;
		int32_t L_5 = ___layerMask4;
		int32_t L_6 = ___queryTriggerInteraction5;
		bool L_7 = PhysicsScene_Raycast_m90D1AE1F641D9344BEBEF32E7AC0BAC883024C0B((PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 *)(&V_0), L_1, L_2, (RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 *)L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		goto IL_0019;
	}

IL_0019:
	{
		bool L_8 = V_1;
		return L_8;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Physics_Raycast_mFB0018A196CC9E45CA8A238899EA8B093264B024 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___origin0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___direction1, RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * ___hitInfo2, float ___maxDistance3, int32_t ___layerMask4, const RuntimeMethod* method)
{
	PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  V_0;
	memset((&V_0), 0, sizeof(V_0));
	bool V_1 = false;
	{
		PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  L_0 = Physics_get_defaultPhysicsScene_mEA96CE906577479223C956BFF0B8D0B79BB47B30(/*hidden argument*/NULL);
		V_0 = L_0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = ___origin0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = ___direction1;
		RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * L_3 = ___hitInfo2;
		float L_4 = ___maxDistance3;
		int32_t L_5 = ___layerMask4;
		bool L_6 = PhysicsScene_Raycast_m90D1AE1F641D9344BEBEF32E7AC0BAC883024C0B((PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 *)(&V_0), L_1, L_2, (RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 *)L_3, L_4, L_5, 0, /*hidden argument*/NULL);
		V_1 = L_6;
		goto IL_0018;
	}

IL_0018:
	{
		bool L_7 = V_1;
		return L_7;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Physics_Raycast_mBEC747ED0A7660BB12AA48B663CCBF7B1EE93D6B (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___origin0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___direction1, RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * ___hitInfo2, float ___maxDistance3, const RuntimeMethod* method)
{
	PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  V_0;
	memset((&V_0), 0, sizeof(V_0));
	bool V_1 = false;
	{
		PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  L_0 = Physics_get_defaultPhysicsScene_mEA96CE906577479223C956BFF0B8D0B79BB47B30(/*hidden argument*/NULL);
		V_0 = L_0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = ___origin0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = ___direction1;
		RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * L_3 = ___hitInfo2;
		float L_4 = ___maxDistance3;
		bool L_5 = PhysicsScene_Raycast_m90D1AE1F641D9344BEBEF32E7AC0BAC883024C0B((PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 *)(&V_0), L_1, L_2, (RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 *)L_3, L_4, ((int32_t)-5), 0, /*hidden argument*/NULL);
		V_1 = L_5;
		goto IL_0018;
	}

IL_0018:
	{
		bool L_6 = V_1;
		return L_6;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Physics_Raycast_m000FC36D038952F8AC5E3E562E7D39BAA8D1E287 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___origin0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___direction1, RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * ___hitInfo2, const RuntimeMethod* method)
{
	PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  V_0;
	memset((&V_0), 0, sizeof(V_0));
	bool V_1 = false;
	{
		PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  L_0 = Physics_get_defaultPhysicsScene_mEA96CE906577479223C956BFF0B8D0B79BB47B30(/*hidden argument*/NULL);
		V_0 = L_0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = ___origin0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = ___direction1;
		RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * L_3 = ___hitInfo2;
		bool L_4 = PhysicsScene_Raycast_m90D1AE1F641D9344BEBEF32E7AC0BAC883024C0B((PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 *)(&V_0), L_1, L_2, (RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 *)L_3, (std::numeric_limits<float>::infinity()), ((int32_t)-5), 0, /*hidden argument*/NULL);
		V_1 = L_4;
		goto IL_001c;
	}

IL_001c:
	{
		bool L_5 = V_1;
		return L_5;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Physics_Raycast_mF33E3BA9FDFE0D19B193D2AB05028B2086738D47 (Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  ___ray0, float ___maxDistance1, int32_t ___layerMask2, int32_t ___queryTriggerInteraction3, const RuntimeMethod* method)
{
	PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  V_0;
	memset((&V_0), 0, sizeof(V_0));
	bool V_1 = false;
	{
		PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  L_0 = Physics_get_defaultPhysicsScene_mEA96CE906577479223C956BFF0B8D0B79BB47B30(/*hidden argument*/NULL);
		V_0 = L_0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = Ray_get_origin_m3773CA7B1E2F26F6F1447652B485D86C0BEC5187((Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *)(&___ray0), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = Ray_get_direction_m9E6468CD87844B437FC4B93491E63D388322F76E((Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *)(&___ray0), /*hidden argument*/NULL);
		float L_3 = ___maxDistance1;
		int32_t L_4 = ___layerMask2;
		int32_t L_5 = ___queryTriggerInteraction3;
		bool L_6 = PhysicsScene_Raycast_mA90972A8E828722A26EBC754EABB3086BE6E2336((PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 *)(&V_0), L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		goto IL_0022;
	}

IL_0022:
	{
		bool L_7 = V_1;
		return L_7;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,System.Single,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Physics_Raycast_m5BA453EA32F5B660CD9A537FED97B995971820E1 (Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  ___ray0, float ___maxDistance1, int32_t ___layerMask2, const RuntimeMethod* method)
{
	PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  V_0;
	memset((&V_0), 0, sizeof(V_0));
	bool V_1 = false;
	{
		PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  L_0 = Physics_get_defaultPhysicsScene_mEA96CE906577479223C956BFF0B8D0B79BB47B30(/*hidden argument*/NULL);
		V_0 = L_0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = Ray_get_origin_m3773CA7B1E2F26F6F1447652B485D86C0BEC5187((Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *)(&___ray0), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = Ray_get_direction_m9E6468CD87844B437FC4B93491E63D388322F76E((Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *)(&___ray0), /*hidden argument*/NULL);
		float L_3 = ___maxDistance1;
		int32_t L_4 = ___layerMask2;
		bool L_5 = PhysicsScene_Raycast_mA90972A8E828722A26EBC754EABB3086BE6E2336((PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 *)(&V_0), L_1, L_2, L_3, L_4, 0, /*hidden argument*/NULL);
		V_1 = L_5;
		goto IL_0022;
	}

IL_0022:
	{
		bool L_6 = V_1;
		return L_6;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Physics_Raycast_m198FD20B3D1BC1634AE30E80E0FD1491D55417E5 (Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  ___ray0, float ___maxDistance1, const RuntimeMethod* method)
{
	PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  V_0;
	memset((&V_0), 0, sizeof(V_0));
	bool V_1 = false;
	{
		PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  L_0 = Physics_get_defaultPhysicsScene_mEA96CE906577479223C956BFF0B8D0B79BB47B30(/*hidden argument*/NULL);
		V_0 = L_0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = Ray_get_origin_m3773CA7B1E2F26F6F1447652B485D86C0BEC5187((Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *)(&___ray0), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = Ray_get_direction_m9E6468CD87844B437FC4B93491E63D388322F76E((Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *)(&___ray0), /*hidden argument*/NULL);
		float L_3 = ___maxDistance1;
		bool L_4 = PhysicsScene_Raycast_mA90972A8E828722A26EBC754EABB3086BE6E2336((PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 *)(&V_0), L_1, L_2, L_3, ((int32_t)-5), 0, /*hidden argument*/NULL);
		V_1 = L_4;
		goto IL_0023;
	}

IL_0023:
	{
		bool L_5 = V_1;
		return L_5;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Physics_Raycast_mF1112EC53F3393455B4ABA0E0092A58E3CD0114A (Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  ___ray0, const RuntimeMethod* method)
{
	PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  V_0;
	memset((&V_0), 0, sizeof(V_0));
	bool V_1 = false;
	{
		PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  L_0 = Physics_get_defaultPhysicsScene_mEA96CE906577479223C956BFF0B8D0B79BB47B30(/*hidden argument*/NULL);
		V_0 = L_0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = Ray_get_origin_m3773CA7B1E2F26F6F1447652B485D86C0BEC5187((Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *)(&___ray0), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = Ray_get_direction_m9E6468CD87844B437FC4B93491E63D388322F76E((Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *)(&___ray0), /*hidden argument*/NULL);
		bool L_3 = PhysicsScene_Raycast_mA90972A8E828722A26EBC754EABB3086BE6E2336((PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 *)(&V_0), L_1, L_2, (std::numeric_limits<float>::infinity()), ((int32_t)-5), 0, /*hidden argument*/NULL);
		V_1 = L_3;
		goto IL_0027;
	}

IL_0027:
	{
		bool L_4 = V_1;
		return L_4;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Physics_Raycast_m6B6C7CB6DDED17F9B1905C606035A56FF9AE88F8 (Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  ___ray0, RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * ___hitInfo1, float ___maxDistance2, int32_t ___layerMask3, int32_t ___queryTriggerInteraction4, const RuntimeMethod* method)
{
	PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  V_0;
	memset((&V_0), 0, sizeof(V_0));
	bool V_1 = false;
	{
		PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  L_0 = Physics_get_defaultPhysicsScene_mEA96CE906577479223C956BFF0B8D0B79BB47B30(/*hidden argument*/NULL);
		V_0 = L_0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = Ray_get_origin_m3773CA7B1E2F26F6F1447652B485D86C0BEC5187((Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *)(&___ray0), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = Ray_get_direction_m9E6468CD87844B437FC4B93491E63D388322F76E((Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *)(&___ray0), /*hidden argument*/NULL);
		RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * L_3 = ___hitInfo1;
		float L_4 = ___maxDistance2;
		int32_t L_5 = ___layerMask3;
		int32_t L_6 = ___queryTriggerInteraction4;
		bool L_7 = PhysicsScene_Raycast_m90D1AE1F641D9344BEBEF32E7AC0BAC883024C0B((PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 *)(&V_0), L_1, L_2, (RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 *)L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		goto IL_0024;
	}

IL_0024:
	{
		bool L_8 = V_1;
		return L_8;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Physics_Raycast_m9F5CAD8DA7923E897C5DCAF913BD411AD1A87950 (Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  ___ray0, RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * ___hitInfo1, float ___maxDistance2, int32_t ___layerMask3, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = Ray_get_origin_m3773CA7B1E2F26F6F1447652B485D86C0BEC5187((Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *)(&___ray0), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = Ray_get_direction_m9E6468CD87844B437FC4B93491E63D388322F76E((Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *)(&___ray0), /*hidden argument*/NULL);
		RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * L_2 = ___hitInfo1;
		float L_3 = ___maxDistance2;
		int32_t L_4 = ___layerMask3;
		bool L_5 = Physics_Raycast_m44270C2D7C8CD056B6D78DE60A4C91FCC0A225C1(L_0, L_1, (RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 *)L_2, L_3, L_4, 0, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_001b;
	}

IL_001b:
	{
		bool L_6 = V_0;
		return L_6;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Physics_Raycast_mE1590EE4E2DC950A9FC2437E98EE8CD2EC2DEE67 (Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  ___ray0, RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * ___hitInfo1, float ___maxDistance2, const RuntimeMethod* method)
{
	PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  V_0;
	memset((&V_0), 0, sizeof(V_0));
	bool V_1 = false;
	{
		PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  L_0 = Physics_get_defaultPhysicsScene_mEA96CE906577479223C956BFF0B8D0B79BB47B30(/*hidden argument*/NULL);
		V_0 = L_0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = Ray_get_origin_m3773CA7B1E2F26F6F1447652B485D86C0BEC5187((Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *)(&___ray0), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = Ray_get_direction_m9E6468CD87844B437FC4B93491E63D388322F76E((Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *)(&___ray0), /*hidden argument*/NULL);
		RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * L_3 = ___hitInfo1;
		float L_4 = ___maxDistance2;
		bool L_5 = PhysicsScene_Raycast_m90D1AE1F641D9344BEBEF32E7AC0BAC883024C0B((PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 *)(&V_0), L_1, L_2, (RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 *)L_3, L_4, ((int32_t)-5), 0, /*hidden argument*/NULL);
		V_1 = L_5;
		goto IL_0024;
	}

IL_0024:
	{
		bool L_6 = V_1;
		return L_6;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Physics_Raycast_mCE618579F667A62D15CB74CEF76147136EB2D495 (Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  ___ray0, RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * ___hitInfo1, const RuntimeMethod* method)
{
	PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  V_0;
	memset((&V_0), 0, sizeof(V_0));
	bool V_1 = false;
	{
		PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  L_0 = Physics_get_defaultPhysicsScene_mEA96CE906577479223C956BFF0B8D0B79BB47B30(/*hidden argument*/NULL);
		V_0 = L_0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = Ray_get_origin_m3773CA7B1E2F26F6F1447652B485D86C0BEC5187((Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *)(&___ray0), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = Ray_get_direction_m9E6468CD87844B437FC4B93491E63D388322F76E((Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *)(&___ray0), /*hidden argument*/NULL);
		RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * L_3 = ___hitInfo1;
		bool L_4 = PhysicsScene_Raycast_m90D1AE1F641D9344BEBEF32E7AC0BAC883024C0B((PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 *)(&V_0), L_1, L_2, (RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 *)L_3, (std::numeric_limits<float>::infinity()), ((int32_t)-5), 0, /*hidden argument*/NULL);
		V_1 = L_4;
		goto IL_0028;
	}

IL_0028:
	{
		bool L_5 = V_1;
		return L_5;
	}
}
// System.Boolean UnityEngine.Physics::Linecast(UnityEngine.Vector3,UnityEngine.Vector3,System.Int32,UnityEngine.QueryTriggerInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Physics_Linecast_m2CAAB76F9786AB83FCF6B9C751B6A754AAEA48FB (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___start0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___end1, int32_t ___layerMask2, int32_t ___queryTriggerInteraction3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics_Linecast_m2CAAB76F9786AB83FCF6B9C751B6A754AAEA48FB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset((&V_0), 0, sizeof(V_0));
	PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  V_1;
	memset((&V_1), 0, sizeof(V_1));
	bool V_2 = false;
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = ___end1;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = ___start0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = Vector3_op_Subtraction_mF9846B723A5034F8B9F5F5DCB78E3D67649143D3(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  L_3 = Physics_get_defaultPhysicsScene_mEA96CE906577479223C956BFF0B8D0B79BB47B30(/*hidden argument*/NULL);
		V_1 = L_3;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_4 = ___start0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_5 = V_0;
		float L_6 = Vector3_get_magnitude_m9A750659B60C5FE0C30438A7F9681775D5DB1274((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_0), /*hidden argument*/NULL);
		int32_t L_7 = ___layerMask2;
		int32_t L_8 = ___queryTriggerInteraction3;
		bool L_9 = PhysicsScene_Raycast_mA90972A8E828722A26EBC754EABB3086BE6E2336((PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 *)(&V_1), L_4, L_5, L_6, L_7, L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		goto IL_0024;
	}

IL_0024:
	{
		bool L_10 = V_2;
		return L_10;
	}
}
// System.Boolean UnityEngine.Physics::Linecast(UnityEngine.Vector3,UnityEngine.Vector3,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Physics_Linecast_m1286F99BB2A490E80EA48A6DF456C81293A35C66 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___start0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___end1, int32_t ___layerMask2, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = ___start0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = ___end1;
		int32_t L_2 = ___layerMask2;
		bool L_3 = Physics_Linecast_m2CAAB76F9786AB83FCF6B9C751B6A754AAEA48FB(L_0, L_1, L_2, 0, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_000d;
	}

IL_000d:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
// System.Boolean UnityEngine.Physics::Linecast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Int32,UnityEngine.QueryTriggerInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Physics_Linecast_mCAA3A0F713478346F006ADF57D65F64C89A544C6 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___start0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___end1, RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * ___hitInfo2, int32_t ___layerMask3, int32_t ___queryTriggerInteraction4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics_Linecast_mCAA3A0F713478346F006ADF57D65F64C89A544C6_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset((&V_0), 0, sizeof(V_0));
	PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  V_1;
	memset((&V_1), 0, sizeof(V_1));
	bool V_2 = false;
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = ___end1;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = ___start0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = Vector3_op_Subtraction_mF9846B723A5034F8B9F5F5DCB78E3D67649143D3(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  L_3 = Physics_get_defaultPhysicsScene_mEA96CE906577479223C956BFF0B8D0B79BB47B30(/*hidden argument*/NULL);
		V_1 = L_3;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_4 = ___start0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_5 = V_0;
		RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * L_6 = ___hitInfo2;
		float L_7 = Vector3_get_magnitude_m9A750659B60C5FE0C30438A7F9681775D5DB1274((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_0), /*hidden argument*/NULL);
		int32_t L_8 = ___layerMask3;
		int32_t L_9 = ___queryTriggerInteraction4;
		bool L_10 = PhysicsScene_Raycast_m90D1AE1F641D9344BEBEF32E7AC0BAC883024C0B((PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 *)(&V_1), L_4, L_5, (RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 *)L_6, L_7, L_8, L_9, /*hidden argument*/NULL);
		V_2 = L_10;
		goto IL_0026;
	}

IL_0026:
	{
		bool L_11 = V_2;
		return L_11;
	}
}
// System.Boolean UnityEngine.Physics::Linecast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Physics_Linecast_mBB78AB8D049EB835949B2940D511E1049F6D5758 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___start0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___end1, RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * ___hitInfo2, int32_t ___layerMask3, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = ___start0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = ___end1;
		RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * L_2 = ___hitInfo2;
		int32_t L_3 = ___layerMask3;
		bool L_4 = Physics_Linecast_mCAA3A0F713478346F006ADF57D65F64C89A544C6(L_0, L_1, (RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 *)L_2, L_3, 0, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_000e;
	}

IL_000e:
	{
		bool L_5 = V_0;
		return L_5;
	}
}
// System.Boolean UnityEngine.Physics::Linecast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Physics_Linecast_mB0BFB97C5093E1D1C08B5C3CDE1062D46CEB2318 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___start0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___end1, RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * ___hitInfo2, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = ___start0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = ___end1;
		RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * L_2 = ___hitInfo2;
		bool L_3 = Physics_Linecast_mCAA3A0F713478346F006ADF57D65F64C89A544C6(L_0, L_1, (RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 *)L_2, ((int32_t)-5), 0, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_000f;
	}

IL_000f:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
// System.Boolean UnityEngine.Physics::SphereCast(UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Physics_SphereCast_m2D57A1F4DA048B95DC33939367F2556CEA22B8BD (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___origin0, float ___radius1, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___direction2, RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * ___hitInfo3, float ___maxDistance4, int32_t ___layerMask5, int32_t ___queryTriggerInteraction6, const RuntimeMethod* method)
{
	PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  V_0;
	memset((&V_0), 0, sizeof(V_0));
	bool V_1 = false;
	{
		PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  L_0 = Physics_get_defaultPhysicsScene_mEA96CE906577479223C956BFF0B8D0B79BB47B30(/*hidden argument*/NULL);
		V_0 = L_0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = ___origin0;
		float L_2 = ___radius1;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_3 = ___direction2;
		RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * L_4 = ___hitInfo3;
		float L_5 = ___maxDistance4;
		int32_t L_6 = ___layerMask5;
		int32_t L_7 = ___queryTriggerInteraction6;
		bool L_8 = PhysicsScene_SphereCast_m99D2D1C789BB817406E7C7FBAA75F2019A13D6C1((PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 *)(&V_0), L_1, L_2, L_3, (RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 *)L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		goto IL_001b;
	}

IL_001b:
	{
		bool L_9 = V_1;
		return L_9;
	}
}
// System.Boolean UnityEngine.Physics::SphereCast(UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Physics_SphereCast_m41B360D0B25BAADAD64F5D4BEAC07F92AF911837 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___origin0, float ___radius1, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___direction2, RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * ___hitInfo3, float ___maxDistance4, int32_t ___layerMask5, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = ___origin0;
		float L_1 = ___radius1;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = ___direction2;
		RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * L_3 = ___hitInfo3;
		float L_4 = ___maxDistance4;
		int32_t L_5 = ___layerMask5;
		bool L_6 = Physics_SphereCast_m2D57A1F4DA048B95DC33939367F2556CEA22B8BD(L_0, L_1, L_2, (RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 *)L_3, L_4, L_5, 0, /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0012;
	}

IL_0012:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Boolean UnityEngine.Physics::SphereCast(UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Physics_SphereCast_m121F1B6EABA580E1661DE00D1974BD614D10C773 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___origin0, float ___radius1, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___direction2, RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * ___hitInfo3, float ___maxDistance4, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = ___origin0;
		float L_1 = ___radius1;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = ___direction2;
		RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * L_3 = ___hitInfo3;
		float L_4 = ___maxDistance4;
		bool L_5 = Physics_SphereCast_m2D57A1F4DA048B95DC33939367F2556CEA22B8BD(L_0, L_1, L_2, (RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 *)L_3, L_4, ((int32_t)-5), 0, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_0012;
	}

IL_0012:
	{
		bool L_6 = V_0;
		return L_6;
	}
}
// System.Boolean UnityEngine.Physics::SphereCast(UnityEngine.Ray,System.Single,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Physics_SphereCast_m6B6F941FB5632998A000262CDC9337E2B46E3871 (Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  ___ray0, float ___radius1, RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * ___hitInfo2, float ___maxDistance3, int32_t ___layerMask4, int32_t ___queryTriggerInteraction5, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = Ray_get_origin_m3773CA7B1E2F26F6F1447652B485D86C0BEC5187((Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *)(&___ray0), /*hidden argument*/NULL);
		float L_1 = ___radius1;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = Ray_get_direction_m9E6468CD87844B437FC4B93491E63D388322F76E((Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *)(&___ray0), /*hidden argument*/NULL);
		RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * L_3 = ___hitInfo2;
		float L_4 = ___maxDistance3;
		int32_t L_5 = ___layerMask4;
		int32_t L_6 = ___queryTriggerInteraction5;
		bool L_7 = Physics_SphereCast_m2D57A1F4DA048B95DC33939367F2556CEA22B8BD(L_0, L_1, L_2, (RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 *)L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		goto IL_001e;
	}

IL_001e:
	{
		bool L_8 = V_0;
		return L_8;
	}
}
// System.Boolean UnityEngine.Physics::SphereCast(UnityEngine.Ray,System.Single,UnityEngine.RaycastHit&,System.Single,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Physics_SphereCast_mE831E516D318F8928FD9020BF7E9FEAD95574D68 (Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  ___ray0, float ___radius1, RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * ___hitInfo2, float ___maxDistance3, int32_t ___layerMask4, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  L_0 = ___ray0;
		float L_1 = ___radius1;
		RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * L_2 = ___hitInfo2;
		float L_3 = ___maxDistance3;
		int32_t L_4 = ___layerMask4;
		bool L_5 = Physics_SphereCast_m6B6F941FB5632998A000262CDC9337E2B46E3871(L_0, L_1, (RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 *)L_2, L_3, L_4, 0, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_0010;
	}

IL_0010:
	{
		bool L_6 = V_0;
		return L_6;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::Internal_RaycastAll(UnityEngine.PhysicsScene,UnityEngine.Ray,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* Physics_Internal_RaycastAll_m366801D3E9D3DE8E9A94C5243D1FB2132DFDD851 (PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  ___physicsScene0, Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  ___ray1, float ___maxDistance2, int32_t ___mask3, int32_t ___queryTriggerInteraction4, const RuntimeMethod* method)
{
	{
		float L_0 = ___maxDistance2;
		int32_t L_1 = ___mask3;
		int32_t L_2 = ___queryTriggerInteraction4;
		RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* L_3 = Physics_Internal_RaycastAll_Injected_m1FD369E67F19BB09F85967481F69EB24B5247913((PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 *)(&___physicsScene0), (Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *)(&___ray1), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* Physics_RaycastAll_m6627435AE46B6720066FEEA5A80923420EE8FD2D (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___origin0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___direction1, float ___maxDistance2, int32_t ___layerMask3, int32_t ___queryTriggerInteraction4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics_RaycastAll_m6627435AE46B6720066FEEA5A80923420EE8FD2D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_2;
	memset((&V_2), 0, sizeof(V_2));
	Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  V_3;
	memset((&V_3), 0, sizeof(V_3));
	RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* V_4 = NULL;
	{
		float L_0 = Vector3_get_magnitude_m9A750659B60C5FE0C30438A7F9681775D5DB1274((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&___direction1), /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = V_0;
		V_1 = (bool)((((float)L_1) > ((float)(1.401298E-45f)))? 1 : 0);
		bool L_2 = V_1;
		if (!L_2)
		{
			goto IL_003a;
		}
	}
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_3 = ___direction1;
		float L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_5 = Vector3_op_Division_mDF34F1CC445981B4D1137765BC6277419E561624(L_3, L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_6 = ___origin0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_7 = V_2;
		Ray__ctor_m695D219349B8AA4C82F96C55A27D384C07736F6B((Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *)(&V_3), L_6, L_7, /*hidden argument*/NULL);
		PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  L_8 = Physics_get_defaultPhysicsScene_mEA96CE906577479223C956BFF0B8D0B79BB47B30(/*hidden argument*/NULL);
		Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  L_9 = V_3;
		float L_10 = ___maxDistance2;
		int32_t L_11 = ___layerMask3;
		int32_t L_12 = ___queryTriggerInteraction4;
		RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* L_13 = Physics_Internal_RaycastAll_m366801D3E9D3DE8E9A94C5243D1FB2132DFDD851(L_8, L_9, L_10, L_11, L_12, /*hidden argument*/NULL);
		V_4 = L_13;
		goto IL_0045;
	}

IL_003a:
	{
		RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* L_14 = (RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57*)(RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57*)SZArrayNew(RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57_il2cpp_TypeInfo_var, (uint32_t)0);
		V_4 = L_14;
		goto IL_0045;
	}

IL_0045:
	{
		RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* L_15 = V_4;
		return L_15;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* Physics_RaycastAll_m4FC397E4B7FEF7B5C9D172F95D6FB32CFC0F66FC (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___origin0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___direction1, float ___maxDistance2, int32_t ___layerMask3, const RuntimeMethod* method)
{
	RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* V_0 = NULL;
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = ___origin0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = ___direction1;
		float L_2 = ___maxDistance2;
		int32_t L_3 = ___layerMask3;
		RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* L_4 = Physics_RaycastAll_m6627435AE46B6720066FEEA5A80923420EE8FD2D(L_0, L_1, L_2, L_3, 0, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_000e;
	}

IL_000e:
	{
		RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* Physics_RaycastAll_m93B94F31C64E8F64ACE18231FC85168D519B7F04 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___origin0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___direction1, float ___maxDistance2, const RuntimeMethod* method)
{
	RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* V_0 = NULL;
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = ___origin0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = ___direction1;
		float L_2 = ___maxDistance2;
		RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* L_3 = Physics_RaycastAll_m6627435AE46B6720066FEEA5A80923420EE8FD2D(L_0, L_1, L_2, ((int32_t)-5), 0, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_000f;
	}

IL_000f:
	{
		RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* L_4 = V_0;
		return L_4;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* Physics_RaycastAll_mE2ED29FCF8983C212F2628A8934CFAEBA4B683DE (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___origin0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___direction1, const RuntimeMethod* method)
{
	RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* V_0 = NULL;
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = ___origin0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = ___direction1;
		RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* L_2 = Physics_RaycastAll_m6627435AE46B6720066FEEA5A80923420EE8FD2D(L_0, L_1, (std::numeric_limits<float>::infinity()), ((int32_t)-5), 0, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0013;
	}

IL_0013:
	{
		RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* Physics_RaycastAll_mF5DB872CCEFE1710439DB9939EACE1DCDEFE2A7D (Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  ___ray0, float ___maxDistance1, int32_t ___layerMask2, int32_t ___queryTriggerInteraction3, const RuntimeMethod* method)
{
	RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* V_0 = NULL;
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = Ray_get_origin_m3773CA7B1E2F26F6F1447652B485D86C0BEC5187((Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *)(&___ray0), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = Ray_get_direction_m9E6468CD87844B437FC4B93491E63D388322F76E((Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *)(&___ray0), /*hidden argument*/NULL);
		float L_2 = ___maxDistance1;
		int32_t L_3 = ___layerMask2;
		int32_t L_4 = ___queryTriggerInteraction3;
		RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* L_5 = Physics_RaycastAll_m6627435AE46B6720066FEEA5A80923420EE8FD2D(L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_001a;
	}

IL_001a:
	{
		RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* L_6 = V_0;
		return L_6;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray,System.Single,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* Physics_RaycastAll_mE9ACFB603E337E2195E4B9419099E178F7FF82F4 (Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  ___ray0, float ___maxDistance1, int32_t ___layerMask2, const RuntimeMethod* method)
{
	RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* V_0 = NULL;
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = Ray_get_origin_m3773CA7B1E2F26F6F1447652B485D86C0BEC5187((Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *)(&___ray0), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = Ray_get_direction_m9E6468CD87844B437FC4B93491E63D388322F76E((Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *)(&___ray0), /*hidden argument*/NULL);
		float L_2 = ___maxDistance1;
		int32_t L_3 = ___layerMask2;
		RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* L_4 = Physics_RaycastAll_m6627435AE46B6720066FEEA5A80923420EE8FD2D(L_0, L_1, L_2, L_3, 0, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_001a;
	}

IL_001a:
	{
		RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* Physics_RaycastAll_m6D5629FD1D3E3B0001F2819C1AE8660A3949C51F (Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  ___ray0, float ___maxDistance1, const RuntimeMethod* method)
{
	RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* V_0 = NULL;
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = Ray_get_origin_m3773CA7B1E2F26F6F1447652B485D86C0BEC5187((Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *)(&___ray0), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = Ray_get_direction_m9E6468CD87844B437FC4B93491E63D388322F76E((Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *)(&___ray0), /*hidden argument*/NULL);
		float L_2 = ___maxDistance1;
		RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* L_3 = Physics_RaycastAll_m6627435AE46B6720066FEEA5A80923420EE8FD2D(L_0, L_1, L_2, ((int32_t)-5), 0, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_001b;
	}

IL_001b:
	{
		RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* L_4 = V_0;
		return L_4;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* Physics_RaycastAll_m37C7F423486CE3ACBAB4F1A326B9EBD7572B4056 (Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  ___ray0, const RuntimeMethod* method)
{
	RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* V_0 = NULL;
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = Ray_get_origin_m3773CA7B1E2F26F6F1447652B485D86C0BEC5187((Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *)(&___ray0), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = Ray_get_direction_m9E6468CD87844B437FC4B93491E63D388322F76E((Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *)(&___ray0), /*hidden argument*/NULL);
		RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* L_2 = Physics_RaycastAll_m6627435AE46B6720066FEEA5A80923420EE8FD2D(L_0, L_1, (std::numeric_limits<float>::infinity()), ((int32_t)-5), 0, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_001f;
	}

IL_001f:
	{
		RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* L_3 = V_0;
		return L_3;
	}
}
// System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit[],System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Physics_RaycastNonAlloc_m606B435CE959801C9B7D7111E49071E893C951DE (Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  ___ray0, RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* ___results1, float ___maxDistance2, int32_t ___layerMask3, int32_t ___queryTriggerInteraction4, const RuntimeMethod* method)
{
	PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  V_0;
	memset((&V_0), 0, sizeof(V_0));
	int32_t V_1 = 0;
	{
		PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  L_0 = Physics_get_defaultPhysicsScene_mEA96CE906577479223C956BFF0B8D0B79BB47B30(/*hidden argument*/NULL);
		V_0 = L_0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = Ray_get_origin_m3773CA7B1E2F26F6F1447652B485D86C0BEC5187((Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *)(&___ray0), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = Ray_get_direction_m9E6468CD87844B437FC4B93491E63D388322F76E((Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *)(&___ray0), /*hidden argument*/NULL);
		RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* L_3 = ___results1;
		float L_4 = ___maxDistance2;
		int32_t L_5 = ___layerMask3;
		int32_t L_6 = ___queryTriggerInteraction4;
		int32_t L_7 = PhysicsScene_Raycast_mFB4A1BB42BB1D950E3854A08DFE3ECCDEA3BCCB1((PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 *)(&V_0), L_1, L_2, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		goto IL_0024;
	}

IL_0024:
	{
		int32_t L_8 = V_1;
		return L_8;
	}
}
// System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit[],System.Single,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Physics_RaycastNonAlloc_mBFDB05B1AA90C5D587A1BEA6BEE66FEF2DF89894 (Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  ___ray0, RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* ___results1, float ___maxDistance2, int32_t ___layerMask3, const RuntimeMethod* method)
{
	PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  V_0;
	memset((&V_0), 0, sizeof(V_0));
	int32_t V_1 = 0;
	{
		PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  L_0 = Physics_get_defaultPhysicsScene_mEA96CE906577479223C956BFF0B8D0B79BB47B30(/*hidden argument*/NULL);
		V_0 = L_0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = Ray_get_origin_m3773CA7B1E2F26F6F1447652B485D86C0BEC5187((Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *)(&___ray0), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = Ray_get_direction_m9E6468CD87844B437FC4B93491E63D388322F76E((Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *)(&___ray0), /*hidden argument*/NULL);
		RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* L_3 = ___results1;
		float L_4 = ___maxDistance2;
		int32_t L_5 = ___layerMask3;
		int32_t L_6 = PhysicsScene_Raycast_mFB4A1BB42BB1D950E3854A08DFE3ECCDEA3BCCB1((PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 *)(&V_0), L_1, L_2, L_3, L_4, L_5, 0, /*hidden argument*/NULL);
		V_1 = L_6;
		goto IL_0023;
	}

IL_0023:
	{
		int32_t L_7 = V_1;
		return L_7;
	}
}
// System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit[],System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Physics_RaycastNonAlloc_m0A9CEF12FB7EE9C790609A15CBFE975725365CF9 (Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  ___ray0, RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* ___results1, float ___maxDistance2, const RuntimeMethod* method)
{
	PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  V_0;
	memset((&V_0), 0, sizeof(V_0));
	int32_t V_1 = 0;
	{
		PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  L_0 = Physics_get_defaultPhysicsScene_mEA96CE906577479223C956BFF0B8D0B79BB47B30(/*hidden argument*/NULL);
		V_0 = L_0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = Ray_get_origin_m3773CA7B1E2F26F6F1447652B485D86C0BEC5187((Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *)(&___ray0), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = Ray_get_direction_m9E6468CD87844B437FC4B93491E63D388322F76E((Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *)(&___ray0), /*hidden argument*/NULL);
		RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* L_3 = ___results1;
		float L_4 = ___maxDistance2;
		int32_t L_5 = PhysicsScene_Raycast_mFB4A1BB42BB1D950E3854A08DFE3ECCDEA3BCCB1((PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 *)(&V_0), L_1, L_2, L_3, L_4, ((int32_t)-5), 0, /*hidden argument*/NULL);
		V_1 = L_5;
		goto IL_0024;
	}

IL_0024:
	{
		int32_t L_6 = V_1;
		return L_6;
	}
}
// System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Physics_RaycastNonAlloc_m9CC4B976EA1936076FFA1339BF6FBE78A2DA593F (Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  ___ray0, RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* ___results1, const RuntimeMethod* method)
{
	PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  V_0;
	memset((&V_0), 0, sizeof(V_0));
	int32_t V_1 = 0;
	{
		PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  L_0 = Physics_get_defaultPhysicsScene_mEA96CE906577479223C956BFF0B8D0B79BB47B30(/*hidden argument*/NULL);
		V_0 = L_0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = Ray_get_origin_m3773CA7B1E2F26F6F1447652B485D86C0BEC5187((Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *)(&___ray0), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = Ray_get_direction_m9E6468CD87844B437FC4B93491E63D388322F76E((Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *)(&___ray0), /*hidden argument*/NULL);
		RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* L_3 = ___results1;
		int32_t L_4 = PhysicsScene_Raycast_mFB4A1BB42BB1D950E3854A08DFE3ECCDEA3BCCB1((PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 *)(&V_0), L_1, L_2, L_3, (std::numeric_limits<float>::infinity()), ((int32_t)-5), 0, /*hidden argument*/NULL);
		V_1 = L_4;
		goto IL_0028;
	}

IL_0028:
	{
		int32_t L_5 = V_1;
		return L_5;
	}
}
// System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit[],System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Physics_RaycastNonAlloc_m120ADF6BD8DF57A40E6B395299B03FAEE348973F (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___origin0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___direction1, RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* ___results2, float ___maxDistance3, int32_t ___layerMask4, int32_t ___queryTriggerInteraction5, const RuntimeMethod* method)
{
	PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  V_0;
	memset((&V_0), 0, sizeof(V_0));
	int32_t V_1 = 0;
	{
		PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  L_0 = Physics_get_defaultPhysicsScene_mEA96CE906577479223C956BFF0B8D0B79BB47B30(/*hidden argument*/NULL);
		V_0 = L_0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = ___origin0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = ___direction1;
		RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* L_3 = ___results2;
		float L_4 = ___maxDistance3;
		int32_t L_5 = ___layerMask4;
		int32_t L_6 = ___queryTriggerInteraction5;
		int32_t L_7 = PhysicsScene_Raycast_mFB4A1BB42BB1D950E3854A08DFE3ECCDEA3BCCB1((PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 *)(&V_0), L_1, L_2, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		goto IL_0019;
	}

IL_0019:
	{
		int32_t L_8 = V_1;
		return L_8;
	}
}
// System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit[],System.Single,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Physics_RaycastNonAlloc_mA795C150FCE1F242C9806AAAFF04125594B56DDB (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___origin0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___direction1, RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* ___results2, float ___maxDistance3, int32_t ___layerMask4, const RuntimeMethod* method)
{
	PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  V_0;
	memset((&V_0), 0, sizeof(V_0));
	int32_t V_1 = 0;
	{
		PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  L_0 = Physics_get_defaultPhysicsScene_mEA96CE906577479223C956BFF0B8D0B79BB47B30(/*hidden argument*/NULL);
		V_0 = L_0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = ___origin0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = ___direction1;
		RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* L_3 = ___results2;
		float L_4 = ___maxDistance3;
		int32_t L_5 = ___layerMask4;
		int32_t L_6 = PhysicsScene_Raycast_mFB4A1BB42BB1D950E3854A08DFE3ECCDEA3BCCB1((PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 *)(&V_0), L_1, L_2, L_3, L_4, L_5, 0, /*hidden argument*/NULL);
		V_1 = L_6;
		goto IL_0018;
	}

IL_0018:
	{
		int32_t L_7 = V_1;
		return L_7;
	}
}
// System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit[],System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Physics_RaycastNonAlloc_m63644DC59B0554846D62ED3BE3B5517BC97D3316 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___origin0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___direction1, RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* ___results2, float ___maxDistance3, const RuntimeMethod* method)
{
	PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  V_0;
	memset((&V_0), 0, sizeof(V_0));
	int32_t V_1 = 0;
	{
		PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  L_0 = Physics_get_defaultPhysicsScene_mEA96CE906577479223C956BFF0B8D0B79BB47B30(/*hidden argument*/NULL);
		V_0 = L_0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = ___origin0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = ___direction1;
		RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* L_3 = ___results2;
		float L_4 = ___maxDistance3;
		int32_t L_5 = PhysicsScene_Raycast_mFB4A1BB42BB1D950E3854A08DFE3ECCDEA3BCCB1((PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 *)(&V_0), L_1, L_2, L_3, L_4, ((int32_t)-5), 0, /*hidden argument*/NULL);
		V_1 = L_5;
		goto IL_0018;
	}

IL_0018:
	{
		int32_t L_6 = V_1;
		return L_6;
	}
}
// System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Physics_RaycastNonAlloc_mD4393134F4470C9B319603A6CD3FF601F5981C88 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___origin0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___direction1, RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* ___results2, const RuntimeMethod* method)
{
	PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  V_0;
	memset((&V_0), 0, sizeof(V_0));
	int32_t V_1 = 0;
	{
		PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  L_0 = Physics_get_defaultPhysicsScene_mEA96CE906577479223C956BFF0B8D0B79BB47B30(/*hidden argument*/NULL);
		V_0 = L_0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = ___origin0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = ___direction1;
		RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* L_3 = ___results2;
		int32_t L_4 = PhysicsScene_Raycast_mFB4A1BB42BB1D950E3854A08DFE3ECCDEA3BCCB1((PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 *)(&V_0), L_1, L_2, L_3, (std::numeric_limits<float>::infinity()), ((int32_t)-5), 0, /*hidden argument*/NULL);
		V_1 = L_4;
		goto IL_001c;
	}

IL_001c:
	{
		int32_t L_5 = V_1;
		return L_5;
	}
}
// UnityEngine.Collider[] UnityEngine.Physics::OverlapSphere_Internal(UnityEngine.PhysicsScene,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* Physics_OverlapSphere_Internal_m10C15A0F86303EF92B205F16591E344E5728B6D0 (PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  ___physicsScene0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___position1, float ___radius2, int32_t ___layerMask3, int32_t ___queryTriggerInteraction4, const RuntimeMethod* method)
{
	{
		float L_0 = ___radius2;
		int32_t L_1 = ___layerMask3;
		int32_t L_2 = ___queryTriggerInteraction4;
		ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* L_3 = Physics_OverlapSphere_Internal_Injected_m315C4BB796FD73B41F5710D1DB52A6BCB57C344D((PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 *)(&___physicsScene0), (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&___position1), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Collider[] UnityEngine.Physics::OverlapSphere(UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* Physics_OverlapSphere_m73878904F621A28F8DD8855CB113C690CF1E89C0 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___position0, float ___radius1, int32_t ___layerMask2, int32_t ___queryTriggerInteraction3, const RuntimeMethod* method)
{
	ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* V_0 = NULL;
	{
		PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  L_0 = Physics_get_defaultPhysicsScene_mEA96CE906577479223C956BFF0B8D0B79BB47B30(/*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = ___position0;
		float L_2 = ___radius1;
		int32_t L_3 = ___layerMask2;
		int32_t L_4 = ___queryTriggerInteraction3;
		ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* L_5 = Physics_OverlapSphere_Internal_m10C15A0F86303EF92B205F16591E344E5728B6D0(L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_0012;
	}

IL_0012:
	{
		ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* L_6 = V_0;
		return L_6;
	}
}
// UnityEngine.Collider[] UnityEngine.Physics::OverlapSphere(UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* Physics_OverlapSphere_m354A92672F7A6DE59EF1285D02D62247F46A5D84 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___position0, float ___radius1, const RuntimeMethod* method)
{
	ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* V_0 = NULL;
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = ___position0;
		float L_1 = ___radius1;
		ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* L_2 = Physics_OverlapSphere_m73878904F621A28F8DD8855CB113C690CF1E89C0(L_0, L_1, (-1), 0, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_000d;
	}

IL_000d:
	{
		ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* L_3 = V_0;
		return L_3;
	}
}
// System.Boolean UnityEngine.Physics::get_autoSimulation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Physics_get_autoSimulation_m7E21DC24254AD53C7CAD20414F86F6F8E5FE47EC (const RuntimeMethod* method)
{
	typedef bool (*Physics_get_autoSimulation_m7E21DC24254AD53C7CAD20414F86F6F8E5FE47EC_ftn) ();
	static Physics_get_autoSimulation_m7E21DC24254AD53C7CAD20414F86F6F8E5FE47EC_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics_get_autoSimulation_m7E21DC24254AD53C7CAD20414F86F6F8E5FE47EC_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::get_autoSimulation()");
	bool retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Void UnityEngine.Physics::get_gravity_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Physics_get_gravity_Injected_m7B5CE9CF65B245C2C2EA078BE636E393225B0335 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___ret0, const RuntimeMethod* method)
{
	typedef void (*Physics_get_gravity_Injected_m7B5CE9CF65B245C2C2EA078BE636E393225B0335_ftn) (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *);
	static Physics_get_gravity_Injected_m7B5CE9CF65B245C2C2EA078BE636E393225B0335_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics_get_gravity_Injected_m7B5CE9CF65B245C2C2EA078BE636E393225B0335_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::get_gravity_Injected(UnityEngine.Vector3&)");
	_il2cpp_icall_func(___ret0);
}
// System.Void UnityEngine.Physics::get_defaultPhysicsScene_Injected(UnityEngine.PhysicsScene&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Physics_get_defaultPhysicsScene_Injected_mEAB07CA5307D0DDB331A31E6A6BA28CF83009262 (PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 * ___ret0, const RuntimeMethod* method)
{
	typedef void (*Physics_get_defaultPhysicsScene_Injected_mEAB07CA5307D0DDB331A31E6A6BA28CF83009262_ftn) (PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 *);
	static Physics_get_defaultPhysicsScene_Injected_mEAB07CA5307D0DDB331A31E6A6BA28CF83009262_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics_get_defaultPhysicsScene_Injected_mEAB07CA5307D0DDB331A31E6A6BA28CF83009262_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::get_defaultPhysicsScene_Injected(UnityEngine.PhysicsScene&)");
	_il2cpp_icall_func(___ret0);
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::Internal_RaycastAll_Injected(UnityEngine.PhysicsScene&,UnityEngine.Ray&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* Physics_Internal_RaycastAll_Injected_m1FD369E67F19BB09F85967481F69EB24B5247913 (PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 * ___physicsScene0, Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 * ___ray1, float ___maxDistance2, int32_t ___mask3, int32_t ___queryTriggerInteraction4, const RuntimeMethod* method)
{
	typedef RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* (*Physics_Internal_RaycastAll_Injected_m1FD369E67F19BB09F85967481F69EB24B5247913_ftn) (PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 *, Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *, float, int32_t, int32_t);
	static Physics_Internal_RaycastAll_Injected_m1FD369E67F19BB09F85967481F69EB24B5247913_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics_Internal_RaycastAll_Injected_m1FD369E67F19BB09F85967481F69EB24B5247913_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::Internal_RaycastAll_Injected(UnityEngine.PhysicsScene&,UnityEngine.Ray&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)");
	RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* retVal = _il2cpp_icall_func(___physicsScene0, ___ray1, ___maxDistance2, ___mask3, ___queryTriggerInteraction4);
	return retVal;
}
// UnityEngine.Collider[] UnityEngine.Physics::OverlapSphere_Internal_Injected(UnityEngine.PhysicsScene&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* Physics_OverlapSphere_Internal_Injected_m315C4BB796FD73B41F5710D1DB52A6BCB57C344D (PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 * ___physicsScene0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___position1, float ___radius2, int32_t ___layerMask3, int32_t ___queryTriggerInteraction4, const RuntimeMethod* method)
{
	typedef ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* (*Physics_OverlapSphere_Internal_Injected_m315C4BB796FD73B41F5710D1DB52A6BCB57C344D_ftn) (PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 *, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *, float, int32_t, int32_t);
	static Physics_OverlapSphere_Internal_Injected_m315C4BB796FD73B41F5710D1DB52A6BCB57C344D_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics_OverlapSphere_Internal_Injected_m315C4BB796FD73B41F5710D1DB52A6BCB57C344D_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::OverlapSphere_Internal_Injected(UnityEngine.PhysicsScene&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)");
	ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* retVal = _il2cpp_icall_func(___physicsScene0, ___position1, ___radius2, ___layerMask3, ___queryTriggerInteraction4);
	return retVal;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String UnityEngine.PhysicsScene::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* PhysicsScene_ToString_m192392A2C486E2F76AAC736C519CB117BD187BE9 (PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhysicsScene_ToString_m192392A2C486E2F76AAC736C519CB117BD187BE9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_0 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)1);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_1 = L_0;
		int32_t L_2 = __this->get_m_Handle_0();
		int32_t L_3 = L_2;
		RuntimeObject * L_4 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_4);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_4);
		String_t* L_5 = UnityString_Format_m415056ECF8DA7B3EC6A8456E299D0C2002177387(_stringLiteral62C02D6AE723470CE8452EABBE91DD3AB73F31F4, L_1, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_0022;
	}

IL_0022:
	{
		String_t* L_6 = V_0;
		return L_6;
	}
}
IL2CPP_EXTERN_C  String_t* PhysicsScene_ToString_m192392A2C486E2F76AAC736C519CB117BD187BE9_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 * _thisAdjusted = reinterpret_cast<PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 *>(__this + _offset);
	return PhysicsScene_ToString_m192392A2C486E2F76AAC736C519CB117BD187BE9(_thisAdjusted, method);
}
// System.Int32 UnityEngine.PhysicsScene::GetHashCode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PhysicsScene_GetHashCode_m8B6554AA89D7DFD183573EA89E5ACF6B21E08654 (PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_Handle_0();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
IL2CPP_EXTERN_C  int32_t PhysicsScene_GetHashCode_m8B6554AA89D7DFD183573EA89E5ACF6B21E08654_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 * _thisAdjusted = reinterpret_cast<PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 *>(__this + _offset);
	return PhysicsScene_GetHashCode_m8B6554AA89D7DFD183573EA89E5ACF6B21E08654(_thisAdjusted, method);
}
// System.Boolean UnityEngine.PhysicsScene::Equals(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PhysicsScene_Equals_m538C6A86CCC4EF0546590BAC51E5D08ACC2CBEF9 (PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhysicsScene_Equals_m538C6A86CCC4EF0546590BAC51E5D08ACC2CBEF9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  V_0;
	memset((&V_0), 0, sizeof(V_0));
	bool V_1 = false;
	bool V_2 = false;
	{
		RuntimeObject * L_0 = ___other0;
		V_1 = (bool)((((int32_t)((!(((RuntimeObject*)(RuntimeObject *)((RuntimeObject *)IsInstSealed((RuntimeObject*)L_0, PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5_il2cpp_TypeInfo_var))) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_1 = V_1;
		if (!L_1)
		{
			goto IL_0015;
		}
	}
	{
		V_2 = (bool)0;
		goto IL_002d;
	}

IL_0015:
	{
		RuntimeObject * L_2 = ___other0;
		V_0 = ((*(PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 *)((PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 *)UnBox(L_2, PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5_il2cpp_TypeInfo_var))));
		int32_t L_3 = __this->get_m_Handle_0();
		PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  L_4 = V_0;
		int32_t L_5 = L_4.get_m_Handle_0();
		V_2 = (bool)((((int32_t)L_3) == ((int32_t)L_5))? 1 : 0);
		goto IL_002d;
	}

IL_002d:
	{
		bool L_6 = V_2;
		return L_6;
	}
}
IL2CPP_EXTERN_C  bool PhysicsScene_Equals_m538C6A86CCC4EF0546590BAC51E5D08ACC2CBEF9_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 * _thisAdjusted = reinterpret_cast<PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 *>(__this + _offset);
	return PhysicsScene_Equals_m538C6A86CCC4EF0546590BAC51E5D08ACC2CBEF9(_thisAdjusted, ___other0, method);
}
// System.Boolean UnityEngine.PhysicsScene::Equals(UnityEngine.PhysicsScene)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PhysicsScene_Equals_m25CC71614BEABB76B6FB618516B8807DAF8CDDF8 (PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 * __this, PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  ___other0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		int32_t L_0 = __this->get_m_Handle_0();
		PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  L_1 = ___other0;
		int32_t L_2 = L_1.get_m_Handle_0();
		V_0 = (bool)((((int32_t)L_0) == ((int32_t)L_2))? 1 : 0);
		goto IL_0012;
	}

IL_0012:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
IL2CPP_EXTERN_C  bool PhysicsScene_Equals_m25CC71614BEABB76B6FB618516B8807DAF8CDDF8_AdjustorThunk (RuntimeObject * __this, PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  ___other0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 * _thisAdjusted = reinterpret_cast<PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 *>(__this + _offset);
	return PhysicsScene_Equals_m25CC71614BEABB76B6FB618516B8807DAF8CDDF8(_thisAdjusted, ___other0, method);
}
// System.Boolean UnityEngine.PhysicsScene::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PhysicsScene_Raycast_mA90972A8E828722A26EBC754EABB3086BE6E2336 (PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___origin0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___direction1, float ___maxDistance2, int32_t ___layerMask3, int32_t ___queryTriggerInteraction4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhysicsScene_Raycast_mA90972A8E828722A26EBC754EABB3086BE6E2336_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_2;
	memset((&V_2), 0, sizeof(V_2));
	Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  V_3;
	memset((&V_3), 0, sizeof(V_3));
	bool V_4 = false;
	{
		float L_0 = Vector3_get_magnitude_m9A750659B60C5FE0C30438A7F9681775D5DB1274((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&___direction1), /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = V_0;
		V_1 = (bool)((((float)L_1) > ((float)(1.401298E-45f)))? 1 : 0);
		bool L_2 = V_1;
		if (!L_2)
		{
			goto IL_003c;
		}
	}
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_3 = ___direction1;
		float L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_5 = Vector3_op_Division_mDF34F1CC445981B4D1137765BC6277419E561624(L_3, L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_6 = ___origin0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_7 = V_2;
		Ray__ctor_m695D219349B8AA4C82F96C55A27D384C07736F6B((Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *)(&V_3), L_6, L_7, /*hidden argument*/NULL);
		PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  L_8 = (*(PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 *)__this);
		Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  L_9 = V_3;
		float L_10 = ___maxDistance2;
		int32_t L_11 = ___layerMask3;
		int32_t L_12 = ___queryTriggerInteraction4;
		bool L_13 = PhysicsScene_Internal_RaycastTest_m09DD752D166094AC2A6F19302F7DDB2EA88DE67F(L_8, L_9, L_10, L_11, L_12, /*hidden argument*/NULL);
		V_4 = L_13;
		goto IL_0041;
	}

IL_003c:
	{
		V_4 = (bool)0;
		goto IL_0041;
	}

IL_0041:
	{
		bool L_14 = V_4;
		return L_14;
	}
}
IL2CPP_EXTERN_C  bool PhysicsScene_Raycast_mA90972A8E828722A26EBC754EABB3086BE6E2336_AdjustorThunk (RuntimeObject * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___origin0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___direction1, float ___maxDistance2, int32_t ___layerMask3, int32_t ___queryTriggerInteraction4, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 * _thisAdjusted = reinterpret_cast<PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 *>(__this + _offset);
	return PhysicsScene_Raycast_mA90972A8E828722A26EBC754EABB3086BE6E2336(_thisAdjusted, ___origin0, ___direction1, ___maxDistance2, ___layerMask3, ___queryTriggerInteraction4, method);
}
// System.Boolean UnityEngine.PhysicsScene::Internal_RaycastTest(UnityEngine.PhysicsScene,UnityEngine.Ray,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PhysicsScene_Internal_RaycastTest_m09DD752D166094AC2A6F19302F7DDB2EA88DE67F (PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  ___physicsScene0, Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  ___ray1, float ___maxDistance2, int32_t ___layerMask3, int32_t ___queryTriggerInteraction4, const RuntimeMethod* method)
{
	{
		float L_0 = ___maxDistance2;
		int32_t L_1 = ___layerMask3;
		int32_t L_2 = ___queryTriggerInteraction4;
		bool L_3 = PhysicsScene_Internal_RaycastTest_Injected_m544D0227B2312D7431A9351ADA5082B390B4ADDF((PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 *)(&___physicsScene0), (Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *)(&___ray1), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean UnityEngine.PhysicsScene::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PhysicsScene_Raycast_m90D1AE1F641D9344BEBEF32E7AC0BAC883024C0B (PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___origin0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___direction1, RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * ___hitInfo2, float ___maxDistance3, int32_t ___layerMask4, int32_t ___queryTriggerInteraction5, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhysicsScene_Raycast_m90D1AE1F641D9344BEBEF32E7AC0BAC883024C0B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_2;
	memset((&V_2), 0, sizeof(V_2));
	Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  V_3;
	memset((&V_3), 0, sizeof(V_3));
	bool V_4 = false;
	{
		RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * L_0 = ___hitInfo2;
		il2cpp_codegen_initobj(L_0, sizeof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 ));
		float L_1 = Vector3_get_magnitude_m9A750659B60C5FE0C30438A7F9681775D5DB1274((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&___direction1), /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = V_0;
		V_1 = (bool)((((float)L_2) > ((float)(1.401298E-45f)))? 1 : 0);
		bool L_3 = V_1;
		if (!L_3)
		{
			goto IL_0045;
		}
	}
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_4 = ___direction1;
		float L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_6 = Vector3_op_Division_mDF34F1CC445981B4D1137765BC6277419E561624(L_4, L_5, /*hidden argument*/NULL);
		V_2 = L_6;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_7 = ___origin0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_8 = V_2;
		Ray__ctor_m695D219349B8AA4C82F96C55A27D384C07736F6B((Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *)(&V_3), L_7, L_8, /*hidden argument*/NULL);
		PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  L_9 = (*(PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 *)__this);
		Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  L_10 = V_3;
		float L_11 = ___maxDistance3;
		RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * L_12 = ___hitInfo2;
		int32_t L_13 = ___layerMask4;
		int32_t L_14 = ___queryTriggerInteraction5;
		bool L_15 = PhysicsScene_Internal_Raycast_m2769B78FDCAA21C6F609DD709BADFF6F4B07C529(L_9, L_10, L_11, (RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 *)L_12, L_13, L_14, /*hidden argument*/NULL);
		V_4 = L_15;
		goto IL_004a;
	}

IL_0045:
	{
		V_4 = (bool)0;
		goto IL_004a;
	}

IL_004a:
	{
		bool L_16 = V_4;
		return L_16;
	}
}
IL2CPP_EXTERN_C  bool PhysicsScene_Raycast_m90D1AE1F641D9344BEBEF32E7AC0BAC883024C0B_AdjustorThunk (RuntimeObject * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___origin0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___direction1, RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * ___hitInfo2, float ___maxDistance3, int32_t ___layerMask4, int32_t ___queryTriggerInteraction5, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 * _thisAdjusted = reinterpret_cast<PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 *>(__this + _offset);
	return PhysicsScene_Raycast_m90D1AE1F641D9344BEBEF32E7AC0BAC883024C0B(_thisAdjusted, ___origin0, ___direction1, ___hitInfo2, ___maxDistance3, ___layerMask4, ___queryTriggerInteraction5, method);
}
// System.Boolean UnityEngine.PhysicsScene::Internal_Raycast(UnityEngine.PhysicsScene,UnityEngine.Ray,System.Single,UnityEngine.RaycastHit&,System.Int32,UnityEngine.QueryTriggerInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PhysicsScene_Internal_Raycast_m2769B78FDCAA21C6F609DD709BADFF6F4B07C529 (PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  ___physicsScene0, Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  ___ray1, float ___maxDistance2, RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * ___hit3, int32_t ___layerMask4, int32_t ___queryTriggerInteraction5, const RuntimeMethod* method)
{
	{
		float L_0 = ___maxDistance2;
		RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * L_1 = ___hit3;
		int32_t L_2 = ___layerMask4;
		int32_t L_3 = ___queryTriggerInteraction5;
		bool L_4 = PhysicsScene_Internal_Raycast_Injected_m039B99FEFE39873FF8B0F60F44DE817BF43D48DA((PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 *)(&___physicsScene0), (Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *)(&___ray1), L_0, (RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 *)L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Int32 UnityEngine.PhysicsScene::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit[],System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PhysicsScene_Raycast_mFB4A1BB42BB1D950E3854A08DFE3ECCDEA3BCCB1 (PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___origin0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___direction1, RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* ___raycastHits2, float ___maxDistance3, int32_t ___layerMask4, int32_t ___queryTriggerInteraction5, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	bool V_1 = false;
	Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  V_2;
	memset((&V_2), 0, sizeof(V_2));
	int32_t V_3 = 0;
	{
		float L_0 = Vector3_get_magnitude_m9A750659B60C5FE0C30438A7F9681775D5DB1274((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&___direction1), /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = V_0;
		V_1 = (bool)((((float)L_1) > ((float)(1.401298E-45f)))? 1 : 0);
		bool L_2 = V_1;
		if (!L_2)
		{
			goto IL_003b;
		}
	}
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_3 = ___origin0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_4 = Vector3_get_normalized_mE20796F1D2D36244FACD4D14DADB245BE579849B((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&___direction1), /*hidden argument*/NULL);
		Ray__ctor_m695D219349B8AA4C82F96C55A27D384C07736F6B((Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *)(&V_2), L_3, L_4, /*hidden argument*/NULL);
		PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  L_5 = (*(PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 *)__this);
		Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  L_6 = V_2;
		RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* L_7 = ___raycastHits2;
		float L_8 = ___maxDistance3;
		int32_t L_9 = ___layerMask4;
		int32_t L_10 = ___queryTriggerInteraction5;
		int32_t L_11 = PhysicsScene_Internal_RaycastNonAlloc_m9E5DAC724FC74D6B618C82BC8F19980E9636D5B1(L_5, L_6, L_7, L_8, L_9, L_10, /*hidden argument*/NULL);
		V_3 = L_11;
		goto IL_003f;
	}

IL_003b:
	{
		V_3 = 0;
		goto IL_003f;
	}

IL_003f:
	{
		int32_t L_12 = V_3;
		return L_12;
	}
}
IL2CPP_EXTERN_C  int32_t PhysicsScene_Raycast_mFB4A1BB42BB1D950E3854A08DFE3ECCDEA3BCCB1_AdjustorThunk (RuntimeObject * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___origin0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___direction1, RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* ___raycastHits2, float ___maxDistance3, int32_t ___layerMask4, int32_t ___queryTriggerInteraction5, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 * _thisAdjusted = reinterpret_cast<PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 *>(__this + _offset);
	return PhysicsScene_Raycast_mFB4A1BB42BB1D950E3854A08DFE3ECCDEA3BCCB1(_thisAdjusted, ___origin0, ___direction1, ___raycastHits2, ___maxDistance3, ___layerMask4, ___queryTriggerInteraction5, method);
}
// System.Int32 UnityEngine.PhysicsScene::Internal_RaycastNonAlloc(UnityEngine.PhysicsScene,UnityEngine.Ray,UnityEngine.RaycastHit[],System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PhysicsScene_Internal_RaycastNonAlloc_m9E5DAC724FC74D6B618C82BC8F19980E9636D5B1 (PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  ___physicsScene0, Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  ___ray1, RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* ___raycastHits2, float ___maxDistance3, int32_t ___mask4, int32_t ___queryTriggerInteraction5, const RuntimeMethod* method)
{
	{
		RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* L_0 = ___raycastHits2;
		float L_1 = ___maxDistance3;
		int32_t L_2 = ___mask4;
		int32_t L_3 = ___queryTriggerInteraction5;
		int32_t L_4 = PhysicsScene_Internal_RaycastNonAlloc_Injected_m4A5BC29FB0D0AAFC1DE827838CE144F882FE632E((PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 *)(&___physicsScene0), (Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *)(&___ray1), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean UnityEngine.PhysicsScene::Query_SphereCast(UnityEngine.PhysicsScene,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,System.Single,UnityEngine.RaycastHit&,System.Int32,UnityEngine.QueryTriggerInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PhysicsScene_Query_SphereCast_m6DD7B632CF528C867859E329E2C4807C2CC891D8 (PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  ___physicsScene0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___origin1, float ___radius2, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___direction3, float ___maxDistance4, RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * ___hitInfo5, int32_t ___layerMask6, int32_t ___queryTriggerInteraction7, const RuntimeMethod* method)
{
	{
		float L_0 = ___radius2;
		float L_1 = ___maxDistance4;
		RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * L_2 = ___hitInfo5;
		int32_t L_3 = ___layerMask6;
		int32_t L_4 = ___queryTriggerInteraction7;
		bool L_5 = PhysicsScene_Query_SphereCast_Injected_mA3953A644D1ED0B7D37A22E44635A413A4A1A12F((PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 *)(&___physicsScene0), (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&___origin1), L_0, (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&___direction3), L_1, (RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 *)L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Boolean UnityEngine.PhysicsScene::Internal_SphereCast(UnityEngine.PhysicsScene,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PhysicsScene_Internal_SphereCast_mAD5109B24E5C668DC9A49E434B5139CA8A43DBD0 (PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  ___physicsScene0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___origin1, float ___radius2, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___direction3, RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * ___hitInfo4, float ___maxDistance5, int32_t ___layerMask6, int32_t ___queryTriggerInteraction7, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhysicsScene_Internal_SphereCast_mAD5109B24E5C668DC9A49E434B5139CA8A43DBD0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_2;
	memset((&V_2), 0, sizeof(V_2));
	bool V_3 = false;
	{
		float L_0 = Vector3_get_magnitude_m9A750659B60C5FE0C30438A7F9681775D5DB1274((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&___direction3), /*hidden argument*/NULL);
		V_0 = L_0;
		RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * L_1 = ___hitInfo4;
		il2cpp_codegen_initobj(L_1, sizeof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 ));
		float L_2 = V_0;
		V_1 = (bool)((((float)L_2) > ((float)(1.401298E-45f)))? 1 : 0);
		bool L_3 = V_1;
		if (!L_3)
		{
			goto IL_003a;
		}
	}
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_4 = ___direction3;
		float L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_6 = Vector3_op_Division_mDF34F1CC445981B4D1137765BC6277419E561624(L_4, L_5, /*hidden argument*/NULL);
		V_2 = L_6;
		PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  L_7 = ___physicsScene0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_8 = ___origin1;
		float L_9 = ___radius2;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_10 = V_2;
		float L_11 = ___maxDistance5;
		RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * L_12 = ___hitInfo4;
		int32_t L_13 = ___layerMask6;
		int32_t L_14 = ___queryTriggerInteraction7;
		bool L_15 = PhysicsScene_Query_SphereCast_m6DD7B632CF528C867859E329E2C4807C2CC891D8(L_7, L_8, L_9, L_10, L_11, (RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 *)L_12, L_13, L_14, /*hidden argument*/NULL);
		V_3 = L_15;
		goto IL_003e;
	}

IL_003a:
	{
		V_3 = (bool)0;
		goto IL_003e;
	}

IL_003e:
	{
		bool L_16 = V_3;
		return L_16;
	}
}
// System.Boolean UnityEngine.PhysicsScene::SphereCast(UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PhysicsScene_SphereCast_m99D2D1C789BB817406E7C7FBAA75F2019A13D6C1 (PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___origin0, float ___radius1, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___direction2, RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * ___hitInfo3, float ___maxDistance4, int32_t ___layerMask5, int32_t ___queryTriggerInteraction6, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5  L_0 = (*(PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 *)__this);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = ___origin0;
		float L_2 = ___radius1;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_3 = ___direction2;
		RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * L_4 = ___hitInfo3;
		float L_5 = ___maxDistance4;
		int32_t L_6 = ___layerMask5;
		int32_t L_7 = ___queryTriggerInteraction6;
		bool L_8 = PhysicsScene_Internal_SphereCast_mAD5109B24E5C668DC9A49E434B5139CA8A43DBD0(L_0, L_1, L_2, L_3, (RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 *)L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_001a;
	}

IL_001a:
	{
		bool L_9 = V_0;
		return L_9;
	}
}
IL2CPP_EXTERN_C  bool PhysicsScene_SphereCast_m99D2D1C789BB817406E7C7FBAA75F2019A13D6C1_AdjustorThunk (RuntimeObject * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___origin0, float ___radius1, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___direction2, RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * ___hitInfo3, float ___maxDistance4, int32_t ___layerMask5, int32_t ___queryTriggerInteraction6, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 * _thisAdjusted = reinterpret_cast<PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 *>(__this + _offset);
	return PhysicsScene_SphereCast_m99D2D1C789BB817406E7C7FBAA75F2019A13D6C1(_thisAdjusted, ___origin0, ___radius1, ___direction2, ___hitInfo3, ___maxDistance4, ___layerMask5, ___queryTriggerInteraction6, method);
}
// System.Boolean UnityEngine.PhysicsScene::Internal_RaycastTest_Injected(UnityEngine.PhysicsScene&,UnityEngine.Ray&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PhysicsScene_Internal_RaycastTest_Injected_m544D0227B2312D7431A9351ADA5082B390B4ADDF (PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 * ___physicsScene0, Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 * ___ray1, float ___maxDistance2, int32_t ___layerMask3, int32_t ___queryTriggerInteraction4, const RuntimeMethod* method)
{
	typedef bool (*PhysicsScene_Internal_RaycastTest_Injected_m544D0227B2312D7431A9351ADA5082B390B4ADDF_ftn) (PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 *, Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *, float, int32_t, int32_t);
	static PhysicsScene_Internal_RaycastTest_Injected_m544D0227B2312D7431A9351ADA5082B390B4ADDF_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PhysicsScene_Internal_RaycastTest_Injected_m544D0227B2312D7431A9351ADA5082B390B4ADDF_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PhysicsScene::Internal_RaycastTest_Injected(UnityEngine.PhysicsScene&,UnityEngine.Ray&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)");
	bool retVal = _il2cpp_icall_func(___physicsScene0, ___ray1, ___maxDistance2, ___layerMask3, ___queryTriggerInteraction4);
	return retVal;
}
// System.Boolean UnityEngine.PhysicsScene::Internal_Raycast_Injected(UnityEngine.PhysicsScene&,UnityEngine.Ray&,System.Single,UnityEngine.RaycastHit&,System.Int32,UnityEngine.QueryTriggerInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PhysicsScene_Internal_Raycast_Injected_m039B99FEFE39873FF8B0F60F44DE817BF43D48DA (PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 * ___physicsScene0, Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 * ___ray1, float ___maxDistance2, RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * ___hit3, int32_t ___layerMask4, int32_t ___queryTriggerInteraction5, const RuntimeMethod* method)
{
	typedef bool (*PhysicsScene_Internal_Raycast_Injected_m039B99FEFE39873FF8B0F60F44DE817BF43D48DA_ftn) (PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 *, Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *, float, RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 *, int32_t, int32_t);
	static PhysicsScene_Internal_Raycast_Injected_m039B99FEFE39873FF8B0F60F44DE817BF43D48DA_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PhysicsScene_Internal_Raycast_Injected_m039B99FEFE39873FF8B0F60F44DE817BF43D48DA_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PhysicsScene::Internal_Raycast_Injected(UnityEngine.PhysicsScene&,UnityEngine.Ray&,System.Single,UnityEngine.RaycastHit&,System.Int32,UnityEngine.QueryTriggerInteraction)");
	bool retVal = _il2cpp_icall_func(___physicsScene0, ___ray1, ___maxDistance2, ___hit3, ___layerMask4, ___queryTriggerInteraction5);
	return retVal;
}
// System.Int32 UnityEngine.PhysicsScene::Internal_RaycastNonAlloc_Injected(UnityEngine.PhysicsScene&,UnityEngine.Ray&,UnityEngine.RaycastHit[],System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PhysicsScene_Internal_RaycastNonAlloc_Injected_m4A5BC29FB0D0AAFC1DE827838CE144F882FE632E (PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 * ___physicsScene0, Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 * ___ray1, RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* ___raycastHits2, float ___maxDistance3, int32_t ___mask4, int32_t ___queryTriggerInteraction5, const RuntimeMethod* method)
{
	typedef int32_t (*PhysicsScene_Internal_RaycastNonAlloc_Injected_m4A5BC29FB0D0AAFC1DE827838CE144F882FE632E_ftn) (PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 *, Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 *, RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57*, float, int32_t, int32_t);
	static PhysicsScene_Internal_RaycastNonAlloc_Injected_m4A5BC29FB0D0AAFC1DE827838CE144F882FE632E_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PhysicsScene_Internal_RaycastNonAlloc_Injected_m4A5BC29FB0D0AAFC1DE827838CE144F882FE632E_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PhysicsScene::Internal_RaycastNonAlloc_Injected(UnityEngine.PhysicsScene&,UnityEngine.Ray&,UnityEngine.RaycastHit[],System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)");
	int32_t retVal = _il2cpp_icall_func(___physicsScene0, ___ray1, ___raycastHits2, ___maxDistance3, ___mask4, ___queryTriggerInteraction5);
	return retVal;
}
// System.Boolean UnityEngine.PhysicsScene::Query_SphereCast_Injected(UnityEngine.PhysicsScene&,UnityEngine.Vector3&,System.Single,UnityEngine.Vector3&,System.Single,UnityEngine.RaycastHit&,System.Int32,UnityEngine.QueryTriggerInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PhysicsScene_Query_SphereCast_Injected_mA3953A644D1ED0B7D37A22E44635A413A4A1A12F (PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 * ___physicsScene0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___origin1, float ___radius2, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___direction3, float ___maxDistance4, RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * ___hitInfo5, int32_t ___layerMask6, int32_t ___queryTriggerInteraction7, const RuntimeMethod* method)
{
	typedef bool (*PhysicsScene_Query_SphereCast_Injected_mA3953A644D1ED0B7D37A22E44635A413A4A1A12F_ftn) (PhysicsScene_tC24001806A99648F6EFD4D63957D47D8AB1668F5 *, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *, float, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *, float, RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 *, int32_t, int32_t);
	static PhysicsScene_Query_SphereCast_Injected_mA3953A644D1ED0B7D37A22E44635A413A4A1A12F_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PhysicsScene_Query_SphereCast_Injected_mA3953A644D1ED0B7D37A22E44635A413A4A1A12F_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PhysicsScene::Query_SphereCast_Injected(UnityEngine.PhysicsScene&,UnityEngine.Vector3&,System.Single,UnityEngine.Vector3&,System.Single,UnityEngine.RaycastHit&,System.Int32,UnityEngine.QueryTriggerInteraction)");
	bool retVal = _il2cpp_icall_func(___physicsScene0, ___origin1, ___radius2, ___direction3, ___maxDistance4, ___hitInfo5, ___layerMask6, ___queryTriggerInteraction7);
	return retVal;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Collider UnityEngine.RaycastHit::get_collider()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * RaycastHit_get_collider_mE70B84C4312B567344F60992A6067855F2C3A7A9 (RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RaycastHit_get_collider_mE70B84C4312B567344F60992A6067855F2C3A7A9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * V_0 = NULL;
	{
		int32_t L_0 = __this->get_m_Collider_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * L_1 = Object_FindObjectFromInstanceID_m7594ED98F525AAE38FEC80052729ECAF3E821350(L_0, /*hidden argument*/NULL);
		V_0 = ((Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF *)IsInstClass((RuntimeObject*)L_1, Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF_il2cpp_TypeInfo_var));
		goto IL_0014;
	}

IL_0014:
	{
		Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * L_2 = V_0;
		return L_2;
	}
}
IL2CPP_EXTERN_C  Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * RaycastHit_get_collider_mE70B84C4312B567344F60992A6067855F2C3A7A9_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * _thisAdjusted = reinterpret_cast<RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 *>(__this + _offset);
	return RaycastHit_get_collider_mE70B84C4312B567344F60992A6067855F2C3A7A9(_thisAdjusted, method);
}
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_point()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  RaycastHit_get_point_m0E564B2A72C7A744B889AE9D596F3EFA55059001 (RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * __this, const RuntimeMethod* method)
{
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = __this->get_m_Point_0();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = V_0;
		return L_1;
	}
}
IL2CPP_EXTERN_C  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  RaycastHit_get_point_m0E564B2A72C7A744B889AE9D596F3EFA55059001_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * _thisAdjusted = reinterpret_cast<RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 *>(__this + _offset);
	return RaycastHit_get_point_m0E564B2A72C7A744B889AE9D596F3EFA55059001(_thisAdjusted, method);
}
// System.Void UnityEngine.RaycastHit::set_point(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RaycastHit_set_point_mCB652835DA3A0AED8A8574B4A47FD9BF8F9A4191 (RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value0, const RuntimeMethod* method)
{
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = ___value0;
		__this->set_m_Point_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void RaycastHit_set_point_mCB652835DA3A0AED8A8574B4A47FD9BF8F9A4191_AdjustorThunk (RuntimeObject * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * _thisAdjusted = reinterpret_cast<RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 *>(__this + _offset);
	RaycastHit_set_point_mCB652835DA3A0AED8A8574B4A47FD9BF8F9A4191(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_normal()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  RaycastHit_get_normal_mF736A6D09D98D63AB7E5BF10F38AEBFC177A1D94 (RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * __this, const RuntimeMethod* method)
{
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = __this->get_m_Normal_1();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = V_0;
		return L_1;
	}
}
IL2CPP_EXTERN_C  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  RaycastHit_get_normal_mF736A6D09D98D63AB7E5BF10F38AEBFC177A1D94_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * _thisAdjusted = reinterpret_cast<RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 *>(__this + _offset);
	return RaycastHit_get_normal_mF736A6D09D98D63AB7E5BF10F38AEBFC177A1D94(_thisAdjusted, method);
}
// System.Void UnityEngine.RaycastHit::set_normal(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RaycastHit_set_normal_mB21B55B92CE45F93EB140793D31DFE69693F7303 (RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value0, const RuntimeMethod* method)
{
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = ___value0;
		__this->set_m_Normal_1(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void RaycastHit_set_normal_mB21B55B92CE45F93EB140793D31DFE69693F7303_AdjustorThunk (RuntimeObject * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * _thisAdjusted = reinterpret_cast<RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 *>(__this + _offset);
	RaycastHit_set_normal_mB21B55B92CE45F93EB140793D31DFE69693F7303(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.RaycastHit::get_distance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float RaycastHit_get_distance_m1CBA60855C35F29BBC348D374BBC76386A243543 (RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Distance_3();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		float L_1 = V_0;
		return L_1;
	}
}
IL2CPP_EXTERN_C  float RaycastHit_get_distance_m1CBA60855C35F29BBC348D374BBC76386A243543_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * _thisAdjusted = reinterpret_cast<RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 *>(__this + _offset);
	return RaycastHit_get_distance_m1CBA60855C35F29BBC348D374BBC76386A243543(_thisAdjusted, method);
}
// UnityEngine.Rigidbody UnityEngine.RaycastHit::get_rigidbody()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * RaycastHit_get_rigidbody_m8E28BDE09DC588AAF0C15182AFF3C00EE11EB0FC (RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RaycastHit_get_rigidbody_m8E28BDE09DC588AAF0C15182AFF3C00EE11EB0FC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * V_0 = NULL;
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * G_B3_0 = NULL;
	{
		Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * L_0 = RaycastHit_get_collider_mE70B84C4312B567344F60992A6067855F2C3A7A9((RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_0, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B3_0 = ((Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 *)(NULL));
		goto IL_001d;
	}

IL_0012:
	{
		Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * L_2 = RaycastHit_get_collider_mE70B84C4312B567344F60992A6067855F2C3A7A9((RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 *)__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * L_3 = Collider_get_attachedRigidbody_m9E3C688EAE2F6A76C9AC14968D96769D9A71B1E8(L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
	}

IL_001d:
	{
		V_0 = G_B3_0;
		goto IL_0020;
	}

IL_0020:
	{
		Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * L_4 = V_0;
		return L_4;
	}
}
IL2CPP_EXTERN_C  Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * RaycastHit_get_rigidbody_m8E28BDE09DC588AAF0C15182AFF3C00EE11EB0FC_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * _thisAdjusted = reinterpret_cast<RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 *>(__this + _offset);
	return RaycastHit_get_rigidbody_m8E28BDE09DC588AAF0C15182AFF3C00EE11EB0FC(_thisAdjusted, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Vector3 UnityEngine.Rigidbody::get_velocity()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Rigidbody_get_velocity_m584A6D79C3657C21AE9CAA56BEE05582B8D5A2B8 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, const RuntimeMethod* method)
{
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Rigidbody_get_velocity_Injected_m61C1628D08B48C8971E476FCBB9323CB5EB73DAC(__this, (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_0), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Rigidbody::set_velocity(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_set_velocity_m8D129E88E62AD02AB81CFC8BE694C4A5A2B2B380 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value0, const RuntimeMethod* method)
{
	{
		Rigidbody_set_velocity_Injected_mA0CBA4077CADA05CF30404564E8D6260EF60E07D(__this, (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&___value0), /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Rigidbody::get_angularVelocity()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Rigidbody_get_angularVelocity_mA5D414D6E27755C944485A750F974BEA24CF27F0 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, const RuntimeMethod* method)
{
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Rigidbody_get_angularVelocity_Injected_mF130145BC134034C2ECB1C4E9BF276F0A63EC0D1(__this, (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_0), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Rigidbody::set_angularVelocity(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_set_angularVelocity_m1839DCBC87B01EFD0B4936E84E503E38774B962C (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value0, const RuntimeMethod* method)
{
	{
		Rigidbody_set_angularVelocity_Injected_mC5F63763F3701505D5F8E41CCE7F6325F1024783(__this, (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.Rigidbody::get_drag()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Rigidbody_get_drag_m2B304BB4C4A1A0E349C8B57C9085C0BC66DDE28E (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, const RuntimeMethod* method)
{
	typedef float (*Rigidbody_get_drag_m2B304BB4C4A1A0E349C8B57C9085C0BC66DDE28E_ftn) (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 *);
	static Rigidbody_get_drag_m2B304BB4C4A1A0E349C8B57C9085C0BC66DDE28E_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_get_drag_m2B304BB4C4A1A0E349C8B57C9085C0BC66DDE28E_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::get_drag()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Single UnityEngine.Rigidbody::get_angularDrag()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Rigidbody_get_angularDrag_mD6C855353D256A0B08285433ACEA090A8460C14B (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, const RuntimeMethod* method)
{
	typedef float (*Rigidbody_get_angularDrag_mD6C855353D256A0B08285433ACEA090A8460C14B_ftn) (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 *);
	static Rigidbody_get_angularDrag_mD6C855353D256A0B08285433ACEA090A8460C14B_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_get_angularDrag_mD6C855353D256A0B08285433ACEA090A8460C14B_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::get_angularDrag()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Single UnityEngine.Rigidbody::get_mass()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Rigidbody_get_mass_m36189AE2961EE2C537D9CF5EC5881FD64CE43EB1 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, const RuntimeMethod* method)
{
	typedef float (*Rigidbody_get_mass_m36189AE2961EE2C537D9CF5EC5881FD64CE43EB1_ftn) (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 *);
	static Rigidbody_get_mass_m36189AE2961EE2C537D9CF5EC5881FD64CE43EB1_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_get_mass_m36189AE2961EE2C537D9CF5EC5881FD64CE43EB1_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::get_mass()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Rigidbody::set_mass(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_set_mass_mA8946A1A06B07CE6DFF2F1A9081A2E2AA406FDC9 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, float ___value0, const RuntimeMethod* method)
{
	typedef void (*Rigidbody_set_mass_mA8946A1A06B07CE6DFF2F1A9081A2E2AA406FDC9_ftn) (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 *, float);
	static Rigidbody_set_mass_mA8946A1A06B07CE6DFF2F1A9081A2E2AA406FDC9_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_mass_mA8946A1A06B07CE6DFF2F1A9081A2E2AA406FDC9_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_mass(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.Rigidbody::get_useGravity()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Rigidbody_get_useGravity_m802E0C0B4F2C2B521D5369EA027325157A53FCAA (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, const RuntimeMethod* method)
{
	typedef bool (*Rigidbody_get_useGravity_m802E0C0B4F2C2B521D5369EA027325157A53FCAA_ftn) (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 *);
	static Rigidbody_get_useGravity_m802E0C0B4F2C2B521D5369EA027325157A53FCAA_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_get_useGravity_m802E0C0B4F2C2B521D5369EA027325157A53FCAA_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::get_useGravity()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Rigidbody::set_useGravity(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_set_useGravity_mB0D957A9D8A9819E18D2E81F465C5C0B60CBC6DA (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, bool ___value0, const RuntimeMethod* method)
{
	typedef void (*Rigidbody_set_useGravity_mB0D957A9D8A9819E18D2E81F465C5C0B60CBC6DA_ftn) (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 *, bool);
	static Rigidbody_set_useGravity_mB0D957A9D8A9819E18D2E81F465C5C0B60CBC6DA_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_useGravity_mB0D957A9D8A9819E18D2E81F465C5C0B60CBC6DA_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_useGravity(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.Rigidbody::get_isKinematic()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Rigidbody_get_isKinematic_mCF624F7C1C78267224EFBEAF9B4FD72CDE56CEB2 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, const RuntimeMethod* method)
{
	typedef bool (*Rigidbody_get_isKinematic_mCF624F7C1C78267224EFBEAF9B4FD72CDE56CEB2_ftn) (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 *);
	static Rigidbody_get_isKinematic_mCF624F7C1C78267224EFBEAF9B4FD72CDE56CEB2_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_get_isKinematic_mCF624F7C1C78267224EFBEAF9B4FD72CDE56CEB2_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::get_isKinematic()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Rigidbody::set_isKinematic(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_set_isKinematic_m856AB59E5A6207892C439AFC8DDF5620B941E71B (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, bool ___value0, const RuntimeMethod* method)
{
	typedef void (*Rigidbody_set_isKinematic_m856AB59E5A6207892C439AFC8DDF5620B941E71B_ftn) (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 *, bool);
	static Rigidbody_set_isKinematic_m856AB59E5A6207892C439AFC8DDF5620B941E71B_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_isKinematic_m856AB59E5A6207892C439AFC8DDF5620B941E71B_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_isKinematic(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::set_freezeRotation(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_set_freezeRotation_m946DD8BB0A2AD6F8C5CA22F506628168A3767D13 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, bool ___value0, const RuntimeMethod* method)
{
	typedef void (*Rigidbody_set_freezeRotation_m946DD8BB0A2AD6F8C5CA22F506628168A3767D13_ftn) (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 *, bool);
	static Rigidbody_set_freezeRotation_m946DD8BB0A2AD6F8C5CA22F506628168A3767D13_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_freezeRotation_m946DD8BB0A2AD6F8C5CA22F506628168A3767D13_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_freezeRotation(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::set_constraints(UnityEngine.RigidbodyConstraints)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_set_constraints_m6E6AACB03165E54952E7CFE13C07188205A7061F (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*Rigidbody_set_constraints_m6E6AACB03165E54952E7CFE13C07188205A7061F_ftn) (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 *, int32_t);
	static Rigidbody_set_constraints_m6E6AACB03165E54952E7CFE13C07188205A7061F_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_constraints_m6E6AACB03165E54952E7CFE13C07188205A7061F_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_constraints(UnityEngine.RigidbodyConstraints)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector3 UnityEngine.Rigidbody::get_centerOfMass()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Rigidbody_get_centerOfMass_m616EAA0BFA8FB7413D6FCB18AD937009DC0929F6 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, const RuntimeMethod* method)
{
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Rigidbody_get_centerOfMass_Injected_m15590C0C367EB322CF67A40CFE2A28A814A8766F(__this, (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_0), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Rigidbody::set_centerOfMass(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_set_centerOfMass_m038CD9CF36C48D04C92AFCF15AB001789AD699D4 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value0, const RuntimeMethod* method)
{
	{
		Rigidbody_set_centerOfMass_Injected_m999B2BDCDFCAC0D049AF7A7BE5BEB7A4B75D1D0A(__this, (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&___value0), /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Rigidbody::get_worldCenterOfMass()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Rigidbody_get_worldCenterOfMass_m4604E98FB98A4515E9F9AD775AB56E2BB98D6F2A (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, const RuntimeMethod* method)
{
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Rigidbody_get_worldCenterOfMass_Injected_mCF61FE2285D07E76A226249DA7C877F6888C945F(__this, (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_0), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = V_0;
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Rigidbody::get_inertiaTensor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Rigidbody_get_inertiaTensor_m27F5982AB5B321AFA501C8453963A9EE8190C578 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, const RuntimeMethod* method)
{
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Rigidbody_get_inertiaTensor_Injected_m87F89C6379EB9C47DDB03A4F5AE47CBFF9CFD2FF(__this, (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_0), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Rigidbody::set_inertiaTensor(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_set_inertiaTensor_mEE7AF73CC252F44FD2693D22FE6AD8B8ED15D146 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value0, const RuntimeMethod* method)
{
	{
		Rigidbody_set_inertiaTensor_Injected_mB43DBD3F1C979D04B0955484E0B14A971C7AE838(__this, (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::set_detectCollisions(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_set_detectCollisions_mD6BE24EC78D298D6F737822E3A63123AF586045F (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, bool ___value0, const RuntimeMethod* method)
{
	typedef void (*Rigidbody_set_detectCollisions_mD6BE24EC78D298D6F737822E3A63123AF586045F_ftn) (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 *, bool);
	static Rigidbody_set_detectCollisions_mD6BE24EC78D298D6F737822E3A63123AF586045F_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_detectCollisions_mD6BE24EC78D298D6F737822E3A63123AF586045F_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_detectCollisions(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector3 UnityEngine.Rigidbody::get_position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Rigidbody_get_position_m478D060638E43DE3AE9C931A42593484B8310113 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, const RuntimeMethod* method)
{
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Rigidbody_get_position_Injected_m8B581634C88AB745341D4ACF1CC12544769EC019(__this, (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_0), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Rigidbody::set_position(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_set_position_m54EED7F2D5EC9D34937D94B671BD6DE356DD0E7F (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value0, const RuntimeMethod* method)
{
	{
		Rigidbody_set_position_Injected_m3210674B4144CD8E599226F6343BC541A9975581(__this, (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&___value0), /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Quaternion UnityEngine.Rigidbody::get_rotation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  Rigidbody_get_rotation_mD967DD98F16F80C0D74F8F1C25953D0609906BE5 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, const RuntimeMethod* method)
{
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Rigidbody_get_rotation_Injected_m5FEC1EC2B3CFCE6585C63CD64ECAE39828B73C91(__this, (Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 *)(&V_0), /*hidden argument*/NULL);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Rigidbody::set_rotation(UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_set_rotation_mFC6AD10748F2A0E04B6D2DBADEC168D60F90345B (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___value0, const RuntimeMethod* method)
{
	{
		Rigidbody_set_rotation_Injected_m52738493BB4ACEDC29288DA8905907C1918A99C3(__this, (Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 *)(&___value0), /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.RigidbodyInterpolation UnityEngine.Rigidbody::get_interpolation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Rigidbody_get_interpolation_mCFC127818CB91952344873D5C0B089A61FAE5C95 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*Rigidbody_get_interpolation_mCFC127818CB91952344873D5C0B089A61FAE5C95_ftn) (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 *);
	static Rigidbody_get_interpolation_mCFC127818CB91952344873D5C0B089A61FAE5C95_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_get_interpolation_mCFC127818CB91952344873D5C0B089A61FAE5C95_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::get_interpolation()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Rigidbody::set_interpolation(UnityEngine.RigidbodyInterpolation)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_set_interpolation_m34A9426028D5360BD8F8FBBB6A1DCB00DE8D1540 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*Rigidbody_set_interpolation_m34A9426028D5360BD8F8FBBB6A1DCB00DE8D1540_ftn) (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 *, int32_t);
	static Rigidbody_set_interpolation_m34A9426028D5360BD8F8FBBB6A1DCB00DE8D1540_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_interpolation_m34A9426028D5360BD8F8FBBB6A1DCB00DE8D1540_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_interpolation(UnityEngine.RigidbodyInterpolation)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::set_solverIterations(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_set_solverIterations_mF597BD259445EF2E8F5A5FA7566CA04B8D4946E7 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*Rigidbody_set_solverIterations_mF597BD259445EF2E8F5A5FA7566CA04B8D4946E7_ftn) (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 *, int32_t);
	static Rigidbody_set_solverIterations_mF597BD259445EF2E8F5A5FA7566CA04B8D4946E7_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_solverIterations_mF597BD259445EF2E8F5A5FA7566CA04B8D4946E7_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_solverIterations(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::set_maxAngularVelocity(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_set_maxAngularVelocity_m0EF6E6142D8F4484B436019609F21637BB2E1142 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, float ___value0, const RuntimeMethod* method)
{
	typedef void (*Rigidbody_set_maxAngularVelocity_m0EF6E6142D8F4484B436019609F21637BB2E1142_ftn) (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 *, float);
	static Rigidbody_set_maxAngularVelocity_m0EF6E6142D8F4484B436019609F21637BB2E1142_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_maxAngularVelocity_m0EF6E6142D8F4484B436019609F21637BB2E1142_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_maxAngularVelocity(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::MovePosition(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_MovePosition_m5807AA5CDEC1B8350618166B2DF56FCAAAFFF7C1 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___position0, const RuntimeMethod* method)
{
	{
		Rigidbody_MovePosition_Injected_mFD8BDE08912A4C3FC240A8834C3394A88349C440(__this, (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&___position0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::MoveRotation(UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_MoveRotation_mCBE2CF9F1B4A86C4BCB899AAB4C4EE8BBAD21C84 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___rot0, const RuntimeMethod* method)
{
	{
		Rigidbody_MoveRotation_Injected_m283E5FB82CB1721DC5220311967ED0E6ED831BF0(__this, (Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 *)(&___rot0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::Sleep()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_Sleep_mFE36499136A22ED98FC53B9EC716B380D7A8AD7E (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, const RuntimeMethod* method)
{
	typedef void (*Rigidbody_Sleep_mFE36499136A22ED98FC53B9EC716B380D7A8AD7E_ftn) (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 *);
	static Rigidbody_Sleep_mFE36499136A22ED98FC53B9EC716B380D7A8AD7E_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_Sleep_mFE36499136A22ED98FC53B9EC716B380D7A8AD7E_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::Sleep()");
	_il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.Rigidbody::IsSleeping()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Rigidbody_IsSleeping_m1B8DD08C5F9B360D3CA5C60500472FC17CA2DA99 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, const RuntimeMethod* method)
{
	typedef bool (*Rigidbody_IsSleeping_m1B8DD08C5F9B360D3CA5C60500472FC17CA2DA99_ftn) (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 *);
	static Rigidbody_IsSleeping_m1B8DD08C5F9B360D3CA5C60500472FC17CA2DA99_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_IsSleeping_m1B8DD08C5F9B360D3CA5C60500472FC17CA2DA99_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::IsSleeping()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Rigidbody::WakeUp()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_WakeUp_m30E4CCC2A1A5829084881EBF52431ADDA4F2B336 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, const RuntimeMethod* method)
{
	typedef void (*Rigidbody_WakeUp_m30E4CCC2A1A5829084881EBF52431ADDA4F2B336_ftn) (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 *);
	static Rigidbody_WakeUp_m30E4CCC2A1A5829084881EBF52431ADDA4F2B336_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_WakeUp_m30E4CCC2A1A5829084881EBF52431ADDA4F2B336_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::WakeUp()");
	_il2cpp_icall_func(__this);
}
// UnityEngine.Vector3 UnityEngine.Rigidbody::GetPointVelocity(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Rigidbody_GetPointVelocity_mFA72C6F1FF5AEC64B25B0F7F7682BB9666D28AC4 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___worldPoint0, const RuntimeMethod* method)
{
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Rigidbody_GetPointVelocity_Injected_mFFC7165D5058AA5B62F08545554F78EE25FACC2E(__this, (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&___worldPoint0), (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_0), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Rigidbody::AddForce(UnityEngine.Vector3,UnityEngine.ForceMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_AddForce_mD64ACF772614FE36CFD8A477A07A407B35DF1A54 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___force0, int32_t ___mode1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___mode1;
		Rigidbody_AddForce_Injected_mEB8DB621D03BE740C5CBD472E873FF8B6EF6C119(__this, (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&___force0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::AddForce(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_AddForce_mC8140D90B806634A733624F671C45AD7CDBEDB38 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___force0, const RuntimeMethod* method)
{
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = ___force0;
		Rigidbody_AddForce_mD64ACF772614FE36CFD8A477A07A407B35DF1A54(__this, L_0, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::AddTorque(UnityEngine.Vector3,UnityEngine.ForceMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_AddTorque_m18EA054D294E8A26F6485C1E933E986BD5BE5F60 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___torque0, int32_t ___mode1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___mode1;
		Rigidbody_AddTorque_Injected_mBD4EFD1DF707E92D237322E8A48B5827D3E8AD6B(__this, (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&___torque0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::AddTorque(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_AddTorque_mB9B6AE5C84CCB626CDDAAA5307F31BC0D9589EE0 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___torque0, const RuntimeMethod* method)
{
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = ___torque0;
		Rigidbody_AddTorque_m18EA054D294E8A26F6485C1E933E986BD5BE5F60(__this, L_0, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::AddForceAtPosition(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.ForceMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_AddForceAtPosition_mF3B282490DFBD57EEA2A67AA44F602368A20D769 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___force0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___position1, int32_t ___mode2, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___mode2;
		Rigidbody_AddForceAtPosition_Injected_m5A653201E107E36E0E70F6A9933B8CE5F3F31E1F(__this, (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&___force0), (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&___position1), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::AddForceAtPosition(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_AddForceAtPosition_m3A5DCFC3E79923C9D8E32A54BC4AAA1E48EEAD6C (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___force0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___position1, const RuntimeMethod* method)
{
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = ___force0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = ___position1;
		Rigidbody_AddForceAtPosition_mF3B282490DFBD57EEA2A67AA44F602368A20D769(__this, L_0, L_1, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::AddExplosionForce(System.Single,UnityEngine.Vector3,System.Single,System.Single,UnityEngine.ForceMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_AddExplosionForce_m8A2C25CE73AC51E1C255929733A4187089BA689A (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, float ___explosionForce0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___explosionPosition1, float ___explosionRadius2, float ___upwardsModifier3, int32_t ___mode4, const RuntimeMethod* method)
{
	{
		float L_0 = ___explosionForce0;
		float L_1 = ___explosionRadius2;
		float L_2 = ___upwardsModifier3;
		int32_t L_3 = ___mode4;
		Rigidbody_AddExplosionForce_Injected_m52842CCC1B482C3713F35132507C8F9652BC5D07(__this, L_0, (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&___explosionPosition1), L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::AddExplosionForce(System.Single,UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_AddExplosionForce_m1FCC884218DB2A0DC0899C6FE4F0B131341C1243 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, float ___explosionForce0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___explosionPosition1, float ___explosionRadius2, const RuntimeMethod* method)
{
	{
		float L_0 = ___explosionForce0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = ___explosionPosition1;
		float L_2 = ___explosionRadius2;
		Rigidbody_AddExplosionForce_m8A2C25CE73AC51E1C255929733A4187089BA689A(__this, L_0, L_1, L_2, (0.0f), 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody__ctor_m7E9FF7B44B8A3B05217339C2C852A4BF620A12E4 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, const RuntimeMethod* method)
{
	{
		Component__ctor_m5E2740C0ACA4B368BC460315FAA2EDBFEAC0B8EF(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::get_velocity_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_get_velocity_Injected_m61C1628D08B48C8971E476FCBB9323CB5EB73DAC (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___ret0, const RuntimeMethod* method)
{
	typedef void (*Rigidbody_get_velocity_Injected_m61C1628D08B48C8971E476FCBB9323CB5EB73DAC_ftn) (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 *, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *);
	static Rigidbody_get_velocity_Injected_m61C1628D08B48C8971E476FCBB9323CB5EB73DAC_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_get_velocity_Injected_m61C1628D08B48C8971E476FCBB9323CB5EB73DAC_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::get_velocity_Injected(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___ret0);
}
// System.Void UnityEngine.Rigidbody::set_velocity_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_set_velocity_Injected_mA0CBA4077CADA05CF30404564E8D6260EF60E07D (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Rigidbody_set_velocity_Injected_mA0CBA4077CADA05CF30404564E8D6260EF60E07D_ftn) (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 *, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *);
	static Rigidbody_set_velocity_Injected_mA0CBA4077CADA05CF30404564E8D6260EF60E07D_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_velocity_Injected_mA0CBA4077CADA05CF30404564E8D6260EF60E07D_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_velocity_Injected(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::get_angularVelocity_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_get_angularVelocity_Injected_mF130145BC134034C2ECB1C4E9BF276F0A63EC0D1 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___ret0, const RuntimeMethod* method)
{
	typedef void (*Rigidbody_get_angularVelocity_Injected_mF130145BC134034C2ECB1C4E9BF276F0A63EC0D1_ftn) (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 *, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *);
	static Rigidbody_get_angularVelocity_Injected_mF130145BC134034C2ECB1C4E9BF276F0A63EC0D1_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_get_angularVelocity_Injected_mF130145BC134034C2ECB1C4E9BF276F0A63EC0D1_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::get_angularVelocity_Injected(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___ret0);
}
// System.Void UnityEngine.Rigidbody::set_angularVelocity_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_set_angularVelocity_Injected_mC5F63763F3701505D5F8E41CCE7F6325F1024783 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Rigidbody_set_angularVelocity_Injected_mC5F63763F3701505D5F8E41CCE7F6325F1024783_ftn) (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 *, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *);
	static Rigidbody_set_angularVelocity_Injected_mC5F63763F3701505D5F8E41CCE7F6325F1024783_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_angularVelocity_Injected_mC5F63763F3701505D5F8E41CCE7F6325F1024783_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_angularVelocity_Injected(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::get_centerOfMass_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_get_centerOfMass_Injected_m15590C0C367EB322CF67A40CFE2A28A814A8766F (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___ret0, const RuntimeMethod* method)
{
	typedef void (*Rigidbody_get_centerOfMass_Injected_m15590C0C367EB322CF67A40CFE2A28A814A8766F_ftn) (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 *, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *);
	static Rigidbody_get_centerOfMass_Injected_m15590C0C367EB322CF67A40CFE2A28A814A8766F_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_get_centerOfMass_Injected_m15590C0C367EB322CF67A40CFE2A28A814A8766F_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::get_centerOfMass_Injected(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___ret0);
}
// System.Void UnityEngine.Rigidbody::set_centerOfMass_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_set_centerOfMass_Injected_m999B2BDCDFCAC0D049AF7A7BE5BEB7A4B75D1D0A (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Rigidbody_set_centerOfMass_Injected_m999B2BDCDFCAC0D049AF7A7BE5BEB7A4B75D1D0A_ftn) (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 *, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *);
	static Rigidbody_set_centerOfMass_Injected_m999B2BDCDFCAC0D049AF7A7BE5BEB7A4B75D1D0A_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_centerOfMass_Injected_m999B2BDCDFCAC0D049AF7A7BE5BEB7A4B75D1D0A_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_centerOfMass_Injected(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::get_worldCenterOfMass_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_get_worldCenterOfMass_Injected_mCF61FE2285D07E76A226249DA7C877F6888C945F (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___ret0, const RuntimeMethod* method)
{
	typedef void (*Rigidbody_get_worldCenterOfMass_Injected_mCF61FE2285D07E76A226249DA7C877F6888C945F_ftn) (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 *, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *);
	static Rigidbody_get_worldCenterOfMass_Injected_mCF61FE2285D07E76A226249DA7C877F6888C945F_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_get_worldCenterOfMass_Injected_mCF61FE2285D07E76A226249DA7C877F6888C945F_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::get_worldCenterOfMass_Injected(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___ret0);
}
// System.Void UnityEngine.Rigidbody::get_inertiaTensor_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_get_inertiaTensor_Injected_m87F89C6379EB9C47DDB03A4F5AE47CBFF9CFD2FF (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___ret0, const RuntimeMethod* method)
{
	typedef void (*Rigidbody_get_inertiaTensor_Injected_m87F89C6379EB9C47DDB03A4F5AE47CBFF9CFD2FF_ftn) (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 *, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *);
	static Rigidbody_get_inertiaTensor_Injected_m87F89C6379EB9C47DDB03A4F5AE47CBFF9CFD2FF_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_get_inertiaTensor_Injected_m87F89C6379EB9C47DDB03A4F5AE47CBFF9CFD2FF_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::get_inertiaTensor_Injected(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___ret0);
}
// System.Void UnityEngine.Rigidbody::set_inertiaTensor_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_set_inertiaTensor_Injected_mB43DBD3F1C979D04B0955484E0B14A971C7AE838 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Rigidbody_set_inertiaTensor_Injected_mB43DBD3F1C979D04B0955484E0B14A971C7AE838_ftn) (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 *, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *);
	static Rigidbody_set_inertiaTensor_Injected_mB43DBD3F1C979D04B0955484E0B14A971C7AE838_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_inertiaTensor_Injected_mB43DBD3F1C979D04B0955484E0B14A971C7AE838_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_inertiaTensor_Injected(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::get_position_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_get_position_Injected_m8B581634C88AB745341D4ACF1CC12544769EC019 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___ret0, const RuntimeMethod* method)
{
	typedef void (*Rigidbody_get_position_Injected_m8B581634C88AB745341D4ACF1CC12544769EC019_ftn) (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 *, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *);
	static Rigidbody_get_position_Injected_m8B581634C88AB745341D4ACF1CC12544769EC019_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_get_position_Injected_m8B581634C88AB745341D4ACF1CC12544769EC019_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::get_position_Injected(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___ret0);
}
// System.Void UnityEngine.Rigidbody::set_position_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_set_position_Injected_m3210674B4144CD8E599226F6343BC541A9975581 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Rigidbody_set_position_Injected_m3210674B4144CD8E599226F6343BC541A9975581_ftn) (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 *, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *);
	static Rigidbody_set_position_Injected_m3210674B4144CD8E599226F6343BC541A9975581_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_position_Injected_m3210674B4144CD8E599226F6343BC541A9975581_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_position_Injected(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::get_rotation_Injected(UnityEngine.Quaternion&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_get_rotation_Injected_m5FEC1EC2B3CFCE6585C63CD64ECAE39828B73C91 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * ___ret0, const RuntimeMethod* method)
{
	typedef void (*Rigidbody_get_rotation_Injected_m5FEC1EC2B3CFCE6585C63CD64ECAE39828B73C91_ftn) (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 *, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 *);
	static Rigidbody_get_rotation_Injected_m5FEC1EC2B3CFCE6585C63CD64ECAE39828B73C91_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_get_rotation_Injected_m5FEC1EC2B3CFCE6585C63CD64ECAE39828B73C91_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::get_rotation_Injected(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___ret0);
}
// System.Void UnityEngine.Rigidbody::set_rotation_Injected(UnityEngine.Quaternion&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_set_rotation_Injected_m52738493BB4ACEDC29288DA8905907C1918A99C3 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Rigidbody_set_rotation_Injected_m52738493BB4ACEDC29288DA8905907C1918A99C3_ftn) (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 *, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 *);
	static Rigidbody_set_rotation_Injected_m52738493BB4ACEDC29288DA8905907C1918A99C3_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_rotation_Injected_m52738493BB4ACEDC29288DA8905907C1918A99C3_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_rotation_Injected(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::MovePosition_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_MovePosition_Injected_mFD8BDE08912A4C3FC240A8834C3394A88349C440 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___position0, const RuntimeMethod* method)
{
	typedef void (*Rigidbody_MovePosition_Injected_mFD8BDE08912A4C3FC240A8834C3394A88349C440_ftn) (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 *, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *);
	static Rigidbody_MovePosition_Injected_mFD8BDE08912A4C3FC240A8834C3394A88349C440_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_MovePosition_Injected_mFD8BDE08912A4C3FC240A8834C3394A88349C440_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::MovePosition_Injected(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___position0);
}
// System.Void UnityEngine.Rigidbody::MoveRotation_Injected(UnityEngine.Quaternion&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_MoveRotation_Injected_m283E5FB82CB1721DC5220311967ED0E6ED831BF0 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * ___rot0, const RuntimeMethod* method)
{
	typedef void (*Rigidbody_MoveRotation_Injected_m283E5FB82CB1721DC5220311967ED0E6ED831BF0_ftn) (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 *, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 *);
	static Rigidbody_MoveRotation_Injected_m283E5FB82CB1721DC5220311967ED0E6ED831BF0_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_MoveRotation_Injected_m283E5FB82CB1721DC5220311967ED0E6ED831BF0_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::MoveRotation_Injected(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___rot0);
}
// System.Void UnityEngine.Rigidbody::GetPointVelocity_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_GetPointVelocity_Injected_mFFC7165D5058AA5B62F08545554F78EE25FACC2E (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___worldPoint0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___ret1, const RuntimeMethod* method)
{
	typedef void (*Rigidbody_GetPointVelocity_Injected_mFFC7165D5058AA5B62F08545554F78EE25FACC2E_ftn) (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 *, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *);
	static Rigidbody_GetPointVelocity_Injected_mFFC7165D5058AA5B62F08545554F78EE25FACC2E_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_GetPointVelocity_Injected_mFFC7165D5058AA5B62F08545554F78EE25FACC2E_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::GetPointVelocity_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___worldPoint0, ___ret1);
}
// System.Void UnityEngine.Rigidbody::AddForce_Injected(UnityEngine.Vector3&,UnityEngine.ForceMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_AddForce_Injected_mEB8DB621D03BE740C5CBD472E873FF8B6EF6C119 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___force0, int32_t ___mode1, const RuntimeMethod* method)
{
	typedef void (*Rigidbody_AddForce_Injected_mEB8DB621D03BE740C5CBD472E873FF8B6EF6C119_ftn) (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 *, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *, int32_t);
	static Rigidbody_AddForce_Injected_mEB8DB621D03BE740C5CBD472E873FF8B6EF6C119_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_AddForce_Injected_mEB8DB621D03BE740C5CBD472E873FF8B6EF6C119_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::AddForce_Injected(UnityEngine.Vector3&,UnityEngine.ForceMode)");
	_il2cpp_icall_func(__this, ___force0, ___mode1);
}
// System.Void UnityEngine.Rigidbody::AddTorque_Injected(UnityEngine.Vector3&,UnityEngine.ForceMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_AddTorque_Injected_mBD4EFD1DF707E92D237322E8A48B5827D3E8AD6B (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___torque0, int32_t ___mode1, const RuntimeMethod* method)
{
	typedef void (*Rigidbody_AddTorque_Injected_mBD4EFD1DF707E92D237322E8A48B5827D3E8AD6B_ftn) (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 *, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *, int32_t);
	static Rigidbody_AddTorque_Injected_mBD4EFD1DF707E92D237322E8A48B5827D3E8AD6B_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_AddTorque_Injected_mBD4EFD1DF707E92D237322E8A48B5827D3E8AD6B_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::AddTorque_Injected(UnityEngine.Vector3&,UnityEngine.ForceMode)");
	_il2cpp_icall_func(__this, ___torque0, ___mode1);
}
// System.Void UnityEngine.Rigidbody::AddForceAtPosition_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.ForceMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_AddForceAtPosition_Injected_m5A653201E107E36E0E70F6A9933B8CE5F3F31E1F (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___force0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___position1, int32_t ___mode2, const RuntimeMethod* method)
{
	typedef void (*Rigidbody_AddForceAtPosition_Injected_m5A653201E107E36E0E70F6A9933B8CE5F3F31E1F_ftn) (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 *, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *, int32_t);
	static Rigidbody_AddForceAtPosition_Injected_m5A653201E107E36E0E70F6A9933B8CE5F3F31E1F_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_AddForceAtPosition_Injected_m5A653201E107E36E0E70F6A9933B8CE5F3F31E1F_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::AddForceAtPosition_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.ForceMode)");
	_il2cpp_icall_func(__this, ___force0, ___position1, ___mode2);
}
// System.Void UnityEngine.Rigidbody::AddExplosionForce_Injected(System.Single,UnityEngine.Vector3&,System.Single,System.Single,UnityEngine.ForceMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_AddExplosionForce_Injected_m52842CCC1B482C3713F35132507C8F9652BC5D07 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, float ___explosionForce0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___explosionPosition1, float ___explosionRadius2, float ___upwardsModifier3, int32_t ___mode4, const RuntimeMethod* method)
{
	typedef void (*Rigidbody_AddExplosionForce_Injected_m52842CCC1B482C3713F35132507C8F9652BC5D07_ftn) (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 *, float, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *, float, float, int32_t);
	static Rigidbody_AddExplosionForce_Injected_m52842CCC1B482C3713F35132507C8F9652BC5D07_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_AddExplosionForce_Injected_m52842CCC1B482C3713F35132507C8F9652BC5D07_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::AddExplosionForce_Injected(System.Single,UnityEngine.Vector3&,System.Single,System.Single,UnityEngine.ForceMode)");
	_il2cpp_icall_func(__this, ___explosionForce0, ___explosionPosition1, ___explosionRadius2, ___upwardsModifier3, ___mode4);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Single UnityEngine.SoftJointLimit::get_limit()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float SoftJointLimit_get_limit_m40164161D58BA46F6F1EDA382FC552607BFC7D03 (SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Limit_0();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		float L_1 = V_0;
		return L_1;
	}
}
IL2CPP_EXTERN_C  float SoftJointLimit_get_limit_m40164161D58BA46F6F1EDA382FC552607BFC7D03_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 * _thisAdjusted = reinterpret_cast<SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 *>(__this + _offset);
	return SoftJointLimit_get_limit_m40164161D58BA46F6F1EDA382FC552607BFC7D03(_thisAdjusted, method);
}
// System.Void UnityEngine.SoftJointLimit::set_limit(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SoftJointLimit_set_limit_m65386F3B7DE799354F233666054E36B4E244FE26 (SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_Limit_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void SoftJointLimit_set_limit_m65386F3B7DE799354F233666054E36B4E244FE26_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 * _thisAdjusted = reinterpret_cast<SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 *>(__this + _offset);
	SoftJointLimit_set_limit_m65386F3B7DE799354F233666054E36B4E244FE26(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.SoftJointLimit::get_bounciness()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float SoftJointLimit_get_bounciness_m387D59B7712B1E70CFA337F9E311573C0D928846 (SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Bounciness_1();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		float L_1 = V_0;
		return L_1;
	}
}
IL2CPP_EXTERN_C  float SoftJointLimit_get_bounciness_m387D59B7712B1E70CFA337F9E311573C0D928846_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 * _thisAdjusted = reinterpret_cast<SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 *>(__this + _offset);
	return SoftJointLimit_get_bounciness_m387D59B7712B1E70CFA337F9E311573C0D928846(_thisAdjusted, method);
}
// System.Void UnityEngine.SoftJointLimit::set_bounciness(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SoftJointLimit_set_bounciness_mC19E24774A8CF6C1229AE5D83AFE13F9F6D0938B (SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_Bounciness_1(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void SoftJointLimit_set_bounciness_mC19E24774A8CF6C1229AE5D83AFE13F9F6D0938B_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 * _thisAdjusted = reinterpret_cast<SoftJointLimit_t6E6117F3F5BF24C61A91836119DF9840D752F7E9 *>(__this + _offset);
	SoftJointLimit_set_bounciness_mC19E24774A8CF6C1229AE5D83AFE13F9F6D0938B(_thisAdjusted, ___value0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Single UnityEngine.SoftJointLimitSpring::get_spring()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float SoftJointLimitSpring_get_spring_mC5D04E551048F33C874E4A7022E6FE582094AE21 (SoftJointLimitSpring_t8D4D658B29266F9573CB134D0DBC15017488FDC2 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Spring_0();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		float L_1 = V_0;
		return L_1;
	}
}
IL2CPP_EXTERN_C  float SoftJointLimitSpring_get_spring_mC5D04E551048F33C874E4A7022E6FE582094AE21_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	SoftJointLimitSpring_t8D4D658B29266F9573CB134D0DBC15017488FDC2 * _thisAdjusted = reinterpret_cast<SoftJointLimitSpring_t8D4D658B29266F9573CB134D0DBC15017488FDC2 *>(__this + _offset);
	return SoftJointLimitSpring_get_spring_mC5D04E551048F33C874E4A7022E6FE582094AE21(_thisAdjusted, method);
}
// System.Void UnityEngine.SoftJointLimitSpring::set_spring(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SoftJointLimitSpring_set_spring_mCC90918E4E61E1DBCB55653A3357B168B3281232 (SoftJointLimitSpring_t8D4D658B29266F9573CB134D0DBC15017488FDC2 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_Spring_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void SoftJointLimitSpring_set_spring_mCC90918E4E61E1DBCB55653A3357B168B3281232_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	SoftJointLimitSpring_t8D4D658B29266F9573CB134D0DBC15017488FDC2 * _thisAdjusted = reinterpret_cast<SoftJointLimitSpring_t8D4D658B29266F9573CB134D0DBC15017488FDC2 *>(__this + _offset);
	SoftJointLimitSpring_set_spring_mCC90918E4E61E1DBCB55653A3357B168B3281232(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.SoftJointLimitSpring::get_damper()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float SoftJointLimitSpring_get_damper_mD1AAF428D25AFDF21E1CD5B349ACBE64D7C3A006 (SoftJointLimitSpring_t8D4D658B29266F9573CB134D0DBC15017488FDC2 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Damper_1();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		float L_1 = V_0;
		return L_1;
	}
}
IL2CPP_EXTERN_C  float SoftJointLimitSpring_get_damper_mD1AAF428D25AFDF21E1CD5B349ACBE64D7C3A006_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	SoftJointLimitSpring_t8D4D658B29266F9573CB134D0DBC15017488FDC2 * _thisAdjusted = reinterpret_cast<SoftJointLimitSpring_t8D4D658B29266F9573CB134D0DBC15017488FDC2 *>(__this + _offset);
	return SoftJointLimitSpring_get_damper_mD1AAF428D25AFDF21E1CD5B349ACBE64D7C3A006(_thisAdjusted, method);
}
// System.Void UnityEngine.SoftJointLimitSpring::set_damper(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SoftJointLimitSpring_set_damper_m03BF5BE752A1620C6691D7DB663D05F0FFD55534 (SoftJointLimitSpring_t8D4D658B29266F9573CB134D0DBC15017488FDC2 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_Damper_1(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void SoftJointLimitSpring_set_damper_m03BF5BE752A1620C6691D7DB663D05F0FFD55534_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	SoftJointLimitSpring_t8D4D658B29266F9573CB134D0DBC15017488FDC2 * _thisAdjusted = reinterpret_cast<SoftJointLimitSpring_t8D4D658B29266F9573CB134D0DBC15017488FDC2 *>(__this + _offset);
	SoftJointLimitSpring_set_damper_m03BF5BE752A1620C6691D7DB663D05F0FFD55534(_thisAdjusted, ___value0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Vector3 UnityEngine.SphereCollider::get_center()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  SphereCollider_get_center_mE7AD1AC46974FF23EEA621B872E2962E52A1DB00 (SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F * __this, const RuntimeMethod* method)
{
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		SphereCollider_get_center_Injected_mE8ECA23236D73D0685D76F15CB21BA1C09C9F6DB(__this, (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_0), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.SphereCollider::set_center(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SphereCollider_set_center_m325070F5252B4A2EA567B653CAE3285F101FA3EE (SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value0, const RuntimeMethod* method)
{
	{
		SphereCollider_set_center_Injected_mB5E101BA19F903AF24B7FAE2D125BBCC781FF82C(__this, (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.SphereCollider::get_radius()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float SphereCollider_get_radius_m255804173C17314FD9538AE45C4A46D4882BC094 (SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F * __this, const RuntimeMethod* method)
{
	typedef float (*SphereCollider_get_radius_m255804173C17314FD9538AE45C4A46D4882BC094_ftn) (SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F *);
	static SphereCollider_get_radius_m255804173C17314FD9538AE45C4A46D4882BC094_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SphereCollider_get_radius_m255804173C17314FD9538AE45C4A46D4882BC094_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SphereCollider::get_radius()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.SphereCollider::get_center_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SphereCollider_get_center_Injected_mE8ECA23236D73D0685D76F15CB21BA1C09C9F6DB (SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___ret0, const RuntimeMethod* method)
{
	typedef void (*SphereCollider_get_center_Injected_mE8ECA23236D73D0685D76F15CB21BA1C09C9F6DB_ftn) (SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F *, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *);
	static SphereCollider_get_center_Injected_mE8ECA23236D73D0685D76F15CB21BA1C09C9F6DB_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SphereCollider_get_center_Injected_mE8ECA23236D73D0685D76F15CB21BA1C09C9F6DB_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SphereCollider::get_center_Injected(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___ret0);
}
// System.Void UnityEngine.SphereCollider::set_center_Injected(UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SphereCollider_set_center_Injected_mB5E101BA19F903AF24B7FAE2D125BBCC781FF82C (SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___value0, const RuntimeMethod* method)
{
	typedef void (*SphereCollider_set_center_Injected_mB5E101BA19F903AF24B7FAE2D125BBCC781FF82C_ftn) (SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F *, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *);
	static SphereCollider_set_center_Injected_mB5E101BA19F903AF24B7FAE2D125BBCC781FF82C_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SphereCollider_set_center_Injected_mB5E101BA19F903AF24B7FAE2D125BBCC781FF82C_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SphereCollider::set_center_Injected(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Single UnityEngine.SpringJoint::get_spring()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float SpringJoint_get_spring_mAA4F0F503D65A513A2B437ED7EB4E0D24973A28B (SpringJoint_t2D196194480E339F728B463619720385523B09AF * __this, const RuntimeMethod* method)
{
	typedef float (*SpringJoint_get_spring_mAA4F0F503D65A513A2B437ED7EB4E0D24973A28B_ftn) (SpringJoint_t2D196194480E339F728B463619720385523B09AF *);
	static SpringJoint_get_spring_mAA4F0F503D65A513A2B437ED7EB4E0D24973A28B_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SpringJoint_get_spring_mAA4F0F503D65A513A2B437ED7EB4E0D24973A28B_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SpringJoint::get_spring()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Single UnityEngine.SpringJoint::get_damper()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float SpringJoint_get_damper_m13C4C3A0902157FB4F5768C2F7B5DF0AFD3B5019 (SpringJoint_t2D196194480E339F728B463619720385523B09AF * __this, const RuntimeMethod* method)
{
	typedef float (*SpringJoint_get_damper_m13C4C3A0902157FB4F5768C2F7B5DF0AFD3B5019_ftn) (SpringJoint_t2D196194480E339F728B463619720385523B09AF *);
	static SpringJoint_get_damper_m13C4C3A0902157FB4F5768C2F7B5DF0AFD3B5019_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SpringJoint_get_damper_m13C4C3A0902157FB4F5768C2F7B5DF0AFD3B5019_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SpringJoint::get_damper()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Single UnityEngine.SpringJoint::get_maxDistance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float SpringJoint_get_maxDistance_mBA26CEC9272132A773229CC1CC929D6DC5BC047E (SpringJoint_t2D196194480E339F728B463619720385523B09AF * __this, const RuntimeMethod* method)
{
	typedef float (*SpringJoint_get_maxDistance_mBA26CEC9272132A773229CC1CC929D6DC5BC047E_ftn) (SpringJoint_t2D196194480E339F728B463619720385523B09AF *);
	static SpringJoint_get_maxDistance_mBA26CEC9272132A773229CC1CC929D6DC5BC047E_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SpringJoint_get_maxDistance_mBA26CEC9272132A773229CC1CC929D6DC5BC047E_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SpringJoint::get_maxDistance()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
