﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.String
struct String_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.CapsuleCollider
struct CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1;
// UnityEngine.CapsuleCollider[]
struct CapsuleColliderU5BU5D_tABE59B2A46DD78D82A1524BBD9A0672473BA5CD7;
// UnityEngine.Cloth
struct Cloth_t119B34B958B50B29353D15B313669817FC2F91C5;
// UnityEngine.ClothSphereColliderPair[]
struct ClothSphereColliderPairU5BU5D_t4EC27C896621FFA6D3CCF95197C4E29A616AB8F7;
// UnityEngine.SphereCollider
struct SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F;


struct CapsuleColliderU5BU5D_tABE59B2A46DD78D82A1524BBD9A0672473BA5CD7;
struct ClothSphereColliderPairU5BU5D_t4EC27C896621FFA6D3CCF95197C4E29A616AB8F7;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t05D97FE700F04A06C0CFE38CDD0C3740B1A22234 
{
public:

public:
};


// System.Object

struct Il2CppArrayBounds;

// System.Array


// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.ClothSphereColliderPair
struct  ClothSphereColliderPair_tAB0F7E085E6423383FC7B1DEC003F27A9078CCCB 
{
public:
	// UnityEngine.SphereCollider UnityEngine.ClothSphereColliderPair::<first>k__BackingField
	SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F * ___U3CfirstU3Ek__BackingField_0;
	// UnityEngine.SphereCollider UnityEngine.ClothSphereColliderPair::<second>k__BackingField
	SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F * ___U3CsecondU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CfirstU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ClothSphereColliderPair_tAB0F7E085E6423383FC7B1DEC003F27A9078CCCB, ___U3CfirstU3Ek__BackingField_0)); }
	inline SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F * get_U3CfirstU3Ek__BackingField_0() const { return ___U3CfirstU3Ek__BackingField_0; }
	inline SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F ** get_address_of_U3CfirstU3Ek__BackingField_0() { return &___U3CfirstU3Ek__BackingField_0; }
	inline void set_U3CfirstU3Ek__BackingField_0(SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F * value)
	{
		___U3CfirstU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CfirstU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CsecondU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ClothSphereColliderPair_tAB0F7E085E6423383FC7B1DEC003F27A9078CCCB, ___U3CsecondU3Ek__BackingField_1)); }
	inline SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F * get_U3CsecondU3Ek__BackingField_1() const { return ___U3CsecondU3Ek__BackingField_1; }
	inline SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F ** get_address_of_U3CsecondU3Ek__BackingField_1() { return &___U3CsecondU3Ek__BackingField_1; }
	inline void set_U3CsecondU3Ek__BackingField_1(SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F * value)
	{
		___U3CsecondU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CsecondU3Ek__BackingField_1), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.ClothSphereColliderPair
struct ClothSphereColliderPair_tAB0F7E085E6423383FC7B1DEC003F27A9078CCCB_marshaled_pinvoke
{
	SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F * ___U3CfirstU3Ek__BackingField_0;
	SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F * ___U3CsecondU3Ek__BackingField_1;
};
// Native definition for COM marshalling of UnityEngine.ClothSphereColliderPair
struct ClothSphereColliderPair_tAB0F7E085E6423383FC7B1DEC003F27A9078CCCB_marshaled_com
{
	SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F * ___U3CfirstU3Ek__BackingField_0;
	SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F * ___U3CsecondU3Ek__BackingField_1;
};

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.Cloth
struct  Cloth_t119B34B958B50B29353D15B313669817FC2F91C5  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:
	// System.Single UnityEngine.Cloth::<useContinuousCollision>k__BackingField
	float ___U3CuseContinuousCollisionU3Ek__BackingField_4;
	// System.Boolean UnityEngine.Cloth::<selfCollision>k__BackingField
	bool ___U3CselfCollisionU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CuseContinuousCollisionU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Cloth_t119B34B958B50B29353D15B313669817FC2F91C5, ___U3CuseContinuousCollisionU3Ek__BackingField_4)); }
	inline float get_U3CuseContinuousCollisionU3Ek__BackingField_4() const { return ___U3CuseContinuousCollisionU3Ek__BackingField_4; }
	inline float* get_address_of_U3CuseContinuousCollisionU3Ek__BackingField_4() { return &___U3CuseContinuousCollisionU3Ek__BackingField_4; }
	inline void set_U3CuseContinuousCollisionU3Ek__BackingField_4(float value)
	{
		___U3CuseContinuousCollisionU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CselfCollisionU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Cloth_t119B34B958B50B29353D15B313669817FC2F91C5, ___U3CselfCollisionU3Ek__BackingField_5)); }
	inline bool get_U3CselfCollisionU3Ek__BackingField_5() const { return ___U3CselfCollisionU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CselfCollisionU3Ek__BackingField_5() { return &___U3CselfCollisionU3Ek__BackingField_5; }
	inline void set_U3CselfCollisionU3Ek__BackingField_5(bool value)
	{
		___U3CselfCollisionU3Ek__BackingField_5 = value;
	}
};


// UnityEngine.Collider
struct  Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.CapsuleCollider
struct  CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1  : public Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF
{
public:

public:
};


// UnityEngine.SphereCollider
struct  SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F  : public Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// UnityEngine.CapsuleCollider[]
struct CapsuleColliderU5BU5D_tABE59B2A46DD78D82A1524BBD9A0672473BA5CD7  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1 * m_Items[1];

public:
	inline CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.ClothSphereColliderPair[]
struct ClothSphereColliderPairU5BU5D_t4EC27C896621FFA6D3CCF95197C4E29A616AB8F7  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) ClothSphereColliderPair_tAB0F7E085E6423383FC7B1DEC003F27A9078CCCB  m_Items[1];

public:
	inline ClothSphereColliderPair_tAB0F7E085E6423383FC7B1DEC003F27A9078CCCB  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ClothSphereColliderPair_tAB0F7E085E6423383FC7B1DEC003F27A9078CCCB * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ClothSphereColliderPair_tAB0F7E085E6423383FC7B1DEC003F27A9078CCCB  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___U3CfirstU3Ek__BackingField_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___U3CsecondU3Ek__BackingField_1), (void*)NULL);
		#endif
	}
	inline ClothSphereColliderPair_tAB0F7E085E6423383FC7B1DEC003F27A9078CCCB  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ClothSphereColliderPair_tAB0F7E085E6423383FC7B1DEC003F27A9078CCCB * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ClothSphereColliderPair_tAB0F7E085E6423383FC7B1DEC003F27A9078CCCB  value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___U3CfirstU3Ek__BackingField_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___U3CsecondU3Ek__BackingField_1), (void*)NULL);
		#endif
	}
};



// UnityEngine.SphereCollider UnityEngine.ClothSphereColliderPair::get_first()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F * ClothSphereColliderPair_get_first_m6EE02F2490C866EE3DE10C883F50DE6BDD2F2E13_inline (ClothSphereColliderPair_tAB0F7E085E6423383FC7B1DEC003F27A9078CCCB * __this, const RuntimeMethod* method);
// UnityEngine.SphereCollider UnityEngine.ClothSphereColliderPair::get_second()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F * ClothSphereColliderPair_get_second_m2E16CC7EA6FA1CA4338A2F9C82497B6FD6C496BB_inline (ClothSphereColliderPair_tAB0F7E085E6423383FC7B1DEC003F27A9078CCCB * __this, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.CapsuleCollider[] UnityEngine.Cloth::get_capsuleColliders()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CapsuleColliderU5BU5D_tABE59B2A46DD78D82A1524BBD9A0672473BA5CD7* Cloth_get_capsuleColliders_m63C7E3486D024D778CE9800B916E50509DA5118C (Cloth_t119B34B958B50B29353D15B313669817FC2F91C5 * __this, const RuntimeMethod* method)
{
	typedef CapsuleColliderU5BU5D_tABE59B2A46DD78D82A1524BBD9A0672473BA5CD7* (*Cloth_get_capsuleColliders_m63C7E3486D024D778CE9800B916E50509DA5118C_ftn) (Cloth_t119B34B958B50B29353D15B313669817FC2F91C5 *);
	static Cloth_get_capsuleColliders_m63C7E3486D024D778CE9800B916E50509DA5118C_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Cloth_get_capsuleColliders_m63C7E3486D024D778CE9800B916E50509DA5118C_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Cloth::get_capsuleColliders()");
	CapsuleColliderU5BU5D_tABE59B2A46DD78D82A1524BBD9A0672473BA5CD7* retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// UnityEngine.ClothSphereColliderPair[] UnityEngine.Cloth::get_sphereColliders()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ClothSphereColliderPairU5BU5D_t4EC27C896621FFA6D3CCF95197C4E29A616AB8F7* Cloth_get_sphereColliders_mFF63C8FC5B1F96C7A913AC4546CE54BCB38186BF (Cloth_t119B34B958B50B29353D15B313669817FC2F91C5 * __this, const RuntimeMethod* method)
{
	typedef ClothSphereColliderPairU5BU5D_t4EC27C896621FFA6D3CCF95197C4E29A616AB8F7* (*Cloth_get_sphereColliders_mFF63C8FC5B1F96C7A913AC4546CE54BCB38186BF_ftn) (Cloth_t119B34B958B50B29353D15B313669817FC2F91C5 *);
	static Cloth_get_sphereColliders_mFF63C8FC5B1F96C7A913AC4546CE54BCB38186BF_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Cloth_get_sphereColliders_mFF63C8FC5B1F96C7A913AC4546CE54BCB38186BF_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Cloth::get_sphereColliders()");
	ClothSphereColliderPairU5BU5D_t4EC27C896621FFA6D3CCF95197C4E29A616AB8F7* retVal = _il2cpp_icall_func(__this);
	return retVal;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UnityEngine.ClothSphereColliderPair
IL2CPP_EXTERN_C void ClothSphereColliderPair_tAB0F7E085E6423383FC7B1DEC003F27A9078CCCB_marshal_pinvoke(const ClothSphereColliderPair_tAB0F7E085E6423383FC7B1DEC003F27A9078CCCB& unmarshaled, ClothSphereColliderPair_tAB0F7E085E6423383FC7B1DEC003F27A9078CCCB_marshaled_pinvoke& marshaled)
{
	Exception_t* ___U3CfirstU3Ek__BackingField_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field '<first>k__BackingField' of type 'ClothSphereColliderPair': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___U3CfirstU3Ek__BackingField_0Exception, NULL);
}
IL2CPP_EXTERN_C void ClothSphereColliderPair_tAB0F7E085E6423383FC7B1DEC003F27A9078CCCB_marshal_pinvoke_back(const ClothSphereColliderPair_tAB0F7E085E6423383FC7B1DEC003F27A9078CCCB_marshaled_pinvoke& marshaled, ClothSphereColliderPair_tAB0F7E085E6423383FC7B1DEC003F27A9078CCCB& unmarshaled)
{
	Exception_t* ___U3CfirstU3Ek__BackingField_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field '<first>k__BackingField' of type 'ClothSphereColliderPair': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___U3CfirstU3Ek__BackingField_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ClothSphereColliderPair
IL2CPP_EXTERN_C void ClothSphereColliderPair_tAB0F7E085E6423383FC7B1DEC003F27A9078CCCB_marshal_pinvoke_cleanup(ClothSphereColliderPair_tAB0F7E085E6423383FC7B1DEC003F27A9078CCCB_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ClothSphereColliderPair
IL2CPP_EXTERN_C void ClothSphereColliderPair_tAB0F7E085E6423383FC7B1DEC003F27A9078CCCB_marshal_com(const ClothSphereColliderPair_tAB0F7E085E6423383FC7B1DEC003F27A9078CCCB& unmarshaled, ClothSphereColliderPair_tAB0F7E085E6423383FC7B1DEC003F27A9078CCCB_marshaled_com& marshaled)
{
	Exception_t* ___U3CfirstU3Ek__BackingField_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field '<first>k__BackingField' of type 'ClothSphereColliderPair': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___U3CfirstU3Ek__BackingField_0Exception, NULL);
}
IL2CPP_EXTERN_C void ClothSphereColliderPair_tAB0F7E085E6423383FC7B1DEC003F27A9078CCCB_marshal_com_back(const ClothSphereColliderPair_tAB0F7E085E6423383FC7B1DEC003F27A9078CCCB_marshaled_com& marshaled, ClothSphereColliderPair_tAB0F7E085E6423383FC7B1DEC003F27A9078CCCB& unmarshaled)
{
	Exception_t* ___U3CfirstU3Ek__BackingField_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field '<first>k__BackingField' of type 'ClothSphereColliderPair': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___U3CfirstU3Ek__BackingField_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.ClothSphereColliderPair
IL2CPP_EXTERN_C void ClothSphereColliderPair_tAB0F7E085E6423383FC7B1DEC003F27A9078CCCB_marshal_com_cleanup(ClothSphereColliderPair_tAB0F7E085E6423383FC7B1DEC003F27A9078CCCB_marshaled_com& marshaled)
{
}
// UnityEngine.SphereCollider UnityEngine.ClothSphereColliderPair::get_first()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F * ClothSphereColliderPair_get_first_m6EE02F2490C866EE3DE10C883F50DE6BDD2F2E13 (ClothSphereColliderPair_tAB0F7E085E6423383FC7B1DEC003F27A9078CCCB * __this, const RuntimeMethod* method)
{
	{
		SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F * L_0 = __this->get_U3CfirstU3Ek__BackingField_0();
		return L_0;
	}
}
IL2CPP_EXTERN_C  SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F * ClothSphereColliderPair_get_first_m6EE02F2490C866EE3DE10C883F50DE6BDD2F2E13_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ClothSphereColliderPair_tAB0F7E085E6423383FC7B1DEC003F27A9078CCCB * _thisAdjusted = reinterpret_cast<ClothSphereColliderPair_tAB0F7E085E6423383FC7B1DEC003F27A9078CCCB *>(__this + _offset);
	return ClothSphereColliderPair_get_first_m6EE02F2490C866EE3DE10C883F50DE6BDD2F2E13_inline(_thisAdjusted, method);
}
// UnityEngine.SphereCollider UnityEngine.ClothSphereColliderPair::get_second()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F * ClothSphereColliderPair_get_second_m2E16CC7EA6FA1CA4338A2F9C82497B6FD6C496BB (ClothSphereColliderPair_tAB0F7E085E6423383FC7B1DEC003F27A9078CCCB * __this, const RuntimeMethod* method)
{
	{
		SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F * L_0 = __this->get_U3CsecondU3Ek__BackingField_1();
		return L_0;
	}
}
IL2CPP_EXTERN_C  SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F * ClothSphereColliderPair_get_second_m2E16CC7EA6FA1CA4338A2F9C82497B6FD6C496BB_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ClothSphereColliderPair_tAB0F7E085E6423383FC7B1DEC003F27A9078CCCB * _thisAdjusted = reinterpret_cast<ClothSphereColliderPair_tAB0F7E085E6423383FC7B1DEC003F27A9078CCCB *>(__this + _offset);
	return ClothSphereColliderPair_get_second_m2E16CC7EA6FA1CA4338A2F9C82497B6FD6C496BB_inline(_thisAdjusted, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F * ClothSphereColliderPair_get_first_m6EE02F2490C866EE3DE10C883F50DE6BDD2F2E13_inline (ClothSphereColliderPair_tAB0F7E085E6423383FC7B1DEC003F27A9078CCCB * __this, const RuntimeMethod* method)
{
	{
		SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F * L_0 = __this->get_U3CfirstU3Ek__BackingField_0();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F * ClothSphereColliderPair_get_second_m2E16CC7EA6FA1CA4338A2F9C82497B6FD6C496BB_inline (ClothSphereColliderPair_tAB0F7E085E6423383FC7B1DEC003F27A9078CCCB * __this, const RuntimeMethod* method)
{
	{
		SphereCollider_tAC3E5E20B385DF1C0B17F3EA5C7214F71367706F * L_0 = __this->get_U3CsecondU3Ek__BackingField_1();
		return L_0;
	}
}
